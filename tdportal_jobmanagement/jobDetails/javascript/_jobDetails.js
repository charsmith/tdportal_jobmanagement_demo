﻿var mapping = { 'ignore': ["__ko_mapping__"] }

var job_details = [];

$.getJSON("JSON/_jobDetails.JSON", function (data) {
    job_details = data;
});

$('#_jobDetails').live('pagebeforecreate', function (event) {
    ko.setTemplateEngine(new ko.nativeTemplateEngine);
   
});

$(document).on('pageshow', '#_jobDetails', function (event) {
    window.setTimeout(function () {
        var pageObj = new jobDetailsViewModel();
        ko.applyBindings(pageObj);
    }, 200);
});

var jobFieldViewModel = function (job_field) {
    var self = this;
    //self.name = name;
    //self.value = value;
    ko.mapping.fromJS(job_field, [], self);
};

var jobDetailsViewModel = function () {
    var self = this;
    self.jobFields = ko.observableArray();
    $.each(job_details, function (key, val) {
        self.jobFields.push(new jobFieldViewModel(val));
    });

    self.redirectToJobManagement = function () {
        window.setTimeout(function () {
            window.location.href = '../jobManagement/jobManagement.html'
        }, 200);
    };
}