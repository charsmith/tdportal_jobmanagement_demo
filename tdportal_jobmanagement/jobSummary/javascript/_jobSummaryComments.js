﻿var jobSummaryComments = function (self) {

    //Filtering comments
    self.filterComments = function () {
        if (!(self.filterValidate()))
            return false;
        $("#popupFilterComments").popup('close');
        self.getFilteredComments();
        window.setTimeout(function getCommentsDelay() {
            self.buildLists();
        }, 1000);
    };

    //Validates the selected filter options when submit button is clicked.
    self.filterValidate = function () {
        $("#popupFilterComments").popup('close');
        if (!($('#filterChkIS')[0].checked || $('#filterChkJobIns')[0].checked || $('#filterChkProductionReport')[0].checked)) {
            var msg = "Please select at least one filter option"
            $('#alertmsg').html(msg);
            $("#popupDialog").popup('open');
            $("#okBut").attr("onclick", "$('#popupDialog').popup('close');$('#popupFilterComments').popup('open');");
            return false;
        }
        return true;
    }


    //Gets the comments based on the selected filter and loads them into UI
    self.getFilteredComments = function () {
        var filterComments = {};
        filterComments["wantsIS"] = ($('#filterChkIS').attr('checked') == 'checked') ? 1 : 0;
        filterComments["wantsJobInstructions"] = ($('#filterChkJobIns').attr('checked') == 'checked') ? 1 : 0;
        filterComments["wantsProductionReport"] = ($('#filterChkProductionReport').attr('checked') == 'checked') ? 1 : 0;
        postCORS(serviceURLDomain + "api/Comments/" + self.facilityId + "/" + self.jobNumber, JSON.stringify(filterComments), function (comments_data) {
            self.loadCommentsData(comments_data);
        }, function (error_response) {
            self.gData.push({
                "list": "commentsDict",
                "items": []
            });
            showErrorResponseText(error_response,false);
        });
    };


    //Loads the comments into UI.
    self.loadCommentsData = function (comments_data) {
        var comments_dict = $.grep(self.gData, function (obj) {
            return obj.list === "commentsDict";
        });
        if (comments_dict.length == 0) {
            self.gData.push({
                "list": "commentsDict",
                "items": comments_data
            });
        }
        else {
            comments_dict[0].items = comments_data;
        }
    };

    //Adds new comments to the job when user clicks add comment button in a popup.
    self.addJobComments = function () {
        var msg = '';
        if ($('#txtJobComments').val() == "" || jQuery.trim($('#txtJobComments').val()) == "") {
            if (msg != undefined && msg != null && msg != "")
                msg += "<br />Please enter the comment";
            else
                msg = "Please enter the comment";
        }
        if (appPrivileges.customerNumber == "1" && appPrivileges.roleName == "admin") {
            if (!($('#chkIS')[0].checked || $('#chkJobIns')[0].checked || $('#chkProductionReport')[0].checked)) {
                if (msg != undefined && msg != null && msg != "")
                    msg += "<br />Please select at least one flag";
                else
                    msg = "Please select at least one flag";
            }
        }
        $('#popupComments').popup('open');
        $('#popupComments').popup('close');
        if (msg != "") {
            $('#alertmsg').html(msg);
            $("#popupDialog").popup('open');
            $("#okBut").attr("onclick", "$('#popupDialog').popup('close');$('#popupComments').popup('open');");
            return false;
        }
        $('#txtJobComments').attr('style', 'height:50px');

        var comments = $.grep(self.gData, function (obj) {
            return obj.list == "commentsDict";
        });

        var flag_comments = {};
        flag_comments["wantsIS"] = ($('#chkIS').attr('checked') == 'checked') ? 1 : 0;
        flag_comments["wantsJobInstructions"] = ($('#chkJobIns').attr('checked') == 'checked') ? 1 : 0;
        flag_comments["wantsProductionReport"] = ($('#chkProductionReport').attr('checked') == 'checked') ? 1 : 0;
        var new_comment = {
            "userName": sessionStorage.username,
            "comment": $('#txtJobComments').val(),
            "flagComments": flag_comments
        }
        comments[0].items.push(new_comment);

        if (Object.keys(new_comment).length > 0) {
            var post_comments_url = serviceURLDomain + 'api/Comments_post/' + sessionStorage.facilityId + '/' + jobCustomerNumber + '/' + sessionStorage.jobNumber;
            postCORS(post_comments_url, JSON.stringify(new_comment), function (response) {
                if (response != "") {
                    msg = 'Comments data was saved.';
                    self.getFilteredComments();
                    window.setTimeout(function getCommentsDelay() {
                        self.buildLists();
                    }, 1000);
                }
                else
                    msg = "Your comments data was not saved.";


                $('#txtJobComments').val('');
                $('#chkIS').attr('checked', false).checkboxradio('refresh');
                $('#chkJobIns').attr('checked', false).checkboxradio('refresh');
                $('#chkProductionReport').attr('checked', false).checkboxradio('refresh');
                $('#alertmsg').text(msg);
                $("#okBut").attr("onclick", "$('#popupDialog').popup('close');");
                $('#popupDialog').popup('open');
            }, function (response_error) {
                ////var msg = 'Error in saving.';
                //var msg = response_error.responseText;
                //msg = "Error: Contact Administrator."
                //if (msg != "") {
                //    $('#alertmsg').text(msg);
                //    $('#popupDialog').popup('open');
                //}
                showErrorResponseText(error_response, false);

            });


        }
    };

}