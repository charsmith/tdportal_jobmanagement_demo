﻿var dailyTaskListInfo = function (self) {
    var self1 = self;
    new taggingInfo(self1);
    new taskAttachments(self1);

    self1.selectedProcess = ko.observable({});
    self1.tagJobInfo = function () {
        var jobNumber = '';
        var jobDesc = '';
        jobNumber = getSessionData("jobNumber");
        jobDesc = getSessionData("jobDesc");
        var neg_job_number = getSessionData("negJobNumber");
        var header_text = "";
        if (jobNumber != undefined && jobDesc != undefined) {
            header_text = jobDesc + ' | ' + jobNumber;
            if (neg_job_number != undefined && neg_job_number != null && neg_job_number != "")
                header_text += ' (' + neg_job_number + ')';
        }
        $('#tagJobName').html(header_text);
    };

    self1.tagJobInfo();
    var daily_taskList = [];
    var DailyTaskModel = function (process_name, process_number, process_attrs, process_theme, process_comments, process_attachments, process_assignments, facility_id, process_status, icon_alt_text, is_editable) {
        var self = this;
        self.processName = process_name;
        self.processNumber = process_number;
        self.processAttrs = process_attrs;
        self.processTheme = process_theme;
        self.processComments = process_comments;
        self.processAttachments = ko.observableArray(process_attachments);
        //self.processAttachments = process_attachments;
        self.processAssignments = process_assignments;
        self.facilityId = facility_id;
        self.processStatusIcon = 'DailyTask' + process_status;
        self.isEditable = (is_editable == 0) ? false : true;
    };

    var DailyTaskListModel = function (title, process_list, incomplete_cnt, facility_id) {
        var self = this;
        self.groupTitle = title;
        self.incompleteCount = ko.observable((incomplete_cnt) ? incomplete_cnt : 0);
        self.processListData = ko.observableArray([]);
        self.processListData(process_list);
    };

    self1.makeTaskList = function () {
        var daily_task_list = [];
        var incomplete_jobs_count = 0;
        if (appPrivileges.customerNumber == "1" || appPrivileges.customerNumber == REDPLUM) {
            var temp_process_list = [];
            $.each(dailyTaskData, function (key, val) {
                var temp_task_list = "";
                temp_process_list = [];
                var temp_comments = [];
                var temp_attachments = [];
                var temp_assignments = [];
                $.each(val, function (key1, val1) {
                    temp_task_list = [];
                    temp_comments = [];
                    $.each(val1.tasks, function (key2, val2) {
                        job_name = (val2["label"] != undefined && val2["label"] != null && val2["label"] != "") ? val2["label"] : '';
                        var index = 0;
                        var previous_split_index = 0;
                        var temp_attr = "";
                        var filter_text = "";
                        attr_list = "";
                        temp_comments = [];
                        $.each(val2, function (job_attr_key, job_attr_val) {
                            if (job_attr_key == "comments") {
                                $.each(job_attr_val, function (job_cmt_key, job_cmt_val) {
                                    temp_comments.push({
                                        "commentLink": job_cmt_val.userName + ' - ' + job_cmt_val.commentDateTime,
                                        "commentText": job_cmt_val.comment,
                                        "commentAsLink": ((appPrivileges.roleName == "admin" || sessionStorage.username == job_cmt_val.userName) ? true : false)
                                    });
                                    //temp_comments += '<p>' + ((appPrivileges.roleName == "admin" || sessionStorage.username == job_cmt_val.userName) ? '<a href="#" onclick="ko.contextFor(this).$root.editCommentClick(ko.contextFor(this).$data,\'' + job_cmt_val.comment + '\',event);">' + job_cmt_val.userName + ' - ' + job_cmt_val.commentDateTime + '</a>' : '<span href="#">' + job_cmt_val.userName + ' - ' + job_cmt_val.commentDateTime + '</span>');
                                    //temp_comments += '<br />' + job_cmt_val.comment + '</p>';
                                });
                            }
                            if (job_attr_key == "attachments") temp_attachments = job_attr_val;
                            //if (job_attr_key == "attachments") {
                            //    $.each(job_attr_val, function (job_cmt_key, job_cmt_val) {
                            //        temp_attachments.push(job_cmt_val);
                            //    });
                            //}
                            if (job_attr_key == "assignments") {
                                $.each(job_attr_val, function (job_cmt_key, job_cmt_val) {
                                    temp_assignments.push(
                                        {
                                            "assigneeName": job_cmt_val.assigneeName,
                                            "assigneeAsLink": ((appPrivileges.roleName == "admin") ? true : false)
                                        });
                                });
                            }
                            if (job_attr_key == "label" || job_attr_key == "name" || job_attr_key == "comments" || job_attr_key == "attachments") temp_attr = ""; else temp_attr = job_attr_key;
                            if (job_attr_val != "" || job_attr_val == 0) {
                                if (temp_attr != "") {
                                    if ((index - previous_split_index) == 3) {
                                        attr_list += "<br/>";
                                    }
                                    attr_list += (attr_list != "") ? ((((index - previous_split_index) < 3) ? " | " : "") + self.getPageDisplayName(job_attr_key) + ": " + ((job_attr_val != null) ? job_attr_val : "")) : "<p class=\"wrapword\">" + (self.getPageDisplayName(job_attr_key) + ": " + ((job_attr_val != null) ? job_attr_val : ""));
                                    if ((index - previous_split_index) == 3) previous_split_index = index;
                                    index++;
                                }
                            }
                        });
                        //temp_attachments.push({
                        //    "attachmentName": "Process1_Report.pdf",
                        //    "attachmentErrorName": "Process1Error_Report.pdf"
                        //});
                        //temp_attachments = "<p><b>Attachments: </b><a href='#' onclick=\"ko.contextFor(this).$root.attachmentClick(ko.contextFor(this).$data,event);\">Process1_Report.pdf</a>&nbsp;&nbsp;<a href='#' onclick=\"ko.contextFor(this).$root.attachmentClick(ko.contextFor(this).$data,event);\">Process1Error_Report.pdf</a></p>";
                        //if (attr_list != "") attr_list += '</p>' + temp_comments + temp_attachments;
                        if (attr_list != "") attr_list += '</p>';
                        if (temp_assignments.length == 0) {
                            temp_assignments.push(
                                        {
                                            "assigneeName": "Unassigned",
                                            "assigneeAsLink": ((appPrivileges.roleName == "admin") ? true : false)
                                        });
                        }
                        temp_task_list.push(new DailyTaskModel(job_name, "", attr_list, 'c', temp_comments, temp_attachments, temp_assignments, '', val2.Status, '', ((appPrivileges.roleName == "admin") ? 1 : 0)));
                        temp_attachments = [];
                        temp_assignments = [];
                    });
                    incomplete_jobs_count += (val1.incomplete) ? val1.incomplete : 0;
                    temp_process_list.push(new DailyTaskListModel(key1, temp_task_list, val1.incomplete, ""));
                });
                daily_task_list.push(
                    {
                        "taskGroup": key,
                        "groupJobs": temp_process_list,
                        "incompleteCount": incomplete_jobs_count
                    });
                incomplete_jobs_count = 0;
            });
            daily_taskList = daily_task_list;
        }
        self1.totalIncompleteTasks = ko.observable();
        var tot_cnt = 0;
        $.each(daily_taskList, function (key, val) {
            tot_cnt += val.incompleteCount;
        });
        self1.totalIncompleteTasks(tot_cnt);
        self1.dailyTaskList = daily_taskList;
        self.gData.push({
            "list": "dailyTaskList",
            "items": self1
        });
    };
    if (appPrivileges.customerNumber == "1"/* || appPrivileges.customerNumber == REDPLUM*/) {
        var g_service_task_list_url = serviceURLDomain + "api/Task_sharedByType/midweek";
        getCORS(g_service_task_list_url, null, function (response_data) {
            var a = response_data;
            $.each(response_data, function (key, val) {
                if (dailyTaskData[key] != undefined) {
                    self.jobNumber = val.jobNumber;
                    dailyTaskData[key] = val.tasks;
                }
            });
            self1.makeTaskList();
        }, function (error_response) {
            if (Object.keys(dailyTaskData).length > 0)
                self1.makeTaskList();
            showErrorResponseText(error_response, false);
        });
    }
    self1.taskStatusClick = function (data, event) {
        $('#txtComments').val('');
        //self1.selectedProcess = ko.observable({});
        self1.selectedProcess(data);
        $('#popupSetTaskStatus').popup('open');
        $("#dvUploadedFilesList div ul").each(function (i) {
            $(this).listview();
        });
        $("#dvUploadedFilesList").collapsibleset('refresh');
    };
    self1.editCommentClick = function (data, e) {
        e.preventDefault();
        e.stopPropagation();
        $('#txtEditComments').val(data.commentText);
        $('#popupEditComment').popup('open');
    }
    self1.attachmentClick = function (data, e) {
        e.preventDefault();
        e.stopPropagation();
    }
    self1.removeComment = function (data, e) {
        e.preventDefault();
        e.stopPropagation();
        $('#popupEditComment').popup('close');
    }
    //self.dailyTaskList = daily_taskList;
}
