﻿isCWSummaryPreferences = function () {
    var customer = getCustomerNumber();    
    return ([DKI, ICYNENE, SUNTIMES, REGIS, SCOTTS, ACE, "203", BRIGHTHOUSE, BBB, AAG, TRIBUNEPUBLISHING, SANDIEGO, AH, SPORTSAUTHORITY, WALGREENS, MOTIV8, DATAWORKS, ALLIED, REDPLUM, SAFEWAY, LOWES, DCA, KUBOTA, OH, SPORTSKING, GWA, REDPLUM, BUFFALONEWS, PIONEERPRESS, EQUINOX, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);
}

showArtworkDiv = function () {
    var customer = getCustomerNumber();
    var role = getUserRole();
    var show_artwork_div = false;
    switch (role) {
        case 'admin':
            show_artwork_div = ([DCA, KUBOTA, OH, GWA, EQUINOX, SPORTSKING ].indexOf(customer) > -1 );
            break;
        case 'power':
            show_artwork_div = ([DCA, KUBOTA, OH, GWA, EQUINOX].indexOf(customer) > -1);
            break;
        case 'user':
            show_artwork_div = ([DCA, OH, GWA, EQUINOX].indexOf(customer) > -1);
            break;
        default: break;
    }
    return show_artwork_div;
}

hideArtworkDiv = function () {
    var customer = getCustomerNumber();
    var role = getUserRole();
    var hide_artwork_div = false;
    switch (role) {        
        case 'user':
            hide_artwork_div = ([KUBOTA].indexOf(customer) > -1);
            break;
        default: break;
    }
    return hide_artwork_div;
}

showCopyJobButton = function () {
    var customer = getCustomerNumber();
    var role = getUserRole();
    var show_copy_job = false;
    switch (role) {
        case 'admin':
            show_copy_job = ([].indexOf(customer) > -1);
            break;
        default: break;
    }
    return show_copy_job;
};

isCreateUploadListDataDictionary = function () {
    var customer = getCustomerNumber();
    return ([DKI, ICYNENE].indexOf(customer) > -1);
}

isUpdateLoginUrlWithCustomerNumber = function () {
    var customer = getCustomerNumber();
    return ([DKI, ICYNENE, REGIS, SCOTTS, ACE, CW, CASEYS, JETS, "1", BRIGHTHOUSE, REDPLUM, EQUINOX, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);
}

showListSelection = function () {
    var customer = getCustomerNumber();
    return ([REGIS, SCOTTS, ACE, CASEYS, JETS, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);
}

showUploadData = function () {
    var customer = getCustomerNumber();
    return ([DKI, ICYNENE, CW, REGIS, SCOTTS, ACE, "203", BRIGHTHOUSE, ANDERSEN , SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);
}

showUploadArtwork = function () {
    var customer = getCustomerNumber();
    return ([SCOTTS, ACE, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(jobCustomerNumber) == -1);
}

showOptInStatus = function () {
    var customer = getCustomerNumber();
    return ([CASEYS, JETS, SUNTIMES].indexOf(customer) > -1);
}

isUpdateDescription = function () {
    var customer = getCustomerNumber();
    return ([CW, REGIS, SCOTTS, ACE, BRIGHTHOUSE, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);
}

isMilestonesValidationRequired = function () {
    var customer = getCustomerNumber();    
    return ([CW, REGIS, SCOTTS, ACE, BRIGHTHOUSE, OH, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);
}

isMilestonesDateValidationRequired = function () {
    var customer = getCustomerNumber();
    return ([CW, REGIS, SCOTTS, ACE, BRIGHTHOUSE, OH, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);
}
 