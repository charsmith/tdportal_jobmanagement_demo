﻿jQuery.support.cors = true;

//******************** Data URL Variables Start **************************
var pageObj;
appPrivileges = jQuery.parseJSON(getSessionData("appPrivileges"));
var gServiceUrl = (jobCustomerNumber != SUNTIMES) ? '../JSON/_reports.JSON' : '../JSON/_stmgReports.JSON';
if (jobCustomerNumber == CW)
    gServiceUrl = '../jobSummary/JSON/jobReportAction.JSON';
var gOutputServiceUrl = '../JSON/_jobSetupOutput.JSON';
var dailyTaskServiceUrl = "../JSON/_dailyTaskList.JSON";
var dailyTaskData = {};
var pagePrefs = "";
var jobDesc = '';

var requiredApproversData = {};

var caseysRequiredApproversData = {
    "requiredApproversDict": [{
        "approverUser": "zoann.ohlson@caseys.com",
        "overallStatus": 0,
        "art": [],
        "list": [{
            "filePk": 7628,
            "listOrderFileVersionses": [{
                "fileVersionPk": 8659,
                "origFileName": "3344",
                "lastUpdateDate": "2017-06-13T13:07:08.277",
                "lastUpdatedEmail": "td.directmailing.development@senecaglobal.com",
                "listOrderFileVersionApprovals": [{
                    "pk": 4078,
                    "approvalStatus": 1,
                    "lastUpdateDate": "2017-06-13T13:07:08.293",
                    "notes": null,
                    "approvalEmail": "jim.wright@tribunedirect.com",
                    "approvalLevel": 1
                }]
            }]
        }, {
            "filePk": 7629,
            "listOrderFileVersionses": [{
                "fileVersionPk": 8660,
                "origFileName": "3857",
                "lastUpdateDate": "2017-06-13T13:07:08.277",
                "lastUpdatedEmail": "td.directmailing.development@senecaglobal.com",
                "listOrderFileVersionApprovals": [{
                    "pk": 4080,
                    "approvalStatus": 1,
                    "lastUpdateDate": "2017-06-13T13:07:08.293",
                    "notes": null,
                    "approvalEmail": "jim.wright@tribunedirect.com",
                    "approvalLevel": 1
                }]
            }]
        }, {
            "filePk": 7630,
            "listOrderFileVersionses": [{
                "fileVersionPk": 8639,
                "origFileName": "8376",
                "lastUpdateDate": "2017-06-13T13:07:08.277",
                "lastUpdatedEmail": "td.directmailing.development@senecaglobal.com",
                "listOrderFileVersionApprovals": [{
                    "pk": 4078,
                    "approvalStatus": 0,
                    "lastUpdateDate": "2017-06-13T13:07:08.293",
                    "notes": null,
                    "approvalEmail": "jim.wright@tribunedirect.com",
                    "approvalLevel": 1
                }]
            }]
        }, {
            "filePk": 7634,
            "listOrderFileVersionses": [{
                "fileVersionPk": 8639,
                "origFileName": "7081",
                "lastUpdateDate": "2017-06-13T13:07:08.277",
                "lastUpdatedEmail": "td.directmailing.development@senecaglobal.com",
                "listOrderFileVersionApprovals": [{
                    "pk": 4078,
                    "approvalStatus": 0,
                    "lastUpdateDate": "2017-06-13T13:07:08.293",
                    "notes": null,
                    "approvalEmail": "jim.wright@tribunedirect.com",
                    "approvalLevel": 1
                }]
            }]
        }]
    }, {
        "approverUser": "tina.krings@caseys.com",
        "overallStatus": 1,
        "art": [],
        "list": [{
            "filePk": 7628,
            "listOrderFileVersionses": [{
                "fileVersionPk": 8659,
                "origFileName": "3456",
                "lastUpdateDate": "2017-06-13T13:07:08.277",
                "lastUpdatedEmail": "td.directmailing.development@senecaglobal.com",
                "listOrderFileVersionApprovals": [{
                    "pk": 4078,
                    "approvalStatus": 1,
                    "lastUpdateDate": "2017-06-13T13:07:08.293",
                    "notes": null,
                    "approvalEmail": "jim.wright@tribunedirect.com",
                    "approvalLevel": 1
                }]
            }]
        }, {
            "filePk": 7629,
            "listOrderFileVersionses": [{
                "fileVersionPk": 8660,
                "origFileName": "6567",
                "lastUpdateDate": "2017-06-13T13:07:08.277",
                "lastUpdatedEmail": "td.directmailing.development@senecaglobal.com",
                "listOrderFileVersionApprovals": [{
                    "pk": 4080,
                    "approvalStatus": 1,
                    "lastUpdateDate": "2017-06-13T13:07:08.293",
                    "notes": null,
                    "approvalEmail": "jim.wright@tribunedirect.com",
                    "approvalLevel": 1
                }]
            }]
        }, {
            "filePk": 7630,
            "listOrderFileVersionses": [{
                "fileVersionPk": 8639,
                "origFileName": "7898",
                "lastUpdateDate": "2017-06-13T13:07:08.277",
                "lastUpdatedEmail": "td.directmailing.development@senecaglobal.com",
                "listOrderFileVersionApprovals": [{
                    "pk": 4078,
                    "approvalStatus": 0,
                    "lastUpdateDate": "2017-06-13T13:07:08.293",
                    "notes": null,
                    "approvalEmail": "jim.wright@tribunedirect.com",
                    "approvalLevel": 1
                }]
            }]
        }]
    },
		{
		    "approverUser": "sue.gullion@caseys.com",
		    "overallStatus": 2,
		    "art": [],
		    "list": [{
		        "filePk": 7628,
		        "listOrderFileVersionses": [{
		            "fileVersionPk": 8659,
		            "origFileName": "8923",
		            "lastUpdateDate": "2017-06-13T13:07:08.277",
		            "lastUpdatedEmail": "td.directmailing.development@senecaglobal.com",
		            "listOrderFileVersionApprovals": [{
		                "pk": 4078,
		                "approvalStatus": 1,
		                "lastUpdateDate": "2017-06-13T13:07:08.293",
		                "notes": null,
		                "approvalEmail": "jim.wright@tribunedirect.com",
		                "approvalLevel": 1
		            }]
		        }]
		    }, {
		        "filePk": 7629,
		        "listOrderFileVersionses": [{
		            "fileVersionPk": 8660,
		            "origFileName": "2321",
		            "lastUpdateDate": "2017-06-13T13:07:08.277",
		            "lastUpdatedEmail": "td.directmailing.development@senecaglobal.com",
		            "listOrderFileVersionApprovals": [{
		                "pk": 4080,
		                "approvalStatus": 1,
		                "lastUpdateDate": "2017-06-13T13:07:08.293",
		                "notes": null,
		                "approvalEmail": "jim.wright@tribunedirect.com",
		                "approvalLevel": 1
		            }]
		        }]
		    }, {
		        "filePk": 7630,
		        "listOrderFileVersionses": [{
		            "fileVersionPk": 8639,
		            "origFileName": "1980",
		            "lastUpdateDate": "2017-06-13T13:07:08.277",
		            "lastUpdatedEmail": "td.directmailing.development@senecaglobal.com",
		            "listOrderFileVersionApprovals": [{
		                "pk": 4078,
		                "approvalStatus": 0,
		                "lastUpdateDate": "2017-06-13T13:07:08.293",
		                "notes": null,
		                "approvalEmail": "jim.wright@tribunedirect.com",
		                "approvalLevel": 1
		            }]
		        }]
		    }, {
		        "filePk": 7634,
		        "listOrderFileVersionses": [{
		            "fileVersionPk": 8639,
		            "origFileName": "8901",
		            "lastUpdateDate": "2017-06-13T13:07:08.277",
		            "lastUpdatedEmail": "td.directmailing.development@senecaglobal.com",
		            "listOrderFileVersionApprovals": [{
		                "pk": 4078,
		                "approvalStatus": 0,
		                "lastUpdateDate": "2017-06-13T13:07:08.293",
		                "notes": null,
		                "approvalEmail": "jim.wright@tribunedirect.com",
		                "approvalLevel": 1
		            }]
		        }]
		    }]
		}
    ]
};


var shouldLockJob = false;
jobDesc = getSessionData("jobDesc");
//******************** Data URL Variables End **************************

//******************** Page Load Events Start **************************
$(window).on('popupbeforeposition', 'div:jqmData(role="popup")', function () {
    var notDismissible = $(this).jqmData('dismissible') === false;
    if (notDismissible) {
        $('.ui-popup-screen').off();
    }
});

$(document).on('pageshow', '#_jobSummary', function (event) {
    //makeGData();
    //makeGOutputData();  
    var $dropArea = $("#drop-area");
    $dropArea.on({
        "drop": pageObj.makeDrop,
        "dragenter": pageObj.ignoreDrag,
        "dragover": pageObj.ignoreDrag
    });
    (($.browser.msie) ? $(window.frames[0].frames.frameElement).contents().find('input[type=file]') : $('input[type=file]')).bind('change', function (e) {
        pageObj.makeDrop(e, true, e.target)
    });

    if (sessionStorage.singleJobPrefs != undefined && sessionStorage.singleJobPrefs != null && sessionStorage.singleJobPrefs != "null" && sessionStorage.singleJobPrefs != "")
        pageObj.loadData();
    $('#dvcontrolGroupName').find('#pCustomerNumber').remove();

    if (appPrivileges.roleName == "admin") {
        var customer_number = '<p style="padding-top:24px;" id="pCustomerNumber">Customer Number : ' + jobCustomerNumber + '</p>';
        $('#dvcontrolGroupName').append(customer_number);
    }

    $("#_jobSummary").trigger("create");
    $('#thermometer-f').attr('data-percent', getPendingApprovalPercent(sessionStorage.jobDesc));
    if (appPrivileges.roleName != "admin") {
        //if (jobCustomerNumber != CW) {
        //    $("#divReports").css("display", "none");
        //}        
        $("#dvDataSpace").css("display", "none");
    }
    if ((jobCustomerNumber == DCA || jobCustomerNumber == KUBOTA || jobCustomerNumber == OH || jobCustomerNumber == SPORTSKING || jobCustomerNumber == GWA) && appPrivileges.roleName != "user") {
        //$("#divReports").css("display", "block");
        $("#dvDataSpace").css("display", "block");
    }
    if (jobCustomerNumber == SPORTSKING && appPrivileges.roleName != "admin") {
        $("#dvData").hide();
    }

    //if ([CASEYS, CW, ACE].indexOf(jobCustomerNumber) > -1) {
    //    $('#divRequiredApprovals').css('display', 'block');
    //}

    if (appPrivileges.customerNumber != "1" && appPrivileges.customerNumber != REDPLUM) {
        $('#dvFlagComments').css('display', 'none');
        $('#divComments').css('display', 'none');
    }
    $('#filterChkViewAllComments').attr('checked', true).checkboxradio('refresh');
    //$("#filterChkViewAllComments").attr("checked", "checked");
    //if (appPrivileges.customerNumber == REGIS || appPrivileges.customerNumber == CASEYS || appPrivileges.customerNumber == "99998" || appPrivileges.customerNumber == "99997") {
    if (showListSelection()) {
        $('#aUploadData').text('List Selection');
        $('#aUploadData').attr('onclick', 'pageObj.fnEditJob("listSelection");');
    }
    window.setTimeout(function () {
        setLogo(jobCustomerNumber);
    }, 500);
    if (jobCustomerNumber == ALLIED) {
        loadDemoJobTicket();
    }
    $('.thermometer-noconfig').thermometer({
        speed: 'slow'
    });    
    $('#ulOrderStatus p span:lt(' + getPendingApprovalStatus(getPendingApprovalPercent(sessionStorage.jobDesc)) + ')').css('color', 'green');
    $('div[data-role=collapsible-set] h3 a').removeAttr('href');
    var user_role = (sessionStorage.userRole != undefined && sessionStorage.userRole != null && sessionStorage.userRole != "") ? sessionStorage.userRole : appPrivileges.roleName;
    if ((shouldLockJob != true) && user_role != "admin" && _shouldLockThisJob()) {
        shouldLockJob = true;
    }    
});

$('#_jobSummary').live('pagebeforecreate', function (event) {

    if (appPrivileges.customerNumber == "1"/* || appPrivileges.customerNumber == REDPLUM*/) {
        getDailyTaskData();
    }

    //load IFRAME for IE to upload files.
    loadUploadIFrame();
    pageObj = new jobSummary();

    //getData();
    if (sessionStorage.singleJobPrefs == undefined || sessionStorage.singleJobPrefs == null || sessionStorage.singleJobPrefs == "")
        if (isCWSummaryPreferences()) {
            getPagePreferences('singleJob.htm', sessionStorage.facilityId, ((jobCustomerNumber == REDPLUM )? "1" :CW));
        }
        else {
            getPagePreferences('singleJob.htm', sessionStorage.facilityId, ((jobCustomerNumber == JETS) ? CASEYS : appPrivileges.customerNumber));
        }
    else {
        managePagePrefs(jQuery.parseJSON(sessionStorage.singleJobPrefs));
    }

    if (appPrivileges.roleName != "admin" || jobCustomerNumber == CASEYS) {
        $('#aJobRequirements').css('display', 'none');
        $('#aJobAdministration').css('display', 'none');
    }

    //if (appPrivileges.customerNumber == "1" && appPrivileges.roleName == "admin" && (sessionStorage.isDemoSelected == undefined || sessionStorage.isDemoSelected == null || sessionStorage.isDemoSelected == "" || sessionStorage.isDemoSelected == 0)) {
    if ((jobCustomerNumber == "1" || jobCustomerNumber == REDPLUM) && appPrivileges.roleName == "admin" && (sessionStorage.isDemoSelected == undefined || sessionStorage.isDemoSelected == null || sessionStorage.isDemoSelected == "" || sessionStorage.isDemoSelected == 0)) {
        $('#aAddJobs').css('display', 'none');
        //$('#aEditJobs').addClass('ui-corner-bl ui-corner-br');
        $('#aEditJobs').addClass('ui-corner-all');
    }
    else {
        $('#aAddJobs').css('display', 'none');
        $('#aEditJobs').addClass('ui-corner-tl ui-corner-tr');
        $('#aUploadArtwork').addClass('ui-corner-bl ui-corner-br');

        if (jobCustomerNumber == CASEYS || jobCustomerNumber == JETS) {
            $('#aUploadData').addClass('ui-corner-bl ui-corner-br');
        }
        if ((appPrivileges.customerNumber == "1" || appPrivileges.customerNumber == REDPLUM) && (appPrivileges.roleName == "user" || appPrivileges.roleName == "power")) {
            $('#aEditJobs').css('display', 'none');
            //$('#btnSignOut').addClass('ui-corner-all');
        }
    }
    if (sessionStorage.isGizmoJob != undefined && sessionStorage.isGizmoJob != null && sessionStorage.isGizmoJob != "" && !JSON.parse(sessionStorage.isGizmoJob)) {
        $('#aEditJobs').css('display', 'none');
        $('#aAddJobs').addClass('ui-corner-bl ui-corner-br');
    }

    $('#aTribTrack').css('display', 'none');
    $('#aShippingOptimization').css('display', 'none');
    //}

    displayMessage('_jobSummary');
    var user_name;

    $("#popPDF").on({
        popupbeforeposition: function () {
            var maxHeight = $(window).height() - 80 + "px";
            $("#popPDF img").css("max-height", maxHeight);
            $("#popPDF embed").css("max-height", maxHeight);
            var maxWidth = $(window).width() - 80 + "px";
            $("#popPDF img").css("max-width", maxWidth);
            $("#popPDF embed").css("max-width", maxWidth);
        }
    });
    user_name = sessionStorage.username;

    var neg_job_number = getSessionData("negJobNumber");
    var header_text = jobDesc + ' | ' + pageObj.jobNumber;
    if (neg_job_number != undefined && neg_job_number != null && neg_job_number != "")
        header_text += ' (' + neg_job_number + ')';
    $('#navHeader').html('<h3>' + header_text + '</h3>');

    $('#aEditJobs').attr('onclick', 'pageObj.fnEditJob("edit");');

    $('#aJobRequirements').attr('onclick', 'pageObj.fnEditJob("jobRequirements");');

    $('#aJobAdministration').attr('onclick', 'pageObj.fnEditJob("jobAdministration");');

    if (showCopyJobButton()) {
        $('#aCopyJob').css('display', 'block');
        $('#aCopyJob').attr('onclick', 'pageObj.fnEditJob("copy");');

    } else {
        $('#aCopyJob').css('display', 'none');
    }

    if (showUploadData()) {
        $('#aUploadData').show();        
        $('#aUploadData').attr('onclick', 'pageObj.fnEditJob("uploadData");');
        $('#aUploadArtwork').attr('onclick', 'pageObj.fnEditJob("uploadArt");');
        if (showUploadArtwork()) {
            $('#aUploadArtwork').show();
            $('#dvArtwork').hide();
        }
        else {
            $('#aUploadArtwork').hide();
            $('#dvArtwork').hide();
        }
    }
    else if (showOptInStatus()) {
        $('#aUploadData').show();
        $('#aUploadArtwork').hide();
        $('#aUploadArtwork').text('Opt-In Status');
        $('#aUploadArtwork').attr('onclick', 'pageObj.fnEditJob("optInStatus");');
        $('#dvArtwork').hide();
        $('#dvData').hide();
    }
    else {
        if (jobCustomerNumber == SPORTSKING && appPrivileges.roleName == "admin") {
            $('#aEditJobs').removeClass('ui-corner-bl ui-corner-br');
        }
        else {
            $('#aEditJobs').addClass('ui-corner-bl ui-corner-br');
        }
        $('#aUploadData').hide();
        $('#aUploadArtwork').hide();
        if (showArtworkDiv()) {
            $('#dvArtwork').hide();
            if (hideArtworkDiv())
                $('#dvArtwork').hide();
            $('#dvData').hide();
        }
        else
            $('#dvArtwork').hide();
    }
    if (jobCustomerNumber == "203")
        $('#aUploadData').hide();


    $('#aAddJobs').attr('href', '../jobSelectTemplate/jobSelectTemplate.html');

    //test for mobility...
    loadMobility();
    loadingImg("_jobSummary");
    if (appPrivileges.customerNumber == KUBOTA)
        setCustomGizmoLogo(appPrivileges.customerNumber);
    else
        setGizmoLogo();
});

function getPendingApprovalPercent(job_name) {
    var count = 0;
    for (var x = 0, c = ''; c = job_name.charAt(x) ; x++) {
        count += c.charCodeAt(0);
    }
    count = count % 100;
    count = count < 15 ? 100 : count;
    return count;
};

function getPendingApprovalStatus(pending_approval_percent) {
    if (pending_approval_percent <= 15) return 1;
    else if (pending_approval_percent <= 30) return 2;
    else if (pending_approval_percent <= 45) return 3;
    else if (pending_approval_percent <= 60) return 4;
    else if (pending_approval_percent <= 75) return 5;
    else if (pending_approval_percent <= 90) return 6;
    else if (pending_approval_percent <= 100) return 7;

    return '';
}

function getDailyTaskData() {
    $.getJSON(dailyTaskServiceUrl, function (data) {
        if (data[jobCustomerNumber] != undefined && data[jobCustomerNumber] != null)
            dailyTaskData = data[jobCustomerNumber];
    });
}

String.prototype.initCap = function () {
    return this.toLowerCase().replace(/(?:^|\s)[a-z]/g, function (m) {
        return m.toUpperCase();
    });
};
//******************** Page Load Events End **************************

