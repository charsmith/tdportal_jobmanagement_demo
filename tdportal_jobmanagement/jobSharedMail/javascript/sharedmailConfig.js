﻿showUploadSelectedListDemoHints = function () {
    var customer = getCustomerNumber();
    return ([BBB, AAG, TRIBUNEPUBLISHING, SANDIEGO, AH, SPORTSAUTHORITY, WALGREENS, SAFEWAY, LOWES, CW, OH, SCOTTS, ACE, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);
}

showLocationAssignment = function () {
    var customer = getCustomerNumber();    
    return ([CW, SCOTTS, ACE, SPORTSKING, KUBOTA, GWA, BUFFALONEWS, PIONEERPRESS, REDPLUM, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);
}