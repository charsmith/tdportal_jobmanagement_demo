﻿var manageUploadLists = function () {
    var self = this;
    new uploadFilesCommon(self);
    new dragAndDropFiles(self);
    //******************** Global Variables Start **************************
    self.filesToUpload = [];
    self.pagePrefs = "";
    self.selectedStates = [];
    self.uploadedSourceFilesCount = 0;
    self.uploadedSeedFilesCount = 0;
    self.uploadedDnmFilesCount = 0;

    //******************** Global Variables End **************************
    //******************** Public Functions Start **************************
    //Get the list of files uploaded from Job Ticket JSON session and displays over the page.
    //If the file list is not available then it creates an empty list.
    self.displayListFileInfo = function () {
        if (sessionStorage.jobSetupOutput == undefined && sessionStorage.jobSetupOutput == undefined) {
            self.makeGOutputData();
        }
        else {
            if (sessionStorage.jobSetupOutput != undefined) {
                self.gOutputData = jQuery.parseJSON(sessionStorage.jobSetupOutput);
                self.getFileListInfoFromSession();
            }
        }
    }

    //Gets the uploaded file list from Job Ticket JSON session.
    self.getFileListInfoFromSession = function () {
        var file_data;
        file_data = (self.gOutputData != undefined && self.gOutputData.standardizeAction.standardizeFileList != undefined) ? self.gOutputData.standardizeAction.standardizeFileList : [];

        if (file_data.length == 0) {
            file_data.push({
                sourceType: "",
                fileName: "",
                fileMap: "",
                priority: "",
                fileSizePretty: "",
                fileStatus: ""
            });

            if (sessionStorage.jobSetupOutputCompare != undefined) {
                var goutput_compare_data = jQuery.parseJSON(sessionStorage.jobSetupOutputCompare);
                if (goutput_compare_data.standardizeAction.standardizeFileList != undefined && JSON.stringify(goutput_compare_data.standardizeAction.standardizeFileList).length == 2) {
                    goutput_compare_data.standardizeAction["standardizeFileList"] = file_data;
                    sessionStorage.jobSetupOutputCompare = JSON.stringify(goutput_compare_data);
                }
            }
            if (self.gOutputData["standardizeAction"] == undefined) {
                self.gOutputData["standardizeAction"] = {};
                self.gOutputData.standardizeAction["name"] = "standardize1";
                self.gOutputData.standardizeAction["type"] = "standardize";
                self.gOutputData.standardizeAction["isEnabled"] = true;
                self.gOutputData.standardizeAction["standardizeFileList"] = [];
            }
            self.gOutputData.standardizeAction.standardizeFileList = file_data;

        }
        
        self.buildOutputLists(false);
        if (sessionStorage.filesToUpload != undefined) {
            self.filesToUpload = jQuery.parseJSON(sessionStorage.filesToUpload);
            self.makeFileListBeforePost(self.filesToUpload);
        }
        getBubbleCounts(self.gOutputData);
    }
    //Generates the info if the Job Ticket JSON for upload is not available.
    self.makeGOutputData = function () {
        self.getFileListInfoFromSession();
    }
    //Creates the uploaded files list and updates the UI.
    self.makeFileList = function (array_name) {
        var array = eval(array_name);
        var list_info = "";
        var list_info_dbf = "";
        var mail_files = "";
        var dbf_files = "";
        var source_type = "";
        $("#uploadList").empty();

        $("#btnMapSelected").hide();
        $("#btnRemoveSelected").hide();
        var replaceable_extensions = ['gz', 'zip'];
        //if ($('#ddlSelectLocations input[type=checkbox]').length <= 1)
        //    self.loadOrderedLocations();
        $.each(array_name, function (key, val) {
            if (val.fileName != "") {
                source_type = getFileType(val.sourceType.toString());
                var file_ext = val.fileName.substring(val.fileName.lastIndexOf('.') + 1, val.fileName.length);
                if (replaceable_extensions.indexOf(file_ext.toLowerCase()) > -1) file_ext = file_ext.toLowerCase().replace('gz', 'txt').replace('zip', 'txt');
                var file_name = val.fileName.substring(0, val.fileName.lastIndexOf('.') + 1) + file_ext;
                var file_list = '<li data-theme="c"><a><h3>' + file_name + '</h3><p>';
                //var map_label = (appPrivileges.roleName == "admin") ? "File Map: " + val.fileMap + "  | " : '';
                //file_list += map_label + 'File Size: ' + val.fileSizePretty + ' | File Status:' + val.fileStatus;
                var temp_loc = "";
                file_list += '</p></a><a href="#" data-icon="delete" onclick="return pageObj.canDeleteListFile(\'' + file_name + '\',\'' + source_type + '\',' + getFileTypeCode(source_type) + ');">Delete List</a></li>';
                //if (val.linkedTo.toLowerCase() == "tmc") {
                mail_files += file_list;
                //}
                //if (val.linkedTo.toLowerCase() == "dbf") {
                //    dbf_files += file_list;
                //}
                $('#tblFiles').show();
            }
            else {
                $("#lnkUploadFile").show();
            }
        });
        self.uploadedSourceFilesCount = 0;
        //if (mail_files != "") {
        //    mail_grid_info = '<div id="dvTMCFilesList" data-role="collapsible" data-theme="f" data-collapsed-icon="carat-r" data-expanded-icon="carat-d" data-collapsed="false"><h2>Uploaded TMC Files</h2>';
        //    mail_grid_info += '<ul id="ulTMCFiles" data-role="listview" data-theme="c" data-divider-theme="c" data-split-icon="delete" data-split-theme="e" >';
        //    list_info = mail_grid_info + mail_files;
        //    list_info += '</ul></div>';
        //}
        //if (dbf_files != "") {
        //    dbf_grid_info = '<div id="dvDBFFilesList" data-role="collapsible" data-theme="f" data-collapsed-icon="carat-r" data-expanded-icon="carat-d" data-collapsed="true"><h2>Uploaded DBF Files</h2>';
        //    dbf_grid_info += '<ul id="ulDBFFiles" data-role="listview" data-theme="c" data-divider-theme="c" data-split-icon="delete" data-split-theme="e" >';
        //    list_info_dbf = dbf_grid_info + dbf_files;
        //    list_info_dbf += '</ul></div>';
        //}
        if (mail_files != "") {
            mail_files = '<li data-role="list-divider">Uploaded Files</li>' + mail_files;
        }
        // $("#uploadedListTMC").empty();
        //$("#uploadedListDBF").empty();

        $('#ulUploadedFiles').empty();
        $('#ulUploadedFiles').append(mail_files);
        $('#ulUploadedFiles').css('display', 'block');
        $('#ulStandardization').css('display', 'block');

        //if (list_info != "") {

        //$("#uploadedListTMC").append(list_info);

        if (mail_files != "") {
            $('#dvStandardization').removeClass('ui-disabled');
            $('#dvStandardization')[0].disabled = false;

            $('#dvProcessing').removeClass('ui-disabled');
            $('#dvProcessing')[0].disabled = false;

            $('#dvOutput').removeClass('ui-disabled');
            $('#dvOutput')[0].disabled = false;
        } else {
            $('#dvStandardization').addClass('ui-disabled');
            $('#dvStandardization')[0].disabled = true;

            $('#dvProcessing').addClass('ui-disabled');
            $('#dvProcessing')[0].disabled = false;

            $('#dvOutput').addClass('ui-disabled');
            $('#dvOutput')[0].disabled = false;
        }

        //if (list_info_dbf != "") {
        //    $('#dvProcess').removeClass('ui-disabled');
        //    $('#dvProcess')[0].disabled = false;
        //} else {
        //    $('#dvProcess').addClass('ui-disabled');
        //    $('#dvProcess')[0].disabled = true;
        //}

        $('#ulUploadedFiles').listview('refresh');

        $("div[data-role=collapsible-set][id*=uploadedList] ul").each(function (i) {
            $(this).listview();
        });
        $('div[data-role=collapsible-set][id*=uploadedList]').collapsibleset('refresh');
        window.setTimeout(function () {
            $('#hidFileType').val('');
            $('#hidSourceType').val('');
        }, 1000);
        //}
    };

    self.loadDropDowns = function () {
        var options_list = "";
        $.each(tempFileList, function (key, val) {
            if (key == 0)
                options_list = '<option value="" selected>Select</option>';
            options_list += '<option value="' + val.fileName.replace(/ /g, '') + '">' + val.fileName + '</option>';
        });
        $('select').append(options_list);
        $('select:not([id=fieldType]):not([id=sldrShowHints])').selectmenu().selectmenu('refresh');
        //$('#ddlCDS').append(options_list);
        //$('#ddlSubscribers').append(options_list);
        //$('#ddlPCD').append(options_list);
        //$('#ddlSundaySelect').append(options_list);
        //$('#ddlElSentinel').append(options_list);
        //$('#ddlForumPublishing').append(options_list);
        //$('#ddlDoNotMail').append(options_list);
        //$('#ddlSeed').append(options_list);

        //$('#ddlCDS').selectmenu('refresh');
    };

    self.fileListChanged = function (ctrl) {
        var id = ctrl.id;
        if ($(ctrl).attr('preValue')) {
            if ($.browser.msie)
                $('select:not([id = ' + id + ']):not([id=fieldType]):not([id=sldrShowHints])').children("option[value*=" + $(ctrl).attr('preValue').substring(0, $(ctrl).attr('preValue').indexOf('.')) + "]").prop('disabled', false);
            else {
                $('select:not([id = ' + id + ']):not([id=fieldType]):not([id=sldrShowHints])').children("option[value*=" + $(ctrl).attr('preValue').substring(0, $(ctrl).attr('preValue').indexOf('.')) + "]").prop('disabled', false);
                $('select:not([id = ' + id + ']):not([id=fieldType]):not([id=sldrShowHints])').children("option[value*=" + $(ctrl).attr('preValue').substring(0, $(ctrl).attr('preValue').indexOf('.')) + "]").show();
            }
        }
        if ($.browser.msie)
            $('select:not([id = ' + id + ']):not([id=fieldType]):not([id=sldrShowHints])').children("option[value*=" + ctrl.value.substring(0, ctrl.value.indexOf('.')) + "]").prop('disabled', true);
        else {
            $('select:not([id = ' + id + ']):not([id=fieldType]):not([id=sldrShowHints])').children("option[value*=" + ctrl.value.substring(0, ctrl.value.indexOf('.')) + "]").prop('disabled', true);
            $('select:not([id = ' + id + ']):not([id=fieldType]):not([id=sldrShowHints])').children("option[value*=" + ctrl.value.substring(0, ctrl.value.indexOf('.')) + "]").hide();
        }

        $(ctrl).attr('preValue', ctrl.value);
        //$('select[not(id='+id+')]').options

    }

    self.rollOverChanged = function (ctrl) {
        if ($(ctrl).is(":checked")) {
            $('#ddl' + $(ctrl).attr('data-target').replace(/ /g, '')).addClass('ui-disabled');
            $('#ddl' + $(ctrl).attr('data-target').replace(/ /g, ''))[0].disabled = true;
        }
        else {
            $('#ddl' + $(ctrl).attr('data-target').replace(/ /g, '')).removeClass('ui-disabled');
            $('#ddl' + $(ctrl).attr('data-target').replace(/ /g, ''))[0].disabled = false;
        }
    }


    self.outputChange = function (ctrl) {
        if (ctrl.value == 'deliverToEmail' && ctrl.checked) {
            $('#dvEmail').css('display', 'block');
        }
        else {
            $('#dvEmail').css('display', 'none');
        }
    }

    self.processMailClick = function () {
        $('#dvConvert').removeClass('ui-disabled');
        $('#dvConvert')[0].disabled = false;
    };

    self.browseClick = function () {
        self.resetFileUploadControl(self.filesToUpload);
        self.resetFields();
        getFile();
    };

    //Navigates to file mapper page when user (admin) clicks on the list item.
    self.navigateMapper = function (url, event) {
        $('#divError').text('');
        sessionStorage.continueToOrder = window.location.href.substring(window.location.href.lastIndexOf('/') + 1);
        window.location.href = url;
    };

    self.updateTempFileList = function () {

    };


    //Updates the list in the UI with the uploaded files list.
    self.updateFileGrid = function () {
        $.each(self.filesToUpload, function (key, value) {
            self.addFileInfo(value.sourceType, value.fileName, "", value.linkedTo, "");
        });
        //reset the fields...
        self.resetFields();
        $('#dvPopupListDefinitionContent').hide();
        $('#fieldType').get(0).selectedIndex = 1;
        $('#ddlFilesUploaded').val('select').selectmenu('refresh');
        var upload_form = ($.browser.msie) ? window.frames[0].document.getElementById('spnFileInput') : document.getElementById('spnFileInput');
        var file_ctrls = $(upload_form).find('input[type=file]');
        if (file_ctrls.length > 0) {
            var newFileInput = file_ctrls[0].cloneNode(true);
            newFileInput.value = null;
            newFileInput.style.display = "";
            newFileInput.id = "mailFiles[]";
            newFileInput.name = "mailFiles[]";
            $((($.browser.msie) ? window.frames[0].document.getElementById('spnFileInput') : document.getElementById('spnFileInput'))).find('input[type=file]').remove();
            $((($.browser.msie) ? window.frames[0].document.getElementById('spnFileInput') : document.getElementById('spnFileInput'))).append(newFileInput);
            (($.browser.msie) ? $(window.frames[0].frames.frameElement).contents().find('input[type=file]') : $('input[type=file]')).bind('change', function (e) {
                self.navigate(e, $(e.target))
            });
            self.fileCount = 0;
        }
        self.filesToUpload = [];
        $("#divFilesToUpload").hide();
        sessionStorage.removeItem("filesToUpload");
    };
    //Gets the file info when the user clicks on edit button before upload the files.
    self.getFileSettings = function (file_name, source_type, location_id) {
        if ($('#dvUploadedArtFiles select option').length == 0)
            $('#dvUploadedArtFiles').empty();
        $('#btnUpdate').attr('onclick', "pageObj.updateEditFileInfo(this,'','','','','')");
        $('#divError').text('');
        $('#dvPopupListDefinitionContent').show();
        $('#testButtonOk').hide();
        $((($.browser.msie) ? window.frames[0].document.getElementById('spnFileInput') : document.getElementById('spnFileInput')))[0].style.display = "none";
        $('#btnUpdate').show();
        if ($('#dvSelectedLocations .ui-checkbox').length == 0)
            $('#dvSelectedLocations').trigger('create');
        if ($('#dvSelectedLocations input[type=checkbox]').length > 0)
            $('#dvSelectedLocations input[type=checkbox]').attr('checked', false).checkboxradio('refresh');
        $.each(self.filesToUpload, function (key, val) {
            if (getFileType(val.sourceType).toLowerCase() == source_type.toLowerCase() && val.fileName.substring(val.fileName.lastIndexOf("\\") + 1).toLowerCase() == file_name.toLowerCase()) {
                $('#fieldName').eq(0).closest('.ui-select').find('.ui-btn-text').find('span').html('Upload a New File');
                if (sessionStorage.roleName != "user" && sessionStorage.roleName != "power")
                    $('#fieldType').val(getFileType(val.sourceType)).selectmenu('refresh').trigger('onchange');
                if (!isMultiple && !isDragDrop) {
                    $('#name').val(val.fileName.substring(val.fileName.lastIndexOf("\\") + 1));
                }
                else if (isMultiple || isDragDrop) {
                    var file_type = ($("#fieldType").val() != "select") ? $("#fieldType").val() : "";
                    var file_found = [];
                    $.each(data, function (key1, val1) {
                        if (val1.name.toLowerCase() === val.fileName.substring(val.fileName.lastIndexOf("\\") + 1).toLowerCase() && (val1.type.toLowerCase() === getFileType(val.sourceType).toLowerCase())) {
                            file_found.push(val1);
                            return false;
                        }
                    });
                    temp_data = [];
                    if (file_found.length > 0) {
                        temp_data.push(file_found[0]);
                        self.displayFileList();
                    }
                }
                $('#hidEditUploadFile').val(val.fileName.substring(val.fileName.lastIndexOf("\\") + 1) + '^' + val.sourceType);
                $('#ddlFilesUploaded').val((val.linkedTo != undefined && (val.linkedTo != "" && val.linkedTo != null) ? val.linkedTo : 'select')).selectmenu('refresh');
                selectedKey = key;
            }
        });
        if (location_id != "" && location_id.indexOf('_') > -1) {
            var selected_locations = location_id.split('_');
            $.each(selected_locations, function (key, val) {
                $('#dvSelectedLocations input[type=checkbox][value=' + val + ']').attr('checked', true);
            });
            if (selected_locations.length == $('#dvSelectedLocations input[type=checkbox]:not([value="all"])').length)
                $('#dvSelectedLocations input[type=checkbox][value="all"]').attr('checked', true);
        }
        else if (location_id != "") {
            $('#dvSelectedLocations input[type=checkbox][value=' + location_id + ']').attr('checked', true);
            $('#dvSelectedLocations input[type=checkbox]:not([value=' + location_id + '])').attr('checked', false);
        }
        window.setTimeout(function () {
            if ($('#dvSelectedLocations .ui-checkbox') == 0)
                $('#dvSelectedLocations').trigger('create');
            $('#dvSelectedLocations input[type=checkbox]').checkboxradio('refresh');
        }, 1000);
        $('#hidMode').val('edit');
        self.popUpListDefinitionDisplay();
    };


    //File open dialog opens, and validates for the allowed types when user selects the file and displays a message if invalid file is selected.
    self.navigate = function (event, ctrl) {
        if ($(ctrl)[0].files != undefined && $(ctrl)[0].files != null && $(ctrl)[0].files.length > 0) {
            isMultiple = true;
            self.resetFileUploadControl(self.filesToUpload);
            self.fileCount = (self.fileCount <= 0) ? self.fileCount = 0 : self.fileCount - 1;
            var files_count = $(ctrl)[0].files.length;
            self.makeDrop(event, true, ctrl);
        }
        else if ($(ctrl)[0].files != undefined && $(ctrl)[0].files != null && $(ctrl)[0].files.length == 0) {
            return false;
        }
        else {
            var file_name = $(ctrl).val();
            var check_file_name = ($(ctrl).val().indexOf('\\') > -1) ? $(ctrl).val().substring($(ctrl).val().lastIndexOf('\\') + 1, $(ctrl).val().length) : $(ctrl).val();
            var myRegexp = /^([a-zA-Z0-9_\.\-\^\ ])+$/ig;
            if (!myRegexp.test(check_file_name.substring(0, check_file_name.lastIndexOf('.')))) {
                self.resetFileUploadControl(self.filesToUpload);
                self.fileCount = (self.fileCount <= 0) ? self.fileCount = 0 : self.fileCount - 1;
                $('#popupListDefinition').popup('close');
                $('#alertmsg').text('Illegal character in file name. Uploading cannot continue.');
                $('#alertmsg').addClass('wordwrap');
                $('#popupDialog').popup('open');
                return false;
            }
            else {
                var extension = file_name.split('.');
                var extension = file_name.substring(file_name.lastIndexOf('.') + 1);
                var allowedExtension = ["txt", "csv", "dat", "data", "lst", "xls", "xlsx", "doc", "docx", "gz", "zip"];
                var isValidFileFormat = false;
                var uploading_file_name = "";
                if (typeof (extension) != 'undefined' && extension.length > 1) {
                    for (var i = 0; i < allowedExtension.length; i++) {
                        if (extension.toLowerCase() == allowedExtension[i]) {
                            isValidFileFormat = true;
                            //var file_type = (sessionStorage.roleName == "user" || sessionStorage.roleName == "power") ? 'source' : $("#fieldType").val();
                            var file_type = $("#fieldType").val();
                            $('#hidUploadFile').val(file_name + '|' + file_type);
                            break;
                        }
                    }
                    if (!isValidFileFormat) {
                        self.resetFileUploadControl(self.filesToUpload);
                        self.fileCount = (self.fileCount <= 0) ? self.fileCount = 0 : self.fileCount - 1;
                        $('#popupListDefinition').popup('close');
                        $('#alertmsg').text('Invalid File Type...Only "TXT","CSV","DAT","DATA","LST","XLS","XLSX","DOC","GZ","ZIP" and "DOCX" types are accepted.');
                        $('#alertmsg').addClass('wordwrap');
                        $('#popupDialog').popup('open');
                        return true;
                    }
                    else if (self.verifyFileExistencyInUploads(check_file_name)) {
                        self.resetFileUploadControl(self.filesToUpload);
                        self.fileCount = (self.fileCount <= 0) ? self.fileCount = 0 : self.fileCount - 1;
                        $('#alertmsg').html('The selected file already exists in the list. Please select another file.');
                        $('#popupListDefinition').popup('close');
                        $("#popupDialog").popup('open');
                    }
                }
                var filemappername = file_name.substring(file_name.lastIndexOf('\\') + 1, file_name.lastIndexOf('.')) + "_map.xml";
                $('#name').val(file_name.substring(file_name.lastIndexOf("\\") + 1));
                //populate the file map field
                $('#txtFileMap').val(filemappername);
                if ($('#hidMode').val().toLowerCase() == "edit") {
                    $('#testButtonOk').hide();
                    $('#btnUpdate').show();
                }
                else {
                    $('#testButtonOk').show();
                    $('#btnUpdate').hide();
                }
            }
        }
    };

    self.resetFields = function () {
        $('#btnUpdate').attr('onclick', "pageObj.updateEditFileInfo(this,'','','','','')");
        if ($('#dvUploadedArtFiles select option').length == 0)
            $('#dvUploadedArtFiles').empty();

        //$('#ddlSelectLocations').val('select').selectmenu('refresh');
        $('#fieldName').val('select').selectmenu('refresh');
        $('#fieldType').val('select').selectmenu('refresh').trigger('onchange');
        $('#fieldType').css('display', 'none');
        $('#ddlFilesUploaded').val('select').selectmenu('refresh');
        $('#name').val("");
        $('#btnDelete').hide();
        $('#dvText').show();
        $('#dvBrowse').show();
        $('#drop-area').html('Drop files here to select them <br /> or');
        if ('draggable' in document.createElement('span')) {
            $("#drop-area").html('Drop files here to select them <br /> or');
            $('#dvDragDrop').css('display', 'block');
            $('#dvText').css('display', 'none');
            $('#testButtonOk').text('Upload');
        }
        else {
            $('#dvDragDrop').css('display', 'none');
            $('#dvText').css('display', 'block');
            $('#testButtonOk').text('Select');
        }
        var delimiter_display = 'none';
        $('#fieldType').val('source').selectmenu('refresh').trigger('onchange');
        $('#fieldType').css('display', 'none');


        $("#popupListDefinition h3[id=popupHeader]").html($('#hidFileType').val() == "DBF" ? 'Select CASS File' : 'Select TMC File');

        // $('#dvSelectedLocations').trigger('create');
        //if ($('#dvSelectedLocations input[type=checkbox]').length > 0) {
        //    $('#dvSelectedLocations input[type=checkbox]').attr('checked', false).checkboxradio('refresh');
        //    $('#dvDoNotAssign').show();
        //}
    };

    //Sets the headings for popup header based on the type selected.
    self.setHeadings = function (ctrl) {
        if ('draggable' in document.createElement('span')) {
            self.filesToUpload = [];
            self.uploadingFiles = "";
            data = [];
            temp_data = [];
            $('#drop-area').empty().html("Drop files here to select them <br />or");
        }

        var source_files_to_upload = 0;
        var seed_files_to_upload = 0;
        var dnm_files_to_upload = 0;
        $.each(self.filesToUpload, function (a, b) {
            if (getFileType(b.sourceType) == "source")
                source_files_to_upload++;
        });

        if (appPrivileges.roleName.toLowerCase() == "admin") {
            if (($(ctrl).val().toLowerCase() == "source") && (source_files_to_upload >= self.maxNumberOfFiles || self.uploadedSourceFilesCount >= self.maxNumberOfFiles || (source_files_to_upload + self.uploadedSourceFilesCount) >= self.maxNumberOfFiles) ||
        ($(ctrl).val().toLowerCase() == "seed") && (seed_files_to_upload >= self.maxNumberOfFiles || self.uploadedSeedFilesCount >= self.maxNumberOfFiles || (seed_files_to_upload + self.uploadedSeedFilesCount) >= self.maxNumberOfFiles) ||
        ($(ctrl).val().toLowerCase() == "donotmail") && (dnm_files_to_upload >= self.maxNumberOfFiles || self.uploadedDnmFilesCount >= self.maxNumberOfFiles || (dnm_files_to_upload + self.uploadedDnmFilesCount) >= self.maxNumberOfFiles) ||
        ((dnm_files_to_upload + seed_files_to_upload + source_files_to_upload) >= self.maxNumberOfFiles)) {
                var msg = "The maximum number of files to be uploaded per order has been reached. Please remove a previously uploaded file in order to upload a new file to this order. If this order requires more than " + self.maxNumberOfFiles + " files, please contact your Account Manager.";
                $('#alertmsg').text(msg);
                $("#popupListDefinition").popup('close');
                self.resetFileUploadControl(self.filesToUpload);
                if (!isMultiple && !isDragDrop)
                    self.resetFileUploadPopup();
                $('#popupDialog').popup('open');
                return false;
            }
        }
    }

    //Show/Hide popup controls in popup based on the type
    self.showHidePopupContent = function (ddlType) {
        var entityType = $(ddlType).val();
        if (entityType == "") {
            self.resetFields();
            $('#dvPopupListDefinitionContent').hide();
        }
        else if (entityType == 'add') {
            $('#dvPopupListDefinitionContent').show();
        }
        else if (entityType == 'upload') {
            $('#popupListDefinition').popup('close');
            $('#dvPopupListDefinitionContent').show();
        }
        self.popUpListDefinitionDisplay();

    };

    //Adds the new file information to the job ticket session..
    self.addFileInfo = function (source_type, filename, delimiter, linked_to, location_id) {
        $('#tblFiles').show();
        var file_name = filename;
        file_name = file_name.substring(file_name.lastIndexOf("\\") + 1);
        //file_name = file_name.substring(0, file_name.lastIndexOf('.') + 1) + file_name.substring(file_name.lastIndexOf('.') + 1).toLowerCase().replace('gz', 'txt');
        //file_name = file_name.substring(0, file_name.lastIndexOf('.') + 1) + file_name.substring(file_name.lastIndexOf('.') + 1).replace('gz', 'txt').replace('GZ', 'txt');
        var replaceable_extensions = ['gz', 'zip'];
        var file_ext = file_name.substring(file_name.lastIndexOf('.') + 1, file_name.length);
        if (replaceable_extensions.indexOf(file_ext.toLowerCase()) > -1) file_ext = file_ext.toLowerCase().replace('gz', 'txt').replace('zip', 'txt');
        file_name = file_name.substring(0, file_name.lastIndexOf('.') + 1) + file_ext;

        var is_file_exists = false;

        var file_info;

        var selected_link = getSessionData('advertiser_session');
        file_info = (self.gOutputData.standardizeAction.standardizeFileList != undefined) ? self.gOutputData.standardizeAction.standardizeFileList : 0;

        //eval(getURLDecode(pagePrefs.scriptBlockDict.addFileInfo));

        if (file_info.length > 0) {
            var file_list = [];
            file_list = jQuery.grep(file_info, function (obj) {
                return (parseInt(obj.sourceType) === parseInt(source_type));
            });
            if (!isNaN(parseInt(self.selectedKey)) && file_list.length > 0) {
                file_info[selectedKey].fileName = file_name;
                file_info[selectedKey].sourceType = source_type;
                file_info[selectedKey].linkedTo = linked_to;
            }
            else {
                if (file_list.length > 0) {
                    $.each(file_list, function (key1, val1) {
                        if (val1.fileName == file_name && parseInt(source_type) == parseInt(val1.sourceType)) {
                            val1.fileName = file_name;
                            is_file_exists = true;
                            val1.fileSizePretty = "";
                            val1.fileStatus = "";
                            val1.linkedTo = linked_to;
                        }
                    });
                }

                if (file_list.length == 0 && file_info[0].sourceType == "" && file_info[0].fileName == "") {
                    file_info[0].fileName = file_name;
                    file_info[0].sourceType = source_type;
                    file_info[0].linkedTo = linked_to;
                }
                else if (!is_file_exists) {
                    var temp_file = {};
                    temp_file = {
                        sourceType: source_type,
                        fileName: file_name,
                        fileMap: "",
                        priority: "",
                        fileSizePretty: "",
                        fileStatus: "",
                        linkedTo: linked_to
                    };
                    file_info.push(temp_file);
                }
                else {
                    var temp_file = {
                        sourceType: source_type,
                        fileName: file_name,
                        fileMap: "",
                        priority: "",
                        fileSizePretty: "",
                        fileStatus: "",
                        linkedTo: linked_to
                    };
                    file_info.push(temp_file);
                }
            }
        }
        self.selectedKey = '';
        sessionStorage.jobSetupOutput = JSON.stringify(self.gOutputData);
    };

    //Updates the selected file information to the temporary session before upload and validates if file already exists.
    self.updateFileInfo = function (file_name, source_type) {
        var checked_ctrls = $('fieldset[data-role=controlgroup]').find('input[type=checkbox]:not([value=all]):checked');
        var source_type_code = getFileTypeCode(source_type);

        var file_info = jQuery.grep(self.filesToUpload, function (obj) {
            return (obj.fileName === file_name && parseInt(obj.sourceType) === parseInt(source_type_code));
        });
        if (file_info.length > 0) {
            file_info[0].sourceType = source_type_code;
            file_info[0].fileName = file_name;
            file_info[0].linkedTo = $('#hidFileType').val();
            self.uploadingFiles = (self.uploadingFiles.split('|').length > 0) ? self.uploadingFiles.substring(0, self.uploadingFiles.lastIndexOf('|')) : "";
            $('#validationAlertmsg').html('The selected file already exists in the list. Please select another file.');
            $('#popupListDefinition').popup('close');
            $("#validationPopupDialog").popup('open');
        }
        else {
            if (source_type_code != undefined && file_name != undefined) {
                var file_list_info = {};

                file_list_info["sourceType"] = source_type_code;
                file_list_info["fileName"] = file_name;

                file_list_info["linkedTo"] = $('#hidFileType').val();
                self.filesToUpload.push(file_list_info);
            }
            else {
                if (!isMultiple && !isDragDrop)
                    self.resetFileUploadPopup();

                $('#popupListDefinition').popup('close');
                $('#hidUploadFile').val('');
            }
        }
        self.selectedStates = [];
        sessionStorage.filesToUpload = JSON.stringify(self.filesToUpload);
    };

    //Updates the selected file information in edit mode.
    self.updateEditFileInfo = function (ctrl, file_name, source_type, source_type_code, update_type, location_id) {
        if (update_type == "") {
            //if (!self.uploadValidation(true)) return false;
            if ((!isMultiple && !isDragDrop) && !self.uploadValidation(true)) return false;
            if ((isMultiple || isDragDrop) && !self.uploadMultiplesValidation()) return false;


            var file_name = "";
            if (!isMultiple && !isDragDrop) {
                file_name = $('#name').val();
            }
            else {
                if (temp_data.length > 0) {
                    file_name = temp_data[0].name;
                }
            }
            var source_type = ($('#fieldType').val() != "") ? $('#fieldType').val() : "";

            var source_type_code = getFileTypeCode(source_type);

            var old_file_name = $('#hidEditUploadFile').val().split('^')[0];
            var old_file_type = $('#hidEditUploadFile').val().split('^')[1];
            var old_file_location = $('#hidEditUploadLocation').val();

            var file_info = jQuery.grep(self.filesToUpload, function (obj) {
                return (obj.fileName.substring(obj.fileName.lastIndexOf("\\") + 1) === old_file_name && obj.sourceType === old_file_type);
            });
            if (file_info.length > 0) {
                file_info[0].sourceType = source_type_code;
                file_info[0].locationId = selected_location;
                file_info[0].fileName = file_name;

                $('#popupListDefinition').popup('close');
                if (!isMultiple && !isDragDrop)
                    self.resetFileUploadPopup();
            }
            if (isMultiple || isDragDrop) {
                var data_file_info = jQuery.grep(data, function (obj) {
                    return (obj.name.substring(obj.name.lastIndexOf("\\") + 1) === old_file_name && obj.type === getFileType(old_file_type));
                });

                if (data_file_info.length > 0 && temp_data.length > 0) {
                    data_file_info[0].name = temp_data[0].name;
                    data_file_info[0].type = source_type;
                    data_file_info[0].file = temp_data[0].file;
                }
            }
            temp_data = [];
            var uploading_files = self.uploadingFiles.split('|');
            var temp_uploading_files = "";
            $.each(uploading_files, function (key, val) {
                var file_parts = val.split('^');
                if (file_parts[0].toLowerCase() == old_file_name.toLowerCase()) {
                    file_parts[0] = file_name;
                    val = file_parts[0] + '^' + source_type_code;
                    temp_uploading_files = (temp_uploading_files != "") ? temp_uploading_files + '|' + val : val;
                    //return false;
                }
                else {
                    temp_uploading_files = (temp_uploading_files != "") ? temp_uploading_files + '|' + val : val;
                }
            });
            self.uploadingFiles = temp_uploading_files;
            self.makeFileListBeforePost(self.filesToUpload);
            sessionStorage.filesToUpload = JSON.stringify(self.filesToUpload);
            $('#hidEditUploadLocation').val('');
        }
        self.updatePostUploadFile('list', file_name, source_type, source_type_code);
        postJobSetUpData('save');
    };

    //Removes the selected file from the Uploaded files list.
    self.removeAFile = function (file_name, source_type, source_type_code, location_id) {
        $('#divError').text('');
        var file_delete_url = serviceURLDomain + "api/FileUpload/delete/" + self.facilityId + "/" + self.jobNumber + "/";
        var selected_link = getSessionData('advertiser_session');
        file_delete_url = file_delete_url + source_type_code + "/" + file_name;
        getCORS(file_delete_url, null, function (prod_data) {
            var file_info;
            if (prod_data != "" && $.parseJSON(prod_data).Message != undefined && $.parseJSON(prod_data).Message.indexOf('error') > -1) {
                $('#popupListDefinition').popup('close');
                $('#popupConfirmDialog').popup('close');
                $('#alertmsg').html('An error occurred. Please contact Administrator.');
                $('#popupDialog').popup('open', { positionTo: 'window' });
                //window.setTimeout(function displayErrorMsg() { $('#popupDialog').popup('open', { positionTo: 'window' }); }, 200);
                return false;
            }

            file_info = (self.gOutputData.standardizeAction.standardizeFileList != undefined) ? self.gOutputData.standardizeAction.standardizeFileList : 0;
            var remove_files = [];
            $.each(file_info, function (key1, val1) {
                if (val1.sourceType == source_type_code && val1.fileName.toLowerCase() == file_name.toLowerCase()) {
                    remove_files.push(key1);
                }
            });

            if (remove_files.length > 0) {
                $.each(remove_files, function (key2, val2) {
                    self.resetLinkToUploadedFiles('list', file_info[val2].linkedTo)
                    file_info.splice(val2, 1);
                    if (file_info.length == 0) {
                        file_info.push({
                            sourceType: "",
                            fileName: "",
                            fileMap: "",
                            priority: "",
                            fileSizePretty: "",
                            fileStatus: ""
                        });
                        $('#tblFiles').hide();
                    }
                });
            }
            postJobSetUpData('save');
            self.buildOutputLists(false);
        }, function (error_response) {
            showErrorResponseText(error_response, true);
        });
        $('#popupListDefinition').popup('close');
        $('#popupConfirmDialog').popup('close');
    };

    //Resets the file upload popup.
    self.resetFileUploadPopup = function () {
        $('#ddlSelectLocations').val('').selectmenu('refresh');
        $('#fieldName').val('select').selectmenu('refresh');
        $('#fieldType').val('select').selectmenu('refresh');
        $('#ddlFilesUploaded').val('select').selectmenu('refresh');
        $('#name').val("");

        $("#drop-area").html('Drop files here to select them <br /> or');
        $('#dvPopupListDefinitionContent').hide();
        $('#dvAdavancedPreference').hide();
        $('#fieldType').get(0).selectedIndex = 1;
        $('#fieldType').css('display', 'none');
    };

    //Hides the file upload settings popup.
    self.popUpListDefinitionHide = function () {
        var file_name_to_remove = $('#name').val();
        var filetype_to_remove = $('#fieldType').val();
        var source_type_code = getFileTypeCode(filetype_to_remove);
        var count = 0;
        var index = 0;
        if ($('#hidMode').val().toLowerCase() != "edit") {
            //remove the file from the local session.
            if (!isMultiple && !isDragDrop) {
                var files_to_remove = jQuery.grep(self.filesToUpload, function (v) {
                    count++;
                    //eval(getURLDecode(pagePrefs.scriptBlockDict.popUpListDefinitionHide));
                    if ((v.sourceType == source_type_code) && (v.fileName.substring(v.fileName.lastIndexOf("\\") + 1) == file_name_to_remove))
                        index = count - 1;
                    return (v.sourceType == source_type_code) ? ((v.fileName.substring(v.fileName.lastIndexOf("\\") + 1) == file_name_to_remove) ? false : true) : true;

                });
                if (($('#hidMode').val().toLowerCase() != "")) {
                    self.filesToUpload = files_to_remove;
                    if (self.filesToUpload.length == 0) {
                        $("#divFilesToUpload").hide();
                        var tdFileInputsTemp = ($.browser.msie) ? window.frames[0].document.getElementById('formUpload') : document.getElementById('formUpload');
                        var number_of_files_selected = $(tdFileInputsTemp).find('input[type=file]').length;
                        var fileInput = $(tdFileInputsTemp).find('input[type=file]')[number_of_files_selected - 1];
                        // Create a new file input
                        var newFileInput = fileInput.cloneNode(true);
                        newFileInput.value = null;
                        var id = "mailFiles[]";
                        newFileInput.id = id // A unique id
                        newFileInput.name = newFileInput.id;
                        newFileInput.style.display = "";
                        $($(tdFileInputsTemp).find('input[type=file]')[number_of_files_selected - 1]).remove();
                        var spnFileInputs = ($.browser.msie) ? window.frames[0].document.getElementById('spnFileInput') : document.getElementById('spnFileInput');
                        spnFileInputs.appendChild(newFileInput);
                        (($.browser.msie) ? $(window.frames[0].frames.frameElement).contents().find('input[type=file]') : $('input[type=file]')).bind('change', function (e) {
                            self.navigate(e, $(e.target))
                        });
                        self.fileCount = 0;
                    }
                    else if (!isMultiple && !isDragDrop) {
                        //remove the file control from the form.
                        self.resetFileUploadControl(self.filesToUpload);
                        self.fileCount = (self.fileCount <= 0) ? 0 : self.fileCount - 1;
                        self.makeFileListBeforePost(self.filesToUpload)
                    }
                }
                else if (!isMultiple && !isDragDrop) {
                    self.resetFileUploadControl(self.filesToUpload);
                    self.fileCount = (self.fileCount <= 0) ? 0 : self.fileCount - 1;
                    self.makeFileListBeforePost(self.filesToUpload)
                }
            }
            else {
                if (isMultiple || isDragDrop) {
                    data = [];
                    temp_data = [];
                    self.filesToUpload = [];
                    self.uploadingFiles = "";
                }
            }
        }
        else {
            if (isMultiple || isDragDrop) {
                data = [];
                temp_data = [];
                self.filesToUpload = [];
                self.uploadingFiles = "";
            }
        }
        sessionStorage.filesToUpload = JSON.stringify(self.filesToUpload);
        if (!isMultiple && !isDragDrop)
            self.resetFileUploadPopup();
        $('#hidMode').val('');
        $('#popupListDefinition').popup('open');
        $('#popupListDefinition').popup('close');
        $('#hidUploadFile').val('');
    };

    //Creates the list of files selected to upload and display in UI.
    self.makeFileListBeforePost = function (array_name) {
        var array = eval(array_name);
        var list_info = "";
        var mail_files = "";
        var dnm_files = "";
        var seed_files = "";
        var select_files = "";
        $("#ulFilesToUpload").empty();
        var row_to_add = (array_name.length > 0) ? '<li data-role="list-divider" >Selected List Files for Upload</li>' : '';

        $.each(array_name, function (key, val) {
            if (val.fileName != "") {

                var file_name = val.fileName;
                file_name = file_name.substring(file_name.lastIndexOf("\\") + 1);
                var source_type = getFileType(val.sourceType);

                var drop_all_states_except = "";
                var temp_loc = "";
                temp_loc = (val.locationId != null && val.locationId.indexOf(',') > -1) ? val.locationId.replace(/,/g, '_') : val.locationId;
                row_to_add += '<li><a href="#popupListDefinition" data-rel="popup" data-position-to="window" data-transition="pop" onclick="pageObj.getFileSettings(\'' + file_name + '\', \'' + source_type + '\',\'' + temp_loc + '\')">';
                row_to_add += '<h3>' + file_name + '</h3>';
                row_to_add += '</a><a href="#" onclick="pageObj.removeFileBeforeUpload(event,\'' + file_name + '^' + getFileType(val.sourceType) + '\')" >Remove List</a></li>';

                $("#popupListDefinition").popup("close");
            }
            else {
                $("#lnkUploadFile").show();
            }
        });
        if (row_to_add != "") {
            $("#ulFilesToUpload").append(row_to_add);

            $("#ulFilesToUpload").listview('refresh');
            $("#divFilesToUpload").show();
            $('#dvSubmit').css('display', 'block');
        }
        else {
            $('#dvSubmit').css('display', 'none');
        }
    };

    //Validates the file selection settings.
    self.uploadValidation = function (validate_all) {
        var msg = "<ul>";
        if ((!isMultiple && !isDragDrop) && !('draggable' in document.createElement('span'))) {
            if (validate_all) {
                if ($('#name').val() == "")
                    msg += "<li>Please choose a file to upload.</li>";
            }
        }
        msg += "</ul>";

        if (msg != "<ul></ul>") {
            $("#popupListDefinition").popup("close");
            $('#validationAlertmsg').html(msg);
            window.setTimeout(function getDelay() {
                $('#validationPopupDialog').popup('open');
                return false;
            }, 200);

        }
        else
            return true;
    };

    //Updates the list of files to be uploaded.
    self.updateFileList = function () {
        if ((!isMultiple && !isDragDrop) && !self.uploadValidation(true)) return false;
        if ((isMultiple || isDragDrop || ('draggable' in document.createElement('span'))) && !self.uploadMultiplesValidation()) return false;

        var file_info = "";
        var file_name = ""
        var source_type = "";
        var source_type_code = "";
        var selected_location = $('#ddlSelectLocations').val();

        if (!isMultiple && !isDragDrop) {
            var file_info = $('#hidUploadFile').val();
            var file_name = file_info.split('|')[0];
            var source_type = file_info.split('|')[1];
            var source_type_code = getFileTypeCode(source_type);
            var selected_location = $('#ddlSelectLocations').val();

            var uploading_file_name = "";
            if (file_name != "") {
                uploading_file_name = file_name.substring(file_name.lastIndexOf("\\") + 1);
                if (!self.verifyFileExistence(uploading_file_name, source_type_code)) {
                    $('#popupListDefinition').popup('close');
                    $('#alertmsg').text('The selected file(s) already exist(s) in the list. Please select another file(s).');
                    self.resetFileUploadControl(self.filesToUpload);
                    self.fileCount = (self.fileCount <= 0) ? self.fileCount = 0 : self.fileCount - 1;
                    window.setTimeout(function getDelay() {
                        $('#popupDialog').find('a[id=okBut]').bind('click', function () {
                            $("#popupDialog").popup("close");
                            $('#popupDialog').find('a[id=okBut]').attr('onclick', "$(\'#popupDialog\').popup(\'close\')");
                        });
                        $('#popupDialog').popup('open');
                        return false;
                    }, 200);
                    return false;
                }
                else {
                    self.uploadingFiles += (self.uploadingFiles != "") ? "|" + uploading_file_name + "^" + source_type_code : uploading_file_name + "^" + source_type_code;
                    self.updateFileInfo(file_name, source_type);
                    self.makeFileListBeforePost(self.filesToUpload);
                    $("#popupListDefinition").popup("close");
                    if (!dontShowHintsAgain && (sessionStorage.showDemoHints == undefined || sessionStorage.showDemoHints == null || sessionStorage.showDemoHints == "" || sessionStorage.showDemoHints == "on") && (sessionStorage.isListFileSelectedFirstTime == undefined || sessionStorage.isListFileSelectedFirstTime == null || sessionStorage.isListFileSelectedFirstTime == "" || sessionStorage.isListFileSelectedFirstTime == "false") && self.filesToUpload.length == 1 && (showUploadSelectedListDemoHints())) {
                        sessionStorage.isListFileSelectedFirstTime = true;
                        $('#uploadSelectedListDemoHints').popup('open', { positionTo: '#btnUploadFiles' });
                    }
                }
            }
            else {
                $("#popupListDefinition").popup("close");
            }
        }
        else {
            $('#btnUploadFiles').trigger('click');
        }
    };

    //Verifies the existence of the file in the list of files selected to upload.
    self.verifyFileExistence = function (file_name, source_type_code) {
        var list_files_to_upload = self.filesToUpload;
        var list_files_uploaded = (self.gOutputData.standardizeAction.standardizeFileList != undefined) ? self.gOutputData.standardizeAction.standardizeFileList : {};
        var to_upload = [];
        if ((!isMultiple && !isDragDrop)) {
            to_upload = $.grep(self.filesToUpload, function (obj) {
                return (obj.fileName.substring(obj.fileName.lastIndexOf('\\') + 1) === file_name && parseInt(obj.sourceType) === parseInt(source_type_code));
            });
        }
        var upload_files = [];
        upload_files = $.grep(list_files_uploaded, function (obj) {
            return (obj.fileName === file_name && parseInt(obj.sourceType) === parseInt(source_type_code));
        });
        if (to_upload.length > 0 || Object.keys(upload_files).length > 0) {
            return false;
        }
        return true;
    };

    //Removes the file from the temp session and the UI list files before upload
    self.removeFileBeforeUpload = function (e, file_name) {
        $('#divError').text('');
        var spnFileInputs = ($.browser.msie) ? window.frames[0].document.getElementById('spnFileInput') : document.getElementById('spnFileInput');
        var file_name_to_remove = file_name.split('^')[0];
        var filetype_to_remove = file_name.split('^')[1]

        var source_type_code = "";
        source_type_code = getFileTypeCode(filetype_to_remove);
        var count = 0;
        var index = -1;

        //remove the file from the local session.
        var files_to_remove = jQuery.grep(self.filesToUpload, function (v) {
            count++;
            if ((v.sourceType == source_type_code) && (v.fileName.substring(v.fileName.lastIndexOf("\\") + 1) == file_name_to_remove))
                index = count - 1;
            return (v.sourceType == source_type_code) ? ((v.fileName.substring(v.fileName.lastIndexOf("\\") + 1) == file_name_to_remove) ? false : true) : true;
        });
        if (index > -1) {
            var uploading_files = self.uploadingFiles.split('|');
            self.uploadingFiles = [];
            $.each(uploading_files, function (a, b) {
                var file_info = b.split('^');
                if (file_name_to_remove.toLowerCase() == file_info[0].toLowerCase() && file_info[1] == source_type_code) {
                }
                else {
                    self.uploadingFiles += (self.uploadingFiles != "") ? '|' + b : b;
                }
            });

        }
        if (isMultiple || isDragDrop) {
            count = 0;
            var data_files_to_remove = jQuery.grep(data, function (v) {
                count++;
                if ((v.type == filetype_to_remove) && (v.name.substring(v.name.lastIndexOf("\\") + 1) == file_name_to_remove))
                    index = count - 1;
                return (v.type == filetype_to_remove) ? ((v.name.substring(v.name.lastIndexOf("\\") + 1) == file_name_to_remove) ? false : true) : true;
            });
            if (index > -1) {
                data.splice(index, 1);
            }
        }
        self.filesToUpload = files_to_remove;
        if (isMultiple || isDragDrop) {
            self.fileCount = (self.fileCount <= 0) ? 0 : self.fileCount - 1;
        } else if (self.filesToUpload.length == 0 && (!isMultiple && !isDragDrop)) {
            $("#divFilesToUpload").hide();
            var tdFileInputsTemp = ($.browser.msie) ? window.frames[0].document.getElementById('formUpload') : document.getElementById('formUpload');
            var number_of_files_selected = $(tdFileInputsTemp).find('input[type=file]').length;
            var fileInput = $(tdFileInputsTemp).find('input[type=file]')[number_of_files_selected - 1];
            // Create a new file input
            var newFileInput = fileInput.cloneNode(true);
            newFileInput.value = null;
            var id = "mailFiles[]";
            newFileInput.id = id // A unique id
            newFileInput.name = newFileInput.id;
            newFileInput.style.display = "";
            $($(tdFileInputsTemp).find('input[type=file]')[number_of_files_selected - 1]).remove();
            var spnFileInputs = ($.browser.msie) ? window.frames[0].document.getElementById('spnFileInput') : document.getElementById('spnFileInput');
            spnFileInputs.appendChild(newFileInput);
            if (!isMultiple && !isDragDrop)
                self.resetFileUploadPopup();
            (($.browser.msie) ? $(window.frames[0].frames.frameElement).contents().find('input[type=file]') : $('input[type=file]')).bind('change', function (e) {
                self.navigate($(e.target))
            });
            fileCount = 0;
        }
        else {
            //remove the file control from the form.
            $($((($.browser.msie) ? window.frames[0].document.getElementById('formUpload') : document.getElementById('formUpload'))).find('input[type=file]')[index]).remove();
            var number_of_files_selected = $('input[type=file]').length;
            if (number_of_files_selected > 0) {
                $($((($.browser.msie) ? window.frames[0].document.getElementById('formUpload') : document.getElementById('formUpload'))).find('input[type=file]')[number_of_files_selected - 1]).attr("id", "mailFiles[]");
                $($((($.browser.msie) ? window.frames[0].document.getElementById('formUpload') : document.getElementById('formUpload'))).find('input[type=file]')[number_of_files_selected - 1]).attr("name", "mailFiles[]");
            }
            self.fileCount = (self.fileCount <= 0) ? 0 : self.fileCount - 1;
        }
        self.makeFileListBeforePost(self.filesToUpload);
        sessionStorage.filesToUpload = JSON.stringify(self.filesToUpload);
    };

    //Opens the file upload popup with file uploaded control added to UI dynamically.
    self.addNewFile = function (ctrl) {
        $('#divError').text('');
        $('#hidMode').val('');
        $('#hidFileType').val($(ctrl).attr('data-fileType'));
        self.openNewFileDialog();
    };

    //Opens the File upload popup and validates before opening for maximum files that can be uploaded to a job/order.
    self.openNewFileDialog = function () {
        //if (self.gOutputData == undefined)
        //    self.gOutputData = jQuery.parseJSON(sessionStorage.jobSetupOutput);
        var source_files_to_upload = 0;
        $.each(self.filesToUpload, function (a, b) {
            if (getFileType(b.sourceType) == "source")
                source_files_to_upload++;
        });
        //(appPrivileges.roleName.toLowerCase() == "user" || appPrivileges.roleName.toLowerCase() == "power") && 
        if (((source_files_to_upload > self.maxNumberOfFiles) || (self.uploadedSourceFilesCount >= self.maxNumberOfFiles) || ((source_files_to_upload + self.uploadedSourceFilesCount > self.maxNumberOfFiles)))) {
            var msg = "The maximum number of files to be uploaded per order has been reached. Please remove a previously uploaded file in order to upload a new file to this order. If this order requires more than " + self.maxNumberOfFiles + " files, please contact your Account Manager.";
            $('#alertmsg').text(msg);
            $('#popupDialog').popup('open');
            return false;
        }
        else {
            if (!('draggable' in document.createElement('span'))) {
                self.addFileInput();
            }
            self.resetFields();
            self.showHidePopupDataOnLocationChange('uploadList');
            $('#testButtonOk').show();
            $(($.browser.msie) ? window.frames[0].document.getElementById('spnFileInput') : document.getElementById('spnFileInput'))[0].style.display = "";
            $('#btnUpdate').hide();
            $('#hidUploadFile').val('');
            self.popUpListDefinitionDisplay();
        }
    };

    //Validates the selected files in list have been uploaded to the job or not.
    self.validateUploadingListFiles = function () {
        if (self.filesToUpload.length > 0) {
            var msg = "The files you have selected have not been uploaded to this order. Please click the 'Upload Selected Lists' button to upload the selected files or Remove selected files you do not want to upload.";
            $('#alertmsg').text(msg);
            $('#popupDialog').popup('open');
            return false;
        }
        else {
            self.persistDropAllStatesExcept();
            if (sessionStorage.isFromList != undefined && sessionStorage.isFromList != null && sessionStorage.isFromList != "")
                sessionStorage.removeItem("isFromList");
            if (sessionStorage.selectedListType != undefined && sessionStorage.selectedListType != null && sessionStorage.selectedListType != "")
                sessionStorage.removeItem("selectedListType");
            return true;
        }
    };
    //Closes the popup dialog.
    self.closeDialog = function () {
        $('#popupDialog').popup('open');
        $('#popupDialog').popup('close');
    }

    //Displays the popup dialog when user clicks on edit button for the files that were uploaded.
    //self.displayPostUploadEditDialog = function (file_name, source_type, source_type_code, linked_to, update_type) {
    //    self.openEditFileDialog(file_name, source_type, source_type_code, linked_to, update_type);
    //}

    //Open the edit file dialog 
    self.openEditFileDialog = function (file_name, source_type, source_type_code, linked_to, update_type, edit_type, location_id) {
        $('#dvListType').hide();
        //$('#dvDDlDelimiter').hide();
        //$('#dvLblDelimiter').hide();
        $('#dvText').hide();
        $('#dvBrowse').hide();
        $('#dvBrowse').css('display', 'none');
        $('#dvDragDrop').css('display', 'none');
        $('#dvFileMap').hide();
        $('#dvDeDupe').hide();
        $('#testButtonOk').hide();
        $('#dvAdavancedPreference').hide();
        //if (edit_type == "editlocationassignment") {
        //    $("#popupListDefinition h3[id=popupHeader]").html('Edit Location Assignment');
        //    $('#hidEditUploadFile').val(file_name);
        //    $('#hidEditUploadLocation').val(location_id);
        //    update_type = "editlocationassignment";
        //    if ($('#dvSelectedLocations .ui-checkbox').length == 0)
        //        $('#dvSelectedLocations').trigger('create');
        //    $('#dvSelectedLocations input[type=checkbox]').attr('checked', false).checkboxradio('refresh');
        //    $('#dvDoNotAssign input[type=checkbox]').attr('checked', false).checkboxradio('refresh');

        //    if (location_id != "" && location_id.toString().indexOf('_') > -1) {
        //        var selected_locations = location_id.split('_');
        //        $.each(selected_locations, function (key, val) {
        //            $('#dvSelectedLocations input[type=checkbox][value=' + val + ']').attr('checked', true);
        //        });
        //        if (selected_locations.length == $('#dvSelectedLocations input[type=checkbox]:not([value="all"])').length)
        //            $('#dvSelectedLocations input[type=checkbox][value="all"]').attr('checked', true);
        //    }
        //    else if (location_id != "") {
        //        if (location_id == "none") {
        //            $('#dvDoNotAssign input[type=checkbox][value=' + location_id + ']').attr('checked', true).checkboxradio('refresh');                    
        //        }
        //        else {
        //            $('#dvSelectedLocations input[type=checkbox][value=' + location_id + ']').attr('checked', true);
        //            if ($('#dvSelectedLocations input[type=checkbox]:not([value="all"])').length == 1 && $('#dvSelectedLocations input[type=checkbox][value=' + location_id + ']').length > 0)
        //                $('#dvSelectedLocations input[type=checkbox]').attr('checked', true).checkboxradio('refresh');
        //        }                
        //    }
        //    self.checkUnAssignLocations();
        //    window.setTimeout(function () {
        //        if ($('#dvSelectedLocations .ui-checkbox') == 0)
        //            $('#dvSelectedLocations').trigger('create');
        //        $('#dvSelectedLocations input[type=checkbox]').checkboxradio('refresh');
        //    }, 1000);
        //}

        $('#dvPopupListDefinitionContent').show();
        if (edit_type != 'editlink' && edit_type != 'editlocationassignment') {
            $('#btnDelete').show();
            $('#btnDelete').attr('onclick', 'return pageObj.canDeleteListFile(\'' + file_name + '\',\'' + source_type + '\',' + source_type_code + ',\'' + location_id + '\');');
        }
        else {
            $('#btnDelete').hide();
        }
        $('#btnUpdate').attr('onclick', 'pageObj.updateEditFileInfo(this,\'' + file_name + '\',\'' + source_type + '\',\'' + source_type_code + '\',\'' + update_type + '\',\'' + location_id + '\')');
        $('#popupListDefinition').popup('open');
    }
    //Gets user confirmation for deleting a list file.
    self.canDeleteListFile = function (file_name, source_type, source_type_code, location_id) {
        $('#popupListDefinition').popup('close');
        $('#popupEditList').popup('close');
        $('#confirmMsg').html('Are you sure you want to delete selected file?');
        $('#okButConfirm').attr('onclick', 'return pageObj.removeAFile(\'' + file_name + '\',\'' + source_type + '\',' + source_type_code + ',\'' + location_id + '\');');
        $('#okButConfirm').text('Delete');
        $('#popupConfirmDialog').popup('open', { positionTo: 'window' });
    };

    //Reassigns the locations to selected list.
    self.reAssignLocations = function (file_name, location_id, linked_to, source_type_code, update_type) {
        var uploading_file_name = "";
        if (file_name != "") {
            // pull the file info from gOutputData and udpate with new file and update the same in session.
            var file_info;
            file_info = (self.gOutputData.standardizeAction.standardizeFileList != undefined) ? self.gOutputData.standardizeAction.standardizeFileList : {};
            file_info = jQuery.grep(file_info, function (obj) {
                return (obj.fileName === file_name && parseInt(obj.sourceType) === parseInt(source_type_code));
            });

            //var file_info = (Object.keys(file_info).length > 0 && file_info[$('#hidEditUploadFile').val()] != undefined && file_info[$('#hidEditUploadFile').val()] != null && Object.keys(file_info[$('#hidEditUploadFile').val()]).length > 0) ? file_info[$('#hidEditUploadFile').val()] : {};

            var checked_ctrls = $('fieldset[data-role=controlgroup]').find('input[type=checkbox]:not([value=all]):checked');
            var selected_location = '';
            if (checked_ctrls.length > 0)
                $.each(checked_ctrls, function (a, b) {
                    selected_location += (selected_location != '') ? ',' + b.value : b.value;
                });

            if (Object.keys(file_info).length > 0) {
                file_info[0].locationId = selected_location;  //Get all the locations as comma separated string.

            }

            sessionStorage.jobSetupOutput = JSON.stringify(self.gOutputData);
            self.reloadJobTicket();
            //Call uploadFiles(e) function to upload.
            $("#popupDialog").on({
                popupafteropen: function (event, ui) {
                    if (!isMultiple && !isDragDrop)
                        self.resetFileUploadPopup();
                    self.resetReplaceFileCtrl();
                    $("#popupDialog").off("popupafteropen");
                }
            });
        }
    };

    self.verifyFileExistencyInUploads = function (file_name) {
        var is_file_exists = false;
        if ('draggable' in document.createElement('span')) {
            if (data.length > 0) {
                $.each(data, function (key, val) {
                    if (val.name.toLowerCase() == file_name.toLowerCase())
                        is_file_exists = true;
                });
            }
        }

        file_info = (self.gOutputData.standardizeAction.standardizeFileList != undefined) ? self.gOutputData.standardizeAction.standardizeFileList : 0;
        if (file_info.length > 0) {
            $.each(file_info, function (key, val) {
                if (val.fileName.toLowerCase().substring(val.fileName.lastIndexOf('\\') + 1, val.fileName.length) == file_name.toLowerCase())
                    is_file_exists = true;
            });
        }
        if (self.filesToUpload.length > 0) {
            $.each(self.filesToUpload, function (key, val) {
                if (val.fileName.toLowerCase().substring(val.fileName.lastIndexOf('\\') + 1, val.fileName.length) == file_name.toLowerCase())
                    is_file_exists = true;
            });
        }

        return is_file_exists;
    }

    //Displays the Popup window settings when the user clicks on edit button from the list of uploaded files.
    self.displayPostUploadEditDialog = function (file_name, source_type, source_type_code, linked_to, update_type, location_id) {
        //self.openEditFileDialog(file_name, location_id, version_code, linked_to, update_type);
        //if (jobCustomerNumber != OH)
        $('#popupEditList a[data-ActionType="editlink"]').parent().css('display', 'none');
        $('#popupEditList a[data-ActionType="editlocationassignment"]').parent().css('display', 'block');
        if (!showLocationAssignment())
            $('#popupEditList a[data-ActionType="editlocationassignment"]').parent().css('display', 'none');

        $('#popupEditList a').bind('click', function (event) {
            var msg = "";
            $('#popupEditList').popup('close');
            var src = event.target || event.targetElement;
            var type = $(src).attr('data-ActionType');
            switch (type) {
                case "editlink":
                    self.openEditFileDialog(file_name, source_type, source_type_code, linked_to, update_type, 'editlink', location_id);
                    break;
                case "deletelist":
                    pageObj.canDeleteListFile(file_name, source_type, source_type_code, location_id);
                    break;
                case "editlocationassignment":
                    $('#confirmMsg').text('This assignment will affect previous list assignments. Please review previous list assignments.');
                    //$('#okButConfirm').attr('onclick', '$("#popupConfirmDialog").popup("open");$("#popupConfirmDialog").popup("close");pageObj.uploadReplacingFile(\'' + file_name + '\',\'' + check_file_name + '\',\'' + linked_to + '\',true);');
                    $('#cancelButConfirm').attr('onclick', '$("#popupConfirmDialog").popup("open");$("#popupConfirmDialog").popup("close");pageObj.resetFileUploadPopup();pageObj.resetReplaceFileCtrl();');
                    //$('#popupConfirmDialog').popup('open');
                    $('#okButConfirm').unbind('click');
                    $('#okButConfirm').removeAttr('onclick');

                    //Display confirmation msg and on user confirmation, update the assignments.
                    $('#okButConfirm').unbind('click');
                    $('#okButConfirm').bind('click', function () {
                        $('#popupConfirmDialog').popup('close');
                        $('#okButConfirm').unbind('click');
                        $('#okButConfirm').text('Ok');
                        window.setTimeout(function () {
                            $('#okButConfirm').unbind('click');
                            self.openEditFileDialog(file_name, source_type, source_type_code, linked_to, '', 'editlocationassignment', location_id);
                        }, 1000);
                    });

                    $('#okButConfirm').text('Continue');
                    $('#popupConfirmDialog').popup('open');
                    break;
            };
            $('#popupEditList a').unbind('click');
        });

        $('#popupEditList').popup('open');
    }

    //Resets the Upload control after the replace action is completed.
    self.resetReplaceFileCtrl = function () {
        //reset the fields...
        self.resetFields();
        if ('draggable' in document.createElement('span')) {
            var upload_form = ($.browser.msie) ? window.frames[0].document.getElementById('spnFileInput') : document.getElementById('spnFileInput');
            var file_ctrls = $(upload_form).find('input[type=file]');
            if (file_ctrls.length > 0) {
                var newFileInput = file_ctrls[0].cloneNode(true);
                newFileInput.value = null;
                newFileInput.style.display = "";
                newFileInput.id = "mailFiles[]";
                newFileInput.name = "mailFiles[]";
                $((($.browser.msie) ? window.frames[0].document.getElementById('spnFileInput') : document.getElementById('spnFileInput'))).find('input[type=file]').remove();
                $((($.browser.msie) ? window.frames[0].document.getElementById('spnFileInput') : document.getElementById('spnFileInput'))).append(newFileInput);
                (($.browser.msie) ? $(window.frames[0].frames.frameElement).contents().find('input[type=file]') : $('input[type=file]')).bind('change', function (e) {
                    self.navigate(e, $(e.target))
                });
                self.fileCount = 0;
            }
        }
    };

    //******************** Public Functions End **************************
};