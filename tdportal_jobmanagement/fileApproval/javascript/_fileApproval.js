﻿jQuery.support.cors = true;

//******************** Global Variables Start **************************
var gData;
var gDataCompare;
//var fileApprovalURL = 'JSON/_fileApproval.JSON';
var fileApprovalURL = serviceURLDomain + 'api/ApprovalMw/' + getSessionData("publisherId");
//var fileApprovalURL = serviceURLDomainInternal + 'api/MediaWorksApproval';

//******************** Global Variables End **************************

//******************** Page Load Events Start **************************
$('#_mediaworksFileApproval').live('pagebeforecreate', function (event) {
    displayMessage('_mediaworksFileApproval');
    fnConfirmSignout('_mediaworksFileApproval');
    fnCreateMWNavLinks('approval');

    //test for mobility...
    var is_mobile = false;
    mobile = ['iphone', 'ipod', 'android', 'blackberry', 'nokia', 'opera mini', 'windows mobile', 'windows phone', 'iemobile'];
    for (var i in mobile) if (navigator.userAgent.toLowerCase().indexOf(mobile[i].toLowerCase()) > 0) {
        is_mobile = true;
    }
    // desktop storage
    if (localStorage.mobile) {
        is_mobile = true;
    }
    if (!is_mobile) {
        $('head').append('<link rel="stylesheet" type="text/css" href="../css/screen.css" media="screen" />');
    }
    // desktop storage
    if (sessionStorage.desktop)
        $('head').append('<link rel="stylesheet" type="text/css" href="../css/screen.css" media="screen" />');
        // mobile storage
    else if (localStorage.mobile)
        return true;

    if ($.browser.msie && jQuery.browser.version.substr(0, 1) == '6') {
        alert("This page does not support Internet Explorer 6. Please consider downloading a newer browser. Click 'OK' to close");
        window.open('', '_self', '');
        window.close();
        return false;
    }

});

$(document).on('pageshow', '#_mediaworksFileApproval', function (event) {
    $('#divHeader').html("File Approval for " + appPrivileges.publisherName + " <BR/");
    makeGData();
});

//******************** Model Creation Start **************************

// Define a "PublisherSet" class that tracks its own name and children, and has a method to add a new child
var Publisher = function (publication_name, children, data_theme, grey_fsd_style, grey_fsd_action_style, action_to_take, action_status, file_service_details, publication_status) {
    this.publicationName = publication_name;
    this.dataTheme = data_theme;
    this.greyFsdStyle = grey_fsd_style;
    this.greyFsdActionStyle = grey_fsd_action_style;
    this.id = publication_name.replace(/ /g, '');
    this.children = ko.observableArray(children);
    this.actionToTake = action_to_take;
    this.actionStatus = action_status;
    this.fileServiceDetails = file_service_details;
    this.publicationStatus = publication_status;
}

function createViewModel() {
    var upload_model = [];
    var are_files_missing = true;
    $.each(gData.approvalPublications, function (key, val) {
        var data_theme = '';
        var grey_fsd_style = '';
        var grey_fsd_action_style = '';
        if (val.actionStatus == -1) {
            data_theme = 'h';
            grey_fsd_action_style = 'font-size:12px; font-style:normal; font-weight:lighter;';
        }
        else if (val.actionStatus == 1) {
            data_theme = 'g';
            grey_fsd_action_style = 'font-size:12px; font-style:normal; font-weight:lighter;';
        }
        else if (val.actionStatus == 2) {
            data_theme = 'b';
            grey_fsd_action_style = 'font-size:12px; font-style:normal; font-weight:lighter;';
        }
        else {
            data_theme = 'c';
            grey_fsd_style = 'color:#CCC;'
            grey_fsd_action_style = 'font-size:12px; font-style:normal; font-weight:lighter;color:#999';
        }
        var publisher = new Publisher(val.publicationName, getNestedMappings(val), data_theme, grey_fsd_style, grey_fsd_action_style, val.actionToTake, val.actionStatus, val.fileServiceDetails, val.publicationStatus);
        upload_model.push(publisher);
    });
    return upload_model;
}

function getNestedMappings(data) {
    var nested_mappings_data = [];
    $.each(data, function (key, value) {
        if (key.toLowerCase().indexOf('dict') > -1)
            nested_mappings_data.push({ 'key': key, 'value': nestedMapping(value) });
    });
    return nested_mappings_data;
}

function nestedMapping(data) {
    var key, value, type;
    var temp_level = [];
    for (key in data) {
        if (data.hasOwnProperty(key)) {
            if (data[key] instanceof Object) {
                value = ko.observableArray([]);
                level.push({ key: key, value: nestedMapping(data[key], value()) });
                //level.push({ key: key, value: value });
            } else {
                value = ko.observable(data[key]);
                temp_level.push({ key: key, value: value });
            }

        }
    }
    return temp_level;
}



//******************** Model Creation End **************************

//******************** Public Functions Start **************************
function makeGData() {
    $.getJSON(fileApprovalURL, function (data) {
        gData = data;
        gDataCompare = data;

        sessionStorage["gDataCompare"] = JSON.stringify(gDataCompare);

        bindEmailDropDown();
        var model = createViewModel();
        ko.applyBindings(model, $('#divList')[0]);
        //fillPublicationDDL();
        //displayList();
        $.each($('#divList'), function (obj) {
            $(this).collapsibleset();
        });
        $("#divList ul").each(function (i) {
            $(this).listview();
        });
        //        $.each($('input[type=radio]'), function (a, b) {
        //            $(this).checkboxradio();
        //        });
        $('.ui-page').trigger('create');
        $('div[id^=div]:[data-theme=c]').find("*").click(function (e) {
            e.stopImmediatePropagation();
            e.preventDefault();
        });
    });
    //TODO: below code need to be uncommented once the web service is ready. Above code need to comment.

    getCORS(fileApprovalURL, null, function (data) {
        data.approvalPublications.sort(function (a, b) {
            var x = a.publicationName, y = b.publicationName;
            return x < y ? -1 : x > y ? 1 : 0;
        });

        var temp_data = $.extend(true, [], data.approvalPublications);
        var publication_name = "";
        var counter = 0;
        $.each(data.approvalPublications, function (a, b) { //TODO: need to remove this code.  This is because, we are getting multiple times (repeating rows)
            if (publication_name != "" && $.trim(publication_name) == $.trim(b.publicationName))
                temp_data.splice(counter, 1);
            else
                counter += 1;

            publication_name = b.publicationName;
        });

        data.approvalPublications = temp_data;

        gData = data;
        gDataCompare = data;

        sessionStorage["gDataCompare"] = JSON.stringify(gDataCompare);

        bindEmailDropDown();
        var model = createViewModel();
        ko.applyBindings(model, $('#divList')[0]);
        //fillPublicationDDL();
        //displayList();
        $.each($('#divList'), function (obj) {
            $(this).collapsibleset();
        });
        $("#divList ul").each(function (i) {
            $(this).listview();
        });
        //        $.each($('input[type=radio]'), function (a, b) {
        //            $(this).checkboxradio();
        //        });
        $('.ui-page').trigger('create');
        $('div[id^=div]:[data-theme=c]').find("*").click(function (e) {
            e.stopImmediatePropagation();
            e.preventDefault();
        });
        //        displayList();
    }, function (error_response) {
        showErrorResponseText(error_response, true);
    });
}
function displayList() {
    $('#divList').empty();
    var control_to_add = '';
    var radio_name = "";
    var show_text_area = 'none';
    var style_row = "";
    $.each(gData.approvalPublications, function (key, val) {
        var grey_fsd_style = "";
        var grey_fsd_action_style = "";
        var count = 0;
        var row_count = 0;
        var data_theme = '';
        if (val.actionStatus == -1)
            data_theme = 'h';
        else if (val.actionStatus == 1)
            data_theme = 'g';
        else if (val.actionStatus == 2)
            data_theme = 'b';
        else {
            data_theme = 'c';
            grey_fsd_style = "style='color:#CCC;'"
            grey_fsd_action_style = 'color:#999';
        }

        control_to_add = "<div data-role='collapsible' data-theme='" + data_theme + "' data-collapsed='true' id='div" + val.publicationName.replace(/ /g, "") + "'><h2><span " + grey_fsd_style + ">" + val.publicationName + "</span><div>";
        if (val.fileServiceDetails != "")
            control_to_add += "<p style='font-size:12px; font-style:normal; font-weight:lighter;" + grey_fsd_action_style + "' class='word_wrap'>" + val.fileServiceDetails + "</p>";
        if (val.actionToTake != "")
            control_to_add += "<p style='font-size:12px; font-style:normal; font-weight:lighter;" + grey_fsd_action_style + "'>" + val.actionToTake + "</p>"
        control_to_add += "</div></h2><div data-role='fieldcontain' >";
        control_to_add += "<div data-role='fieldcontain'><table class='roundedCorners' width='100%' style='padding: 5px; background-color:#FFFFFF' id='thetable" + val.publicationName.replace(/ /g, "") + "'>";
        control_to_add += "<tbody><tr><th scope='row' align='left' colspan='2' style='font-size:18px; font-weight:bold; border-bottom:solid 1px #999999'>File Summary</th></tr>";

        $.each(val, function (product_key, product_val) {
            if (product_key != "publicationName" && product_key != "actionToTake" && product_key != "fileServiceDetails" && product_key != "actionStatus") {
                control_to_add += "<tr><th scope='row' align='left' colspan='2' style='font-size:16px; font-weight:bold; border-bottom:solid 1px #999999'>" + fnFileName(product_key) + "</th></tr>";

                if (product_key.indexOf("Dict") != -1) {
                    row_count = 0;
                    var qa_failed_stat = false;
                    $.each(product_val, function (key_grid, val_grid) {
                        style_row = (row_count % 2 == 0) ? "even" : "odd";
                        if (key_grid == "Service Levels") {
                            control_to_add += "<tr><td colspan='2' align ='left' style='font-size:16px;font-weight:bold;'>" + key_grid + "</td></tr>";
                            row_count = 0;
                        }
                        else {
                            if (key_grid.toLowerCase().indexOf('_message') == -1) {
                                var failed_style = ((val_grid.toLowerCase().indexOf('failed') > -1) ? ' style="color:#900; font-weight:bold;"' : '');
                                control_to_add += "<tr class=" + style_row + "><td align ='left' style='padding-left:24px'>" + key_grid + "</td><td align='right'" + failed_style + ">" + ((val_grid == "") ? "0" : val_grid) + "</td></tr>";
                                if (failed_style != '')
                                    qa_failed_stat = true;
                            }
                            if (key_grid.toLowerCase().indexOf('_message') > -1 && qa_failed_stat) {
                                control_to_add += "<tr class=" + style_row + "><td align ='left' style='padding-left:24px;color:#900;font-weight:bold;' colspan='2'>" + val_grid + "</td></tr>";
                                qa_failed_stat = false;
                            }
                            row_count++;
                        }
                    });
                }
                    //else if (product_key == "circulationStatus" || product_key == "currentBuyStatus" || product_key == "projectionsStatus") {
                else if (product_key == "publicationStatus") {
                    var is_approve_checked = ' ';
                    var is_rejected_checked = ' ';
                    radio_name = 'rdo' + val.publicationName.replace(/ /g, "") + product_key.replace(/ /g, "");
                    control_to_add += "<tr><td  scope='row' align='left' colspan='1' width='75%'><fieldset data-role='controlgroup' width='100%'  data-mini='true'>";
                    if (product_val.approvalStatus == 1) {
                        is_approve_checked = ' checked ';
                        is_rejected_checked = ' ';
                    }
                    else if (product_val.approvalStatus == -1) {
                        is_approve_checked = ' ';
                        is_rejected_checked = ' checked ';
                    }
                    count++;
                    control_to_add += "<input type='radio' name='" + radio_name + "' id='" + radio_name + "Approve'" + is_approve_checked + " value='choice-1' width='100%' onclick='clickedApprovalOrReject(this,\"" + product_key + "\",\"" + val.publicationName + "\",\"" + radio_name + "\");'/><label for='" + radio_name + "Approve'>Approve</label>";
                    control_to_add += "<input type='radio' name='" + radio_name + "' id='" + radio_name + "Reject'" + is_rejected_checked + " value='choice-2' width='100%' onclick='clickedApprovalOrReject(this,\"" + product_key + "\",\"" + val.publicationName + "\",\"" + radio_name + "\");'/><label for='" + radio_name + "Reject'>Reject</label>";
                    control_to_add += "</fieldset></td><td align='right' valign='top' width='25%'><a href='#' data-theme='b' data-role='button' data-inline='true' data-mini='true'>Submit</a><a href='#' data-theme='b' data-role='button' data-inline='true' data-mini='true' onclick='clearApprovals(\"" + val.publicationName + "\",\"" + product_key + "\",\"" + radio_name + "\");'>Clear</a></td></tr>";
                    if (product_val.approvalStatus == -1)
                        show_text_area = 'block';
                    else
                        show_text_area = 'none';

                    control_to_add += "<tr><td colspan='2' align='left'> <div style='display:" + show_text_area + ";' name='div" + radio_name + "' id='div" + radio_name + "' data-role='fieldcontain'>Reasons for Rejection:<label for='div" + radio_name + "'></label></br>";
                    control_to_add += "<textarea name='ta" + radio_name + "' id='ta" + radio_name + "' data-mini='true' >" + product_val.reasonForRejection + "</textarea></div></td></tr>";
                    if (product_val.approvalStatus === 1 || product_val.approvalStatus === -1)
                        control_to_add += "<tr><td align ='left' colspan='2'>" + ((product_val.approvalStatus === 1) ? "Approved" : "Rejected") + " by " + product_val.approvalEmail + " on " + product_val.approvalDate + "</td></tr>";
                }
                control_to_add += "<tr><td colspan='2'>&nbsp;</td></tr>";
            }
        });
        control_to_add += "</tbody> </table></div>";
        control_to_add += "</div></div>";
        $('#divList').append(control_to_add);
        if (val.actionStatus === 0) {
            $('#div' + val.publicationName.replace(/ /g, "")).find("*").click(function (e) {
                e.stopImmediatePropagation();
                e.preventDefault();
            });
        }
    });

    $('.ui-page').trigger('create');
}

function bindEmailDropDown() {
    var ddl_json = [{ 'text': 'Select', 'value': 'select' }];
    $.each(gData.emailSelectList, function (a, b) {
        ddl_json.push({
            "text": b,
            "value": b
        });
    });
    $("#dropDownTemplate").tmpl(ddl_json).appendTo("#ddlEmail");
    $('#ddlEmail').val('select').selectmenu('refresh');
}

function fnFileName(file_name) {
    switch (file_name) {
        case "qaResultsDict":
            return "QA Results";
            break;
        case "circulationDict":
            return "Circ File";
            break;
        case "currentBuyDict":
            return "Current Buy";
            break;
        case "projectionsDict":
            return "Projections";
            break;
        case "publicationStatus":
            return "Publication Status";
            break;
    }
}

//when user clicks on "approve" or "reject" radion buttions, make sure we update the gData with "true/false" value.
function clickedApprovalOrReject(ctrl, title, radio_name) {
    var gdata_publication = jQuery.grep(gData.approvalPublications, function (obj, key) {
        return obj.publicationName.toLowerCase() === title.toLowerCase();
    });

    if (ctrl.id.indexOf('Approve') > -1) {
        gdata_publication[0].publicationStatus.approvalStatus = 1;
        $('#tr' + radio_name).css("display", "none");
    } else {
        gdata_publication[0].publicationStatus.approvalStatus = -1;
        gdata_publication[0].publicationStatus.reasonForRejection = "";
        $('#tr' + radio_name).css("display", "block");
        $('#ta' + radio_name).val('');
    }
    var div_id = $('#thetable' + title.replace(/ /g, "")).parent();
    $('div.ui-collapsible-content', '#div' + title.replace(/ /g, "")).trigger('expand');
}

function clearApprovals(title, radio_name) {
    var gdata_publication = jQuery.grep(gData.approvalPublications, function (obj, key) {
        return obj.publicationName.toLowerCase() === title.toLowerCase();
    });

    gdata_publication[0].publicationStatus.approvalStatus = 0;
    gdata_publication[0].publicationStatus.reasonForRejection = "";
    gdata_publication[0].publicationStatus.approvalEmail = "";
    gdata_publication[0].publicationStatus.approvalDate = "";
    $('#divList').empty();
    ko.cleanNode($('#divList')[0]);
    var model = createViewModel();
    ko.applyBindings(model, $('#divList')[0]);
    $.each($('#divList'), function (obj) {
        $(this).collapsibleset();
    });
    $("#divList ul").each(function (i) {
        $(this).listview();
    });
    $('.ui-page').trigger('create');
    $('div[id^=div]:[data-theme=c]').find("*").click(function (e) {
        e.stopImmediatePropagation();
        e.preventDefault();
    });
    var div_id = $('#thetable' + title.replace(/ /g, "")).parent();
    $('div.ui-collapsible-content', '#div' + title.replace(/ /g, "")).trigger('expand');
}

function emailValidation() {
    var message = "";
    if ($('#ddlEmail').val() == "select") {
        message = "Please selecet an email";
        $('#alertmsg').text(message);
        $('#popupDialog').popup('open');
        return false;
    }
    return true;
}
function fnSubmit() {
    if (!emailValidation()) return false;

    //var mw_json = gData;
    //var post_url = serviceURLDomain + "api/ApprovalMw/" + getSessionData("publisherId") + "/bob";

    //postCORS(post_url, JSON.stringify(mw_json), function (response) {
    //    //makeGData();
    //    var msg = 'Data was saved.';
    //    if (response.replace(/"/g, "") != "success") {
    //        msg = "Data was not saved.";
    //    }
    //    $('#alertmsg').text(msg);
    //    $('#popupDialog').popup('open');
    //}, function (response_error) {
    //    $('#alertmsg').text(response_error.responseText);
    //    $('#popupDialog').popup('open');
    //});
}
//******************** Public Functions End **************************