﻿var getReportingService = serviceURLDomain + "api/JobTicket_reporting_action_get/" + sessionStorage.facilityId + "/" + jobCustomerNumber + "/" + sessionStorage.jobNumber;
var saveReportingService = serviceURLDomain + "api/JobTicket_reporting_action_save/" + sessionStorage.facilityId + "/" + jobCustomerNumber + "/" + sessionStorage.jobNumber + "/";

var mapping = { 'ignore': ["__ko_mapping__"] }
var jobDesc = '';
jobDesc = getSessionData("jobDesc");
var reportsInfo = [];
var facilities = [];
var customers = [];
var default_dropdown_option = {
    "label": "",
    "name": "",
    "disable": "0",
    "selected": "0"
};
var reportsContent =
    {
        "reportParamList": [],
        "outputType": {
            "reportOptionList": [{
                "label": "PDF",
                "name": "pdf",
                "disable": 0,
                "selected": 1
            }, {
                "label": "Excel",
                "name": "excel",
                "disable": 0,
                "selected": 0
            }, {
                "label": "Windows Word Doc",
                "name": "doc",
                "disable": 0,
                "selected": 0
            }, {
                "label": "CSV",
                "name": "csv",
                "disable": 0,
                "selected": 0
            }],
            "label": "Output Type",
            "name": "outputType",
            "controlType": "dropdown"
        },
        "availability": {
            "super": 0,
            "admin": 0,
            "power": 0,
            "user": 0,
            "basic": 0
        },
        "name": "",
        "ssrsName": "",
        "ssrsDirectory": "Gizmo/%isPrdOrStage%",
        "created": null,
        "createdBy": sessionStorage.username
    };

// LOADS CUSTOMERS
getCORS(serviceURLDomain + "api/Customer/", null, function (data) {
    var temp = [];
    $.each(data, function (key, val) {
        temp.push({
            "name": val.customerName,
            "value": val.customerNumber
        });
    });
    customers = temp;
}, function (error_response) {
    showErrorResponseText(error_response, true);
});
$.getJSON("../JSON/_facilityList.JSON", function (data) {
    var temp = [];
    $.each(data.facility, function (key, val) {
        if (key < 4) {
            if (val.facilityName == "Chicago") {
                val.facilityName = "Northlake";
            }
            temp.push({
                "name": val.facilityName,
                "value": val.facilityValue
            });
        }
    });
    temp.push({
        "name": 'None',
        "value": '0'
    });
    facilities = temp;
});

// LOADS CUSTOMERS
getCORS(getReportingService, null, function (data) {
    if (data.reports == undefined || data.reports == null) {
        var temp = {};
        temp["reportList"] = [];
        data.reports = temp;
    }
    reportsInfo = data;
}, function (error_response) {
    showErrorResponseText(error_response, true);
});

//$.getJSON('JSON/_jobReporting.JSON', function (result) {
//    reportsInfo = result;
//});

$('#_jobReporting').live('pagebeforecreate', function (event) {
    displayMessage('_jobReporting');
    createConfirmMessage('_jobReporting');
    ko.setTemplateEngine(new ko.nativeTemplateEngine);
    var neg_job_number = getSessionData("negJobNumber");
    var header_text = jobDesc + ' | ' + sessionStorage.jobNumber;
    if (neg_job_number != undefined && neg_job_number != null && neg_job_number != "")
        header_text += ' (' + neg_job_number + ')';
    $('#navHeader').html('<h4 style="margin-top:10px !important;margin-bottom:10px !important;">' + header_text + '</h4>');
});

$(document).on('pageshow', '#_jobReporting', function (event) {
    window.setTimeout(function () {
        var pageObj = new jobReporting();
        ko.applyBindings(pageObj);
        pageObj.refreshCollapseSet();
        if (reportsInfo.reports.reportList.length > 0) {
            $('div[data-role=collapsible-set]').css('display', 'block');
            window.setTimeout(function () {
                $.each(reportsInfo.reports.reportList, function (key, val) {
                    if ($.browser.msie)
                        $("#ddlSsrsName").children("option[value=" + val.ssrsName + "]").prop('disabled', true);
                    else
                        $('#ddlSsrsName option[value=' + val.ssrsName + ']').css("display", "none");
                });
                if (reportsInfo.reports.reportList.length == 1) {
                    $('#div' + reportsInfo.reports.reportList[0].name.replace(/ /g, '_')).collapsible('expand');
                }
                //var txtCustIdCtrls = $('input[type=text][id*=CustomerId]');
                var txtCustIdCtrls = $('#popUpEditParamter input[type=text][id=txtValue]');
                pageObj.bindAutoComplete(txtCustIdCtrls, true);
            }, 500);
        }
    }, 1000);
    $('div[data-role=popup] div[data-role=header]').removeClass('ui-corner-top ui-header ui-bar-d ui-header-fixed slidedown ui-panel-fixed-toolbar ui-panel-animate ui-panel-page-content-position-left ui-panel-page-content-display-push ui-panel-page-content-open');
    $('div[data-role=popup] div[data-role=header]').addClass('ui-corner-top ui-header ui-bar-d');
});

var dropdownOptionModel = function (option) {
    var self = this;
    self.disable = ko.observable(option.disable);
    self.selected = ko.observable(option.selected);
    self.label = ko.observable(option.label);
    self.name = ko.observable(option.name);
};


var jobReporting = function () {
    var self = this;
    self.selectedReport = ko.observable({});
    self.reports = ko.observableArray();
    self.selectedReportParameter = ko.observable({});
    self.prevSelectedReportParameter = ko.observable({});
    self.paramRemovalContext = {};
    self.attachedOptionList = ko.observableArray([]);
    self.dropdownOption = ko.observable({});
    self.paramAvailability = ko.observable({});
    $.each(reportsInfo.reports.reportList, function (key, val) {
        var tmp = {};
        ko.mapping.fromJS(val, {}, tmp);
        tmp.formattedReportOptions = ko.computed(function () {
            var formatted_optionlist = "";
            $.each(tmp.outputType.reportOptionList(), function (key, val) {
                if (val.disable() != "1") {
                    formatted_optionlist = ((formatted_optionlist != "") ? formatted_optionlist + ', ' + val.label() : val.label());
                }
            });
            return formatted_optionlist;
        });
        self.reports.push(tmp);
    });

    self.addClick = function () {
        //var temp = {
        //    "report": "",
        //    "reportData": reportsContent
        //};
        var tmp = {};//ko.observable({});
        ko.mapping.fromJS(reportsContent, {}, tmp);
        self.selectedReport(tmp);
        //self.clearFields();
        $('#popUpReport').popup('open');
        //$('select').selectmenu('refresh').trigger('change');
    }

    self.addDropdownOption = function (data, e) {
        var msg = self.validateDropdownOption();
        if (msg != "") {
            $('#popUpEditParamter').popup('close');
            window.setTimeout(function () {
                $('#alertmsg').html(msg);
                $('#okBut').unbind('click');
                $('#okBut').bind('click', function () {
                    $('#okBut').unbind('click');
                    $('#popupDialog').popup('close');
                    window.setTimeout(function () {
                        $('#popUpEditParamter').popup('open');
                    }, 300);
                });
                $('#popupDialog').popup('open');
            }, 200);
            return false;
        }
        else {
            self.attachedOptionList.push(self.dropdownOption());
            self.resetDropdownOption();
            $('ul[id*=dropdownOptionList]').listview('refresh');
        }
    };

    self.dropdownOptionSelected = function (data, e) {
        self.resetDropdownOption();
        self.dropdownOption(data);
    };

    self.addNewDropdownOption = function (data, e) {
        var tmp = {};
        ko.mapping.fromJS(default_dropdown_option, {}, tmp);
        self.selectedReportParameter().controlOptionList.push(tmp);
        $('ul[id*=dropdownOptionList]').listview('refresh');

        $('select[data-role=slider]').slider().slider('refresh');
        $('div[data-role=collapsible-set] div[data-role=collapsible]').trigger('create');

        $('div[data-role=collapsible-set]').collapsibleset('refresh');
        window.setTimeout(function () {
            if (self.selectedReportParameter().controlOptionList().length > 0) {
                var last_index = self.selectedReportParameter().controlOptionList().length - 1;
                $($('#popUpEditParamter div[data-role=collapsible-set] div[data-role=collapsible]')[0]).trigger('collapsibleexpand');;
            }
        }, 500);
    };

    self.removeDropdownOption = function (data, e) {
        var ele = e.target || e.currentTarget;
        if (self.attachedOptionList().length > 0) {
            self.attachedOptionList.remove(data);
        }
        else {
            self.selectedReportParameter().controlOptionList.remove(data);
        }

        $('ul[id*=dropdownOptionList]').listview('refresh');
        self.refreshCollapseSet();
    };

    self.resetDropdownOption = function () {
        self.dropdownOption({});
        var tmp = {};
        ko.mapping.fromJS(default_dropdown_option, {}, tmp);
        self.dropdownOption(tmp);
        $('select[data-role=slider]').slider().slider('refresh');
    };

    self.controlTypeChanged = function (data, e) {
        var msg = "";
        var current_element = e.target || e.targetElement || e.currentTarget;
        self.resetDropdownOption();
        $('#dvParameterDefaultValue').hide();
        $('#dvDropdownOptionList').hide();
        switch ($(current_element).val()) {
            case "text":
            case "hidden":
                $('#dvParameterDefaultValue label[for="txtValue"]').text('Default Value:');
                $('#dvParameterDefaultValue').show();
                break;
            case "dropdown":
                //var temp = ko.mapping.toJS(self.selectedReportParameter, mapping);
                self.selectedReportParameter()["controlOptionList"] = ko.observableArray([]);
                //temp["controlOptionList"] = [];
                //self.selectedReportParameter({});
                //ko.mapping.fromJS(temp, {}, self.selectedReportParameter);
                $('#dvDropdownOptionList').show();
                break;
            case "jobTicket":
            case "":
                $('#dvParameterDefaultValue [id="txtValue"]').val('');
                if (self.selectedReportParameter().value != undefined)
                    self.selectedReportParameter().value('');
                break;
            default:
                $('dvControlType').val('').selectmenu('refresh');
                break;
        }
        $('dvControlType').selectmenu('refresh');
    };

    self.saveToJob = function (data, e) {
        self.saveReportingInfo(false, data, e);
    };

    self.saveReportingInfo = function (can_save_to_template, data, e) {
        var temp_data = ko.mapping.toJS(self.reports, mapping);
        var msg = self.validateReportData(data, e);
        if (msg != "") {
            window.setTimeout(function () {
                $('#alertmsg').html(msg);
                $("#okBut").attr("onclick", "$('#popupDialog').popup('close');");
                $('#popupDialog').popup('open');
            }, 100);
            return false;
        }
        else {
            var temp = self.reports();
            var tmp_list = ko.mapping.toJS(temp, mapping);
            reportsInfo.reports.reportList = tmp_list;
            reportsInfo = DeepTrim(reportsInfo);
            postCORS(saveReportingService + can_save_to_template, JSON.stringify(reportsInfo), function (reporting_data) {
                $('#alertmsg').text("Reporting has been saved successfully.");
                window.setTimeout(function () {
                    $('#okBut').unbind('click');
                    $('#okBut').bind('click', function () {
                        $('#okBut').unbind('click');
                        $('#popupDialog').popup('close');
                    });
                    $('#popupDialog').popup('open');
                }, 1000);
            }, function (error_response) {
                showErrorResponseText(error_response, true);
                self.addedComments = [];
            });
        }
    };

    self.saveToTemplate = function (data, e) {

        $('#confirmMsg').html("Click \"Ok\" to save BOTH this job, and the parent template for this job.");
        $('#okButConfirm').bind('click', function () {
            $('#okButConfirm').unbind('click');
            $('#popupConfirmDialog').popup('close');
            self.saveReportingInfo('saveTemplate', data, e);
        });
        $('#popupConfirmDialog').popup('open');
    };

    self.DeepTrim = function (obj) {
        for (var prop in obj) {
            var value = obj[prop], type = typeof value;
            if (value != null && (type == "string" || type == "object") && obj.hasOwnProperty(prop)) {
                if (type == "object") {
                    self.DeepTrim(obj[prop]);
                } else {
                    obj[prop] = obj[prop].trim();
                }
            }
        }
        //$.each(obj, function (index) {
        //    var that = this;
        //    if (typeof that === 'object') {
        //        $.each(that, function (key, value) {
        //            var newKey = $.trim(key);

        //            if (typeof value === 'string') {
        //                that[newKey] = $.trim(value);
        //            }

        //            if (newKey !== key) {
        //                delete that[key];
        //            }
        //        });
        //    }
        //});
        return obj;
    }
    self.saveNewTemplateClick = function (data, e) {
        var temp_data = ko.mapping.toJS(self.reports, mapping);
        var msg = self.validateReportData(data, e);
        if (msg != "") {
            window.setTimeout(function () {
                $('#alertmsg').html(msg);
                $("#okBut").attr("onclick", "$('#popupDialog').popup('close');");
                $('#popupDialog').popup('open');
            }, 100);
            return false;
        }
        else {
            window.setTimeout(function () {
                $('#popUpNewTemplate').popup('open');
            }, 200);
        }
    };

    self.saveAsNewTemplate = function (data, e) {
        window.setTimeout(function () {
            $('#popUpNewTemplate').popup('close');
        }, 200);
    };

    self.editReportParameter = function (data, e) {
        var current_element = e.target || e.targetElement || e.currentTarget;
        var parent_context = ko.contextFor(current_element).$parent;
        self.paramRemovalContext = parent_context;
        if ($(current_element).attr('data-icon') == 'delete') {
            self.selectedReportParameter(data);
            self.prevSelectedReportParameter(ko.mapping.toJS(data, mapping));
            self.deleteParameterMenuClick(data, e);
            return false;
        }
        else {
            var parameter_type = $(current_element).attr('data-paramtype');
            var popup_identifier = 'popUpEditParamter';
            parameter_type = (parameter_type == undefined || parameter_type == null || parameter_type == "") ? data.controlType() : parameter_type;
            self.selectedReportParameter({});
            self.prevSelectedReportParameter({});
            $('#dvParameterDefaultValue label[for="txtValue"]').text('Default Value:');
            switch (parameter_type) {
                case "output":
                    self.selectedReportParameter(data.outputType);
                    self.prevSelectedReportParameter(ko.mapping.toJS(data.outputType, mapping));
                    $('select[data-role=slider]').slider().slider('refresh');
                    $('div[data-role=collapsible-set] div[data-role=collapsible]').trigger('create');
                    $('div[data-role=collapsible-set] div[data-role=collapsible]').collapsible();
                    $('div[data-role=collapsible-set]').collapsibleset('refresh');
                    popup_identifier = 'popUpEditOptionsParamter';
                    $('#dvNewOption').css('display', 'none');
                    $('#btnAddNewOption').css('display', 'none');
                    break;
                case "dropdown":
                    self.selectedReportParameter(data);
                    self.prevSelectedReportParameter(ko.mapping.toJS(data, mapping));
                    $('select[data-role=slider]').slider().slider('refresh');
                    $('div[data-role=collapsible-set] div[data-role=collapsible]').trigger('create');
                    $('div[data-role=collapsible-set] div[data-role=collapsible]').collapsible();
                    $('div[data-role=collapsible-set]').collapsibleset('refresh');
                    $('#popUpEditParamter h3').text('Edit Parameter');
                    $('ul[id*=updateOptionList]').listview('refresh');
                    $('select[data-role=slider]').slider().slider('refresh');
                    $('#btnUpdateReportParameter').text('Update');
                    $('#dvNewOption').css('display', 'block');
                    $('#btnAddNewOption').css('display', 'block');
                    break;
                case "availability":
                    self.paramAvailability(data.availability);
                    self.prevSelectedReportParameter(ko.mapping.toJS(data.availability, mapping));
                    $('select[data-role=slider]').slider().slider('refresh');
                    popup_identifier = 'popUpEditRoleParamter';
                    $('#dvNewOption').css('display', 'none');
                    $('#btnAddNewOption').css('display', 'none');
                    break;
                default:
                    self.selectedReportParameter(data);
                    self.prevSelectedReportParameter(ko.mapping.toJS(data, mapping));
                    $('#popUpEditParamter [id="dvControlType"]').hide();
                    if (data.controlType().toLowerCase() == 'jobticket' || data.controlType().toLowerCase() == 'select') {
                        $('#dvParameterDefaultValue [id="txtValue"]').val('');
                        $('#dvParameterDefaultValue').hide();
                    }
                    else {
                        $('#dvParameterDefaultValue').show();
                    }
                    $('#popUpEditParamter h3').text('Edit Parameter');
                    $('#btnUpdateReportParameter').text('Update');
                    $('select[data-role=slider]').slider().slider('refresh');
                    $('#dvNewOption').css('display', 'none');
                    $('#btnAddNewOption').css('display', 'none');
                    break;
            }
            window.setTimeout(function () {
                $('#' + popup_identifier).popup('open');
            }, 200);
        }
    };

    self.editParameterMenuClick = function (data, e) {
        $('#popupUpdateParameter').popup('close');
        $('#popUpEditParamter h3').text('Edit Parameter');
        $('#btnUpdateReportParameter').text('Update');
        $('select[data-role=slider]').slider().slider('refresh');
        window.setTimeout(function () {
            $('#popUpEditParamter').popup('open');
        }, 200);
    };

    self.deleteParameterMenuClick = function (data, e) {
        self.paramRemovalContext.reportParamList.remove(self.selectedReportParameter());
        $('#popupUpdateParameter').popup('close');
        window.setTimeout(function () {
            self.paramRemovalContext = {};
            self.selectedReportParameter({});
            self.prevSelectedReportParameter({});
            //$('#alertmsg').html("Report parameter deleted successfully.");
            //$("#okBut").attr("onclick", "$('#popupDialog').popup('close');");
            //$('#popupDialog').popup('open');
        }, 200);
    };

    self.cancelReportParameter = function (data, e) {
        var current_element = e.target || e.targetElement || e.currentTarget;
        var parameter_type = $(current_element).attr('data-paramtype');
        var popup_to_close = "";

        switch (parameter_type) {
            case "output":
                popup_to_close = "popUpEditOptionsParamter";
                $('#' + popup_to_close).popup('close');
                ko.mapping.fromJS(self.prevSelectedReportParameter(), {}, self.selectedReportParameter());
                $('select[data-role=slider]').slider().slider('refresh');
                $('div[data-role=collapsible-set] div[data-role=collapsible]').trigger('create');
                $('div[data-role=collapsible-set] div[data-role=collapsible]').collapsible();
                $('div[data-role=collapsible-set]').collapsibleset('refresh');

                break;
            case "availability":
                popup_to_close = "popUpEditRoleParamter";
                $('#' + popup_to_close).popup('close');
                ko.mapping.fromJS(self.prevSelectedReportParameter(), {}, self.paramAvailability());
                break;
            default:
                popup_to_close = "popUpEditParamter";
                $('#' + popup_to_close).popup('close');
                ko.mapping.fromJS(self.prevSelectedReportParameter(), {}, self.selectedReportParameter());
                break;
        }
    };

    self.updateReportParameter = function (data, e) {
        var current_element = e.target || e.targetElement || e.currentTarget;
        var parameter_type = $(current_element).attr('data-paramtype');
        var operation_type = $(current_element).text().toLowerCase();

        var popup_to_close = "";
        switch (parameter_type) {
            case "output":
                popup_to_close = "popUpEditOptionsParamter";
                break;
            case "availability":
                popup_to_close = "popUpEditRoleParamter";
                break;
            default:
                popup_to_close = "popUpEditParamter";
                break;
        }
        if (parameter_type != "availability") {
            var msg = self.validateAddOrUpdateReportParameter(operation_type, parameter_type);
            if (msg != "") {
                $('#' + popup_to_close).popup('close');
                if (operation_type != 'add') {
                    ko.mapping.fromJS(self.prevSelectedReportParameter(), {}, self.selectedReportParameter());
                }

                window.setTimeout(function () {
                    $('#alertmsg').html(msg);
                    $('#okBut').unbind('click');
                    $('#okBut').bind('click', function () {
                        $('#okBut').unbind('click');
                        $('#popupDialog').popup('close');
                        $('select[data-role=slider]').slider().slider('refresh');
                        $('div[data-role=collapsible-set] div[data-role=collapsible]').trigger('create');
                        $('div[data-role=collapsible-set] div[data-role=collapsible]').collapsible();
                        $('div[data-role=collapsible-set]').collapsibleset('refresh');
                        window.setTimeout(function () { $('#' + popup_to_close + '').popup('open'); }, 300);
                    });
                    //$("#okBut").attr("onclick", "$('#popupDialog').popup('close'); window.setTimeout(function(){ $('#" + popup_to_close + "').popup('open'); }, 300);");
                    $('#popupDialog').popup('open');
                }, 200);
                return false;
            }
        }
        if (operation_type == 'add')
            self.addReportParameter(data, e);
        else {
            $('#' + popup_to_close).popup('close');
            window.setTimeout(function () {
                self.selectedReportParameter({});
                self.prevSelectedReportParameter({});
            }, 200);
        }
    };

    self.addReportClick = function () {
        var tmp = ko.mapping.toJS(self.selectedReport, mapping);
        var msg = self.validateNewReport();
        if (msg != "") {
            $('#popUpReport').popup('close');
            window.setTimeout(function () {
                $('#alertmsg').html(msg);
                $("#okBut").attr("onclick", "$('#popupDialog').popup('close'); window.setTimeout(function(){ $('#popUpReport').popup('open'); }, 300);");
                $('#popupDialog').popup('open');
            }, 200);
            return false;
        }
        var tmp1 = {};
        ko.mapping.fromJS(tmp, {}, tmp1);
        tmp1.formattedReportOptions = ko.computed(function () {
            var formatted_optionlist = "";
            $.each(tmp1.outputType.reportOptionList(), function (key, val) {
                if (val.disable() != "1") {
                    formatted_optionlist = ((formatted_optionlist != "") ? formatted_optionlist + ', ' + val.label() : val.label());
                }
            });
            return formatted_optionlist;
        });
        self.reports.push(tmp1);
        self.selectedReport({});
        $('div[data-role=collapsible-set]').css('display', 'block');
        $('#popUpReport').popup('close');
        self.refreshCollapseSet();

        if ($.browser.msie)
            $("#ddlSsrsName option[value=" + tmp.ssrsName + "]").prop('disabled', true);
        else
            $('#ddlSsrsName option[value=' + tmp.ssrsName + ']').css("display", "none");
    }

    //Rediret to System Dashboard.
    self.redirectToPages = function (data, e) {
        var ele = e.target || e.targetElement;
        switch ($(ele).text()) {
            case "Notifications":
                window.setTimeout(function () {
                    window.location.href = '../jobNotifications/jobNotifications.html';
                }, 500);
                break;
            case "Logging":
                window.setTimeout(function () {
                    window.location.href = '../jobLogging/jobLogging.html';
                }, 500);
                break;
            case "Reporting":
                window.setTimeout(function () {
                    window.location.href = '../jobReporting/jobReporting.html';
                }, 500);
                break;
            default:
                break;
        }

    };

    self.removeReportClick = function (data, el) {
        var msg = "Do you want to remove this report?";
        $('#confirmMsg').html(msg);
        $('#okButConfirm').bind('click', function () {
            self.reports.remove(data);
            if ($.browser.msie)
                $("#ddlSsrsName option[value=" + data.ssrsName() + "]").prop('disabled', false);
            else
                $('#ddlSsrsName option[value=' + data.ssrsName() + ']').css("display", "block");
            $('#okButConfirm').unbind('click');
            $('#popupConfirmDialog').popup('close');
            self.refreshCollapseSet();
        });
        $('#popupConfirmDialog').popup('open');
    }

    self.addReportParameterClick = function (data, e) {
        var current_element = e.target || e.targetElement || e.currentTarget;
        self.selectedReportParameter({});
        self.prevSelectedReportParameter({});
        self.attachedOptionList([]);
        $('#popUpEditParamter [id="dvControlType"]').show();
        var temp_data = {
            "label": "",
            "name": "",
            "value": "",
            "controlType": ""
        };
        $('#dvNewOption').css('display', 'none');
        $('#btnAddNewOption').css('display', 'none');
        var tmp = {};
        ko.mapping.fromJS(temp_data, {}, tmp);
        self.selectedReportParameter(tmp);
        self.prevSelectedReportParameter(ko.mapping.toJS(tmp, mapping));
        self.paramRemovalContext = data;
        $('#btnUpdateReportParameter').text('Add');
        //$('#dvControlType [id="ddlControlType"]').selectmenu();
        //$('#dvParameterDefaultValue').show();
        $('#popUpEditParamter h3').text('Add Parameter');
        window.setTimeout(function () {
            $('#popUpEditParamter').popup('open');
        }, 200);
    };

    self.addReportParameter = function (data, e) {
        if (self.selectedReportParameter().controlType() == "dropdown") {
            var options = ko.mapping.toJS(self.attachedOptionList, mapping);            
            self.selectedReportParameter().controlOptionList([]);
            var selected_report_param = ko.mapping.toJS(self.selectedReportParameter, mapping);
            selected_report_param["controlOptionList"] = [];
            $.each(self.attachedOptionList(), function (key, val) {
                var tmp = ko.mapping.toJS(val, mapping);
                selected_report_param.controlOptionList.push(tmp);
            });

            var temp = {};
            ko.mapping.fromJS(selected_report_param, {}, temp);

            temp.formattedControlOptions = ko.computed(function () {
                var formatted_optionlist = "";
                $.each(temp.controlOptionList(), function (key, val) {
                    formatted_optionlist = ((val.disable() != "1") ? ((formatted_optionlist != "") ? formatted_optionlist + ', ' + val.label() : val.label()) : formatted_optionlist);
                });
                return formatted_optionlist;
            });

            self.paramRemovalContext.reportParamList.push(temp);
            delete self.paramRemovalContext.reportParamList()[self.paramRemovalContext.reportParamList().length - 1].value;
            delete self.paramRemovalContext.reportParamList()[self.paramRemovalContext.reportParamList().length - 1].__ko_mapping__;
        }
        else {
            self.paramRemovalContext.reportParamList.push(self.selectedReportParameter());
        }
        $('#popUpEditParamter').popup('close');
        window.setTimeout(function () {
            $('ul[data-role=listview]').listview('refresh');
            self.paramRemovalContext = {};
            self.selectedReportParameter({});
            self.prevSelectedReportParameter({});
            self.attachedOptionList([]);
            //$('#alertmsg').html("Report parameter added successfully.");
            //$("#okBut").attr("onclick", "$('#popupDialog').popup('close');");
            //$('#popupDialog').popup('open');
        }, 200);
    };

    self.showHomePage = function () {
        //Need to call the function
        redirectToIndexPage();
    }

    self.showJobSummary = function () {
        //Need to call the function
        redirectToSingleJobPage();
    }

    self.refreshCollapseSet = function () {
        $('div[data-role=collapsible-set] div[data-role=collapsible]').trigger('create');
        $('div[data-role=collapsible-set] div[data-role=collapsible]').collapsible();
        $('ul[data-role=listview]').listview('refresh');
        $('div[data-role=collapsible-set]').collapsibleset('refresh');
        window.setTimeout(function () {
            $('#dvReportList').find('.ui-collapsible-content').css('padding', '0px');
            $('ul[data-role=listview]').css('border-radius', '0');
        }, 100);
    };

    self.bindAutoComplete = function (controls, isCustomer) {
        var data_obj = isCustomer ? customers : facilities;
        $(controls).autocomplete({
            source: function (request, response) {
                response($.map(data_obj, function (item) {
                    //if (item.toLowerCase().indexOf((isCustomer?'CustomerId':'FacilityId')).val().toLowerCase()) > -1) {
                    if (isCustomer && item.value.indexOf(request.term) > -1)
                        return item;
                    else if (item.value.indexOf(request.term) > -1)
                        return item;
                    //}
                }));
            },
            minLength: isCustomer ? 3 : 1,
            select: function (event, ui) {
                //var disp_name = getDisplayLabel(control_name.replace('txt','')).replace(/ /g, '_');
                //$("#" + control_name).val('');
                //if (control_name.indexOf('CustomerId') > -1){
                //    self.selectedClient.push(ui.item.label);
                //}
                //else if (control_name.indexOf('txtAccountManager') > -1){
                //    self.selectedAccountMgr.push(ui.item.label);
                //}
                //else
                //    self.selectedSales.push(ui.item.label);
                //self.search();
                //$('#ul' + disp_name).listview('refresh');
                //return false;
            },
            response: function (event, ui) {
                if (ui.content.length === 0) {
                    $('#error-message').css("visibility", "visible");
                } else {
                    $('#error-message').css("visibility", "hidden");
                }
            }
        });
    };

    /**************** Validations ************************/
    self.validateInputValue = function (data, event) {
        var ele = event.target || event.currentTarget;
        if ($(ele).val() == "") {
            $('#popUpEditOptionsParamter').popup('close');
            ko.mapping.fromJS(self.prevSelectedReportParameter(), {}, self.selectedReportParameter());
            $('select[data-role=slider]').slider().slider('refresh');
            $('div[data-role=collapsible-set] div[data-role=collapsible]').trigger('create');
            $('div[data-role=collapsible-set] div[data-role=collapsible]').collapsible();
            $('div[data-role=collapsible-set]').collapsibleset('refresh');
            window.setTimeout(function () {
                $('#alertmsg').html("Value shouldn't be empty.");
                $("#okBut").attr("onclick", "$('#popupDialog').popup('close'); window.setTimeout(function(){ $('#popUpEditOptionsParamter').popup('open'); }, 300);");
                $('#popupDialog').popup('open');
            }, 200);
            return false;
        }
    }

    self.validateAddOrUpdateReportParameter = function (operation_type, parameter_type) {
        var msg = "";
        var update_failure_msg = "";
        var selected_report_parameter = ko.mapping.toJS(self.selectedReportParameter, mapping);
        if (selected_report_parameter.label.trim() == "") {
            msg = "<li>parameter label.</li>";
        }
        if (selected_report_parameter.name.trim() == "") {
            msg += "<li>parameter name.</li>";
        }

        if (selected_report_parameter.controlType == "") {
            msg += "<li>parameter type.</li>";
        }
        else if (selected_report_parameter.controlType.toLowerCase() != "jobticket") {
            if (selected_report_parameter.controlType.toLowerCase() == "dropdown") {
                if (operation_type == "add" && self.attachedOptionList().length == 0)
                    msg += "<li>parameter options.</li>";
                else if (self.attachedOptionList().length > 0 || operation_type == "update") {
                    var select_count = 0;
                    var optionList;
                    if (operation_type == "update") {
                        optionList = parameter_type == "output" ? selected_report_parameter.reportOptionList : selected_report_parameter.controlOptionList;
                    }
                    else {
                        optionList = ko.mapping.toJS(self.attachedOptionList, mapping);
                    }
                     
                    $.each(optionList, function (key, val) {
                        if (val.disable != 1) {
                            if (val.name.trim() == "")
                                update_failure_msg += "<li>Option name is required</li>";
                            if (val.label.trim() == "")
                                update_failure_msg += "<li>Option label is required</li>";

                            if (val.selected == 1) {
                                if (select_count > 0) {
                                    update_failure_msg += "<li>more than one option shouldn't be selected as default selection.</li>";
                                }
                                else {
                                    select_count = select_count + 1;
                                }
                            }
                        }
                        if (key == optionList.length - 1 && select_count == 0) {
                            update_failure_msg += "<li>one option should be selected as default selection.</li>";
                        }
                        if (update_failure_msg != "")
                            return false;
                    });
                }
            }
            //else if (self.selectedReportParameter().value() == "") {
            //    msg += "<li>parameter value.</li>";
            //}
        }

        msg = (msg != "") ? ("Default values required for <ul>" + msg + "</ul>") : msg;
        if (update_failure_msg != "")
            msg = msg + "Following need to be corrected for update parameter <ul>" + update_failure_msg + "</ul>";
        if (msg == "") {
            msg = self.checkDuplicateParamters(operation_type, selected_report_parameter);
        }
        return msg;
    };

    self.checkDuplicateParamters = function (operation_type, selected_report_parameter) {
        var msg = "";
        if (self.paramRemovalContext.reportParamList != undefined) {
            var duplicate_paramter_count = 0;
            var existing_report_paramters = ko.mapping.toJS(self.paramRemovalContext.reportParamList, mapping);
            $.each(existing_report_paramters, function (key, val) {
                if (val.name.toLowerCase().trim() == selected_report_parameter.name.toLowerCase().trim() && val.controlType == selected_report_parameter.controlType) {
                    duplicate_paramter_count = duplicate_paramter_count + 1;
                    if (operation_type == "add" || duplicate_paramter_count > 1) {
                        msg = "Duplicate parameters are not allowed in report."
                        return false;
                    }
                }
            });
        }        
        return msg;
    };

    self.validateReportData = function (data, e) {
        var temp_msg = "";
        var data_reports = ko.mapping.toJS(data.reports(), mapping);
        $.each(data_reports, function (key, val) {
            var msg = "";
            var is_output_selected = false;
            var is_userrole_selected = false;
            //$.each(val.reportParamList, function (key1, val1) {
            //    if (val1.controlType != "dropdown" && val1.controlType != "jobTicket" && val1.value == "") {
            //        msg += "<li>" + val1.label + "</li>";
            //    }
            //});
            $.each(val.outputType.reportOptionList, function (key1, val1) {
                if (val1.selected == 1) {
                    is_output_selected = true;
                    return false;
                }
            });
            if (!is_output_selected)
                msg += "<li>" + val.outputType.label + "</li>";
            $.each(val.availability, function (key1, val1) {
                if (val1 == 1) {
                    is_userrole_selected = true;
                    return false;
                }
            });
            if (!is_userrole_selected)
                msg += "<li>Availability</li>";

            temp_msg = (msg != "") ? temp_msg + ("<span style='font-weight:bold;'>" + getPageDisplayName(val.name.initCap()) + "</span><ul>" + msg + "</ul>") : temp_msg;
        });
        temp_msg = (temp_msg != "") ? "Default values are required for:<br/><br/>" + temp_msg : temp_msg;
        if (temp_msg == "" && data_reports.length == 0) {
            temp_msg = "No Reports available";
        }
        return temp_msg;
    };

    self.validateNewReport = function () {
        var msg = "";
        var selected_report = ko.mapping.toJS(self.selectedReport, mapping);
        if (selected_report.name.trim() == "") {
            msg = "<li>Please enter report name.</li>";
        }
        if (selected_report.ssrsName.trim() == "") {
            msg += "<li>Please select SSRS Name.</li>";
        }
        if (selected_report.ssrsDirectory.trim() == "") {
            msg += "<li>Please enter SSRS Directory.</li>";
        }
        msg = (msg != "") ? ("<ul>" + msg + "</ul>") : msg;
        return msg;
    };

    self.validateDropdownOption = function () {
        var msg = "";
        var dropdown_option = ko.mapping.toJS(self.dropdownOption, mapping);
        if (dropdown_option.label.trim() == "") {
            msg = "<li>Option label.</li>";
        }
        if (dropdown_option.name.trim() == "") {
            msg += "<li>Option name.</li>";
        }
        msg = (msg != "") ? ("Default Values are required for:<ul>" + msg + "</ul>") : msg;
        if (msg == "" && self.attachedOptionList().length > 0) {
            var dropdown_options = ko.mapping.toJS(self.attachedOptionList, mapping);
            $.each(dropdown_options, function (key, val) {
                if (val.selected == "1" && val.disable == "0" && dropdown_option.selected == "1" && dropdown_option.disable == "0") {
                    msg = "More than one option shouldn't be selected as default selection.";
                    return false;
                }
                else if (val.label.toLowerCase() == dropdown_option.label.toLowerCase() || val.name.toLowerCase() == dropdown_option.name.toLowerCase()) {
                    msg = "Duplicate options shouldn't be allowed.";
                    return false;
                }
            });
        }
        return msg;
    };
    /**************** Validations End************************/
}

String.prototype.initCap = function () {
    return this.replace(/(?:^|\s)[a-z]/g, function (m) {
        return m.toUpperCase();
    });
};

function getPageDisplayName(page_name) {
    var PATTERN = /^[ABCDEFGHIJKLMNOPQRSTUVWXYZ]/;
    var page_display_name = '';
    $.each(new String(page_name), function (key, value) {
        if (key > 0 && value.match(PATTERN)) {
            page_display_name += ' ' + value;
        }
        else {
            page_display_name += value;
        }
    });
    return page_display_name;
}

function allowNumbersOnly(event) {
    event.value = event.value.replace(/[^0-9]+/g, '');
}