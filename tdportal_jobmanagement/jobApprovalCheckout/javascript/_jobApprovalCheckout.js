﻿jQuery.support.cors = true;

//******************** Global Variables Start **************************
var gData;
var jobNumber = "";
var facilityId = "";
jobNumber = getSessionData("jobNumber");
facilityId = sessionStorage.facilityId;

var serviceURL = "../jobApprovalCheckout/JSON/_approvalCheckout.JSON";
var gCheckoutServiceURL = serviceURLDomain + "api/CheckOut/";
//var gCheckoutServiceURL = serviceURLDomainInternal + "api/CheckOut/";
var gOutputServiceUrl = '../JSON/_jobSetupOutput.JSON';
var gOutputData;
var postJobURL;
var PAGE_NAME = 'approvalCheckout.html';
//******************** Global Variables End **************************

//******************** Page Load Events Start **************************

$(window).on('popupbeforeposition', 'div:jqmData(role="popup")', function () {
    var notDismissible = $(this).jqmData('dismissible') === false;
    if (notDismissible) {
        $('.ui-popup-screen').off();
    }
});

$('#_jobApprovalCheckout').live('pagebeforecreate', function (event) {

    //showExecuteConfirmationToAdmin();
    displayMessage('_jobApprovalCheckout');
    displayNavLinks();
    if (sessionStorage.approvalCheckoutPrefs == undefined || sessionStorage.approvalCheckoutPrefs == null || sessionStorage.approvalCheckoutPrefs == "")
        if (jobCustomerNumber == "203" )
            getPagePreferences('ApprovalCheckout.html', sessionStorage.facilityId, CW);
        else
            getPagePreferences('ApprovalCheckout.html', sessionStorage.facilityId, jobCustomerNumber);
    else
        managePagePrefs(jQuery.parseJSON(sessionStorage.approvalCheckoutPrefs));

    //test for mobility...
    loadMobility();
    loadingImg("_jobApprovalCheckout");
    if (sessionStorage.userRole != 'admin') {
        $('#btnCountApprovalClear').hide();
        $('#btnArtApprovalClear').hide();
        $('#btnExecute').css('visibility', 'hidden');
        $('#btnExecute').css("display", "");
    }
});

$(document).on('pageshow', '#_jobApprovalCheckout', function (event) {
    makeGData();
    $('#txtStartDateInvoiceSummary').on('click change keydown', function () {  // .live() event important
        dtpThemes();
    });
    $('#txtEndDateInvoiceSummary').on('click change keydown', function () {  // .live() event important
        dtpThemes();
    });
    //enabled showing for normal user too.  It was integrated with page preference (not to show for normal user) earlier.  Whenever reposting page preferences, need to consider this change.
    $('#btnExecute').show();
    $('#btnExecute').css("display", "");
    //setGridProperties();

});

function dtpThemes() {
    $('#ui-datepicker-div').find('.ui-datepicker-header').css('border', '1px solid #333');
    $('#ui-datepicker-div').find('.ui-datepicker-header').css('background', '#333 url(../Calendar/css/ui-darkness/images/ui-bg_gloss-wave_25_333333_500x100.png) 50% 50% repeat-x');
    $('#ui-datepicker-div').find('.ui-datepicker-header').css('color', '#fff');
    $('#ui-datepicker-div').find('.ui-datepicker-header').css('font-weight', 'bold');
    $('#ui-datepicker-div').find('.ui-datepicker-other-month,.ui-datepicker-unselectable,.ui-state-disabled').css('opacity', '.35');
    $('#ui-datepicker-div').find('.ui-datepicker-other-month,.ui-datepicker-unselectable,.ui-state-disabled').css('filter', 'Alpha(Opacity=35)');
    $('#ui-datepicker-div').find('.ui-datepicker-other-month,.ui-datepicker-unselectable,.ui-state-disabled').css('background-image', 'none');
    $('#ui-datepicker-div').find('.ui-state-default').css('border', '1px solid #666');
    $('#ui-datepicker-div').find('.ui-state-default').css('background', '#555 url(../Calendar/css/ui-darkness/images/ui-bg_glass_20_555555_1x400.png) 50% 50% repeat-x');
    $('#ui-datepicker-div').find('.ui-state-default').css('font-weight', 'bold');
    $('#ui-datepicker-div').find('.ui-state-default').css('color', '#eee');
    $('#ui-datepicker-div').find('.ui-datepicker-prev').find('.ui-icon-circle-triangle-w').css('background-image', 'url(../Calendar/css/ui-darkness/images/ui-icons_ffffff_256x240.png)');
    $('#ui-datepicker-div').find('.ui-datepicker-next').find('.ui-icon-circle-triangle-e').css('background-image', 'url(../Calendar/css/ui-darkness/images/ui-icons_ffffff_256x240.png)');
    $('#ui-datepicker-div').find(".ui-datepicker-prev").click(function () {
        dtpThemes();
    });
    $('#ui-datepicker-div').find(".ui-datepicker-next").click(function () {
        dtpThemes();
    });
    $('#ui-datepicker-div').find(".ui-datepicker-current-day").find('.ui-state-active').css('background', 'url(../Calendar/css/ui-darkness/images/ui-bg_inset-soft_30_f58400_1x100.png) repeat-x scroll 50% 50% #F58400');
    $('#ui-datepicker-div').find(".ui-datepicker-current-day").find('.ui-state-active').css('border', '1px solid #FFAF0F');
    $('#ui-datepicker-div').find(".ui-datepicker-current-day").find('.ui-state-active').css('color', '#FFFFFF');
    $('#ui-datepicker-div').find(".ui-datepicker-current-day").find('.ui-state-active').css('font-weight', 'bold');
    $('#ui-datepicker-div').find('.ui-state-default').hover(function () {
        $(this).css('border', '1px solid #59b4d4');
        $(this).css('background', '#0078a3 url(../Calendar/css/ui-darkness/images/ui-bg_glass_40_0078a3_1x400.png) 50% 50% repeat-x');
        $(this).css('font-weight', 'bold');
        $(this).css('color', '#ffffff');
    }, function () {
        $(this).css('border', '1px solid #666');
        $(this).css('background', '#555 url(../Calendar/css/ui-darkness/images/ui-bg_glass_20_555555_1x400.png) 50% 50% repeat-x');
        $(this).css('font-weight', 'bold');
        $(this).css('color', '#eee');
    });

    $('#ui-datepicker-div').find('.ui-state-default').mouseover(function () {
        $('#ui-datepicker-div').find(".ui-datepicker-current-day").find('.ui-state-active').css('background', 'url(../Calendar/css/ui-darkness/images/ui-bg_inset-soft_30_f58400_1x100.png) repeat-x scroll 50% 50% #F58400');
        $('#ui-datepicker-div').find(".ui-datepicker-current-day").find('.ui-state-active').css('border', '1px solid #FFAF0F');
        $('#ui-datepicker-div').find(".ui-datepicker-current-day").find('.ui-state-active').css('color', '#FFFFFF');
        $('#ui-datepicker-div').find(".ui-datepicker-current-day").find('.ui-state-active').css('font-weight', 'bold');
    }, function () {
        $('#ui-datepicker-div').find(".ui-datepicker-current-day").find('.ui-state-active').css('background', 'url(../Calendar/css/ui-darkness/images/ui-bg_inset-soft_30_f58400_1x100.png) repeat-x scroll 50% 50% #F58400');
        $('#ui-datepicker-div').find(".ui-datepicker-current-day").find('.ui-state-active').css('border', '1px solid #FFAF0F');
        $('#ui-datepicker-div').find(".ui-datepicker-current-day").find('.ui-state-active').css('color', '#FFFFFF');
        $('#ui-datepicker-div').find(".ui-datepicker-current-day").find('.ui-state-active').css('font-weight', 'bold');
    });

}
function dtpMouseOver() {
    $('#ui-datepicker-div').find(".ui-datepicker-current-day").find('.ui-state-active').css('background', 'url(../Calendar/css/ui-darkness/images/ui-bg_inset-soft_30_f58400_1x100.png) repeat-x scroll 50% 50% #F58400');
    $('#ui-datepicker-div').find(".ui-datepicker-current-day").find('.ui-state-active').css('border', '1px solid #FFAF0F');
    $('#ui-datepicker-div').find(".ui-datepicker-current-day").find('.ui-state-active').css('color', '#FFFFFF');
    $('#ui-datepicker-div').find(".ui-datepicker-current-day").find('.ui-state-active').css('font-weight', 'bold');
}
function dtpMouseOut() {
    $('#ui-datepicker-div').find('.ui-state-default').css('border', '1px solid #666');
    $('#ui-datepicker-div').find('.ui-state-default').css('background', '#555 url(../Calendar/css/ui-darkness/images/ui-bg_glass_20_555555_1x400.png) 50% 50% repeat-x');
    $('#ui-datepicker-div').find('.ui-state-default').css('font-weight', 'bold');
    $('#ui-datepicker-div').find('.ui-state-default').css('color', '#eee');
    $('#ui-datepicker-div').find(".ui-datepicker-current-day").find('.ui-state-active').css('background', 'url(../Calendar/css/ui-darkness/images/ui-bg_inset-soft_30_f58400_1x100.png) repeat-x scroll 50% 50% #F58400');
    $('#ui-datepicker-div').find(".ui-datepicker-current-day").find('.ui-state-active').css('border', '1px solid #FFAF0F');
    $('#ui-datepicker-div').find(".ui-datepicker-current-day").find('.ui-state-active').css('color', '#FFFFFF');
    $('#ui-datepicker-div').find(".ui-datepicker-current-day").find('.ui-state-active').css('font-weight', 'bold');
}
//******************** Page Load Events End **************************

//******************** Public Functions Start **************************

function makeGOutputData() {
    if (sessionStorage.jobSetupOutput == undefined && sessionStorage.jobSetupOutput == undefined)
        $.getJSON(gOutputServiceUrl, function (dataOutput) {
            gOutputData = dataOutput;
            //gOutputData.checkOutAction = gData.checkOutAction;
            buildOutputLists();
        });
    else {
        gOutputData = jQuery.parseJSON(sessionStorage.jobSetupOutput);
        //var is_firsttime = false;
        //if (gOutputData.checkOutAction == undefined) { gOutputData["checkOutAction"] = {}; is_firsttime = true; }
        //gOutputData.checkOutAction = gData.checkOutAction;

        //if (!is_firsttime) {
        //    updateStorePaymentInfoInSession();
        //    gOutputData = jQuery.parseJSON(sessionStorage.jobSetupOutput);
        //    gData.checkOutAction = gOutputData.checkOutAction;
        //    $.each(gData.checkOutAction.storePaymentInfoList, function (key, val) {
        //        $.each(val, function (key1, val1) {
        //            if (key1.indexOf('Pretty') > -1) {
        //                if (val1.indexOf('$') == -1)
        //                    gData.checkOutAction.storePaymentInfoList[key][key1] = '$' + val1;
        //                if (val1.indexOf('.00') == -1)
        //                    gData.checkOutAction.storePaymentInfoList[key][key1] = gData.checkOutAction.storePaymentInfoList[key][key1] + '.00';

        //            }
        //        });
        //    });
        //}

        if (sessionStorage.jobSetupOutputCompare != undefined) {
            var goutput_compare_data = jQuery.parseJSON(sessionStorage.jobSetupOutputCompare);
            // goutput_compare_data.checkOutAction = gOutputData.checkOutAction;
            //sessionStorage.jobSetupOutputCompare = JSON.stringify(goutput_compare_data);
            //if ($.trim($('#divError').html()) == "") {
            //            var erroMessage1 = "<b>Please select Locations</b>";
            //            $('#divError').html(erroMessage1);
            //            $('#divError').css('display', 'block');
            //}
            //return false;
        }
        buildOutputLists();
        getUploadedFilesCount();
    }
    //delete gOutputData.checkoutAction; //TODO: to be discussed with Jim. We are getting this attribute in templateOnchange web service
}

function makeGData() {
    makeGOutputData();
    bindAccountsReceivable();
    //$.getJSON(serviceURL, function (data) {
    //    gData = data;
    //    if (sessionStorage.jobSetupOutput != undefined && sessionStorage.jobSetupOutput != "" && sessionStorage.jobSetupOutput != null) {
    //        var output_json = jQuery.parseJSON(sessionStorage.jobSetupOutput);
    //        var selected_stores = output_json.selectedLocationsAction.selectedLocationsList;
    //        var inhome_date = output_json.inHomeDate;
    //        if (selected_stores != undefined && selected_stores.length > 0) {
    //            $.each(selected_stores, function (a, b) {
    //                gData.selectedLocationsList.push({
    //                    "city": b.city,
    //                    "state": b.state,
    //                    "originalIndex": b.originalIndex,
    //                    "quantity": b.quantity,
    //                    "address1": b.address1,
    //                    "zip": b.zip,
    //                    "storeName": b.storeName,
    //                    "storeId": b.storeId
    //                });
    //            });
    //        }
    //        //gData.pricingId = output_json.pricingId;
    //        gData["jobTemplate"] = (output_json.templateName != undefined) ? output_json.templateName : ((output_json.template != undefined) ? output_json.template : '');
    //        gData["mailstreamAction"] = output_json.mailstreamAction;
    //        if (output_json.mailstreamAction != undefined) {
    //            gData.isFirstClassMail = (output_json.mailstreamAction.mailClass != undefined && output_json.mailstreamAction.mailClass == "1") ? 1 : 0;
    //        }

    //        //postCORS(gCheckoutServiceURL + facilityId + '/' + jobNumber, JSON.stringify(gData), function (response) {
    //var checkout_url=gCheckoutServiceURL + facilityId + '/' + ((jobCustomerNumber == "203" ) ? CW : jobCustomerNumber) + '/' + jobNumber + '/asdf';
    //        //postCORS(checkout_url, JSON.stringify(gData), function (response) {
    //        //    gData = response;
    //        //    //fillgDataWithLinks();
    //        //    //buildGrid();
    //        //    //makeGOutputData();
    //        //    //bindAccountsReceivable();
    //        //}, function (error_response) {
    //        //    showErrorResponseText(error_response);
    //        //});
    //        //}
    //        //else {
    //        //    makeGOutputData();
    //        //}
    //    }
    //});
}

function fillgDataWithLinks() {
    var diff = [];

    var job_setup_output = {};
    var job_setup_output_compare = {};

    $.each(jQuery.parseJSON(sessionStorage.jobSetupOutput), function (key, val) {
        if (key != "checkoutAction" && key != "checkOutAction")
            job_setup_output[key] = val;
    });

    if (sessionStorage.jobSetupOutputCompare != undefined)
        $.each(jQuery.parseJSON(sessionStorage.jobSetupOutputCompare), function (key, val) {
            if (key != "checkoutAction" && key != "checkOutAction")
                job_setup_output_compare[key] = val;
        });

    diff = DiffObjects(job_setup_output, job_setup_output_compare);

    //diff = DiffObjects(jQuery.parseJSON(sessionStorage.jobSetupOutput), jQuery.parseJSON(sessionStorage.jobSetupOutputCompare));

    if (gData != null && gData.checkOutGrid.checkOutGridStoreList.length > 0) {
        var temp_gOutputData = jQuery.parseJSON(sessionStorage.jobSetupOutput);
        var rate_type = temp_gOutputData.mailstreamAction["rateType"];
        var selected_template = temp_gOutputData.templateName;
        $.each(gData.checkOutGrid.checkOutGridStoreList, function (a, b) {
            var values_parameter = [];
            values_parameter.push({
                "storeId": b.storeId,
                "address1": b.address1,
                "city": b.city,
                "state": b.state,
                "zip": b.zip,
                "quantity": b.quantityPretty,
                "actualPostage": b.actualPostagePretty,
                "postageReceived": b.postageReceivedPretty,
                "campaignName": gData.campaignName,
                "orderPk": gData.orderPk,
                "postageCPP": b.postageCPPPretty,
                "rateType": rateType(rate_type),
                "baseId": selected_template
            });
            if (jobNumber != "-1") {
                if (diff.length == 0) {
                    b["postageInvoice"] = "<img id='imgPostage" + a + "' src='images/btn_open_file.png' onmouseover='changeImage(this)' onmouseout='changeImageBack(this)' onclick=populateAccountingReportsOutputForm('rptPostageRequest',\"" + b.storeId + "\") href='#popupReportsParam' data-rel='popup' data-position-to='window'  data-inline='true'\>";
                }

                //b["postageInvoice"] = "<img id='imgPostage" + a + "' src='images/btn_open_file.png' onmouseover='changeImage(this)' onmouseout='changeImageBack(this)' onclick=populateAccountingReportsOutputForm('rptPostageRequest',\"" + b.storeId + "\") href='#popupReportsParam' data-rel='popup' data-position-to='window'  data-inline='true'\>";
                //b["outStandingInvoices"] = "<img id='imgOutStandingInvoices" + a + "' src='images/btn_open_file.png' onclick=populateAccountingReportsOutputForm('outStandingInvoices',\"" + b.storeId + "\") href='#popupReportsParam' data-rel='popup' data-position-to='window'  data-inline='true'\>";
                b["billingInvoice"] = "<img id='imgBilling" + a + "' src='images/btn_open_file.png' onmouseover='changeImage(this)' onmouseout='changeImageBack(this)'  onclick=populateAccountingReportsOutputForm('rptBillingInvoice',\"" + b.storeId + "\") href='#popupReportsParam' data-rel='popup' data-position-to='window'  data-inline='true'\>";
            }
            //b["incrementalEnhancement"] = "1234"; //TODO: to be removed, once the web service returns the value
        });
    }
}


function getDifferences(oldObj, newObj) {
    var diff = {};
    for (var k in oldObj) {
        if (!(k in newObj))
            diff[k] = undefined;  // property gone so explicitly set it undefined
        else if (oldObj[k] !== newObj[k])
            diff[k] = newObj[k];  // property in both but has changed
    }

    for (k in newObj) {
        if (!(k in oldObj))
            diff[k] = newObj[k]; // property is new
    }

    return diff;
}

function rateType(value) {
    if (value != null) {
        switch (value.toLowerCase()) {
            case "r":
                return "Regular";
                break;
            case "p":
                return "Postcard"
                break;
            case "n":
                return "Non-Profit";
                break;
        }
    }
}

function changeImage(ctrl) {
    $('#' + ctrl.id).attr("src", "images/btn_open_file_lit.png");
}
function changeImageBack(ctrl) {
    $('#' + ctrl.id).attr("src", "images/btn_open_file.png");
}

function buildGrid() {
    var display = (sessionStorage.userRole != 'admin') ? true : false;
    $("#gridMain").GridUnload();
    if (gData != null && gData != undefined) {
        jQuery("#gridMain").jqGrid({
            datatype: "local",
            data: gData.checkOutGrid.checkOutGridStoreList,
            viewrecords: true,
            sortable: false,
            columnReordering: false,
            width: (!display) ? "155%" : "215%",
            hoverrows: false,
            shrinkToFit: true,
            colNames: ["Store ID", 'Quantity', "Production (CPP)", "Postage (CPP)", "Additional Version Charge", "Data Charges </br>(per 1000)", "Incremental Enhancements", "Total", "Postage Invoice", "Billing Invoice"],
            colModel: [
            //{ name: 'storeId', width: '70%', hidden: false, key: true, sortable: false },
            //{ name: 'quantityPretty', width: '70%', align: "right", sortable: false },
            //{ name: 'productionCPPPretty', width: '90%', sortable: false, align: "right" },
            //{ name: 'postageCPPPretty', width: '95%', sortable: false, align: "right" },
            //{ name: 'additionalStoreChargePretty', width: '130%', sortable: false, align: "right" },
            //{ name: 'dataChargesPretty', width: '90%', sortable: false, align: "right" },
            //{ name: 'optionalChargesPretty', width: '90%', sortable: false, align: "right" },
            //{ name: 'totalPretty', width: '70%', sortable: false, align: "right" },
            //{ name: 'postageInvoice', width: '70%', sortable: false, align: "center" },
            //{ name: 'billingInvoice', width: '60%', sortable: false, align: "center", hidden: display }
                {name: 'storeId', width: (!display) ? '70%' : '80%', hidden: false, key: true, sortable: false },
                { name: 'quantityPretty', width: (!display) ? '70%' : '80%', align: "right", sortable: false },
                { name: 'productionCPPPretty', width: (!display) ? '90%' : '100%', sortable: false, align: "right" },
                { name: 'postageCPPPretty', width: (!display) ? '95%' : '105%', sortable: false, align: "right" },
                { name: 'additionalStoreChargePretty', width: (!display) ? '130%' : '140%', sortable: false, align: "right" },
                { name: 'dataChargesPretty', width: (!display) ? '90%' : '100%', sortable: false, align: "right" },
                { name: 'optionalChargesPretty', width: (!display) ? '95%' : '105%', sortable: false, align: "right" },
                { name: 'totalPretty', width: '70%', sortable: false, align: "right" },
                { name: 'postageInvoice', width: '70%', sortable: false, align: "center" },
                { name: 'billingInvoice', width: '60%', sortable: false, align: "center", hidden: display }
            //{ name: 'outStandingInvoices', width: '60%', sortable: false, align: "center", hidden: display }
            ]
        });

        $('#tdFinalInvoiceAmt').html(gData.checkOutGrid.finalInvoiceAmountPretty);
        $('#tdPostageReceived').html(gData.checkOutGrid.totalPostageReceivedPretty);
        $('#tdBillingReceived').html(gData.checkOutGrid.totalBillingReceivedPretty);
        $('#tdRemainingBalance').html(gData.checkOutGrid.remainingBalancePretty);

        if (sessionStorage.userRole == "user" || sessionStorage.userRole == "power") {
            $("#aBillingInvoice").hide(); //for normal user this button should not be displayed.
        }
    }
}

function bindAccountsReceivable() {
    var ulData = '';
    if (gData != null && gData != undefined) {
        $.each(gData.checkOutAction.storePaymentInfoList, function (key, val) {
            ulData += '<div data-theme="b" data-role="collapsible" data-collapsed-icon="arrow-r" data-expanded-icon="arrow-d"><h2>Accounts Receivable - ' + val.storeId + ' - ' + val.city + ', ' + val.state + '</h2>';
            ulData += '<ul data-role="listview" data-inset="true">';
            ulData += '<li data-role="fieldcontain" data-mini="true"><label for="txtPostageAmtReceived' + val.storeId + '" data-mini="true">Postage Amount Received:</label>';
            ulData += '<input type="text" name="txtPostageAmtReceived' + val.storeId + '" maxlength="10" id="txtPostageAmtReceived' + val.storeId + '" value="' + val.postageReceivedPretty + '" data-mini="true" /></li>';
            ulData += '<li data-role="fieldcontain"><label for="txtPostageCheck' + val.storeId + '">Postage Check #:</label>';
            ulData += '<input type="text" onkeyup="return replaceString(this.id);" maxlength="9" name="txtPostageCheck' + val.storeId + '" id="txtPostageCheck' + val.storeId + '" value="' + val.postageCheck + '" data-mini="true" /></li>';
            ulData += '<li data-role="fieldcontain"><label for="txtBillingAmtReceived' + val.storeId + '">Billing Amount Received:</label>';
            ulData += '<input type="text" name="txtBillingAmtReceived' + val.storeId + '" maxlength="10" id="txtBillingAmtReceived' + val.storeId + '" value="' + val.billingReceivedPretty + '" data-mini="true" /></li>';
            ulData += '<li data-role="fieldcontain"><label for="txtBillingCheck' + val.storeId + '">Billing Check #:</label>';
            ulData += '<input type="text" onkeyup="return replaceString(this.id);" maxlength="9" name="txtBillingCheck' + val.storeId + '" id="txtBillingCheck' + val.storeId + '" value="' + val.billingCheck + '" data-mini="true" /></li></ul>';
            ulData += '</div>';
        });
        $("#dvAccountsReceivable").empty();
        $("#dvAccountsReceivable").append(ulData);

        $('.ui-page').trigger('create');
        $("#dvAccountsReceivable ul").listview('refresh');
    }
}

function replaceString(ctrl_id) {
    return $('#' + ctrl_id).val($('#' + ctrl_id).val().replace(/[^\d]/ig, ''))
}


//function validationAcctReceivable() {
//    var message = "<ul>";
//    $.each(gOutputData.checkOutAction.storePaymentInfoList, function (key_approval_checkout, val_approval_checkout) {
//        var postage_amount_received = $('#txtPostageAmtReceived' + val_approval_checkout.storeId).val();
//        var postage_check = $('#txtPostageCheck' + val_approval_checkout.storeId).val();
//        var billing_amount_received = $('#txtBillingAmtReceived' + val_approval_checkout.storeId).val();
//        var billing_check = $('#txtBillingCheck' + val_approval_checkout.storeId).val();

//        postage_amount_received = (postage_amount_received.indexOf("$") > "-1") ? postage_amount_received.substring(1) : postage_amount_received;
//        billing_amount_received = (billing_amount_received.indexOf("$") > "-1") ? billing_amount_received.substring(1) : billing_amount_received;

//        postage_amount_received = (postage_amount_received.indexOf('.') != "-1") ? postage_amount_received.substring(0, postage_amount_received.indexOf('.')) : postage_amount_received;
//        billing_amount_received = (billing_amount_received.indexOf('.') != "-1") ? billing_amount_received.substring(0, billing_amount_received.indexOf('.')) : billing_amount_received;

//        if (postage_amount_received != 0 && postage_check == 0)
//            message += "<li>Please enter 'Postage Check #' for - Store Id : '" + val_approval_checkout.storeId + "'</li>";

//        if (billing_amount_received != 0 && billing_check == 0)
//            message += "<li>Please enter 'Billing Check #' for - Store Id : '" + val_approval_checkout.storeId + "'</li>";

//        if (postage_check > 0 && postage_amount_received == 0)
//            message += "<li>Please enter 'Postage Amount Received' for - Store Id : '" + val_approval_checkout.storeId + "'</li>";

//        if (billing_check > 0 && billing_amount_received == 0)
//            message += "<li>Please enter 'Billing Amount Received' for - Store Id : '" + val_approval_checkout.storeId + "'</li>";

//    });
//    message += "</ul>";
//    if (message != "<ul></ul>") {
//        $('#alertmsg').html(message);
//        $('#popupDialog').popup('open');
//        return false;
//    }
//    return true;
//}

function showExecuteConfirmationToAdmin() {
    var msg_box = '';
    msg_box = '<div data-role="popup" id="confirmAdmin" data-overlay-theme="a" data-history="false" data-theme="c" data-dismissible="false" style="max-width:520px;" class="ui-corner-all">' +
              '<div data-role="header" data-theme="a" class="ui-corner-top" id="dialogHeader">' +
              '<h1>Warning!</h1>' +
              '</div>' +
              '<div data-role="content" data-theme="d" class="ui-corner-bottom ui-content">' +
              '<div id="dvMsg"></div>' +
              '<div align="right"><a href="#" data-role="button" data-inline="true" data-mini="true"  id="btnContinuePopup" data-theme="b" data-transition="flow" onclick="okClickForAdmin(); ">Continue</a>' +
              '<a href="#" data-role="button" data-inline="true" data-mini="true" id="btnCancelPopup"  data-theme="b"  onclick="$(\'#confirmAdmin\').popup(\'close\') ">Cancel</a></div>' +
              '</div>' +
              '</div>';
    $("#_jobApprovalCheckout").append(msg_box);
}

function okClickForAdmin() {
    $('#confirmAdmin').popup('close');
    fnJobFinal("submit");
}

//function fnValidateApprovalsForExecute(action_type) {
//    if (action_type == "submit") {
//        //if (!($('#rdArtworkApprove')[0].checked || $('#rdArtworkReject')[0].checked || $('#rdCountsApprove')[0].checked || $('#rdCountsReject')[0].checked) && sessionStorage.userRole == 'admin') {
//        if (sessionStorage.userRole == 'admin') {
//            //if (!$('#rdCountsApprove')[0].checked || !$('#rdArtworkApprove')[0].checked) {
//            //if (sessionStorage.userRole == 'admin') {
//            $('#confirmAdmin').popup('open');
//            return false;
//            //}
//        }
//        if (!($('#rdArtworkApprove')[0].checked || $('#rdArtworkReject')[0].checked || $('#rdCountsApprove')[0].checked || $('#rdCountsReject')[0].checked) && (sessionStorage.userRole == "user" || sessionStorage.userRole == "power") {
//            //else if (sessionStorage.userRole == "user" || sessionStorage.userRole == "power") {
//            $('#alertmsg').html("Final Counts and Artwork must be approved before you can Submit your Order for Execution.");
//            $('#popupDialog').popup('open');
//            return false;
//        }
//        if (jobNumber > -1) {
//            $('#alertmsg').html("This order has already been submitted for execution.");
//            $('#btnExecute').addClass("ui-disabled");
//            $('#popupDialog').popup('open');
//            return false;
//        }
//    }
//    return true;
//}

//function fnValidateApprovals() {
//    var is_list_uploaded = false;
//    var is_art_uploaded = false;
//    var message = "";
//    if (gOutputData.standardizeAction.standardizeFileList != undefined && gOutputData.standardizeAction.standardizeFileList.length > 0 && gOutputData.standardizeAction.standardizeFileList[0].fileName != "")
//        is_list_uploaded = true;
//    if (gOutputData.artworkAction.artworkFileList != undefined && gOutputData.artworkAction.artworkFileList.length > 0 && gOutputData.artworkAction.artworkFileList[0].fileName != "")
//        is_art_uploaded = true;

//    if (($('#rdCountsApprove')[0].checked || $('#rdCountsReject')[0].checked) && !is_list_uploaded) {
//        message = "You cannot approve/reject final counts until a list has been uploaded.";
//        fnClearApproval('count');
//    }
//    else if (($('#rdCountsApprove')[0].checked || $('#rdCountsReject')[0].checked) && is_list_uploaded) {
//        var uploaded_source_files_list = [];
//        if (gOutputData != null && gOutputData.standardizeAction != null && gOutputData.standardizeAction != undefined && gOutputData.standardizeAction.standardizeFileList != undefined && gOutputData.standardizeAction.standardizeFileList != null)
//            uploaded_source_files_list = gOutputData.standardizeAction.standardizeFileList;

//        $.each(uploaded_source_files_list, function (key, val) {
//            if (val.sourceType == 1 && val.fileName != "" && (val.linkedTo == "" || val.linkedTo == null)) {
//                message += ((message != "") ? "<br><br>" : "") + '"' + val.fileName + '"' + ' has not been linked to an artwork file. Please upload an appropriate file to link it to.';
//                fnClearApproval('count');
//            }
//        });
//    }

//    if (($('#rdArtworkApprove')[0].checked || $('#rdArtworkReject')[0].checked) && !is_art_uploaded) {
//        message += ((message != "") ? "<br><br>" : "") + "You cannot approve/reject final artwork until artwork has been uploaded.";
//        fnClearApproval('art');
//    }
//    else if (($('#rdArtworkApprove')[0].checked || $('#rdArtworkReject')[0].checked) && is_art_uploaded) {
//        var uploaded_art_files_list = [];
//        if (gOutputData != null && gOutputData.artworkAction != null && gOutputData.artworkAction != undefined && gOutputData.artworkAction.artworkFileList != undefined && gOutputData.artworkAction.artworkFileList != null)
//            uploaded_art_files_list = gOutputData.artworkAction.artworkFileList;

//        $.each(uploaded_art_files_list, function (key, val) {
//            if (val.fileName != "" && (val.linkedTo == "" || val.linkedTo == null)) {
//                message += ((message != "") ? "<br><br>" : "") + '"' + val.fileName + '"' + ' has not been linked to an list file. Please upload an appropriate file to link it to.';
//                fnClearApproval('art');
//            }
//        });
//    }

//    if (message != "") {
//        $('#alertmsg').html(message);
//        $('#popupDialog').popup('open');
//        return false;
//    }
//    return true;
//}

function bindCheckoutData(action_type) {
    if (gOutputData.selectedLocationsAction != undefined && gOutputData.selectedLocationsAction.selectedLocationsList != undefined && gOutputData.selectedLocationsAction.selectedLocationsList.length == 0) {// && action_type == "submit"
        $('#alertmsg').text("Please select Location(s) from the Location page.");
        $('#popupDialog').popup('open', { positionTo: 'window' });
        return false;
    }
    if (action_type == "submit" && jobCustomerNumber == CW && appPrivileges.roleName == "admin") {
        var est_qty_data = gOutputData.selectedLocationsAction.selectedLocationsList;
        var est_qty_sum = 0;
        $.each(est_qty_data, function (key1, val1) {
            var est_qty_val = val1.quantity;
            est_qty_sum += est_qty_val;
        });
        if (est_qty_sum >= 50000) {
            // $('#alertmsg').text("This order has a quantity of 50,000 or more pieces and has not been approved. It cannot be executed without list and artwork approvals.");
            // $('#popupDialog').popup('open', { positionTo: 'window' });
            $('#dvMsg').html('This order has a quantity of 50,000 or more pieces and has not been approved. It cannot be executed without list and artwork approvals.');
            $('#btnContinuePopup').css('display', 'none');
            $('#btnCancelPopup').text('Close');
            $('#confirmAdmin').popup('open', { positionTo: 'window' });
        }
        else {
            $('#dvMsg').html('This order has not been approved. Do you want to Continue without approvals?');
            $('#confirmAdmin').popup('open');
        }
        return false;
    }
    //if (!fnValidateApprovalsForExecute(action_type))
    //    return false;

    //if (!fnValidateApprovals())
    //    return false;

    //if (sessionStorage.userRole == 'admin')
    //    if (!validationAcctReceivable())
    //        return false;

    //validationAcctReceivable
    var message = "<ul>";
    //if (sessionStorage.userRole == 'admin') {
    //    $.each(gOutputData.checkOutAction.storePaymentInfoList, function (key_approval_checkout, val_approval_checkout) {
    //        var postage_amount_received = $('#txtPostageAmtReceived' + val_approval_checkout.storeId).val();
    //        var postage_check = $('#txtPostageCheck' + val_approval_checkout.storeId).val();
    //        var billing_amount_received = $('#txtBillingAmtReceived' + val_approval_checkout.storeId).val();
    //        var billing_check = $('#txtBillingCheck' + val_approval_checkout.storeId).val();

    //        postage_amount_received = (postage_amount_received.indexOf("$") > "-1") ? postage_amount_received.substring(1) : postage_amount_received;
    //        billing_amount_received = (billing_amount_received.indexOf("$") > "-1") ? billing_amount_received.substring(1) : billing_amount_received;

    //        postage_amount_received = (postage_amount_received.indexOf('.') != "-1") ? postage_amount_received.substring(0, postage_amount_received.indexOf('.')) : postage_amount_received;
    //        billing_amount_received = (billing_amount_received.indexOf('.') != "-1") ? billing_amount_received.substring(0, billing_amount_received.indexOf('.')) : billing_amount_received;

    //        if (postage_amount_received != 0 && postage_check == 0)
    //            message += "<li>Please enter 'Postage Check #' for - Store Id : '" + val_approval_checkout.storeId + "'</li>";

    //        if (billing_amount_received != 0 && billing_check == 0)
    //            message += "<li>Please enter 'Billing Check #' for - Store Id : '" + val_approval_checkout.storeId + "'</li>";

    //        if (postage_check > 0 && postage_amount_received == 0)
    //            message += "<li>Please enter 'Postage Amount Received' for - Store Id : '" + val_approval_checkout.storeId + "'</li>";

    //        if (billing_check > 0 && billing_amount_received == 0)
    //            message += "<li>Please enter 'Billing Amount Received' for - Store Id : '" + val_approval_checkout.storeId + "'</li>";

    //    });
    //}

    //fnValidateApprovals
    //var is_list_uploaded = false;
    //var is_art_uploaded = false;
    //if (gOutputData.standardizeAction.standardizeFileList != undefined && gOutputData.standardizeAction.standardizeFileList.length > 0 && gOutputData.standardizeAction.standardizeFileList[0].fileName != "")
    //    is_list_uploaded = true;
    //if (gOutputData.artworkAction.artworkFileList != undefined && gOutputData.artworkAction.artworkFileList.length > 0 && gOutputData.artworkAction.artworkFileList[0].fileName != "")
    //    is_art_uploaded = true;

    //if (!is_list_uploaded && action_type == "submit")
    //    message += "<li>This job has not had a list uploaded.</li>";

    //if (action_type !== "validate") {
    //    if (($('#rdCountsApprove')[0].checked || $('#rdCountsReject')[0].checked) && !is_list_uploaded) {
    //        message += "<li>You cannot approve/reject final counts until a list has been uploaded.</li>";
    //        //fnClearApproval('count');
    //    }
    //    else if ((is_list_uploaded && action_type == "submit") || ($('#rdCountsApprove')[0].checked || $('#rdCountsReject')[0].checked) && is_list_uploaded && action_type != "submit") {
    //        var uploaded_source_files_list = [];
    //        if (gOutputData != null && gOutputData.standardizeAction != null && gOutputData.standardizeAction != undefined && gOutputData.standardizeAction.standardizeFileList != undefined && gOutputData.standardizeAction.standardizeFileList != null)
    //            uploaded_source_files_list = gOutputData.standardizeAction.standardizeFileList;

    //        $.each(uploaded_source_files_list, function (key, val) {
    //            if (val.sourceType == 1 && val.fileName != "" && (val.linkedTo == "" || val.linkedTo == null)) {
    //                message += '<li>"' + val.fileName + '"' + ' has not been linked to an artwork file. Please upload an appropriate file to link it to.</li>';
    //                //fnClearApproval('count');
    //            }
    //        });
    //    }
    //}
    //if (!is_art_uploaded && action_type == "submit")
    //    message += "<li>This job has not had artwork uploaded.</li>";

    //if (action_type !== "validate") {
    //    if (($('#rdArtworkApprove')[0].checked || $('#rdArtworkReject')[0].checked) && !is_art_uploaded) {
    //        message += "<li>You cannot approve/reject final artwork until an artwork has been uploaded.</li>";
    //        //fnClearApproval('art');
    //    }
    //    else if ((is_art_uploaded && action_type == "submit") || ($('#rdArtworkApprove')[0].checked || $('#rdArtworkReject')[0].checked) && is_art_uploaded && action_type != "submit") {
    //        var uploaded_art_files_list = [];
    //        if (gOutputData != null && gOutputData.artworkAction != null && gOutputData.artworkAction != undefined && gOutputData.artworkAction.artworkFileList != undefined && gOutputData.artworkAction.artworkFileList != null)
    //            uploaded_art_files_list = gOutputData.artworkAction.artworkFileList;

    //        $.each(uploaded_art_files_list, function (key, val) {
    //            if (val.fileName != "" && (val.linkedTo == "" || val.linkedTo == null)) {
    //                message += '<li>"' + val.fileName + '"' + ' has not been linked to a list file. Please upload an appropriate file to link it to.</li>';
    //                //fnClearApproval('art');
    //            }
    //        });
    //    }
    //}

    //fnValidateApprovalsForExecute
    if (action_type == "submit") {
        //if (sessionStorage.userRole == 'admin') {
        //    if (!$('#rdArtworkApprove')[0].checked || !$('#rdCountsApprove')[0].checked) {
        //        message += '<li>Final counts and artwork have not been approved.</li>';
        //        message += '</br>Click "Cancel" to return to the order, click "Continue" to submit the job.';

        //    }
        //    else {
        //        if (message != "<ul>")
        //            message += '</br>Click "Cancel" to return to the order, click "Continue" to submit the job.';
        //    }
        //}
        //if (sessionStorage.userRole == "user" || sessionStorage.userRole == "power") {
        //    if (!$('#rdArtworkApprove')[0].checked || !$('#rdCountsApprove')[0].checked)
        //        message += '<li>Final counts and artwork have not been approved.</li>';
        //}
        if (jobNumber > -1) {
            message += '<li>This order has already been submitted for execution.</li>';
            $('#btnExecute').addClass("ui-disabled");
        }
    }

    message += "</ul>";

    if (message != "<ul></ul>") {
        if (sessionStorage.userRole == 'admin' && action_type == "submit") {
            $('#btnContinuePopup').show();
            $('#btnContinuePopup').css("display", "");
        }
        else if (sessionStorage.userRole == "user" || sessionStorage.userRole == "power") {
            $('#dialogHeader').prepend('<a id="btnClose" href="#" data-role="button" data-theme="a" data-icon="delete" data-iconpos="notext" class="ui-btn-right" onclick="removeCloseButton();">Close</a>');
            $('#btnCancelPopup').hide();
            $('#confirmAdmin a').each(function (i) {
                $(this).button();
            });
            $('#btnContinuePopup').hide();

        }
        else if (sessionStorage.userRole == 'admin' && action_type != "submit") {
            $('#btnContinuePopup').hide();
        }
        $('#dvMsg').html(message);
        $('#confirmAdmin').popup('open');
        return false;
    }
    else
        fnJobFinal(action_type);
}

function removeCloseButton() {
    $('#btnClose').remove();
    $('#confirmAdmin').popup('close');
}

function fnJobFinal(action_type) {
    //    if (action_type != "validate") {
    //        if (!$('#rdArtworkApprove')[0].checked && !$('#rdArtworkReject')[0].checked)
    //            gOutputData.checkOutAction.checkOutApproval.isArtworkApproved = null;
    //        else
    //            gOutputData.checkOutAction.checkOutApproval.isArtworkApproved = $('#rdArtworkApprove')[0].checked;

    //        if (!$('#rdCountsApprove')[0].checked && !$('#rdCountsReject')[0].checked)
    //            gOutputData.checkOutAction.checkOutApproval.isCountsApproved = null;
    //        else
    //            gOutputData.checkOutAction.checkOutApproval.isCountsApproved = $('#rdCountsApprove')[0].checked;
    //    }

    //$.each(gOutputData.checkOutAction.storePaymentInfoList, function (key_approval_checkout, val_approval_checkout) {
    //    val_approval_checkout["postageReceivedPretty"] = $('#txtPostageAmtReceived' + val_approval_checkout.storeId).val();
    //    val_approval_checkout["postageCheck"] = $('#txtPostageCheck' + val_approval_checkout.storeId).val();
    //    val_approval_checkout["billingReceivedPretty"] = $('#txtBillingAmtReceived' + val_approval_checkout.storeId).val();
    //    val_approval_checkout["billingCheck"] = $('#txtBillingCheck' + val_approval_checkout.storeId).val();
    //});

    if (jobCustomerNumber === CW && jobNumber != "-1") //Biz rules for needsBillingSave flag
        gOutputData = fnNeedsBillingSave(gOutputData);

    //    if (action_type == "validate")
    //        sessionStorage.jobSetupOutput = JSON.stringify(gOutputData);
    //    else {

    var tmp_gOutputData = [];
    if (sessionStorage.jobSetupOutput != undefined && sessionStorage.jobSetupOutput != null && sessionStorage.jobSetupOutput != "")
        tmp_gOutputData = jQuery.parseJSON(sessionStorage.jobSetupOutput);
    $.each(tmp_gOutputData.selectedLocationsAction.selectedLocationsList, function (key, val) {
        delete val.isLatLngExists;
        delete val.isNewStore;
    });
    sessionStorage.jobSetupOutput = JSON.stringify(tmp_gOutputData);
    postJobURL = serviceURLDomain + 'api/JobTicket/' + facilityId + '/' + jobNumber + '/' + sessionStorage.username + '/' + action_type + '/ticket';
    postCORS(postJobURL, JSON.stringify(gOutputData), function (response) {
        var msg = (sessionStorage.jobNumber == "-1") ? 'Your job was saved.' : 'Your job was updated.';
        msg = (action_type == "submit") ? "Thank you! Your order has been submitted for execution." : msg;
        if (response != "") {
            if (!isNaN(parseInt(response.replace(/"/g, "")))) {
                jobNumber = parseInt(response.replace(/"/g, ""));
                sessionStorage.jobNumber = jobNumber;
                gOutputData.jobNumber = jobNumber;
                sessionStorage.jobDesc = (gOutputData.jobName != undefined) ? gOutputData.jobName : "";
                $('#liJobSummary').css('display', "block");
                displayJobInfo();
                sessionStorage.jobSetupOutput = JSON.stringify(gOutputData);
                sessionStorage.jobSetupOutputCompare = JSON.stringify(gOutputData);
            }
            else
                msg = response;
            $('#alertmsg').text(msg);
            makeGData();
            setGridProperties();
            window.setTimeout(function getDelay() {
                if (msg == 'Your job was saved.' || msg == 'Your job was updated.')
                    $("#okBut").attr("onclick", "$('#popupDialog').popup('close');");

                $('#popupDialog').popup('open', { positionTo: 'window' });
            }, 500);
        }
        else {
            var msg = 'Job creation failed.';
            $('#alertmsg').text(msg);
            $('#popupDialog').popup('open', { positionTo: 'window' });
        }
    }, function (response_error) {
        //var msg = 'Job creation failed.';
        var msg = response_error.responseText;
        if (response_error.status == 409) {
            msg = "Your job <b>" + gOutputData.jobName + "</b> could not be saved as there is already a job in the system with that name. Please rename your job and try again.";
            $('#alertmsg').html(msg);
        }
        else
            $('#alertmsg').text(msg);
        $('#popupDialog').popup('open', { positionTo: 'window' });
        var error = response_error;
    });
    //}
}

function setGridProperties() {
    window.setTimeout(function setDelay() {
        $('#gbox_gridMain').css('width', '100%');
        $('#gview_gridMain').css('width', '100%');
        $('.ui-state-default.ui-jqgrid-hdiv').css('width', '100%');
        $('.ui-jqgrid-bdiv').css('width', '100%');
    }, 1000);
}

jQuery(window).bind('resize', function () {
    var width = calculateGridWidth();
    if (width > 0 &&
    // Only resize if new width exceeds a minimal threshold
    // Fixes IE issue with in-place resizing when mousing-over frame bars
        Math.abs(width - jQuery('#gridMain').width()) > 2) {
        jQuery('#gridMain').setGridWidth(width);
    }
}).trigger('resize');

$(document).ready(function () {
    window.setTimeout(function setDelay() {
        var grid_width = calculateGridWidth();
        if (grid_width > 0 &&
        // Only resize if new width exceeds a minimal threshold
        // Fixes IE issue with in-place resizing when mousing-over frame bars
        Math.abs(grid_width - jQuery('#gridMain').width()) > 2) {
            jQuery('#gridMain').setGridWidth(grid_width);
        }
    }, 1000);
});

function buildOutputLists() {
    //if (gOutputData != null) {
    //    $.each(gOutputData.checkOutAction.storePaymentInfoList, function (key_rows, val_rows) {
    //        $('#txtPostageAmtReceived' + val_rows.storeId).val(val_rows["postageReceivedPretty"]);
    //        $('#txtPostageCheck' + val_rows.storeId).val(val_rows["postageCheck"]);
    //        $('#txtBillingAmtReceived' + val_rows.storeId).val(val_rows["billingReceivedPretty"]);
    //        $('#txtBillingCheck' + val_rows.storeId).val(val_rows["billingCheck"]);
    //    });
    //    //sessionStorage.jobSetupOutput = JSON.stringify(gOutputData);
    //}
}
//******************** Public Functions End **************************