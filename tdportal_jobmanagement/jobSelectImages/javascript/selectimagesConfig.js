﻿hideUploadFile = function () {
    var customer = getCustomerNumber();
    return ([KUBOTA, SPORTSKING, ALLIED, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);
}

showImageLibraryDemoHint = function () {
    var customer = getCustomerNumber();
    return ([KUBOTA, SPORTSKING, GWA, ALLIED, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);
}

isLogoBaseArtworkCustomers = function () {
    var customer = getCustomerNumber();
    return ([SPORTSKING, KUBOTA, GWA, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);
}

isBronzeImageList = function () {
    var customer = getCustomerNumber();
    return ([ALLIED].indexOf(customer) > -1);
}

isSixMangoImages = function () {
    var customer = getCustomerNumber();
    return ([KUBOTA].indexOf(customer) > -1);
}

showImageFullName = function () {
    var customer = getCustomerNumber();
    return ([AAG, KUBOTA, SPORTSKING, GWA, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);
}

isSetDefaultValue = function () {
    var customer = getCustomerNumber();
    return ([DCA, KUBOTA, SPORTSKING, GWA, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);
}

isLoadUsersWithLocationInfo = function () {
    var customer = getCustomerNumber();
    return ([DCA, KUBOTA, SPORTSKING, GWA, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);
}

isLoadUsersInfo = function () {
    var customer = getCustomerNumber();
    return ([AAG].indexOf(customer) > -1);
}

isChangeTextFromDefaultLogoToViewImage = function () {
    var customer = getCustomerNumber();
    return ([SPORTSKING, KUBOTA, GWA, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);
}

hideDeleteAndReplace = function () {
    var customer = getCustomerNumber();
    return ([KUBOTA].indexOf(customer) > -1);
}

isNonLoanOfficerCustomers = function () {
    var customer = getCustomerNumber();
    return ([SPORTSKING, KUBOTA, GWA, ALLIED, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);
}

isNewVariableFieldListCustomers = function () {
    var customer = getCustomerNumber();
    return ([ANDERSEN, SPORTSKING, KUBOTA].indexOf(customer) > -1);
}

isOldVariableCustomers = function () {
    var customer = getCustomerNumber();
    return ([ANDERSEN, SPORTSKING, KUBOTA].indexOf(customer) > -1);
}

showSelectedArtworkFile = function () {
    var customer = getCustomerNumber();
    return ([ANDERSEN, SPORTSKING, KUBOTA].indexOf(customer) > -1);
}

isReplicateEventsVerificationNeeded = function () {
    var customer = getCustomerNumber();
    return ([AAG, DCA, KUBOTA, SPORTSKING, GWA, ALLIED, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);
}

showReplicateAndNavbar = function () {
    var customer = getCustomerNumber();
    return ([ALLIED].indexOf(customer) > -1);
}

hideLocationsButton = function () {
    var customer = getCustomerNumber();
    return ([ANDERSEN, DCA, KUBOTA, SPORTSKING, GWA, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);
}

showSelectImagesDemoHint = function () {
    var customer = getCustomerNumber();
    return ([ANDERSEN, KUBOTA, SPORTSKING, GWA, ALLIED, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);
}

isLoadOldImageAction = function () {
    var customer = getCustomerNumber();
    return ([DCA].indexOf(customer) > -1);
}

isNewImageActionVariableFieldList = function () {
    var customer = getCustomerNumber();
    return ([ANDERSEN, SPORTSKING, KUBOTA, GWA, ALLIED, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);
}

isOldVariableDictCustomers = function () {
    var customer = getCustomerNumber();
    return ([GWA, ALLIED, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);
}

showClicktoDefaultTitle = function () {
    var customer = getCustomerNumber();
    return ([AAG, DCA, GWA, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);
}

showAddAnImage = function () {
    var customer = getCustomerNumber();
    return ([DCA].indexOf(customer) > -1);
}

showMakeASelection = function () {
    var customer = getCustomerNumber();
    return ([GWA].indexOf(customer) > -1);
}

isDeleteConfirmCustomers = function () {
    var customer = getCustomerNumber();
    return ([GWA, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);
}
