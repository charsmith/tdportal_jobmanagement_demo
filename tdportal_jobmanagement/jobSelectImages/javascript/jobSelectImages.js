﻿$.mobile.popup.prototype.options.history = false;
jQuery.support.cors = true;

//******************** Data URL Variables Start **************************

var gServiceLoList = "../jobSelectImages/JSON/_jobSelectImages.JSON";
var loData;
var jobLOData;
var pageObj;
var gOutputData;
var gPageSize = 1;
var gVarArtworkInfoServiceUrl = "../jobLogoSelection/JSON/_jobLogoSelection.JSON";
var gServiceOptionalLogos = [];
var dontShowHintsAgain = (localStorage.getItem('doNotShowHintsAgain') != undefined && localStorage.getItem('doNotShowHintsAgain') != null && localStorage.getItem('doNotShowHintsAgain') != "" && localStorage.getItem('doNotShowHintsAgain') != "false") ? true : false;
//******************** Data URL Variables End **************************

//******************** Page Load Events Start **************************

$(window).on('popupbeforeposition', 'div:jqmData(role="popup")', function () {
    var notDismissible = $(this).jqmData('dismissible') === false;
    if (notDismissible) {
        $('.ui-popup-screen').off();
    }
});

$('#_jobSelectImages').live('pagebeforecreate', function (event) {
    displayMessage('_jobSelectImages');
    createConfirmMessage('_jobSelectImages');
    displayNavLinks();
    getLoanOfficersData();
    getOptionalLogosInfo();
    loadingImg('_jobSelectImages');
    var pageobj_time = (sessionStorage.setUpTwoDate != undefined && sessionStorage.setUpTwoDate != null && sessionStorage.setUpTwoDate != "" && JSON.parse(sessionStorage.setUpTwoDate) == true) ? 3000 : 1800;
    window.setTimeout(function () {
        pageObj = new makeLoOfficers();
        ko.applyBindings(pageObj);
        //window.setTimeout(function () {
        $('#dvImages ul').listview().listview('refresh');
        //$('div').trigger('create');
        //$('#navbar').trigger('create');
        $('#dvNavBar div ul li a').first().addClass('ui-btn-active');
        if (isReplicateEventsVerificationNeeded()) {
            if (sessionStorage.setUpTwoDate != undefined && sessionStorage.setUpTwoDate != null && sessionStorage.setUpTwoDate != "" && JSON.parse(sessionStorage.setUpTwoDate) == true) {
                $('#dvNavBar').css('display', 'block');
                $("label[for='chkReplicate']").text("Replicate Images For All Events");
                $('#dvReplicate').css('display', 'block');
            }
            else {
                if (showReplicateAndNavbar()) {
                    $('#dvNavBar').css('display', 'block');
                    $('#dvReplicate').css('display', 'block');
                    $('#dvNavBar').trigger('create');
                    $('#dvNavBar div ul li a').first().addClass('ui-btn-active');
                    $('#chkReplicate').checkboxradio('refresh');
                    $('a[data-icon="myapp-locations"]').css('display', 'block');
                    $('#ddlLocations').selectmenu('refresh');
                    $('#dvOptionalLogos fieldset').trigger('create');
                    $('#dvOptionalLogos ul').listview().listview('refresh');
                    //$('#dvOptionalLogos ul li input[type=checkbox]').checkboxradio();
                    $('#dvOptionalLogos').css('display', 'block');
                }
                else {
                    $('#dvNavBar').css('display', 'none');
                    $('#dvReplicate').css('display', 'none');
                }
            }
            if (hideLocationsButton()) {
                $('a[data-icon="myapp-locations"]').css('display', 'none');
            }
        }
        //}, 1000);
        //$('#dvEmailRecipients').listview('refresh');
    }, pageobj_time);
    $("#navLinksPanel").on("panelopen", function (event, ui) {
        sessionStorage.isNavPanelOpen = true;
    });

    if (sessionStorage.jobSetupOutput != undefined && sessionStorage.jobSetupOutput != null && sessionStorage.jobSetupOutput != "") {
        gOutputData = jQuery.parseJSON(sessionStorage.jobSetupOutput);
        getBubbleCounts(gOutputData);
    }
    setDemoHintsSliderValue();
    createDemoHints("jobSelectImages");
    createPlaceHolderforIE();
    if ([AAG, DCA, GWA, ALLIED].indexOf(jobCustomerNumber) > -1) {
        if (jobCustomerNumber == ALLIED)
            $('div[data-role="header"]').find('div[data-role=controlgroup]').prepend('<a href="#" data-rel="popup" data-position-to="window" data-transition="pop" data-role="button" data-icon="myapp-locations" data-iconpos="notext" data-inline="true" data-mini="true" title="Ordered Locations" data-bind="click:$root.showOrderedLocations">Ordered Locations</a>');
        $('#dvHeaderButtons div').prepend('<a href="#" data-rel="popup" data-position-to="window" data-transition="pop" data-role="button" data-icon="eye" data-iconpos="notext" data-inline="true" data-mini="true" title="Preview Mailing" data-bind="click:$root.previewMailing">Preview Mailing</a>');
    }
    if (jobCustomerNumber == AAG && appPrivileges.roleName == "power") {
        $('#dvHeaderButtons div').prepend('<a href="#" data-rel="popup" data-position-to="window" data-transition="pop" data-role="button" data-icon="user" data-iconpos="notext" data-inline="true" data-mini="true" title="Loan Officers" data-bind="click:$root.showLoanOfficers">Loan Officers</a>');
    }
});

$(document).on('pageshow', '#_jobSelectImages', function (event) {
    persistNavPanelState();
    if (!dontShowHintsAgain && (showSelectImagesDemoHint())
                            && (sessionStorage.showDemoHints == undefined || sessionStorage.showDemoHints == null || sessionStorage.showDemoHints == "" ||
                                sessionStorage.showDemoHints == "on")
                            && (sessionStorage.isSelectImagesDemoHintsDisplayed == undefined || sessionStorage.isSelectImagesDemoHintsDisplayed == null ||
                                sessionStorage.isSelectImagesDemoHintsDisplayed == "false")) {
        sessionStorage.isSelectImagesDemoHintsDisplayed = true;
        $('#selectImagesHints').popup('close');
        window.setTimeout(function loadHints() {
            $('#selectImagesHints').popup('open', { positionTo: '#dvImages' });
        }, 500);
    }
    
});

function getLoanOfficersData() {
    if (gOutputData == undefined || gOutputData == null || gOutputData == "" || Object.keys(gOutputData).length == 0) {
        if (sessionStorage.jobSetupOutput != undefined && sessionStorage.jobSetupOutput != null && sessionStorage.jobSetupOutput != "") {
            gOutputData = $.parseJSON(sessionStorage.jobSetupOutput);
        }
    }
    $.getJSON(gServiceLoList, function (data) {
        $.each(data, function (key, val) {
            if (key.indexOf(jobCustomerNumber) > -1) {
                //if (jobCustomerNumber == DCA) {
                //    var temp_data = [];
                //    if ((gOutputData.selectedLocationsAction != undefined && gOutputData.selectedLocationsAction != null && gOutputData.selectedLocationsAction != "") && (gOutputData.selectedLocationsAction.selectedLocationsList.length > 0)) {
                //        $.each(gOutputData.selectedLocationsAction.selectedLocationsList, function (key1, val1) {
                //            $.each(val, function (key2, val2) {
                //                temp_data[val1.pk] = val2;
                //            });
                //        });
                //        jobLOData = temp_data;
                //    }
                //}
                //else {
                //if (jobCustomerNumber == DCA) {
                //    $.each(val, function (key1, val1) {
                //        $.each(gOutputData.selectImagesAction, function (key2, val2) {
                //            if (key2 != "isReplicated") {
                //                gOutputData.selectImagesAction[key2] = {};
                //                $.each(val2, function (key3, val3) {
                //                    gOutputData.selectImagesAction[key2][key3] = val1;
                //                    //gOutputData.selectImagesAction[key2] = val;
                //                });
                //            }
                //        });
                //    });
                //}
                //else {
                if (sessionStorage.selectedLoanOffierToEdit != undefined && sessionStorage.selectedLoanOffierToEdit != null && sessionStorage.selectedLoanOffierToEdit != "") {
                    temp_image_info = $.parseJSON(sessionStorage.selectedLoanOffierToEdit);
                    gOutputData.selectImagesAction = temp_image_info.loInfo;
                    sessionStorage.removeItem("selectedLoanOffierToEdit");
                } else {
                    if (isLoadOldImageAction()) {
                        $.each(val, function (key1, val1) {
                            $.each(gOutputData.selectImagesAction, function (key2, val2) {
                                if (key2 != "isReplicated") {
                                    gOutputData.selectImagesAction[key2] = {};
                                    $.each(val2, function (key3, val3) {
                                        gOutputData.selectImagesAction[key2][key3] = val1;
                                        //gOutputData.selectImagesAction[key2] = val;
                                    });
                                }
                            });
                        });
                    }
                    else if (isNewImageActionVariableFieldList()) {
                        //$.each(val, function (key1, val1) {
                        //$.each(gOutputData.variableImageAction, function (key3, val3) {
                        //    if (key3 != "isReplicated") {
                        if (isNewVariableFieldListCustomers()) {
                            if (Object.keys(gOutputData.variableImageAction.variableFieldList).length == 0) {
                                $.each(val, function (key1, val1) {
                                    if ([KUBOTA].indexOf(jobCustomerNumber) > -1 && (sessionStorage.username.toLowerCase().indexOf('six_mango') > -1) && val1.value == "") {
                                        val1.value = "six-and-mango_logo_black.png";
                                    }
                                    gOutputData.variableImageAction.variableFieldList.push(val1);
                                });
                            }
                            sessionStorage.jobSetupOutput = JSON.stringify(gOutputData);
                        }
                        else {
                            if (gOutputData.variableImageAction.variableDict == undefined || gOutputData.variableImageAction.variableDict == null || Object.keys(gOutputData.variableImageAction.variableDict).length == 0) {
                                gOutputData.variableImageAction.variableDict = {};
                                $.each(val, function (key1, val1) {
                                    gOutputData.variableImageAction.variableDict["image" + (key1 + 1)] = val1.loThumbnailImg.toLowerCase();
                                });
                                //gOutputData.variableImageAction.variableDict = val;
                                sessionStorage.jobSetupOutput = JSON.stringify(gOutputData);
                            }
                            else {
                                var val_lower = '';
                                $.each(gOutputData.variableImageAction.variableDict, function (key1, val1) {
                                    val_lower = (val1.indexOf('/') > -1 ? val1.substring(val1.lastIndexOf('/')) : val1).replace('.png', '').replace('/', '');
                                    gOutputData.variableImageAction.variableDict[key1] = val_lower.toLowerCase();
                                });
                            }
                        }

                        //    }
                        //});

                        //});
                    }
                    else {
                        $.each(gOutputData.selectImagesAction, function (key1, val1) {
                            if (key1 != "isReplicated") {
                                gOutputData.selectImagesAction[key1] = {};
                                gOutputData.selectImagesAction[key1] = val;

                            }
                        });
                    }
                }
                //}
                jobLOData = val;
                if (isOldVariableDictCustomers())
                    jobLOData = gOutputData.variableImageAction.variableDict

                //}
                return false;
            }
        });
    });
};

function createPagerObject1(total_pages, start, end) {
    var pager_length = Math.ceil(total_pages / gPageSize);
    var pager_object = [];
    if (total_pages > 1) {
        for (var j = 0; j < total_pages; j++) {
            pager_object.push({ 'name': 'Page ' + (j + 1), 'value': (j) });
        }
    }
    return pager_object;
}


var getOptionalLogosInfo = function () {

    if (sessionStorage.jobSetupOutput != undefined && sessionStorage.jobSetupOutput != null && sessionStorage.jobSetupOutput != "") {
        gOutputData = $.parseJSON(sessionStorage.jobSetupOutput);
        $.getJSON(gVarArtworkInfoServiceUrl, function (data) {
            $.each(data, function (key, val) {
                //if (key == gOutputData.templateName.substring(1, 4).replace('_', 'X')) {
                if (gOutputData.templateName.indexOf(key) > -1) {
                    gServiceOptionalLogos = val;
                    return false;
                }
                //var selected_artwork_info = $.gServiceData.filter
            });
        });



        //$.getJSON(gVarArtworkInfoServiceUrl, function (data) {
        //    gServiceOptionalLogos = data;
        //});
    }
}

var loRecipientGroup = function (name, lo_list, selected_page_value, search_value) {
    var self = this;
    self.caption = ko.observable(name);
    self.emails = ko.observableArray([]);
    $.each(lo_list, function (key, val) {
        self.emails.push(new loRecipient(val));
    });
    self.pagingData = ko.observableArray([]).extend({ rateLimit: { method: "notifyWhenChangesStop", timeout: 200 } });
    self.selectedPageValue = ko.observable();
    self.selectedPageValue(selected_page_value);
    self.showMoreOption = ko.observable();
    self.showSearchField = ko.observable();
    self.searchValue = ko.observable();
    self.prevSearchValue = ko.observable('');
    self.searchValue(((search_value != undefined && search_value != null) ? search_value : ""));
    self.selectedFilterOption = ko.observable();
    self.selectedFilterOption('emailAddress');
};

var jobImageInfo = function (data_info) {
    var self = this;
    self.name = ko.observable(data_info.name);
    self.value = ko.observable(data_info.value);
    self.type = ko.observable(data_info.type);
    self.SelectedImage = ko.computed(function () {
        var thumb_nail = getThumbnailPath(logoPath);
        if (data_info.name == "base1") {
            thumb_nail += getValueExists(self.value()) ? (gOutputData.templateName + "/" + self.value()) : "missing_image.png";
        }
        else {
            thumb_nail += getValueExists(self.value()) ? self.value() : "missing_image.png";
        }
        
        return thumb_nail;
    });
    self.Title = ko.computed(function () {
        return getValueExists(self.value()) ? getImageDisplayName(self.value()) : ("Click to add " + (self.name() == "base1" ? "Base Artwork" : "Logo"));
    });
};


var jobLoanOfficer = function (caption, data_info) {
    var self = this;
    self.caption = caption;
    self.firstName = ko.observable((data_info != undefined && data_info != null && data_info != "") ? data_info.firstName : "");
    self.lastName = ko.observable((data_info != undefined && data_info != null && data_info != "") ? data_info.lastName : "");
    self.nmls = ko.observable((data_info != undefined && data_info != null && data_info != "") ? data_info.nmls : "");
    self.emailAddress = ko.observable((data_info != undefined && data_info != null && data_info != "") ? data_info.emailAddress : "");
    self.loThumbnailImg = ko.observable();
    var thumb_nail = getThumbnailPath(logoPath);
    thumb_nail += (data_info != undefined && data_info != null && data_info != "" && data_info.loThumbnailImg != undefined && data_info.loThumbnailImg != null && data_info.loThumbnailImg != "") ? data_info.loThumbnailImg : "missing_image.png";
    self.loThumbnailImg(thumb_nail);
    self.fullName = ko.computed(function () {
        var temp_fields = "";
        temp_fields = self.firstName() + ' ' + self.lastName();
        temp_fields = ($.trim(temp_fields) == "" && (showClicktoDefaultTitle())) ? "Click to " + ((showAddAnImage()) ? "add an Image" : (showMakeASelection()) ? "make a selection" : "add a Loan Officer") : temp_fields;
        return temp_fields;
    });
}

var optionalLogosVM = function (logo_name, image_name, is_checked) {
    var self = this;
    self.logoName = ko.observable('');
    self.logoName(logo_name);
    self.logoFileName = ko.observable('');
    self.logoFileName(image_name);
    self.isChecked = ko.observable();
    self.isChecked(is_checked);
};


var loanOfficer = function (user_id, first_name, last_name) {
    var self = this;
    self.loanOfficerId = user_id;
    self.loanOfficerName = first_name + ' ' + last_name;
};

var locationVm = function (store_id, store_name, store_pk) {
    var self = this;
    self.locationId = store_pk;
    if (store_id != undefined && store_id != null && store_id != "")
        self.locationName = store_id + ' - ' + store_name;
    else
        self.locationName = store_name;
};

function getThumbnailPath(thumb_nail_logo_path) {
    switch (jobCustomerNumber) {
        case AAG: thumb_nail_logo_path += "AAG/LO_Headshots/"; break;
        case DCA: thumb_nail_logo_path += "DCA/"; break;
        case SPORTSKING: thumb_nail_logo_path += "SportsKing/"; break;
        case KUBOTA: thumb_nail_logo_path += "Kubota/"; break;
        case GWA: thumb_nail_logo_path += "GWA/"; break;    
        case ALLIED: thumb_nail_logo_path += "Allied/"; break;
        case ANDERSEN: thumb_nail_logo_path += "Andersen/"; break;
        case SHERWINWILLIAMS: thumb_nail_logo_path += "SherwinWilliams/"; break;
        case MILWAUKEE: thumb_nail_logo_path += "Milwaukee/"; break;
        case BENJAMINMOORE: thumb_nail_logo_path += "BenjaminMoore/"; break;
        case TRAEGER: thumb_nail_logo_path += "Traeger/"; break;
    }

    return thumb_nail_logo_path;
}

var makeLoOfficers = function () {
    var self = this;
    self.emailsList = ko.observableArray([]);
    self.selectedEmailListItem = ko.observable({});
    self.selectedLoanOfficerToEdit = ko.observable({});
    self.prevSelectedEmailListItem = {};
    self.selectedEmailType = ko.observable();
    self.emailsQuantity = ko.observable();
    self.sendEmailType = ko.observable();
    self.emailTypeMode = ko.observable();
    self.sendEmailsList = ko.observableArray([]);
    self.loanOffiersList = ko.observableArray([]);
    self.selectedLoanOfficer = ko.observable({});
    self.inHomeDatesList = ko.observableArray([]);
    self.currentInHome = ko.observable();
    self.selectedImagesInfo = ko.observable({});
    self.jobLoInfo = ko.observable({});
    self.isReplicated = ko.observable();
    self.locationsList = ko.observableArray([]);
    self.selectedLocation = ko.observable({});
    self.selectedStore = ko.observable('');
    self.previousSelectedStore = ko.observable({});
    self.optionalLogos = ko.observableArray([]);
    var temp_arr = ko.observableArray([]);
    var temp_list = {};
    var temp_list_sk = [];
    var temp_key = "";
    var evt_key = "";

    self.showOrderedLocations = function () {
        $('#popUpLocations').popup('open');
    };

    self.getPageDisplayName = function (page_name) {
        var PATTERN = /^[ABCDEFGHIJKLMNOPQRSTUVWXYZ]/;
        var page_display_name = '';
        $.each(new String(page_name), function (key, value) {
            if (key > 0 && value.match(PATTERN)) {
                page_display_name += ((!page_display_name.endsWith(' ') && !page_display_name.endsWith('-')) ? ' ' : '') + value;
            }
            else {
                page_display_name += value;
            }
        });
        return page_display_name;
    };

    if (gOutputData != undefined && gOutputData != null) {
        var nav_bar_items = '<div data-role="navbar" class="ui-body-o" data-iconpos="right"><ul>';
        var cnt = -1;
        //if (sessionStorage.setUpTwoDate != undefined && sessionStorage.setUpTwoDate != null && sessionStorage.setUpTwoDate != "" && JSON.parse(sessionStorage.setUpTwoDate) == true) {
        if (jobCustomerNumber == AAG) {
            $.each(gOutputData.selectImagesAction, function (key, val) {
                if (key.toLowerCase().indexOf('event') > -1) {
                    var temp_list = {};
                    cnt++;
                    temp_list[key] = "Event " + (cnt + 1);
                    self.inHomeDatesList.push({
                        "showCheckIcon": false,
                        'dateValue': "Event " + (cnt + 1)
                    });

                    if (cnt == 0)
                        nav_bar_items += '<li><a href="#" data-icon="check">Event ' + (cnt + 1) + ' </a></li>';
                    else
                        nav_bar_items += '<li><a href="#" data-icon="edit">Event ' + (cnt + 1) + ' </a></li>';
                }
            });
        }
        else if (jobCustomerNumber == DCA || jobCustomerNumber == ALLIED) {
            $.each(gOutputData, function (key, val) {
                if (key.toLowerCase().indexOf('inhome') > -1) {
                    var temp_list = {};
                    temp_list[key] = val;
                    self.inHomeDatesList.push({
                        "showCheckIcon": false,
                        'dateValue': val
                    });
                    cnt++;
                    if (cnt == 0)
                        nav_bar_items += '<li><a href="#" data-icon="check">' + val + ' In Home</a></li>';
                    else
                        nav_bar_items += '<li><a href="#" data-icon="edit">' + val + ' In Home</a></li>';
                }
            });
            if (jobCustomerNumber == ALLIED) {

                $.each(gServiceOptionalLogos, function (key, val) {
                    var optional_logo_path = logoPath;
                    optional_logo_path += appPrivileges.companyName;
                    var temp_name = gOutputData.templateName.substring(1);
                    optional_logo_path += "/" + temp_name.substring(0, temp_name.indexOf('_'));
                    optional_logo_path += "/" + "OptionalLogos";
                    optional_logo_path += "/" + val.logoFileName;

                    self.optionalLogos.push(new optionalLogosVM(val.logoName, optional_logo_path, val.isChecked));
                });
            }
        }
        if (!isLogoBaseArtworkCustomers()) {
            nav_bar_items += '</ul></div>';
            self.currentInHome(self.inHomeDatesList()[0]);
        }
    }
    if (isNewVariableFieldListCustomers()) {
        var image_dict = [];
        $.each(gOutputData.variableImageAction.variableFieldList, function (k, v) {
            if ((gOutputData.templateName.toLowerCase() != "_endlessyes_8_5x11") || (gOutputData.templateName.toLowerCase() == "_endlessyes_8_5x11" && v.name != "base1")) {
                image_dict.push(new jobImageInfo(v));
            }
        });
        self.jobLoInfo(image_dict);
    }
    else {
        var source = (isOldVariableDictCustomers()) ? gOutputData.variableImageAction : gOutputData.selectImagesAction;
        if (source) {
            $.each(source, function (key1, val1) {
                if (key1 != "isReplicated" && key1 != " isEnabled" && key1 != "name" && key1 != "type" && (isOldVariableDictCustomers())) {
                    lo_arr = [];
                    var idx = 0;
                    $.each(val1, function (key2, val2) {
                        //if (jobCustomerNumber == SPORTSKING) {
                        var img = (isOldVariableDictCustomers()) ? (val2.indexOf('/') > -1 ? val2.substring(val2.lastIndexOf('/') + 1) : val2) : val2.substring(val2.lastIndexOf('/') + 1, val2.length).substring(0, (val2.substring(val2.lastIndexOf('/') + 1, val2.length).lastIndexOf('.')));
                        //}
                        var temp_obj = {};
                        temp_obj["firstName"] = img;
                        temp_obj["lastName"] = "";
                        temp_obj["nmls"] = "";
                        temp_obj["emailAddress"] = "";
                        if ((isOldVariableDictCustomers()) && val2 != undefined && val2 != "") {

                            if (isBronzeImageList()) {
                                var img_name = "";
                                var template_name = ((key2 == 'image2' && val2.toLowerCase().indexOf(gOutputData.templateName.toLowerCase()) == -1) ? '/' + gOutputData.templateName : '');
                                if (val2.indexOf('logo') > -1) {
                                    template_name = template_name.substring(2, template_name.length);
                                    template_name = template_name.substring(0, template_name.indexOf('_'));
                                    img_name = val2 + (val2.indexOf('.png') > -1 || val2.indexOf('.jpg') > -1 ? '' : '.png');;
                                }
                                else {
                                    img_name = 'bronze/GWBerkheimer/' + val2 + '.jpg';
                                }
                                template_name = template_name + '/' + img_name;
                                temp_obj["loThumbnailImg"] = template_name;
                            }
                            else
                                temp_obj["loThumbnailImg"] = ((key2 == 'image2' && val2.toLowerCase().indexOf(gOutputData.templateName.toLowerCase()) == -1) ? '/' + gOutputData.templateName : '') + '/' + val2 + (val2.indexOf('.png') > -1 || val2.indexOf('.jpg') > -1 ? '' : '.png');
                        }
                        temp_key = self.getPageDisplayName(img);
                        lo_arr.push(new jobLoanOfficer(temp_key, temp_obj));
                        if ((idx + 1) == Object.keys(val1).length) {
                            temp_list_sk = lo_arr;
                            lo_arr = [];
                        }
                        idx++;
                    });
                }
                else if (!isOldVariableDictCustomers()) {
                    if (key1 != "isReplicated") {
                        evt_key = key1;
                        var lo_arr = [];
                        $.each(val1, function (key2, val2) {
                            lo_arr = [];
                            $.each(val2, function (key3, val3) {

                                var full_name = val3.firstName.initCap() + ' ' + val3.lastName.initCap();
                                temp_key = self.getPageDisplayName(full_name);
                                lo_arr.push(new jobLoanOfficer(temp_key, val3));

                                if ((key3 + 1) == val2.length && val2.length == 1 && jobCustomerNumber == AAG) {
                                    var temp_val = { "firstName": "", "lastName": "", "address": "", "city": "", "state": "", "zip": "", "phone": "", "email": "", "userName": "", "userId": "1", "biography": "", "nmls": "" };
                                    lo_arr.push(new jobLoanOfficer("Loan Officer 2", temp_val));
                                }
                                if ((jobCustomerNumber == DCA || jobCustomerNumber == GWA) && gOutputData.templateName.indexOf('_11x6_') > -1 && key3 == 0) {
                                    return false;
                                }
                            });
                            if (temp_list[evt_key] == undefined || temp_list[evt_key] == null)
                                temp_list[evt_key] = {};
                            if (temp_list[evt_key][key2] == undefined || temp_list[evt_key][key2] == null)
                                temp_list[evt_key][key2] = {};
                            temp_list[evt_key][key2] = lo_arr;
                            lo_arr = [];

                        });
                    }
                }
            });
            if (Object.keys(temp_list).length > 0)
                self.jobLoInfo(temp_list);
            else
                self.jobLoInfo(temp_list_sk);
        }
    }
    

    self.loanOfficerChanged = function () {
        loData = jobLOData[self.selectedLoanOfficer()];
        //if (sessionStorage.selectedLoanOffierToEdit != undefined && sessionStorage.selectedLoanOffierToEdit != null && sessionStorage.selectedLoanOffierToEdit != "") {
        //    temp_image_info = $.parseJSON(sessionStorage.selectedLoanOffierToEdit);
        //    loData = temp_image_info.loInfo;
        //    $.each(self.inHomeDatesList(), function (key, val) {
        //        if (val.dateValue == temp_image_info.selectedInHome) {
        //            self.currentInHome(val);
        //            return false;
        //        }
        //    });
        //    $.each(self.jobLoInfo(), function (key1, val1) {
        //        if (jobCustomerNumber == AAG) {
        //            if (key1 == temp_image_info.selectedInHome) {
        //                $.each(val1, function (key2, val2) {
        //                    if (key2 == temp_image_info.selectedLoanOfficerId) {
        //                        $.each(val2, function (key3, val3) {
        //                            if (key3 == temp_image_info.loIndex) {
        //                                var temp;
        //                                var full_name = temp_image_info.loInfo[key3].firstName.initCap() + ' ' + temp_image_info.loInfo[key3].lastName.initCap();
        //                                temp_key = self.getPageDisplayName(full_name);
        //                                temp = new jobLoanOfficer(temp_key, temp_image_info.loInfo[key3]);
        //                                val2[key3] = temp;
        //                                return false;
        //                            }
        //                        });
        //                        return false;
        //                    }
        //                });
        //            }
        //        }
        //        else if (jobCustomerNumber == DCA) {
        //            if (key1 == self.currentInHome().dateValue) {
        //                $.each(val1, function (key2, val2) {
        //                    $.each(val2, function (key3, val3) {
        //                        if (key3 == temp_image_info.loIndex) {
        //                            if (temp_image_info.loInfo[key3] != undefined && temp_image_info.loInfo[key3] != null) {
        //                                var temp;
        //                                var full_name = temp_image_info.loInfo[key3].firstName.initCap() + ' ' + temp_image_info.loInfo[key3].lastName.initCap();
        //                                temp_key = self.getPageDisplayName(full_name);
        //                                temp = new jobLoanOfficer(temp_key, temp_image_info.loInfo[key3]);
        //                                val2[key3] = temp;
        //                                return false;
        //                            }
        //                        }
        //                    });
        //                });
        //            }
        //        }
        //    });
        //    sessionStorage.removeItem("selectedLoanOffierToEdit");
        //}
        var temp_key_email = "";
        self.emailsList.removeAll();
        $.each(self.jobLoInfo(), function (key1, val1) {
            if (isNewImageActionVariableFieldList()) {
                self.emailsList.push(val1);
            }
            else {
                if (key1 == self.currentInHome().dateValue) {
                    $.each(val1, function (key2, val2) {
                        if (key2 == self.selectedLoanOfficer()) {
                            $.each(val2, function (key3, val3) {
                                self.emailsList.push(val3);
                                if (key3 == 0)
                                    temp_key_email = val3.caption;
                            });
                        }
                    });
                }
            }
        });

        //TO BE REMOVED FROM HERE -- START
        //var temp_key_email = "";
        //var temp_email_list = [];
        //self.emailsList.removeAll();
        //$.each(loData, function (key, val) {
        //    if (self.selectedLoanOfficer() == val.id) {
        //        var full_name = val.firstName.initCap() + ' ' + val.lastName.initCap();
        //        temp_key_email = self.getPageDisplayName(full_name);
        //    }
        //    self.emailsList.push(new jobLoanOfficer(temp_key_email, val));
        //    if ((key + 1) == loData.length && loData.length == 1 && jobCustomerNumber == AAG) {
        //        var temp_val = { "firstName": "", "lastName": "", "address": "", "city": "", "state": "", "zip": "", "phone": "", "email": "", "userName": "", "userId": "1", "biography": "", "nmls": "" };
        //        self.emailsList.push(new jobLoanOfficer("Loan Officer 2", temp_val));
        //    }
        //    if (jobCustomerNumber == DCA && gOutputData.templateName.indexOf('_11x6_') > -1 && key == 0) {
        //        return false;
        //    }
        //});
        //TO BE REMOVED FROM HERE -- END
        $('#dvEmailRecipients').listview('refresh');
        displayJobInfo();
        var header_text = $('#navHeader').text();
        header_text = header_text + ((temp_key_email != "") ? ' (' + temp_key_email + ')' : '');
        $('#navHeader').text(header_text);
        if (!isLogoBaseArtworkCustomers())
            $('#dvNavBar div ul li a[data-inHome="' + self.currentInHome().dateValue + '"]').addClass('ui-btn-active');
    };

    if (sessionStorage.loanOffiersList != undefined && sessionStorage.loanOffiersList != null && sessionStorage.loanOffiersList != "") {
        var lo_list = [];
        lo_list = $.parseJSON(sessionStorage.loanOffiersList);
        $.each(lo_list, function (key, val) {
            if (key == 0) {
                if (sessionStorage.selectedLoanOffierToEdit != undefined && sessionStorage.selectedLoanOffierToEdit != null && sessionStorage.selectedLoanOffierToEdit != "") {
                    temp_image_info = $.parseJSON(sessionStorage.selectedLoanOffierToEdit);
                    self.selectedLoanOfficer(temp_image_info.selectedLoanOfficerId);
                }
                else { self.selectedLoanOfficer(val.userId); }

            }
            self.loanOffiersList.push(new loanOfficer(val.userId, val.firstName, val.lastName));
        });
        self.loanOfficerChanged();
    }
    else {
        if (sessionStorage.jobSetupOutput != undefined && sessionStorage.jobSetupOutput != null && sessionStorage.jobSetupOutput != "") {
            if (gOutputData == undefined || gOutputData == null || gOutputData == "") gOutputData = $.parseJSON(sessionStorage.jobSetupOutput);
            if ((gOutputData.selectedLocationsAction != undefined && gOutputData.selectedLocationsAction != null && gOutputData.selectedLocationsAction != "")) {
                if (jobCustomerNumber != ALLIED) {
                    $.each(gOutputData.selectedLocationsAction.selectedLocationsList, function (key, val) {
                        if (key == 0) {
                            self.selectedLoanOfficer(val.pk);
                        }
                        self.loanOffiersList.push(new loanOfficer(val.pk, val.storeName, val.storeId));
                    });
                    self.loanOfficerChanged();
                }
                else {
                    if (sessionStorage.jobSetupOutput != undefined && sessionStorage.jobSetupOutput != null && sessionStorage.jobSetupOutput != "") {
                        gOutputData = $.parseJSON(sessionStorage.jobSetupOutput);
                        if ((gOutputData.selectedLocationsAction != undefined && gOutputData.selectedLocationsAction != null && gOutputData.selectedLocationsAction != "") && (gOutputData.selectedLocationsAction.selectedLocationsList.length > 0)) {
                            $.each(gOutputData.selectedLocationsAction.selectedLocationsList, function (key, val) {
                                if (key == 0) {
                                    self.selectedLocation(val.pk);
                                    self.selectedStore(val.pk);
                                    self.previousSelectedStore(self.selectedStore());
                                }
                                self.locationsList.push(new locationVm(val.storeId, ((val.storeName) ? val.storeName : ((val.location) ? val.location : "")), val.pk));
                            });
                            self.loanOfficerChanged();
                        }
                    }
                }
            }
        }
    }

    //var temp_key_email = "";
    //var temp_email_list = [];
    //$.each(loData, function (key, val) {
    //    temp_key_email = self.getPageDisplayName(key.initCap());
    //    self.emailsList.push(new loRecipientGroup(temp_key_email, val, 0, ''));
    //});

    self.navBarItemClick = function (data, event) {
        if (jobCustomerNumber != ALLIED) {
            //self.validateForDataExistency();
            var ele = event.target || event.targetElement;
            $('#dvNavBar div ul li a').removeClass('ui-btn-active');
            $(ele).addClass('ui-btn-active');
            //if ((gOutputData.selectedLocationsAction != undefined && gOutputData.selectedLocationsAction != null && gOutputData.selectedLocationsAction != "") && (gOutputData.selectedLocationsAction.selectedLocationsList.length > 1)) {
            self.emailsList.removeAll();
            //}
            self.currentInHome(ko.dataFor(ele));
            self.loanOfficerChanged();
        }
    }

    self.replicateChanged = function (data, event) {
        var ele = event.target || event.targetElement;
        $('#confirmMsg').html('Replicating Setup Entries will overwrite any previous images selected.');
        $('#okButConfirm').text('Continue');

        //var curr_info = ko.toJS(self.varCompanyInfo());
        //var prev_info = ko.toJS(self.prevVarCompanyInfo());
        var is_diff_found = false;
        //$.each(curr_info, function (key1, val1) {
        //    $.each(prev_info, function (key2, val2) {
        //        if (key1 == key2) {
        //            $.each(val1, function (key3, val3) {
        //                var curr_temp1 = val3;
        //                var prev_temp2 = val2[key3];
        //                diff_between_json_objects = DiffObjects(jQuery.parseJSON(JSON.stringify(curr_temp1)), jQuery.parseJSON(JSON.stringify(prev_temp2)));
        //                if (diff_between_json_objects.length != 0) {
        //                    is_diff_found = true;
        //                    return false;
        //                }
        //            });
        //        }
        //        if (is_diff_found) {
        //            return false;
        //        }
        //    });
        //    if (is_diff_found) {
        //        return false;
        //    }
        //});
        is_diff_found = true;
        $('#cancelButConfirm').bind('click', function () {
            $('#cancelButConfirm').unbind('click');
            $('#okButConfirm').unbind('click');
            $('#cancelButConfirm').attr('onclick', '$(\'#popupConfirmDialog\').popup(\'close\');');
            self.isReplicated(false);
            $('#chkReplicate').checkboxradio('refresh');
            $('#popupConfirmDialog').popup('close');
        });
        $('#okButConfirm').bind('click', function () {
            $('#cancelButConfirm').unbind('click');
            $('#okButConfirm').unbind('click');
            $('#cancelButConfirm').attr('onclick', '$(\'#popupConfirmDialog\').popup(\'close\');');
            self.replicateData();
            self.isReplicated(true);
            $('#chkReplicate').checkboxradio('refresh');
            $('#popupConfirmDialog').popup('close');
        });
        if ($(ele).is(':checked') && is_diff_found) {
            $('#popupConfirmDialog').popup('open');
        }
    };
    self.replicateData = function () {
        var selected_inhome = self.currentInHome().dateValue;
        //$.each(self.selectedInhomeInfo(), function (key1, val1) {
        //    var temp_key = key1;
        //    $.each(self.varCompanyInfo(), function (key2, val2) {
        //        if (selected_inhome != key2) {
        //            //val2[temp_key](val1());
        //            val2[temp_key] = val1;
        //        }
        //    });
        //});


        //self.varCompanyInfo().isReplicated(true);
        //var temp = ko.toJS(self.varCompanyInfo());
        //self.prevVarCompanyInfo(temp);
        //$.each(temp, function (key, val) {
        //    if (key != "isReplicated") {
        //        var is_valid = self.verifyData(val);
        //        if (is_valid) {
        //            $('#dvNavBar div ul li a[data-inHome="' + key + '"]').attr('data-icon', 'check').addClass('ui-icon-check').removeClass('ui-icon-edit');
        //        }
        //        else {
        //            $('#dvNavBar div ul li a[data-inHome="' + key + '"]').attr('data-icon', 'edit').addClass('ui-icon-edit').removeClass('ui-icon-check');
        //        }
        //    }
        //});
    };

    //Clicking on Edit Icon, navigates to image library page.   
    self.navigate = function (data, event) {

        var ctrl = event.target || event.currentTarget || event.targetElement;
        if (ctrl.localName != "a") ctrl = $(ctrl).parent();
        self.selectedLoanOfficerToEdit(data);
        var temp_edit_info = {};

        temp_edit_info["loIndex"] = $(ctrl).attr('data-Index');
        var temp_lo_info = [];

        //temp_edit_info["loInfo"] = loData;
        if (isLogoBaseArtworkCustomers())
            temp_edit_info["loInfo"] = gOutputData.variableImageAction;
        else {
            temp_edit_info["loInfo"] = gOutputData.selectImagesAction; //ko.toJS(self.emailsList); //ko.toJS(data);
            temp_edit_info["selectedLoanOfficerId"] = self.selectedLoanOfficer();
        }

        //if (jobCustomerNumber == AAG) {
        if (!isLogoBaseArtworkCustomers())
            temp_edit_info["selectedInHome"] = self.currentInHome().dateValue;
        //}

        sessionStorage.selectedLoanOffierToEdit = JSON.stringify(temp_edit_info);
        window.location.href = "jobImageLibrary.html"
    };

    //Displays Wait image while opening preview.
    self.displayOverlay = function () {
        $('#waitPopUp').popup('open', { positionTo: 'window' });
        $('#overlay').css({ top: '40%', left: '50%', margin: '-' + ($('#overlay').height() / 2) + 'px 0 0 -' + ($('#overlay').width() / 2) + 'px' });
    }

    self.openArtworkFile = function (data, event) {
        //self.displayOverlay();
        $('#waitPopUp').popup('open', { positionTo: 'window' });
        var content_type = 'application/pdf';
        var pdf_preview_url = getThumbnailPath(logoPath) + jQuery.parseJSON(sessionStorage.jobSetupOutput).templateName + "/" + data.loThumbnailImg().substring(data.loThumbnailImg().lastIndexOf('/')).replace('png', 'pdf');
        var htmlText = "";
        if (content_type == 'application/pdf') {
            htmlText = "<embed width=850 height=500 type='" + content_type + "' src='" + pdf_preview_url + "' id='selectedFile'></embed>";
        } else {
            htmlText = "<img src='" + pdf_preview_url + "' id='selectedFile'></img>";
        }
        $('#popupPDFContent').html(htmlText);

        window.setTimeout(function getDelayNDisplayFile() {
            if ($('#selectedFile')[0].clientWidth > 0 && $('#selectedFile')[0].clientHeight > 0)
                var display = $("#popPDF");
            if (display != undefined) {
                display.scroll(function () {

                    var ctrls = $("#popPDF img");
                    console.log("************************");
                    ctrls.each(function (index) {
                        console.log($(this).position().top);

                        if ($(this).position().top < 0)
                            $(this).css("visibility", "hidden")//.hide();
                        else {
                            $(this).css("visibility", "")//.show();
                        }
                    });
                });
            }

            window.setTimeout(function getDelay() {
                $('#waitPopUp').popup('close');
                $('#popPDF').popup('open', { positionTo: 'window' });
            }, 200);
        }, 5000);
    }

    self.removeUser = function (data, event) {
        if ([SPORTSKING, ANDERSEN, KUBOTA].indexOf(jobCustomerNumber) > -1) {
            if (data.SelectedImage().indexOf('missing_image.png') > -1) return false;
            var ctrl = event.target || event.currentTarget || event.targetElement;
            var img_idx = $(ctrl).attr('data-Index');
            $('#confirmMsg').text('Are you sure you want to delete the image?');
            $('#okButConfirm').bind('click', function () {
                data.value("");
                $.each(gOutputData.variableImageAction.variableFieldList, function (k, v) {
                    if (v.name == data.name()) {
                        v.value = "";
                    }
                });
                sessionStorage.jobSetupOutput = JSON.stringify(gOutputData);
                $('#popupConfirmDialog').popup('close');
            });
            $('#popupConfirmDialog').popup('open');
        }
        else {
            if (data.loThumbnailImg().indexOf('missing_image.png') > -1) return false;
            var ctrl = event.target || event.currentTarget || event.targetElement;
            if (ctrl.localName != "a") ctrl = $(ctrl).parent();
            var img_idx = $(ctrl).attr('data-Index');
            $('#confirmMsg').text('Are you sure you want to delete the image?');
            $('#okButConfirm').bind('click', function () {
                data.caption = "";
                data.firstName("");
                data.lastName("");
                data.loThumbnailImg(getThumbnailPath(logoPath) + "missing_image.png");
                if (data.emailAddress != undefined && data.emailAddress != null) data.emailAddress('');
                if (isDeleteConfirmCustomers()) {
                    gOutputData.variableImageAction.variableDict["image" + (parseInt(img_idx) + 1)] = "";
                    sessionStorage.jobSetupOutput = JSON.stringify(gOutputData);
                    $('#btnSaveTop').trigger('click');
                }
                $('#popupConfirmDialog').popup('close');
            });
            $('#popupConfirmDialog').popup('open');
        }
    };

    self.showLoanOfficers = function () {
        $('#popUpLoanOfficers').popup('open');
    };

    self.previewMailing = function () {
        if (isNewImageActionVariableFieldList()) {
            previewMailing();
            return false;
        }
        else if (appPrivileges.customerNumber == DCA) {
            encodedFileName = "DCA%2041941%20Jupiter%20Dental%20_Postcard_Dr.%20Vera_MECH.pdf";
            if (gOutputData.templateName.indexOf('_11x6_') > -1) {
                encodedFileName = "DCA_42754_DoubleDentRite_DM_Jupiter.pdf";
            }
        }
        if (appPrivileges.customerNumber == AAG) {
            encodedFileName = "TCA_10232_AAG_Lunch_Learn_2_Presenters-2_Events_Lenfers_Culbreth_Let.pdf";
        } else if (appPrivileges.customerNumber == ALLIED) {
            encodedFileName = "GW_Berkheimer_Jansen.pdf";
        }
        getNextPageInOrder();
        //var preview_proof = "../jobProofing/jobProofing.html?fileName=58683_GWBerkheimerOLSONCOMFORT.pdf&jobNumber=" + sessionStorage.jobNumber;
        var preview_proof = "../jobApproval/previewMailing.html?fileName=" + encodedFileName + "&jobNumber=" + sessionStorage.jobNumber;
        window.location.href = preview_proof;
    }

    self.persistSelectedOfferInfo = function () {
        if (jobCustomerNumber == GWA) {

        }
    };

    self.confirmContinue = function (current_page, page_name, i_count, click_type) {
        //if (jobCustomerNumber == SPORTSKING) {
        //    self.persistSelectedOfferInfo();
        return self.updateImageSelectionInfo(current_page, page_name, i_count, click_type);
        //}
    };

    self.updateImageSelectionInfo = function (current_page, page_name, i_count, click_type) {
        // Call validation function to validate the data in the fields.  If it returns true proceed further else stop further execution.
        var temp_json = ko.mapping.toJS(self.emailsList);
        var output_json = [];
        if ([ANDERSEN, SPORTSKING, KUBOTA].indexOf(jobCustomerNumber) > -1) {
            var image_names = ["logo1", "base1"];
            $.each(temp_json, function (k, v) {
                if (v.value != "") {
                    image_names = jQuery.grep(image_names, function (value) {
                        return value != v.name;
                    });
                }
            });
            if (image_names.length == 2) {
                var msg = "Please add logo and base artwork.";
                if (gOutputData.templateName.toLowerCase() == "_endlessyes_8_5x11" || [KUBOTA].indexOf(jobCustomerNumber) > -1) {
                    msg = "Please add " + ([KUBOTA].indexOf(jobCustomerNumber) > -1 ? "company " : "custom ") + "logo";
                }
                $('#alertmsg').text(msg);
                $('#popupDialog').bind('okBut', function () {
                    $('#popupDialog').popup('close');
                });
                $('#popupDialog').popup('open');
                return false;
            }
            else if (image_names.length == 1 && gOutputData.templateName.toLowerCase() != "_endlessyes_8_5x11" && [KUBOTA].indexOf(jobCustomerNumber) == -1) {
                var image_to_add = (image_names[0] == "logo1" ? (([KUBOTA].indexOf(jobCustomerNumber) > -1 ? "company " : "custom ") + "logo") : "base artwork");
                $('#alertmsg').text("Please add " + image_to_add);
                $('#popupDialog').bind('okBut', function () {
                    $('#popupDialog').popup('close');
                });
                $('#popupDialog').popup('open');
                return false;
            }
            else {
                var is_images_updated = false;
                var output_json = [];
                var g_output_data_compare = $.parseJSON(sessionStorage.jobSetupOutputCompare);
                var output_compare_list = g_output_data_compare.variableImageAction.variableFieldList;
                $.each(temp_json, function (k, v) {
                    if (is_images_updated) {
                        return false;
                    }
                    else {
                        $.each(output_compare_list, function (k1, v1) {
                            if (v.name == v1.name && v.value != v1.value) {
                                is_images_updated = true;
                                return false;
                            }
                        });
                    }
                });
                if (is_images_updated && click_type != "save") {
                    var message = "Changes to the Select Images have not been saved. Please save or discard your changes now.";
                    $('#confirmMsg').html(message);
                    $('#okButConfirm').text('Save');
                    $('#cancelButConfirm').text('Discard');
                    $('#okButConfirm').bind('click', function () {
                        $('#cancelButConfirm').unbind('click');
                        $('#okButConfirm').unbind('click');
                        $('#cancelButConfirm').attr('onclick', '$(\'#popupConfirmDialog\').popup(\'close\');');
                        $('#okButConfirm').text('Ok');
                        $('#cancelButConfirm').text('Cancel');
                        $('#popupConfirmDialog').popup('close');
                        $.each(temp_json, function (k, v) {
                            delete v["SelectedImage"];
                            delete v["Title"];
                            output_json.push(v);
                        });
                        gOutputData.variableImageAction.variableFieldList = output_json;
                        gOutputData.isGmcDataUpdated = true;
                        sessionStorage.jobSetupOutput = JSON.stringify(gOutputData);
                        postJobSetUpData('save');
                        sessionStorage.isJobTicketUpdate = true;
                        if (click_type == "onlycontinue" || click_type == undefined)
                            window.location.href = (page_name != undefined && page_name != null && page_name != "") ? page_name : current_page;
                        return true;
                    });
                    $('#cancelButConfirm').bind('click', function () {
                        $('#cancelButConfirm').unbind('click');
                        $('#okButConfirm').unbind('click');
                        $('#cancelButConfirm').attr('onclick', '$(\'#popupConfirmDialog\').popup(\'close\');');
                        $('#okButConfirm').unbind('click');
                        $('#okButConfirm').text('Ok');
                        $('#cancelButConfirm').text('Cancel');
                        $('#popupConfirmDialog').popup('close');
                        sessionStorage.jobSetupOutput = sessionStorage.jobSetupOutputCompare;
                        if (click_type == "onlycontinue" || click_type == undefined)
                            window.location.href = (page_name != undefined && page_name != null && page_name != "") ? page_name : current_page;
                        return true;
                    });
                    $('#popupConfirmDialog').popup('open');
                    disableSaveForClosedJobs();
                    return false;
                }
                else {
                    if (is_images_updated) {
                        gOutputData.isGmcDataUpdated = true;
                        sessionStorage.isJobTicketUpdate = true;
                        $.each(temp_json, function (k, v) {
                            delete v["SelectedImage"];
                            delete v["Title"];
                            output_json.push(v);
                        });
                        gOutputData.variableImageAction.variableFieldList = output_json;
                    }
                }
            }
        }
        return true;
    };

    self.manageLocationsPopup = function (data, event) {
        var ele = event.target || event.targetElement;
        //if ($(ele).attr('data-btnType') == "submit") {
        //    self.previousSelectedStore(self.selectedStore());
        //}
        //else {
        //    self.selectedStore(self.previousSelectedStore());
        //    self.selectedLocation(self.previousSelectedStore())
        //}
        $('#ddlLocations').selectmenu('refresh');
        displayJobInfo();
        var header_text = $('#navHeader').text();
        var sel_location = $('#ddlLocations option:selected').text();
        header_text = header_text + ' (' + sel_location + ')';
        $('#navHeader').text(header_text);
        $('#popUpLocations').popup('close');
    };
};

function getImageDisplayName(image_name) {
    return image_name.replace(".png", "").replace(".jpg", "").replace("logo", "").replace(/_/g, ' ').replace(/-/g, ' ').initCap();
}

String.prototype.endsWith = function (suffix) {
    return this.indexOf(suffix, this.length - suffix.length) !== -1;
};
//Capitalizes the first letter in the given string.
String.prototype.initCap = function () {
    return this.replace(/(?:^|\s)[a-z]/g, function (m) {
        return m.toUpperCase();
    });
};