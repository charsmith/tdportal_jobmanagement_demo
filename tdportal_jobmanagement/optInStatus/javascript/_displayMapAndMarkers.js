﻿var displayMapAndMarkers = function (self) {
    // Displays Markers for the given lat lng points and the type - e / o / n
    self.displayMarkers = function () {
        var displayed_markers = [];
        var type;
        $.each(self.markers_array, function (key, val) {
            if ((self.uIViewConfig[val.type.replace('-', '')] != undefined && self.uIViewConfig[val.type.replace('-', '')] != null && self.uIViewConfig[val.type.replace('-', '')]()) || val.type.indexOf('anonymous') > -1) {
                var map_center = map.getCenter();
                var marker;
                var status;
                var marker_number;
                val.markers = [];
                $.each(val.latlngs, function (a, b) {
                    status = val.addresses[a].substring(val.addresses[a].lastIndexOf('^') + 1);
                    marker_number = val.addresses[a].substring(0, val.addresses[a].lastIndexOf('^'));
                    marker_number = marker_number.substring(marker_number.lastIndexOf('^') + 1);
                    if (b.lat != "" && b.lng != "")
                        marker = self.createMarkerForPoint(this, val.type, marker_number, status);
                    else
                        marker = undefined;
                    if (marker != undefined && marker != null && marker != "")
                        val.markers.push({
                            marker: marker,
                            markerNo: marker_number
                        });

                    //if (marker != undefined) {
                    //    marker.setMap(map);
                    //    google.maps.event.addListener(marker, 'click', self.showInfoWindowExisting(a, val.type, val.addresses[a]), marker);
                    //}
                    //val.markers.push({
                    //    marker: marker,
                    //    markerNo: marker_number
                    //});
                });
                $.each(val.markers, function (a1, b1) {
                    if (b1.marker != undefined) {
                        this.marker.setMap(map);
                        google.maps.event.addListener(b1.marker, 'click', self.showInfoWindowExisting(a1, val.type, val.addresses[a1]));
                    }

                });
            }
        });
        var $mapDiv = $('#map-canvas');

        var mapDim = {
            height: $mapDiv.height(),
            width: $mapDiv.width()
        }
        var bounds = self.createBoundsForMarkers();
        window.setTimeout(function setMapBounds() {
            var markers_not_found_count = 0;
            var markers_count = 0;
            $.each(self.markers_array, function (key, val) {
                if (val.markers.length == 0)
                    markers_not_found_count++;
                else markers_count += val.markers.length;
            });
            if (markers_not_found_count == 7 || markers_count == 1 || markers_count == 0) {
                if (markers_count != 0) {
                    map.setCenter(((bounds) ? bounds.getCenter() : new google.maps.LatLng(0, 0)));
                    map.setZoom(17);
                }
                else
                    map.setZoom(7);
            }
            else {
                map.setCenter(((bounds) ? bounds.getCenter() : new google.maps.LatLng(0, 0)));
                map.setZoom(((bounds) ? self.getBoundsZoomLevel(bounds, mapDim) : 0));
            }
            window.setTimeout(function () {
                $('#waitPopUp').popup('close');

                window.setTimeout(function () {
                    if (!dontShowHintsAgain && (jobCustomerNumber == CASEYS || jobCustomerNumber == AAG) && (sessionStorage.showDemoHints == undefined || sessionStorage.showDemoHints == null || sessionStorage.showDemoHints == "" || sessionStorage.showDemoHints == "on") && (sessionStorage.isOptInStatusHintsDisplayed == undefined || sessionStorage.isOptInStatusHintsDisplayed == null || sessionStorage.isOptInStatusHintsDisplayed == "false")) {
                        sessionStorage.isOptInStatusHintsDisplayed = true;
                        $('#statusSummary').popup('close');
                            $('#statusSummary').popup('open', { positionTo: '#tblCheckOutrGrid' });
                    }
                }, 1000);

            }, 2000);
        }, 2000);
    };

    //Clears the existing markers based on the type provided - e/o/n
    self.clearMarkers = function () {
        $.each(self.markers_array, function (key, val) {
            for (var i = 0; i < val.markers.length; i++) {
                if (val.markers[i].marker)
                    val.markers[i].marker.setMap(null);
            }
            val.addresses = [];
            val.markers = [];
            val.latlngs = [];
        });
    };

    //Shows the info window on the marker when the user clicks on the marker.
    self.showInfoWindowExisting = function (i, type, address) {
        var current_markers = $.grep(self.markers_array, function (obj) {
            return obj.type.toLowerCase() === type.toLowerCase();
        });
        var address = current_markers[0].addresses[i];
        return function (address) {
            if (self.infowindow()) {
                self.infowindow().close();
                self.infowindow(null);
            }

            self.infowindow(new google.maps.InfoWindow({
                content: self.getIWContentExisting(i, type, address),
                height: '100px'
            }));
            self.infowindow().open(map, current_markers[0].markers[i].marker);
        }
    };
    //Gets the content required to display in the info window for a particular marker.
    self.getIWContentExisting = function (i, type, address) {
        var current_markers = $.grep(self.markers_array, function (obj) {
            return obj.type.toLowerCase() === type.toLowerCase();
        });
        var address_parts = current_markers[0].addresses[i].split('^');
        var content = '';
        content += '<table>';
        content += '<tr class="iw_table_row">';
        content += '<td colspan="2"><b>' + address_parts[1] + '</b></td></tr>';
        var address_contents = address_parts[0].split(',');
        content += '<tr class="iw_table_row"><td>' + address_contents[0] + '</td></tr>';
        content += '<tr class="iw_table_row"><td>' + $.trim(address_contents[1]) + '</td></tr>';
        content += '<tr class="iw_table_row"><td>' + $.trim(address_contents[2]) + '</td></tr>';
        content += '</table>';
        return content;
    };

    //Get the zoom level based on the provided bounds.
    self.getBoundsZoomLevel = function (bounds, mapDim) {
        //var WORLD_DIM = { height: 256, width: 256 };
        var WORLD_DIM = { height: 520, width: 256 };
        /*var WORLD_DIM ={};
        if ($.browser.msie) {
        WORLD_DIM = { height: 520, width: 520 };
        }
        else {
        WORLD_DIM = { height: 256, width: 256 };
        }*/
        var ZOOM_MAX = 21;

        function latRad(lat) {
            var sin = Math.sin(lat * Math.PI / 180);
            var radX2 = Math.log((1 + sin) / (1 - sin)) / 2;
            return Math.max(Math.min(radX2, Math.PI), -Math.PI) / 2;
        }

        function zoom(mapPx, worldPx, fraction) {
            return Math.floor(Math.log(mapPx / worldPx / fraction) / Math.LN2);
        }

        var ne = bounds.getNorthEast();
        var sw = bounds.getSouthWest();

        var latFraction = (latRad(ne.lat()) - latRad(sw.lat())) / Math.PI;

        var lngDiff = ne.lng() - sw.lng();
        var lngFraction = ((lngDiff < 0) ? (lngDiff + 360) : lngDiff) / 360;

        var latZoom = zoom(mapDim.height, WORLD_DIM.height, latFraction);
        var lngZoom = zoom(mapDim.width, WORLD_DIM.width, lngFraction);

        return Math.min(latZoom, lngZoom, ZOOM_MAX);
    };
    //Creates the bounds on the map for the displayed markers.
    self.createBoundsForMarkers = function (markers) {
        var bounds = new google.maps.LatLngBounds();
        $.each(self.markers_array, function (a, b) {
            $.each(b.markers, function (c, d) {
                if (d.marker != undefined)
                    bounds.extend(d.marker.getPosition());
            });
        });
        return bounds;
    };

    //Creates the marker on the map for the provided lat lng point.
    self.createMarkerForPoint = function (point, type, i, status) {
        var marker_no = 0;
        var temp_index = i.substring(0, i.indexOf('-'));
        var marker_color = "";
        marker_color = self.getMarkerColor(status);
        var marker_folder = "";
        marker_folder = (($('#ddlDisplayResultsBy').val() != "status" && (status.toLowerCase().replace('-', '') == "open" || status.toLowerCase().replace('-', '') == "available" || status.toLowerCase().replace('-', '') == "optedout")) || (type == "open" || type == "available" || type == "optedOut")) ? "e" : "o";

        marker_no = parseInt(temp_index) + 1;

        var icon = logoPath + 'mapMarkers/' + marker_folder + '/marker' + marker_folder.toUpperCase() + '_' + marker_color + marker_no + '.png';

        return new google.maps.Marker({
            position: new google.maps.LatLng(point.lat, point.lng),
            icon: icon
        });
    };

    self.getMarkerColorAndFolder = function (status) {
        var marker_color = "";
        marker_color = self.getMarkerColor(status);
        var marker_folder = "";
        marker_folder = (status.toLowerCase() == "open" || status.toLowerCase() == "available" || status.toLowerCase() == "opted-out") ? "e" : "o";
        var temp_info = {};
        temp_info["color"] = marker_color;
        temp_info["folder"] = marker_folder;
        return temp_info;
    };

    self.getMarkerColor = function (status) {
        var marker_color = "";
        switch (status.toLowerCase()) {
            case "available":
            case "open":
                marker_color = "blue";
                break;
            case "opted-out":
            case "declined":
                marker_color = "grey";
                break;
            case "opted-in":
                marker_color = "yellow";
                break;
            case "pending":
                marker_color = "orange";
                break;
            case "approved":
                marker_color = "red";
                break;
        }
        return marker_color;
    };

    //Shows the location details over the map when user clicks on the marker on the map/or on the marker image in the respectie locations list.
    self.showLocationDetails = function (type, index, data, event) {
        var current_markers = $.grep(self.markers_array, function (obj) {
            return obj.type === (($('#ddlDisplayResultsBy').val() == "status") ? type().replace('-', '').initLower() : ko.contextFor(event.srcElement).$parent.type());
        });
        if (current_markers[0].markers[index] != undefined) {
            google.maps.event.trigger(current_markers[0].markers[index].marker, 'click');
        }
        else {
            var address = current_markers[0].addresses[index]
            var store_id = address.split('^')[1];
            $('#alertmsg').text('Location ' + store_id + ' does not have a latitude or longitude and will not be displayed on the map.');
            $('#popupDialog').popup('open');
        }
    };
}
