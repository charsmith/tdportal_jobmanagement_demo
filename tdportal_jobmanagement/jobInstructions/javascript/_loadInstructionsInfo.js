﻿var loadJobInstructionsInfo = function () {
    var self = this;
    self.gOutputData =
    self.jobInstructionServiceData = [];
    self.getInstructionsData = function () {
        $.getJSON(jobInstructionsServiceUrl, function (data) {
            self.jobInstructionServiceData = data;
            new InstructionVM(self);
            ko.applyBindings(self);
            $("#tblComponent").table("refresh");
            $("#tblComponentProcess").table("refresh");
            $("#tblFinishing").table("refresh");
            $('select[id^=ddlComponent]').selectmenu().selectmenu('refresh', true);
        });
    };
    self.makeGOutputData = function () {
        if (sessionStorage.jobSetupOutput == undefined && sessionStorage.jobSetupOutput == undefined)
            $.getJSON(gOutputServiceUrl, function (dataOutput) {
                self.gOutputData = dataOutput;
                getBubbleCounts(self.gOutputData);
            });
        else {
            self.gOutputData = jQuery.parseJSON(sessionStorage.jobSetupOutput);
            getBubbleCounts(self.gOutputData);
        }
    };
};

