﻿//var componentSource = []; // for component dropdown
//var processSource = []; //for process dropdown
//var processFinishSource = []; //for finish dropdown

$.mobile.popup.prototype.options.history = false;
//jQuery.support.cors = true;
//******************** Global Variables Start **************************
var jobInstructionsServiceUrl = "JSON/_jobInstruction.JSON";
var gOutputServiceUrl = '../JSON/_jobSetupOutput.JSON';
var pageObj;
$('#_jobInstructions').live('pagebeforecreate', function (event) {
    displayNavLinks();
    displayMessage('_jobInstructions');
    //test for mobility...
    //loadMobility();
    $("#navLinksPanel").on("panelopen", function (event, ui) {
        sessionStorage.isNavPanelOpen = true;
    });
});

$(document).on('pageshow', '#_jobInstructions', function (event) {
    pageObj = new loadJobInstructionsInfo();
    pageObj.getInstructionsData();
    pageObj.makeGOutputData();
    persistNavPanelState();
    if (sessionStorage.jobSetupOutput != undefined) {
        pageObj.gOutputData = jQuery.parseJSON(sessionStorage.jobSetupOutput);
    }
    //buildOutputData();
});
