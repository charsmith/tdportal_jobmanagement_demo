﻿var appPrivileges = jQuery.parseJSON(getSessionData("appPrivileges"));

$.mobile.popup.prototype.options.history = false;
jQuery.support.cors = true;
var pageObj;
var dontShowHintsAgain = (localStorage.getItem('doNotShowHintsAgain') != undefined && localStorage.getItem('doNotShowHintsAgain') != null && localStorage.getItem('doNotShowHintsAgain') != "" && localStorage.getItem('doNotShowHintsAgain') != "false") ? true : false;

$('#_jobPieceAttributes').live('pagebeforecreate', function (event) {
    pageObj = new pieceAttrs();
    displayNavLinks();
    displayMessage('_jobPieceAttributes');
    commonCreateDemoHintBoxes('_jobPieceAttributes');
    commonCreateGizmoAlert('_jobPieceAttributes');

    !(appPrivileges.dispaly_advPresortOptRows != undefined && appPrivileges.dispaly_advPresortOptRows) ? $("#dvAdvPresortOptRows").hide() : "";  //getting info from authorization web service

    if (sessionStorage.pieceAttributesPrefs == undefined || sessionStorage.pieceAttributesPrefs == null || sessionStorage.pieceAttributesPrefs == "")
        if (isCWPieceAttributePreferences())
            getPagePreferences('pieceAttributes.html', sessionStorage.facilityId, CW);
        else
            getPagePreferences('pieceAttributes.html', sessionStorage.facilityId, jobCustomerNumber);
    else {
        managePagePrefs(jQuery.parseJSON(sessionStorage.pieceAttributesPrefs));
    }
   // loadMobility();
    if (sessionStorage.templates == undefined) {
        getCORS(gTemplateUrlForAll, null, function (data) {
            sessionStorage.templates = JSON.stringify(data);
        }, function (error_response) {
            showErrorResponseText(error_response,true);
        });
    }
    $("#navLinksPanel").on("panelopen", function (event, ui) {
        sessionStorage.isNavPanelOpen = true;
    });
    setDemoHintsSliderValue();
    createDemoHints("jobPieceAttributes");
});

$(document).on('pageshow', '#_jobPieceAttributes', function (event) {

    commonShowDemoHintAfterPageLoad('_jobPieceAttributes');

    var powerUserPrefs = ["spthickness", "thickness", "spweight", "weight", "spmailType", "mailType", "spentryPoint", "entryPoint", "spcrrtSort", "crrtSort", "spsortOrder", "sortOrder", "sprateType", "rateType", "sppstgPymnt", "pstgPymnt", "sppermit", "permit"];
    if (jobCustomerNumber == GWA)
        powerUserPrefs = ["spmailType", "mailType", "spentryPoint", "entryPoint", "spcrrtSort", "crrtSort", "spsortOrder", "sortOrder"];
    if (sessionStorage.userRole == "power") {
        if (sessionStorage.pieceAttributesPrefs != undefined || sessionStorage.pieceAttributesPrefs != null || sessionStorage.pieceAttributesPrefs != "") {
            var power_prefs = $.parseJSON(sessionStorage.pieceAttributesPrefs);
            $.each(powerUserPrefs, function (key, val) {
                power_prefs.buttonPrefsDict[val].showControl = 0;
            });
            sessionStorage.pieceAttributesPrefs = JSON.stringify(power_prefs);
            managePagePrefs(power_prefs);
        }
    }
    pagePrefs = jQuery.parseJSON(sessionStorage.pieceAttributesPrefs);

    if (!fnVerifyScriptBlockDict(pagePrefs))
        return false;
    if (pagePrefs != null) {
        pageObj.bindTemplates();
    }
    persistNavPanelState();
    if ((jobCustomerNumber != DCA && jobCustomerNumber != SPORTSKING) || ((jobCustomerNumber == DCA || jobCustomerNumber == SPORTSKING) && appPrivileges.roleName != "admin"))
        $('#dvAddressBlockPlacement').css('display', 'none');
    $('#sldrShowHints').slider("refresh");
    if (sessionStorage.userRole == "user" && jobCustomerNumber == CW) {
        $('#dvPieceAttributesDefaults').css('display', 'none');
    }
});
