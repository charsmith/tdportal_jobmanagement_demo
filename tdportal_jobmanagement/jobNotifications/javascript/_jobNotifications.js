﻿var mapping = { 'ignore': ["__ko_mapping__"] }
var notifications = [];
var reportsList = [];
var getNotificationsService = serviceURLDomain + "api/JobTicket_notification_action_get/" + sessionStorage.facilityId + "/" + jobCustomerNumber + "/" + sessionStorage.jobNumber;
var saveNotificationsService = serviceURLDomain + "api/JobTicket_notification_action_save/" + sessionStorage.facilityId + "/" + jobCustomerNumber + "/" + sessionStorage.jobNumber + "/";
var getReportingService = serviceURLDomain + "api/JobTicket_reporting_action_get/" + sessionStorage.facilityId + "/" + jobCustomerNumber + "/" + sessionStorage.jobNumber;

var notificationContent = {
    "attachedReportList": [],
    "emailDefinition": {
        "templateName": "",
        "subjectLine": "",
        "banner1": "",
        "banner2": "",
        "head": "",
        "emailMessage": "",
        "attention1": "",
        "attention2": "",
        "buttonLabel": "",
        "assistanceEmail": "GizmoTeam@conversionalliance.com",
        "disclaimer": "",
        "image": "",
        "testEmail": ""
    },
    "textDefinition": {
        "textMessage": "",
        "testPhoneNumber": ""
    },
    "textList": [],
    "emailList": [],
    "proofUserList": [],
    "forwardsUsername": "",
    "name": "",
    "type": "",
    "processType": "",
    "notifyEvent": "",
    "created": "",
    "createdBy": sessionStorage.username
}

var jobDesc = '';
jobDesc = getSessionData("jobDesc");


//Service call to load list of notifications.
getCORS(getNotificationsService, null, function (notifications_data) {
    if (notifications_data != "" && Object.keys(notifications_data).length > 0) {
        notifications = notifications_data
        if (notifications_data.notifications == undefined || notifications_data.notifications == null) {
            var temp = {};
            temp["artUploder"] = "";
            temp["listUploader"] = "";
            temp["originator"] = "";
            temp["notificationList"] = [];
            notifications.notifications = temp;
        }
        //notifications["name"] = notifications_data.name;
        //notifications["type"] = notifications_data.type;
        //notifications["isEnabled"] = notifications_data.isEnabled;
    }
}, function (error_response) {
    showErrorResponseText(error_response, true);
});

//Service call to load list of reports to add into notification.
getCORS(getReportingService, null, function (reports_data) {
    if (reports_data.reports == undefined || reports_data.reports == null) {
        var temp = {};
        temp["reportList"] = [];
        reports_data.reports = temp;
    }
    if (reports_data != "" && Object.keys(reports_data).length > 0 && reports_data.reports.reportList.length > 0) {
        $.each(reports_data.reports.reportList, function (key, val) {
            if (val.name == "") val.name = "Postage Invoice" + (key + 1);
            reportsList.push({
                "name": val.name
            });
        });
    }
}, function (error_response) {
    showErrorResponseText(error_response, true);
});

//$.getJSON('JSON/_jobNotifications.JSON', function (result) {
//    notifications = result.notifications;
//    notifications["name"] = result.name;
//    notifications["type"] = result.type;
//    notifications["isEnabled"] = result.isEnabled;
//});

$('#_jobNotifications').live('pagebeforecreate', function (event) {
    displayMessage('_jobNotifications');
    createConfirmMessage('_jobNotifications');
    ko.setTemplateEngine(new ko.nativeTemplateEngine);
    var neg_job_number = getSessionData("negJobNumber");
    var header_text = jobDesc + ' | ' + sessionStorage.jobNumber;
    if (neg_job_number != undefined && neg_job_number != null && neg_job_number != "")
        header_text += ' (' + neg_job_number + ')';
    $('#navHeader').html('<h4 style="margin-top:10px !important;margin-bottom:10px !important;">' + header_text + '</h4>');
});

$(document).on('pageshow', '#_jobNotifications', function (event) {
    var pageObj;
    window.setTimeout(function () {
        pageObj = new jobNotifications();
        ko.applyBindings(pageObj);

        //$('#ddlEventType').selectmenu('refresh');

        $('div[data-role=collapsible-set] div[data-role=collapsible]').trigger('create');
        $('div.ui-controlgroup-controls').css('width', '100%');
        $('div[data-role=collapsible-set]').collapsibleset('refresh');
        $('#ulReportsList').listview('refresh');
        $('ul[id*=RecipientList]').listview('refresh');
        if (Object.keys(notifications).length > 0) {
            $('div[data-role=collapsible-set]').css('display', 'block');
            $('ul[id*=ReportsList]').listview();
            $('ul[id*=RecipientList]').listview();
            window.setTimeout(function () {
                if ((notifications.notifications != undefined && notifications.notifications != null) && (notifications.notifications.notificationList != undefined && notifications.notifications.notificationList != null)) {
                    $.each(notifications.notifications.notificationList, function (key, val) {
                        if ($.browser.msie)
                            $("#ddlEventType").children("option[value=" + val.notifyEvent + "]").prop('disabled', true);
                        else {
                            $("#ddlEventType").children("option[value=" + val.notifyEvent + "]").prop('disabled', true);
                            $('#ddlEventType').children('option[value=' + val.notifyEvent + ']').hide();
                        }

                    });
                }
                $.each(pageObj.addedEventTypes(), function (key, val) {
                    $('div[data-role=collapsible-set] div[data-role=collapsible] select[id^=ddlEventType]:not([id*=' + val + ']):not([data-notificationType=approvalAction])').children("option[value=" + val + "]").prop('disabled', true);
                    $('div[data-role=collapsible-set] div[data-role=collapsible] select[id^=ddlEventType]:not([id*=' + val + ']):not([data-notificationType=approvalAction])').children("option[value=" + val + "]").hide();
                });
            }, 1000);
        }
    }, 1000);
    $('div[data-role=popup] div[data-role=header]').removeClass('ui-corner-top ui-header ui-bar-d ui-header-fixed slidedown ui-panel-fixed-toolbar ui-panel-animate ui-panel-page-content-position-left ui-panel-page-content-display-push ui-panel-page-content-open');
    $('div[data-role=popup] div[data-role=header]').addClass('ui-corner-top ui-header ui-bar-d');
});

var newEmailRecipient = function (data) {
    var self = this;
    self.name = ko.observable('');
    if (data.name != undefined && data.name != null)
        self.name(data.name);

    self.value = ko.observable('');
    if (data.value != undefined && data.value != null)
        self.value(data.value);

    self.type = ko.observable('');
    if (data.type != undefined && data.type != null)
        self.type(data.type);

    self.proofUserType = ko.observable('');
    if (data.proofUserType != undefined && data.proofUserType != null)
        self.proofUserType(data.proofUserType);
};

var newNotificationModel = function (new_notification_data) {
    var self = this;
    self.emailDefinition = ko.observable({});
    var tmp = {};
    ko.mapping.fromJS(new_notification_data.emailDefinition, {}, tmp);
    self.emailDefinition(tmp);

    self.attachedReportList = ko.observableArray([]);
    self.attachedReportList(new_notification_data.attachedReportList);

    self.staticEmailRecipients = ko.observable("");
    self.dynamicEmailRecipients = ko.observableArray([]);

    self.textDefinition = ko.observable({});
    var tmp_text = {};
    ko.mapping.fromJS(new_notification_data.textDefinition, {}, tmp_text);
    self.textDefinition(tmp_text);

    self.textPhoneNumberList = ko.observable("");

    self.proofUserList = ko.observableArray([]);

    self.name = ko.observable("");
    self.name(new_notification_data.name);

    self.type = ko.observable("");
    self.type(new_notification_data.type);

    self.processType = ko.observable("");
    self.processType(new_notification_data.processType);

    self.notifyEvent = ko.observable("");
    self.notifyEvent(new_notification_data.notifyEvent);

    self.created = ko.observable("");
    self.created(new_notification_data.created);

    self.createdBy = ko.observable("");
    self.createdBy(new_notification_data.createdBy);
};

var notificationTextModel = function (notification_data) {
    var self = this;
    self.textDefinition = ko.observable({});
    var tmp = {};
    ko.mapping.fromJS(notification_data.textDefinition, {}, tmp);
    self.textDefinition(tmp);

    self.textPhoneNumberList = ko.observable("");
    //var tmpList = [];
    //ko.mapping.fromJS(notification_data.textList, [], tmpList);
    //self.textList(tmpList);

    var text_list = "";
    if (notification_data.textList.length > 0) {
        $.each(notification_data.textList, function (textKey, textValue) {
            text_list += (text_list != "") ? ',' + textValue.phoneNumber : textValue.phoneNumber;
        });
    }
    self.textPhoneNumberList(text_list);

    self.name = ko.observable("");
    self.name(notification_data.name);

    self.type = ko.observable("");
    self.type(notification_data.type);

    self.processType = ko.observable("");
    self.processType(notification_data.processType);

    self.notifyEvent = ko.observable("");
    self.notifyEvent(notification_data.notifyEvent);

    self.created = ko.observable("");
    self.created(notification_data.created);

    self.createdBy = ko.observable("");
    self.createdBy(notification_data.createdBy);
};


var notificationApprovalActionModel = function (notification_data) {
    var self = this;
    self.emailDefinition = ko.observable({});
    var tmp = {};
    ko.mapping.fromJS(notification_data.emailDefinition, {}, tmp);
    self.emailDefinition(tmp);
    self.proofUserList = ko.observableArray([]);
    var dynamic_emails_list = "";
    if (notification_data.proofUserList.length > 0) {
        $.each(notification_data.proofUserList, function (emailKey, emailValue) {
            self.proofUserList.push(new newEmailRecipient(emailValue));
        });
    }

    self.forwardsUsername = ko.observable({});
    self.forwardsUsername(notification_data.forwardsUsername)
    self.name = ko.observable("");
    self.name(notification_data.name);

    self.type = ko.observable("");
    self.type(notification_data.type);

    self.processType = ko.observable("");
    self.processType(notification_data.processType);

    self.notifyEvent = ko.observable("");
    self.notifyEvent(notification_data.notifyEvent);

    self.created = ko.observable("");
    self.created(notification_data.created);

    self.createdBy = ko.observable("");
    self.createdBy(notification_data.createdBy);
};

var notificationApprovalReportModel = function (notification_data) {
    var self = this;
    self.attachedReportList = ko.observableArray([]);
    if (notification_data.attachedReportList.length > 0) {
        $.each(notification_data.attachedReportList, function (key, val) {
            self.attachedReportList.push(val);
        });
    }
    self.name = ko.observable("");
    self.name(notification_data.name);

    self.type = ko.observable("");
    self.type(notification_data.type);

    self.processType = ko.observable("");
    self.processType(notification_data.processType);

    self.notifyEvent = ko.observable("");
    self.notifyEvent(notification_data.notifyEvent);

    self.created = ko.observable("");
    self.created(notification_data.created);

    self.createdBy = ko.observable("");
    self.createdBy(notification_data.createdBy);
};

var notificationEmailModel = function (notification_data) {
    var self = this;
    self.emailDefinition = ko.observable({});
    var tmp = {};
    ko.mapping.fromJS(notification_data.emailDefinition, {}, tmp);
    self.emailDefinition(tmp);
    self.staticEmailRecipients = ko.observable("");
    self.dynamicEmailRecipients = ko.observableArray([]);
    var static_emails_list = "";
    var dynamic_emails_list = "";
    if (notification_data.emailList.length > 0) {
        $.each(notification_data.emailList, function (emailKey, emailValue) {
            if (emailValue.value.indexOf('%') > -1)
                self.dynamicEmailRecipients.push(emailValue.value.replace(/%/g, ''));
            else
                static_emails_list += (static_emails_list != "") ? ',' + emailValue.value : emailValue.value;
        });
    }
    self.staticEmailRecipients(static_emails_list);

    self.attachedReportList = ko.observableArray([]);
    if (notification_data.attachedReportList.length > 0) {
        if (reportsList != undefined && reportsList != null && reportsList.length > 0) {
            $.each(notification_data.attachedReportList, function (key, val) {
                var is_report_found = false;
                $.each(reportsList, function (rpt_key, rpt_val) {
                    if (rpt_val.name == val.name) {
                        is_report_found = true;
                        return false;
                    }
                });
                if (is_report_found) {
                    self.attachedReportList.push(val);
                }
            });
        }
    }
    self.name = ko.observable("");
    self.name(notification_data.name);

    self.type = ko.observable("");
    self.type(notification_data.type);

    self.processType = ko.observable("");
    self.processType(notification_data.processType);

    self.notifyEvent = ko.observable("");
    self.notifyEvent(notification_data.notifyEvent);

    self.created = ko.observable("");
    self.created(notification_data.created);

    self.createdBy = ko.observable("");
    self.createdBy(notification_data.createdBy);
};


var notificationsModel = function (notifications_data) {
    var self = this;

    self.notificationsList = ko.observableArray([]);
    if ((notifications_data.notifications != undefined && notifications_data.notifications != null) && (notifications_data.notifications.notificationList != undefined && notifications_data.notifications.notificationList != null)) {
        $.each(notifications_data.notifications.notificationList, function (key, val) {
            switch (val.type) {
                case "email":
                    self.notificationsList.push(new notificationEmailModel(val));
                    break;
                case "text":
                    self.notificationsList.push(new notificationTextModel(val));
                    break;
                case "approvalReport":
                    self.notificationsList.push(new notificationApprovalReportModel(val));
                    break;
                case "approvalAction":
                    self.notificationsList.push(new notificationApprovalActionModel(val));
                default:
                    break;
            }
        });
    }
    self.originator = ko.observable("")
    self.originator(notifications_data.originator);

    self.artUploader = ko.observable("")
    self.artUploader(notifications_data.artUploader);

    self.listUploader = ko.observable("")
    self.listUploader(notifications_data.listUploader);
};

var jobNotifications = function () {
    var self = this;
    //self.notifications = ko.observableArray([]);
    self.reportsList = ko.observableArray([]);
    self.notifications = ko.observable({});
    self.selectedNotification = ko.observable({});
    self.selectedRecipient = ko.observable({});
    self.selectedApprovalRecipient = ko.observable({});
    self.selectedApprovalRecipient(new newEmailRecipient({}));
    self.prevSelectedApprovalRecipient = ko.observable({});
    self.addedEventTypes = ko.observableArray([]);
    self.currentValue = "";
    self.notificationTypeChanged = function () {
        if ($('#ddlNotificationType').val() == "email" || $('#ddlNotificationType').val() == "approvalAction")
            $('#dvEmailTempaltes').css('display', 'block');
        else
            $('#dvEmailTempaltes').css('display', 'none');

        self.updateEventTypes($('#ddlNotificationType').val());
    };
    //var tmp = {};
    //ko.mapping.fromJS(notifications, {}, tmp);
    self.notifications(new notificationsModel(notifications));
    if ((notifications.notifications != undefined && notifications.notifications != null) && (notifications.notifications.notificationList != undefined && notifications.notifications.notificationList != null)) {
        self.addedEventTypes([]);
        $.each(notifications.notifications.notificationList, function (key, val) {
            if ($.browser.msie)
                $("#ddlEventType option[value=" + val.notifyEvent + "]").prop('disabled', true);
            else {
                $("#ddlEventType").children("option[value=" + val.notifyEvent + "]").prop('disabled', true);
                $('#ddlEventType').children('option[value=' + val.notifyEvent + ']').hide();
            }
            self.addedEventTypes.push(val.notifyEvent);
        });
    }

    $.each(reportsList, function (key, val) {
        self.reportsList.push(val);
    });

    self.addReport = function (data, event) {
        var is_attached = self.validateAttachReport(data);
        if (!is_attached) {
            self.selectedNotification().attachedReportList.push(data);
            $('ul[id*=ReportsList]').listview('refresh');
        }
        else {
            $('#popupReportsList').popup('close');
            window.setTimeout(function () {
                $('#alertmsg').html('Report already attached.');
                $('#okBut').unbind('click');
                $('#okBut').bind('click', function () {
                    $('#okBut').unbind('click');
                    $('#popupDialog').popup('close');
                    window.setTimeout(function () { $('#popupReportsList').popup('open'); }, 300);
                });
                $('#popupDialog').popup('open');
            }, 100);
        }
    }

    self.removeReport = function (data, event) {
        var ele = event.target || event.currentTarget;
        var parent_context = ko.contextFor($(ele)[0]).$parent;
        parent_context.attachedReportList.remove(data);
        $('ul[id*=ReportsList]').listview('refresh');
        //$('#popupReportsList').popup('open');
    }

    self.showReportsList = function (data, event) {
        if (self.reportsList.length == 0) {
            $('#alertmsg').html('No Reports available.');
            $('#okBut').unbind('click');
            $('#okBut').bind('click', function () {
                $('#okBut').unbind('click');
                window.setTimeout(function () { $('#popupDialog').popup('close'); }, 300);
            });
            $('#popupDialog').popup('open');
            return false;
        }
        var ele = event.target || event.currentTarget;
        var parent_context = ko.contextFor($(ele)[0]).$parent;
        $.each(parent_context, function (key, val) {
            self.selectedNotification()[key] = val;
        });
        //self.selectedNotification(parent_context);
        $('#ulReportsList').listview('refresh');
        $('#popupReportsList').popup('open');
    }
    self.addClick = function () {
        //self.selectedNotification(new notificationEmailModel(notificationContent));
        self.selectedNotification(new newNotificationModel(notificationContent));
        self.updateEventTypes("select");
        window.setTimeout(function () {
            $('#ddlEventType').val('').selectmenu('refresh');
            $('#ddlNotificationType').val('').trigger('change').selectmenu('refresh');
            $('input[name*=rdoEmail]').checkboxradio();
            $('#popupNotifications').popup('open');
            self.clearFields();
        });
        //$('select').selectmenu('refresh').trigger('change');
    }

    self.addNotificationClick = function () {
        var tmp = ko.mapping.toJS(self.selectedNotification, mapping);
        if (tmp.type == "text") {
            delete self.selectedNotification().emailDefinition;
            delete self.selectedNotification().emailList;
            delete self.selectedNotification().attachedReportList;
            delete self.selectedNotification().dynamicEmailRecipients;
            delete self.selectedNotification().staticEmailRecipients;
            delete self.selectedNotification().proofUserList;
            delete self.selectedNotification().forwardsUsername;
        }
        else if (tmp.type == "email") {
            delete self.selectedNotification().textDefinition;
            delete self.selectedNotification().textList;
            delete self.selectedNotification().textPhoneNumberList;
            delete self.selectedNotification().proofUserList;
            delete self.selectedNotification().forwardsUsername;
        }
        else if (tmp.type == "email") {
            delete self.selectedNotification().emailDefinition;
            delete self.selectedNotification().emailList;
            delete self.selectedNotification().textDefinition;
            delete self.selectedNotification().dynamicEmailRecipients;
            delete self.selectedNotification().staticEmailRecipients;
            delete self.selectedNotification().textList;
            delete self.selectedNotification().textPhoneNumberList;
            delete self.selectedNotification().proofUserList;
            delete self.selectedNotification().forwardsUsername;
        }
        else if (tmp.type == "approvalAction") {
            delete self.selectedNotification().textDefinition;
            delete self.selectedNotification().textList;
            delete self.selectedNotification().textPhoneNumberList;
            delete self.selectedNotification().emailList;
            delete self.selectedNotification().attachedReportList;
            delete self.selectedNotification().dynamicEmailRecipients;
            delete self.selectedNotification().staticEmailRecipients;
        }
        if (self.selectedNotification().type() == "email" || self.selectedNotification().type() == "approvalAction")
            self.selectedNotification().emailDefinition().templateName($('input[type=radio][name=rdoEmailTemplates]:checked').val());

        var msg = self.validateNewNotifications();
        if (msg != "") {
            msg = '<ul>' + msg + '</ul>';
            $('#popupNotifications').popup('close');
            window.setTimeout(function () {
                $('#alertmsg').html(msg);
                $('#okBut').unbind('click');
                $('#okBut').bind('click', function () {
                    $('#okBut').unbind('click');
                    $('#popupDialog').popup('close');
                    window.setTimeout(function () { $('#popupNotifications').popup('open'); }, 300);
                });
                $('#popupDialog').popup('open');
            }, 100);
            return false;
        }
        //var tmp1 = {};
        //ko.mapping.fromJS(tmp, {}, tmp1);
        self.notifications().notificationsList.push(self.selectedNotification());
        self.selectedNotification({});
        $('#popupNotifications').popup('close');
        $('div[data-role=collapsible-set] div[data-role=collapsible]').trigger('create');
        $('div.ui-controlgroup-controls').css('width', '100%');
        $('div[data-role=collapsible-set]').collapsibleset('refresh');
        $('div[data-role=collapsible-set]').css('display', 'block');
        //$('#ddlEventType option[value=' + tmp.eventType + ']').attr("disabled", "disabled").selectmenu('refresh');
        if ($.browser.msie)
            $("#ddlEventType option[value=" + tmp.notifyEvent + "]").prop('disabled', true);
        else {
            $("#ddlEventType").children("option[value=" + tmp.notifyEvent + "]").prop('disabled', true);
            $('#ddlEventType').children('option[value=' + tmp.notifyEvent + ']').hide();
        }
       
        $('div[data-role=collapsible-set] div[data-role=collapsible] select[id^=ddlEventType]:not([id=ddlEventType' + tmp.type + tmp.notifyEvent + ']):not([id^=ddlEventTypeapprovalAction])').children("option[value=" + tmp.notifyEvent + "]").prop('disabled', true);
        $('div[data-role=collapsible-set] div[data-role=collapsible] select[id^=ddlEventType]:not([id=ddlEventType' + tmp.type + tmp.notifyEvent + ']):not([id^=ddlEventTypeapprovalAction])').children("option[value=" + tmp.notifyEvent + "]").hide();
        
    }

    self.recipientTypeChanged = function () {
        if ($('#ddlRecipientType').val() == "user") {
            $('#dvUser').css('display', 'block');
            $('#dvUser').trigger('create');
            $('#dvEmail').css('display', 'none');
            $('#dvDynamic').css('display', 'none');
        }
        else if ($('#ddlRecipientType').val() == "email") {
            $('#dvUser').css('display', 'none');
            $('#dvEmail').css('display', 'block');
            $('#dvEmail').trigger('create');
            $('#dvDynamic').css('display', 'none');
        }
        else if ($('#ddlRecipientType').val() == "dynamic") {
            $('#dvUser').css('display', 'none');
            $('#dvEmail').css('display', 'none');
            $('#dvDynamic').css('display', 'block');
            $('#dvDynamic').trigger('create');
        }
        else {
            $('#dvUser').css('display', 'none');
            $('#dvEmail').css('display', 'none');
            $('#dvDynamic').css('display', 'none');
        }
    };

    self.addRecipient = function (data, event) {
        self.selectedNotification(data);
        self.selectedApprovalRecipient(new newEmailRecipient({}));
        $('#ddlRecipientType').selectmenu('refresh');
        $('#popupEmailRecipient input[type=radio]').checkboxradio('refresh');
        $('#btnAddRecipient').text('Add');
        $('#popupEmailRecipient').popup('open');
    };

    self.editRecipient = function (data, event) {
        self.selectedApprovalRecipient(data);
        self.selectedRecipient = ko.mapping.toJS(data);
        $('#popupEmailRecipient input[type=radio]').checkboxradio('refresh');
        $('#popupEmailRecipient').popup('open');
        $('#ddlRecipientType').trigger('change');
        $('#btnAddRecipient').text('Update');
    };

    self.addEmailRecipientClick = function (data, event) {
        var ctrl = event.target || event.currentTarget;
        var msg = self.validateEmailRecipient();
        if (msg != "") {
            $('#popupEmailRecipient').popup('close');
            window.setTimeout(function () {
                $('#alertmsg').html(msg);
                $("#okBut").attr("onclick", "$('#popupDialog').popup('close'); window.setTimeout(function(){ $('#popupEmailRecipient').popup('open'); }, 300);");
                $('#popupDialog').popup('open');
            }, 100);
            return false;
        }

        if ($(ctrl).text() == "Add") {
            //if (self.selectedApprovalRecipient().dynamic() != "")
            //    self.selectedApprovalRecipient().value(self.selectedApprovalRecipient().dynamic());
            //delete self.selectedApprovalRecipient().dynamic;

            //if (self.selectedApprovalRecipient().addAs() == "approver") {
            //    delete self.selectedApprovalRecipient().addAs;
            //    self.selectedNotification().approverList.push(self.selectedApprovalRecipient());
            //}
            //else {
            //    delete self.selectedApprovalRecipient().addAs;
            //    self.selectedNotification().viewerList.push(self.selectedApprovalRecipient());
            //}
            self.selectedNotification().proofUserList.push(self.selectedApprovalRecipient());
            $('ul[id*=RecipientList]').listview('refresh');
        }
        else {

            //self.selectedRecipient.name(self.selectedApprovalRecipient().name());

            //if (self.selectedApprovalRecipient().dynamic() != "")
            //    self.selectedRecipient.value(self.selectedApprovalRecipient().dynamic());
            //else if (self.selectedApprovalRecipient().value() != "")
            //    self.selectedRecipient.value(self.selectedApprovalRecipient().value());
            self.selectedRecipient = "";
        }
        self.selectedApprovalRecipient(new newEmailRecipient({}));
        $('#popupEmailRecipient').popup('close');
    };

    self.removeRecipient = function (data, e) {
        var current_element = e.target || e.targetElement || e.currentTarget;
        var parent_context = ko.contextFor(current_element).$parent;
        parent_context.proofUserList.remove(data);
        $('ul[id*=RecipientList]').listview('refresh');
    };

    self.cancelEmailRecipientClick = function (data, event) {
        $.each(self.selectedRecipient, function (key, val) {
            self.selectedApprovalRecipient()[key](val);
        });
        self.selectedApprovalRecipient(new newEmailRecipient({}));
        $('#popupEmailRecipient').popup('close');
    };

    self.getCurrentValue = function (data, el) {
        self.currentValue = data.notifyEvent();
    };

    self.eventNotificationTypeChanged = function (data, el) {
        if ($.browser.msie) {
            //Reset previous selection
            $('div[data-role=collapsible-set] div[data-role=collapsible] select[id^=ddlEventType] option[value=' + self.currentValue + ']').prop('disabled', false);

            //Set current selection
            $('div[data-role=collapsible-set] div[data-role=collapsible] select[id^=ddlEventType]:not([id=ddlEventType' + data.type() + data.notifyEvent() + ']):not([id^=ddlEventTypeapprovalAction]) option[value=" + data.notifyEvent() + "]').prop('disabled', true);
        }
        else {
            //Reset previous selection
            $('div[data-role=collapsible-set] div[data-role=collapsible] select[id^=ddlEventType]').children("option[value=" + self.currentValue + "]").prop('disabled', false);
            $('div[data-role=collapsible-set] div[data-role=collapsible] select[id^=ddlEventType]').children("option[value=" + self.currentValue + "]").show();

            //Set current selection
            $('div[data-role=collapsible-set] div[data-role=collapsible] select[id^=ddlEventType]:not([id=ddlEventType' + data.type() + data.notifyEvent() + ']):not([id^=ddlEventTypeapprovalAction])').children("option[value=" + data.notifyEvent() + "]").prop('disabled', true);
            $('div[data-role=collapsible-set] div[data-role=collapsible] select[id^=ddlEventType]:not([id=ddlEventType' + data.type() + data.notifyEvent() + ']):not([id^=ddlEventTypeapprovalAction])').children("option[value=" + data.notifyEvent() + "]").hide();
        }
        var remove_index = -1;
        self.addedEventTypes.remove(self.currentValue);
        self.addedEventTypes.push(data.notifyEvent());
        self.updateEventTypes(data.type());
    };

    self.sendTest = function (data, e) {
        var temp_data = ((data.data != undefined) ? data.data : data);
        msg = self.validateNotification(temp_data, true);
        if (msg != "") {
            msg = "Default values are required for:<br/><br/>" + msg;
            window.setTimeout(function () {
                $('#alertmsg').html(msg);
                $("#okBut").attr("onclick", "$('#popupDialog').popup('close');");
                $('#popupDialog').popup('open');
            }, 100);
            return false;
        }
    }

    self.removeNotification = function (data, event) {
        var ele = event.target || event.currentTarget;
        var message = "Do you want to remove this notification?";
        $('#confirmMsg').html(message);
        $('#okButConfirm').bind('click', function () {
            $('#okButConfirm').unbind('click');

            if ($.browser.msie)
                $("#ddlEventType option[value=" + data.notifyEvent() + "]").prop('disabled', false);
            else {
                $("#ddlEventType option[value=" + data.notifyEvent() + "]").prop('disabled', false);
                $('#ddlEventType').children('option[value=' + data.notifyEvent() + ']').show();
            }

            self.notifications().notificationsList.remove(data);
            $('#popupConfirmDialog').popup('close');
            $('div[data-role="collapsible-set"]').collapsibleset('refresh');
        });
        $('#cancelButConfirm').bind('click', function () {
            $('#cancelButConfirm').unbind('click');
            $('#popupConfirmDialog').popup('close');
        });
        window.setTimeout(function () {
            $('#popupConfirmDialog').popup('open', { positionTo: 'window' });
        }, 200);
    };

    self.clearFields = function () {
        $('#ddlEventType').val('').selectmenu('refresh');
        $('#ddlNotificationType').val('').trigger('change').selectmenu('refresh');
        $('input[name*=rdoEmail]').attr('checked', false).checkboxradio('refresh');
    };

    self.validateNewNotifications = function () {
        var msg = "";
        if (self.selectedNotification().name() === "") {
            msg += "<li>Please enter name.</li>";
        }
        if (self.selectedNotification().notifyEvent() === "") {
            msg += "<li>Please select an event type.</li>";
        }
        if (self.selectedNotification().type() === "") {
            msg += "<li>Please select notification type.</li>";
        }
        else if (self.selectedNotification().type() === "email" && self.selectedNotification().emailDefinition().templateName() === "") {
            msg += "<li>Please select email notification template.</li>";
        }

        return msg;
    };

    self.validateAttachReport = function (data) {
        var attached_report_list = ko.mapping.toJS(self.selectedNotification().attachedReportList, mapping);
        var is_attached = false;
        $.each(attached_report_list, function (key, val) {
            if (val.name == data.name) {
                is_attached = true;
                return false;
            }
        });
        return is_attached;
    };

    //Redirect to System Dashboard.
    self.redirectToPages = function (data, e) {
        var ele = e.target || e.targetElement;
        switch ($(ele).text()) {
            case "Notifications":
                window.setTimeout(function () {
                    window.location.href = '../jobNotifications/jobNotifications.html';
                }, 500);
                break;
            case "Logging":
                window.setTimeout(function () {
                    window.location.href = '../jobLogging/jobLogging.html';
                }, 500);
                break;
            case "Reporting":
                window.setTimeout(function () {
                    window.location.href = '../jobReporting/jobReporting.html';
                }, 500);
                break;
            default:
                break;
        }
    };
    self.showHomePage = function () {
        //Need to call the function
        redirectToIndexPage();
    }
    self.showJobSummary = function () {
        //Need to call the function
        redirectToSingleJobPage();
    }

    self.saveToJob = function (data, e) {
        self.saveNotifications(false, e);
    };

    self.saveNotifications = function (can_save_to_template, e) {
        var temp_data = ko.mapping.toJS(self.notifications, mapping);
        //var msg = self.validateEventData(data, e);
        msg = self.validateEventData(self.notifications(), e);
        if (msg != "") {
            window.setTimeout(function () {
                $('#alertmsg').html(msg);
                $("#okBut").attr("onclick", "$('#popupDialog').popup('close');");
                $('#popupDialog').popup('open');
            }, 100);
            return false;
        } else {
            var temp = self.notifications();
            var added_event_types = [];
            var tmp_list = ko.mapping.toJS(self.notifications().notificationsList, mapping);
            $.each(tmp_list, function (key, val) {
                if (val.type == "email") {
                    var static_list = (val.staticEmailRecipients.length > 0) ? val.staticEmailRecipients.split(',') : [];
                    if (val.emailList == undefined || val.emailList == null)
                        val["emailList"] = [];
                    $.each(static_list, function (emailKey, emailVal) {
                        val.emailList.push({
                            "name": "",
                            "value": emailVal
                        });
                    });
                    var dynamic_list = (val.dynamicEmailRecipients.length > 0) ? val.dynamicEmailRecipients : [];
                    if (val.emailList == undefined || val.emailList == null)
                        val["emailList"] = [];
                    $.each(dynamic_list, function (emailKey, emailVal) {
                        val.emailList.push({
                            "name": "",
                            "value": '%' + emailVal + '%'
                        });
                    });
                    delete val.staticEmailRecipients;
                    delete val.dynamicEmailRecipients;
                }
                else if (val.type == "text") {
                    var text_list = (val.textPhoneNumberList.length > 0) ? val.textPhoneNumberList.split(',') : [];
                    if (val.textList == undefined || val.textList == null)
                        val["textList"] = [];
                    $.each(text_list, function (textKey, textVal) {
                        var tmp_number = "";
                        tmp_number = textVal.replace(/\(/g, '').replace(/\)/g, '').replace(/-/g, '').replace(/\//g, '').replace(/ /g, '');
                        if (tmp_number.length > 10)
                            tmp_number = tmp_number.substring(1, tmp_number.length);
                        val.textList.push({
                            "name": "",
                            "phoneNumber": tmp_number
                        });
                    });
                    delete val.textPhoneNumberList;
                    if (val.textDefinition.testPhoneNumber != "") {
                        val.textDefinition.testPhoneNumber = val.textDefinition.testPhoneNumber.replace(/\(/g, '').replace(/\)/g, '').replace(/-/g, '').replace(/\//g, '').replace(/ /g, '');
                        var temp_ph_number = [];
                        temp_ph_number = (val.textDefinition.testPhoneNumber.length > 0) ? val.textDefinition.testPhoneNumber.split(',') : [];
                        var temp_ph_number_string = "";
                        if (temp_ph_number.length > 0) {
                            $.each(temp_ph_number, function (key, val) {
                                if (val.length > 10)
                                    val = val.substring(1, val.length);
                                temp_ph_number_string += (temp_ph_number_string != "") ? "," + val : val;
                            });
                        }

                        val.textDefinition.testPhoneNumber = temp_ph_number_string;
                    }
                }
                added_event_types.push(val.notifyEvent);
            });
            var temp = tmp_list;
            notifications.notifications.notificationList = tmp_list;
            //console.log(notifications);
            notifications = DeepTrim(notifications);
            postCORS(saveNotificationsService + can_save_to_template, JSON.stringify(notifications), function (notifications_data) {
                self.addedEventTypes([]);
                $('#alertmsg').text("Notifications have been saved successfully.");
                window.setTimeout(function () {
                    $('#okBut').unbind('click');
                    $('#okBut').bind('click', function () {
                        $('#popupDialog').popup('close');
                    });
                    $('#popupDialog').popup('open');
                    self.addedEventTypes(added_event_types);
                }, 1000);
            }, function (error_response) {
                showErrorResponseText(error_response, true);
                self.addedComments = [];
            });
        }
    };

    self.updateEventTypes = function (selected_type) {
        if (self.addedEventTypes().length > 0) {
            var event_types_added = ko.mapping.toJS(self.addedEventTypes, mapping);
            var is_approval_action = ((selected_type == "approvalAction") ? true : false);
            if ($.browser.msie) {
                $("#ddlEventType option").prop('disabled', false);
            }
            else {
                $("#ddlEventType").children("option").prop('disabled', false);
                $('#ddlEventType').children('option').show();
            }
            $("#ddlEventType").val("").selectmenu("refresh");
            $.each(event_types_added, function (key, val) {
                if (is_approval_action) {
                    if ($.browser.msie)
                        $("#ddlEventType option[value=" + val + "]").prop('disabled', false);
                    else {
                        $("#ddlEventType").children("option[value=" + val + "]").prop('disabled', false);
                        $('#ddlEventType').children('option[value=' + val + ']').show();
                    }
                }
                else {
                    if ($.browser.msie)
                        $("#ddlEventType option[value=" + val + "]").prop('disabled', true);
                    else {
                        $("#ddlEventType").children("option[value=" + val + "]").prop('disabled', true);
                        $('#ddlEventType').children('option[value=' + val + ']').hide();
                    }
                }
            });
        }
    };

    self.saveToTemplate = function (data, e) {
        $('#confirmMsg').html("Click \"Ok\" to save BOTH this job, and the parent template for this job.");
        $('#okButConfirm').bind('click', function () {
            $('#okButConfirm').unbind('click');
            $('#popupConfirmDialog').popup('close');
            self.saveNotifications('saveTemplate', e);
        });
        $('#popupConfirmDialog').popup('open');
    };

    self.validateEmailRecipient = function () {
        var email_recipient = ko.toJS(self.selectedApprovalRecipient(), mapping);
        var msg = "";
        if (email_recipient.type == "") {
            msg += "<li>Recipient type is required.</li>"
        }
        else {
            switch (email_recipient.type) {
                case "user":
                    if (email_recipient.value == "") {
                        msg += "<li>Username is required.</li>"
                    }
                    break;
                case "email":
                    if (email_recipient.value == "") {
                        msg += "<li>Email value is required.</li>"
                    }
                    else {
                        var temp_msg = self.validateEmailAddress(email_recipient.value);
                        msg += (temp_msg != "") ? ("<li>" + temp_msg + " Email Address</li>") : msg;
                    }
                    break;
                case "dynamic":
                    if (email_recipient.value == "") {
                        msg += "<li>Select one option for dynamic recipient type.</li>"
                    }
                    break;
                default:
                    break;
            }
        }
        if (email_recipient.proofUserType == "") {
            msg += "<li>Proof User Type is required.</li>"
        }
        msg = (msg != "") ? "<ul>" + msg + "</ul>" : msg;
        return msg;
    };

    self.validateNotification = function (data_notification, is_send_test) {
        var notification = ko.mapping.toJS(data_notification, mapping);
        msg = "";
        if (notification.type == "email" || notification.type.toLowerCase() == "approvalaction") {
            if (notification.type == "email") {
                if (notification.staticEmailRecipients === "" && notification.dynamicEmailRecipients.length == 0) {
                    msg += "<li>Email Recipient (Static/Dynamic)</li>";
                }
                if (notification.staticEmailRecipients != "") {
                    var temp_message = self.validateEmailAddress(notification.staticEmailRecipients);
                    msg += (temp_message != "") ? "<li>" + temp_message + " Email Address.</li>" : "";
                }
            }
            else {
                if (notification.forwardsUsername == "") {
                    msg += "<li>Forwards Username</li>";
                }
                if (notification.proofUserList.length == 0) {
                    msg += "<li>Recipients</li>";
                }

                if (notification.assistanceEmail == "") {
                    msg += "<li>Assistance Email</li>";
                }
                else {
                    var temp_message = self.validateEmailAddress(notification.assistanceEmail);
                    msg += (temp_message != "") ? "<li>" + temp_message + " Assistance Email.</li>" : "";
                }
            }

            if (notification.emailDefinition.subjectLine === "") {
                msg += "<li>Subject line</li>";
            }
            if (notification.emailDefinition.head === "") {
                msg += "<li>Headline</li>";
            }
            if (notification.emailDefinition.emailMessage === "") {
                msg += "<li>Email message</li>";
            }
            else {
                var temp_message = self.validateJunkData(notification.emailDefinition.emailMessage);
                msg += (temp_message != "") ? "<li>" + temp_message + "Email Message</li>" : "";
            }
            if (notification.emailDefinition.disclaimer == "") {
                msg += "<li>Disclaimer</li>";
            }
            else {
                var temp_message = self.validateJunkData(notification.emailDefinition.disclaimer);
                msg += (temp_message != "") ? "<li>" + temp_message + "Disclaimer</li>" : "";
            }
            if (notification.emailDefinition.image === "") {
                msg += "<li>Image</li>";
            }
            if (is_send_test) {
                if (notification.emailDefinition.testEmail === "")
                    msg += "<li>Test email</li>";
            }
            if (notification.emailDefinition.testEmail != "") {
                var temp_message = self.validateEmailAddress(notification.emailDefinition.testEmail);
                msg += (temp_message != "") ? "<li>" + temp_message + " Test Email Address.</li>" : "";
            }
            msg = (msg != "") ? ("<span style='font-weight:bold;'>" + notification.name + "</span><ul>" + msg + "</ul>") : msg;
        }
        else if (notification.type == "text") {
            if (notification.textDefinition.textMessage == "") {
                msg += "<li>Text message</li>";
            }
            else {
                var temp_message = self.validateJunkData(notification.textDefinition.textMessage);
                msg += (temp_message != "") ? "<li>" + temp_message + "Text Message</li>" : "";
            }

            if (notification.textPhoneNumberList === "") {
                msg += "<li>Phone Number</li>";
            }
            else {
                var temp_message = self.validatePhoneNumbers(notification.textPhoneNumberList)
                msg += (temp_message != "") ? "<li>" + temp_message + " Phone Number</li>" : "";
            }

            if (is_send_test) {
                if (notification.textDefinition.testPhoneNumber === "")
                    msg += "<li>Test phone number</li>";
            }
            if (notification.textDefinition.testPhoneNumber != "") {
                var temp_message = self.validatePhoneNumbers(notification.textDefinition.testPhoneNumber);
                msg += (temp_message != "") ? "<li>" + temp_message + " Test Phone Number.</li>" : "";
            }

            msg = (msg != "") ? ("<span style='font-weight:bold;'>" + notification.name + "</span><ul>" + msg + "</ul>") : msg;
        }
        return msg;
    };

    self.validateEventData = function (data, e) {
        var temp_msg = "";
        $.each(data.notificationsList(), function (key, val) {
            msg = self.validateNotification(val, false);
            temp_msg = (msg != "") ? temp_msg + msg : temp_msg;
        });
        temp_msg = (temp_msg != "") ? "Default values are required for:<br/><br/>" + temp_msg : temp_msg;
        if (temp_msg == "" && data.notificationsList().length == 0) {
            temp_msg = "No Notifications available";
        }
        return temp_msg;
    };
    self.acceptValidChars = function (data, event) {
        var ctrl = event.target | event.currentTarget;
        var regex = /^[0-9-.(), /]+$/;
        var key = String.fromCharCode(event.charCode ? event.which : event.charCode);
        if (!regex.test(key)) {
            event.preventDefault();
            return false;
        } else
            return true;
    };

    //Validate US Phone numbers
    self.validatePhoneNumbers = function (phone_val) {
        return validateMulitpleUSPhoneNumbers(phone_val);
    };

    //Validate Email Addresses
    self.validateEmailAddress = function (email_address) {
        if (email_address == undefined)
            return "";
        return validateMultipleEmails(email_address);
    };

    self.validateJunkData = function (data) {
        return validateJunkCharacters(data);
    };

    self.viewNotificationTemplate = function () {
        $('#viewTemplatePopup').popup('open');
    };
}

String.prototype.initCap = function () {
    return this.replace(/(?:^|\s)[a-z]/g, function (m) {
        return m.toUpperCase();
    });
};

function getPageDisplayName(page_name) {
    var PATTERN = /^[ABCDEFGHIJKLMNOPQRSTUVWXYZ]/;
    var page_display_name = '';
    $.each(new String(page_name), function (key, value) {
        if (key > 0 && value.match(PATTERN)) {
            page_display_name += ' ' + value;
        }
        else {
            page_display_name += value;
        }
    });
    return page_display_name;
}