jQuery.support.cors = true;

//******************** Global Variables Start **************************
var gData;
var gDataAll;
var searchData;
var selectedStoreId;
var prevSelectedStoreId;
//var serviceURL = '../listSelection/JSON/_jobListSelection.JSON';
//var serviceURL = myProtocol + 'maps.tribunedirect.com/api/ZipSelectList/';
var serviceURL = mapsUrl + 'api/ZipSelectList/';
var gOutputServiceUrl = '../JSON/_jobSetupOutput.JSON';
var gOutputData;
var gDataStartIndex = 0;
var gInnerData = [];
var gSelectedZips = [];
var jobNumber = "";
var facilityId = "";
var totalAvailable = 0;
var jsonStoreLocations = "";
var selectedCRRTS = [];
var gDataStartIndex = 0;
var gPageSize = 7;
var pagePrefs;
var locationsList = [];
var isTargetChanged = false;
var isRadiusChanged = false;
var isEnterPress = false;
jobNumber = getSessionData("jobNumber");
facilityId = getSessionData("facilityId");
caseysStaicJobNumber = sessionStorage.jobNumber;
var dontShowHintsAgain = (localStorage.getItem('doNotShowHintsAgain') != undefined && localStorage.getItem('doNotShowHintsAgain') != null && localStorage.getItem('doNotShowHintsAgain') != "" && localStorage.getItem('doNotShowHintsAgain') != "false") ? true : false;
//******************** Global Variables End **************************

//******************** Page Load Events Start **************************
$(window).on('popupbeforeposition', 'div:jqmData(role="popup")', function () {
    var notDismissible = $(this).jqmData('dismissible') === false;
    if (notDismissible) {
        $('.ui-popup-screen').off();
    }
});

$('#_jobListSelection').live('pagebeforecreate', function (event) {

    displayMessage('_jobListSelection');
    saveConfirmMessage('_jobListSelection');
    showStoreChangeMessage('_jobListSelection');
    radiusZipCRRTChangeMessage('_jobListSelection');
    confirmNavigationMessage('_jobListSelection');
    $('#btnContinue').attr('onclick', 'continueToOrder()');
    //displayNavLinks();
    //test for mobility...
    //loadMobility();
    //var over = '<div id="overlay" style="z-index:100;"><img id="loading" alt="Loading...." src="../images/kaleidoscope_wait.gif"></img><span id="loadingText">Loading Map....</span></div>';
    //$(over).appendTo('body');
    loadingImg("_jobListSelection");
    sessionStorage.removeItem('updatedCRRTs');
    setDemoHintsSliderValue();
    //window.setTimeout(function loadHints() {
    createDemoHints("jobListSelection");
});

$(document).bind('pageshow', '#_jobListSelection', function (event) {
    //$('#overlay').css('display', 'none');
    if (sessionStorage.ListSelectionPrefs == undefined || sessionStorage.ListSelectionPrefs == null || sessionStorage.ListSelectionPrefs == "")
        getPagePreferences('ListSelection.html', sessionStorage.facilityId, appPrivileges.customerNumber);
    else
        managePagePrefs(jQuery.parseJSON(sessionStorage.ListSelectionPrefs));
    pagePrefs = jQuery.parseJSON(sessionStorage.ListSelectionPrefs);

    if (!fnVerifyScriptBlockDict(pagePrefs))
        return false;

    if (pagePrefs != null) {
        displayListFileInfo();
        //$('#ddlStoreLocations').change(function () {

        //});
        //eval(getURLDecode(pagePrefs.scriptBlockDict.pageshow));
    }

    $('#rdoRadius').attr('checked', true).checkboxradio('refresh').trigger('change');
    $('#chkViewByHouseHold').attr('checked', false).checkboxradio('refresh');
    if (appPrivileges.customerNumber == CASEYS)
        $("#dvSelectByRadius").trigger('collapsibleexpand');
    else
        $("#dvSelectByZipCrrt").trigger('collapsibleexpand');

    if (appPrivileges.customerNumber == CW) {
        $('#btnRevert').css('display', "none");
        $('#btnSaveSelections').css('display', "none");
        $('#btnRefresh').css('display', "none");
    }
    else {
        $('#btnRevert').addClass('ui-disabled');
        $('#btnSaveSelections').addClass('ui-disabled');
        $('#btnRefresh').addClass('ui-disabled');
        //click events.
        $('#btnRefresh').click(function () {
            //validateTargetQty();
            $('#waitPopUp').popup('open', { positionTo: 'window' });
            //Need to write code for radius
            window.setTimeout(function makeDelay() {
                if (!$('#dvSelectByRadius').hasClass('ui-collapsible-collapsed')) {
                    if ((gData.targetedQty != $('#txtRadiusTargeted').val()) || (($('#rdoTargetCounts')[0].checked && $('#hdnZipsChanged').val() == "") && (sessionStorage.updatedCRRTs == undefined && sessionStorage.updatedCRRTs == null || sessionStorage.updatedCRRT == "")) || ((!$('#sldrRange')[0].disabled) && $('#sldrRange').attr('prevValue') != $('#sldrRange').val())) {
                        if (gData.targetedQty != $('#txtRadiusTargeted').val()) {
                            isTargetChanged = true;
                            isEnterPress = false;
                        }
                        sessionStorage.removeItem('updatedCRRTs');
                        getSelectedStoreData();
                    }
                    else
                        calculateTargetedCnt();

                }
                else if (!$('#dvSelectByZipCrrt').hasClass('ui-collapsible-collapsed')) {
                    calculateTargetedCnt();
                }
            }, 1000);
        });
        $('#btnSaveSelections').click(function () {
            saveSelections();
        });
    }
    $('#loadingText1').text("Loading Map.....");

    $('#dvSelectByZipCrrt').on('collapsibleexpand', function (event) {
        if ($('#hdnMode').val() != "rtoc") {
            var msg = "Clicking on \"Select Only By Zip/Carrier Route\" will create Zip/Carrier Route selection instead of Radius selection. Click \"Continue\" to select by Zip/Carrier Route or click \"Cancel\" to keep the current Radius selection."
            //var msg = "Removing zips/carrier routes or adding zips/carrier routes that intersect with the existing radius will create a Zip/Carrier Route selection instead of a Radius selection. Click \"Continue\" to select by Zip/Carrier Route or click \"Cancel\" to keep the current Radius selection.";
            $('#popupradiusZipCRRTConfirmDialog').find('a[id=okBut1]').attr('onclick', 'convertRadiusToZipCRSelections()');
            //$('#popupradiusZipCRRTConfirmDialog').find('a[id=cancelBut1]').attr('onclick', '$(\'#hdnMode\').val(\'radius\');$(\'#dvSelectByRadius\').collapsible(\'expand\');$(\'#hdnSelectedZip\').val(\'\');$(\'#popupradiusZipCRRTConfirmDialog\').popup(\'open\');$(\'#popupradiusZipCRRTConfirmDialog\').popup(\'close\');');
            $('#popupradiusZipCRRTConfirmDialog').find('a[id=cancelBut1]').bind('click', function () {
                $('#hdnMode').val('radius');
                $('#dvSelectByRadius').collapsible('expand');
                $('#hdnSelectedZip').val('');
                $('#popupradiusZipCRRTConfirmDialog').find('a[id=cancelBut1]').unbind('click');
                $('#popupradiusZipCRRTConfirmDialog').popup('open');
                $('#popupradiusZipCRRTConfirmDialog').popup('close');
            });
            $('#popupradiusZipCRRTConfirmDialog').find('div[id=confirmMsg1]').text(msg);
            $('#popupradiusZipCRRTConfirmDialog').popup('open');
        }
        $('#hdnMode').val('zipcrrt');
        $('#hdnSelectedZip').val('');
    });

    $('#dvSelectByRadius').on('collapsibleexpand', function (event, ui) {
        if ($('#hdnMode').val() == 'zipcrrt') {
            //var temp_ctrls = $('input[type="checkbox"]:not([id^=checkAll]):not([id^=chkEntireZip]):checked:[selectedType=f]');
            var temp_ctrls = $('input[type="checkbox"]:not([id^=checkAll]):not([id^=chkEntireZip]):checked[selectedType=f]');
            var msg = "";
            var ctrl = null;
            if (temp_ctrls.length > 0) {
                ctrl = $('#rdoTargetCounts');
                msg = "Resetting the targeted count will remove all of your current carrier route selections. Click \"Continue\" to reset the targeted count or click \"Cancel\" to keep your current selections.";
            }
            else {
                ctrl = $('#rdoRadius');
                msg = "Resetting the radius will remove all of your current carrier route selections. Click \"Continue\" to reset the radius or click \"Cancel\" to keep your current selections.";
            }

            //$('#rdoTargetCounts')[0].checked
            //             Resetting the targeted count will remove all of your current carrier route selections. Click "Continue" to reset the targeted count or click "Cancel" to keep your current selections.
            //Resetting the radius will remove all of your current carrier route selections. Click \"Continue\" to reset the radius or click \"Cancel\" to keep your current selections.
            //var msg = "Resetting the targeted count will remove all of your current carrier route selections. Click \"Continue\" to reset the targeted count or click \"Cancel\" to keep your current selections.";
            $('#popupradiusZipCRRTConfirmDialog').find('a[id=okBut1]').attr('onclick', 'getCRRTSelectionByRadius(\'' + ctrl[0].id + '\');');
            //$('#popupradiusZipCRRTConfirmDialog').find('a[id=cancelBut1]').attr('onclick', '$(\'#hdnSelectedZip\').val(\'test\');$(\'#dvSelectByZipCrrt\').collapsible(\'expand\');$(\'#popupradiusZipCRRTConfirmDialog\').popup(\'open\');$(\'#popupradiusZipCRRTConfirmDialog\').popup(\'close\');');
            $('#popupradiusZipCRRTConfirmDialog').find('a[id=cancelBut1]').bind('click', function () {
                $('#hdnSelectedZip').val('test');
                $('#dvSelectByZipCrrt').collapsible('expand');
                $('#popupradiusZipCRRTConfirmDialog').find('a[id=cancelBut1]').unbind('click');
                $('#popupradiusZipCRRTConfirmDialog').popup('open');
                $('#popupradiusZipCRRTConfirmDialog').popup('close');
            });
            $('#popupradiusZipCRRTConfirmDialog').find('div[id=confirmMsg1]').text(msg);
            $('#popupradiusZipCRRTConfirmDialog').popup('open');
            $('#hdnMode').val('radius');
            $('#hdnSelectedZip').val('');
            //}
        }
    });

    $('#sldrRange').bind('slidestop blur', function (e, ui) {
        if ($('#sldrRange').val() != gData.radius) {
            isRadiusChanged = true;
        }
        if ($('#sldrRange').attr('prevValue') != $('#sldrRange').val())
            if (!$('#dvSelectByRadius').hasClass('ui-collapsible-collapsed') && $('#rdoRadius')[0].checked && (!$('#sldrRange')[0].disabled)) {
                $('#waitPopUp').popup('open', { positionTo: 'window' });
                window.setTimeout(function getDelay() {
                    sessionStorage.removeItem('updatedCRRTs');
                    getSelectedStoreData();
                }, 1000);

            }
    });

    $('#txtRadiusTargeted').bind('blur', function () {
        if (gData.targetedQty != $('#txtRadiusTargeted').val() && !isEnterPress) {
            $('#btnRefresh').trigger('click');
         }
    });

    $('#sldrRange').slider('disable');
    $('#sldrRange')[0].disabled = true;

    if (!dontShowHintsAgain && (jobCustomerNumber == CASEYS) && (sessionStorage.showDemoHints == undefined || sessionStorage.showDemoHints == null || sessionStorage.showDemoHints == "" || sessionStorage.showDemoHints == "on") && (sessionStorage.isListSelectionDemoHintsDisplayed == undefined || sessionStorage.isListSelectionDemoHintsDisplayed == null || sessionStorage.isListSelectionDemoHintsDisplayed == "false")) {
        sessionStorage.isListSelectionDemoHintsDisplayed = true;
        $('#storeSelection').popup('close');
        window.setTimeout(function loadHints() {
            $('#storeSelection').popup('open', { positionTo: '#dvSelectedLocations' });
        }, 1000);
    }
    if (jobCustomerNumber == CASEYS) {
        $('#storeSelection p a').text('Turn Hints Off');
        $('#selectionMethod p a').text('Turn Hints Off');
        $('#postalBoundarySelections p a').text('Turn Hints Off');
    }
    window.setTimeout(function loadHints() {
        $('#sldrShowHints').slider();
        setDemoHintsSliderValue();
        $('#sldrShowHints').slider('refresh');
    }, 2000);

});

//******************** Public Functions Start **************************

function convertRadiusToZipCRSelections() {
    $('#popupradiusZipCRRTConfirmDialog').popup('close');
    $('#waitPopUp').popup('open', { positionTo: 'window' });
    window.setTimeout(function getDelay1() {
        $('#dvSelectByRadius').trigger('collapsiblecollapse');
        if (gData != null && gData.zips != undefined) {
            var r_crrts = [];
            var total_targets_selected = 0;
            var zip_routes = jQuery.grep(gData.zips, function (key, obj) {
                var zip = key.zipCode;
                var index = 0;
                var targeted_counts = 0;
                $.each(key.crrts, function (i, j) {
                    //if (j.selectType == 'r') {
                    j.isChecked = ($('#' + 'check_' + zip + '_' + index)[0].checked) ? 1 : 0;
                    j.selectType = ($('#' + 'check_' + zip + '_' + index)[0].checked) ? 'f' : '';
                    $('#' + 'check_' + zip + '_' + index).attr('selectedType', j.selectType);
                    j.targeted = ($('#' + 'check_' + zip + '_' + index)[0].checked) ? j.available : 0;
                    $('#spn_' + zip + '_' + index).text(((j.isChecked) ? j.available : 0));
                    targeted_counts = parseInt(targeted_counts) + parseInt(j.targeted);
                    //}
                    index++;
                });
                $('#spnCheckAll' + zip).text(targeted_counts);
                $('#spnTargetedZipCnt' + zip).text(targeted_counts);
                key.targeted = targeted_counts;
                total_targets_selected = parseInt(total_targets_selected) + parseInt(targeted_counts);
            });
            $('#spnTargeted').text(total_targets_selected);
        }
        sessionStorage.removeItem('updatedCRRTs');
        sessionStorage.removeItem('storeListSelection');
        gSelectedZips = [];
        $('#dvLocations').empty();
        buildZips(selectedStoreId);
        calculateTargetedCnt();
        $('#waitPopUp').popup('close');
    }, 1000);
}

function getCRRTSelectionByRadius(ctrl) {
    $('#popupradiusZipCRRTConfirmDialog').popup('open');
    $('#popupradiusZipCRRTConfirmDialog').popup('close');
    $('#waitPopUp').popup('open', { positionTo: 'window' });
    window.setTimeout(function getDelay2() {
        ctrl = $('#' + ctrl);
        $('#dvSelectByZipCrrt').trigger('collapsiblecollapse');
        if (gData != null && gData.zips != undefined) {
            var r_crrts = [];
            var total_targets_selected = 0;
            var zip_routes = jQuery.grep(gData.zips, function (key, obj) {
                var zip = key.zipCode;
                var index = 0;
                var targeted_counts = 0;
                $.each(key.crrts, function (i, j) {
                    j.isChecked = ($('#' + 'check_' + zip + '_' + index)[0].checked) ? 1 : 0;
                    j.selectType = ($('#' + 'check_' + zip + '_' + index)[0].checked) ? 'r' : '';

                    $('#' + 'check_' + zip + '_' + index).attr('selectedType', j.selectedType);
                    targeted_counts = parseInt(targeted_counts) + parseInt(j.targeted);
                    index++;
                });
                key.targeted = targeted_counts;
                total_targets_selected = parseInt(total_targets_selected) + parseInt(targeted_counts);
            });
            $('#txtRadiusTargeted').val(total_targets_selected);
            $('#spnRadiusTargeted').text(total_targets_selected);
            buildZips(selectedStoreId);
            gData.radius = 0;
        }
        sessionStorage.removeItem('updatedCRRTs');
        sessionStorage.removeItem('storeListSelection');
        gSelectedZips = [];

        if (ctrl.attr('id') == "rdoRadius") {
            $('#rdoTargetCounts').attr('checked', false).checkboxradio('refresh');
            $('#rdoRadius').attr('checked', true).checkboxradio('refresh').trigger('onchange');
        }
        else {
            $('#rdoRadius').attr('checked', false).checkboxradio('refresh');
            $('#rdoTargetCounts').attr('checked', true).checkboxradio('refresh').trigger('onchange');
        }

        getSelectedStoreData();
        //calculateTargetedCnt();
        //$('#dvSelectByRadius').trigger('expand');
        $('#waitPopUp').popup('close');
    }, 1000);
}

function makeGData() {
    var zip_select_url = serviceURL + appPrivileges.customerNumber + "/1/" + ((appPrivileges.customerNumber == CW) ? jobNumber : caseysStaicJobNumber + '/' + selectedStoreId);
    var radius_selected = ($('#sldrRange')[0].disabled) ? 0 : $('#sldrRange').val();
    var target_selected = (!$('#sldrRange')[0].disabled) ? 0 : $('#txtRadiusTargeted').val();
    if (!$('#dvSelectByRadius').hasClass('ui-collapsible-collapsed')) {
        $('#btnSaveSelections').removeClass('ui-disabled');
        var selected_crrts = $('span[id^=spn_]');
        var selected_target = 0;
        //if ($('#hdnZipsChanged').val() == "") {
        //        if ($('#hdnZipsChanged').val() != "") {
        //            if (selected_crrts.length == 0) {
        //                selected_target = target_selected;
        //            }
        //            else {
        //                //$.each($('#hdnZipsChanged').val().split('|'), function (a, b) {
        //                $.each(selected_crrts, function (a, b) {
        //                    //selected_target = parseInt(selected_target) + parseInt($('#spnCheckAll' + b).text());
        //                    selected_target = parseInt(selected_target) + parseInt($(b).text());
        //                    sessionStorage.removeItem('updatedCRRTs');
        //                });
        //                radius_selected = 0;
        //            }
        //        }
        //        else {
        selected_target = target_selected;
        //       }
        if ((prevSelectedStoreId == "" || prevSelectedStoreId == 0) || prevSelectedStoreId != selectedStoreId)
            zip_select_url += "/0/0";
        else
            zip_select_url += "/" + radius_selected + "/" + selected_target;

    }
    else if (!$('#dvSelectByZipCrrt').hasClass('ui-collapsible-collapsed')) {
        zip_select_url += "/0/0";
    }
    getCORS(zip_select_url, null, function (data) {
        $('#dvPostalBoundarySelections').css('display', 'block');
        gDataAll = data;
        gData = data;
        $('#sldrRange').attr('hasChanged', false);
        $('#sldrRange').val(data.radius).slider("refresh");
        $('#sldrRange').attr('prevValue', $('#sldrRange').val())
        sessionStorage.storeListSelection = JSON.stringify(gData);
        prevSelectedStoreId = selectedStoreId;
        var mode = verifyCRRTsMode();

        if (mode == "radius" || mode == "")
            //$('#dvSelectByRadius').trigger('collapsibleexpand');
            $('#dvSelectByRadius').collapsible('expand');
        else if (mode == "crrt")
            $('#dvSelectByZipCrrt').collapsible('expand');
        //$('#dvSelectByZipCrrt').trigger('collapsibleexpand');

        $('#waitPopUp').popup('close');
    }, function (error_response) {
        showErrorResponseText(error_response);
    });
}

function verifyCRRTsMode() {
    var select_mode = "";
    if (gData.zips.length > 0) {
        $.each(gData.zips, function (a, b) {
            var is_r_found = false;
            var is_f_found = false;
            if (b.crrts.length > 0) {
                $.each(b.crrts, function (c, d) {
                    if (d.selectType == "r") {
                        is_r_found = true;
                        return false;
                    }
                    else if (!is_r_found && d.selectType == "f") {
                        is_f_found = true;
                    }
                });
            }
            if (is_r_found) {
                select_mode = "radius";
                return false;
            }
            if (!is_r_found && is_f_found)
                select_mode = "crrt";
        });
    }
    return select_mode;
}


function displayListFileInfo() {
    if (sessionStorage.jobSetupOutput != undefined) {
        gOutputData = jQuery.parseJSON(sessionStorage.jobSetupOutput);
        loadStoreLocations();
        getUploadedFilesCount();
        displayNavLinks();
    }
}
//load the store locations that are selected in the "select location" job page.
function loadStoreLocations() {
    var loaded_stores_data = [];
    //if (gOutputData != undefined && gOutputData.selectedLocationsAction != undefined && gOutputData.selectedLocationsAction.selectedLocationsList != undefined && gOutputData.selectedLocationsAction.selectedLocationsList.length > 0) {
    //    loaded_stores_data = gOutputData.selectedLocationsAction.selectedLocationsList;
    //    loadStores(loaded_stores_data);
    //}
    //else {
    getCORS(serviceURLDomain + "api/Locations_dropDown/" + appPrivileges.facility_id + "/" + appPrivileges.customerNumber + "/" + ((appPrivileges.customerNumber == CW) ? jobNumber : caseysStaicJobNumber) + "/0/2", null, function (data) {
        if (data.locationsSelected != undefined && data.locationsSelected != undefined != null && data.locationsSelected != "" && Object.keys(data.locationsSelected).length > 0) {
            locationsList = data.locationsSelected;
            loadStores(locationsList);
        }

        //if (data["opted-in"] != undefined && data["opted-in"] != null) {
        //if (gOutputData != null && gOutputData.selectedLocationsAction != null && gOutputData.selectedLocationsAction != undefined && gOutputData.selectedLocationsAction.selectedLocationsList != undefined && gOutputData.selectedLocationsAction.selectedLocationsList != null) {
        //    gOutputData.selectedLocationsAction.selectedLocationsList = data["opted-in"];
        //    gOutputData.selectedLocationsAction.selectedLocationsList = gOutputData.selectedLocationsAction.selectedLocationsList.sort(function (a, b) {
        //        return parseInt(a.storeId) > parseInt(b.storeId) ? 1 : -1;
        //    });
        //    sessionStorage.jobSetupOutput = JSON.stringify(gOutputData);
        //    loadStores(gOutputData.selectedLocationsAction.selectedLocationsList);
        //}
        //}
    }, function (error_response) {
        showErrorResponseText(error_response);
    });
    //}
}
function loadStores(loaded_stores_data) {
    var stores_data = '<select name="ddlStoreLocations" id="ddlStoreLocations" data-theme="a" data-mini="true" onchange="selectedStoreChanged();">';
    stores_data += '<option value="">Select Store</option>';
    var store_display_name = "";
    $.each(loaded_stores_data, function (key, val) {
        store_display_name = key + ' - ' + val;
        stores_data += '<option value="' + key + '">' + store_display_name + '</option>';
    });
    stores_data += '</select>';
    //stores_data += '* <span style="font-size:12px">Pending Approval</span>';
    $('#dvSelectedLocations').empty();
    $('#dvSelectedLocations').html(stores_data);
    $('#dvSelectedLocations select').each(function (i) {
        $(this).selectmenu();
    });
}

function selectedStoreChanged() {
    if ($('#ddlStoreLocations').val() == "") {
        $('#sldrRange').slider('disable');
        $('#sldrRange')[0].disabled = true;
    }
    else {
        //var selected_crrts = $('div input:checked:not("id^=checkAll"):not("id^=chkEntireZip")');
        $('#chkViewByHouseHold').attr('checked', false).checkboxradio('refresh');
        //var selected_crrts = $('div input[type="checkbox"]:not([id^=checkAll]):not([id^=chkEntireZip]):checked');
        var selected_crrts = $('div[id=dvLocations] input[type="checkbox"]:not([id^=checkAll]):not([id^=chkEntireZip]):checked');
        var diff = [];
        if ((selectedCRRTS.length > 0 && selected_crrts.length > 0) || (selectedCRRTS.length > 0 && selected_crrts.length == 0) || (selectedCRRTS.length == 0 && selected_crrts.length == 0))
            //diff = getDifferences(selectedCRRTS, selected_crrts);
            diff = getDifferences(selected_crrts, selectedCRRTS);
        else if (selectedCRRTS.length == 0 && selected_crrts.length > 0)
            //diff = getDifferences(selected_crrts, selectedCRRTS);
            diff = getDifferences(selectedCRRTS, selected_crrts);

        if ((diff.length == undefined || diff.length == 0)) {
            if (sessionStorage.storeListSelection != undefined) {
                var selected_text = $('#hdnSelectedStore').val();
                if ((selected_text != "" && selectedCRRTS.length > 0) || isTargetChanged || isRadiusChanged) {
                    var message = "The Targeted Selections have been changed for Store '" + selected_text + "' " +
                              "To select another location you must either Save Selections or Revert to the previously saved selections.";
                    $('#storeMsg').text(message);
                    $('#popupStoreDialog').popup('open');
                }
                else {
                    $('#waitPopUp').popup('open', { positionTo: 'window' });
                    $('#hdnZipsChanged').val('');
                    $('#sldrRange').attr('hasChanged', false);
                    $('#sldrRange').val('1').slider('refresh');
                    $('#rdoTargetCounts').attr('checked', false).checkboxradio('refresh');
                    $('#rdoRadius').attr('checked', true).checkboxradio('refresh').trigger('change');
                    window.setTimeout(function setDelay() {
                        $('#sldrRange').slider('enable');
                        $('#sldrRange')[0].disabled = false;
                        getSelectedStoreData();
                    }, 1000);
                    selectedCRRTS = [];
                }
            }
            else {
                $('#waitPopUp').popup('open', { positionTo: 'window' });
                $('#hdnZipsChanged').val('');
                $('#sldrRange').attr('hasChanged', false);
                $('#sldrRange').val('1').slider('refresh');
                $('#rdoTargetCounts').attr('checked', false).checkboxradio('refresh');
                $('#rdoRadius').attr('checked', true).checkboxradio('refresh').trigger('change');
                window.setTimeout(function setPause() {
                    $('#sldrRange').slider('enable');
                    $('#sldrRange')[0].disabled = false;
                    getSelectedStoreData();
                }, 1000);
                selectedCRRTS = [];
            }
            prevSelectedStoreId = ($('#hdnSelectedStore').val() != "") ? $('#hdnSelectedStore').val().substring(0, $('#hdnSelectedStore').val().indexOf(' ')) : 0;
            $('#hdnSelectedStore').val($('#ddlStoreLocations option:selected').text());
        }
        else {
            if (selected_crrts.length > 0 || selectedCRRTS.length > 0) {
                var selected_text = $('#hdnSelectedStore').val();
                var msg = "The Targeted Selections have been changed for Store '" + selected_text + "'. " +
                      "To select another location you must refresh the map. Click 'Ok' to Refresh Map";
                $('#confirmMsg').text(msg);
                $('#popupConfirmDialog').popup('open');
            }
        }
        $('#btnRefresh').removeClass('ui-disabled');
        makeReadOnlyZips();
    }
}


function getSelectedStoreData() {
    selectedStoreId = $('#ddlStoreLocations').attr('value');
    //selectedStoreId = "2218"; // TO BE Removed and used above commented line when the service url becomes dynamic.
    makeGData();
    if (gData == undefined || gData == null || gData == "" || gData.zips == undefined || gData.zips == null || gData.zips == "" || gData.zips.length == 0) {
        $('#map').empty();
        $('#map').hide();
        $('#dvLocations').empty();
        $('#divAvailable').empty();
        $('#spnRadiusAvblCount').empty();
        $('#txtTargeted').empty();
        $('#spnTargeted').text('0');
        return;
    } else {
        $('#map').show();
    }


    //eval(getURLDecode(pagePrefs.scriptBlockDict.getSelectedStoreData));

    if (appPrivileges.customerNumber == CW) {
        makePageSelect(false)
        $('#pageSelect').trigger('onchange');
    }
    else
        buildZips(selectedStoreId);
    getDataFromSession();
    ////$('#waitPopUp').popup('open', { positionTo: 'window' });
    ////$('#overlay').css('display', 'block');
    calculateTargetedCnt();
    ////$('#overlay').css('display', 'none');

    //$('#waitPopUp').popup('close');
}

function getDataFromSession() {
    if (sessionStorage.jobSetupOutput != undefined && sessionStorage.jobSetupOutput != null) {
        var job_info = jQuery.parseJSON(sessionStorage.jobSetupOutput);
        if (job_info.listSelection != undefined && job_info.listSelection != null) {
            var store_info = jQuery.grep(job_info.listSelection, function (obj) {
                return obj.storeId === selectedStoreId;
            });
            if (store_info.length > 0) {
                gSelectedZips = store_info[0];
                buildSelectedZips();
                //calculateTargetedCnt();
            }
        }
    }
}

//Prevents the collapsible set item expansion when clicked on checkbox.
function chkClick(e) {
    e.stopPropagation();
}

function chkSingleCRRT() {
    var selected_crrts = $('div[id=dvLocations] input[type="checkbox"]:not([id^=checkAll]):not([id^=chkEntireZip]):checked');
    selectedCRRTS = $.extend(true, {}, selected_crrts);
}

function makeEntireZipSelection(is_check_all, ctrl, avlb, also_in_store, selected_zip, status) {
    $('#checkAll' + selected_zip)[0].checked = status;
    $('#checkAll' + selected_zip).attr("checked", status).checkboxradio("refresh");
    $('#chkEntireZip' + selected_zip).attr("checked", status).checkboxradio("refresh");
    $('#hdnSelectedZip').val(selected_zip);
    selectAllZips(is_check_all, ctrl, avlb, also_in_store);
    if (is_check_all)
        $('div.ui-collapsible-content', "#" + $('#hdnSelectedZip').val()).trigger('collapse');
    //selectAllZips(true, $('#checkAll' + selected_zip), 0);
}

function checkEntireZip(is_check_all, ctrl, avlb, also_in_store, selected_zip) {
    if (ctrl.checked) {
        if (!$('#dvSelectByRadius').hasClass('ui-collapsible-collapsed')) {
            var is_allowed = true;
            if (is_check_all) {
                $.each($('input[id^=check_' + selected_zip + '_]'), function (a, b) {
                    if ($(this).attr('selectedType') != "null" && $(this).attr('selectedType') != "" && $(this).attr('selectedType') == "r") {
                        is_allowed = false;
                        return false;
                    }
                });
            }
            else {
                if ($('#' + ctrl.id).attr('selectedType') == "r")
                    is_allowed = false;
            }
            if (!is_allowed) {
                //var msg = "Adding/Removing zips or carrier routes will create a Zip/Carrier Route selection instead of a Radius selection. Click \"Continue\" to select by Zip/Carrier Route or click \"Cancel\" to stay with the Radius selection.";
                var msg = "Removing zips/carrier routes or adding zips/carrier routes that intersect with the existing radius will create a Zip/Carrier Route selection instead of a Radius selection. Click \"Continue\" to select by Zip/Carrier Route or click \"Cancel\" to keep the current Radius selection.";
                $('#popupradiusZipCRRTConfirmDialog').find('a[id=okBut1]').attr('onclick', 'convertToZipCRRTMode(' + is_check_all + ',\'' + ctrl.id + '\',\'' + avlb + '\',\'' + also_in_store + '\',\'' + selected_zip + '\',true)');
                $('#popupradiusZipCRRTConfirmDialog').find('a[id=cancelBut1]').attr('onclick', '$(\'#hdnSelectedZip\').val(\'\');$(\'#' + ctrl.id + '\').attr("checked",false).checkboxradio("refresh");$(\'#popupradiusZipCRRTConfirmDialog\').popup(\'open\');$(\'#popupradiusZipCRRTConfirmDialog\').popup(\'close\');');
                $('#popupradiusZipCRRTConfirmDialog').find('div[id=confirmMsg1]').text(msg);
                $('#popupradiusZipCRRTConfirmDialog').popup('open');
            }
            else {
                makeEntireZipSelection(is_check_all, ctrl.id, avlb, also_in_store, selected_zip, true);
            }
        }
        else {
            makeEntireZipSelection(is_check_all, ctrl.id, avlb, also_in_store, selected_zip, true);
        }

    }
    else {
        if (!$('#dvSelectByRadius').hasClass('ui-collapsible-collapsed')) {
            var is_allowed = true;
            if (is_check_all) {//.attr('selectedType');
                $.each($('input[id^=check_' + selected_zip + '_]'), function (a, b) {
                    if ($(this).attr('selectedType') != "null" && $(this).attr('selectedType') != "" && $(this).attr('selectedType') == "r") {
                        is_allowed = false;
                        return false;
                    }
                });
            }
            else {
                if ($('#' + ctrl.id).attr('selectedType') == "r")
                    is_allowed = false;
            }
            if (!is_allowed) {
                //var msg = "Adding/Removing zips or carrier routes will create a Zip/Carrier Route selection instead of a Radius selection. Click \"Continue\" to select by Zip/Carrier Route or click \"Cancel\" to stay with the Radius selection.";
                var msg = "Removing zips/carrier routes or adding zips/carrier routes that intersect with the existing radius will create a Zip/Carrier Route selection instead of a Radius selection. Click \"Continue\" to select by Zip/Carrier Route or click \"Cancel\" to keep the current Radius selection.";
                $('#popupradiusZipCRRTConfirmDialog').find('a[id=okBut1]').attr('onclick', 'convertToZipCRRTMode(' + is_check_all + ',\'' + ctrl.id + '\',\'' + avlb + '\',\'' + also_in_store + '\',\'' + selected_zip + '\',false)');
                $('#popupradiusZipCRRTConfirmDialog').find('a[id=cancelBut1]').attr('onclick', '$(\'#hdnSelectedZip\').val(\'\');$(\'#' + ctrl.id + '\').attr("checked",true).checkboxradio("refresh");$(\'#popupradiusZipCRRTConfirmDialog\').popup(\'open\');$(\'#popupradiusZipCRRTConfirmDialog\').popup(\'close\');');
                $('#popupradiusZipCRRTConfirmDialog').find('div[id=confirmMsg1]').text(msg);
                $('#popupradiusZipCRRTConfirmDialog').popup('open');
            }
            else {
                makeEntireZipSelection(is_check_all, ctrl.id, avlb, also_in_store, selected_zip, false);
            }
        }
        else {
            makeEntireZipSelection(is_check_all, ctrl.id, avlb, also_in_store, selected_zip, false);
        }
    }
}

if (typeof String.prototype.startsWith != 'function') {
    // see below for better implementation!
    String.prototype.startsWith = function (str) {
        return this.indexOf(str) == 0;
    };
}

function convertToZipCRRTMode(is_check_all, ctrl, avlb, also_in_store, selected_zip, status) {
    $('#popupradiusZipCRRTConfirmDialog').popup('open');
    $('#popupradiusZipCRRTConfirmDialog').popup('close');
    $('#waitPopUp').popup('open', { positionTo: 'window' });
    window.setTimeout(function getDelay3() {
        makeEntireZipSelection(is_check_all, ctrl, avlb, also_in_store, selected_zip, status);
        //$('#dvSelectByRadius').trigger('collapsiblecollapse');
        $('#dvSelectByRadius').collapsible('collapse');
        if (gData != null && gData.zips != undefined) {
            var r_crrts = [];
            var total_targets_selected = 0;
            var zip_routes = jQuery.grep(gData.zips, function (key, obj) {
                var zip = key.zipCode;
                var index = 0;
                var targeted_counts = 0;
                $.each(key.crrts, function (i, j) {
                    //if (j.selectType == 'r') {
                    j.isChecked = ($('#' + 'check_' + zip + '_' + index)[0].checked) ? 1 : 0;
                    j.selectType = ($('#' + 'check_' + zip + '_' + index)[0].checked) ? 'f' : '';
                    $('#' + 'check_' + zip + '_' + index).attr('selectedType', j.selectType);
                    j.targeted = ($('#' + 'check_' + zip + '_' + index)[0].checked) ? j.available : 0;
                    $('#spn_' + zip + '_' + index).text(((j.isChecked) ? j.available : 0));
                    targeted_counts = parseInt(targeted_counts) + parseInt(j.targeted);
                    //}
                    index++;
                });
                $('#spnCheckAll' + zip).text(targeted_counts);
                $('#spnTargetedZipCnt' + zip).text(targeted_counts);
                key.targeted = targeted_counts;
                total_targets_selected = parseInt(total_targets_selected) + parseInt(targeted_counts);
            });
            $('#spnTargeted').text(total_targets_selected);
        }
        sessionStorage.removeItem('updatedCRRTs');
        sessionStorage.removeItem('storeListSelection');
        gSelectedZips = [];
        $('#dvLocations').empty();
        buildZips(selectedStoreId);
        calculateTargetedCnt();
        $('#hdnMode').val('rtoc');
        //$('#dvSelectByZipCrrt').trigger('collapsibleexpand');
        $('#dvSelectByZipCrrt').collapsible('expand');
        $('#waitPopUp').popup('close');
    }, 1000);
}

//to display the "All zips" for the selected store location.
//function buildZips(selectedStoreId) {
//    var li_zips = "";
//    var li_all_routes = "";
//    var li_chk_routes = "";
//    var index = 0;
//    var allzip = 0;
//    var i = 0;
//    var is_all_checkbox_selected = true;
//    var totalAvailable = 0;
//    var totalTargeted = 0;
//    var target_selected = 0;
//    if (selectedStoreId != undefined) {
//        li_zips = '<div data-role="collapsible-set"  data-inset="false" data-theme="b" style="padding-left: 10px" data-collapsed-icon="arrow-r" data-expanded-icon="arrow-d">';
//        $('#dvLocations').empty();
//        if (gData != null) {
//            gInnerData = gData;
//            $.each(gInnerData.zips, function (zip_key, zip_val) {
//                if (gInnerData.zips[index] != undefined) {
//                    var city_name = zip_val.zipName;
//                    var zipCode = zip_val.zipCode;
//                    if (zip_val.available != null && zip_val.available > 0) {
//                        var avail = zip_val.available;
//                        //                        var targeted = zip_val.targeted;
//                        //                        totalTargeted = eval(parseInt(totalTargeted) + parseInt(targeted));
//                        totalAvailable = eval(parseInt(totalAvailable) + parseInt(avail));
//                    }
//                    if (zip_val.alsoInStore == null)
//                        li_zips = '<div data-theme="b" data-role="collapsible" data-collapsed-icon="arrow-r" data-expanded-icon="arrow-d" onclick="getZipInfo(' + zipCode + ');" id="' + zipCode + '"><h3 style="width:105%">' + zipCode + ' - ' + city_name + '';
//                    else
//                        li_zips = '<div data-theme="e" data-role="collapsible" data-collapsed-icon="arrow-r" data-expanded-icon="arrow-d" onclick="getZipInfo(' + zipCode + ');" id="' + zipCode + '"><h3 style="width:105%">' + zipCode + ' - ' + city_name + '';
//                    li_all_routes = '<div data-role="fieldcontain" data-inset="false"><fieldset id="fieldset" data-role="controlgroup" data-mini="true" data-inset="false">';
//                    var selected_routes_count = 0;
//                    var len = 0;
//                    var is_Crrt_Starts_With_B = false;
//                    if (zip_val.crrts != null || zip_val.crrts.length > 0) {
//                        li_chk_routes = "";
//                        i = 0;
//                        len = zip_val.crrts.length;
//                        var zip_targeted_cnt = 0;
//                        target_selected = 0;
//                        var zip_avbl_cnt = 0;
//                        $.each(zip_val.crrts, function (key, val) {
//                            allzip += eval(parseInt(val.available));

//                            var is_checked = '';
//                            zip_targeted_cnt += parseInt(val.targeted);
//                            target_selected += parseInt(val.targeted);
//                            zip_avbl_cnt += parseInt(val.available);
//                            if (val.isChecked == 1) {
//                                selected_routes_count++;
//                                //zip_targeted_cnt += parseInt(val.available);
//                                //target_selected += parseInt(val.available);
//                                is_checked = ' checked="checked" ';
//                            } else {
//                                is_checked = '';
//                            }
//                            if (is_checked == '')
//                                is_all_checkbox_selected = false;
//                            if (!is_Crrt_Starts_With_B)
//                                is_Crrt_Starts_With_B = (val.crrtName.toUpperCase().startsWith('B'));
//                            //li_chk_routes += '<input type="checkbox" id="check_' + zipCode + '_' + i + '" onmouseup="chkSingleCRRT();" onchange="selectAllZips(false,this,' + val.available + ', \'' + zip_val.alsoInStore + '\')" targeted ="' + val.targeted + '" value="' + val.available + '" "' + is_checked + '"/>';
//                            li_chk_routes += '<input type="checkbox" id="check_' + zipCode + '_' + i + '" onmouseup="chkSingleCRRT();" onchange="checkEntireZip(false,this,' + val.available + ', \'' + zip_val.alsoInStore + '\',\'' + zipCode + '\')" selectedType="' + val.selectType + '" targeted ="' + val.targeted + '" value="' + val.available + '" "' + is_checked + '"/>';

//                            li_chk_routes += '<label for="check_' + zipCode + '_' + i + '" onmouseup="chkSingleCRRT();"><span style="font-weight:normal;font-size:12px;color:#369">' + val.crrtName + ' | <span  id="spn_' + zipCode + '_' + i + '">' + val.targeted + '</span> of ' + val.available + '</span></label>';
//                            i = i + 1;
//                        });
//                        totalTargeted += target_selected;
//                    }
//                    //if (selected_routes_count == len)
//                    if (zip_avbl_cnt == target_selected)
//                    //li_all_routes += '<input type="checkbox" name="checkAll" onchange="selectAllZips(true,this,0, \'' + zip_val.alsoInStore + '\')" id="checkAll' + zipCode + '" checked/>';
//                        li_all_routes += '<input type="checkbox" name="checkAll" onchange="checkEntireZip(true,this,0, \'' + zip_val.alsoInStore + '\',\'' + zipCode + '\')" id="checkAll' + zipCode + '" checked />';
//                    else
//                        li_all_routes += '<input type="checkbox" name="checkAll" onchange="checkEntireZip(true,this,0, \'' + zip_val.alsoInStore + '\',\'' + zipCode + '\')" id="checkAll' + zipCode + '"/>';
//                    //li_all_routes += '<label for="checkAll' + zipCode + '">Select Entire Zip<span style="font-weight:normal;font-size:12px;color:#369;"> | Avlb: ' + zip_val.available + '</span></label>';
//                    li_all_routes += '<label for="checkAll' + zipCode + '">Select Entire Zip<span style="font-weight:normal;font-size:12px;color:#369;"> | <span id="spnCheckAll' + zipCode + '">' + target_selected + '</span> of <span id="spnCheckAllAvbl' + zipCode + '">' + zip_val.available + '</span> targeted</span></label>';
//                    $('#divAvailable').html(zip_val.available);
//                    $('#spnRadiusAvblCount').html(zip_val.available);

//                    if (is_Crrt_Starts_With_B && appPrivileges.customerNumber === CASEYS) {
//                        li_chk_routes += '<div style="font-size:12px;padding-top:8px">Carrier Routes starting with "B" are P.O. Boxes and will not display on the map.</div>';
//                        is_Crrt_Starts_With_B = false;
//                    }

//                    li_all_routes += li_chk_routes + '</fieldset></div>';
//                    var checked_status = '';
//                    if (!$('#dvSelectByRadius').hasClass('ui-collapsible-collapsed')) {
//                        if (selected_routes_count > 0)
//                            checked_status = 'checked';
//                    }
//                    else {
//                        if (selected_routes_count == len) {
//                            checked_status = 'checked';
//                        }
//                    }
//                    if (checked_status != "") {
//                        if (zip_val.alsoInStore == null)
//                        //li_zips += '<div data-role="fieldcontain" style="width:40px;" data-inset="true" data-mini="true"  class="ui-li-aside" ><fieldset data-role="controlgroup" data-inset="false" data-mini="true" style="width:40px"><legend></legend><input class="my-list" type="checkbox" name="chkEntireZip' + zipCode + '" id="chkEntireZip' + zipCode + '" class="custom" onchange="checkEntireZip(\'' + zipCode + '\',this)" onclick="chkClick(event);" data-theme="b" data-mini="true" checked/><label for="chkEntireZip' + zipCode + '" onclick="chkClick(event);" style="width:40px">&nbsp;</label></fieldset></div>';
//                            li_zips += '<div data-role="fieldcontain" style="width:40px;" data-inset="true" data-mini="true"  class="ui-li-aside" ><fieldset data-role="controlgroup" data-inset="false" data-mini="true" style="width:40px"><legend></legend><input class="my-list" type="checkbox" name="chkEntireZip' + zipCode + '" id="chkEntireZip' + zipCode + '" class="custom" onchange="checkEntireZip(true,this,\'\',\'' + zip_val.alsoInStore + '\',\'' + zipCode + '\')" onclick="chkClick(event);" data-theme="b" data-mini="true" ' + checked_status + '/><label for="chkEntireZip' + zipCode + '" onclick="chkClick(event);" style="width:40px">&nbsp;</label></fieldset></div>';
//                        else
//                        //li_zips += '<div data-theme="e" data-role="fieldcontain" style="width:40px;" data-inset="true" data-mini="true"  class="ui-li-aside" ><fieldset data-role="controlgroup" data-inset="false" data-mini="true" style="width:40px" data-theme="e"><legend></legend><input class="my-list" type="checkbox" name="chkEntireZip' + zipCode + '" id="chkEntireZip' + zipCode + '" class="custom" onchange="checkEntireZip(\'' + zipCode + '\',this)" onclick="chkClick(event);" data-theme="e" data-mini="true" checked/><label data-theme="e" for="chkEntireZip' + zipCode + '" onclick="chkClick(event);" style="width:40px">&nbsp;</label></fieldset></div>';
//                            li_zips += '<div data-theme="e" data-role="fieldcontain" style="width:40px;" data-inset="true" data-mini="true"  class="ui-li-aside" ><fieldset data-role="controlgroup" data-inset="false" data-mini="true" style="width:40px" data-theme="e"><legend></legend><input class="my-list" type="checkbox" name="chkEntireZip' + zipCode + '" id="chkEntireZip' + zipCode + '" class="custom" onchange="checkEntireZip(true,this,\'\',\'' + zip_val.alsoInStore + '\',\'' + zipCode + '\')" onclick="chkClick(event);" data-theme="e" data-mini="true" ' + checked_status + '/><label data-theme="e" for="chkEntireZip' + zipCode + '" onclick="chkClick(event);" style="width:40px">&nbsp;</label></fieldset></div>';
//                    }

//                    if (checked_status == "") {
//                        if (zip_val.alsoInStore == null)
//                        //li_zips += '<div data-role="fieldcontain" style="width:40px;" data-inset="true" data-mini="true"  class="ui-li-aside"><fieldset  data-role="controlgroup" data-inset="false" data-mini="true" style="width:40px"><legend></legend><input class="my-list" type="checkbox" name="chkEntireZip' + zipCode + '" id="chkEntireZip' + zipCode + '" class="custom" onchange="checkEntireZip(\'' + zipCode + '\',this)" onclick="chkClick(event);" data-theme="b" data-mini="true"/><label for="chkEntireZip' + zipCode + '" onclick="chkClick(event);" style="width:40px">&nbsp;</label></fieldset></div>';
//                            li_zips += '<div data-role="fieldcontain" style="width:40px;" data-inset="true" data-mini="true"  class="ui-li-aside"><fieldset  data-role="controlgroup" data-inset="false" data-mini="true" style="width:40px"><legend></legend><input class="my-list" type="checkbox" name="chkEntireZip' + zipCode + '" id="chkEntireZip' + zipCode + '" class="custom" onchange="checkEntireZip(true,this,\'\',\'' + zip_val.alsoInStore + '\',\'' + zipCode + '\')" onclick="chkClick(event);" data-theme="b" data-mini="true"/><label for="chkEntireZip' + zipCode + '" onclick="chkClick(event);" style="width:40px">&nbsp;</label></fieldset></div>';
//                        else
//                        //li_zips += '<div data-theme="e" data-role="fieldcontain" style="width:40px;" data-inset="true" data-mini="true"  class="ui-li-aside"><fieldset data-theme="e" data-role="controlgroup" data-inset="false" data-mini="true" style="width:40px"><legend></legend><input class="my-list" type="checkbox" name="chkEntireZip' + zipCode + '" id="chkEntireZip' + zipCode + '" class="custom" onchange="checkEntireZip(\'' + zipCode + '\',this)" onclick="chkClick(event);" data-theme="e" data-mini="true"/><label for="chkEntireZip' + zipCode + '" onclick="chkClick(event);" style="width:40px" data-theme="e">&nbsp;</label></fieldset></div>';
//                            li_zips += '<div data-theme="e" data-role="fieldcontain" style="width:40px;" data-inset="true" data-mini="true"  class="ui-li-aside"><fieldset data-theme="e" data-role="controlgroup" data-inset="false" data-mini="true" style="width:40px"><legend></legend><input class="my-list" type="checkbox" name="chkEntireZip' + zipCode + '" id="chkEntireZip' + zipCode + '" class="custom" onchange="checkEntireZip(true,this,\'\',\'' + zip_val.alsoInStore + '\',\'' + zipCode + '\')" onclick="chkClick(event);" data-theme="e" data-mini="true"/><label for="chkEntireZip' + zipCode + '" onclick="chkClick(event);" style="width:40px" data-theme="e">&nbsp;</label></fieldset></div>';
//                    }

//                    li_zips += '<br/><span style="font-weight:normal;font-size:12px;">Available: ' + zip_val.available + ' | Targeted: <span style="font-weight:normal;font-size:12px;" id="spnTargetedZipCnt' + zipCode + '">' + zip_targeted_cnt + '</span></span><br/>';

//                    var zip_status = "";
//                    if (selected_routes_count == len && target_selected == zip_val.available)
//                        zip_status = "Entire Zip Selected"
//                    else if (selected_routes_count == len && target_selected < zip_val.available)
//                        zip_status = ""
//                    else if (selected_routes_count < len)
//                        zip_status = selected_routes_count + ' of ' + len + ' Carrier Routes Selected';

//                    if (selected_routes_count == len) {
//                        if (zip_val.alsoInStore == null)
//                            li_zips += '<span id="spn' + zipCode + '" style="font-weight:normal;font-size:12px;">' + zip_status + '</span></h3>';
//                        else
//                            li_zips += '<span id="spn' + zipCode + '" style="font-weight:normal;font-size:12px;">' + zip_status + '<br />Also in Store: ' + zip_val.alsoInStore + '</span></h3>';
//                    }
//                    else {
//                        if (zip_val.alsoInStore == null)
//                            li_zips += '<span id="spn' + zipCode + '" style="font-weight:normal;font-size:12px;">' + zip_status + '</span></h3>';
//                        else
//                            li_zips += '<span id="spn' + zipCode + '" style="font-weight:normal;font-size:12px;">' + zip_status + '<br />Also in Store: ' + zip_val.alsoInStore + '</span></h3>';
//                    }
//                    li_zips += li_all_routes;
//                    li_zips += '</div>';
//                    if (is_all_checkbox_selected)
//                        $('#checkAll').attr("checked", true);
//                    index++;
//                    $('#dvLocations').append(li_zips);
//                }
//            });
//            $("#dvLocations").trigger("create");
//            $($('div[class=ui-checkbox]').find('label[for^=chkEntireZip]')).each(function (i) {
//                $(this).find('span').css('padding-left', '1px');
//            });
//        }
//        $('#divAvailable').html(totalAvailable);
//        $('#spnRadiusAvblCount').html(totalAvailable);
//        $('#txtRadiusTargeted').val(totalTargeted);
//        $('#spnRadiusTargeted').html(totalTargeted);
//    }
//    else {
//        $('#hdnSelectedStore').val('');
//    }
//}

function buildZips(selectedStoreId) {
    var li_zips = "";
    var li_all_routes = "";
    var li_chk_routes = "";
    var index = 0;
    var allzip = 0;
    var i = 0;
    var is_all_checkbox_selected = true;
    var totalAvailable = 0;
    var totalTargeted = 0;
    var target_selected = 0;
    var data_theme = 'e';
    if (selectedStoreId != undefined) {
        li_zips = '<div data-theme="c" data-role="collapsible-set"  data-inset="false" data-theme="' + data_theme + '" style="padding-left: 10px" data-collapsed-icon="carat-r" data-expanded-icon="carat-d">';
        $('#dvLocations').empty();
        if (gData != null) {
            gInnerData = gData;
            $.each(gInnerData.zips, function (zip_key, zip_val) {
                if (gInnerData.zips[index] != undefined) {
                    var city_name = zip_val.zipName;
                    var zipCode = zip_val.zipCode;
                    if (zip_val.available != null && zip_val.available > 0) {
                        var avail = zip_val.available;
                        //                        var targeted = zip_val.targeted;
                        //                        totalTargeted = eval(parseInt(totalTargeted) + parseInt(targeted));
                        totalAvailable = parseInt(totalAvailable) + parseInt(avail);
                    }
                    if (zip_val.alsoInStore == null)
                        li_zips = '<div data-theme="' + data_theme + '" data-role="collapsible" data-collapsed-icon="carat-r" data-expanded-icon="carat-d" onclick="getZipInfo(' + zipCode + ');" id="' + zipCode + '"><h3 style="width:100%"><div class="ui-grid-a ui-responsive"><div class="ui-block-a" style="width:80%">' + zipCode + ' - ' + city_name + '';
                    else
                        li_zips = '<div data-theme="c" data-role="collapsible" data-collapsed-icon="carat-r" data-expanded-icon="carat-d" onclick="getZipInfo(' + zipCode + ');" id="' + zipCode + '"><h3 style="width:100%"><div class="ui-grid-a ui-responsive"><div class="ui-block-a" style="width:80%">' + zipCode + ' - ' + city_name + '';
                    li_all_routes = '<div data-role="fieldcontain" data-inset="false" data-theme="c"><fieldset id="fieldset" data-role="controlgroup" data-mini="true" data-inset="false" data-theme="c">';
                    var selected_routes_count = 0;
                    var len = 0;
                    var is_Crrt_Starts_With_B = false;
                    if (zip_val.crrts != null || zip_val.crrts.length > 0) {
                        li_chk_routes = "";
                        i = 0;
                        len = zip_val.crrts.length;
                        var zip_targeted_cnt = 0;
                        target_selected = 0;
                        var zip_avbl_cnt = 0;
                        $.each(zip_val.crrts, function (key, val) {
                            allzip += parseInt(val.available);

                            var is_checked = '';
                            zip_targeted_cnt += parseInt(val.targeted);
                            target_selected += parseInt(val.targeted);
                            zip_avbl_cnt += parseInt(val.available);
                            if (val.isChecked == 1) {
                                selected_routes_count++;
                                //zip_targeted_cnt += parseInt(val.available);
                                //target_selected += parseInt(val.available);
                                is_checked = ' checked="checked" ';
                            } else {
                                is_checked = '';
                            }
                            if (is_checked == '')
                                is_all_checkbox_selected = false;
                            if (!is_Crrt_Starts_With_B)
                                is_Crrt_Starts_With_B = (val.crrtName.toUpperCase().startsWith('B'));
                            //li_chk_routes += '<input type="checkbox" id="check_' + zipCode + '_' + i + '" onmouseup="chkSingleCRRT();" onchange="selectAllZips(false,this,' + val.available + ', \'' + zip_val.alsoInStore + '\')" targeted ="' + val.targeted + '" value="' + val.available + '" "' + is_checked + '"/>';
                            li_chk_routes += '<input data-theme="c" type="checkbox" id="check_' + zipCode + '_' + i + '" onmouseup="chkSingleCRRT();" onchange="checkEntireZip(false,this,' + val.available + ', \'' + zip_val.alsoInStore + '\',\'' + zipCode + '\')" selectedType="' + val.selectType + '" targeted ="' + val.targeted + '" value="' + val.available + '" ' + is_checked + '/>';

                            li_chk_routes += '<label for="check_' + zipCode + '_' + i + '" onmouseup="chkSingleCRRT();"><span style="font-weight:normal;font-size:12px;color:#369">' + val.crrtName + ' | <span  id="spn_' + zipCode + '_' + i + '">' + val.targeted + '</span> of ' + val.available + '</span></label>';
                            i = i + 1;
                        });
                        totalTargeted += target_selected;
                    }
                    //if (selected_routes_count == len)
                    if (zip_avbl_cnt == target_selected)
                        //li_all_routes += '<input type="checkbox" name="checkAll" onchange="selectAllZips(true,this,0, \'' + zip_val.alsoInStore + '\')" id="checkAll' + zipCode + '" checked/>';
                        li_all_routes += '<input data-theme="c" type="checkbox" name="checkAll" onchange="checkEntireZip(true,this,0, \'' + zip_val.alsoInStore + '\',\'' + zipCode + '\')" id="checkAll' + zipCode + '" checked />';
                    else
                        li_all_routes += '<input data-theme="c" type="checkbox" name="checkAll" onchange="checkEntireZip(true,this,0, \'' + zip_val.alsoInStore + '\',\'' + zipCode + '\')" id="checkAll' + zipCode + '"/>';
                    //li_all_routes += '<label for="checkAll' + zipCode + '">Select Entire Zip<span style="font-weight:normal;font-size:12px;color:#369;"> | Avlb: ' + zip_val.available + '</span></label>';
                    li_all_routes += '<label for="checkAll' + zipCode + '">Select Entire Zip<span style="font-weight:normal;font-size:12px;color:#369;"> | <span id="spnCheckAll' + zipCode + '">' + target_selected + '</span> of <span id="spnCheckAllAvbl' + zipCode + '">' + zip_val.available + '</span> targeted</span></label>';
                    $('#divAvailable').html(zip_val.available);
                    $('#spnRadiusAvblCount').html(zip_val.available);

                    if (is_Crrt_Starts_With_B && (jobCustomerNumber === CASEYS)) {
                        li_chk_routes += '<div style="font-size:12px;padding-top:8px">Carrier Routes starting with "B" are P.O. Boxes and will not display on the map.</div>';
                        is_Crrt_Starts_With_B = false;
                    }

                    li_all_routes += li_chk_routes + '</fieldset></div>';
                    var checked_status = '';
                    if (!$('#dvSelectByRadius').hasClass('ui-collapsible-collapsed')) {
                        if (selected_routes_count > 0)
                            checked_status = 'checked';
                    }
                    else {
                        if (selected_routes_count == len) {
                            checked_status = 'checked';
                        }
                    }
                    var entire_zip_option = "";
                    if (checked_status != "") {
                        if (zip_val.alsoInStore == null)
                            //li_zips += '<div data-role="fieldcontain" style="width:40px;" data-inset="true" data-mini="true"  class="ui-li-aside" ><fieldset data-role="controlgroup" data-inset="false" data-mini="true" style="width:40px"><legend></legend><input class="my-list" type="checkbox" name="chkEntireZip' + zipCode + '" id="chkEntireZip' + zipCode + '" class="custom" onchange="checkEntireZip(\'' + zipCode + '\',this)" onclick="chkClick(event);" data-theme="b" data-mini="true" checked/><label for="chkEntireZip' + zipCode + '" onclick="chkClick(event);" style="width:40px">&nbsp;</label></fieldset></div>';
                            entire_zip_option += '<div class="ui-block-b" style="width:20%"><label for="chkEntireZip' + zipCode + '" onclick="chkClick(event);" data-theme="' + data_theme + '" style="border-left-width: 1px;color: transparent;width: 1px;border-right-width: 1px;padding-left: 20px;">&nbsp;</label><input class="my-list" type="checkbox" data-theme="' + data_theme + '" name="chkEntireZip' + zipCode + '" id="chkEntireZip' + zipCode + '" class="custom" onchange="checkEntireZip(true,this,\'\',\'' + zip_val.alsoInStore + '\',\'' + zipCode + '\')" onclick="chkClick(event);" data-mini="true" ' + checked_status + ' style="left: 0.28em;top: 43%;" /></div>';
                        else
                            //li_zips += '<div data-theme="e" data-role="fieldcontain" style="width:40px;" data-inset="true" data-mini="true"  class="ui-li-aside" ><fieldset data-role="controlgroup" data-inset="false" data-mini="true" style="width:40px" data-theme="e"><legend></legend><input class="my-list" type="checkbox" name="chkEntireZip' + zipCode + '" id="chkEntireZip' + zipCode + '" class="custom" onchange="checkEntireZip(\'' + zipCode + '\',this)" onclick="chkClick(event);" data-theme="e" data-mini="true" checked/><label data-theme="e" for="chkEntireZip' + zipCode + '" onclick="chkClick(event);" style="width:40px">&nbsp;</label></fieldset></div>';
                            entire_zip_option += '<div class="ui-block-b" style="width:20%"><label data-theme="' + data_theme + '" for="chkEntireZip' + zipCode + '" onclick="chkClick(event);" style="border-left-width: 1px;color: transparent;width: 1px;border-right-width: 1px;padding-left: 20px;" style="border-left-width: 1px;color: transparent;width: 1px;border-right-width: 1px;padding-left: 20px;">&nbsp;</label><input class="my-list" type="checkbox" data-theme="' + data_theme + '"  name="chkEntireZip' + zipCode + '" id="chkEntireZip' + zipCode + '" class="custom" onchange="checkEntireZip(true,this,\'\',\'' + zip_val.alsoInStore + '\',\'' + zipCode + '\')" onclick="chkClick(event);" data-mini="true" ' + checked_status + ' style="left: 0.28em;top: 43%;" /></div>';
                    }

                    if (checked_status == "") {
                        if (zip_val.alsoInStore == null)
                            //li_zips += '<div data-role="fieldcontain" style="width:40px;" data-inset="true" data-mini="true"  class="ui-li-aside"><fieldset  data-role="controlgroup" data-inset="false" data-mini="true" style="width:40px"><legend></legend><input class="my-list" type="checkbox" name="chkEntireZip' + zipCode + '" id="chkEntireZip' + zipCode + '" class="custom" onchange="checkEntireZip(\'' + zipCode + '\',this)" onclick="chkClick(event);" data-theme="b" data-mini="true"/><label for="chkEntireZip' + zipCode + '" onclick="chkClick(event);" style="width:40px">&nbsp;</label></fieldset></div>';
                            entire_zip_option += '<div class="ui-block-b" style="width:20%"><label for="chkEntireZip' + zipCode + '" onclick="chkClick(event);" data-theme="' + data_theme + '" style="border-left-width: 1px;color: transparent;width: 1px;border-right-width: 1px;padding-left: 20px;">&nbsp;</label><input class="my-list" type="checkbox" data-theme="' + data_theme + '"  name="chkEntireZip' + zipCode + '" id="chkEntireZip' + zipCode + '" class="custom" onchange="checkEntireZip(true,this,\'\',\'' + zip_val.alsoInStore + '\',\'' + zipCode + '\')" onclick="chkClick(event);" data-mini="true" style="left: 0.28em;top: 43%;" /></div>';
                        else
                            //li_zips += '<div data-theme="e" data-role="fieldcontain" style="width:40px;" data-inset="true" data-mini="true"  class="ui-li-aside"><fieldset data-theme="e" data-role="controlgroup" data-inset="false" data-mini="true" style="width:40px"><legend></legend><input class="my-list" type="checkbox" name="chkEntireZip' + zipCode + '" id="chkEntireZip' + zipCode + '" class="custom" onchange="checkEntireZip(\'' + zipCode + '\',this)" onclick="chkClick(event);" data-theme="e" data-mini="true"/><label for="chkEntireZip' + zipCode + '" onclick="chkClick(event);" style="width:40px" data-theme="e">&nbsp;</label></fieldset></div>';
                            entire_zip_option += '<div class="ui-block-b" style="width:20%"><label for="chkEntireZip' + zipCode + '" onclick="chkClick(event);" data-theme="' + data_theme + '" style="border-left-width: 1px;color: transparent;width: 1px;border-right-width: 1px;padding-left: 20px;">&nbsp;</label><input class="my-list" type="checkbox" data-theme="' + data_theme + '"  name="chkEntireZip' + zipCode + '" id="chkEntireZip' + zipCode + '" class="custom" onchange="checkEntireZip(true,this,\'\',\'' + zip_val.alsoInStore + '\',\'' + zipCode + '\')" onclick="chkClick(event);" data-mini="true" style="left: 0.28em;top: 43%;" /></div>';
                    }

                    li_zips += '<br/><span style="font-weight:normal;font-size:12px;">Available: ' + zip_val.available + ' | Targeted: <span style="font-weight:normal;font-size:12px;" id="spnTargetedZipCnt' + zipCode + '">' + zip_targeted_cnt + '</span></span><br/>';

                    var zip_status = "";
                    if (selected_routes_count == len && target_selected == zip_val.available)
                        zip_status = "Entire Zip Selected"
                    else if (selected_routes_count == len && target_selected < zip_val.available)
                        zip_status = ""
                    else if (selected_routes_count < len)
                        zip_status = selected_routes_count + ' of ' + len + ' Carrier Routes Selected';

                    if (selected_routes_count == len) {
                        if (zip_val.alsoInStore == null)
                            li_zips += '<span id="spn' + zipCode + '" style="font-weight:normal;font-size:12px;">' + zip_status + '</span>';
                        else
                            li_zips += '<span id="spn' + zipCode + '" style="font-weight:normal;font-size:12px;">' + zip_status + '<br />Also in Store: ' + zip_val.alsoInStore + '</span>';
                    }
                    else {
                        if (zip_val.alsoInStore == null)
                            li_zips += '<span id="spn' + zipCode + '" style="font-weight:normal;font-size:12px;">' + zip_status + '</span>';
                        else
                            li_zips += '<span id="spn' + zipCode + '" style="font-weight:normal;font-size:12px;">' + zip_status + '<br />Also in Store: ' + zip_val.alsoInStore + '</span>';
                    }
                    li_zips += '</div>' + entire_zip_option + '</div></h3>';
                    li_zips += li_all_routes;
                    li_zips += '</div>';
                    if (is_all_checkbox_selected)
                        $('#checkAll').attr("checked", true);
                    index++;
                    $('#dvLocations').append(li_zips);
                }
            });
            $("#dvLocations").trigger("create");
            $($('div[class=ui-checkbox]').find('label[for^=chkEntireZip]')).each(function (i) {
                $(this).find('span').css('padding-left', '1px');
            });
        }
        $('#divAvailable').html(totalAvailable);
        $('#spnRadiusAvblCount').html(totalAvailable);
        $('#txtRadiusTargeted').val(totalTargeted);
        $('#spnRadiusTargeted').html(totalTargeted);
    }
    else {
        $('#hdnSelectedStore').val('');
    }
    if (!dontShowHintsAgain && (sessionStorage.showDemoHints == undefined || sessionStorage.showDemoHints == null || sessionStorage.showDemoHints == "" || sessionStorage.showDemoHints == "on") && (sessionStorage.isPostalBoundarySelectionsHintsDisplayed == undefined || sessionStorage.isPostalBoundarySelectionsHintsDisplayed == null || sessionStorage.isPostalBoundarySelectionsHintsDisplayed == "" || sessionStorage.isPostalBoundarySelectionsHintsDisplayed == "false") && (jobCustomerNumber == JETS || jobCustomerNumber == ALLIED || jobCustomerNumber == CASEYS)) {
        sessionStorage.isPostalBoundarySelectionsHintsDisplayed = true;
        $('#postalBoundarySelections').popup('open', { positionTo: '#dvPostalBoundarySelections' });
    }
}
function getZipInfo(zip_code) {
    $('#hdnSelectedZip').val(zip_code);
}

function getDifferences(oldObj, newObj) {
    var diff = {};

    for (var k in oldObj) {
        if (!(k in newObj))
            diff[k] = undefined;  // property gone so explicitly set it undefined
        else if (oldObj[k] !== newObj[k])
            //else if (!isEqual(oldObj[k], newObj[k]))
            diff[k] = newObj[k];  // property in both but has changed
    }

    for (k in newObj) {
        if (!(k in oldObj))
            diff[k] = newObj[k]; // property is new
    }

    return diff;
}

//when clicked on "Save Selections" button..
function saveSelections() {
    //var selected_crrts = $('div input[type="checkbox"]:not([id^=checkAll]):not([id^=chkEntireZip]):checked');
    var selected_crrts = $('div[id=dvLocations] input[type="checkbox"]:not([id^=checkAll]):not([id^=chkEntireZip]):checked');
    var diff = [];
    if ((selectedCRRTS.length > 0 && selected_crrts.length > 0) || (selectedCRRTS.length > 0 && selected_crrts.length == 0) || (selectedCRRTS.length == 0 && selected_crrts.length == 0))
        diff = getDifferences(selected_crrts, selectedCRRTS);
    else if (selectedCRRTS.length == 0 && selected_crrts.length > 0)
        diff = getDifferences(selectedCRRTS, selected_crrts);

    if (diff.length == undefined || diff.length == 0) {
        //Get the complete jon info.
        if (sessionStorage.jobSetupOutput != undefined) {
            gOutputData = jQuery.parseJSON(sessionStorage.jobSetupOutput);
        }

        //get the selected zips info for the selected store.
        if (sessionStorage.storeListSelection != undefined && sessionStorage.storeListSelection != null) {
            gSelectedZips = jQuery.parseJSON(sessionStorage.storeListSelection);
        }

        if (gSelectedZips != undefined && gSelectedZips != null) {
            if (gOutputData.listSelection != undefined && gOutputData.listSelection != "") {
                var zip_index = 0;
                var selected_zip = jQuery.grep(gOutputData.listSelection, function (key, obj) {
                    zip_index = obj;
                    return key.storeId === selectedStoreId;
                });
                if (selected_zip != undefined && selected_zip.length == 0) {
                    selected_zip = gSelectedZips;
                    gOutputData.listSelection.push(gSelectedZips);
                }
                else {
                    selected_zip = gSelectedZips;
                    gOutputData.listSelection[zip_index] = selected_zip;
                }
            } else {
                gOutputData["listSelection"] = [];
                gOutputData.listSelection.push(gSelectedZips);
            }
        }
        sessionStorage.jobSetupOutput = JSON.stringify(gOutputData);
        sessionStorage.removeItem('storeListSelection');
        $('#hdnSelectedStore').val('');
        var post_json_zips = (selected_zip != undefined) ? selected_zip : gSelectedZips;
        //postCORS(myProtocol + "maps.tribunedirect.com/api/ZipSelectList/1/" + (appPrivileges.customerNumber == CW) ? jobNumber : caseysStaicJobNumber + "/" + selectedStoreId + "/store_select", JSON.stringify(post_json_zips), function (response) {
        //postCORS(myProtocol + "maps.tribunedirect.com/api/ZipSelectList/" + appPrivileges.customerNumber + "/1/" + ((appPrivileges.customerNumber == CW) ? jobNumber : caseysStaicJobNumber + "/" + selectedStoreId), JSON.stringify(post_json_zips), function (response) {
        postCORS(mapsUrl + "api/ZipSelectList_save/" + appPrivileges.customerNumber + "/1/" + ((appPrivileges.customerNumber == CW) ? jobNumber : caseysStaicJobNumber + "/" + selectedStoreId), JSON.stringify(post_json_zips), function (response) {
            var msg = (response.toLowerCase() == "\"success\"") ? 'Your selections have been saved.' : 'Your selections have not been saved.';
            $('#alertmsg').text(msg);
            $('#popupDialog').popup('open');
        }, function (response_error) {
            //var msg = 'Your selections have not been saved.';
            var msg = response_error.responseText;
            $('#alertmsg').text(msg);
            $('#popupDialog').popup('open');
        });
        $('#btnRevert').addClass('ui-disabled');
        sessionStorage.removeItem('updatedCRRTs');
        selectedCRRTS = [];
        isTargetChanged = false;
        isRadiusChanged = false;
    }
    else {
        var msg = "The Targeted Selections have been changed for Store '" + $('#hdnSelectedStore').val() + "'. " +
                  "To save selections you must refresh the map. Click 'Ok' to Refresh Map.";
        $('#confirmMsg').text(msg);
        $('#popupConfirmDialog').popup('open');
    }
}

//when clicked on "Refresh Map" button to validate the targeted quantity. If the entered quantity is greater than total targeted quanity, show message.
function validateTargetQty() {
    //    var entered_targeted_qty = parseInt($("#txtTargeted").val());
    //    alert("entered_targeted_qty:" + entered_targeted_qty);
    //    var total_targeted = parseInt(getTargetedCounts());
    //    alert("total_targeted:" + total_targeted);
    //    $("#txtTargeted").val(total_targeted);
    //    var radius = parseInt($("#txtRadius").val());
    //    var error_text = "The targeted quantity of " + entered_targeted_qty + " cannot be found within a " + radius + " mile radius of this location. ";
    //    error_text += "Either increase the radius selection or decrease the targeted quantity.";
    //    if (entered_targeted_qty > total_targeted) {
    //        $('#alertmsg').text(error_text);
    //        $('#popupDialog').popup('open');
    //        $("#txtTargeted").val(entered_targeted_qty);
    //        return false;
    //    }
    //    else {
    //        refreshMap();
    //    }
    var targeted_qty = parseInt($("#txtTargeted").val());
    if (targeted_qty <= 0) {
        $('#alertmsg').text("Please select at least One Zip route.");
        $('#popupDialog').popup('open');
        $("#txtTargeted").val(targeted_qty);
        $("#spnTargeted").html(targeted_qty);
        return false;
    }
    else {
        return true;
    }
}


//when user checks "Select Entire Zip" / "Individual Zip codes"..
function selectAllZips(is_check_all, ctrl, avlb, also_in_store) {
    ctrl = $('#' + ctrl)[0];
    //var selected_crrts = $('div input:checked:not("id^=checkAll"):not("id^=chkEntireZip")');
    //var selected_crrts = $('div input[type="checkbox"]:not([id^=checkAll]):not([id^=chkEntireZip]):checked');
    var selected_crrts = $('div[id=dvLocations] input[type="checkbox"]:not([id^=checkAll]):not([id^=chkEntireZip])');
    if (is_check_all)
        selectedCRRTS = $.extend(true, {}, selected_crrts);

    var selected_zip = $('#hdnSelectedZip').val();
    var is_select_all = $('#checkAll' + selected_zip)[0].checked;
    var targeted_value = 0;
    $('#hdnSelectedStore').val($('#ddlStoreLocations option:selected').text());
    //    var store_info = jQuery.grep(gData, function (obj) {
    //        return obj.storeId === selectedStoreId;
    //    });
    store_info = gData;
    var zip_routes = jQuery.grep(store_info.zips, function (key, obj) {
        return key.zipCode === parseInt(selected_zip);
    });
    if (!is_select_all) {
        targeted_value = zip_routes[0].targeted;
    }

    //getting all crrt checkboxes in that selected zip.
    var selected_zip_crrts = $('#' + selected_zip + ' input:checkbox:not("#checkAll' + selected_zip + '"):not("#chkEntireZip' + selected_zip + '")');
    if (is_check_all) {
        $.each(selected_zip_crrts, function (i, j) {
            if (is_select_all)
                $(this).attr('checked', true).checkboxradio('refresh');
            else
                $(this).attr('checked', false).checkboxradio('refresh');
        });
        if (is_select_all)
            $('#chkEntireZip' + selected_zip).attr('checked', true).checkboxradio('refresh');
        else
            //
            $('#chkEntireZip' + selected_zip).attr('checked', false).checkboxradio('refresh');
    }

    //getting previous selections in the selected zip.
    //var selected_crrts = $('#' + selected_zip + ' input:checkbox:not("#checkAll' + selected_zip + '):not("#chkEntireZip' + selected_zip + '")');
    var selected_crrts = $('#' + selected_zip + ' input:checkbox:not("#checkAll' + selected_zip + '"):not("#chkEntireZip' + selected_zip + '")');
    var prev_targetted_value = 0;
    var index = 0;

    var udpated_crrts = (sessionStorage.updatedCRRTs != undefined && sessionStorage.updatedCRRTs != null && sessionStorage.updatedCRRTs != "") ? jQuery.parseJSON(sessionStorage.updatedCRRTs) : [];
    $.each(selected_crrts, function (a, b) {
        var ctrl_id = $(this)[0].id;
        var current_crrt = $.grep(udpated_crrts, function (obj) {
            return obj.crrtID === ctrl_id;
        });

        if ($(this)[0].checked) {
            if (ctrl.id.split('_').length > 2 && $(this)[0].id != ctrl.id) {
                prev_targetted_value += parseInt($(b).attr('targeted'));
                $('#spn_' + zip_routes[0].zipCode + '_' + a).text($(b).attr('targeted'));
            }
            else {
                prev_targetted_value += parseInt(b.value);
                $('#spn_' + zip_routes[0].zipCode + '_' + a).text(b.value);
                $(b).attr('targeted', b.value)
                $(b).attr('isCrrtChanged', true);
                if (current_crrt.length > 0) {
                    current_crrt[0].crrtID = $(this)[0].id;
                    current_crrt[0].status = 'checked';
                }
                else {
                    udpated_crrts.push({
                        crrtID: $(this)[0].id,
                        status: 'checked'
                    });
                }
            }
        }
        else {
            $('#spn_' + zip_routes[0].zipCode + '_' + a).text(0);
            if (current_crrt.length > 0) {
                current_crrt[0].crrtID = $(this)[0].id;
                current_crrt[0].status = 'unchecked';
            }
            else {
                udpated_crrts.push({
                    crrtID: $(this)[0].id,
                    status: 'unchecked'
                });
            }
        }
        index++;
    });
    var the_spn = '';
    $('#spnCheckAll' + zip_routes[0].zipCode).text(prev_targetted_value);
    //var zip_selected_crrts = $('#' + selected_zip + ' input:checkbox:not("#checkAll' + selected_zip + '):not("#chkEntireZip' + selected_zip + '"):checked');
    var zip_selected_crrts = $('#' + selected_zip + ' input:checkbox:not("#checkAll' + selected_zip + '"):not("#chkEntireZip' + selected_zip + '"):checked');

    var checked_status = false;
    if (!$('#dvSelectByRadius').hasClass('ui-collapsible-collapsed')) {
        if (zip_selected_crrts.length > 0)
            checked_status = true;
    }
    else {
        if (zip_selected_crrts.length == selected_crrts.length) {
            checked_status = true;
        }
    }
    if (checked_status) {
        $('#chkEntireZip' + selected_zip).attr('checked', checked_status).checkboxradio('refresh');
        if (zip_selected_crrts.length == selected_crrts.length)
            $('#checkAll' + selected_zip).attr('checked', checked_status).checkboxradio('refresh');
        else {
            $('#checkAll' + selected_zip).attr('checked', false).checkboxradio('refresh');
            the_spn = zip_selected_crrts.length + ' of ' + selected_crrts.length + ' Carrier Routes Selected';
        }

        if (parseInt($('#spnCheckAllAvbl' + zip_routes[0].zipCode).text()) == prev_targetted_value)
            the_spn = 'Entire Zip Selected';
    }
    else {
        $('#chkEntireZip' + selected_zip).attr('checked', checked_status).checkboxradio('refresh');
        $('#checkAll' + selected_zip).attr('checked', checked_status).checkboxradio('refresh');
        the_spn = zip_selected_crrts.length + ' of ' + selected_crrts.length + ' Carrier Routes Selected';
    }

    if (also_in_store != 'null' && also_in_store != null) {
        the_spn += '<br/>Also in Store: ' + also_in_store;
    }
    $('#spn' + selected_zip).html(the_spn);

    $('#spnTargetedZipCnt' + selected_zip).html(prev_targetted_value);
    $('div.ui-collapsible-content', "#" + $('#hdnSelectedZip').val()).trigger('expand');

    //var selected_crrts = $('#' + selected_zip + ' input:checkbox:not("#checkAll' + selected_zip + '):not("#chkEntireZip' + selected_zip + '")');
    //var selected_crrts = $('div input[type="checkbox"]:not([id^=checkAll]):not([id^=chkEntireZip]):checked');
    var selected_crrts = $('div[id=dvLocations] input[type="checkbox"]:not([id^=checkAll]):not([id^=chkEntireZip])');
    var diff = [];
    if ((selectedCRRTS.length > 0 && selected_crrts.length > 0) || (selectedCRRTS.length > 0 && selected_crrts.length == 0) || (selectedCRRTS.length == 0 && selected_crrts.length == 0))
        //diff = getDifferences(selectedCRRTS, selected_crrts);
        diff = getDifferences(selected_crrts, selectedCRRTS);
    else if (selectedCRRTS.length == 0 && selected_crrts.length > 0)
        //diff = getDifferences(selected_crrts, selectedCRRTS);
        diff = getDifferences(selectedCRRTS, selected_crrts);
    if (diff.length != undefined && diff.length > 0)
        $('#btnSaveSelections').removeClass('ui-disabled');
    $('#btnRefresh').removeClass('ui-disabled');

    var zips_changed = $('#hdnZipsChanged').val();
    if (zips_changed.indexOf(zip_routes[0].zipCode) == -1)
        zips_changed += (zips_changed != "") ? '|' + zip_routes[0].zipCode : zip_routes[0].zipCode;
    $('#hdnZipsChanged').val(zips_changed);
    sessionStorage.updatedCRRTs = JSON.stringify(udpated_crrts);
}


//when user checks "Select Entire Zip" / "Individual Zip codes"..
function calculateTargetedCnt() {

    //eval(getURLDecode(pagePrefs.scriptBlockDict.calculateTargetedCnt));
    if (appPrivileges.customerNumber == CW) {
        var selected_crrts = $('div[id=dvLocations] input[type="checkbox"]:not([id^=checkAll]):[id^=chkEntireZip]');
        $.each(selected_crrts, function (key1, val1) {
            $(val1).attr('checked', true).checkboxradio('refresh');
        });
        //var selected_crrts = $('div input[type="checkbox"]:not([id^=checkAll]):[id^=chkEntireZip]:checked');
        var selected_crrts = $('div[id=dvLocations] input[type="checkbox"]:not([id^=checkAll]):[id^=chkEntireZip]');
        //        $.each(gDataAll.zips, function (key, val) {
        //            $('#spnTargetedZipCnt' + val.zipCode).html(val.available);
        //            //$('#spnTargetedZipCnt' + val.zipCode).html(val.available);
        //        });
        gSelectedZips = $.extend(true, {}, gDataAll);
        $.each(gSelectedZips.zips, function (key, val) {
            val.crrts = [];
        });
        sessionStorage.storeListSelection = JSON.stringify(gSelectedZips);
        refreshMap();
    }
    else {
        //var selected_crrts = $('div input[type="checkbox"]:not([id^=checkAll]):not([id^=chkEntireZip]):checked');
        var selected_crrts = $('div[id=dvLocations] input[type="checkbox"]:not([id^=checkAll]):not([id^=chkEntireZip]):checked');
        //var selected_crrts = $('div input:checked:not("id^=checkAll"):not("id^=chkEntireZip")');
        var diff = [];
        if ((selectedCRRTS.length > 0 && selected_crrts.length > 0) || (selectedCRRTS.length > 0 && selected_crrts.length == 0) || (selectedCRRTS.length == 0 && selected_crrts.length == 0))
            //diff = getDifferences(selectedCRRTS, selected_crrts);
            diff = getDifferences(selected_crrts, selectedCRRTS);
        else if (selectedCRRTS.length == 0 && selected_crrts.length > 0)
            //diff = getDifferences(selected_crrts, selectedCRRTS);
            diff = getDifferences(selectedCRRTS, selected_crrts);

        //var targed_cnt = 0;
        //        $.each(selected_crrts, function (i, j) {
        //            if ($(this)[0].id.toLowerCase().indexOf('checkall') == -1 && $(this)[0].id.toLowerCase().indexOf('chkentirezip') == -1 && $(this)[0].checked) {
        //                //                var tar_zip_cnt = ($(this)[0].targeted != undefined) ? $(this)[0].targeted : 0;
        //                //                var avbl_zip_cnt = ($(this)[0].value != undefined) ? $(this)[0].value : 0;
        //                //                targed_cnt = ((!$('#dvSelectByRadius').hasClass('ui-collapsible-collapsed')) ? parseInt(tar_zip_cnt) : parseInt(avbl_zip_cnt)) + parseInt(targed_cnt);
        //                targed_cnt = parseInt(targed_cnt) + parseInt($(this)[0].value);
        //            }
        //        });
        //$('#txtTargeted').html(targed_cnt);
        //if (validateTargetQty()) {
        var temp_g_data = $.extend(true, {}, gData);
        var temp_g_data = gData;
        var targeted_qty = 0;
        var udpated_crrts = (sessionStorage.updatedCRRTs != undefined && sessionStorage.updatedCRRTs != null && sessionStorage.updatedCRRTs != "") ? jQuery.parseJSON(sessionStorage.updatedCRRTs) : [];
        if (temp_g_data != null && temp_g_data.zips != undefined) {
            var zip_routes = jQuery.grep(temp_g_data.zips, function (key, obj) {
                var zip = key.zipCode;
                var is_selected = false;
                var index = 0;
                var selected_crrts_cnt = 0;
                $.each(key.crrts, function (i, j) {
                    var current_crrt = $.grep(udpated_crrts, function (obj) {
                        return obj.crrtID === 'check_' + zip + '_' + index;
                    });
                    if ($('#check_' + zip + '_' + index)[0] != undefined && $('#check_' + zip + '_' + index)[0].checked) {
                        is_selected = true;
                        j.isChecked = 1;
                        if (current_crrt.length == 0)
                            selected_crrts_cnt = ((!$('#dvSelectByRadius').hasClass('ui-collapsible-collapsed')) ? parseInt(j.targeted) : parseInt(j.available)) + parseInt(selected_crrts_cnt);
                    }
                    else {
                        j.isChecked = 0;
                    }
                    if (current_crrt.length > 0)
                        if (current_crrt[0].status == 'checked') {
                            j.selectType = 'f';
                            j.targeted = j.available;
                            selected_crrts_cnt = ((!$('#dvSelectByRadius').hasClass('ui-collapsible-collapsed')) ? parseInt(j.targeted) : parseInt(j.available)) + parseInt(selected_crrts_cnt);
                        }
                        else {
                            j.selectType = '';
                            j.targeted = 0;
                        }
                    index++;
                });
                if (is_selected) {
                    key.targeted = selected_crrts_cnt;
                    key.isChecked = 1;
                    targeted_qty = parseInt(targeted_qty) + parseInt(selected_crrts_cnt);
                    return key;
                }
            });
            $('#spnTargeted').html(targeted_qty);
            if ($('#hdnZipsChanged').val() != "") {
                $('#spnRadiusTargeted').html(targeted_qty);
                $('#txtRadiusTargeted').val(targeted_qty);
            }
            temp_g_data.targetedQty = targeted_qty;
            gData = $.extend(true, {}, temp_g_data);
            temp_g_data.zips = zip_routes;
            gSelectedZips = temp_g_data;
            gSelectedZips.wantsScatter = ($('#chkViewByHouseHold')[0].checked) ? 1 : 0;
            sessionStorage.storeListSelection = JSON.stringify(gSelectedZips);
            refreshMap();
            //selectedCRRTS = $('div input:checked:not("id^=checkAll"):not("id^=chkEntireZip")');
            if (udpated_crrts.length > 0)
                selectedCRRTS = $('div[id=dvLocations] input[type="checkbox"]:not([id^=checkAll]):not([id^=chkEntireZip]):checked');
            if (diff.length != undefined && diff.length > 0)
                $('#btnRevert').removeClass('ui-disabled');
        }
    }
    $('#hdnZipsChanged').val('');
    //sessionStorage.removeItem('updatedCRRTs');
    $('#waitPopUp').popup('close');
}
function buildSelectedZips() {
    //    var store_info = jQuery.grep(gSelectedZips, function (obj) {
    //        return obj.storeId === selectedStoreId;
    //    });
    var e = 0;
    var zip_code = 0;
    var index = 0;
    var selected_crrts_cnt = 0;
    var target_selected = 0;

    $.each(gSelectedZips.zips, function (a, b) {
        e = 0;
        zip_code = b.zipCode;
        index = 0;
        selected_crrts_cnt = 0;
        target_selected = 0;
        $.each(b.crrts, function (c, d) {
            if (d.isChecked) {
                $('#check_' + zip_code + '_' + index).attr('checked', true).checkboxradio('refresh');
                $('#spn_' + zip_code + '_' + index).text(d.targeted);
                target_selected += parseInt(d.targeted);
                selected_crrts_cnt++;
            }
            index++;
        });
        var the_spn = '';


        var zip_status = "";
        if (selected_crrts_cnt == b.crrts.length && target_selected == b.available)
            zip_status = "Entire Zip Selected"
        else if (selected_crrts_cnt == b.crrts.length && target_selected < b.available)
            zip_status = ""
        else if (selected_crrts_cnt < b.crrts.length)
            zip_status = selected_crrts_cnt + ' of ' + b.crrts.length + ' Carrier Routes Selected';

        if (index == selected_crrts_cnt) {
            if (target_selected == b.available)
                $('#checkAll' + zip_code).attr('checked', true).checkboxradio('refresh');
            else
                $('#checkAll' + zip_code).attr('checked', false).checkboxradio('refresh');
            $('#chkEntireZip' + zip_code).attr('checked', true).checkboxradio('refresh');
            //the_spn = 'Entire Zip Selected';
        }
        the_spn = zip_status;
        $('#spnTargetedZipCnt' + b.zipCode).text(b.targeted);
        //        else {
        //            the_spn = selected_crrts_cnt + ' of ' + index + ' Carrier Routes Selected';
        //        }

        //
        if (b.alsoInStore != 'null' && b.alsoInStore != null) {
            the_spn += '<br/>Also in Store: ' + b.alsoInStore;
        }
        $('#spn' + zip_code).html(the_spn);

    });
}
function refreshMap() {
    makeGoogleMap();
    $("#dvMapMarker").css('display', 'block');
}

function showStoreChangeMessage(page_name) {
    var msg_box = '';
    msg_box = '<div data-role="popup" id="popupStoreDialog" data-overlay-theme="a" data-history="false" data-theme="c" data-dismissible="false" style="max-width:500px;" class="ui-corner-all">' +
					'<div data-role="header" data-theme="d" class="ui-corner-top">' +
					'	<h1>Confirm Changes?</h1>' +
					'</div>' +
					'<div data-role="content" data-theme="d" class="ui-corner-bottom ui-content">' +
					'<div id="storeMsg"></div>' +
					'	<a href="#" data-role="button" data-inline="true" data-mini="true" id="btnModelSave" data-theme="a" onclick="savaSelectedInfo();">Save Selections</a>' +
					'	<a href="#" data-role="button" data-inline="true" data-mini="true" id="btnModelRevert" data-transition="flow" data-theme="a" onclick="revertSelections();">Revert</a>' +
                    '	<a href="#" data-role="button" data-inline="true" data-mini="true" id="btnCancel" data-transition="flow" data-theme="a" onclick="cancelSelections()">Cancel</a>' +
					'</div>' +
				'</div>';
    $("#" + page_name).append(msg_box);
}

function savaSelectedInfo() {
    saveSelections();
    getSelectedStoreData();
    $('#popupStoreDialog').popup('open');
    $('#popupStoreDialog').popup('close');
    $('#sldrRange').slider('enable');
    $('#sldrRange')[0].disabled = false;
}

function revertSelections() {
    $('#popupStoreDialog').popup('close');
    $('#waitPopUp').popup('open', { positionTo: 'window' });
    window.setTimeout(function getDelay5() {
        gSelectedZips = [];
        //$('#ddlStoreLocations').val(selectedStoreId).selectmenu('refresh');

        $('#hdnSelectedStore').val('');
        //    var checked_items = $('input:checked');
        //    $.each(checked_items, function (a, b) {
        //        $(this).attr('checked', false).checkboxradio('refresh');
        //    });
        //getDataFromSession();
        isTargetChanged = false;
        isRadiusChanged = false;
        selectedCRRTS = [];
        sessionStorage.removeItem('storeListSelection');
        sessionStorage.removeItem('updatedCRRTs');
        //buildSelectedZips();
        //refreshMap();
        $('#sldrRange').attr('hasChanged', false);
        $('#sldrRange').val('1').slider('refresh');
        $('#sldrRange').slider('enable');
        $('#sldrRange')[0].disabled = false;
        $('#rdoRadius').attr('checked', true).checkboxradio('refresh').trigger('change');
        $('#rdoTargetCounts').attr('checked', false).checkboxradio('refresh');
        $('#ddlStoreLocations').trigger('onchange');
        $('#btnRevert').addClass('ui-disabled');
        $('#btnSaveSelections').addClass('ui-disabled');
        if (!$('#dvSelectByRadius').hasClass('ui-collapsible-collapsed'))
            $('#btnSaveSelections').removeClass('ui-disabled');
        $('#waitPopUp').popup('close');
    }, 1000);
}
function cancelSelections() {
    $('#ddlStoreLocations').val(selectedStoreId).selectmenu('refresh');
    $('#popupStoreDialog').popup('close');
    $('#sldrRange').slider('enable');
    $('#sldrRange')[0].disabled = false;
}

function refreshCurrentSelections() {
    $('#popupConfirmDialog').popup('open');
    $('#popupConfirmDialog').popup('close');
    $('#waitPopUp').popup('open', { positionTo: 'window' });
    window.setTimeout(function getDelay4() {
        calculateTargetedCnt();
        selectedCRRTS = $('div[id=dvLocations] input[type="checkbox"]:not([id^=checkAll]):not([id^=chkEntireZip]):checked');
        //selectedCRRTS = [];
        $('#ddlStoreLocations').val(selectedStoreId).selectmenu('refresh');
        //$('#ddlStoreLocations').trigger('onchange');
        //saveSelections();
        $('#sldrRange').slider('enable');
        $('#sldrRange')[0].disabled = false;
        $('#waitPopUp').popup('close');
    }
    , 1000);
}

function revertCurrentSelections() {
    $('#popupConfirmDialog').popup('close');
    $('#ddlStoreLocations').val(selectedStoreId).selectmenu('refresh');
    $('#sldrRange').slider('enable');
    $('#sldrRange')[0].disabled = false;
}

function saveConfirmMessage(page_name) {
    var msg_box = '';
    msg_box = '<div data-role="popup" id="popupConfirmDialog" data-history="false" data-overlay-theme="a" data-theme="c" style="max-width:400px;" class="ui-corner-all">';
    msg_box += '<div data-role="header" data-theme="d" class="ui-corner-top" id="dialogbox">';
    msg_box += '<h1>Confirm</h1>';
    msg_box += '</div>';
    msg_box += '<div data-role="content" data-theme="d" class="ui-corner-bottom ui-content">';
    msg_box += '<div id="confirmMsg"></div>';
    msg_box += '<div id="colId"></div>';
    msg_box += '<div id="valList"></div>';
    msg_box += '<a href="#" data-role="button" data-inline="true" data-theme="a" id="cancelBut" onclick="revertCurrentSelections();">Cancel</a>';
    msg_box += '<a href="#" data-role="button" data-inline="true" data-theme="a"  id="okBut" onclick="refreshCurrentSelections();">Ok</a>';
    msg_box += '</div></div>';
    $("#" + page_name).append(msg_box);
}

function radiusZipCRRTChangeMessage(page_name) {
    var msg_box = '';
    msg_box = '<div data-role="popup" id="popupradiusZipCRRTConfirmDialog" data-history="false" data-overlay-theme="a" data-theme="c" style="max-width:400px;" class="ui-corner-all">';
    msg_box += '<div data-role="header" data-theme="d" class="ui-corner-top" id="dialogbox">';
    msg_box += '<h1>Confirm</h1>';
    msg_box += '</div>';
    msg_box += '<div data-role="content" data-theme="d" class="ui-corner-bottom ui-content">';
    msg_box += '<div id="confirmMsg1"></div>';
    msg_box += '<div id="colId1"></div>';
    msg_box += '<div id="valList1"></div>';
    msg_box += '<a href="#" data-role="button" data-inline="true" data-mini="true" data-theme="a" id="cancelBut1" onclick="">Cancel</a>';
    msg_box += '<a href="#" data-role="button" data-inline="true" data-mini="true" data-theme="a"  id="okBut1" onclick="">Continue</a>';
    msg_box += '</div></div>';
    $("#" + page_name).append(msg_box);
}

function makePageSelect(has_search) {
    var zip_data = ((has_search) ? searchData : gDataAll);
    var page_size = Math.round(zip_data.zips.length / gPageSize);
    //    if (gDataAll.zips.length % gPageSize > 0) {
    //        page_size++;
    //    }
    var str = '';
    //var str = '<li>';
    //str += '<select name="pageSelect" id="pageSelect" data-theme="b" data-overlay-theme="d" data-native-menu="false" data-mini="true" onchange="updateStartIndex()">';
    for (var i = 0; i < page_size; i++) {
        str += '<option value="' + i + '">Page ' + (1 + i) + '</option>';
    }
    //str += '</select>';
    //str += '</li>';
    if ($('#search').length == 0) {
        var search_ctrl = '<br/><input type="search" width="20%" maxlength="25" name="search" id="search" value="" onkeyup="searchZips();" onchange="searchZips();"/>';
        $(search_ctrl).insertAfter('#dvPageNumbers');
    }
    if (page_size == 0)
        $('#dvPageNumbers').css('display', 'none');
    else {
        $('#pageSelect option').remove();
        $(str).appendTo('#pageSelect');
        $('#pageSelect').selectmenu('refresh');
        $('#dvPageNumbers').css('display', 'block');
    }
    $('.ui-page').trigger('create');
}

function updateStartIndex(has_search) {
    var multiplier = parseInt($('#pageSelect').prop("selectedIndex"));
    gDataStartIndex = multiplier * gPageSize;
    var upper_value = (gDataStartIndex + gPageSize <= gDataAll.zips.length) ? gDataStartIndex + gPageSize : gDataAll.zips.length;
    var temp_gdata = [];
    temp_gdata = $.extend(true, {}, ((has_search || (searchData != undefined && searchData != null && searchData.zips != undefined && searchData.zips.length > 0)) ? searchData : gDataAll));
    var zips_in_page = temp_gdata.zips.splice(gDataStartIndex, upper_value - gDataStartIndex);
    temp_gdata.zips = zips_in_page;
    gData = temp_gdata;
    $('#dvLocations').empty();
    buildZips(selectedStoreId);
    $.each(gData.zips, function (key, val) {
        $('#spnTargetedZipCnt' + val.zipCode).html(val.available);
    });
    var avbl_cnt = 0;
    $.each(gDataAll.zips, function (key, val) {
        avbl_cnt = parseInt(avbl_cnt) + parseInt(val.available);
    });
    $('#divAvailable').html(avbl_cnt);
    $('#spnRadiusAvblCount').html(avbl_cnt);
    $('#txtTargeted').val($('#divAvailable').text());
    $('#spnTargeted').html($('#divAvailable').text());
    makeReadOnlyZips();
}

function makeReadOnlyZips() {

    eval(getURLDecode(pagePrefs.scriptBlockDict.makeReadOnlyZips));
    //if (appPrivileges.customerNumber == CW) {
    //    $($('div[class=ui-checkbox]')).each(function (i) {
    //        $(this).addClass('ui-disabled')
    //        $(this).css('opacity', '0.8');
    //    });
    //    $.each($('#dvLocations div'), function (key, val) {
    //        if (val.id != undefined && val.id != null && val.id != "") {
    //            $('#' + val.id + ' h3 a').click(function (event) {
    //                return false;
    //            });
    //            $('#' + val.id + ' h3 a').css('cursor', 'default');
    //            //$('#' + val.id + ' h3 a').removeClass('ui-btn-icon-left');
    //        }
    //    });
    //}
}

function searchZips() {
    var filterd_list = [];
    var search_value = $('#search').val();

    if (search_value != "" && search_value != undefined) {
        var searched_zips = jQuery.grep(gDataAll.zips, function (a, b) {
            return (a.zipCode.toString().indexOf(search_value) > -1 || a.zipName.toLowerCase().indexOf(search_value.toLowerCase()) > -1 || a.available.toString().indexOf(search_value) > -1 || (a.zipCode.toString() + ' - ' + a.zipName.toLowerCase()).indexOf(search_value.toLowerCase()) > -1);
        });
        searchData = $.extend(true, {}, gDataAll);
        searchData.zips = searched_zips;
        makePageSelect(true);
        updateStartIndex(true);
    }
    else {
        searchData = [];
        makePageSelect(false);
        updateStartIndex(false);
    }

}

function getCountsBy(ctrl) {
    $('#chkViewByHouseHold').attr('checked', false).checkboxradio('refresh');
    if ($(ctrl).val() == 'r') {
        $('#trRadius').css('display', 'block');
        $('#trRadius').css('display', '');
        $('#sldrRange').slider('enable');
        $('#sldrRange')[0].disabled = false;
        if ($('#ddlStoreLocations').val() == "") {
            $('#sldrRange').slider('disable');
            $('#sldrRange')[0].disabled = true;
        }

        $('#sldrRange').css('color', 'black');
        $('#spnRadiusTargeted').css('display', 'block');
        $('#txtRadiusTargeted').css('display', 'none');
        $('#spnTextRadiusTargeted').css('display', 'none');
    }
    else if ($(ctrl).val() == 't') {
        $('#trRadius').css('display', 'block');
        $('#trRadius').css('display', '');
        $('#sldrRange').slider('disable');
        $('#sldrRange')[0].disabled = true;
        $('#sldrRange').css('color', 'lightgray');
        $('#txtRadiusTargeted').css('display', 'block');
        $('#spnRadiusTargeted').css('display', 'none');
        $('#spnTextRadiusTargeted').css('display', 'block');
    }
}

//function setCRRTSelection(is_clicked) {
//    if (is_clicked) {
//        $('#chkViewByHouseHold').attr('checked', false).checkboxradio('refresh');
//        $('#dvSelectByRadius').trigger('collapse');
//        if (gData != null && gData.zips != undefined) {
//            var r_crrts = [];
//            var zip_routes = jQuery.grep(gData.zips, function (key, obj) {
//                var zip = key.zipCode;
//                var is_selected = false;
//                var index = 0;
//                $.each(key.crrts, function (i, j) {
//                    if (j.selectType == 'r') {
//                        is_selected = false;
//                        j.isChecked = 0;
//                        j.selectType = '';
//                        j.targeted = 0;
//                        //r_crrts[i] = 'check_' + zip + '_' + index;
//                        $('#' + 'check_' + zip + '_' + index).attr('checked', false).checkboxradio('refresh');
//                        $('#spn_' + zip + '_' + index).text(j.targeted);
//                        $('#spnCheckAll' + zip).text(j.targeted);
//                    }
//                    index++;
//                });
//                key.targeted = 0;
//                //if (!is_selected) {
//                $('#chkEntireZip' + zip).attr('checked', false).checkboxradio('refresh');
//                $('#checkAll' + zip).attr('checked', false).checkboxradio('refresh');
//                //}
//                $('#spn' + zip).text('0 of ' + key.crrts.length + ' Carrier Routes Selected');
//                $('#spnTargetedZipCnt' + key.zipCode).text(key.targeted);
//                $('#spnTargeted').text(0);
//            });
//        }
//        sessionStorage.removeItem('updatedCRRTs');
//        sessionStorage.removeItem('storeListSelection');
//        gSelectedZips = [];
//    }
//}
function openMapPrefs() {
    $('#popupMapPrefs').panel('open', 'optionsHash');
}
function openNavPanel() {
    //    $("#ulNavLinks").empty();
    //    $("#ulNavLinks").append(displayNavLinks()).listview('refresh');
    //    if (jobCustomerNumber == "99997") {
    //        $('#alertmsg').html("Menu locked for the purpose of this demo.");
    //        $('#popupDialog').popup('open');
    //    }
    //    else {
    $('#navLinksPanel').panel('open', 'optionsHash');
    // }
}
function validateForChangesAndSave(next_page_name) {
    var selected_crrts = $('div[id=dvLocations] input[type="checkbox"]:not([id^=checkAll]):not([id^=chkEntireZip]):checked');
    var diff = [];
    if ((selectedCRRTS.length > 0 && selected_crrts.length > 0) || (selectedCRRTS.length > 0 && selected_crrts.length == 0) || (selectedCRRTS.length == 0 && selected_crrts.length == 0) || (selectedCRRTS.length == 0 && selected_crrts.length > 0))
        //diff = getDifferences(selectedCRRTS, selected_crrts);
        diff = getDifferences(selected_crrts, selectedCRRTS);
    else if (selectedCRRTS.length == 0 && selected_crrts.length > 0)
        //diff = getDifferences(selected_crrts, selectedCRRTS);
        diff = getDifferences(selectedCRRTS, selected_crrts);

    if (diff.length == undefined || diff.length == 0) {
        if (sessionStorage.storeListSelection != undefined) {
            var selected_text = $('#hdnSelectedStore').val();
            if (selected_text != "" && (selectedCRRTS.length > 0 || selected_crrts.length > 0)) {
                var message = 'Leaving this screen without saving the Targeted Selections for Store ' + selected_text + ' will cause you to lose all changes. Click "Leave This Screen" to leave this screen without saving or click "Continue With My Selections" to continue and save.';
                $('#confirmNavMsg').html(message);
                $('#cancelNavBut').text('Leave This Screen');
                $('#cancelNavBut').bind('click', function () {
                    window.location.href = next_page_name;
                });
                $('#okNavBut').text('Continue With My Selections');
                $('#okNavBut').bind('click', function () {
                    var lnk_id = next_page_name.substring(next_page_name.lastIndexOf('\/') + 1, next_page_name.lastIndexOf('.')).replace('job', '');
                    if (lnk_id.indexOf('Milestones') > -1) lnk_id = "Milestones";
                    $('#li' + lnk_id).attr('data-theme', 'c');
                    $('#li'+ lnk_id +' a').removeClass('ui-btn-active');
                    $('#popupNavConfirmDialog').popup('close');
                });
                window.setTimeout(function () {
                    $('#popupNavConfirmDialog').popup('open');
                }, 1000);

                return false;
            }
            else {
                return true;
            }
        }
        else {
            return true;
        }
    }
    else {
        var selected_text = $('#hdnSelectedStore').val();
        if (selected_text != "" && (selectedCRRTS.length > 0 || selected_crrts.length > 0)) {
            var message = 'Leaving this screen without saving the Targeted Selections for Store ' + selected_text + ' will cause you to lose all changes. Click "Leave This Screen" to leave this screen without saving or click "Continue With My Selections" to continue and save.';
            $('#confirmNavMsg').html(message);
            $('#cancelNavBut').text('Leave This Screen');
            $('#cancelNavBut').bind('click', function () {
                window.location.href = next_page_name;
            });
            $('#okNavBut').text('Continue With My Selections');
            $('#okNavBut').bind('click', function () {
                var lnk_id = next_page_name.substring(next_page_name.lastIndexOf('\/') + 1, next_page_name.lastIndexOf('.')).replace('job', '');
                if (lnk_id.indexOf('Milestones') > -1) lnk_id = "Milestones";
                $('#li' + lnk_id).attr('data-theme', 'c');
                $('#li' + lnk_id + ' a').removeClass('ui-btn-active');
                $('#popupNavConfirmDialog').popup('close');
            });
            $('#popupNavConfirmDialog').popup('open');
            return false;
        }
        else {
            return true;
        }
    }
}
function confirmNavigationMessage(page) {
    var msg_box = '';
    msg_box = '<div data-role="popup" data-position-to="window" data-transition="pop"  id="popupNavConfirmDialog" data-overlay-theme="d" data-theme="c" style="text-align:left;max-width:600px;" class="ui-corner-all" data-history="false">';
    msg_box += '<div data-role="header" data-theme="d" class="ui-corner-top" id="navdialogbox">';
    msg_box += '<h1>Confirm</h1>';
    msg_box += '</div>';
    msg_box += '<div data-role="content" data-theme="c" class="ui-corner-bottom ui-content">';
    msg_box += '<div id="confirmNavMsg"></div>';
    msg_box += '<div id="colId1"></div>';
    msg_box += '<div id="valList1"></div>';
    msg_box += '<a href="#" data-role="button" data-inline="true" data-theme="a" id="cancelNavBut" data-mini="true">Cancel</a>';
    msg_box += '<a href="#" data-role="button" data-inline="true" data-theme="a" id="okNavBut" data-mini="true">Ok</a>';
    msg_box += '</div></div>';
    $("#" + page).append(msg_box);
}

function validateAndNavigate() {
    $('#btnContinueTop').click();
}

function navLinksClick(path) {
    var a = path;
    window.location.href = path;
}

function closeInitialPopup(popup_ctrl) {
    $('#' + popup_ctrl).popup('close');
    if (jobCustomerNumber == CASEYS) {
        $('#selectionMethod').popup();
        $('#selectionMethod').popup('open', { positionTo: "#dvSelectBy" });
    }
}

//$('#txtRadiusTargeted').on('keyup', function (event) {
//    this.value = this.value.replace(/[^0-9]/g, '');
//});

function allowNumbersOnly(event) {
    event.value = event.value.replace(/[^0-9]+/g, '');
    //var tempVal = $('#txtRadiusTargeted').val();
    //if (/^[0-9]{1,10}$/.test(+tempVal)) // OR if (/^[0-9]{1,10}$/.test(+tempVal) && tempVal.length<=10) 
    //    return true;
    //else
    //    return false;
}
//******************** Public Functions End **************************