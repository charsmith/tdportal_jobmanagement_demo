jQuery.support.cors = true;

//******************** Global Variables Start **************************
//Variables used when page loads initially from email link -- START --

var gServiceUrl = serviceURLDomain + "api/Authorization";
var gEditJobService = serviceURLDomain + "api/JobTicket/";

//Variables used when page loads from email link -- END --

var crocodocViewURL = "https://crocodoc.com/view/";
var gOutputData;
var jobNumber = "";
var facilityId = "";
jobNumber = getSessionData("jobNumber");
facilityId = getSessionData("facilityId");
var urlString = unescape(window.location);
var queryString = (urlString.indexOf("?") > -1) ? urlString.substr(urlString.indexOf("?") + 1, urlString.length) : "";
var params = queryString.split("&");//(queryString.indexOf('&') > -1) ? queryString.split("&") : [];
var artworkFileInfo = [];
var encodedFileName = getURLParameter("fileName");
var proofJobNumber = getURLParameter("jobNumber");
var proofFacilityId = getURLParameter("facilityId");
var crocSession = "";
var fileName = "";
var windowType = "";
var version = "";
var userName = "";
var proofingFileVersionList = [];
var staticProofingFileVersionList = [];
var sk_ApprovalList = {};
var proofBobDetails = {};
var isJobTicketDataUpdated = false;
var encryptedString = (params[0] != undefined && params[0] != "") ? params[0] : "";
if ((window.opener != undefined && window.opener != null && window.opener.length > 0) && sessionStorage.isNewWindow == "true")
    windowType = "blank";
var approvalUserEmail = (sessionStorage.isOpenedFromEmail != undefined && sessionStorage.isOpenedFromEmail != null && sessionStorage.isOpenedFromEmail == "true") ? sessionStorage.approvalUserEmail : "";
var userApprovalLevel = "";
var jobSummaryProofType = "";
var isMarkupClosed = false;
var dontShowHintsAgain = (localStorage.getItem('doNotShowHintsAgain') != undefined && localStorage.getItem('doNotShowHintsAgain') != null && localStorage.getItem('doNotShowHintsAgain') != "" && localStorage.getItem('doNotShowHintsAgain') != "false") ? true : false;

if (appPrivileges != undefined && appPrivileges != null && appPrivileges != "" && appPrivileges.customerNumber == SPORTSKING && (params.length > 1 || params[0] == "")) {
    encryptedString = encryptedString == "" ? "fileName=SportsKing-Baseball.pdf" : encryptedString;
    urlString = "../jobApproval/jobApproval.html?" + encryptedString + "&jobNumber=" + skApprovalStaticJobNumber;
    queryString = (urlString.indexOf("?") > -1) ? urlString.substr(urlString.indexOf("?") + 1, urlString.length) : "";
    params = queryString.split("&");
    encodedFileName = encryptedString.substring(encryptedString.indexOf('=') + 1);//"SportsKing-Baseball.pdf";
    if (sessionStorage.jobSetupOutput != undefined && sessionStorage.jobSetupOutput != null && sessionStorage.jobSetupOutput != "") {
        gOutputData = $.parseJSON(sessionStorage.jobSetupOutput);
        //if (gOutputData.templateName.indexOf('_11x6_') > -1) {
        // encodedFileName = "SportsKing-Baseball.pdf";
        //}
    }
    proofJobNumber = skApprovalStaticJobNumber;
}
else if (appPrivileges != undefined && appPrivileges != null && appPrivileges != "" && appPrivileges.customerNumber == ALLIED) {
    encryptedString = encryptedString == "" ? "fileName=GW_Berkheimer_Jansen.pdf" : encryptedString;
    urlString = "../jobApproval/jobApproval.html?" + encryptedString + "&jobNumber=-16739";
    queryString = (urlString.indexOf("?") > -1) ? urlString.substr(urlString.indexOf("?") + 1, urlString.length) : "";
    params = queryString.split("&");
    encodedFileName = encryptedString.substring(encryptedString.indexOf('=') + 1);//"SportsKing-Baseball.pdf";
    if (sessionStorage.jobSetupOutput != undefined && sessionStorage.jobSetupOutput != null && sessionStorage.jobSetupOutput != "") {
        gOutputData = $.parseJSON(sessionStorage.jobSetupOutput);
        //if (gOutputData.templateName.indexOf('_11x6_') > -1) {
        // encodedFileName = "SportsKing-Baseball.pdf";
        //}
    }
    proofJobNumber = "-16739";
}
else if (appPrivileges != undefined && appPrivileges != null && appPrivileges != "" && appPrivileges.customerNumber == KUBOTA && appPrivileges.roleName == "user" && (params.length > 1 || params[0] == "")) {
    encryptedString = encryptedString == "" ? "fileName=AG Special Invite.pdf" : encryptedString;
    urlString = "../jobApproval/jobApproval.html?" + encryptedString + "&jobNumber=" + kubotaUserApprovalStaticJobNumber;
    queryString = (urlString.indexOf("?") > -1) ? urlString.substr(urlString.indexOf("?") + 1, urlString.length) : "";
    params = queryString.split("&");
    encodedFileName = encryptedString.substring(encryptedString.indexOf('=') + 1);//"SportsKing-Baseball.pdf";
    if (sessionStorage.jobSetupOutput != undefined && sessionStorage.jobSetupOutput != null && sessionStorage.jobSetupOutput != "") {
        gOutputData = $.parseJSON(sessionStorage.jobSetupOutput);

    }
    proofJobNumber = kubotaUserApprovalStaticJobNumber;
}
else if (appPrivileges != undefined && appPrivileges != null && appPrivileges != "" && appPrivileges.customerNumber == ICYNENE && (params.length > 1 || params[0] == "")) {
    encryptedString = encryptedString == "" ? "fileName=Closed Cell Product Portfolio - 2017 - USA - Print.pdf" : encryptedString;
    urlString = "../jobApproval/jobApproval.html?" + encryptedString + "&jobNumber=" + icyneneApprovalStaticJobNumber;
    queryString = (urlString.indexOf("?") > -1) ? urlString.substr(urlString.indexOf("?") + 1, urlString.length) : "";
    params = queryString.split("&");
    encodedFileName = encryptedString.substring(encryptedString.indexOf('=') + 1);//"SportsKing-Baseball.pdf";
    if (sessionStorage.jobSetupOutput != undefined && sessionStorage.jobSetupOutput != null && sessionStorage.jobSetupOutput != "") {
        gOutputData = $.parseJSON(sessionStorage.jobSetupOutput);

    }
    proofJobNumber = icyneneApprovalStaticJobNumber;
}

//******************** Global Variables End **************************

//******************** Page Load Events Start **************************
$(window).on('popupbeforeposition', 'div:jqmData(role="popup")', function () {
    var notDismissible = $(this).jqmData('dismissible') === false;
    if (notDismissible) {
        $('.ui-popup-screen').off();
    }
});

if (params[0] != undefined && params[0] != "" && params.length == 1) {
    getProofDetails();
}

$.getJSON('JSON/_approveList.JSON', function (data) {
    sk_ApprovalList = data;
});

$('#_jobApproval').live('pagebeforecreate', function () {
    displayMessage('_jobApproval');
    loadingImg('_jobApproval');
    createConfirmMessage('_jobApproval');
    createConfirmDialog();
    if ((params.length == 1 && params[0] != "") || (sessionStorage.isOpenedFromEmail != undefined && sessionStorage.isOpenedFromEmail != null && sessionStorage.isOpenedFromEmail == "true")) {
        $('a[title = "Navigation"]').hide();
        if (showArtApproval()) {
            $('#dvArtApproval').css('display', 'block');
            $('#dvArtApproval').css('display', '');
        }
    }
    else {
        $('#dvArtApproval').css('display', 'none');
        displayNavLinks();
    }

    $("#navLinksPanel").on("panelopen", function () {
        sessionStorage.isNavPanelOpen = true;
    });
    if (sessionStorage.userRole == "proofUser")
        $('#btnProofForward').hide(); //hides Forward button if the user is proof user.
    var image_path = "../images/gizmo_load_anim.gif";
    var alter_text = (isNewProofGenerationCustomers()) ? "Generating and processing your proof. May take a minute or two. Please be patient." : "Loading....";
    var over = '<br/><br/><br/><br/><br/><br/><br/><br/><center style="top:800px"><span id="spnWaitImage" style="width:500px;height:200px;"><img id="loading2" alt="Loading...." src="' + image_path + '" /><p><span id="loadingText2" style="font-weight:bold;color:#4a80af;font-family:Tahoma,verdana, Arial;font-size:large;">' + alter_text + '</span></p></span></center>';
    $('#viewFrame')[0].contentWindow.document.body.innerHTML = over;
    setDemoHintsSliderValue();
    createDemoHints("jobApproval");
    if (appPrivileges != undefined && appPrivileges != null && appPrivileges != "") {
        loadFooterInfo();
    }
    isJobTicketDataUpdated = false;
    if (jobTicketVerificationRequired()) {
        isJobTicketDataUpdated = (sessionStorage.isJobTicketUpdate != undefined && sessionStorage.isJobTicketUpdate != null && sessionStorage.isJobTicketUpdate != "" && JSON.parse(sessionStorage.isJobTicketUpdate)) ? true : false;
    }
    if (sessionStorage.proofType) {
        jobSummaryProofType = sessionStorage.proofType;
        sessionStorage.removeItem("proofType");
    }
});

$(document).bind('pageshow', '#_jobApproval', function (event) {
    //persistNavPanelState();
    /*if (((sessionStorage.isDemoSelected != undefined && sessionStorage.isDemoSelected != null && sessionStorage.isDemoSelected == "1") || (sessionStorage.username.toLowerCase().indexOf('demo') > -1)) &&
                (sessionStorage.isDemoInsDisplayed == undefined || sessionStorage.isDemoInsDisplayed == null || sessionStorage.isDemoInsDisplayed == "" || sessionStorage.isDemoInsDisplayed == "false")) {
        sessionStorage.isDemoInsDisplayed = true;
        //$('#btnDemoInstructions').trigger('click');
    }*/
    if ([SCOTTS, MILWAUKEE, ANDERSEN, ACE].indexOf(jobCustomerNumber) > -1) {
        $('#pClosingText').text("This proof is only available for viewing. Please contact your account manager if you have questions.");
        gOutputData = (sessionStorage.jobSetupOutput != undefined && sessionStorage.jobSetupOutput != null) ? jQuery.parseJSON(sessionStorage.jobSetupOutput) : [];
        var notify_user_for_new_proof_generation = false;
        if (isGmcDataUpdate(gOutputData.isGmcDataUpdated)) {
            if (!isJobTicketDataUpdated) {
                notify_user_for_new_proof_generation = true;
            }
            isJobTicketDataUpdated = true;
        }
        if ((params.length > 1) || (params.length == 1 && params[0] == "")) {
            if (!isOrderLocationExists('_jobApproval'))
                return false;
            else if ([SCOTTS, ACE].indexOf(jobCustomerNumber) > -1 && [97, ACE_CustomUpload_JobType].indexOf(gOutputData.jobTypeId) == -1 && (!isVariableFieldListValid('_jobApproval') || !isOfferListValid('_jobApproval'))) {
                return false;
            }
            else if ([ANDERSEN].indexOf(jobCustomerNumber) > -1 && [ANDERSEN_CustomUpload_JobType].indexOf(gOutputData.jobTypeId) == -1 && (!isVariableFieldListValid('_jobApproval') || !isSelectImagesValid('_jobApproval'))) {
                return false;
            }
            else if ([MILWAUKEE].indexOf(jobCustomerNumber) > -1 && !isVariableFieldListValid('_jobApproval')) {
                return false;
            }
                //else if (((params.length > 1) || (params.length == 1 && params[0] == "")) && gOutputData.jobTypeId != 97 && gOutputData.jobTypeId != ACE_CustomUpload_JobType && !isOfferListValid('_jobApproval')) {
                //    return false;
                //}
            else if ([97, ACE_CustomUpload_JobType, ANDERSEN_CustomUpload_JobType].indexOf(gOutputData.jobTypeId) == -1 && sessionStorage.variableChangesUnSaved != undefined && sessionStorage.variableChangesUnSaved != null && sessionStorage.variableChangesUnSaved != "" && JSON.parse(sessionStorage.variableChangesUnSaved) /*&& JSON.parse(sessionStorage.variableChangesUnSaved) != false*/) {
                $('#confirmMsg').html('Before we show you a proof, please save your order.');
                $('#okButConfirm').text('Save');
                $('#cancelButConfirm').css('display', 'none');
                $('#cancelButConfirm').bind('click', function () {
                    $('#cancelButConfirm').unbind('click');
                    $('#okButConfirm').unbind('click');
                    $('#cancelButConfirm').attr('onclick', '$(\'#popupConfirmDialog\').popup(\'close\');');
                    sessionStorage.variableChangesUnSaved = true;
                    isJobTicketDataUpdated = false;
                    if ((sessionStorage.isGmcFired != undefined && sessionStorage.isGmcFired != null && sessionStorage.isGmcFired != "" && JSON.parse(sessionStorage.isGmcFired)) ||
                            (gOutputData.artworkAction != undefined && gOutputData.artworkAction != null && gOutputData.artworkAction.artworkFileList != undefined && gOutputData.artworkAction.artworkFileList != null && Object.keys(gOutputData.artworkAction.artworkFileList).length > 0)) {
                        loadApprovalPage(event);
                    }
                    else {
                        var alter_text = "Failed to display your proof.";
                        var over = '<br/><br/><br/><br/><br/><br/><br/><br/><center style="top:800px"><span id="spnWaitImage" style="width:500px;height:200px;"><img id="loading2" alt="Loading...." src="' + image_path + '" /><p><span id="loadingText2" style="font-weight:bold;color:#4a80af;font-family:Tahoma,verdana, Arial;font-size:large;">' + alter_text + '</span></p></span></center>';
                        $('#viewFrame')[0].contentWindow.document.body.innerHTML = over;
                    }
                    $('#popupConfirmDialog').popup('close');
                });
                $('#okButConfirm').bind('click', function () {
                    $('#cancelButConfirm').unbind('click');
                    $('#okButConfirm').unbind('click');
                    $('#cancelButConfirm').css('display', 'block');
                    $('#cancelButConfirm').attr('onclick', '$(\'#popupConfirmDialog\').popup(\'close\');');
                    $('#popupConfirmDialog').popup('close');
                    sessionStorage.variableChangesUnSaved = false;
                    $('#btnSaveTop').trigger('click');
                    isJobTicketDataUpdated = true;
                    loadApprovalPage(event);
                });
                $('#popupConfirmDialog').popup('open');
                return false;
            }
            else if (notify_user_for_new_proof_generation) {
                $('#confirmMsg').html('Document has been updated for new proof generation. Please save the document in order to generate new proof.');
                $('#okButConfirm').text('Save');
                $('#cancelButConfirm').css('display', 'none');
                $('#cancelButConfirm').bind('click', function () {
                    $('#cancelButConfirm').unbind('click');
                    $('#okButConfirm').unbind('click');
                    $('#cancelButConfirm').attr('onclick', '$(\'#popupConfirmDialog\').popup(\'close\');');                   
                    $('#popupConfirmDialog').popup('close');
                });
                $('#okButConfirm').bind('click', function () {
                    $('#cancelButConfirm').unbind('click');
                    $('#okButConfirm').unbind('click');
                    $('#cancelButConfirm').css('display', 'block');
                    $('#cancelButConfirm').attr('onclick', '$(\'#popupConfirmDialog\').popup(\'close\');');
                    $('#popupConfirmDialog').popup('close');                  
                    loadApprovalPage(event);
                });
                $('#popupConfirmDialog').popup('open');
                return false;
            }
            else
                loadApprovalPage(event);
        }
        else
            loadApprovalPage(event);
    }
    else
        loadApprovalPage(event);
});

function loadApprovalPage(event) {
    window.setTimeout(function () {
        if (sessionStorage.isFirstTime == undefined || sessionStorage.isFirstTime == null)
            sessionStorage.isFirstTime = true;
        gOutputData = (sessionStorage.jobSetupOutput != undefined && sessionStorage.jobSetupOutput != null) ? jQuery.parseJSON(sessionStorage.jobSetupOutput) : [];
        if (gOutputData != undefined && gOutputData != null && Object.keys(gOutputData).length > 0) {//&& (params.length >= 1)

            getBubbleCounts(gOutputData);

            var artwork_file_list = getArtworkFileList();
            if (Object.keys(artwork_file_list).length > 0) {
                if ((approvalUserEmail == undefined || approvalUserEmail == null || approvalUserEmail == "") && windowType != "blank") {
                    if (urlString.indexOf('?fileName') == -1) {
                        $.each(artwork_file_list, function (key) {
                            urlString = unescape(window.location) + '?fileName=' + key + '&jobNumber=' + jobNumber;
                            queryString = urlString.substr(urlString.indexOf("?") + 1, urlString.length);
                            params = queryString.split("&");
                            encodedFileName = key;
                            proofJobNumber = jobNumber;
                            proofFacilityId = facilityId;
                            return false;
                        });
                    }
                    loadProofsToApprove(artwork_file_list);
                }
                if (jobCustomerNumber == DCA || jobCustomerNumber == SPORTSKING)
                    $('#btnSubmitForExecution').css('display', 'none');

            }
            else {
                if ((approvalUserEmail == undefined || approvalUserEmail == null || approvalUserEmail == "") && windowType != "blank" && gOutputData.artworkAction != undefined && gOutputData.artworkAction != null && gOutputData.artworkAction.artworkFileList != undefined && gOutputData.artworkAction.artworkFileList != null && Object.keys(gOutputData.artworkAction.artworkFileList).length > 0) {
                    if (urlString.indexOf('?fileName') == -1) {
                        $.each(gOutputData.artworkAction.artworkFileList, function (key) {
                            urlString = unescape(window.location) + '?fileName=' + key + '&jobNumber=' + jobNumber;
                            queryString = urlString.substr(urlString.indexOf("?") + 1, urlString.length);
                            params = queryString.split("&");
                            encodedFileName = key;
                            proofJobNumber = jobNumber;
                            proofFacilityId = facilityId;
                            return false;
                        });
                    }
                }
                else if (sessionStorage.isOpenedFromEmail == undefined && gOutputData.artworkAction != undefined && gOutputData.artworkAction != null && gOutputData.artworkAction.artworkFileList != undefined && gOutputData.artworkAction.artworkFileList != null && Object.keys(gOutputData.artworkAction.artworkFileList).length == 0 && (isRequiredToShowNoArtworksAvailable())) {
                    $('#dvProofButtons').hide();
                    $('#alertmsg').text("No artworks available to display proof.");
                    $('#viewFrame')[0].contentWindow.document.body.innerHTML = "";
                    $('#popupDialog').popup('open');
                    return false;
                }
                if (jobCustomerNumber == DCA || jobCustomerNumber == SPORTSKING)
                    $('#btnSubmitForExecution').css('display', 'none');

            }
        }
        if (!checkVersion()) {
            event.preventDefault();
            $('#dvProofButtons').hide();
            $('#btnContinue').hide();
            $('#viewFrame').css('display', 'none');
            $('#dvProofFileName').css('display', 'none');
            var frame_height = (window.screen.availHeight > 860) ? window.screen.availHeight - 154 : '100%';
            $('#dvBrowserInfo').css('display', 'block');
            $('#dvBrowserInfo').css('height', frame_height);
            $('#dvBrowserInfo').css('width', '100%');
            $('#dvBrowserInfo').css('position', 'absolute');
            $('#dvBrowserInfo').css('top', '100px');
            $('#dvBrowserInfo').css('text-align', 'center');
            $('#alertmsg').text("This proofing system does not support Internet Explorer version 8 or older versions. Please use a current version of Internet Explorer, Chrome, Firefox or Safari.");
            $('#popupDialog').popup('open');
        }
        else {
            $('#dvEmailLinkFor').css('display', 'none');
            if (!hideForwardProof()) {
                //$('#btnProofForward').css('display', 'block');
                $('#btnProofForward').css('display', 'none');
            }
            if (params.length == 1 && (params[0] != undefined && params[0] != "")) {
                sessionStorage.isOpenedFromEmail = true;
                $('#btnContinue').css('display', 'none');
                $('#btnProofForward').css('display', 'none');
                authorizeAndGetProof();
            }
            else {
                if (windowType != "blank") {
                    gOutputData = (sessionStorage.jobSetupOutput != undefined && sessionStorage.jobSetupOutput != null) ? jQuery.parseJSON(sessionStorage.jobSetupOutput) : [];
                    if (isJobTicketDataUpdated || (isNewProofGenerationCustomers() && gOutputData != undefined && gOutputData != null && Object.keys(gOutputData).length > 0 && gOutputData.artworkAction != undefined && gOutputData.artworkAction != null && gOutputData.artworkAction.artworkFileList != undefined && gOutputData.artworkAction.artworkFileList != null && Object.keys(gOutputData.artworkAction.artworkFileList).length == 0)) {
                        getGmcAuthorizedProof();
                    }
                    else {
                        getAuthorizedProof();
                    }
                }
                loadFooterInfo();
            }

            $('.ui-panel-content-wrap').removeClass('ui-panel-content-wrap');
            if (windowType == "blank") {
                var selected_version = (sessionStorage.selectedVersion != undefined && sessionStorage.selectedVersion != null && sessionStorage.selectedVersion != "") ? jQuery.parseJSON(sessionStorage.selectedVersion) : [];
                if (selected_version.length > 0) {
                    if ((sessionStorage.isOpenedFromEmail != undefined && sessionStorage.isOpenedFromEmail != null && sessionStorage.isOpenedFromEmail != "" && sessionStorage.isOpenedFromEmail == "true") || (sessionStorage.approvalUserEmail != undefined && sessionStorage.approvalUserEmail != null && sessionStorage.approvalUserEmail != ""))
                        approvalUserEmail = sessionStorage.approvalUserEmail;
                    jobNumber = selected_version[0].jobNumber;
                    facilityId = selected_version[0].facilityId;
                    fileName = selected_version[0].fileName;
                    $('#artApproval').css('display', 'block');
                    if (selected_version[0].index > 0)
                        $('#artApproval').css('display', 'none');
                    if (selected_version[0].proofUUID != "")
                        viewProof(selected_version[0].fileName, selected_version[0].proofCreatedDate, selected_version[0].proofUUID, selected_version[0].facilityId, selected_version[0].jobNumber, 'blank', selected_version[0].index);
                    else {

                        getAuthorizedProof('job_proofs');
                    }
                }
                $('#divBottom').css("display", "none");
                $('#btnSaveTop').css('display', 'none');
                $('#btnContinueTop').css('display', 'none');
            }
            if (!hideForwardProof()) {
                //$('#btnProofForward').css('display', 'block');
                $('#btnProofForward').css('display', 'none');
            }
            if (sessionStorage.isOpenedFromEmail == "true") {
                $('#btnContinue').css('display', 'none');
                $('#btnProofForward').css('display', 'none');
                $('#btnSaveTop').css('display', 'none');
                $('#btnContinueTop').css('display', 'none');
            }
            /*window.setTimeout(function() {
                $('#footer').find('a').remove();
            }, 200);*/
        }
        $.each($('#dvProofButtons'), function (a, b) {
            $(b).controlgroup();
        });

        if ((sessionStorage.isOpenedFromEmail == undefined || sessionStorage.isOpenedFromEmail == null || sessionStorage.isOpenedFromEmail == "" || sessionStorage.isOpenedFromEmail == "false") && !dontShowHintsAgain && (showApprovalDemoHint()) && (sessionStorage.showDemoHints == undefined || sessionStorage.showDemoHints == null || sessionStorage.showDemoHints == "" || sessionStorage.showDemoHints == "on") && (sessionStorage.isApprovalDemoHintsDisplayed == undefined || sessionStorage.isApprovalDemoHintsDisplayed == null || sessionStorage.isApprovalDemoHintsDisplayed == "false")) {
            sessionStorage.isApprovalDemoHintsDisplayed = true;
            if (isNewProofGenerationCustomers()) {
                $('#approvalDemoHint').popup('close');
                $('#approvalDemoHint fieldset[data-role=controlgroup]').css('display', 'block');
                window.setTimeout(function () {
                    $('#approvalDemoHint').popup('open', { positionTo: '#dvProofFileName left' });
                }, 1000);
            }
            else {
                $('#markupAnnotations').popup('close');
                window.setTimeout(function () {
                    $('#markupAnnotations').popup('open', { positionTo: '#dvProofFileName left' });
                }, 1000);
            }
        }

        $("#markupAnnotations").bind({
            popupafterclose: function () {
                if (sessionStorage.showDemoHints != "off")
                    $('#viewMultipleProofs').popup('open', { positionTo: '#dvProofFileName' });
            }
        });

        $("#viewMultipleProofs").bind({
            popupafterclose: function () {
                if (sessionStorage.showDemoHints != "off")
                    $('#approvalDemoHint').popup('open', { positionTo: '#dvProofFileName' });
            }
        });

        if (showApproversProofStatusDemoHint()) {
            $("#approvalDemoHint").bind({
                popupafterclose: function () {
                    if (sessionStorage.showDemoHints != "off")
                        $('#approversProofStatus').popup('open', { positionTo: '#dvProofFileName' });
                }
            });
            $("#approversProofStatus").bind({
                popupafterclose: function () {
                    if (sessionStorage.showDemoHints != "off")
                        $('#historyDemoHint').popup('open', { positionTo: '#dvProofFileName' });
                }
            });
        }
        else {
            $("#approvalDemoHint").bind({
                popupafterclose: function () {
                    if (sessionStorage.showDemoHints != "off")
                        $('#forwardProofDemoHint').popup('open', { positionTo: '#dvProofFileName' });
                }
            });

            $("#forwardProofDemoHint").bind({
                popupafterclose: function () {
                    if (sessionStorage.showDemoHints != "off")
                        $('#approversProofStatus').popup('open', { positionTo: '#dvProofFileName' });
                }
            });

            $("#approversProofStatus").bind({
                popupafterclose: function () {
                    if (sessionStorage.showDemoHints != "off")
                        $('#historyDemoHint').popup('open', { positionTo: '#dvProofFileName' });
                }
            });
        }
    }, 1500);
    if (jobCustomerNumber == MILWAUKEE) {
        $('#sldrShowHints').parent().css('disabled', 'disabled');
        $('#sldrShowHints').parent()[0].disabled = true;
        $('#sldrShowHints').parent().addClass('ui-disabled');
    }
}

function loadFooterInfo() {
    window.setTimeout(function () {
        switch (appPrivileges.customerNumber) {
            case KUBOTA:
            case GWA:
                $('#spnCustomerName').html("Gregory Welteroth Advertising");
                break;
                //case GWA:
                //    $('#spnCustomerName').html("Gregory Welteroth Advertising");
                //    break;
            case DCA:
                $('#spnCustomerName').html("MDG Advertising");
                break;
            default:
                break;
        }
        updateFooterInfo(appPrivileges.customerNumber);
    }, 1000);

    if (appPrivileges != undefined && appPrivileges != null && appPrivileges != "" && appPrivileges.roleName == "admin") {
        $('#chkApproveJobArtwork').parent().show();
    }
    else {
        $('#chkApproveJobArtwork').parent().hide();
    }
}

function previewMailing() {
    //Need to update pdf Kubato customer
    if (appPrivileges.customerNumber == DCA || appPrivileges.customerNumber == KUBOTA || appPrivileges.customerNumber == SPORTSKING || appPrivileges.customerNumber == GWA) {
        encodedFileName = "DCA%2041941%20Jupiter%20Dental%20_Postcard_Dr.%20Vera_MECH.pdf";
    }
    if (appPrivileges.customerNumber == AAG) {
        encodedFileName = "TCA_10232_AAG_Lunch_Learn_2_Presenters-2_Events_Lenfers_Culbreth_Let.pdf";
    }
    else if (appPrivileges.customerNumber == ALLIED) {
        encodedFileName = "GW_Berkheimer_Jansen.pdf";
    }
    var preview_proof = "../jobApproval/previewMailing.html?fileName=" + encodedFileName + "&jobNumber=" + sessionStorage.jobNumber;
    window.location.href = preview_proof;
}

function authorizeAndGetProof() {
    var post_data = {};
    post_data["encryptedString"] = "";
    post_data.encryptedString = queryString;
    if (proofBobDetails != undefined && proofBobDetails != null && proofBobDetails != "") {
        loginAuthorization(proofBobDetails);
        $('#btnContinue').attr('onclick', 'continueToOrder()');
        $('#btnContinueTop').attr('onclick', 'continueToOrder()');
        $('#btnContinue').css('display', 'none');
        $('#btnProofForward').css('display', 'none');
        window.setTimeout(function () {
            if (proofBobDetails.isPowerUser == 1) {
                var job_customer_number = (sessionStorage.jobCustomerNumber != undefined && sessionStorage.jobCustomerNumber != null && sessionStorage.jobCustomerNumber != "") ? sessionStorage.jobCustomerNumber : appPrivileges.customerNumber;
                if ((job_customer_number != BBB && job_customer_number != AAG && job_customer_number != ALLIED) || ((job_customer_number == BBB || job_customer_number == AAG || job_customer_number == ALLIED) && appPrivileges.roleName == "admin")) {
                    $('#btnContinue').css('display', 'block');
                    if (!hideForwardProof()) {
                        //$('#btnProofForward').css('display', 'block');
                        $('#btnProofForward').css('display', 'none');
                    }
                }
                else {
                    $('#btnContinue').css('display', 'none');
                    $('#btnProofForward').css('display', 'none');
                    $('#demoInstructions1').addClass('ui-first-child ui-last-child');
                }
                $.each($('#dvProofButtons'), function (a, b) {
                    $(b).controlgroup();
                });
            }
        }, 2000);
    }
}

function getProofDetails() {
    var post_data = {};
    post_data["encryptedString"] = "";
    post_data.encryptedString = queryString;
    postCORS(serviceURLDomain + "api/ProofV2/bob", JSON.stringify(post_data), function (response) {
        if (response != undefined && response != null && response != "") {
            proofBobDetails = response;
            if (response.customerNumber) {
                jobCustomerNumber = response.customerNumber;
                customStyles();
            }
        }

    }, function (response_error) {
        $('#viewFrame')[0].contentWindow.document.body.innerHTML = "";
        showErrorResponseText(response_error, false);
    });
}

function getGmcAuthorizedProof() {
    var job_customer_number = (sessionStorage.jobCustomerNumber != undefined && sessionStorage.jobCustomerNumber != null && sessionStorage.jobCustomerNumber != "") ? sessionStorage.jobCustomerNumber : appPrivileges.customerNumber;
    var gmc_flag = (sessionStorage.isGmcFired == undefined || sessionStorage.isGmcFired == null || sessionStorage.isGmcFired == "" || !JSON.parse(sessionStorage.isGmcFired) || isJobTicketDataUpdated) ? "1" : "0";
    var gmc_service_url = serviceURLDomain + "api/Gmc/" + sessionStorage.facilityId + "/" + job_customer_number + "/" + sessionStorage.jobNumber + "/" + gmc_flag;
    getCORS(gmc_service_url, null, function () {
        encodedFileName = "proof_" + sessionStorage.jobNumber + ".pdf";
        fileName = encodedFileName;
        proofJobNumber = sessionStorage.jobNumber;
        getCrocdocProofing(encodedFileName, '');
        sessionStorage.isGmcFired = true;
        sessionStorage.isJobTicketUpdate = false;
        if (gOutputData.isGmcDataUpdated) {
            gOutputData.isGmcDataUpdated = false;
            sessionStorage.jobSetupOutput = JSON.stringify(gOutputData);
            forceSaveJobTicket();
        }
        else {
            sessionStorage.jobSetupOutput = JSON.stringify(gOutputData);
        }
        
    }, function () {
        var alter_text = "Failed to generate the proof. Please contact administrator.";
        var over = '<br/><br/><br/><br/><br/><br/><br/><br/><center style="top:800px"><p><span id="loadingText2" style="font-weight:bold;color:#4a80af;font-family:Tahoma,verdana, Arial;font-size:large;">' + alter_text + '</span></p></span></center>';
        $('#viewFrame')[0].contentWindow.document.body.innerHTML = over;
    });
}

function getAuthorizedProof(window_origin) {
    if (queryString.indexOf('&') > -1) {
        if (queryString.split("=")[1].indexOf('&') > -1)
            fileName = queryString.split("=")[1].substring(0, queryString.split("=")[1].indexOf('&'));
        else
            fileName = queryString.split("=")[1];
    }
    if (params.length > 2)
        version = params[2].substring(params[2].indexOf('=') + 1, params[2].length);

    getCrocdocProofing(encodedFileName, window_origin);

    $('#btnContinue').attr('onclick', 'continueToOrder()');
    $('#btnContinueTop').attr('onclick', 'continueToOrder()');
    if (windowType != "blank")
        displayJobInfo();

    if (sessionStorage.jobSetupOutput != undefined) {
        gOutputData = jQuery.parseJSON(sessionStorage.jobSetupOutput);
        var artwork_file_list = getArtworkFileList();
        if (Object.keys(artwork_file_list).length > 0) {
            artworkFileInfo[0] = (artwork_file_list[fileName] != undefined && artwork_file_list[fileName] != null && Object.keys(artwork_file_list[fileName]).length > 0) ? artwork_file_list[fileName] : {};
            //if (sessionStorage.userRole == "admin") {
            //    $('#btnOverrideApprovals').css('display', 'block');
            //    $('#btnOverrideApprovals').css('display', '');
            //}
            //else {
            //    $('#btnOverrideApprovals').css('display', 'none');
            //}
            if ([97, ACE_CustomUpload_JobType, ANDERSEN_CustomUpload_JobType].indexOf(gOutputData.jobTypeId) > -1)
                $('#btnJobProofs').show();
            if (Object.keys(artwork_file_list).length == 1) {
                if (sessionStorage.customerNumber == SPORTSKING && sessionStorage.userRole != "admin")
                    $('#btnJobProofs').show();
                else if (sessionStorage.customerNumber == CW) {
                    $('#btnJobProofs').show();
                }
                else
                    $('#btnJobProofs').hide();

            } else if (Object.keys(artwork_file_list).length > 1) {
                $('#btnJobProofs').show();
            }
            $.each($('#dvProofButtons'), function (a, b) {
                $(b).controlgroup();
            });
            artworkFileInfo = (artworkFileInfo[0] != undefined && artworkFileInfo[0] != null) ? artworkFileInfo[0] : [];
        }
        else {
            if (gOutputData.artworkAction != undefined && gOutputData.artworkAction != null) {
                if (Object.keys(gOutputData.artworkAction.artworkFileList).length > 0) {
                    artworkFileInfo[0] = (gOutputData.artworkAction.artworkFileList[fileName] != undefined && gOutputData.artworkAction.artworkFileList[fileName] != null && Object.keys(gOutputData.artworkAction.artworkFileList[fileName]).length > 0) ? gOutputData.artworkAction.artworkFileList[fileName] : {};
                }
                if ([97, ACE_CustomUpload_JobType, ANDERSEN_CustomUpload_JobType].indexOf(gOutputData.jobTypeId) > -1)
                    $('#btnJobProofs').show();
                if (Object.keys(gOutputData.artworkAction.artworkFileList).length > 1) {
                    if (sessionStorage.customerNumber == SPORTSKING && sessionStorage.userRole != "admin")
                        $('#btnJobProofs').show();
                    else if (sessionStorage.customerNumber == CW) {
                        $('#btnJobProofs').show();
                    }
                    else
                        $('#btnJobProofs').hide();
                    $.each($('#dvProofButtons'), function (a, b) {
                        $(b).controlgroup();
                    });
                }
                artworkFileInfo = (artworkFileInfo[0] != undefined && artworkFileInfo[0] != null) ? artworkFileInfo[0] : [];
            }
            if ([97, ACE_CustomUpload_JobType, ANDERSEN_CustomUpload_JobType].indexOf(gOutputData.jobTypeId) == -1 && hideJobProofs())
                $('#btnJobProofs').hide();
        }
    }
}

function getCrocdocProofing(encoded_file_name, window_origin) {
    var job_customer_number = (sessionStorage.jobCustomerNumber != undefined && sessionStorage.jobCustomerNumber != null && sessionStorage.jobCustomerNumber != "") ? sessionStorage.jobCustomerNumber : appPrivileges.customerNumber;
    if (sessionStorage.isOpenedFromEmail == "true")
        job_customer_number = sessionStorage.customerNumber;
    var user_email_id = (sessionStorage.approvalUserEmail != undefined && sessionStorage.approvalUserEmail != null && sessionStorage.approvalUserEmail != "") ? sessionStorage.approvalUserEmail : "null";
    
    var temp_job_number = proofJobNumber;
    if (sessionStorage.isOpenedFromEmail == "true" && sessionStorage.customerNumber == SPORTSKING) {
        temp_job_number = skApprovalStaticJobNumber;
    }
    else if (sessionStorage.isOpenedFromEmail == "true" && sessionStorage.customerNumber == KUBOTA && sessionStorage.userRole == "user") {
        temp_job_number = kubotaUserApprovalStaticJobNumber;
    }
        
    else if (sessionStorage.customerNumber == ICYNENE) {
        temp_job_number = icyneneApprovalStaticJobNumber;
    }
    getCORS(serviceURLDomain + "api/ProofV3/" + facilityId + "/" + job_customer_number + "/" + temp_job_number + "/" + encoded_file_name + "/" + user_email_id, null, function (data) {
        proofBobDetails = data;
        loadJobProofsList();
        loadProofsInfo(data, window_origin);
    }, function (error_response) {
        $('#viewFrame')[0].contentWindow.document.body.innerHTML = "";
        showErrorResponseText(error_response, true);
    });
}

function loadProofsInfo(data, window_origin) {
    var version_list = [];
    if (data != undefined && data != null && data != "") {
        if (sessionStorage.isOpenedFromEmail != undefined && sessionStorage.isOpenedFromEmail != null && sessionStorage.isOpenedFromEmail != "" && sessionStorage.isOpenedFromEmail == "true" && params.length == 1) {
            jobNumber = data.jobNumber;
            facilityId = data.facitlityId;
            fileName = data.origProofName;
        }
        var visited_proofs = [];
        visited_proofs = (sessionStorage.visitedProofs != undefined && sessionStorage.visitedProofs != null) ? $.parseJSON(sessionStorage.visitedProofs) : [];
        approvalUserEmail = (data.email != undefined && data.email != null) ? data.email : "";
        if (sessionStorage.isOpenedFromEmail != undefined && sessionStorage.isOpenedFromEmail != null && sessionStorage.isOpenedFromEmail != "" && sessionStorage.isOpenedFromEmail == "true" && sessionStorage.approvalUserEmail != undefined && sessionStorage.approvalUserEmail != null && sessionStorage.approvalUserEmail != "")
            approvalUserEmail = sessionStorage.approvalUserEmail;
        if ((sessionStorage.isOpenedFromEmail != undefined && sessionStorage.isOpenedFromEmail != null && sessionStorage.isOpenedFromEmail != "" && sessionStorage.isOpenedFromEmail == "true") && (data.proofingFileVersionList == undefined || data.proofingFileVersionList == null || data.proofingFileVersionList.length == 0) && (staticProofingFileVersionList != undefined && staticProofingFileVersionList != null && staticProofingFileVersionList.length > 0)) {
            data.proofingFileVersionList = staticProofingFileVersionList;
        }
        var selected_file_info = {};//data.tblOrderFileses[0];
        var order_files_list_cnt = (data.myOrder.listOrderFileses != undefined && data.myOrder.listOrderFileses != null && data.myOrder.listOrderFileses.length > 0) ? data.myOrder.listOrderFileses.length : 0;
        if (order_files_list_cnt > 0) {
            var temp = $.extend(true, {}, data);
            $.each(temp.myOrder.listOrderFileses, function (key, val) {
                if (val.listOrderFileVersionses != undefined && val.listOrderFileVersionses != null && val.listOrderFileVersionses.length > 0) {
                    $.each(val.listOrderFileVersionses.reverse(), function (key1, val1) {
                        if (val1.origFileName == fileName) {
                            selected_file_info = val;
                            return false;
                        }
                    });
                    if (selected_file_info != undefined && selected_file_info != null && Object.keys(selected_file_info).length > 0)
                        return false;
                }

            });
        }

        if (order_files_list_cnt > 0 && selected_file_info.listOrderFileVersionses != undefined && selected_file_info.listOrderFileVersionses != null && selected_file_info.listOrderFileVersionses.length > 0) {
            var current_view_proof = {};
            //userApprovalLevel = data.approvalLevel;
            //var job_customer_number = (sessionStorage.jobCustomerNumber != undefined && sessionStorage.jobCustomerNumber != null && sessionStorage.jobCustomerNumber != "") ? sessionStorage.jobCustomerNumber : appPrivileges.customerNumber;
            $.each(selected_file_info.listOrderFileVersionses, function (key, val) {
                var visited_file = $.grep(visited_proofs, function (obj) {
                    return obj.fileName == val.origFileName;
                });
                var is_styled = false;
                if (key == 0)
                    is_styled = true;
                version_list.push({
                    "id": "lnk" + val.uuid,
                    "proofUUID": val.uuid,
                    "versionText": val.lastUpdateDate + "\n\r" + val.origFileName,
                    "fileName": val.origFileName,
                    "createdDate": val.lastUpdateDate,
                    "style": (is_styled || visited_file.length > 0) ? "color:#0033CC" : "",
                    "facilityId": proofBobDetails.facilityId,
                    "jobNumber": jobNumber,
                    "index": key
                });
                if (isNewProofGenerationCustomers()) {
                    if (key == 0) {
                        if (visited_file.length == 0)
                            visited_proofs.push({
                                "fileName": val.origFileName
                            });
                        sessionStorage.visitedProofs = JSON.stringify(visited_proofs);
                        current_view_proof = $.extend(true, {}, val);
                        current_view_proof["index"] = key;
                    }
                }
                else if (key == 0) {
                    if (visited_file.length == 0)
                        visited_proofs.push({
                            "fileName": val.origFileName
                        });
                    sessionStorage.visitedProofs = JSON.stringify(visited_proofs);
                    current_view_proof = $.extend(true, {}, val);
                    current_view_proof["index"] = key;
                }
            });
            $('#ulHistory').empty();
            $('#ulHistory').append($("#historyTemplate").tmpl({ versions: version_list }));
            $('#ulHistory').append($("#historyTemplate").tmpl(version_list));
            $('#ulHistory').listview('refresh');
            $('#btnProofHistory').show();
            if (selected_file_info.listOrderFileVersionses.length == 1) {
                $('#btnProofHistory').hide();
                $.each($('#dvProofButtons'), function (a, b) {
                    $(b).controlgroup();
                });
            }
            if (selected_file_info.listOrderFileVersionses != undefined && selected_file_info.listOrderFileVersionses != null && selected_file_info.listOrderFileVersionses.length > 0) {
                sessionStorage.proofingFileVersionList = JSON.stringify(selected_file_info.listOrderFileVersionses);
                viewProof(current_view_proof.origFileName, current_view_proof.lastUpdateDate, current_view_proof.uuid, facilityId, jobNumber, '', window_origin, current_view_proof.index);
            }

            window.setTimeout(function () {
                $.each($('#dvProofButtons'), function (a, b) {
                    $(b).controlgroup();
                });
            }, 2000);
            $.each($('#dvProofButtons'), function (a, b) {
                $(b).controlgroup();
            });
        }
        else {
            $('#viewFrame')[0].contentWindow.document.body.innerHTML = "";
            $('#alertmsg').text("Preview not available.");
            $('#popupDialog').popup('open');
        }
    }
}

function viewProof(proof_file_name, proof_create_date, proof_uuid, facility_id, job_number, window_type, window_origin, proof_index) {
    var visited_proofs = [];
    visited_proofs = (sessionStorage.visitedProofs != undefined && sessionStorage.visitedProofs != null) ? $.parseJSON(sessionStorage.visitedProofs) : [];

    if (gOutputData != undefined && gOutputData != null && (params.length > 1))
        if (gOutputData != undefined && gOutputData != null && Object.keys(gOutputData.artworkAction.artworkFileList).length == 1) {
            if (sessionStorage.customerNumber != CW) {
                $('#btnJobProofs').hide();
            }
            var available_buttons = $('#dvProofButtons').find('a[data-role=button]');
            $.each(available_buttons, function (key) {
                if (key == 0) {
                    $(available_buttons[0]).addClass('ui-corner-lt');
                    $(available_buttons[0]).addClass('ui-corner-lb');
                }
                if (key == (available_buttons.length - 1)) {
                    $(available_buttons[0]).addClass('ui-corner-rt');
                    $(available_buttons[0]).addClass('ui-corner-rb');
                }
            });
        }
        else {
            if ([97, ACE_CustomUpload_JobType, ANDERSEN_CustomUpload_JobType].indexOf(gOutputData.jobTypeId) > -1)
                $('#btnJobProofs').show();
            $('#artApproval').css('display', 'block');
            if (proof_index > 0)
                $('#artApproval').css('display', 'none');
        }
    if (proof_index > 0)
        $('#btnProofForward').css('display', 'none');
    else {
        if (sessionStorage.isOpenedFromEmail == "true" || (sessionStorage.isEmailLogin != undefined && sessionStorage.isEmailLogin != null && sessionStorage.isEmailLogin == "true")) {
            $('#btnProofForward').css('display', 'none');
        }
        else {
            if (!hideForwardProof()) {
                //$('#btnProofForward').css('display', 'block');
                $('#btnProofForward').css('display', 'none');
            }
        }
    }

    //if (sessionStorage.userRole == "admin") {
    //    $('#btnOverrideApprovals').css('display', 'block');
    //    $('#btnOverrideApprovals').css('display', '');
    //}
    //else {
    //    $('#btnOverrideApprovals').css('display', 'none');
    //}

    if (sessionStorage.isOpenedFromEmail == "true")
        sessionStorage.approvalUserEmail = approvalUserEmail;

    if (params.length == 1 && params[0] != undefined && params[0] != "") {
        var current_proof_json = {};
        current_proof_json["encryptedString"] = queryString;
        current_proof_json["uuid"] = proof_uuid;
        if (jobCustomerNumber != SCOTTS) {
            postCORS(serviceURLDomain + "api/Proof_uuid_enc", JSON.stringify(current_proof_json), function (data) {
                crocSession = (data != "" && data.length > 1) ? $.parseJSON(data) : "";
                displayProof(proof_file_name, proof_create_date, proof_uuid, facility_id, job_number, window_type, window_origin, visited_proofs, proof_index);
            }, function (error_response) {
                $('#popupHistory').popup('close');
                showErrorResponseText(error_response, true);
            });
        }
        else {
            displayProof(proof_file_name, proof_create_date, proof_uuid, facility_id, job_number, window_type, window_origin, visited_proofs, proof_index);
        }
    }
    else {
        if (jobCustomerNumber != SCOTTS) {
            if (params.length >= 3 && params[2].substring(0, params[2].indexOf('=')) == "isJobProof" && params[2].substring(params[2].indexOf('=') + 1) == "true" && sessionStorage.isOpenedFromEmail == "true") {
                getCORS(serviceURLDomain + "api/Proof_uuid_email/" + facility_id + "/" + job_number + "/" + proof_uuid + "/" + sessionStorage.approvalUserEmail, null, function (data) {
                    crocSession = data;
                    displayProof(proof_file_name, proof_create_date, proof_uuid, facility_id, job_number, window_type, window_origin, visited_proofs, proof_index);
                }, function (error_response) {
                    $('#popupHistory').popup('close');
                    showErrorResponseText(error_response, true);
                });
            }
            else {
                var temp = "test";
                getCORS(serviceURLDomain + "api/Proof_uuid_email/" + facility_id + "/" + job_number + "/" + proof_uuid + "/" + ((jobCustomerNumber == CASEYS || jobCustomerNumber == EQUINOX || isSendTestEmailWithProofUUID()) ? temp : sessionStorage.approvalUserEmail), null, function (data) { //To be removed later.
                    crocSession = data;
                    displayProof(proof_file_name, proof_create_date, proof_uuid, facility_id, job_number, window_type, window_origin, visited_proofs, proof_index);
                }, function (error_response) {
                    $('#popupHistory').popup('close');
                    showErrorResponseText(error_response, true);
                });
            }
        }
        else {
            displayProof(proof_file_name, proof_create_date, proof_uuid, facility_id, job_number, window_type, window_origin, visited_proofs, proof_index);
        }
    }
}

function displayProof(proof_file_name, proof_create_date, proof_uuid, facility_id, job_number, window_type, window_origin, visited_proofs, proof_index) {
    var job_customer_number = (sessionStorage.jobCustomerNumber != undefined && sessionStorage.jobCustomerNumber != null && sessionStorage.jobCustomerNumber != "") ? sessionStorage.jobCustomerNumber : appPrivileges.customerNumber;
    $('#popupHistory').popup('close');
    if (job_customer_number != CW || appPrivileges.roleName != "admin")
        $('#btnSubmitForExecution').css('display', 'none');

    if (crocSession) {
        var ipage = crocodocViewURL + crocSession;
        window.setTimeout(function () {
            $('#viewFrame').attr('src', '' + ipage + '');
            $('#lnk' + proof_uuid).css('color', '#0033CC');
            var frame_height = (window.screen.availHeight > 860) ? window.screen.availHeight - 154 : '100%';
            $('#viewFrame').css('height', frame_height);
        }, 1000);
        fileName = proof_file_name;
        sessionStorage.currentProofUUID = proof_uuid;
    }
    else {
        if (jobCustomerNumber == SCOTTS) {
            var ipage = "images/proof_-21485.pdf";
            window.setTimeout(function () {
                $('#viewFrame').attr('src', '' + ipage + '');
                $('#lnk' + proof_uuid).css('color', '#0033CC');
                var frame_height = (window.screen.availHeight > 860) ? window.screen.availHeight - 154 : '100%';
                $('#viewFrame').css('height', frame_height);
            }, 1000);
            fileName = proof_file_name;
            sessionStorage.currentProofUUID = proof_uuid;
        }
    }
    var visited_file = $.grep(visited_proofs, function (obj) {
        return obj.fileName == proof_file_name;
    });
    if (visited_file.length == 0)
        visited_proofs.push({
            "fileName": proof_file_name
        });
    sessionStorage.visitedProofs = JSON.stringify(visited_proofs);

    var current_file_version = [];
    current_file_version = getCurrentProofVersion();

    var markup_closed_text = "";
    
    //var temp_date = getProofCreatedDate(proof_create_date);

    var file_name_header = "<b>Name: </b> " + proof_file_name + ((job_customer_number == MILWAUKEE) ? "<b> | Created: </b>" : "<b> | Uploaded: </b>") + proof_create_date;
    //var file_name_header = "<b>Name: </b> " + proof_file_name + "<b> | Uploaded: </b>" + new Date(proof_create_date);
    var proof_status_text = '';
    
    var artworks_files_length = 0;
    if (sessionStorage.jobSetupOutput != undefined) {
        gOutputData = jQuery.parseJSON(sessionStorage.jobSetupOutput);
        var file_approval_status = {};
        if (current_file_version.length > 0)
            file_approval_status = current_file_version[0];
        
        if (Object.keys(file_approval_status).length > 0) {
            var temp_email = ((proofBobDetails.email != undefined && proofBobDetails.email != null) ? proofBobDetails.email : ((sessionStorage.approvalUserEmail != undefined && sessionStorage.approvalUserEmail != null && sessionStorage.approvalUserEmail != "") ? sessionStorage.approvalUserEmail : ""));
            file_name_header += "<b> | User: </b>" + (temp_email.toLowerCase().indexOf('senecaglobal') > -1 ? "" : temp_email);
        }
        if (file_approval_status != undefined && file_approval_status != null && Object.keys(file_approval_status).length > 0 && file_approval_status.listOrderFileVersionApprovals != undefined && file_approval_status.listOrderFileVersionApprovals != null && file_approval_status.listOrderFileVersionApprovals.length > 0) {
            var is_email_found = false;
            $.each(file_approval_status.listOrderFileVersionApprovals, function (key, val) {
                if (temp_email == val.approvalEmail) {
                    is_email_found = true;
                    if (val.approvalLevel == 0) {
                        $('#artApproval').addClass('ui-disabled');
                        $('#artApproval')[0].disabled = true;
                        $('#artApproval').css('display', 'none');
                        $('#btnArtApproval').css('display', 'none');
                    }
                    else {
                        $('#artApproval').removeClass('ui-disabled');
                        $('#artApproval')[0].disabled = false;
                        $('#artApproval').css('display', 'none');
                        $('#btnArtApproval').css('display', 'none');
                        if (proof_index == "0") {
                            $('#artApproval').css('display', 'block');
                            $('#btnArtApproval').css('display', 'block');
                        }
                    }
                    if (val.approvalStatus != undefined && val.approvalStatus != null) {
                        if (val.approvalStatus == "1" || val.approvalStatus == "6") {
                            $('#dvProofFileName').removeClass('ui-bar-a');
                            $('#dvProofFileName').removeClass('ui-bar-h');
                            $('#dvProofFileName').addClass('ui-bar-i');
                        }
                        else if (val.approvalStatus == "0") {
                            $('#dvProofFileName').removeClass('ui-bar-a');
                            $('#dvProofFileName').removeClass('ui-bar-i');
                            $('#dvProofFileName').addClass('ui-bar-h');
                        } else {
                            $('#dvProofFileName').removeClass('ui-bar-i');
                            $('#dvProofFileName').removeClass('ui-bar-h');
                            $('#dvProofFileName').addClass('ui-bar-d');
                        }
                        var approval_icon = "";
                        switch (val.approvalStatus) {
                            case 6:
                                approval_icon = "icon_approved_with_changes_white.png";
                                break;
                            case 1:
                                approval_icon = "icon_approved_white.png";
                                break;
                            case 0:
                                approval_icon = "icon_rejected_white.png";
                                break;
                            default:
                                break;
                        }
                        var approval_status = getApprovalStatus(parseInt(val.approvalStatus));
                        if (approval_icon != "")
                            proof_status_text = '<div style="width:11%;float:right;padding-right:5px"><div class="ui-grid-c" style="width:70%"><div class="ui-block-a" style="width:auto;"><img src="images/' + approval_icon + '" title="' + approval_status + '" class="ui-li-icon ui-corner-none" style="padding-right:4px"/></div><div class="ui-block-b" style="width:auto;"><b>' + approval_status.initCap() + '</b></div><div class="ui-block-c" style="width:auto;"><b> ' + '</b></div></div></div>';
                    }
                    return false;
                }
            });
            if (!is_email_found) {
                $('#artApproval').addClass('ui-disabled');
                $('#artApproval')[0].disabled = true;
            }
            if (file_approval_status.overallStatus == 1 && (appPrivileges.roleName == "user" || (sessionStorage.isOpenedFromEmail != undefined && sessionStorage.isOpenedFromEmail != null && sessionStorage.isOpenedFromEmail == "true"))) {
                markup_closed_text = "<span  class='ui-li-aside' style='font-weight:bold;margin-left:20%'><img src='../images/alert_icon_whitee.png'/>&nbsp;THE ARTWORK HAS BEEN APPROVED.</span>";
                $('#artApproval').css('display', 'none');
                $('#btnArtApproval').css('display', 'none');
                $('#artApproval').hide();
                $('#btnProofForward').css('display', 'none');
                $('#btnProofForward').hide();
                $.each($('#dvProofButtons'), function (a, b) {
                    $(b).controlgroup();
                });
                isMarkupClosed = true;
            }
        }
    }


    $('#dvProofFileName').html(file_name_header + ((markup_closed_text == "") ? markup_closed_text + proof_status_text : markup_closed_text));

    if (window_type == "self" || window_type == "blank" || window_origin == 'job_proofs') {
        if (window_type == "blank" || window_origin == 'job_proofs') {
            $('div[data-role="header"]').remove();
            $('#btnArtApproval').css('display', 'none');
        }
    }
    window.setTimeout(function () {
        if (sessionStorage.isFirstTime != undefined && sessionStorage.isFirstTime != null && sessionStorage.isFirstTime == "true") {
            sessionStorage.isFirstTime = false;
            if (markup_closed_text != "") {
                $('#popupClosedProofingInstructios h3').text('The Artwork has been approved.');
                $('#popupClosedProofingInstructios').popup('open');
            }
            else {
                if (gOutputData.artworkAction != undefined && gOutputData.artworkAction != null)
                    if (Object.keys(gOutputData.artworkAction.artworkFileList).length == 0)
                        $('#multipleProofsIns').css('display', 'none');
                if (userApprovalLevel == 0)
                    $('#approverIns').css('display', 'none');
            }
        }
        if (markup_closed_text != "") {
            $('#artApproval').css('display', 'none');
            $('#btnArtApproval').css('display', 'none');
            $('#btnProofForward').css('display', 'none');
            $.each($('#dvProofButtons'), function (a, b) {
                $(b).controlgroup();
            });
        }
        if (jobCustomerNumber == BBB || jobCustomerNumber == AAG || jobCustomerNumber == ALLIED) {
            if (appPrivileges.roleName != "admin")
                $('#btnProofForward').css('display', 'none');
            if (artworks_files_length > 1)
                $('#btnJobProofs').addClass('ui-first-child');
            else {
                $('#btnJobProofs').css('display', 'none');
                $('#demoInstructions1').addClass('ui-first-child ui-last-child');
            }
        }
        if (jobSummaryProofType) {
            if (jobSummaryProofType == "history") {
                openPopups("popupHistory");
            }
            else {
                openPopups("popupApproval");
            }
            jobSummaryProofType = "";
        }
    }, 2000);
    $('#historyPanel').panel('close', 'optionsHash');
    $('#jobProofsPanel').panel('close', 'optionsHash');
    disableSaveForClosedJobs();
}

function getProofCreatedDate(proof_create_date) {
    var temp_date = new Date(proof_create_date);

    var day = temp_date.getDate();
    day = (day.toString().length == 1) ? ('0' + day.toString()) : day;
    var month = temp_date.getMonth();
    month = (month.toString().length == 1) ? ('0' + month.toString()) : month;
    var year = temp_date.getFullYear();
    var hours = temp_date.getHours();
    var minutes = temp_date.getMinutes();
    //var ampm = (hours>12)?
    var date_string = "";
    date_string += month + "/" + day + "/" + year;
    date_string += " " + hours + ":" + minutes;
    return date_string;
    //
}

function fnApprovalValidation(type) {
    if ((type == undefined || type == null || type == "") && $("input[type=radio]:checked").val() === undefined) {
        return false;
    }
    else if (type === "approval" && $("input[type=radio]:checked").val() === undefined) {
        $('#alertmsg').text('Please choose any one of the approval type.');
        $('#popupApproval').popup('close');
        if (hideListSummaryApproval()) {
            $('#dvListSummaryApproval').css('display', 'none');
        }
        window.setTimeout(function () {
            $('#popupDialog').popup('open');
            $('#okBut').attr('onclick', '$("#popupDialog").popup("close");window.setTimeout(function(){$("#popupApproval").popup("open", { positionTo: "#artApproval" });},500)');
        }, 500);
        return false;
    }
    else if (type === "approval" && $("input[type=radio]:checked").val() == 0) {
        if ($('#txtReasonForRejection').val() == "") {
            $('#alertmsg').text('Please enter Reason for Rejection.');
            $('#popupApproval').popup('close');
            window.setTimeout(function () {
                $('#popupDialog').popup('open');
                $('#okBut').attr('onclick', '$("#popupDialog").popup("close");window.setTimeout(function(){$("#popupApproval").popup("open", { positionTo: "#artApproval" });},500)');
            }, 500);
            return false;
        }
    }
    return true;
}


function applyToAll() {
    if (!fnApprovalValidation('approval'))
        return false;
    $('#okButConfirm').text('Submit');
    $('#okButConfirm').attr('onclick', 'continueApplyToAll()');
    $('#cancelButConfirm').attr('onclick', 'cancelApplyToAll()');
    $('#confirmMsg').text('The selected approval status will be applied to all proofs in this job if you submit.');
    $("#popupApproval").popup("close");
    $('#popupConfirmDialog').popup('open');
}

function cancelApplyToAll() {
    $('#popupConfirmDialog').popup('close');
    $("#popupApproval").popup("close");
    if (hideListSummaryApproval()) {
        $('#dvListSummaryApproval').css('display', 'none');
    }
    $("#popupApproval").popup("open", { positionTo: '#artApproval' });
}

function continueApplyToAll() {
    if (fnApprovalValidation("")) {
        $('#popupConfirmDialog').popup('close');
        submitApprovals("approval");
    }
    else {
        $('#btnSubmitApprovals').attr('onclick', 'submitApprovals("approval")');
        $('#alertmsg').text('You must select an approval status to apply to all of the proofs.');
        $('#popupConfirmDialog').popup('close');
        $('#popupDialog').popup('open');
        if (hideListSummaryApproval()) {
            $("#dvListSummaryApproval").css("display", "none");
        }
        $('#okBut').attr('onclick', '$("#popupDialog").popup("close");$("#popupApproval").popup("open", { positionTo: "#artApproval" });');
    }
}

function approvalSubmit(approval_override_status) {
    var job_customer_number = getCustomerNumber();

    var proofing_file_versions_list = [];
    proofing_file_versions_list = (sessionStorage.proofingFileVersionList != undefined && sessionStorage.proofingFileVersionList != null && sessionStorage.proofingFileVersionList != "") ? jQuery.parseJSON(sessionStorage.proofingFileVersionList) : [];
    var selected_file_version = (sessionStorage.currentProofUUID != undefined && sessionStorage.currentProofUUID != null && sessionStorage.currentProofUUID != "") ? sessionStorage.currentProofUUID : "";
    var current_file_version = [];
    if (selected_file_version != undefined && selected_file_version != null && selected_file_version != "") {
        current_file_version = $.grep(proofing_file_versions_list, function (obj) {
            return obj.uuid === selected_file_version && obj.origFileName === fileName;
        });
    }
    var temp_approvals = {};
    if (current_file_version.length > 0) {
        $.each(current_file_version[0].listOrderFileVersionApprovals, function (key, val) {
            if (proofBobDetails.email == val.approvalEmail) {
                if (val.approvalStatus != undefined && val.approvalStatus != null) {
                    val.approvalStatus = $("input[type=radio]:checked").val();
                    if ($("input[type=radio]:checked").val() == 0) {
                        val["notes"] = $('#txtReasonForRejection').val();
                    } else {
                        val["notes"] = "";
                    }
                }
                temp_approvals = val;
                return false;
            }
        });
    }
    sessionStorage.proofingFileVersionList = JSON.stringify(proofing_file_versions_list);

    if (sessionStorage.isOpenedFromEmail == "true")
        job_customer_number = sessionStorage.customerNumber;

    var temp_info = $.extend(true, {}, proofBobDetails);
    delete temp_info.listOrderJobses;
    var is_found = false;
    //var file_index = 0;
    var temp_data = {};
    $.each(temp_info.myOrder.listOrderFileses, function (key, val) {
        $.each(val.listOrderFileVersionses.reverse(), function (key2, val2) {
            if (val2.origFileName == proofing_file_versions_list[0].origFileName) {
                is_found = true;
                return false;
            }
        });
        if (is_found) {
            //file_index = key;
            temp_data = val;
            return false;
        }
    });
    temp_info.myOrder.listOrderFileses = [];
    temp_info.myOrder.listOrderFileses.push(temp_data);
    temp_info.myOrder.listOrderFileses[0].listOrderFileVersionses = [];
    temp_info.myOrder.listOrderFileses[0].listOrderFileVersionses.push(proofing_file_versions_list[0]);
    temp_info.myOrder.listOrderFileses[0].listOrderFileVersionses[0].listOrderFileVersionApprovals = [];
    temp_info.myOrder.listOrderFileses[0].listOrderFileVersionses[0].listOrderFileVersionApprovals.push(temp_approvals);

    //api/Approval_overrideApprovals/{facility_id}/{customer_number}/{job_number}/{approval_status}, 
    //api/Approval_create/{facility_id}/{customer_number}/{job_number}/{approval_override_status}
    var service_url = serviceURLDomain + "api/Approval_create/" + appPrivileges.facility_id + "/" + job_customer_number + "/" + jobNumber + "/" + approval_override_status
    postCORS(service_url, JSON.stringify(temp_info), function (response) {
        if (response != undefined && response != null && response != "") {
            getCORS(serviceURLDomain + "api/Proof_getApprovals/" + appPrivileges.facility_id + "/" + job_customer_number + "/" + jobNumber, null, function (data) {
                if (data != null && data != "") {
                    $.each(proofBobDetails.myOrder.listOrderFileses, function (key, val) {
                        $.each(val.listOrderFileVersionses, function (key1, val1) {
                            if (val1.uuid == data.uuid) {
                                $.each(val1.listOrderFileVersionApprovals, function (appr_key, appr_val) {
                                    $.each(data.listOrderFileVersionApprovals, function (temp_appr_key, temp_appr_val) {
                                        if (appr_val.pk == temp_appr_val.pk) {
                                            val1.listOrderFileVersionApprovals[appr_key] = temp_appr_val;
                                            val1.overallStatus = data.overallStatus;
                                            return false;
                                        }
                                    });
                                });
                                return false;
                            }
                        });
                    });
                }
            }, function (error_response) {
                showErrorResponseText(error_response, true);
            });

            var temp_email = ((proofBobDetails.email != undefined && proofBobDetails.email != null) ? proofBobDetails.email : ((sessionStorage.approvalUserEmail != undefined && sessionStorage.approvalUserEmail != null && sessionStorage.approvalUserEmail != "") ? sessionStorage.approvalUserEmail : ""));

            if (current_file_version.length > 0) {
                $.each(current_file_version[0].listOrderFileVersionApprovals, function (key, val) {
                    if (temp_email == val.approvalEmail) {
                        if (val.approvalStatus != undefined && val.approvalStatus != null) {
                            if (val.approvalStatus == "1" || val.approvalStatus == "6") {
                                $('#dvProofFileName').removeClass('ui-bar-a');
                                $('#dvProofFileName').removeClass('ui-bar-h');
                                $('#dvProofFileName').addClass('ui-bar-i');
                            }
                            else if (val.approvalStatus == "0") {
                                $('#dvProofFileName').removeClass('ui-bar-a');
                                $('#dvProofFileName').removeClass('ui-bar-i');
                                $('#dvProofFileName').addClass('ui-bar-h');
                            } else {
                                $('#dvProofFileName').removeClass('ui-bar-i');
                                $('#dvProofFileName').removeClass('ui-bar-h');
                                $('#dvProofFileName').addClass('ui-bar-a');
                            }
                            var approval_icon = "";
                            switch (val.approvalStatus) {
                                case "6":
                                    approval_icon = "icon_approved_with_changes_white.png";
                                    break;
                                case "1":
                                    approval_icon = "icon_approved_white.png";
                                    break;
                                case "0":
                                    approval_icon = "icon_rejected_white.png";
                                    break;
                                default:
                                    break;
                            }
                            var approval_status = getApprovalStatus(parseInt(val.approvalStatus));
                            $('#dvProofFileName').find('img').attr('src', 'images/' + approval_icon);
                            $('#dvProofFileName').find('img').attr('title', approval_status.initCap());
                            $('#dvProofFileName').find('img').parent().parent().find('div.ui-block-b').html('<b>' + approval_status.initCap() + '</b>');
                        }
                        return false;
                    }
                });
            }

            var msg = "Approvals have been submitted successfully.";

            $('#alertmsg').text(msg);
            $('#popupOverrideApprovals').popup('close');
            window.setTimeout(function () {
                $('#popupDialog').popup('open');
            }, 1000);
            $('#okBut').attr('onclick', '$("#popupDialog").popup("close");');
        }
    }, function (error_response) {
        showErrorResponseText(error_response, true);
    });
}

function submitApprovalsOverride(popup_id) {
    if (popup_id == 'overrideapprovals') {
        $('#popupOverrideApprovals').popup('close');        
        var approval_status = $("input[type=radio][name=rdoApproveOverride]:checked").val();
        if (!approval_status)
            approval_status = 4;

        approvalSubmit(approval_status);
    }
}

function submitApprovals(type) {
    var job_customer_number = (sessionStorage.jobCustomerNumber != undefined && sessionStorage.jobCustomerNumber != null && sessionStorage.jobCustomerNumber != "") ? sessionStorage.jobCustomerNumber : appPrivileges.customerNumber;

    if (!fnApprovalValidation(type))
        return false; //checking whether any one of the approval radio control selected

    if ((job_customer_number == BBB || job_customer_number == AAG || job_customer_number == ALLIED) && appPrivileges.roleName != "admin") {
        $('#alertmsg').text("Thanks, but this is a demo. No further action will be taken.");
        $('#popupApproval').popup('close');
        $('#popupDialog').popup('open');
        $('#okBut').attr('onclick', '$("#popupDialog").popup("close");');
        return false;
    }

    var selected_proofs_list = $('#dvProofsToApprove input[type=checkbox]:not([id="chkListSummary"]):not([id=chkSelectAllProofs_approve])');
    var selected_proofs = [];
    $.each(selected_proofs_list, function (a, b) {
        selected_proofs.push({
            "fileName": $(b).val(),
            "requiredApproval": ($(b)[0].checked) ? 1 : 0,
            "notes": $('#txtReasonForRejection').val()
        });
    });
    
    approvalSubmit("-1");

    $('#popupApproval').popup('close');
    $('#btnSubmitApprovals').attr('onclick', 'submitApprovals("approval")');
}

function closePopUps(popup_id) {
    if (popup_id == "popupApproval") {
        $('input[type=radio]').attr('checked', false).checkboxradio('refresh');
        $('#txtApproverName').val('');
    }
    else if (popup_id == "popupForward") {
        $('#txtEmailLink').val('');
    }
    $('#' + popup_id).popup('close');
}

function loadProofsToApprove(file_list) {
    var selected_proofs = "";
    var id = "";
    $.each(file_list, function (key, val) {
        $.each(val.listOrderFileVersionses.reverse(), function (key1, val1) {
            if (key1 == 0) {
                $.each(val1.listOrderFileVersionApprovals, function (key2, val2) {
                    if ((val1.lastUpdatedEmail == val2.approvalEmail && val2.approvalLevel == "1") || (sessionStorage.isOpenedFromEmail == undefined || sessionStorage.isOpenedFromEmail == null || sessionStorage.isOpenedFromEmail == "false" || approvalUserEmail == "")) {
                        id = val1.origFileName.replace(/ /g, '_').substring(0, val1.origFileName.lastIndexOf('.')) + '_approve';
                        selected_proofs += '<input type="checkbox" name="chk' + id + '" id="chk' + id + '" data-theme="c" value="' + val1.origFileName + '" onchange="makeProofSelection(this,\'dvProofsToApprove\')"/><label for="chk' + id + '">' + val1.origFileName + '</label>';
                    }
                });
            }
        });
    });
    if (selected_proofs != "") {
        var proof_header = '<div data-role="collapsible-set" data-theme="a" data-content-theme="d" data-mini="true" style="padding-left: 8px; padding-right: 8px;">';
        proof_header += '<div data-role="collapsible" data-collapsed-icon="carat-r" data-expanded-icon="carat-d" data-theme="f" data-content-theme="d" data-collapsed="true">';
        proof_header += '<h3>Select Proofs to Apply Approvals</h3><fieldset data-role="controlgroup" data-mini="true" data-theme="a">';
        selected_proofs = proof_header + '<input type="checkbox" name="chkSelectAllProofs_approve" id="chkSelectAllProofs_approve" data-theme="c" onchange="selectAllProofs(this,\'dvProofsToApprove\')"/><label for="chkSelectAllProofs_approve"> Select All Proofs</label>' + selected_proofs;
        selected_proofs += '</fieldset></div></div>';
        $('#dvProofsToApprove').empty();
        $('#dvProofsToApprove').append(selected_proofs);
        $('#dvProofsToApprove').find('div[data-role=collapsible-set]').collapsibleset().trigger('create');
        $('#dvProofsToApprove').css('display', 'none');
        $('div.ui-collapsible-content', $('#dvProofsToApprove').find('div[data-role=collapsible-set]')).trigger('expand');
    }
}

function openPopups(popup_id) {
    var current_file_version = [];
    var job_customer_number = "";
    if (popup_id == "popupHistory") {
        $('#historyPanel').panel('open', 'optionsHash');
    }
    else if (popup_id == "popupOverrideApprovals") {
        //$('#popupApprovers').popup('close');
    }
    else if (popup_id == "popupApproval" || popup_id == "bottomPopupApproval") {
        var artwork_file_list = (proofBobDetails.myOrder.listOrderFileses != undefined && proofBobDetails.myOrder.listOrderFileses != null && proofBobDetails.myOrder.listOrderFileses.length > 0) ? proofBobDetails.myOrder.listOrderFileses : [];
        var temp_file_list = $.extend(true, {}, artwork_file_list);

        loadProofsToApprove(temp_file_list);

        $('input[type=radio]').attr('checked', false).checkboxradio('refresh');
        $('#txtApproverName').val('');
        current_file_version = [];
        current_file_version = getCurrentProofVersion();

        if (current_file_version.length > 0) {
            $('input[type=checkbox][value="' + current_file_version[0].origFileName + '"]').attr('checked', true).checkboxradio('refresh');
            if (current_file_version[0].approvalType != undefined && current_file_version[0].approvalType != null) {
                if (current_file_version[0].approvalType == 2)
                    $('#spnApprovalType').text('Print');
                else
                    $('#spnApprovalType').text('Lettershop');
            }

            if (current_file_version != undefined && current_file_version != null && current_file_version.length > 0 && current_file_version[0].listOrderFileVersionApprovals != undefined && current_file_version[0].listOrderFileVersionApprovals != null && current_file_version[0].listOrderFileVersionApprovals.length > 0) {
                $.each(current_file_version[0].listOrderFileVersionApprovals, function (key, val) {
                    if (proofBobDetails.email == val.approvalEmail) {
                        approvalUserEmail = val.approvalEmail;
                        if (val.approvalStatus != undefined && val.approvalStatus != null) {
                            $('input:radio[value="' + val.approvalStatus + '"]').attr('checked', true).checkboxradio('refresh');
                            if (val.approvalStatus == 0) {
                                $('#txtReasonForRejection').val(val.notes);
                                $('#dvReasonForRejection').css('display', 'block');
                            }
                        }
                        return false;
                    }
                });
            }
            $('#txtApproverName').text(((approvalUserEmail.toLowerCase().indexOf('senecaglobal') > -1) ? "" : approvalUserEmail));
            job_customer_number = jobCustomerNumber || appPrivileges.customerNumber;
            if (isNewProofGenerationCustomers()) {
                var location_wise_cost_data = "";
                $('#tblLocationWiseCosts').empty();
                var storeId_pk_dicts;
                getCORS(serviceURLDomain + "api/JobSummary_get/" + appPrivileges.facility_id + "/" + job_customer_number + "/" + jobNumber, null, function (data) {
                    storeId_pk_dicts = data.storeIdToPkDict;
                    if (storeId_pk_dicts != undefined && storeId_pk_dicts != null && storeId_pk_dicts != "") {
                        if (data.myTblSummaryJob && data.myTblSummaryJob.listSummaryStores) {
                            $.each(data.myTblSummaryJob.listSummaryStores, function (key, val) {
                                if (val.actualQuantity != undefined && val.actualQuantity != null && val.actualQuantity != "" && val.actualQuantity != 0) {
                                    $.each(storeId_pk_dicts, function (key1, val1) {
                                        if (val.storePk == val1) {
                                            var sub_total = ((val.actualPostageCostPerPiece + val.actualQuantity) * val.productionCostPerPiece);
                                            if (val.listSummaryAdditionalExpenses != undefined && val.listSummaryAdditionalExpenses != null && val.listSummaryAdditionalExpenses != "") {
                                                $.each(val.listSummaryAdditionalExpenses, function (key2, val2) {
                                                    if (val2.costType == "costPerPiece") {
                                                        sub_total += (val.actualQuantity * val2.cost);
                                                    }
                                                    else {
                                                        sub_total += ((val2.cost != undefined && val2.cost != null && val2.cost != "") ? parseFloat(val2.cost) : 0);
                                                    }
                                                });
                                            }
                                            location_wise_cost_data += "<tr><td>" + key1 + "</td><td style='text-align:right;'>" + val.actualQuantity.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + "</td><td style='text-align:right'>" + ("$" + (parseFloat(sub_total).toFixed(2)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")) + "</td></tr>";
                                            return false;
                                        }
                                    });
                                }
                                else if (val.estimatedQuantity != undefined && val.estimatedQuantity != null && val.estimatedQuantity != "" && val.estimatedQuantity != 0) {
                                    $.each(storeId_pk_dicts, function (key1, val1) {
                                        if (val.storePk == val1) {
                                            var sub_total = ((val.estimatedPostageCostPerPiece + val.estimatedQuantity) * val.productionCostPerPiece);
                                            if (val.listSummaryAdditionalExpenses != undefined && val.listSummaryAdditionalExpenses != null && val.listSummaryAdditionalExpenses != "") {
                                                $.each(val.listSummaryAdditionalExpenses, function (key3, val2) {
                                                    if (val2.costType == "costPerPiece") {
                                                        sub_total += (val.estimatedQuantity * val2.cost);
                                                    }
                                                    else {
                                                        sub_total += ((val2.cost != undefined && val2.cost != null && val2.cost != "") ? parseFloat(val2.cost) : 0);
                                                    }
                                                });
                                            }
                                            location_wise_cost_data += "<tr><td>" + key1 + "</td><td style='text-align:right;'>" + val.estimatedQuantity.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + "</td><td style='text-align:right'>" + ("$" + (parseFloat(sub_total).toFixed(2)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")) + "</td></tr>";
                                            return false;
                                        }
                                    });
                                }
                            });
                        }
                        if (location_wise_cost_data != "") {
                            $('#tblLocationWiseCosts').append('<thead><tr class="th-groups" data-theme="f"><th>Location</th><th>Qty</th><th>Cost</th></tr></thead>');
                            location_wise_cost_data = '<tbody>' + location_wise_cost_data + '</tbody>';
                            $('#tblLocationWiseCosts').append(location_wise_cost_data);
                            $('#tblLocationWiseCosts').trigger('create');
                            window.setTimeout(function () {
                                $('#tblLocationWiseCosts').table('refresh');
                            }, 1000);
                        }
                    }
                }, function (error_response) {
                    showErrorResponseText(error_response, true);
                });
            }
        }
    }
    else if (popup_id == "popupForward") {
        $('#txtEmailLinkApprovers').val('');
        $('#txtEmailLinkViewers').val('');
        $('#txtMessage').val('');
        var customer_number = jobCustomerNumber || appPrivileges.customerNumber;
        if (customer_number != SPORTSKING && customer_number != KUBOTA) {
            loadSelectedProofs();
        }
        $('#chkRequireApprovals').attr('checked', false).checkboxradio('refresh');
        $('#chkEnablePowerUser').hide();
        if (appPrivileges.roleName == "admin") {
            $('#chkEnablePowerUser').attr('checked', false).checkboxradio('refresh');
            $('#chkEnablePowerUser').show();
        }
        $('#chkRequireApprovals').parent().hide();
        if (artworkFileInfo.proofForwardEmailLinks != undefined && artworkFileInfo.proofForwardEmailLinks != null && artworkFileInfo.proofForwardEmailLinks != "")
            $('#txtEmailLink').val(artworkFileInfo.proofForwardEmailLinks);
        current_file_version = [];
        current_file_version = getCurrentProofVersion();
        if (current_file_version.length > 0) {
            $('input[type=checkbox][value="' + current_file_version[0].origFileName + '"]').attr('checked', true).checkboxradio('refresh');
        }
    }
    else if (popup_id == "popupApprovers") {
        displayApproversInfo();
        job_customer_number = jobCustomerNumber || appPrivileges.customerNumber;
        if (job_customer_number == DCA || job_customer_number == SPORTSKING || job_customer_number == OH || job_customer_number == CASEYS || job_customer_number == KUBOTA || job_customer_number == EQUINOX || jobCustomerNumber == ALLIED || hideApproverSubmit()) {
            $('#btnApproverSubmit').css('display', 'none');
            $('#btnApproverCancel').text('OK');
        }
    }
    createPlaceHolderforIE();
    if (hideListSummaryApproval()) {
        $("#dvListSummaryApproval").css("display", "none");
    }
    //This button will display at the bottom, it is added in navLinks (popup_id = bottomPopupApproval & popupApproval Both are same) 
    if (popup_id == "bottomPopupApproval") {
        $('#popupApproval').popup("open", { positionTo: '#viewFrame' });
    }
    else if (popup_id == "popupOverrideApprovals") {
        window.setTimeout(function () {
            $('#' + popup_id).popup("open", { positionTo: '#btnApprovers' });
        }, 200);
    }
    else {
        $('#' + popup_id).popup("open", { positionTo: '#' + $("a[id*='" + popup_id.substring(popup_id.indexOf('popup') + 5) + "']").attr('id') });
    }

}

function getCurrentProofVersion() {
    var current_file_version = [];
    var proofing_file_versions_list = (sessionStorage.proofingFileVersionList != undefined && sessionStorage.proofingFileVersionList != null && sessionStorage.proofingFileVersionList != "") ? jQuery.parseJSON(sessionStorage.proofingFileVersionList) : [];
    var selected_file_version = (sessionStorage.currentProofUUID != undefined && sessionStorage.currentProofUUID != null && sessionStorage.currentProofUUID != "") ? sessionStorage.currentProofUUID : "";
    if (selected_file_version != undefined && selected_file_version != null && selected_file_version != "") {
        current_file_version = $.grep(proofing_file_versions_list, function (obj) {
            return obj.uuid === selected_file_version && obj.origFileName === fileName;
        });
    }
    return current_file_version;
}
function createPlaceHolderforIE() {
    if ($.browser.msie) {
        $('input[placeholder]').each(function () {
            var input = $(this);
            //$(input).val('');
            //if ($(input).val() == "")
            $(input).val(input.attr('placeholder'));
            if ($(input).val() == input.attr('placeholder'))
                $(input).css('color', 'grey');
            $(input).focus(function () {
                if (input.val() == input.attr('placeholder')) {
                    input.val('').css('color', 'black');
                }
            });
            $(input).blur(function () {
                if (input.val() == '' || input.val() == input.attr('placeholder')) {
                    input.val(input.attr('placeholder')).css('color', 'grey');
                }
            });
        });
    };
}
function getURLParameter(name) {
    return decodeURI(
        (RegExp(name + '=' + '(.+?)(&|$)').exec(location.search) || [, null])[1]
    );
}
//HISTORY functionality -- START --
function openProofInNewWindow(proof_file_name, proof_create_date, proof_uuid, facility_id, job_number, proof_index, window_type) {
    var selected_version = [];
    selected_version.push({
        "facilityId": facility_id,
        "fileName": proof_file_name,
        "jobNumber": job_number,
        "proofCreatedDate": proof_create_date,
        "proofUUID": proof_uuid,
        "windowType": window_type,
        "index": proof_index
    });
    sessionStorage.selectedVersion = JSON.stringify(selected_version);
    var visited_proofs = [];
    visited_proofs = (sessionStorage.visitedProofs != undefined && sessionStorage.visitedProofs != null) ? $.parseJSON(sessionStorage.visitedProofs) : [];
    var visited_file = $.grep(visited_proofs, function (obj) {
        return obj.fileName == proof_file_name;
    });
    if (visited_file.length == 0)
        visited_proofs.push({
            "fileName": proof_file_name
        });
    sessionStorage.visitedProofs = JSON.stringify(visited_proofs);
    var query_string = "fileName=" + proof_file_name + "&jobNumber=" + jobNumber;
    var new_url_string = urlString.substr(0, urlString.indexOf("?") + 1);
    new_url_string = new_url_string + query_string;
    $('#popupHistory').popup('close');
    $('#lnk' + proof_uuid).css('color', '#0033CC');
    $('#historyPanel').panel('close', 'optionsHash');

    sessionStorage.approvalUserEmail = approvalUserEmail;
    sessionStorage.isNewWindow = true;
    if (new_url_string.indexOf('?') == -1) {
        new_url_string = serviceURLDomain + "/" + "jobApproval/jobApproval.html?" + new_url_string;
    } 
    window.open(new_url_string);
}
//HISTORY functionality -- END --

function fnCheckEmailFormat(email_id) {
    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    var msg = "";
    if (email_id == "")
        msg = 'Please provide an email address';
    else {
        var email_ids = email_id;
        if (email_ids.indexOf(',') > -1) {
            email_ids = email_ids.split(',');
            $.each(email_ids, function (a, b) {
                if (!filter.test(b))
                    msg = 'Please provide a valid email address';
            });
        }
        else {
            if (!filter.test(email_id))
                msg = 'Please provide a valid email address';
        }
    }

    $('#okBut').attr('onclick', '$("#popupDialog").popup("close");');
    if (msg != "") {
        $('#popupForward').popup('close');
        $('#alertmsg').text(msg);
        $('#popupDialog').popup('open');
        $('#okBut').attr('onclick', '$("#popupDialog").popup("close");$("#popupForward").popup("open",{positionTo:"#btnProofForward"});');
        return false;
    }
    return true;
}
function sendProofEmail() {
    //---------- START ----------- 
    //The below code has to be used when sending optional message to service is enabled as post else need to use message as parameter to the service as email_links are being sent.
    
    var job_customer_number = (sessionStorage.jobCustomerNumber != undefined && sessionStorage.jobCustomerNumber != null && sessionStorage.jobCustomerNumber != "") ? sessionStorage.jobCustomerNumber : appPrivileges.customerNumber;
    var email_to = {};
    email_to["approvers"] = ($('#txtEmailLinkApprovers').val().trim() != "" && $('#txtEmailLinkApprovers').val().trim() != $('#txtEmailLinkApprovers').attr('placeholder')) ? $('#txtEmailLinkApprovers').val() : "";
    email_to["viewers"] = ($('#txtEmailLinkViewers').val().trim() != "" && $('#txtEmailLinkViewers').val().trim() != $('#txtEmailLinkViewers').attr('placeholder')) ? $('#txtEmailLinkViewers').val() : "";
    if (!forwardProofEmailsValidation(email_to["approvers"], email_to["viewers"])) {
        return false;
    }
    var forward_proof_json = {};
    forward_proof_json["uuid"] = (sessionStorage.currentProofUUID != undefined && sessionStorage.currentProofUUID != null && sessionStorage.currentProofUUID != "") ? sessionStorage.currentProofUUID : "";
    forward_proof_json["proofName"] = fileName; //encodedFileName;
    forward_proof_json["emailTos"] = email_to;
    forward_proof_json["emailMessage"] = $('#txtMessage').val();
    forward_proof_json["requireApprovals"] = ($('#chkRequireApprovals').attr('checked') == "checked") ? true : false;
    if (!isNewProofGenerationCustomers()) {
        var selected_proofs_list = $('#dvSelectedProofs input[type=checkbox]:not([id^=chkSelectAllProofs])');
        if ($('#dvSelectedProofs input[type=checkbox]:not([id^=chkSelectAllProofs]):checked').length == 0 && email_to.approvers != "") {
            $('#popupForward').popup('close');
            var msg = "Please select at least one proof for approvers.";
            $('#alertmsg').html(msg);
            window.setTimeout(function () {
                $('#popupDialog').popup('open');
            }, 1000);
            return false;
        }
        var selected_proofs = [];
        $.each(selected_proofs_list, function (a, b) {
            selected_proofs.push({
                "fileName": $(b).val(),
                "requiredApproval": ($(b)[0].checked) ? 1 : 0
            });
        });
        forward_proof_json["selectedProofs"] = selected_proofs;
    }

    forward_proof_json["isPowerUser"] = ($('#chkEnablePowerUser').length > 0 && $('#chkEnablePowerUser')[0].checked) ? 1 : 0;
    //---------- END ----------- 
    postCORS(serviceURLDomain + "api/Proof_post/" + job_customer_number + "/" + facilityId + "/" + jobNumber, JSON.stringify(forward_proof_json), function (response) {
        if (response == "") {
            $('#popupForward').popup('close');
            $('#alertmsg').text("An email has been successfully sent with a link to the proof.");
            window.setTimeout(function () {
                $('#popupDialog').popup('open');
            }, 1000);
        }
    }, function (error_response) {
        $('#popupForward').popup('close');
        showErrorResponseText(error_response, true);
    });
}
//Funtions to authorise the user and get the job info.  --- START ---
function makeBasicAuth(user, password) {
    var tok = user + ':' + password;
    var hash = Base64.encode(tok);
    return "Basic " + hash;
}

function loginAuthorization(response) {
    if (response != undefined && response != null && response != "") {
        userName = response.userName;
        jobNumber = response.jobNumber;
        facilityId = response.facilityId;
        sessionStorage.authString = makeBasicAuth(userName, response.password);
        getCORS(gServiceUrl, null, function (data) {
            if (data.Message != undefined) {
                $('#alertmsg').text(data.Message);
                $('#popupDialog').popup('open');
                return false;
            }
            else {
                captureAndNavigate(data, response);
            }
        }, function (response_error) {
            $('#viewFrame')[0].contentWindow.document.body.innerHTML = "";
            showErrorResponseText(response_error, false);
        });
    }
}

function getEditJobData() {
    getCORS(gEditJobService + facilityId + "/" + jobNumber + "/asdf"/* + userName*/, null, function (data) {
        var g_output_data = data;
        //var is_email_user = (sessionStorage.isOpenedFromEmail != undefined && sessionStorage.isOpenedFromEmail != null && sessionStorage.isOpenedFromEmail != "") ? JSON.parse(sessionStorage.isOpenedFromEmail) : false;
        getCORS(serviceURLDomain + "api/Locations_dropDown/" + appPrivileges.facility_id + "/" + appPrivileges.customerNumber + "/" + g_output_data.jobNumber + "/0/2/" + g_output_data.jobTypeId, null, function (data) {
            if (data.locationsSelected != undefined && data.locationsSelected != null && data.locationsSelected != "")
                g_output_data.selectedLocationsAction.selectedLocationsList = data.locationsSelected;
            else
                g_output_data.selectedLocationsAction.selectedLocationsList = [];
            sessionStorage.jobSetupOutput = JSON.stringify(g_output_data);
            sessionStorage.jobSetupOutputCompare = JSON.stringify(g_output_data);
            gOutputData = (sessionStorage.jobSetupOutput != undefined && sessionStorage.jobSetupOutput != null) ? jQuery.parseJSON(sessionStorage.jobSetupOutput) : [];
            sessionStorage.jobDesc = g_output_data.jobName;
            loadJobTicket();
            displayJobInfo();
        }, function (error_response) {
            showErrorResponseText(error_response, true);
            g_output_data.selectedLocationsAction.selectedLocationsList = [];
            sessionStorage.jobSetupOutput = JSON.stringify(g_output_data);
            sessionStorage.jobSetupOutputCompare = JSON.stringify(g_output_data); //this session is to use in approval checkout page to get 
            loadJobTicket();
        });
    }, function (error_response) {        
        showErrorResponseText(error_response, true);
        $('#viewFrame')[0].contentWindow.document.body.innerHTML = "";
    });
}

function loadJobTicket() {
    if (sessionStorage.jobSetupOutput != undefined) {
        gOutputData = jQuery.parseJSON(sessionStorage.jobSetupOutput);
        loadJobProofsList();
    }
}

function captureAndNavigate(data, response) {
    sessionStorage.username = userName;
    sessionStorage.userRole = (data["roleName"].indexOf('_') > -1) ? data["roleName"].split('_')[0] : data["roleName"];
    sessionStorage.companyName = data["companyName"].toLowerCase();
    sessionStorage.customerNumber = (sessionStorage.isOpenedFromEmail == "true") ? response.customerNumber : data["customerNumber"];
    sessionStorage.jobCustomerNumber = response.customerNumber;
    $.getJSON('JSON/_proofingFileVersionInfo.JSON', function (data) {
        $.each(data, function (key, val) {
            if (key == sessionStorage.jobCustomerNumber) {
                if (key == KUBOTA && sessionStorage.userRole != "user")
                    return false;

                staticProofingFileVersionList = val;
            }
        });
    });
    if (response.customerNumber == DCA || response.customerNumber == SPORTSKING)
        $('#btnSubmitForExecution').css('display', 'none');
    facilityId = data.facility_id;
    sessionStorage.facilityId = facilityId;
    sessionStorage.jobNumber = jobNumber;
    if (response.isPowerUser == 1) {
        $.each(data, function (key) {
            if (key.toLowerCase().indexOf('_page_') > -1 && key.toLowerCase().indexOf('artwork') == -1)
                delete data[key];
        });
    }
    loadDemoHintsInfo();

    sessionStorage.nextPageInOrder = "../jobUploadArtwork/jobUploadArtwork.html";
    saveSessionData("appPrivileges", JSON.stringify(data));
    appPrivileges = jQuery.parseJSON(getSessionData("appPrivileges"));
    sessionStorage.nextPageInOrder = '../jobUploadArtwork/jobUploadArtwork.html';
    if (appPrivileges.roleName != "admin" || response.isPowerUser == 0)
        $('#btnContinue').css('display', 'none');
    loadFooterInfo();
    window.setTimeout(function () {
        loadProofsInfo(response, '');
        createDemoHints("jobApproval");
        if (!dontShowHintsAgain && (isShowApprovalDemoHint(response.customerNumber)) && (sessionStorage.showDemoHints == undefined || sessionStorage.showDemoHints == null || sessionStorage.showDemoHints == "" || sessionStorage.showDemoHints == "on") && (sessionStorage.isApprovalDemoHintsDisplayed == undefined || sessionStorage.isApprovalDemoHintsDisplayed == null || sessionStorage.isApprovalDemoHintsDisplayed == "false")) {
            sessionStorage.isApprovalDemoHintsDisplayed = true;
            if (isNewProofGenerationCustomer(response.customerNumber)) {
                $('#approvalDemoHint').popup('close');
                $('#approvalDemoHint fieldset[data-role=controlgroup]').css('display', 'block');
                window.setTimeout(function () {
                    $('#approvalDemoHint').popup('open', { positionTo: '#dvProofFileName left' });
                }, 4000);
            }
            else {
                $('#markupAnnotations').popup('close');
                window.setTimeout(function () {
                    $('#markupAnnotations').popup('open', { positionTo: '#dvProofFileName left' });
                }, 4000);
            }
        }
    }, 2000);
    getEditJobData();
}

//Funtion to authorise the user and get the job info.  --- END ---

//Functions to load and manage job proofs -- START ---
function loadJobProofsList() {
    var visited_proofs = [];
    visited_proofs = (sessionStorage.visitedProofs != undefined && sessionStorage.visitedProofs != null) ? $.parseJSON(sessionStorage.visitedProofs) : [];

    var file_list = (proofBobDetails.myOrder.listOrderFileses != undefined && proofBobDetails.myOrder.listOrderFileses != null && proofBobDetails.myOrder.listOrderFileses.length > 0) ? proofBobDetails.myOrder.listOrderFileses : [];

    //if (gOutputData.standardizeAction != undefined && gOutputData.standardizeAction != null) {
    //    if (Object.keys(gOutputData.standardizeAction.standardizeFileList).length > 0) {
    //    }
    //}
    if (file_list.length > 0) {
        var un_approved_list = [];
        var approved_list = [];
        var created_date = "";
        var new_item = "";
        var temp_file_list = $.extend(true, {}, file_list);
        $.each(temp_file_list, function (key, val) {
            var proof_items = [];
            $.each(val.listOrderFileVersionses.reverse(), function (key1, val1) {
                created_date = ((val1.lastUpdateDate != undefined && val1.lastUpdateDate != null && val1.lastUpdateDate != "") ? val1.lastUpdateDate : "");
                if (key1 == 0 && val1.overallStatus == "1") {
                    new_item = {
                        "fileName": val1.origFileName,
                        "id": "lnk" + (val1.origFileName.substring(0, val1.origFileName.indexOf('.'))).replace(/ /g, '#'),
                        "createdDate": created_date
                    };

                    if (approved_list != undefined && approved_list != null) {
                        $.each(approved_list, function (a, b) {
                            proof_items = $.grep(b, function (obj) {
                                return obj.fileName.toLowerCase() == val1.origFileName.toLowerCase();
                            });
                            if (proof_items.length > 0) {
                                proof_items.push(new_item);
                                return false;
                            }

                        });
                    }
                    if (proof_items.length == 0)
                        proof_items.push(new_item);
                    if (Object.keys(proof_items).length == 1) {
                        approved_list.push(proof_items);
                    }
                    return false;
                }
                else {
                    if (key1 == 0) {
                        new_item = {
                            "fileName": val1.origFileName,
                            "id": "lnk" + (val1.origFileName.substring(0, val1.origFileName.indexOf('.'))).replace(/ /g, '#'),
                            "createdDate": created_date
                        };

                        if (un_approved_list != undefined && un_approved_list != null) {
                            $.each(un_approved_list, function (a, b) {
                                proof_items = $.grep(b, function (obj) {
                                    return obj.fileName.toLowerCase() == val1.origFileName.toLowerCase();
                                });
                                if (proof_items.length > 0) {
                                    proof_items.push(new_item);
                                    return false;
                                }

                            });
                        }
                        if (proof_items.length == 0)
                            proof_items.push(new_item);
                        if (Object.keys(proof_items).length == 1) {
                            un_approved_list.push(proof_items);
                        }
                    }
                    return false;
                }
            });

        });

        $('#ulJobProofs').empty();

        var download_proofs_links = "";
        download_proofs_links += '<li data-theme="a" data-icon="delete"><a href="#" data-rel="close" style="font-size:14px" onclick="$(\'#jobProofsPanel\').panel(\'close\');">Order Elements</a></li>';

        /* The below code has to be opend after the issue has been resolved.
        download_proofs_links += '<li data-theme="c" data-icon="myapp-download"><a id="aDownloadAll" style="font-size:12px;" onclick="openDownloadPopup(\'0\');">Download All Files</a></li>';
        download_proofs_links += '<li data-theme="c" data-icon="myapp-download-comments"><a id="aDownloadAnnotated" style="font-size:12px;"  onclick="openDownloadPopup(\'1\');">Download All Annotated PDFs</a></li>';*/
        $('#ulJobProofs').append(download_proofs_links);
        $('#ulJobProofs').append($("#unApprovedJobsTemplate").tmpl({ proofs: un_approved_list }));
        $('#ulJobProofs').append($("#approvedJobsTemplate").tmpl({ proofs: approved_list }));

        $.each(visited_proofs, function (key, val) {
            $('#ulJobProofs li a[id=lnk' + val.fileName.substring(0, val.fileName.indexOf('.')).replace(/ /g, '#') + ']').css('color', '#0033CC');
        });
        $('#ulJobProofs').listview('refresh');
        if (un_approved_list.length > 0)
            $('#btnJobProofs').buttonMarkup({ theme: 'h' });
        else if (un_approved_list.length == 0)
            $('#btnJobProofs').buttonMarkup({ theme: 'b' });
    }
}
function openDownloadPopup(type) {
    $('#txtDownloadProofsEmailTo').val('');
    var user_email_id = (sessionStorage.approvalUserEmail != undefined && sessionStorage.approvalUserEmail != null && sessionStorage.approvalUserEmail != "") ? sessionStorage.approvalUserEmail : "";
    $('#txtDownloadProofsEmailTo').val(user_email_id);
    $('#btnDownload').attr('onclick', 'downloadProofs(' + type + ')');
    $('#popupDownloadProofs').popup('open');
}

function validateMail(mail_id) {
    var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    if (!expr.test($.trim(mail_id)))
        return false;
    return true;
}

function validateMultipleEmails(emails) {
    var msg = "";
    var email_ids = emails;
    if (email_ids.indexOf(',') > -1) {
        email_ids = email_ids.split(',');
        $.each(email_ids, function (a, b) {
            if (!validateMail(b))
                msg = 'Invalid email address.';
        });
    }
    else {
        if (!validateMail(emails))
            msg = 'Invalid email address.';
    }

    return msg;
}

function forwardProofEmailsValidation(approval_mails, viewer_mails) {
    var msg = "";
    if (approval_mails == "" && viewer_mails == "")
        msg = 'Email address is mandatory to forward Proofs.';
    else {
        if (approval_mails != "")
            msg = validateMultipleEmails(approval_mails);
        if (msg == "" && viewer_mails != "")
            msg = validateMultipleEmails(viewer_mails);
    }
    if (msg != "") {
        $('#popupForward').popup('close');
        $('#alertmsg').text(msg);
        $('#okBut').attr('onclick', '$("#popupDialog").popup("close");');
        window.setTimeout(function () {
            $('#popupDialog').popup('open');
        }, 500);
        return false;
    }
    return true;
}

function emailValidation(email_id) {
    var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    var msg = "";
    if (email_id == "")
        msg = 'Email address is mandatory to download Proofs.';
    else {
        if (!expr.test($.trim(email_id)))
            msg = 'Invalid email address.';
    }
    $('#okBut').attr('onclick', '$("#popupDialog").popup("close");');
    if (msg != "") {
        $('#popupDownloadProofs').popup('close');
        $('#alertmsg').text(msg);
        window.setTimeout(function () { $('#popupDialog').popup('open'); }, 300);
        $('#okBut').attr('onclick', '$("#popupDialog").popup("close");$("#popupDownloadProofs").popup("open");');
        return false;
    }
    return true;
}
function downloadProofs(type) {
    if (!(emailValidation($("#txtDownloadProofsEmailTo").val()))) {
        return false;
    }
    if ($('#txtDownloadProofsEmailTo').val() != "") {
        $('#popupDownloadProofs').popup('close');
        $('#loadingText1').text('Please Wait..');
        $('#waitPopUp').popup('open');
        window.setTimeout(function () {
            var job_customer_number = (sessionStorage.jobCustomerNumber != undefined && sessionStorage.jobCustomerNumber != null && sessionStorage.jobCustomerNumber != "") ? sessionStorage.jobCustomerNumber : appPrivileges.customerNumber;
            var temp_job_number = jobNumber;//job_customer_number == SPORTSKING ? skApprovalStaticJobNumber : jobNumber;
            if (job_customer_number == SPORTSKING)
                temp_job_number = skApprovalStaticJobNumber;
            else if (job_customer_number == KUBOTA && sessionStorage.userRole == "user")
                temp_job_number = kubotaUserApprovalStaticJobNumber;

            var url = serviceURLDomain + "api/Proof_download/" + job_customer_number + "/" + facilityId + "/" + temp_job_number + "/" + $('#txtDownloadProofsEmailTo').val() + "/" + type;
            getCORS(url, null, function (data) {
                if (data == "success") {
                    $('#waitPopUp').popup('close');
                    $('#alertmsg').html("Proofs have been downloaded successfully");
                    $('#popupDownloadProofs').popup('close');
                    $('#okBut').attr('onclick', '$(\'#popupDialog\').popup(\'close\');');
                    $('#popupDialog').popup('open');
                }
            }, function () {
                $('#viewFrame')[0].contentWindow.document.body.innerHTML = "";

                $('#alertmsg').text('Failed to download proofs. Please contact Administrator.');
                $('#popupDownloadProofs').popup('close');
                $('#okBut').attr('onclick', '$(\'#popupDialog\').popup(\'close\');');
                $('#popupDialog').popup('open');
            });
        }, 1000);
    }
}
function viewJobProof(file_name, window_type) {
    var visited_proofs = [];
    visited_proofs = (sessionStorage.visitedProofs != undefined && sessionStorage.visitedProofs != null) ? $.parseJSON(sessionStorage.visitedProofs) : [];
    var query_string = "fileName=" + file_name + "&jobNumber=" + jobNumber + "&isJobProof=true";
    var new_url_string = urlString.substr(0, urlString.indexOf("?") + 1);
    if (new_url_string == "" && file_name == ("proof_" + jobNumber + ".pdf")) {
        return false;
    }
    new_url_string = new_url_string + query_string;

    var visited_file = $.grep(visited_proofs, function (obj) {
        return obj.fileName == file_name;
    });
    if (visited_file.length == 0)
        visited_proofs.push({
            "fileName": file_name
        });
    sessionStorage.visitedProofs = JSON.stringify(visited_proofs);
    if (sessionStorage.isOpenedFromEmail == "true")
        sessionStorage.approvalUserEmail = approvalUserEmail;
    if (window_type == "blank") {
        var selected_version = [];
        selected_version.push({
            "facilityId": proofBobDetails.facilityId,
            "fileName": file_name,
            "jobNumber": jobNumber,
            "proofCreatedDate": "",
            "proofUUID": "",
            "windowType": window_type
        });
        sessionStorage.selectedVersion = JSON.stringify(selected_version);
        $('#jobProofsPanel').panel('close', 'optionsHash');
        sessionStorage.isNewWindow = true;
        window.open(new_url_string);
    }
    else {
        sessionStorage.isNewWindow = false;
        window.location.href = new_url_string;
    }
}
function getInternetExplorerVersion() {
    var rv = -1; // Return value assumes failure.
    if (navigator.appVersion.indexOf('Trident') > -1 || navigator.appName == 'Microsoft Internet Explorer') {
        var ua = navigator.userAgent;
        var re = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
        var re1 = new RegExp("rv:([0-9]{1,}[\.0-9]{0,})");
        if (re.exec(ua) != null || re1.exec(ua) != null)
            rv = parseFloat(RegExp.$1);
    }
    return rv;
}
function checkVersion() {
    var ver = getInternetExplorerVersion();
    return (ver > -1) ? ((ver >= 9.0) ? true : false) : true;
}

function createConfirmDialog() {
    var msg_box = '';
    msg_box = '<div data-role="popup" id="popupConfirmDialog" data-overlay-theme="d" data-theme="c" style="max-width:400px;" class="ui-corner-all" data-history="false">';
    msg_box += '<div data-role="header" data-theme="d" class="ui-corner-top" id="dialogbox">';
    msg_box += '<h1>Confirm</h1>';
    msg_box += '</div>';
    msg_box += '<div data-role="content" data-theme="d" class="ui-corner-bottom ui-content">';
    msg_box += '<div id="confirmMsg"></div>';
    msg_box += '<div id="colId"></div>';
    msg_box += '<div id="valList"></div>';
    msg_box += '<a href="#" data-role="button" data-inline="true" data-theme="a" id="cancelButConfirm" data-mini="true">Cancel</a>';
    msg_box += '<a href="#" data-role="button" data-inline="true" data-theme="a" id="okButConfirm" data-mini="true">Continue</a>';
    msg_box += '</div></div>';
    $("#_jobApproval").append(msg_box);
}
//Functions to load and manage job proofs -- END ---
function getArtworkFileList() {
    if (((appPrivileges != undefined && appPrivileges != null && appPrivileges.customerNumber == SPORTSKING)
             || (sessionStorage.jobCustomerNumber != undefined && sessionStorage.jobCustomerNumber == SPORTSKING)) &&
                        (sk_ApprovalList != undefined && Object.keys(sk_ApprovalList).length > 0)) {
        return sk_ApprovalList.myOrder.listOrderFileses;
    } else {
        if (Object.keys(proofBobDetails).length > 0)
            return proofBobDetails.myOrder.listOrderFileses;
    }
    return {};
}

//funtion to load selected proofs for required approvals
function loadSelectedProofs() {
    var file_list = {};
    file_list = getArtworkFileList();

    if (Object.keys(file_list).length > 0) {
        var selected_proofs = "";
        var id = "";
        $.each(file_list, function (key, val) {
            $.each(val.listOrderFileVersionses, function (key1, val1) {
                id = val1.origFileName.replace(/ /g, '_').substring(0, val1.origFileName.lastIndexOf('.'));
                selected_proofs += '<input type="checkbox" name="chk' + id + '" id="chk' + id + '" data-theme="c" value="' + val1.origFileName + '" onchange="makeProofSelection(this,\'dvSelectedProofs\')" /><label for="chk' + id + '">' + val1.origFileName + '</label>';
            });
        });

        if (selected_proofs != "") {
            var proof_header = '<div data-role="collapsible-set" data-theme="a" data-content-theme="d" data-mini="true" style="padding-left: 8px; padding-right: 8px;">';
            proof_header += '<div data-role="collapsible" data-collapsed-icon="carat-r" data-expanded-icon="carat-d" data-theme="a" data-content-theme="d" data-collapsed="true">';
            proof_header += '<h3>Selected Proofs</h3><fieldset data-role="controlgroup" data-mini="true">';
            selected_proofs = proof_header + '<input type="checkbox" name="chkSelectAllProofs" id="chkSelectAllProofs" data-theme="a" onchange="selectAllProofs(this,\'dvSelectedProofs\')"/><label for="chkSelectAllProofs"> Select All Proofs</label>' + selected_proofs;
            selected_proofs += '</fieldset></div></div>';
            $('#dvSelectedProofs').empty();
            $('#dvSelectedProofs').append(selected_proofs);
            $('#dvSelectedProofs').find('div[data-role=collapsible-set]').collapsibleset().trigger('create');
            $('#dvSelectedProofs').css('display', 'none');
        }
    }
}
function makeProofSelection(ctrl, dv_type) {
    if ($('#' + ctrl.id).attr("readonly") == "readonly") {
        $('#' + ctrl.id).attr('checked', false).checkboxradio('refresh');
        return true;
    }
    var checked_proofs = $('#' + dv_type + ' input[type=checkbox]:checked:not([id^=chkSelectAllProofs])');
    if (!ctrl.checked) {
        $('#' + dv_type + ' input[type=checkbox][id^=chkSelectAllProofs]').attr('checked', false).checkboxradio('refresh');
        if (checked_proofs.length == 0)
            $('#chkRequireApprovals').attr('checked', false).checkboxradio('refresh');
    }
    else {
        var all_proofs = $('#' + dv_type + ' input[type=checkbox]:not([id^=chkSelectAllProofs]):not([readonly="readonly"])');
        if (all_proofs.length == checked_proofs.length)
            $('#' + dv_type + ' input[type=checkbox][id^=chkSelectAllProofs]').attr('checked', true).checkboxradio('refresh');
        if (dv_type.indexOf('Approve') == -1)
            $('#chkRequireApprovals').attr('checked', true).checkboxradio('refresh');
    }
}

function selectAllProofs(ctrl, dv_type) {
    if (ctrl.checked) {
        $('#' + dv_type + ' input[type=checkbox]:not([id^=chkSelectAllProofs]):not([readonly="readonly"])').attr('checked', true).checkboxradio('refresh');
        if (dv_type.indexOf('Approve') == -1)
            $('#chkRequireApprovals').attr('checked', true).checkboxradio('refresh');
    }
    else {
        $('#' + dv_type + ' input[type=checkbox]:not([id^=chkSelectAllProofs])').attr('checked', false).checkboxradio('refresh');
        if (dv_type.indexOf('Approve') == -1)
            $('#chkRequireApprovals').attr('checked', false).checkboxradio('refresh');
    }
}
function selectRequiredProofs(ctrl) {
    if (ctrl.checked) {
        $('#dvSelectedProofs').css('display', 'block');
        $('div.ui-collapsible-content', $('#dvSelectedProofs').find('div[data-role=collapsible-set]')).trigger('expand');
        $('#chkSelectAllProofs').attr('checked', true).checkboxradio('refresh').trigger('change');
    }
    else {
        $('#dvSelectedProofs').css('display', 'none');
        $('div.ui-collapsible-content', $('#dvSelectedProofs').find('div[data-role=collapsible-set]')).trigger('collapse');
        $('#chkSelectAllProofs').attr('checked', false).checkboxradio('refresh').trigger('change');
    }
}
String.prototype.initCap = function () {
    return this.toLowerCase().replace(/(?:^|\s)[a-z]/g, function (m) {
        return m.toUpperCase();
    });
};

function openNavPanel() {
    $('#navLinksPanel').panel('open', 'optionsHash');
}

function navLinksClick(path) {
    window.location.href = path;
}

function displayApproversInfo() {
    var file_list = {};

    file_list = (proofBobDetails.myOrder.listOrderFileses != undefined && proofBobDetails.myOrder.listOrderFileses != null && proofBobDetails.myOrder.listOrderFileses.length > 0) ? proofBobDetails.myOrder.listOrderFileses : [];
    
    var art_files = "";
    var rejected_count = 0;
    var temp_file_list = $.extend(true, {}, file_list);

    $.each(temp_file_list, function (key, val) {
        var approval_icon = "";
        if (val.overallStatus == "0")
            rejected_count = 1;
        var approval_status = "";
        
        $.each(val.listOrderFileVersionses.reverse(), function (key1, val1) {
            if (key1 == 0) {
                if (val1.overallStatus == undefined && val1.overallStatus == null);
                switch (val1.overallStatus) {
                    case 6:
                        approval_icon = "icon_approved_with_changes.png";
                        break;
                    case 4:
                        approval_icon = "icon_pending.png";
                        break;
                    case 1:
                        approval_icon = "icon_approved.png";
                        break;
                    case 0:
                        approval_icon = "icon_rejected.png";
                        break;
                    default:
                        break;
                }
                approval_status = getApprovalStatus(parseInt(val1.overallStatus));
                art_files += '<li data-icon="false" data-theme="c"><img src="../images/' + approval_icon + '" title="' + approval_status + '" class="ui-li-icon ui-corner-none"><h3 class="word_wrap" title="' + val1.origFileName + '" style="cursor:default;margin-top:0em;">' + val1.origFileName + '</h3><p style="cursor:default" class="word_wrap">';
                if (val1.listOrderFileVersionApprovals.length > 0)
                    art_files += getApproversList(val1);//val1
                art_files += (val.approvalsNeeded != undefined && val.approvalsNeeded != null && val.approvalsNeeded != "") ? "<br /> Approvals Needed:" + val.approvalsNeeded : "";
                art_files += '</p></li>';
            }
        });
    });
    if (art_files != '') {
        art_files = '<li data-role="list-divider" data-theme="f">Artwork<a onclick="openApprovalInfoPopup();" data-rel="popup" data-position-to="window" data-transition="pop" class="ui-icon ui-icon-info" style="color:white;float:right;margin-right: -2px;"></a></li>' + art_files;
    }
    $('#ulApprovers').empty();
    $('#ulApprovers').append(art_files).listview('refresh');
    $('#chkApproveJobArtwork').attr('checked', false).checkboxradio('refresh');
    if (jobCustomerNumber == DCA || jobCustomerNumber == SPORTSKING) {
        $('#btnApproverSubmit').css('display', 'none');
        $('#btnApproverCancel').text('OK');
    }

    if (rejected_count > 0)
        $('#btnApprovers').buttonMarkup({ theme: 'h' });
    else if (rejected_count == 0)
        $('#btnApprovers').buttonMarkup({ theme: 'p' });

}
function openApprovalInfoPopup() {
    $('#popupApprovers').popup('close');
    $('#popupUploadedInfo').popup('open', { positionTo: "#btnApprovers" });
}
//Generates the Approvers and Viewers list
function getApproversList(artwork_obj) {
    var approvers_list = "";
    var viewers_list = "";
    if (artwork_obj.listOrderFileVersionApprovals != undefined && artwork_obj.listOrderFileVersionApprovals != null && artwork_obj.listOrderFileVersionApprovals.length > 0) {
        $.each(artwork_obj.listOrderFileVersionApprovals, function (key, val) {
            if (val.approvalLevel == 1)
                approvers_list += (approvers_list != "") ? ',<br /> ' + val.approvalEmail + ' is ' + getApprovalStatus(parseInt(val.approvalStatus)) : '<b>Approvers:</b><br /> ' + val.approvalEmail + ' is ' + getApprovalStatus(parseInt(val.approvalStatus));
            else if (val.approvalLevel == 0) {
                if (val.approvalEmail != undefined && val.approvalEmail != null && val.approvalEmail != "")
                    viewers_list += (viewers_list != "") ? ',<br /> ' + val.approvalEmail : '<b>Viewers:</b><br /> ' + val.approvalEmail;
            }
        });
    }
    var view_approval_history = '<br /><br /><a href="#" onclick="openApprovalHistory(\'' + artwork_obj + '\');">View Approval History</a>';
    return ((approvers_list != "") ? approvers_list + ((sessionStorage.userRole == "admin") ? view_approval_history : "") + '<br />' : "") + ((viewers_list != "") ? viewers_list : "");
}

function openApprovalHistory() {
    $('#popupApprovers').popup('close');

    $('#popupApprovalHistory h4').html(fileName);

    var proofing_file_versions_list = (sessionStorage.proofingFileVersionList != undefined && sessionStorage.proofingFileVersionList != null && sessionStorage.proofingFileVersionList != "") ? jQuery.parseJSON(sessionStorage.proofingFileVersionList) : [];

    var history = "";

    $.each(proofing_file_versions_list, function (key, val) {
        $.each(val.listOrderFileVersionApprovals, function (key1, val1) {
            history += '<tr class="th-groups" data-theme="f"><td>' + val1.approvalEmail + '</td><td>' + val1.lastUpdateDate + '</td><td>' + getApprovalStatus(parseInt(val1.approvalStatus)) + '</td><td>' + ((val1.notes != undefined && val1.notes != null) ? val1.notes : "") + '</td></tr>';
        });
    });

    $('#tblApprovalHistoryBody').empty().html(history);

    window.setTimeout(function () {
        $('#popupApprovalHistory').popup('open', { positionTo: "#btnApprovers" });
    }, 500);
}

function closeApprovalHistory() {
    $('#popupApprovalHistory').popup('close');
    window.setTimeout(function () {
        $('#popupApprovers').popup('open', { positionTo: "#btnApprovers" });
    }, 500);
}

function approvalsChanged(ctrl) {
    if (ctrl.value == 1)
        $('#dvReasonForRejection').css('display', 'none');
    else if (ctrl.value == 0) {
        $('#dvReasonForRejection').css('display', 'block');
        $('#dvReasonForRejection').css('display', '');
    }

}

function setDemosProof() {
    if (appPrivileges != undefined && appPrivileges != null) {
        if (appPrivileges.customerNumber == ALLIED || appPrivileges.customerNumber == AAG) {
            sessionStorage.authString = "Basic YWRtaW5fY3cxOmFkbWluX2N3MQ==";
            facilityId = "1";
            jobCustomerNumber = CW;
            sessionStorage.jobCustomerNumber = CW;
        }

        if (appPrivileges.customerNumber == ALLIED) {
            encodedFileName = "58683_GWBerkheimerOLSONCOMFORT.pdf";
            proofJobNumber = "-16288";
        }
        else if (appPrivileges.customerNumber == SPORTSKING) {
            proofJobNumber = skApprovalStaticJobNumber;
        }
        else if (appPrivileges.customerNumber == KUBOTA && appPrivileges.roleName == "user") {
            proofJobNumber = kubotaUserApprovalStaticJobNumber;
        }
        else if (appPrivileges.customerNumber == AAG) {
            encodedFileName = "TCA_10232_AAG_Lunch_Learn_2_Presenters-2_Events_Lenfers_Culbreth_Let.pdf";
            proofJobNumber = "-15565";
        }
        else {
            $('#dvMsg').html('This order has not been approved. Do you want to Continue without approvals?');
        }
        $('#confirmAdmin').popup('open', { positionTo: 'window' });
        return false;
    }
}
Number.prototype.padLeft = function (base, chr) {
    var len = (String(base || 10).length - String(this).length) + 1;
    return len > 0 ? new Array(len).join(chr || '0') + this : this;
};
String.prototype.initCap = function () {
    return this.toLowerCase().replace(/(?:^|\s)[a-z]/g, function (m) {
        return m.toUpperCase();
    });
};