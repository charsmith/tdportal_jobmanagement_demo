﻿isSendTestEmailWithProofUUID = function () {
    var customer = getCustomerNumber();
    return ([DKI, ICYNENE].indexOf(customer) > -1);
}

hideApproverSubmit = function () {
    var customer = getCustomerNumber();
    return ([DKI, ICYNENE].indexOf(customer) > -1);
}

isBindCheckoutDataVerified = function () {
    var customer = getCustomerNumber();
    return ([
            DKI,
            ICYNENE,
            DCA,
            KUBOTA,
            SPORTSKING,
            GWA,
            OH,
            CASEYS,
            SCOTTS,
            CW,
            EQUINOX,
            ACE,
            ALLIED,
            ANDERSEN,
            SHERWINWILLIAMS,
            MILWAUKEE,
            BENJAMINMOORE,
            TRAEGER
    ].indexOf(customer) > -1);
}

hideListSummaryApproval = function () {
    return true;
}

hideForwardProof = function () {
    var hide_forward_proof = false;
    var customer = getCustomerNumber();
    var role = getUserRole();

    switch (role) {
        case 'admin': break;
        case 'power': break;
        case 'user':
            hide_forward_proof = ([SCOTTS, ACE, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);
            break;
        default: break;
    }

    return hide_forward_proof;
}

showArtApproval = function () {
    var customer = getCustomerNumber();
    return ([SCOTTS, ACE, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);
}

isNewProofGenerationCustomers = function () {
    var customer = getCustomerNumber();
    return ([SCOTTS, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);
}

jobTicketVerificationRequired = function () {
    var customer = getCustomerNumber();
    return ([SCOTTS, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER, ACE].indexOf(customer) > -1);
}

isRequiredToShowNoArtworksAvailable = function () {
    var customer = getCustomerNumber();
    var role = getUserRole();
    return (([SPORTSKING].indexOf(customer) > -1 && role == "admin") || [SPORTSKING, ALLIED, SCOTTS, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER, ACE].indexOf(customer) == -1);
}

showApprovalDemoHint = function () {
    var customer = getCustomerNumber();
    return ([DCA, KUBOTA, SPORTSKING, GWA, OH, SCOTTS, ACE, ANDERSEN, SHERWINWILLIAMS, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);
}

showApproversProofStatusDemoHint = function () {
    var customer = getCustomerNumber();
    return ([SCOTTS, ACE, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);
}

hideJobProofs = function () {
    var customer = getCustomerNumber();
    return ([SCOTTS, ACE, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);
}

isClosedJobVerficationRequired = function (customer_number) {
    return ([SCOTTS, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer_number) > -1);
}

isShowApprovalDemoHint = function (customer_number) {
    return ([DCA, KUBOTA, SPORTSKING, GWA, SCOTTS, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer_number) > -1);
}

isNewProofGenerationCustomer = function (customer_number) {
    return ([SCOTTS, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer_number) > -1);
}