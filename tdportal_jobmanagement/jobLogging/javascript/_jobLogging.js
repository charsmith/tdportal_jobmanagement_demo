﻿jQuery.support.cors = true;

//******************** Global Variables Start **************************
//var gLogListUrl = serviceURLDomain + 'api/Logging/' + sessionStorage.facilityId + '/' + jobCustomerNumber + '/' + sessionStorage.jobNumber;
//var gLogListUrl = serviceURLDomain + 'api/Logging/' + sessionStorage.facilityId + '/153009/403745';
var gLogListUrl = serviceURLDomain + 'api/Logging/' + sessionStorage.facilityId + '/' + jobCustomerNumber + '/' + sessionStorage.jobNumber;
var gLogDetailsUrl = 'JSON/_logDetails.JSON';
var pageObj;

var jobDesc = '';
jobDesc = getSessionData("jobDesc");
//******************** Global Variables End **************************
function buildNavigation() {
    if (getCustomerNumber() == REDPLUM) {
        displayNavLinks();
    }
    else {
        var display_navlinks = '';
        display_navlinks += '<li data-theme="a" data-icon="carat-l" id="liCloseMenu"><a onclick="closeNavPanel();" >Close Menu</a></li>';
        display_navlinks += '<li data-icon="home"><a href="#" data-bind="event:{click:$root.showHomePage}">Home</a></li>';
        display_navlinks += '<li data-icon="carat-l"><a href="#" data-bind="event:{click:$root.showJobSummary}">Summary</a></li>';
        display_navlinks += '<li><a data-bind="event:{click:$root.redirectToPages}">Notifications</a></li>';
        display_navlinks += '<li><a data-bind="event:{click:$root.redirectToPages}">Reporting</a></li>';
        display_navlinks += '<li data-theme="d"><a data-bind="event:{click:$root.redirectToPages}">Logging</a></li>';
        $("#ulJobLinks").empty();
        $("#ulJobLinks").append(display_navlinks);
    }
}

//******************** Page Load Events Start **************************
$('#_jobLogging').live('pagebeforecreate', function (event) {
    ko.setTemplateEngine(new ko.nativeTemplateEngine);
    buildNavigation();
    pageObj = new logDetails();

    var neg_job_number = getSessionData("negJobNumber");
    var header_text = jobDesc + ' | ' + sessionStorage.jobNumber;
    if (neg_job_number != undefined && neg_job_number != null && neg_job_number != "")
        header_text += ' (' + neg_job_number + ')';
    $('#navHeader').html('<h4 style="margin-top:10px !important;margin-bottom:10px !important;">' + header_text + '</h4>');

    displayMessage('_jobLogging');

    $('#jobListInfo').css('display', 'none');
    pageObj.getLoggingData();

    //test for mobility...
    //loadMobility();
    $("#navLinksPanel").on("panelopen", function (event, ui) {
        sessionStorage.isNavPanelOpen = true;
    });
});

$('#_jobLogging').live('pageshow', function (event) {
    //window.setTimeout(function makeDelayLoad() {
        pageObj.loadLoggingInfo();
        persistNavPanelState();
    //}, 700);
});

//******************** Page Load Events End **************************
//******************** Public Functions Start **************************
var logDetails = function () {
    var self = this;
    self.gLogginData = [];
    self.gLogDetailsData = [];
    self.gOutputData = [];
    self.logging = ko.observableArray(self.gLogginData);
    self.selectedItems = ko.observable("1");
    self.listJobDetails = ko.observableArray([]);
    //This function will display details info for selected logging
    self.setJobDetailsInfo = function (event_id, current) {
        self.listJobDetails([]);
        getCORS(gLogListUrl + '/' + event_id + '/0/' + current, null, function (data) {
            $.each(data, function (a, b) {
                for (i = 0; i < b.length; i++) {
                    self.listJobDetails.push(b[i]);
                }
            });
            $('#jobListInfo').css('display', 'block');
            $("div ul").each(function (I) {
                $(this).listview();
            });
            $("#jobListInfo").collapsibleset('refresh');

            $('#jobListInfo').trigger('create');
            $('.ui-page').trigger('create').listview().listview("refresh");

        }, function (error_response) {
            showErrorResponseText(error_response,true);
        });
        if (sessionStorage.jobSetupOutput != undefined) {
            self.gOutputData = jQuery.parseJSON(sessionStorage.jobSetupOutput);
            getBubbleCounts(self.gOutputData);
        }
    }

    //This function will execute when logging dropdown changed
    self.loggingChanged = function (event, data) {
        var search_data;
        self.setJobDetailsInfo(self.selectedItems()[0], 0);
    }

    //This function will provide logging info
    self.getLoggingData = function () {
        //api/Logging/{facility_id}/{customer_number}/{job_number}/{event_id}/{is headers required}/{is most current entry}
        getCORS(gLogListUrl + '/0/1/0', null, function (data) {
            $.each(data, function (a, b) {
                self.gLogginData.push(b[0]);
            });
        }, function (error_response) {
            showErrorResponseText(error_response,true);
        });
    }

    //This function will load default logging information
    self.loadLoggingInfo = function () {
        var logging = [];
        self.setJobDetailsInfo(0, 1);
        ko.applyBindings(pageObj);
        $('#jobListInfo').css('display', 'block');
        $("div ul").each(function (I) {
            $(this).listview();
        });
        $("#jobListInfo").collapsibleset('refresh');

        $('#jobListInfo').trigger('create');
        $('.ui-page').trigger('create').listview().listview("refresh");

    }
    //Rediret to System Dashboard.
    self.redirectToPages = function (data, e) {
        var ele = e.target || e.targetElement;
        switch ($(ele).text()) {
            case "Notifications":
                window.setTimeout(function () {
                    window.location.href = '../jobNotifications/jobNotifications.html';
                }, 500);
                break;
            case "Logging":
                window.setTimeout(function () {
                    window.location.href = '../jobLogging/jobLogging.html';
                }, 500);
                break;
            case "Reporting":
                window.setTimeout(function () {
                    window.location.href = '../jobReporting/jobReporting.html';
                }, 500);
                break;
            default:
                break;
        }
    };
    self.showHomePage = function () {
        //Need to call the function
        redirectToIndexPage();
    }
    self.showJobSummary = function () {
        //Need to call the function
        redirectToSingleJobPage();
    }
}
//******************** Public Functions End **************************