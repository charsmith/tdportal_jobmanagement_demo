﻿var selectTemplate = function () {

    //******************** Global Variables Start **************************
    var self = this;
    self.gData;
    self.gOutputData;
    self.gCustomerData;
    self.templateData;
    self.selectedTemplate;
    self.pagePrefs = "";
    self.filter_customer = [];
    self.isValidJobName = true;
    //******************** Global Variables End **************************
    //******************** Public Functions Start **************************

    //Gets the page information like templates, job ticket object etc and binds to page.
    self.bindTemplates = function () {
        if (sessionStorage.jobSetupLoadObject == undefined && sessionStorage.jobSetupLoadObject == undefined) {
            self.makeGData();
        }
        else {
            if (sessionStorage.jobSetupLoadObject != undefined) {
                self.gData = jQuery.parseJSON(sessionStorage.jobSetupLoadObject);
                var is_found = false;
                $.each(self.gData, function (key, val) {
                    if (val.list == "template") {
                        is_found = true;
                        return false;
                    }
                });

                if (!is_found) {
                    self.gData.push({
                        "list": "template",
                        "items": jQuery.parseJSON(sessionStorage.templates)
                    });
                }
                if (!showTemplateApproversList())
                    self.buildLists();

                if (sessionStorage.jobSetupOutput != undefined) {
                    self.gOutputData = jQuery.parseJSON(sessionStorage.jobSetupOutput);
                    if (sessionStorage.selectedCustomerInfo != undefined && sessionStorage.selectedCustomerInfo != null && sessionStorage.selectedCustomerInfo != "") {
                        var selected_cust_info = jQuery.parseJSON(sessionStorage.selectedCustomerInfo);
                        self.gOutputData.customerNumber = selected_cust_info.customerNumber;
                        self.gOutputData.customerName = selected_cust_info.customerName;
                    }
                    if (jobCustomerNumber == CASEYS || jobCustomerNumber == JETS || jobCustomerNumber == BUFFALONEWS || jobCustomerNumber == PIONEERPRESS || jobCustomerNumber == REDPLUM || jobCustomerNumber == EQUINOX || isUpdateExpiryDate()) {
                        if (self.gOutputData != undefined && self.gOutputData != null && self.gOutputData.expirationDate == undefined)
                            if (jobCustomerNumber == JETS)
                                self.gOutputData["expirationDate"] = "01/01/2016";
                            else
                                self.gOutputData["expirationDate"] = "";

                        if (self.gOutputData != undefined && self.gOutputData != null && self.gOutputData.listSelection == undefined)
                            self.gOutputData["listSelection"] = "";
                    }
                    self.buildOutputLists();
                }
                else {
                    self.makeGOutputData();
                }

                if (showTemplateApproversList())
                    self.setProductType();
            }
        }
    };

    //Loads the page information.
    self.loadData = function () {
        getNextPageInOrder();
        self.pagePrefs = jQuery.parseJSON(sessionStorage.selectTemplatePrefs);
        if (!fnVerifyScriptBlockDict(self.pagePrefs))
            return false;
        if (sessionStorage.jobNumber != "-1") {
            if (isDisableJobName()) {
                $('#txtJobName').addClass('ui-disabled');
                $('#txtJobName').css('opacity', '0.6');
                $('#txtJobName')[0].disabled = true;
                $('#txtJobName').css('filter', 'Alpha(Opacity=90) !important');
                if (isEmailLogin() && $('#inHomeDate').length > 0) {
                    $('#inHomeDate')[0].disabled = true;
                    $('#inHomeDate').addClass('ui-disabled');
                    $('#inHomeDate').css('opacity', '0.6');
                    $('#inHomeDate').css('filter', 'Alpha(Opacity=90) !important');
                }
            }
        } else if (isEmailLogin()) {
            if (params.length > 0 && queryString != "") {
                $('#txtJobName').addClass('ui-disabled');
                $('#txtJobName')[0].disabled = true;
                $('#txtJobName').css('opacity', '0.6');
                $('#txtJobName').css('filter', 'Alpha(Opacity=90) !important');
            }
            $('#inHomeDate').addClass('ui-disabled');
            if ($('#inHomeDate').length > 0)
                $('#inHomeDate')[0].disabled = true;
            $('#inHomeDate').css('opacity', '0.6');
            $('#inHomeDate').css('filter', 'Alpha(Opacity=90) !important');
        }

        var file_data = (self.gOutputData != undefined && self.gOutputData.uploadList != undefined) ? self.gOutputData.uploadList : [];

        var cust_no = "null";
        if (sessionStorage.selectedCustomerInfo != undefined && sessionStorage.selectedCustomerInfo != null && sessionStorage.selectedCustomerInfo != "")
            cust_no = jobCustomerNumber;
        else
            cust_no = jobCustomerNumber;
        if (jobCustomerNumber == ALLIED)
            getTemplatePackageInfo();

        window.setTimeout(function () {
            self.getTemplatesList(cust_no);
        }, 500);

    };

    //Gets the templates list from server loads.
    self.getTemplatesList = function (customer_number) {
        var is_pricing_enabled = (jobCustomerNumber == KUBOTA || jobCustomerNumber == OH || jobCustomerNumber == BUFFALONEWS || jobCustomerNumber == PIONEERPRESS || jobCustomerNumber == REDPLUM || jobCustomerNumber == GWA) ? "0" : "1"; //(jobCustomerNumber == CW || jobCustomerNumber == SPORTSKING || jobCustomerNumber == OH || jobCustomerNumber == AAG || jobCustomerNumber == TRIBUNEPUBLISHING) ? "1" : "0";
        var templates_list_url = gTemplateUrlForAll + "/" + sessionStorage.facilityId + "/" + customer_number + "/" + is_pricing_enabled;

        getCORS(templates_list_url, null, function (data) {
            // if (jobCustomerNumber == SCOTTS && sessionStorage.userRole == "admin") {
            // data[data.length] = {
            // "text": "Custom Artwork – 5.5 x 10.5 Postcard",
            // "value": "_ct1_customArtwork_55x105",
            // "thumbnail": "t0_thumb.png",
            // "pricingList": [{
            // "minCount": "1",
            // "maxCount": "",
            // "value": "$0.310"
            // }]
            // };
            // }
            /*if (jobCustomerNumber == ACE) {
                            data[0] = {
                                "text": "ACE Brands 5.5 x 10.5 Postcard",
                                "value": "_ace_55x105",
                                "thumbnail": "",
                                "pricingList": [{
                                    "minCount": "1",
                                    "maxCount": "",
                                    "value": "$0.310"
                                }]
                            };
                        }*/
            sessionStorage.templates = JSON.stringify(data);
            self.bindTemplates();
        }, function (error_response) {
            showErrorResponseText(error_response, true);
        });
    };

    //Gets static template data incase service fails to get the info.
    self.makeGData = function () {
        $.getJSON(gServiceUrl, function (data) {
            self.gData = data;
            self.gData.push({
                "list": "template",
                "items": jQuery.parseJSON(sessionStorage.templates)
            });

            if (sessionStorage.jobSetupOutput == undefined) {
                self.makeGOutputData();
            }
            else {
                self.gOutputData = jQuery.parseJSON(sessionStorage.jobSetupOutput);
                if (jobCustomerNumber == CASEYS || jobCustomerNumber == JETS || jobCustomerNumber == BUFFALONEWS || jobCustomerNumber == PIONEERPRESS || jobCustomerNumber == REDPLUM || jobCustomerNumber == EQUINOX || isUpdateExpiryDate()) {
                    if (self.gOutputData != undefined && self.gOutputData != null && self.gOutputData.expirationDate == undefined)
                        if (jobCustomerNumber == JETS)
                            self.gOutputData["expirationDate"] = "11/30/2015";
                        else
                            self.gOutputData["expirationDate"] = "";

                    if (self.gOutputData != undefined && self.gOutputData != null && self.gOutputData.listSelection == undefined)
                        self.gOutputData["listSelection"] = "";
                }
                if (!showTemplateApproversList())
                    self.buildLists();
                else
                    self.setProductType();

                self.buildOutputLists();
            }
        });
    };

    self.setProductType = function () {
        var templt_name = ((jobCustomerNumber == KUBOTA) ? "combo" : "select");
        if ((sessionStorage.jobNumber != "-1") && ($("#divProductType").css('display') == 'block')) {
            templt_name = self.gOutputData.templateName.substring(self.gOutputData.templateName.indexOf('_', 1) + 1);
            templt_name = templt_name.substring(0, templt_name.indexOf('_'));
        }
        $('#ddlProductType').val(templt_name).selectmenu('refresh').trigger('change');
    }

    //Loads static job ticket object into local session incase of failure to get data from service.
    self.makeGOutputData = function () {
        if (sessionStorage.jobSetupOutput == undefined || sessionStorage.jobSetupOutput == null) {
            window.setTimeout(function delay() {
                $.getJSON(gOutputServiceUrl, function (dataOutput) {
                    self.gOutputData = dataOutput;
                    if (self.gOutputData["milestoneAction"] == undefined) self.gOutputData["milestoneAction"] = {};
                    if (jobCustomerNumber == CASEYS || jobCustomerNumber == JETS || jobCustomerNumber == BUFFALONEWS || jobCustomerNumber == PIONEERPRESS || jobCustomerNumber == REDPLUM || jobCustomerNumber == EQUINOX || isClearExpiryDate()) {
                        self.gOutputData["expirationDate"] = "";
                    }
                    self.gOutputData.milestoneAction["milestoneDict"] = {};
                    sessionStorage.jobSetupOutput = JSON.stringify(self.gOutputData);
                    if (!showTemplateApproversList())
                        self.buildLists();
                    else
                        self.setProductType();

                    self.buildOutputLists();
                });
            }, 500);
        }
    };

    self.IsValidImageUrl = function (url) {
        $("<img>", {
            src: url,
            error: function () { alert(url + ': ' + false); },
            load: function () { alert(url + ': ' + true); }
        });
    };

    self.getDefaultImage = function (ctrl) {
        var url = ctrl.src;
        ctrl.src = url.substring(0, url.lastIndexOf('/') + 1) + 'defaultThumbnail.png';
    };

    //Creates the template item and appends to UI list.
    self.makeTemplate = function (name, text, thumbnail) {
        var displayThumbImage = '';
        var template_item = '';
        var cust_folder_name = (jobCustomerName.indexOf('Casey') > -1) ? 'Caseys' : ((jobCustomerName.indexOf('Regis') > -1) ? 'Regis' : ((jobCustomerName.indexOf('Scotts') > -1) ? 'Scotts' : ((jobCustomerName.toLowerCase().indexOf("conversion alliance") > -1) ? "conversionAlliance" : ((jobCustomerName.toLowerCase().indexOf("bob") > -1) ? "bobs" : ((jobCustomerName.toLowerCase().indexOf("bright") > -1) ? "brighthouse" : ((jobCustomerName.toLowerCase().indexOf("walgreens") > -1) ? "walgreens" : ((jobCustomerName.toLowerCase().indexOf("suddenlink") > -1) ? "suddenlink" : jobCustomerName.replace(/ /g, '').replace(/`/g, '').replace(/'/g, ''))))))));
        cust_folder_name = (cust_folder_name.indexOf('SanDiego') > -1) ? 'SanDiegoUT' : cust_folder_name;
        cust_folder_name = (cust_folder_name.toLowerCase().indexOf('andersen') > -1) ? "Andersen" : cust_folder_name;
        cust_folder_name = (cust_folder_name.toLowerCase().indexOf('sherwin') > -1) ? "SherwinWilliams" : cust_folder_name;

        if ((text.toLowerCase() != "custom" && text.toLowerCase() != "other") || sessionStorage.userRole == "admin") {
            displayThumbImage = '<img style="cursor:pointer;" src="' + logoPath + 'jobThumbnails/' + cust_folder_name + '/' + thumbnail + '" onerror="this.onerror=null;pageObj.getDefaultImage(this);"/>';
        }

        if (jobCustomerNumber == JETS || jobCustomerNumber == BUFFALONEWS || jobCustomerNumber == PIONEERPRESS || jobCustomerNumber == REDPLUM) { // casey's        
            template_item = '<li onclick="pageObj.onListClick(\'' + name.replace(/\b\.\b/g, "_") + '\',event)"><input name="radio-choice" id="' + name.replace(/\b\.\b/g, "_") + '" type="radio" value="' + name + '"  data-theme="d">';
            template_item += '<label for="' + name.replace(/\b\.\b/g, "_") + '">';
            template_item += '<div class="ui-grid-a ui-responsive">';
            template_item += '<div class="ui-block-a" style="width:25%" align="right">' + displayThumbImage + '</div>';
            template_item += '<div class="ui-block-b" style="width:75%"><div class="ui-bar " style="padding-top:24px;">' + text + '<br /></div>';
            template_item += '</div></div></label></li>';
            $("#template").append(template_item);
        }
        else {
            if (displayThumbImage != "") {
                template_item = '<li data-theme="f" onclick="pageObj.onListClick(\'' + name.replace(/\b\.\b/g, "_") + '\',event)"><input name="radio-choice" id="' + name.replace(/\b\.\b/g, "_") + '" type="radio" value="' + name + '"  data-theme="d">';

                template_item += '<label for="' + name.replace(/\b\.\b/g, "_") + '">';
                template_item += '<div class="ui-grid-a ui-responsive">';
                if (isDisplayThumbImageInCenter()) {
                    template_item += '<div class="ui-block-a" align="center">' + displayThumbImage + '</div>';
                }
                else {
                    template_item += '<div class="ui-block-a" style="width:25%" align="right">' + displayThumbImage + '</div>';
                }

                if (isBronzeGoldPackageCustomer() && packageInfo != undefined && packageInfo != null && packageInfo != "") {
                    template_item += '<div class="ui-block-b" style="width:75%"><div class="ui-bar"><b>' + text;
                    $.each(packageInfo, function (key, val) {
                        text = text.toLowerCase().replace(' ', '');
                        if (key.toLowerCase() == "bronzepackage" && text == "bronzepackage") {
                            template_item += ' - ' + val.totalInvest + '</b>' + '<br /><b>Quantity: ' + val.qty + '</b><br /><b>Includes: ' + val.include + '</b></div>';
                            return false;
                        }
                        if (key.toLowerCase() == "goldpackage" && text == "goldpackage") {
                            template_item += ' - ' + val.totalInvest + '</b>' + '<br /><b>Quantity: ' + val.qty + '</b><br /><b>Includes: ' + val.include + '</b></div>';
                            return false;
                        }
                        if (key.toLowerCase() == "platinumpackage" && text == "platinumpackage") {
                            template_item += ' - ' + val.totalInvest + '</b>' + '<br /><b>Quantity: ' + val.qty + '</b><br /><b>Includes: ' + val.include + '</b></div>';
                            return false;
                        }
                    });
                }
                else {
                    if (pricingListInfo != undefined && Object.keys(pricingListInfo).length > 0) {
                        if (isDisplayThumbImageInCenter()) {
                            template_item += '<div class="ui-block-b"><div style="padding-left:20px;" class="ui-bar word_wrap">' + text + '<br /></div>';
                        }
                        else {
                            template_item += '<div class="ui-block-b" style="width:75%"><div  style="padding-left:20px;" class="ui-bar word_wrap">' + text + '<br /></div>';
                        }

                        if (Object.keys(pricingListInfo).length == 1) {
                            template_item += '<table style="padding-left:16px;padding-top:0px;font-size:small;text-align:middle;">'
                            $.each(pricingListInfo, function (key, val) {
                                template_item += "<tr>";
                                template_item += "<td align='left'>" + val.value + ' each' + "</td>";
                                return false;
                            });
                            if (isDisplayThumbImageInCenter()) {
                                template_item += '</tr><tr><td align="left" style="padding:0px 0px 0px 0px"><a data-role="button" data-theme="a" href="#" data-mini="true" style="margin:0px 0px 0px 0px" onclick="pageObj.viewTemplatePreview(\'' + name.replace(/\b\.\b/g, "_") + '\',event);" class="ui-link ui-btn ui-btn-a ui-btn-inline ui-shadow ui-corner-all ui-mini">View PDF</a></td/tr>';
                            }
                        }
                        else {
                            template_item += '<table style="padding-left:20px;padding-top:0px;font-size:small;text-align:middle;">'
                            $.each(pricingListInfo, function (key, val) {
                                if ((jobCustomerNumber == CW && key > 0) || (jobCustomerNumber != CW)) {
                                    template_item += "<tr>";
                                    template_item += "<td align='right'>" + val.minCount + ((val.maxCount == '') ? '+' : '') + "</td>";
                                    template_item += "<td>" + ((val.maxCount != '') ? ' - ' : '') + "</td>";
                                    template_item += "<td align='right'>" + val.maxCount + "</td>";
                                    template_item += "<td align='right'>&nbsp;&nbsp;" + val.value + ' each' + "</td>";
                                    template_item += "</tr>";
                                }
                            });
                            if (isDisplayThumbImageInCenter()) {
                                template_item += '<tr><td align="left" colspan="4" style="padding:0px 0px 0px 0px"><a data-role="button" data-theme="a" data-mini="true" href="#" style="margin:0px 0px 0px 0px" onclick="pageObj.viewTemplatePreview(\'' + name.replace(/\b\.\b/g, "_") + '\',event);" class="ui-link ui-btn ui-btn-a ui-btn-inline ui-shadow ui-corner-all ui-mini">View PDF</a></td></tr>';
                            }
                        }
                        template_item += '</table>';
                        if (jobCustomerNumber == CW) {
                            template_item += '<table style="padding-left:20px;padding-top:0px;font-size:small;text-align:middle;"><tr><td align="left" colspan="3" style="padding:0px 0px 0px 0px">Prices shown do not included shipping cost for standard class mail.</td></tr></table>';
                        }
                    }
                    else
                        template_item += '<div class="ui-block-b" style="width:75%"><div class="ui-bar " style="padding-top:24px;">' + text + '<br /></div>';
                }
                template_item += '</div></div></label></li>';
                $("#template").append(template_item);
            }
        }
        var templates = jQuery.parseJSON(sessionStorage.templates);
        if (templates.length == 1) {
            self.onListClick(name.replace(/\b\.\b/g, "_"));
        }
    };
    var isPreviewClicked = false;
    self.viewTemplatePreview = function (ctrl, event) {
        event.cancelBubble = true;
        event.stopPropagation();
        var pdf_name = ctrl.replace(/\b\.\b/g, "_");
        var preview_folder_path = "";
        switch (jobCustomerNumber) {
            case ACE:
                preview_folder_path = "ACE";
                break;
            case ANDERSEN:
                preview_folder_path = "Andersen";
                break;
            case SHERWINWILLIAMS:
                preview_folder_path = "SherwinWilliams";
                break;
            case MILWAUKEE:
                preview_folder_path = "Milwaukee";
                break;
            case BENJAMINMOORE:
                preview_folder_path = "BenjaminMoore";
                break;
            case TRAEGER:
                preview_folder_path = "Traeger";
                break;
            default:
                preview_folder_path = "Scotts";
                break;
        }

        var preview_artwork_url = logoPath + preview_folder_path + "/templatePreviews/" + pdf_name + ".pdf";
        var htmlText = null;
        htmlText = "<embed width=850 height=500 type='application/pdf' src='" + preview_artwork_url + "' id='selectedFile'  style='background: transparent url(../images/gizmo_load_anim.gif) no-repeat center;'></embed>";

        $('#popupPDFTemplateContent').html(htmlText);
        window.setTimeout(function getDelay() {
            $('#popPDFTemplate').popup('open', { positionTo: 'window' });
            isPreviewClicked = false;
        }, 200);
        isPreviewClicked = true;
    };

    //Event function to select the template.
    self.onListClick = function (ctrl, event) {
        if (event != undefined) {
            event.cancelBubble = true;
            event.stopPropagation();
        }
        if (jobCustomerNumber == SPORTSKING && ctrl != "_9x6_sportsKing_custom") {
            var prev_template_data = jQuery.parseJSON(sessionStorage.jobSetupOutput);
            var sel_temp_name = prev_template_data.templateName.replace(/\b\.\b/g, "_");
            $('#hdnOption').val('0');
            self.cancelTemplateSelection(prev_template_data, sel_temp_name);
            return false;
        }
        if (!isPreviewClicked) {
            if ($('#template li').find('input[checked]').length > 0) {
                $('#template li').find('input[checked]').attr('checked', false);
                $('#template li').removeClass('ui-body-j').addClass('ui-body-f');
            }
            $('#' + ctrl).attr('checked', true);
            $('#' + ctrl).parent().removeClass('ui-body-f').removeClass('ui-body-inherit').addClass('ui-body-j');
            $('#' + ctrl).parent().parent().attr('data-theme', 'j');
            self.onTemplateChange(ctrl, event);
            if (event != undefined) {
                event.stopPropagation();

                if ($('#hdnOption').val() == "0") {
                    event.preventDefault();
                    $('#hdnOption').val('');
                }
            }
        } else {
            var prev_template_data = jQuery.parseJSON(sessionStorage.jobSetupOutput);
            var sel_temp_name = prev_template_data.templateName.replace(/\b\.\b/g, "_");
            $('#hdnOption').val('0');
            self.cancelTemplateSelection(prev_template_data, sel_temp_name);
            return false;
        }
    };

    //Keeps the previous template information into temp variable, warns the user, cancels the new templat selection if user clicks cancel, 
    //resets the job ticket with new template information if user clicks continue/ok button.
    self.onTemplateChange = function (ctrl_name, event) {
        if (!isPreviewClicked) {
            if (event != undefined) {
                event.cancelBubble = true;
                event.stopPropagation();
            }
            var template_url = serviceURLDomain + "api/Template_customer/" + ctrl_name + "/" + jobCustomerNumber + "/" + sessionStorage.facilityId + "/" + sessionStorage.jobNumber;

            $("#divError").empty();

            $('#template li').each(function () {
                if ($(this).find('input').attr("checked")) {
                    self.selectedTemplate = $.trim($(this).find('input').val());
                }
            });
            var prev_template_data = jQuery.parseJSON(sessionStorage.jobSetupOutput);

            var sel_temp_name = "";
            if (prev_template_data != null && prev_template_data.templateName != null && prev_template_data.templateName != undefined && prev_template_data.templateName != "" && prev_template_data.templateName != self.selectedTemplate && jQuery.parseJSON(sessionStorage.templates).length > 1) {
                if ((jobCustomerNumber == CASEYS || jobCustomerNumber == JETS || jobCustomerNumber == BUFFALONEWS || jobCustomerNumber == PIONEERPRESS || jobCustomerNumber == REDPLUM || jobCustomerNumber == EQUINOX || showSavedJobTemplateChangeRule()) && parseInt(sessionStorage.jobNumber) != -1) {
                    $('#alertmsg').html('The template cannot be changed once the job has been saved');
                    $("#popupDialog").popup('open');
                    sel_temp_name = prev_template_data.templateName.replace(/\b\.\b/g, "_");

                    $('#template li input:checked').attr('checked', false);
                    $("#" + sel_temp_name).attr('checked', true);
                    return false;
                }
                else {
                    var temp_gOutputData = [];
                    if (sessionStorage.previousCustomerNumber != undefined && sessionStorage.previousCustomerNumber != null && sessionStorage.previousCustomerNumber != "") {
                        if (sessionStorage.previousCustomerNumber != self.gOutputData.customerNumber) {
                            self.getSelectedTemplateInfo(template_url, []);
                            sessionStorage.removeItem('previousCustomerNumber');
                        }
                    }
                    else if (sessionStorage.jobNumber > -1) {
                        self.cancelTemplateSelection(prev_template_data, sel_temp_name);
                        $('#alertmsg').html('This order has already been submitted for processing. If your order requires a template change, please contact an account manager.');
                        $("#popupDialog").popup('open');
                        return false;
                    }
                    else if (!confirm('Selecting a new template will discard any other changes you have made to this job. Do you want to continue?')) {
                        $('#hdnOption').val('0');
                        self.cancelTemplateSelection(prev_template_data, sel_temp_name);
                        return false;
                    }
                    else {
                        if (prev_template_data.templateName == null || (self.selectedTemplate.toLowerCase() != prev_template_data.templateName.toLowerCase()))
                            self.getSelectedTemplateInfo(template_url, prev_template_data);
                    }
                }
            }
            else {
                if (prev_template_data == null || prev_template_data.templateName == null || (self.selectedTemplate.toLowerCase() != prev_template_data.templateName.toLowerCase()))
                    self.getSelectedTemplateInfo(template_url, prev_template_data);
            }
        }
    };

    self.cancelTemplateSelection = function (prev_template_data, sel_temp_name) {
        if ($.trim(prev_template_data.templateName).toLowerCase() == "custom")
            sel_temp_name = "cw_" + $.trim(prev_template_data.templateName).toLowerCase();
        else if ($.trim(prev_template_data.templateName).toLowerCase() == "default" || $.trim(prev_template_data.templateName).toLowerCase() == "other")
            sel_temp_name = prev_template_data.templateName.toLowerCase();
        else {

            if (jobCustomerNumber == CW || jobCustomerNumber == CASEYS || jobCustomerNumber == JETS || jobCustomerNumber == REGIS || jobCustomerNumber == SCOTTS || jobCustomerNumber == ACE || jobCustomerNumber == "203" || jobCustomerNumber == BRIGHTHOUSE || jobCustomerNumber == BBB || jobCustomerNumber == AAG || jobCustomerNumber == TRIBUNEPUBLISHING || jobCustomerNumber == SANDIEGO || jobCustomerNumber == AH || jobCustomerNumber == SPORTSAUTHORITY || jobCustomerNumber == WALGREENS || jobCustomerNumber == DATAWORKS || jobCustomerNumber == ALLIED || jobCustomerNumber == SAFEWAY || jobCustomerNumber == LOWES || jobCustomerNumber == DCA || jobCustomerNumber == KUBOTA || jobCustomerNumber == SPORTSKING || jobCustomerNumber == GWA || jobCustomerNumber == BUFFALONEWS || jobCustomerNumber == PIONEERPRESS || jobCustomerNumber == REDPLUM || jobCustomerNumber == EQUINOX || jobCustomerNumber == ANDERSEN || isReplacePreviousTemplateOnCancel()) {
                sel_temp_name = prev_template_data.templateName.replace(/\b\.\b/g, "_");
            }
            else if (jobCustomerNumber == "1") {
                sel_temp_name = prev_template_data.templateName.replace(/\b\.\b/g, "_");
            }

        }
        if (showTemplateApproversList())
            self.setProductType();

        $('#template li input:checked').attr('checked', false);
        $('#template li').removeClass('ui-body-j').addClass('ui-body-f');
        $("#" + sel_temp_name).attr('checked', true);
        $('#' + sel_temp_name).parent().removeClass('ui-body-f').removeClass('ui-body-inherit').addClass('ui-body-j');
    }

    self.addOrderLocationsInVariableList = function () {
        var variable_text_fields = [];
        if (self.gOutputData.selectedLocationsAction.selectedLocationsList != undefined && self.gOutputData.selectedLocationsAction.selectedLocationsList != null && self.gOutputData.selectedLocationsAction.selectedLocationsList.length > 0) {
            $.each(self.gOutputData.selectedLocationsAction.selectedLocationsList, function (tmp_key, tmp_val) {
                var store_cnt = getNextStoreIndex(variable_text_fields, true);   //This method should call from common functions
                if (store_cnt == (getCustomTextLocationsLimit() + 1)) {
                    return false;
                }
                if (variable_text_fields.length == 0) {
                    new_loc_field = {};
                    new_loc_field["name"] = "wantsLogo";
                    new_loc_field["value"] = "";
                    new_loc_field["type"] = "string";
                    variable_text_fields.push(new_loc_field);
                }
                $.each(tmp_val, function (field_key, field_val) {
                    new_loc_field = {};
                    switch (field_key.toLowerCase()) {
                        case "location":
                        case "storename":
                            new_loc_field["name"] = "store" + store_cnt + "Name";
                            new_loc_field["value"] = field_val;
                            new_loc_field["type"] = "string";
                            variable_text_fields.push(new_loc_field);
                            break;
                        case "address":
                        case "address1":
                            new_loc_field["name"] = "store" + store_cnt + "Address";
                            new_loc_field["value"] = field_val;
                            new_loc_field["type"] = "string";
                            variable_text_fields.push(new_loc_field);
                            break;
                        case "city":
                            new_loc_field["name"] = "store" + store_cnt + "City";
                            new_loc_field["value"] = field_val;
                            new_loc_field["type"] = "string";
                            variable_text_fields.push(new_loc_field);
                            break;
                        case "phone":
                        case "phonenumber":
                            new_loc_field["name"] = "store" + store_cnt + "Phone";
                            new_loc_field["value"] = getFormattedPhone(tmp_val["areaCode"], field_val);
                            new_loc_field["type"] = "string";
                            variable_text_fields.push(new_loc_field);
                            break;
                        case "pk":
                            new_loc_field["name"] = "store" + store_cnt + "pk";
                            new_loc_field["value"] = field_val;
                            new_loc_field["type"] = "string";
                            variable_text_fields.push(new_loc_field);
                            break;
                        case "state":
                            if (isDisplayStateAndZipInCustomTextLocation()) {
                                new_loc_field["name"] = "store" + store_cnt + "State";
                                new_loc_field["value"] = field_val;
                                new_loc_field["type"] = "string";
                                variable_text_fields.push(new_loc_field);
                            }
                            break;
                        case "zipcode":
                        case "zip":
                            if (isDisplayStateAndZipInCustomTextLocation() > -1) {
                                new_loc_field["name"] = "store" + store_cnt + "ZipCode";
                                new_loc_field["value"] = field_val;
                                new_loc_field["type"] = "string";
                                variable_text_fields.push(new_loc_field);
                            }
                            break;
                    }
                });
            });
        }
        return variable_text_fields;
    }

    //Gets the selected template information from service and loads into local session.
    self.getSelectedTemplateInfo = function (template_url, prev_template_data) {
        getCORS(template_url, null, function (data) {
            if (data != null) {
                if (jobCustomerNumber == CASEYS) {
                    data["modulesAction"] = {};
                    if (data.jobTypeId == 71) {
                        data.modulesAction["modulesDict"] = {
                            "display_page_selectTemplate": "true",
                            "display_page_selectLocations": "true",
                            "display_page_milestones": "true",
                            "display_page_listSelection": "true",
                            "display_page_optInStatus": "true"
                        };
                        $('#dvOfferExp').css("display", "block");
                    }
                    else if (data.jobTypeId == 72) {
                        data.modulesAction["modulesDict"] = {
                            "display_page_selectTemplate": "true",
                            "display_page_selectLocations": "true",
                            "display_page_milestones": "true",
                            "display_page_listSelection": "true",
                            "display_page_optInStatus": "true"
                        };
                        $('#dvOfferExp').css("display", "block");
                    }
                    else if (data.jobTypeId == 73 || data.jobTypeId == 74) {
                        data.modulesAction["modulesDict"] = {
                            "display_page_selectTemplate": "true",
                            "display_page_selectLocations": "true",
                            "display_page_milestones": "true",
                            "display_page_pieceAttributes": "true",
                            "display_page_uploadArtwork": "true",
                            "display_page_listSelection": "true",
                            "display_page_approval": "true",
                            "display_page_submitOrder": "true"
                        };
                        $('#dvOfferExp').css("display", "none");
                    }
                }
                if (jobCustomerNumber == ALLIED || jobCustomerNumber == AAG || jobCustomerNumber == DCA || jobCustomerNumber == KUBOTA) {

                    if (jobCustomerNumber != KUBOTA) {
                        if (data["companyInfoAction"] == undefined) {
                            data["companyInfoAction"] = {};
                            data.companyInfoAction["isReplicated"] = true;
                        }
                    }
                    if (jobCustomerNumber == ALLIED) {
                        if (data["baseSelectionAction"] == undefined) {
                            data["baseSelectionAction"] = {};
                            data.baseSelectionAction["isReplicated"] = false;
                        }

                        if (data["logoSelectionAction"] == undefined) {
                            data["logoSelectionAction"] = {};
                            data.logoSelectionAction["isReplicated"] = false;
                        }
                        if (data["inHomeDate2"] == undefined || data["inHomeDate2"] == null) data["inHomeDate2"] == "";
                        $('#dvInHomeDate2').css('display', 'block');
                        if (template_url.toLowerCase().indexOf('platinum') > -1 || template_url.toLowerCase().indexOf('gold') > -1 || template_url.toLowerCase().indexOf('bronze') > -1) {
                            if (data["inHomeDate3"] == undefined || data["inHomeDate3"] == null) data["inHomeDate3"] == "";
                            $('#dvInHomeDate3').css('display', 'block');
                        }
                        else {
                            if (data["inHomeDate3"] != undefined) delete data["inHomeDate3"];
                            $('#inHomeDate3').val('');
                            $('#dvInHomeDate3').css('display', 'none');
                        }
                    }
                    if (jobCustomerNumber == ALLIED || jobCustomerNumber == DCA) {
                        if (data["offerSetupAction"] == undefined) {
                            data["offerSetupAction"] = {};
                            data.offerSetupAction["isReplicated"] = false;
                        }
                    }
                    if (jobCustomerNumber == DCA || jobCustomerNumber == AAG) {
                        if (data["selectImagesAction"] == undefined) {
                            data["selectImagesAction"] = {};
                            data.selectImagesAction["isReplicated"] = true;
                        }
                    }
                }

                if ((jobCustomerNumber == DCA || jobCustomerNumber == KUBOTA || jobCustomerNumber == SPORTSKING || jobCustomerNumber == GWA) && data.templateName.indexOf('_11x6_') > -1) {
                    data.mailstreamAction.width = 11;
                }
                if ((isNewCustomTextVariableFieldListCustomers()) && template_url.indexOf(getCustomArtworkTemplate()) == -1 && self.gOutputData.selectedLocationsAction.selectedLocationsList != undefined && self.gOutputData.selectedLocationsAction.selectedLocationsList != null && self.gOutputData.selectedLocationsAction.selectedLocationsList != "" && self.gOutputData.selectedLocationsAction.selectedLocationsList.length > 0) {
                    data.variableTextAction.variableFieldList = self.addOrderLocationsInVariableList();
                }
                // END.
                if (isNewCustomTextVariableFieldListCustomers()) {
                    if (([97, 150, ANDERSEN_CustomUpload_JobType, SHERWINWILLIAMS_CustomUpload_JobType, MILWAUKEE_CustomUpload_JobType, BENJAMINMOORE_CustomUpload_JobType, TRAEGER_CustomUpload_JobType].indexOf(data.jobTypeId) > -1) && (template_url.indexOf(getCustomArtworkTemplate()) > -1)) {
                        data["modulesAction"] = {};
                        data.modulesAction["modulesDict"] = {
                            "display_page_selectTemplate": "true",
                            "display_page_selectLocations": "true",
                            "display_page_uploadArtwork": "true",
                            "display_page_listSelection": "true",
                            "display_page_approval": "true",
                            "display_page_submitOrder": "true"
                        };
                    }
                    else {
                        loadCustNavLinks(jobCustomerNumber);
                    }
                    if (prev_template_data.templateName == getCustomArtworkTemplate()) {
                        delete self.gOutputData.modulesAction;
                    }
                }
                templateData = data;
                self.getTemplateData(templateData, prev_template_data);
                self.gOutputData = templateData;
                self.gOutputData["needsPostageReportUpdate"] = true; //if quantity, template and/or mail class have changed, we need make this attribue to true
                sessionStorage.jobSetupOutput = JSON.stringify(self.gOutputData);

                var template_data = jQuery.parseJSON(sessionStorage.templates);
                $.each(template_data, function (key, val) {
                    if (template_data[key].value == self.gOutputData.templateName) {
                        if (val.pricingList != undefined && val.pricingList != null && val.pricingList != "")
                            sessionStorage.selectedTemplatePricing = JSON.stringify(val.pricingList);
                        return false;
                    }
                });

                //Builds the left navigation panel. --- START
                if (self.gOutputData.modulesAction != undefined && self.gOutputData.modulesAction != null && self.gOutputData.modulesAction != "" && self.gOutputData.modulesAction.modulesDict != null && self.gOutputData.modulesAction.modulesDict != null && self.gOutputData.modulesAction.modulesDict != "") {
                    sessionStorage.custNavLinks = JSON.stringify(self.gOutputData.modulesAction.modulesDict);
                    getCustNavLinks();
                    createNavLinks();
                }
                else {
                    getCustNavLinks();
                    createNavLinks();
                }
                // -- END
                return true;
            }
            else {
                self.getStaticTemplateInfo();
            }
        }, function (error_response) {
            showErrorResponseText(error_response, true);
            self.getStaticTemplateInfo();
        });
        $('#template li').removeClass('ui-body-j').addClass('ui-body-f');//.listview('refresh');
        $(':radio:checked').parent().addClass(' ui-body-j').removeClass('ui-body-f');//.listview('refresh');
    };

    //Loads the job ticket object into the session when template selection fails from service.
    self.getStaticTemplateInfo = function () {
        var job_setup_json = jQuery.parseJSON(sessionStorage.jobSetupOutput);
        template = (job_setup_json.templateName != undefined) ? job_setup_json.templateName : job_setup_json.template.template;
        if (self.selectedTemplate.toLowerCase() != job_setup_json.templateName.toLowerCase()) {
            var json_output_format;
            $.getJSON(gOutputServiceUrl, function (dataOutput) {
                json_output_format = dataOutput;
                self.resetTemplateSelection(template, self.selectedTemplate, json_output_format, job_setup_json);
                return true;
            });
        }
    };
    //Gets the selected template information and saves in the local object, if user changes the template.
    self.getTemplateData = function (templateData, prev_template_data) {
        var template = "";
        //Adding instructionsAction node into JSON object returned from web service.
        if (prev_template_data != null && prev_template_data.instructionsAction != undefined && prev_template_data.instructionsAction != null) {
            templateData.instructionsAction = prev_template_data.instructionsAction;
            if (prev_template_data.instructionsAction.specialInstructions != undefined && prev_template_data.instructionsAction.specialInstructions.length == 0)
                templateData.instructionsAction.specialInstructions = [];
        }

        //Adding uploadList node into JSON object returned from web service.
        if (prev_template_data != null && prev_template_data.standardizeAction != undefined && prev_template_data.standardizeAction != null) {
            templateData.standardizeAction = prev_template_data.standardizeAction;
            if (prev_template_data.standardizeAction.standardizeFileList != null && prev_template_data.standardizeAction.standardizeFileList.length == 0) {
                var upload_list_info = [];
                upload_list_info.push({ sourceType: "", fileName: "", fileMap: "", priority: "", fileSizePretty: "", fileStatus: "" });
                templateData.standardizeAction.standardizeFileList = upload_list_info;
            }
        }
        //Adding uploadArtwork node into JSON object returned from web service.
        if (prev_template_data != null && prev_template_data.artworkAction != undefined && prev_template_data.artworkAction != null) {
            templateData.artworkAction = prev_template_data.artworkAction;
            if (prev_template_data.artworkAction.artworkFileList != null && prev_template_data.artworkAction.artworkFileList.length == 0) {
                var artwork_file_info = {};
                //artwork_file_info.push({ "fileName": "", "fileSizePretty": "", "fileStatus": "" });
                templateData.artworkAction.artworkFileList = artwork_file_info;
            }
        }

        if (templateData["inHomeDate"] == undefined || templateData["inHomeDate"] == null) templateData["inHomeDate"] = "";
        if (jobCustomerNumber == ALLIED) {
            templateData["inHomeDate2"] = prev_template_data.inHomeDate2;
            if (templateData.templateName.toLowerCase().indexOf('platinum') > -1 || templateData.templateName.toLowerCase().indexOf('gold') > -1 || templateData.templateName.toLowerCase().indexOf('bronze') > -1) {
                templateData["inHomeDate3"] = prev_template_data.inHomeDate3;
            }
        }
        if (jobCustomerNumber == CASEYS || jobCustomerNumber == JETS || jobCustomerNumber == BUFFALONEWS || jobCustomerNumber == PIONEERPRESS || jobCustomerNumber == REDPLUM || jobCustomerNumber == EQUINOX || isClearExpiryDate()) {
            if (jobCustomerNumber == JETS)
                templateData["expirationDate"] = "01/01/2016";
            else
                templateData["expirationDate"] = "";
        }
        templateData["estimatedQuantity"] = "";

        if (templateData.mailstreamAction != undefined) {
            if (templateData.mailstreamAction.optionalAttributesDict == undefined)
                templateData.mailstreamAction["optionalAttributesDict"] = null;
            if (jobCustomerNumber == "1")
                templateData.mailstreamAction["advancedPresortOptions"] = [];
        }
        if (sessionStorage.previousCustomerNumber == undefined || sessionStorage.previousCustomerNumber == null || sessionStorage.previousCustomerNumber == "") {
            if (templateData["customerName"] == undefined || templateData["customerName"] == null) templateData["customerName"] = "";
            templateData["customerName"] = jobCustomerName;

            template = (prev_template_data != null && prev_template_data.templateName != undefined && prev_template_data.templateName != null) ? prev_template_data.templateName : ((prev_template_data != null && prev_template_data.template != undefined && prev_template_data.template != null) ? prev_template_data.template.template : "");
            if (sessionStorage.jobSetupOutput != undefined && sessionStorage.jobSetupOutput != null && sessionStorage.jobSetupOutput != "")
                templateData["selectedLocationsAction"] = jQuery.parseJSON(sessionStorage.jobSetupOutput).selectedLocationsAction;
        }
        if (jobCustomerNumber == CASEYS || jobCustomerNumber == JETS || jobCustomerNumber == BUFFALONEWS || jobCustomerNumber == PIONEERPRESS || jobCustomerNumber == REDPLUM || jobCustomerNumber == EQUINOX || isUpdateListSelectionInTemplateData())
            templateData["listSelection"] = (sessionStorage.jobSetupOutput != undefined && sessionStorage.jobSetupOutput != null && sessionStorage.jobSetupOutput != "" && jQuery.parseJSON(sessionStorage.jobSetupOutput).listSelection != undefined && jQuery.parseJSON(sessionStorage.jobSetupOutput).listSelection.length > 0) ? jQuery.parseJSON(sessionStorage.jobSetupOutput).listSelection : [];

        if (templateData["milestoneAction"] == undefined) { templateData["milestoneAction"] = {}; }
        if (sessionStorage.jobSetupOutput != undefined && sessionStorage.jobSetupOutput != null && sessionStorage.jobSetupOutput != "") {
            templateData.milestoneAction = (jQuery.parseJSON(sessionStorage.jobSetupOutput)).milestoneAction;
            templateData.jobName = (jQuery.parseJSON(sessionStorage.jobSetupOutput)).jobName;
            templateData.inHomeDate = (jQuery.parseJSON(sessionStorage.jobSetupOutput)).inHomeDate;
        }
    };

    //Resets the template information, if user clicks cancel button in the message displayed, when trying to change the already selected template.
    self.resetTemplateSelection = function (template, selectedTemplate, json_output_format, job_setup_json) {
        job_setup_json = json_output_format;
        job_setup_json.templateName = selectedTemplate;
        var goutput_data = jQuery.parseJSON(sessionStorage.jobSetupOutput);
        job_setup_json.selectedLocationsAction = goutput_data.selectedLocationsAction;
        if ((jobCustomerNumber == CASEYS && goutput_data.jobTypeId != 73) || jobCustomerNumber == JETS || jobCustomerNumber == BUFFALONEWS || jobCustomerNumber == PIONEERPRESS || jobCustomerNumber == REDPLUM || jobCustomerNumber == EQUINOX || isUpdateExpiryAndListInResetTemplateSelection()) {
            job_setup_json["expirationDate"] = (goutput_data.expirationDate != undefined) ? goutput_data.expirationDate : (jobCustomerNumber == JETS) ? "11/30/2015" : "";
            job_setup_json["listSelection"] = (goutput_data.listSelection != undefined && goutput_data.listSelection.length > 0) ? goutput_data.listSelection : [];
        }
        else {
            job_setup_json["expirationDate"] = "";
            job_setup_json["listSelection"] = (goutput_data.listSelection != undefined && goutput_data.listSelection.length > 0) ? goutput_data.listSelection : [];
        }
        job_setup_json.facilityId = goutput_data.facilityId;
        job_setup_json.customerNumber = (goutput_data.customerNumber != "") ? goutput_data.customerNumber : jobCustomerNumber;
        job_setup_json.customerName = (goutput_data.customerName != "") ? goutput_data.customerName : jobCustomerName;
        job_setup_json.jobName = goutput_data.jobName;
        job_setup_json.userId = goutput_data.userId;
        job_setup_json.jobNumber = (goutput_data.jobNumber != "") ? goutput_data.jobNumber : "-1";
        if (jobCustomerNumber == ALLIED) {
            job_setup_json.inHomeDate = goutput_data.inHomeDate;
            job_setup_json.inHomeDate2 = goutput_data.inHomeDate2;
            if (goutput_data.templateName.toLowerCase().indexOf('platinum') > -1 || goutput_data.templateName.toLowerCase().indexOf('gold') > -1) {
                job_setup_json.inHomeDate3 = goutput_data.inHomeDate3;
            }
        }
        else {
            job_setup_json.inHomeDate = goutput_data.inHomeDate;
        }
        job_setup_json.estimatedQuantity = goutput_data.estimatedQuantity;
        job_setup_json.milestoneAction = goutput_data.milestoneAction;

        if (goutput_data.instructionsAction != undefined && goutput_data.instructionsAction != null) {
            job_setup_json.instructionsAction = goutput_data.instructionsAction;
            if (goutput_data.instructionsAction.specialInstructions == null || goutput_data.instructionsAction.specialInstructions.length == 0)
                job_setup_json.instructionsAction.specialInstructions = [];
        }

        //Adding uploadList node into JSON object returned from web service.
        if (goutput_data.standardizeAction != undefined && goutput_data.standardizeAction != null) {
            job_setup_json.standardizeAction = goutput_data.standardizeAction;
            if (goutput_data.standardizeAction.standardizeFileList == null || goutput_data.standardizeAction.standardizeFileList.length == 0) {
                var upload_list_info = [];
                upload_list_info.push({ sourceType: "", fileName: "", fileMap: "", priority: "", fileSizePretty: "", fileStatus: "" });
                job_setup_json.standardizeAction.standardizeFileList = upload_list_info;
            }
        }
        //Adding uploadArtwork node into JSON object returned from web service.
        if (goutput_data.artworkAction != undefined && goutput_data.artworkAction != null) {
            job_setup_json.artworkAction = goutput_data.artworkAction;
            if (goutput_data.artworkAction.artworkFileList == null || goutput_data.artworkAction.artworkFileList.length == 0) {
                var artwork_file_info = {};
                //artwork_file_info.push({ "fileName": "", "fileSizePretty": "", "fileStatus": "" });
                job_setup_json.artworkAction.artworkFileList = artwork_file_info;
            }
        }

        if ((jobCustomerNumber == CASEYS && goutput_data.jobTypeId != 73) || jobCustomerNumber == JETS || jobCustomerNumber == BUFFALONEWS || jobCustomerNumber == PIONEERPRESS || jobCustomerNumber == REDPLUM || jobCustomerNumber == EQUINOX || isUpdateExpiryAndListInResetTemplateSelection()) {
            job_setup_json["expirationDate"] = (goutput_data.expirationDate != undefined) ? goutput_data.expirationDate : (jobCustomerNumber == JETS) ? "11/30/2015" : "";
            job_setup_json["listSelection"] = (goutput_data.listSelection != undefined && goutput_data.listSelection.length > 0) ? goutput_data.listSelection : [];
        }
        else if (jobCustomerNumber == CASEYS && goutput_data.jobTypeId != 73) {
            job_setup_json["expirationDate"] = "";
            job_setup_json["listSelection"] = (goutput_data.listSelection != undefined && goutput_data.listSelection.length > 0) ? goutput_data.listSelection : [];
        }
        else if (jobCustomerNumber == CW || jobCustomerNumber == BRIGHTHOUSE || jobCustomerNumber == BBB || jobCustomerNumber == AAG || jobCustomerNumber == TRIBUNEPUBLISHING || jobCustomerNumber == SANDIEGO || jobCustomerNumber == AH || jobCustomerNumber == SPORTSAUTHORITY || jobCustomerNumber == WALGREENS || jobCustomerNumber == DATAWORKS || jobCustomerNumber == REDPLUM || jobCustomerNumber == SAFEWAY || jobCustomerNumber == LOWES) {
            job_setup_json["pricingId"] = "";
            self.gOutputData = job_setup_json;
            sessionStorage.jobSetupOutput = JSON.stringify(job_setup_json);
            sessionStorage.jobOutputFile = JSON.stringify(job_setup_json);
        }
    };
    //Displays the list of controls and displays in the page.
    self.buildLists = function () {
        $.each(self.gData, function (key, val) {
            switch (val.list) {
                case "template":
                    if ($("#pageName").val() == "template") {
                        $("#template").empty();
                        var json_templates = jQuery.parseJSON(sessionStorage.templates);
                        if (json_templates != null) {
                            var hiding_templates_for_prod_users = ['_t1_fourStepSouthWest', '_t9_fourStepNorthEastMidwest', '_t10_fourStepTV', '_t11_fourStepCircular', '_ct1_customArtwork_55x105', '_ace_55x105'];
                            $.each(json_templates, function (key, value) {
                                //makeTemplate(value.value, value.text, ((value.pricingId != undefined && value.pricingId != null) ? value.pricingId : ""));
                                if ((showPricingList()) || hiding_templates_for_prod_users.indexOf(value.value) == -1) {
                                    if (value.pricingList != undefined && value.pricingList != null)
                                        pricingListInfo = value.pricingList;
                                    if ((($("#divProductType").css('display') == 'block') && ($("#ddlProductType").val() != "select" && value.value.indexOf(("_" + $("#ddlProductType").val() + "_")) > -1)) || (($("#divProductType").css('display') == 'block') && (isMakeTemplateWithProductType()))) {
                                        self.makeTemplate(value.value, value.text, value.thumbnail);
                                    }
                                    else if ($("#divProductType").css('display') == 'none')
                                        self.makeTemplate(value.value, value.text, value.thumbnail);
                                }
                            });
                            $("#template").listview('refresh');
                        }
                        if (showTemplateApproversList() && ((self.gOutputData != undefined && self.gOutputData.templateName != undefined && self.gOutputData.templateName != null && self.gOutputData.templateName != ""))) {
                            $('#template li').removeClass('ui-body-j').addClass('ui-body-f');
                            $("#" + self.gOutputData.templateName).attr('checked', true);
                            $('#' + self.gOutputData.templateName).parent().removeClass('ui-body-f').removeClass('ui-body-inherit').addClass('ui-body-j');
                        }
                    }
                    break;
                case "inHomeDate":
                case "inHomeDate2":
                case "inHomeDate3":
                    if ($("#pageName").val() == "template") {
                        if (isEmailLogin() && jobNumber == -1) {
                            if (preSetInHomeDate == "" && (sessionStorage.asmFixedInHomeDate != undefined && sessionStorage.asmFixedInHomeDate != null && sessionStorage.asmFixedInHomeDate != "")) {
                                preSetInHomeDate = sessionStorage.asmFixedInHomeDate;
                            }
                            $("#inHomeDate").val(preSetInHomeDate);
                        }
                        else if (sessionStorage.asmFixedInHomeDate != undefined && sessionStorage.asmFixedInHomeDate != null && sessionStorage.asmFixedInHomeDate != "") {
                            $("#inHomeDate").val(sessionStorage.asmFixedInHomeDate);
                        }
                        else if (jobCustomerNumber == JETS) {
                            $("#inHomeDate").val("12/01/2015");
                        }
                        else if (jobCustomerNumber == ALLIED) {
                            $("#" + key).val(val);
                        }
                        else
                            $("#inHomeDate").val(val);
                    }
                    break;
                case "expirationDate":
                    if ($("#pageName").val() == "template") {
                        if (jobCustomerNumber == JETS) {
                            $("#expirationDate").val("01/01/2016");
                        }
                        else
                            $("#expirationDate").val(val);
                    }
                    break;
                case "estimatedQuantity":
                    if ($("#pageName").val() == "template") {
                        $("#estimatedQuantity").val(val);
                    }
                    break;
                case "customerName":
                    if ($("#pageName").val() == "template") {
                        $("#customerName").val(val);
                    }
                    break;
                case "jobName":
                    if (isEmailLogin() && jobNumber == -1 && $("#pageName").val() == "template") {
                        $("#txtJobName").val(preSetJobDesc);
                    }
                    break;
            }

        });
        sessionStorage.jobSetupLoadObject = JSON.stringify(self.gData);
    }
    //Displays the persisted information into the page controls.
    self.buildOutputLists = function () {
        //Builds the left navigation panel. -- START
        if (self.gOutputData != null && self.gOutputData.modulesAction != undefined && self.gOutputData.modulesAction != null && self.gOutputData.modulesAction != "" && self.gOutputData.modulesAction.modulesDict != null && self.gOutputData.modulesAction.modulesDict != null && self.gOutputData.modulesAction.modulesDict != "") {
            sessionStorage.custNavLinks = JSON.stringify(self.gOutputData.modulesAction.modulesDict);
            getCustNavLinks();
            createNavLinks();
        }
        // -- END
        if (self.gOutputData != null) {
            getBubbleCounts(self.gOutputData);
        }
        $.each(self.gOutputData, function (key, val) {
            if (key != undefined) {
                switch (key) {
                    case "template":
                    case "templateName":
                        if (val != null && val != "") {
                            $(':radio[value="' + ((val.indexOf(' ') > -1) ? val.split(' ')[0] : val) + '"]').attr('checked', true);
                            $('#template li').removeClass('ui-body-j').addClass('ui-body-f');
                            $(':radio:checked').parent().addClass('ui-body-j').removeClass('ui-body-f').removeClass('ui-body-inherit');
                            if ((jobCustomerNumber == CASEYS && (self.gOutputData.jobTypeId == 73 || self.gOutputData.jobTypeId == 74)) || jobCustomerNumber == BUFFALONEWS || jobCustomerNumber == PIONEERPRESS || jobCustomerNumber == REDPLUM || jobCustomerNumber == EQUINOX || hideOfferExpiryDiv())
                                $('#dvOfferExp').css("display", "none");
                        }
                        break;
                    case "inHomeDate":
                    case "inHomeDate2":
                    case "inHomeDate3":
                        if ($("#pageName").val() == "template") {
                            if ((jobCustomerNumber == AAG || jobCustomerNumber == TRIBUNEPUBLISHING || jobCustomerNumber == SANDIEGO || jobCustomerNumber == AH || jobCustomerNumber == SPORTSAUTHORITY || jobCustomerNumber == WALGREENS || jobCustomerNumber == ALLIED || jobCustomerNumber == SAFEWAY || jobCustomerNumber == LOWES) && sessionStorage.jobNumber == "-1") {
                                $("#inHomeDate").datepicker('setDate', '+' + ((jobCustomerNumber == DCA || jobCustomerNumber == SPORTSKING) ? 15 : ((jobCustomerNumber == OH) ? 7 : 21)) + 'd');
                                //Find Last date of the month
                                var date = new Date(); y = date.getFullYear(), m = date.getMonth();
                                var lastDate = new Date(y, m + 1, 0);
                                $("#inHomeDate").val((lastDate.getMonth() + 1) + "/" + lastDate.getDate() + "/" + lastDate.getFullYear());

                            }
                            else if (jobCustomerNumber == JETS) {
                                $("#inHomeDate").val("12/01/2015");
                            }
                            else if (isEmailLogin() && jobNumber == "-1") {
                                if (preSetInHomeDate == "" && (sessionStorage.asmFixedInHomeDate != undefined && sessionStorage.asmFixedInHomeDate != null && sessionStorage.asmFixedInHomeDate != "")) {
                                    preSetInHomeDate = sessionStorage.asmFixedInHomeDate;
                                }
                                $('#inHomeDate').val(preSetInHomeDate);
                            }
                            else if (sessionStorage.asmFixedInHomeDate != undefined && sessionStorage.asmFixedInHomeDate != null && sessionStorage.asmFixedInHomeDate != "") {
                                $("#inHomeDate").val(sessionStorage.asmFixedInHomeDate);
                            }
                            else
                                if (jobCustomerNumber == ALLIED) {
                                    $("#" + key).val(val);
                                }
                                else {
                                    $("#inHomeDate").val(val);
                                }
                            if (jobCustomerNumber == ALLIED && self.gOutputData.templateName != "") {
                                $('#dvInHomeDate2').css('display', 'block');
                                if (self.gOutputData.templateName.toLowerCase().indexOf('platinum') > -1 || self.gOutputData.templateName.toLowerCase().indexOf('gold') > -1 || self.gOutputData.templateName.toLowerCase().indexOf('bronze') > -1) {
                                    $('#dvInHomeDate3').css('display', 'block');
                                }
                                else {
                                    $('#inHomeDate3').val('');
                                    $('#dvInHomeDate3').css('display', 'none');
                                }
                            }
                        }
                        break;
                    case "expirationDate":
                        if ($("#pageName").val() == "template") {
                            if (jobCustomerNumber == JETS) {
                                $("#expirationDate").val("01/01/2016");
                            }
                            else
                                $("#expirationDate").val(val);

                        }
                        break;
                    case "estimatedQuantity":
                        if ($("#pageName").val() == "template") {
                            $("#estimatedQuantity").val(val);
                        }
                        break;
                    case "jobName":
                        if ($("#pageName").val() == "template") {
                            if ((jobCustomerNumber == AAG || jobCustomerNumber == TRIBUNEPUBLISHING || jobCustomerNumber == SANDIEGO || jobCustomerNumber == AH || jobCustomerNumber == SPORTSAUTHORITY || jobCustomerNumber == WALGREENS || jobCustomerNumber == SAFEWAY || jobCustomerNumber == LOWES) && sessionStorage.jobNumber != "-1") {
                                var temp_default_json = (sessionStorage.defaultJobDetails != undefined && sessionStorage.defaultJobDetails != null && sessionStorage.defaultJobDetails != "") ? $.parseJSON(sessionStorage.defaultJobDetails) : {};
                                if (Object.keys(temp_default_json).length > 0 && appPrivileges.roleName != "admin") {
                                    $('#txtJobName').addClass('ui-disabled');
                                    $('#txtJobName')[0].disabled = true;
                                    $("#txtJobName").attr('disabled', true);
                                    $('#txtJobName').css('opacity', '0.6');
                                    $('#txtJobName').val(temp_default_json.defaultJobName);
                                }
                                else {
                                    $('#txtJobName').addClass('ui-disabled');
                                    $('#txtJobName')[0].disabled = true;
                                    $("#txtJobName").attr('disabled', true);
                                    $('#txtJobName').css('opacity', '0.6');
                                    $("#txtJobName").val(val);
                                }
                            }
                            else if (isEmailLogin() && jobNumber == "-1") {
                                $('#txtJobName').val(preSetJobDesc);
                            }
                            else
                                $("#txtJobName").val(val);
                        }
                        break;
                    case "customerNumber":
                        if ($("#pageName").val() == "template") {
                            if (val != "")
                                $('#ddlCustomer').val((val != "1") ? val : "-1").selectmenu('refresh');
                        }
                        break;
                }
            }
        });
        sessionStorage.jobSetupOutput = JSON.stringify(self.gOutputData);
    };

    //Persists the selected data into local session before navigating to other page.
    self.bindData = function () {
        if (jobCustomerNumber == "1") {
            var selected_customer = jQuery.grep(self.filter_customer, function (obj) {
                return obj.label.toLowerCase() === $("#customerName").val().toLowerCase();
            });
            if (selected_customer.length > 0) {
                sessionStorage.jobCustomerNumber = selected_customer[0].custNo;
                self.gOutputData.customerNumber = selected_customer[0].custNo;
                self.gOutputData.customerName = selected_customer[0].name;
            }

        }

        $.each(self.gOutputData, function (key, val) {
            if (key != undefined) {
                switch (key) {
                    case "customerName":
                        if ($("#pageName").val() == "template") {
                            if ($('#ddlCustomer').val() != null)
                                self.gOutputData[key] = $('#ddlCustomer').find('option:selected').text(); // $("#" + key).val();
                        }
                        break;
                    case "inHomeDate":
                    case "inHomeDate2":
                    case "inHomeDate3":
                        if ($("#pageName").val() == "template") {
                            self.gOutputData[key] = $("#" + key).val();
                        }
                        break;
                    case "expirationDate":
                        if ($("#pageName").val() == "template") {
                            if (self.gOutputData.jobTypeId != 73 && self.gOutputData.jobTypeId != 74)
                                self.gOutputData[key] = $("#" + key).val();
                        }
                        break;
                    case "estimatedQuantity":
                        if ($("#pageName").val() == "template") {
                            self.gOutputData[key] = (jQuery.trim($("#" + key).val().replace(/,/g, '')) != "") ? parseInt(jQuery.trim($("#" + key).val().replace(/,/g, ''))) : 0;
                        }
                        break;
                    case "templateName":
                    case "template":
                        self.gOutputData[key] = $('#template li').find('input[checked]').val();
                        break;
                    case "jobName":
                        if ($("#pageName").val() == "template") {
                            self.gOutputData[key] = jQuery.trim($('#txtJobName').val());
                        }
                        break;
                }
            }
        });
        if ((jobCustomerNumber == CW || jobCustomerNumber == BRIGHTHOUSE || jobCustomerNumber == BBB || jobCustomerNumber == AAG || jobCustomerNumber == TRIBUNEPUBLISHING || jobCustomerNumber == SANDIEGO || jobCustomerNumber == AH || jobCustomerNumber == SPORTSAUTHORITY || jobCustomerNumber == WALGREENS || jobCustomerNumber == DATAWORKS || jobCustomerNumber == SAFEWAY || jobCustomerNumber == LOWES) && sessionStorage.jobNumber == -1 && (self.gOutputData.milestoneAction != null && self.gOutputData.milestoneAction != undefined) && (self.gOutputData.milestoneAction.milestoneDict == undefined || self.gOutputData.milestoneAction.milestoneDict == null)) {
            var mile_stone_dict = {
                "list": { "hasChanged": 1, "isRequired": 0, "description": "Data Arrival", "scheduledDate": "", "scheduledBy": "", "completedDate": "", "completedBy": "" },
                "artarrival": { "hasChanged": 1, "isRequired": 0, "description": "Art Arrival", "scheduledDate": "", "scheduledBy": "", "completedDate": "", "completedBy": "" },
                "inhome": { "hasChanged": 1, "isRequired": 1, "description": "In-Home", "scheduledDate": $('#inHomeDate').val(), "scheduledBy": "admin_cw1", "completedDate": "", "completedBy": "" },
                "salestart": { "hasChanged": 1, "isRequired": 0, "description": "Sale Start", "scheduledDate": "", "scheduledBy": "", "completedDate": "", "completedBy": null },
                "saleend": { "hasChanged": 1, "isRequired": 0, "description": "Sale End", "scheduledDate": "", "scheduledBy": "", "completedDate": "", "completedBy": "" }
            };
            self.gOutputData.milestoneAction["milestoneDict"] = mile_stone_dict;
        }
        if ((self.gOutputData.milestoneAction != undefined && self.gOutputData.milestoneAction != null) && (self.gOutputData.milestoneAction.milestoneDict != undefined && self.gOutputData.milestoneAction.milestoneDict != null)) {
            if (self.gOutputData.milestoneAction.milestoneDict["inhome"] != undefined)
                self.gOutputData.milestoneAction.milestoneDict.inhome["scheduledDate"] = self.gOutputData.inHomeDate;
        }
        if (jobCustomerNumber == "203") {
            self.gOutputData.selectedLocationsAction.selectedLocationsList = [
        {
            "city": "St. Louis",
            "state": "MO",
            "originalIndex": "0",
            "quantity": 1000,
            "address1": "520 Maryville Centre Drive",
            "zip": "63141",
            "storeName": "Suddenlink Corporate",
            "storeId": "CORP"
        }
            ];

        }
        sessionStorage.jobSetupOutput = JSON.stringify(self.gOutputData);
    };

    //Loads customers into drop down.
    self.loadCustomerInfo = function () {
        var gServiceUrl = "";
        gServiceUrl = serviceURLDomain + "api/Customer/";

        getCORS(gServiceUrl, null, function (data) {
            self.gCustomerData = data;
            var cust_options = "";
            $.each(data, function (key, val) {
                if (key == 0)
                    cust_options += '<option value="-1">Select Customer</option>';
                cust_options += '<option value="' + val.customerNumber + '">' + val.customerName + '</option>';
            });
            if (cust_options != '')
                $('#ddlCustomer').html(cust_options).selectmenu('refresh');

        }, function (error_response) {
            showErrorResponseText(error_response, true);
        });
    };

    //Loads left navigation links and templates based on the selected customer from the drop down.
    self.getSelectedCustomerInfo = function (ctrl) {
        sessionStorage.previousCustomerNumber = $.extend(true, [], self.gOutputData).customerNumber;
        self.gOutputData.customerNumber = ($(ctrl).val() == "-1") ? "1" : $(ctrl).val();
        self.gOutputData.customerName = $(ctrl).find('option:selected').text();

        var selected_customer_info = { "customerNumber": self.gOutputData.customerNumber, "customerName": self.gOutputData.customerName };
        sessionStorage.selectedCustomerInfo = JSON.stringify(selected_customer_info);
        sessionStorage.removeItem('locationConfig');
        loadCustNavLinks(($(ctrl).val() == "-1") ? "1" : $(ctrl).val())
        return false;
    };

    //Validates the user selected selected data and warns the user if required.
    self.updateSelectedTemplate = function (click_type) {
        if (!self.templatesValidation())
            return false;
        else {
            var error_msg = self.validateJobName();
            if (error_msg != "") {
                $('#alertmsg').html(error_msg);
                $("#popupDialog").popup('open');
                return false;
            }

            self.bindData();
            if (sessionStorage.jobNumber == "-1" && click_type != "save") {
                postJobSetUpData('save');
            }
            return true;
        }
    };

    //Validates the fields and displays the message.
    self.templatesValidation = function () {
        var msg = "";
        if ($('#txtJobName').css('border').indexOf('solid') > -1) {
            msg = 'The entered Job name <b>' + $('#txtJobName').val() + '</b> already exists. Please enter unique job name.';
            $('#alertmsg').html(msg);
            $('#popupDialog').popup('open');
            return false;
        } else {
            var count = 0;
            var cust_name = $('#ddlCustomer').val();
            var radio_list = $('input:radio');
            $.each(radio_list, function (key, ctrl) {
                if (ctrl.checked)
                    count++;
            });

            if (count == 0 && cust_name != -1)
                msg = 'Please select a Template.';

            var job_desc = $('#txtJobName').val().trim();

            if (job_desc != "" && job_desc != undefined) {
                if (job_desc.length <= 5) {
                    if (msg != undefined && msg != "" && msg != null)
                        msg += '<br />Job Description should be more than 5 characters.';
                    else
                        msg = 'Job Description should be more than 5 characters.';
                }
            }
            else {
                if (msg != undefined && msg != "" && msg != null)
                    msg += '<br />Please enter Job Description.';
                else
                    msg = 'Please enter Job Description.';
            }

            if (!self.isValidJobName) {
                if (msg != undefined && msg != "" && msg != null)
                    msg += '<br />The entered Job name "' + $('#txtJobName').val() + '" already exists. Please enter unique job name.';
                else
                    msg += 'The entered Job name "' + $('#txtJobName').val() + '" already exists. Please enter unique job name.';
            }

            var in_home_date = $('#inHomeDate').val();
            if ((in_home_date == "" || in_home_date == undefined) && jobCustomerNumber != AAG && jobCustomerNumber != TRIBUNEPUBLISHING && jobCustomerNumber != SANDIEGO && jobCustomerNumber != AH && jobCustomerNumber != SPORTSAUTHORITY && jobCustomerNumber != WALGREENS && jobCustomerNumber != SAFEWAY && jobCustomerNumber != LOWES) {
                if (msg != undefined && msg != "" && msg != null)
                    msg += '<br />Please enter Inhome Date.';
                else
                    msg = 'Please enter Inhome Date.';
            }

            if ((jobCustomerNumber == CASEYS && self.gOutputData.jobTypeId != 73 && self.gOutputData.jobTypeId != 74) || jobCustomerNumber == JETS) {
                var offer_exp_date = $('#expirationDate').val();
                if (offer_exp_date == "" || offer_exp_date == undefined) {
                    if (msg != undefined && msg != "" && msg != null)
                        msg += '<br />Please enter Offer Expiration Date.';
                    else
                        msg = 'Please enter Offer Expiration Date.';
                }
            } else if (jobCustomerNumber == "1") {
                if (cust_name == "" || cust_name == undefined || cust_name == -1) {
                    if (msg != undefined && msg != "" && msg != null)
                        msg += '<br />Please enter Customer.';
                    else
                        msg = 'Please enter Customer.';
                }
            }
            if (msg != "") {
                $('#alertmsg').html(msg);
                $("#popupDialog").popup('open');
                return false;
            }
            return true;
        }
    };

    //Validated job name (description) field to avoid illegal characters.
    self.validateJobName = function () {
        var validation_regex = /^[ A-Za-z0-9,._-]*$/;
        var field_value = '';
        var alert_message = '';

        if ($('#txtJobName').val() != $('#txtJobName').attr('placeholder')) {
            field_value = $('#txtJobName').val().trim();
            if ($('#txtJobName').val().trim() == "") {
                alert_message = 'Please enter job description.';
            }
           else if (!validation_regex.test(field_value)) {
                alert_message = 'Please enter a valid job description. The following characters are not allowed "\/@#$%^&*(){}[]|?<>~`+\'".';
            }
        }

        return alert_message;
    };
    self.checkJobNameExistency = function () {
        var msg = self.validateJobName();
        if (msg != "") {
            $('#alertmsg').html(msg);
            $('#popupDialog').popup('open');
        }
        else {
            $("#icoInfo").css('display', 'none');
            if (sessionStorage.jobNumber == "-1") {
                var job_name = ($('#txtJobName').val().toLowerCase() != $('#txtJobName').attr('placeholder').toLowerCase()) ? $('#txtJobName').val().trim() : "";
                if (job_name != "") {
                    getCORS(gVerifyJobNameUrl + encodeURIComponent(job_name), null, function (data) {
                        if (data == "success") {
                            self.isValidJobName = true;
                            $('#txtJobName').css('border', 'none');
                        }
                        else {
                            self.isValidJobName = false;
                            $('#txtJobName').css('border', 'solid 1px red');
                            var msg = 'The entered Job name <b>' + job_name + '</b> already exists. Please enter unique job name.';
                            $('#alertmsg').html(msg);
                            $('#popupDialog').popup('open');
                        }
                    }, function (error_response) {
                        showErrorResponseText(error_response, true);
                    });
                }
            }
            else if (sessionStorage.jobNumber != -1) {
                var job_name = ($('#txtJobName').val().toLowerCase() != $('#txtJobName').attr('placeholder').toLowerCase()) ? $('#txtJobName').val() : "";
                if (self.gOutputData != undefined && self.gOutputData != null && self.gOutputData.jobName != undefined && self.gOutputData.jobName != "" && self.gOutputData.jobName != "") {
                    if (self.gOutputData.jobName != job_name && job_name != undefined && job_name != null && job_name != "") {
                        getCORS(gVerifyJobNameUrl + encodeURIComponent(job_name), null, function (data) {
                            if (data == "success") {
                                self.isValidJobName = true;
                                $('#txtJobName').css('border', 'none');
                            }
                            else {
                                self.isValidJobName = false;
                                $('#txtJobName').css('border', 'solid 1px red');
                                var msg = 'The entered Job name <b>' + job_name + '</b> already exists. Please enter unique job name.';
                                $('#alertmsg').html(msg);
                                $('#popupDialog').popup('open');
                            }
                        }, function (error_response) {
                            showErrorResponseText(error_response, true);
                        });
                    }
                }
            }
        }
    };
    //******************** Public Functions End ****************************
};
