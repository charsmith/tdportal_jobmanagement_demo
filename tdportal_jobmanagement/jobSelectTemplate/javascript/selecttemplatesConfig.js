﻿showOrderTypeDiv = function () {
    var customer = getCustomerNumber();
    return ([DKI, ICYNENE].indexOf(customer) > -1);
}

isCreateTemplatesDemoHints = function () {
    var customer = getCustomerNumber();   
    return ([DKI, ICYNENE, SCOTTS, ACE, BBB, JETS, AAG, TRIBUNEPUBLISHING, SANDIEGO, AH, SPORTSAUTHORITY, WALGREENS, MOTIV8, ALLIED, REDPLUM, SAFEWAY, LOWES, CASEYS, CW, DCA, KUBOTA, OH, SPORTSKING, GWA, BUFFALONEWS, PIONEERPRESS, REDPLUM, EQUINOX, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);
}

isChangeTextToTemplatePopups = function () {
    var customer = getCustomerNumber();
    return ([DKI, ICYNENE].indexOf(customer) > -1);
}

showTemplatesWelcomePopup = function () {
    var customer = getCustomerNumber();    
    return ([DKI, ICYNENE, BBB, JETS, AAG, TRIBUNEPUBLISHING, SANDIEGO, AH, SPORTSAUTHORITY, WALGREENS, MOTIV8, ALLIED, REDPLUM, SAFEWAY, LOWES, CASEYS, CW, DCA, KUBOTA, OH, SPORTSKING, GWA, SCOTTS, ACE, BUFFALONEWS, PIONEERPRESS, REDPLUM, EQUINOX, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);
}

showTemplateApproversList = function () {
    var customer = getCustomerNumber();
    return ([KUBOTA].indexOf(customer) > -1);
}

isEmailLogin = function () {
    var customer = getCustomerNumber();
    return ([SCOTTS, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1 && (sessionStorage.isEmailLogin != undefined && sessionStorage.isEmailLogin != null && sessionStorage.isEmailLogin == "true"));
}

isUpdateExpiryDate = function () {
    var customer = getCustomerNumber();
    return ([DKI, ICYNENE].indexOf(customer) > -1);
}

isClearExpiryDate = function () {
    var customer = getCustomerNumber();
    return ([DKI, ICYNENE].indexOf(customer) > -1);
}

showSavedJobTemplateChangeRule = function () {
    var customer = getCustomerNumber();
    return ([DKI, ICYNENE].indexOf(customer) > -1);
}

isReplacePreviousTemplateOnCancel = function () {
    var customer = getCustomerNumber();
    return ([DKI, ICYNENE].indexOf(customer) > -1);
}

isUpdateListSelectionInTemplateData = function () {
    var customer = getCustomerNumber();
    return ([DKI, ICYNENE].indexOf(customer) > -1);
}

isUpdateExpiryAndListInResetTemplateSelection = function () {
    var customer = getCustomerNumber();
    return ([DKI, ICYNENE].indexOf(customer) > -1);
}

hideOfferExpiryDiv = function () {
    var customer = getCustomerNumber();
    return ([DKI, ICYNENE].indexOf(customer) > -1);
}

hideEstQtyDiv = function () {
    var customer = getCustomerNumber();
    return ([CW, SCOTTS, ACE, BRIGHTHOUSE, "203", BBB, AAG, TRIBUNEPUBLISHING, SANDIEGO, AH, SPORTSAUTHORITY, WALGREENS, DATAWORKS, ALLIED, REDPLUM, SAFEWAY, LOWES, DCA, KUBOTA, OH, SPORTSKING, GWA, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);
}

isDisableJobName = function () {
    var customer = getCustomerNumber();
    return ([CW, SCOTTS, ACE, BRIGHTHOUSE, BBB, AAG, TRIBUNEPUBLISHING, SANDIEGO, AH, SPORTSAUTHORITY, WALGREENS, DATAWORKS, REDPLUM, SAFEWAY, LOWES, OH, BUFFALONEWS, PIONEERPRESS, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);
}

isDisplayThumbImageInCenter = function () {
    var customer = getCustomerNumber();    
    return ([SCOTTS, ACE, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);
}

isBronzeGoldPackageCustomer = function () {
    var customer = getCustomerNumber();
    return ([ALLIED].indexOf(customer) > -1);
}

isDisplayStateAndZipInCustomTextLocation = function () {
    var customer = getCustomerNumber();
    return ([ANDERSEN, SHERWINWILLIAMS, ACE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);
}

isNewCustomTextVariableFieldListCustomers = function () {
    var customer = getCustomerNumber();
    return ([SCOTTS, ACE, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);
}

showPricingList = function () {
    var customer = getCustomerNumber();
    var role = getUserRole();
    var show_pricing_list = false;
    switch (role) {
        case 'admin':
            show_pricing_list = ([SCOTTS, ACE, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);
            break;
        default: break;
    }
    return show_pricing_list;
}

isMakeTemplateWithProductType = function () {    
    var customer = getCustomerNumber();
    return ([SCOTTS, ACE, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);
}