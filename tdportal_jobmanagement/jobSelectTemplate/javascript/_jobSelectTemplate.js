﻿$.mobile.popup.prototype.options.history = false;
jQuery.support.cors = true;
urlString = unescape(window.location);
queryString = (urlString.indexOf("?") > -1) ? urlString.substr(urlString.indexOf("?") + 1, urlString.length) : "";
var params = queryString.split("&");
var encryptedString = (params[0] != undefined && params[0] != "") ? params[0] : "";

var gVerifyJobNameUrl = serviceURLDomain + "api/JobTicket_conflicts/";
var staticInHomeDatesUrl = "JSON/staticInHomeDates.JSON";

var jobNumber = "";
var facilityId = "";
var preSetInHomeDate = "";
var preSetJobDesc = "";
var unsubscribeUser = "";
var isOptInPeriodClosed = false;
var staticInHomeDateCollection = new scottsOptInDates();

function verifyOptInPeriodClosed(in_home_date) {    
    var is_optin_period_closed = false;
    var current_date = new Date();
    current_date = ((current_date.getMonth() + 1) + "/" + current_date.getDate() + "/" + current_date.getFullYear());
    var date_parts = null;
    if (current_date.indexOf('/') > -1) {
        date_parts = current_date.split('/');
    }    
    if (in_home_date != null && in_home_date != "" && in_home_date != undefined) {
        var optin_end_date = "";
        $.each(staticInHomeDateCollection, function (key, val) {
            if (val.optInStart == "") {
                val.optInStart = current_date;
            }
            if (Date.parse(in_home_date) == Date.parse(val.inHomeDate)) {
                optin_end_date = val.optInEnd;
                return false;
            }
        });
        if (optin_end_date != "") {
            var optIn_parts = optin_end_date.split('/');
            if (date_parts[2].length == 2)
                date_parts[2] = '20' + date_parts[2];
            var today = new Date(Number(date_parts[2]), Number(date_parts[0]) - 1, Number(date_parts[1]), 0, 0, 0, 0);
            if (optIn_parts[2].length == 2)
                optIn_parts[2] = '20' + optIn_parts[2];
            var optin_end_date = new Date(Number(optIn_parts[2]), Number(optIn_parts[0]) - 1, Number(optIn_parts[1]), 0, 0, 0, 0);
            is_optin_period_closed = (getNumberOfDaysBetweenDates(optin_end_date, today) > 0) ? true : false;
        }
    }

    return is_optin_period_closed;
}

function loadFromEmailLink() {
    var emailEncryptedURL = serviceURLDomain + 'api/Encryption_decQs/';
    var email = "";
    var user = "";
    var password = "";
    //var customerNumber = "";
    //var jobNumber = "";
    //var facilityId = "";
    var temp = {
        'qs': encryptedString
    }
    postCORS(emailEncryptedURL, JSON.stringify(temp), function (url_with_data) {
        //angie.white@caseys.com|admin_cgs1|admin_cgs1|155711|83965|1
        //var is_unsubscribe_click = false;
        var data_split = [];
        if (url_with_data.gizmoEmailString != undefined && url_with_data.gizmoEmailString != "") {
            data_split = url_with_data.gizmoEmailString.split('|');

            email = (data_split[0] != undefined && data_split[0] != "") ? data_split[0] : "";
            user = (data_split[1] != undefined && data_split[1] != "") ? data_split[1] : "";
            sessionStorage.username = user;

            password = (data_split[2] != undefined && data_split[2] != "") ? data_split[2] : "";
            jobCustomerNumber = (data_split[3] != undefined && data_split[3] != "") ? data_split[3] : "";            
            jobNumber = (data_split[4] != undefined && data_split[4] != "") ? data_split[4] : "";     
            facilityId = (data_split[5] != undefined && data_split[5] != "") ? data_split[5] : "";
            sessionStorage.authString = makeBasicAuth(user, password);

            preSetJobDesc = (data_split[6] != undefined && data_split[6] != "") ? data_split[6] : "";
            if (preSetJobDesc != "") {
                getCORS(gVerifyJobNameUrl + encodeURIComponent(preSetJobDesc), null, function (data) {
                    if (data.toLowerCase().indexOf('conflict_') > -1) {
                        jobNumber = data.substring(data.indexOf('_') + 1);
                        sessionStorage.jobNumber = jobNumber;
                    }
                }, function (error_response) {
                    if (error_response.status == 401) {
                        window.location.href = '../unAuthorized.html';
                    }
                    else {
                        showErrorResponseText(error_response, true);
                    }                    
                });
            }
            else {
                preSetJobDesc = "test job desc";
            }

            preSetInHomeDate = (data_split[7] != undefined && data_split[7] != "") ? data_split[7] : "";
            sessionStorage.jobNumber = jobNumber;
            sessionStorage.jobDesc = preSetJobDesc;
            sessionStorage.authString = makeBasicAuth(user, password);
            sessionStorage.isEmailLogin = true;
            loginAuthorization();
            //if (is_unsubscribe_click) {//update this condition based on api flag
            //    unsubscribeUser = email;
            //    sessionStorage.jobNumber = "";
            //    sessionStorage.jobDesc = "";
            //}
            //else
            if (verifyOptInPeriodClosed(preSetInHomeDate)) {
                isOptInPeriodClosed = true;
                sessionStorage.jobNumber = "";
                sessionStorage.jobDesc = "";
            }
        }

    }, function (error_response) {
        showErrorResponseText(error_response);
    });
};

function loginAuthorization() {
    var gLoginServiceUrl = serviceURLDomain + "api/Authorization";
    getCORS(gLoginServiceUrl, null, function (data) {
        if (data.Message != undefined) {
            $('#alertmsg').text(data.Message);
            $('#popupDialog').popup('open');
            return false;
        }
        else {
            captureAndNavigate(data);
            getCustNavLinks();
            customStyles();
        }

    }, function (response_error) {
        $('#viewFrame')[0].contentWindow.document.body.innerHTML = "";
        showErrorResponseText(response_error, false);
    });
}

function captureAndNavigate(data) {
    loginAndCreateReqSessions(data)    
    setCustomerInfo();
    if (jobNumber != "-1")
        getEditJobData();
}

function getEditJobData() {
    var gEditJobService = serviceURLDomain + "api/JobTicket/";
    getCORS(gEditJobService + facilityId + "/" + jobNumber + "/asdf" /*+ sessionStorage.username*/, null, function (data) {
        sessionStorage.jobDesc = data.jobName;
        var params = [];
        params.push(data);
        params.push('edit');
        makeEditJobData(params);
    }, function (error_response) {
        showErrorResponseText(error_response, false);
    });
}

if (encryptedString != "")
    loadFromEmailLink();
if (queryString != "" && jobCustomerNumber == KUBOTA) {
    var user = "user_kub";
    sessionStorage.username = "six_mango";
    sessionStorage.authString = makeBasicAuth("six_mango", "topDog123$");
    var job_number = "-1";
    sessionStorage.jobNumber = job_number;
    getUserAuthenticate();
    
}

function disableInHomeDateSelection() {
    $('#inHomeDate')[0].disabled = true;
    $('#inHomeDate').addClass('ui-disabled');
    $('#inHomeDate').css('opacity', '0.7');
    $('#inHomeDate').css('filter', 'Alpha(Opacity=90) !important');
}

//******************** Data URL Variables Start **************************
var pageObj;
appPrivileges = jQuery.parseJSON(getSessionData("appPrivileges"));
gServiceUrl = '../JSON/_jobSetup.JSON';
gOutputServiceUrl = '../JSON/_jobSetupOutput.JSON';
gTemplateUrlForAll = serviceURLDomain + 'api/Template';
var orderPAckageServiceUrl = "JSON/orderingPackage.JSON";
var pricingListServiceUrl = "JSON/pricingList.JSON";
var templateSelectOptionsUrl = "JSON/productTypes.JSON";
var packageInfo;
var pricingListInfo = {};
var dontShowHintsAgain = (localStorage.getItem('doNotShowHintsAgain') != undefined && localStorage.getItem('doNotShowHintsAgain') != null && localStorage.getItem('doNotShowHintsAgain') != "" && localStorage.getItem('doNotShowHintsAgain') != "false") ? true : false;
//******************** Data URL Variables End **************************

//******************** Page Load Events Start **************************
$('#_jobSelectTemplate').live('pagebeforecreate', function (event) {
    displayNavLinks();
    displayMessage('_jobSelectTemplate');
    commonCreateDemoHintBoxes('_jobSelectTemplate');
    var dt_html = "<span id='spnInHome'>*</span>";
    $('#schedDate').html(dt_html + appPrivileges.label_scheduledDate.toLowerCase().replace("inhome", "In Home").replace("data", "Date").replace("date", "Date")); //coming from config.xml file dynamically

    if (jobCustomerNumber == DCA || jobCustomerNumber == SPORTSKING) {
        $.getJSON(pricingListServiceUrl, function (data) {
            pricingListInfo = data[jobCustomerNumber];
            sessionStorage.pricingListInfo = JSON.stringify(pricingListInfo);
        });
    }
    //if (jobCustomerNumber == SCOTTS && sessionStorage.jobNumber == "-1" && !isEmailLogin()) {
    if ((jobCustomerNumber == SCOTTS) && sessionStorage.jobNumber == "-1" && sessionStorage.userRole == "user") {
        if (sessionStorage.asmFixedInHomeDate == undefined || sessionStorage.asmFixedInHomeDate == null || sessionStorage.asmFixedInHomeDate == "") {
            var current_date = new Date();
            current_date = ((current_date.getMonth() + 1) + "/" + current_date.getDate() + "/" + current_date.getFullYear());
            $.each(staticInHomeDateCollection, function (key, val) {
                if (val.optInStart == "") {
                    val.optInStart = current_date;
                }
                if (Date.parse(current_date) <= Date.parse(val.optInEnd) && Date.parse(current_date) >= Date.parse(val.optInStart)) {
                    sessionStorage.asmFixedInHomeDate = val.inHomeDate;
                    return false;
                }

            });
        }
    }
    $.getJSON(templateSelectOptionsUrl, function (data) {
        var template_select_options = $(data).map(function () {
            return $('<option>').val(this.value).text(this.label);
        });
        if (template_select_options.length > 0 && showTemplateApproversList()) {
            $('#ddlProductType').append("<option value='select' selected='selected'>Select</option>");
            template_select_options.appendTo('#ddlProductType');
        }
    });
    $('#divEstQty').css("display", "none");
    if (jobCustomerNumber == CASEYS || jobCustomerNumber == JETS) { //also for casey's show
        dt_html = "<span id='spnOfferExp'>*</span>";
        $('#lblOfferExp').html(dt_html + "Offer Expiration");
		$('#dvOfferExp').css("display", "block");
    }

    if ((jobCustomerNumber == CW || jobCustomerNumber == BRIGHTHOUSE || jobCustomerNumber == BBB || jobCustomerNumber == AAG || jobCustomerNumber == TRIBUNEPUBLISHING || jobCustomerNumber == SANDIEGO || jobCustomerNumber == TRIBUNEPUBLISHING || jobCustomerNumber == AH || jobCustomerNumber == SPORTSAUTHORITY || jobCustomerNumber == WALGREENS || jobCustomerNumber == DATAWORKS || jobCustomerNumber == ALLIED || jobCustomerNumber == REDPLUM || jobCustomerNumber == SAFEWAY || jobCustomerNumber == LOWES || jobCustomerNumber == OH || jobCustomerNumber == BUFFALONEWS || jobCustomerNumber == PIONEERPRESS || isEmailLogin()) && sessionStorage.jobNumber != "-1") {
        $('#txtJobName').addClass('ui-disabled');
        $('#txtJobName').css('opacity', '0.7');
        $('#txtJobName').css('filter', 'Alpha(Opacity=90) !important');
        if ($('#inHomeDate').length > 0 && isEmailLogin()) {
            disableInHomeDateSelection();
        }
    }
	else if (([SCOTTS].indexOf(jobCustomerNumber) > -1 && getUserRole() != "admin" && sessionStorage.jobNumber != "-1")) {
        disableInHomeDateSelection();
    }
    if (jobCustomerNumber == CASEYS || jobCustomerNumber == JETS || jobCustomerNumber == BUFFALONEWS || jobCustomerNumber == PIONEERPRESS || jobCustomerNumber == REDPLUM || jobCustomerNumber == EQUINOX || showOrderTypeDiv()) {
        $('#dvOrderType').css("display", "block");
        $('#schedDate').html(dt_html + "1st In Home Date:");
    }    
    if ((jobCustomerNumber == REGIS)) {        
        $('#dvSalonBrand').attr("style", "display:none;");
    }
    else {
        $('#dvSalonBrand').attr("style", "display:none;");
    }
    $("#navLinksPanel").on("panelopen", function (event, ui) {
        sessionStorage.isNavPanelOpen = true;
    });
    pageObj = new selectTemplate();
    
    if ((isCreateTemplatesDemoHints())) {
        setDemoHintsSliderValue();
        window.setTimeout(function loadHints() {
            createDemoHints("jobSelectTemplate");
        }, 200);
    }

    //$("#navHints").bind({
    //    popupafterclose: function (event, ui) {
    //        if (!dontShowHintsAgain && (sessionStorage.showDemoHints == undefined || sessionStorage.showDemoHints == null || sessionStorage.showDemoHints == "" || sessionStorage.showDemoHints == "on")) {
    //            $('#walkthroughSteps').popup('open', { positionTo: '#namingHint' });
    //        }            
    //    }
    //});
});
function getTemplatePackageInfo() {
    $.getJSON(orderPAckageServiceUrl, function (data) {
        packageInfo = data;
    });
}
$(document).keydown(function (e) {
    if (e.keyCode === 8) {
        var d = e.srcElement || e.target;
        if (jobCustomerNumber == AAG || jobCustomerNumber == TRIBUNEPUBLISHING || jobCustomerNumber == SANDIEGO || jobCustomerNumber == AH || jobCustomerNumber == SPORTSAUTHORITY || jobCustomerNumber == WALGREENS || jobCustomerNumber == SAFEWAY || jobCustomerNumber == LOWES || jobCustomerNumber == OH) {
            if ($(d).attr('id') == "inHomeDate" || e.target.nodeName == "DIV")
                return false;
        }
       else {
            if (($(d).attr('id') == "inHomeDate" || e.target.nodeName == "DIV") && ($(d).attr('id') != "_jobSelectTemplate"))
                return false;
        }
    }
});
$(document).on('pageshow', '#_jobSelectTemplate', function (event) {
    if (jobCustomerNumber == DCA || jobCustomerNumber == KUBOTA || jobCustomerNumber == OH || jobCustomerNumber == TRIBUNEPUBLISHING || jobCustomerNumber == SANDIEGO || jobCustomerNumber == SPORTSKING || jobCustomerNumber == GWA) {
        $('div[data-role="content"] h2').html("Templates");
    }
    if (!showTemplateApproversList())
        $('#divProductType').css('display', 'none');
  
    jQuery("#inHomeDate").datepicker({
        minDate: new Date(2010, 0, 1),
        dateFormat: 'mm/dd/yy',
        constrainInput: true,
        beforeShowDay: function (date) {
            if (jobCustomerNumber == MOTIV8)
                return displayBlockDays(date, 0);
            else if(sessionStorage.asmFixedInHomeDate != undefined && sessionStorage.asmFixedInHomeDate != null && sessionStorage.asmFixedInHomeDate != "")
                return displayBlockDaysOtherthanSelected(date);
            else if (showFixedInHomeDatesForSelection())
                return displaySelectedDays(date);
            else if (sessionStorage.userRole != "admin")
                return displayBlockDays(date, ((jobCustomerNumber == DCA || jobCustomerNumber == SPORTSKING) ? 15 : ((jobCustomerNumber == OH) ? 7 : 21)));
            else
                return displayBlockDays(date, 1);
        }        
    });
    if (jobCustomerNumber == ALLIED) {
        jQuery("#inHomeDate2").datepicker({
            minDate: new Date(2010, 0, 1),
            dateFormat: 'mm/dd/yy',
            constrainInput: true,
            beforeShowDay: function (date) {
                if (jobCustomerNumber == MOTIV8)
                    return displayBlockDays(date, 0);
                else if (sessionStorage.userRole != "admin")
                    return displayBlockDays(date, 21);
                else
                    return displayBlockDays(date, 1);
            }            
        });
        jQuery("#inHomeDate3").datepicker({
            minDate: new Date(2010, 0, 1),
            dateFormat: 'mm/dd/yy',
            constrainInput: true,
            beforeShowDay: function (date) {
                if (jobCustomerNumber == MOTIV8)
                    return displayBlockDays(date, 0);
                else if (sessionStorage.userRole != "admin")
                    return displayBlockDays(date, 21);
                else
                    return displayBlockDays(date, 1);
            }            
        });
    }
    if (jobCustomerNumber != AAG && jobCustomerNumber != TRIBUNEPUBLISHING && jobCustomerNumber != SANDIEGO && jobCustomerNumber != AH && jobCustomerNumber != SPORTSAUTHORITY && jobCustomerNumber != WALGREENS && jobCustomerNumber != SAFEWAY && jobCustomerNumber != LOWES) {
        if (sessionStorage.userRole != "admin" && sessionStorage.jobNumber == "-1")
            $("#inHomeDate").datepicker('setDate', '+' + ((jobCustomerNumber == DCA || jobCustomerNumber == SPORTSKING) ? 15 : 21) + 'd');
        else
            $("#inHomeDate").datepicker('setDate', '+1d');
    }
    else {
        $("#dvInHomeDate").css('display', 'none');
    }
    
    var is_closed_job = false;
    if (sessionStorage.isClosedJob != undefined && sessionStorage.isClosedJob != null && sessionStorage.isClosedJob != "" && JSON.parse(sessionStorage.isClosedJob)) {
        is_closed_job = true;
        $('#alertmsg').text("This job is closed. Any changes made in the edit screens will not be saved. These screens are provided only to review previous selections and proofs. If you have any questions, please contact a site administrator.");
        $('#popupDialog').popup('open');
    }

    if (jobCustomerNumber == DCA || jobCustomerNumber == SPORTSKING)
        $("#spanPricingNote").css('display', 'block');

    if (jobCustomerNumber == CASEYS || jobCustomerNumber == CW || jobCustomerNumber == AAG || jobCustomerNumber == OH || jobCustomerNumber == BUFFALONEWS || jobCustomerNumber == PIONEERPRESS || jobCustomerNumber == REDPLUM || jobCustomerNumber == EQUINOX || isChangeTextToTemplatePopups()) {
        $('#navHints p a').text('Turn Hints Off');
        $('#walkthroughSteps p a').text('Turn Hints Off');
        $('#dvWelcomeDemoPopup p a').text('Turn Hints Off');
    }
    var should_lock_job = (sessionStorage.isEmailLogin != undefined && sessionStorage.isEmailLogin != null && sessionStorage.isEmailLogin == "true" && !isOptInPeriodClosed) ? _shouldLockThisJob() : false;
    if (!should_lock_job && !isOptInPeriodClosed && !is_closed_job && !dontShowHintsAgain && (showTemplatesWelcomePopup()) && (sessionStorage.showDemoHints == undefined || sessionStorage.showDemoHints == null || sessionStorage.showDemoHints == "" || sessionStorage.showDemoHints == "on") && (sessionStorage.isTemplateDemoHintsDisplayed == undefined || sessionStorage.isTemplateDemoHintsDisplayed == null || sessionStorage.isTemplateDemoHintsDisplayed == "false")) {
        window.setTimeout(function () {
            if (sessionStorage.isEmailLogin != undefined && sessionStorage.isEmailLogin != null && sessionStorage.isEmailLogin == "true") {
                commonOpenHintBox('dvwelcomeOptInPop', 'window');
                getCustNavLinks();
                createNavLinks();
            }
            else
                commonOpenHintBox('dvWelcomeDemoPopup', 'window');
        }, 500);
        sessionStorage.isTemplateDemoHintsDisplayed = true;      
    }

    //show ONLY for TD.        
    if ((jobCustomerNumber == "1")) {
        $('#divCustomer').attr("style", "display:block;");
        $('#divCustomer').removeAttr('disabled');  //posted to page preferences
        if (sessionStorage.jobNumber != "-1")
            $('#divCustomer').attr('disabled', 'disabled');
        
        pageObj.loadCustomerInfo();
    }
    window.setTimeout(function () {
        pageObj.loadData();
    }, 1000);
    
    if (hideEstQtyDiv()) //posted to  page preferences
        $('#divEstQty').hide();

    if ((jobCustomerNumber == CASEYS && sessionStorage.userRole == "user") || (jobCustomerNumber == JETS || jobCustomerNumber == BUFFALONEWS || jobCustomerNumber == PIONEERPRESS || jobCustomerNumber == REDPLUM) && sessionStorage.userRole != "admin") //for CGS and normal user should not get Save button in any of the pages
        $('#btnSaveTop').css('visibility', 'hidden'); //posted to  page preferences
    if (!isEmailLogin() && $('#inHomeDate').length > 0 && (([SCOTTS].indexOf(jobCustomerNumber) == -1) || ([SCOTTS].indexOf(jobCustomerNumber) > -1 && sessionStorage.userRole == "admin"))) {
        $('#inHomeDate')[0].disabled = false;
        $('#inHomeDate').removeClass('ui-disabled'); //TODO: need to check the code while doing page preferences
        $('#inHomeDate').css('opacity', '1.0');
    }
    $('#txtJobName').removeClass('ui-disabled');
    $('#txtJobName').css('opacity', '1.0');

    if (parseInt(sessionStorage.jobNumber) > -1) {
        $('#txtJobName').addClass('ui-disabled');
        $("#txtJobName").attr('disabled', true);
        $('#txtJobName').css('opacity', '0.7');
        $('#txtJobName').css('filter', 'Alpha(Opacity=90) !important');
    }
    persistNavPanelState();        
    $('#sldrShowHints').slider("refresh");
    //Verify overall approval status for this job
    if (should_lock_job || isOptInPeriodClosed) {
        //if (unsubscribeUser != "") {
        //    $('#_approverSpnText').css({ 'color': 'grey', 'font-weight': 'normal', 'padding-top': '30px' });
        //    $('#_approverSpnText h3').html("We're sorry to see you go. <i>" + unsubscribeUser + "</i> has been unsubscribed from future Gizmo emails.");
        //}
        //else 
        if (isOptInPeriodClosed) {
            var next_optin_dates = ""
            $.each(staticInHomeDateCollection, function (key, val) {
                if (Date.parse(preSetInHomeDate) < Date.parse(val.inHomeDate)) {
                    next_optin_dates = val;
                    return false;
                }
            });
            if (next_optin_dates != "") {
                $('#_approverSpnText h3').html("Opt-In for the " + preSetInHomeDate + " In-Home has closed. Opt-In for the In-home beginning " + next_optin_dates.inHomeDate + " will begin on " + next_optin_dates.optInStart + ".");
            }
            else {
                $('#_approverSpnText h3').html("Opt-In for the March 15 In-Home has closed. Opt-In for the In-home beginning March 29 will begin on March 1st.");
            }
        }
        $('#_approverSpnText').show();
        $('#btnSaveTop').css('visibility', 'hidden');
        $('#btnContinueTop').css('visibility', 'hidden');
        $('a[data-icon=bars]').css('display', 'none');
        $("#navLinksPanel").hide();
        $("div[data-role=content]").hide();
    }
});

function loadProductTemplates() {
    pageObj.buildLists();
}

//function closeOptInHintsPopup() {
//    $('#dvwelcomeOptInPop').popup('close');
//    $('#txtJobName').focus();
// 	$('#navHints').popup('open', { positionTo: 'window' });
//}

//function manageDemoHints() {
//    $('#navHints').popup('close');
//    $('#walkthroughSteps').popup('open', { positionTo: '#namingHint' });
//}

//******************** Page Load Events End **************************
