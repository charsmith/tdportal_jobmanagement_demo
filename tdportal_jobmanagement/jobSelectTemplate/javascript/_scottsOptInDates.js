﻿var scottsOptInDates = function () {
    return [{
        "inHomeDate": "10/04/2017",
        "optInStart": "09/06/2017",
        "optInEnd": "09/12/2017"
    }, {
        "inHomeDate": "10/18/2017",
        "optInStart": "09/18/2017",
        "optInEnd": "09/26/2017"
    }, {
        "inHomeDate": "11/08/2017",
        "optInStart": "10/09/2017",
        "optInEnd": "10/16/2017"
    }, {
        "inHomeDate": "11/22/2017",
        "optInStart": "10/23/2017",
        "optInEnd": "10/30/2017"
    }, {
        "inHomeDate": "12/06/2017",
        "optInStart": "11/06/2017",
        "optInEnd": "11/13/2017"
    }, {
        "inHomeDate": "12/20/2017 ",
        "optInStart": "11/20/2017",
        "optInEnd": "11/27/2017"
    }];
}