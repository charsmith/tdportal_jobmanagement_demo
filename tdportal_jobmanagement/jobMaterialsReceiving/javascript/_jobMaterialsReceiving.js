﻿$.mobile.popup.prototype.options.history = false;
jQuery.support.cors = true;


//******************** Data URL Variables Start **************************
var pageObj;
appPrivileges = jQuery.parseJSON(getSessionData("appPrivileges"));

var gServicePrintersUrl = serviceURLDomainIPInternal + "api/MaterialsPrinters"
//var gServiceMaterialUrl = "JSON/_jobMaterialRecvData.JSON";
var gServiceMaterialUrl = serviceURLDomainIPInternal + "api/Materials/";
var gServiceMaterialPostUrl = serviceURLDomainIPInternal + "api/MaterialsUpdate";
var gServiceSkidLocSaveUrl = serviceURLDomainIPInternal + "api/MaterialLocationInsert";
var gServiceSkidLocAssignedUrl = serviceURLDomainIPInternal + "api/MaterialLocationAssigned/";
var gServiceSkidLocDeleteUrl = serviceURLDomainIPInternal + "api/MaterialLocationDelete";
var gServiceMaterialReceivingUrl = serviceURLDomainIPInternal + "api/Receiving/"
var gServiceMaterialReceivingSaveUrl = serviceURLDomainIPInternal + "api/ReceivingUpdate"
var gAssignedSkidLocations = [];
var gMaterialReceivedList = [];
var gServiceMaterialData;
var isPageLoaded = false;
var printersList = [];
//******************** Data URL Variables End **************************

//******************** Page Load Events Start **************************

$(window).on('popupbeforeposition', 'div:jqmData(role="popup")', function () {
    var notDismissible = $(this).jqmData('dismissible') === false;
    if (notDismissible) {
        $('.ui-popup-screen').off();
    }
});

$('#_jobMaterialsReceiving').live('pagebeforecreate', function (event) {
    displayNavLinks();
    displayMessage('_jobMaterialsReceiving');
    $("#navLinksPanel").on("panelopen", function (event, ui) {
        sessionStorage.isNavPanelOpen = true;
    });
    loadPrinters();
    searchSkids();
});
$(document).keydown(function (e) {
    if (e.keyCode === 8) {
        var d = e.srcElement || e.target;
        if (($(d).attr('readonly') != undefined && $(d).attr('readonly') != null) || ($.browser.msie? $(d).parent().parent().attr('id') : $(d).attr('id')) == "_jobMaterialsReceiving") {
            return false;
        }
    }
});

$(document).on('pageshow', '#_jobMaterialsReceiving', function (event) {
    persistNavPanelState();
    makeMaterialRecvData();
});
var mapping = { 'ignore': ["__ko_mapping__"] }
function loadPrinters() {
    //
    if (sessionStorage.materialsPrintersList == undefined || sessionStorage.materialsPrintersList == null || sessionStorage.materialsPrintersList == "") {
        getCORS(gServicePrintersUrl, null, function (data) {
            printersList = data;
            sessionStorage.materialsPrintersList = JSON.stringify(printersList);

        }, function (error_response) {
            showErrorResponseText(error_response,true);
        });
    }
    else {
        printersList = $.parseJSON(sessionStorage.materialsPrintersList);
    }
}
function makeMaterialRecvData() {
    if (sessionStorage.adOrderId != undefined && sessionStorage.adOrderId != null && sessionStorage.adOrderId != null) {
        getCORS(gServiceMaterialUrl + sessionStorage.adOrderId, null, function (data) {
            gServiceMaterialData = data;
            getCORS(gServiceSkidLocAssignedUrl + sessionStorage.adOrderId, null, function (data) {
                gAssignedSkidLocations = data;
                //displayAssignedSkids(data);
            }, function (error_response) {
                showErrorResponseText(error_response,true);
            });

            if (!isPageLoaded)
                pageObj = new adMaterialVM();

            getCORS(gServiceMaterialReceivingUrl + sessionStorage.adOrderId, null, function (data) {
                var temp_material_rec_list = [];
                gMaterialReceivedList = data;
                if (data.length == 0) {
                    temp_material_rec_list.push(ko.mapping.fromJS({
                        "receivedPk": -1,
                        "printerId": "",
                        "adorderId": sessionStorage.adOrderId,
                        "receivedDate": "",
                        "receivedBOL": "",
                        "weightVerifiedAmount": "",
                        "verifiedWeight": "",
                        "userName": sessionStorage.username,
                        "printerName": ""
                    }, {}));
                }
                else {
                    $.each(data, function (key, val) {
                        val["printerName"] = pageObj.getPrinterName(val.printerId);
                        val.receivedDate = $.datepicker.formatDate('mm/dd/yy', new Date(val.receivedDate));
                        if (val.receivedBol != undefined) val["receivedBOL"] = val.receivedBol;
                        delete val.receivedBol;
                        temp_material_rec_list.push(ko.mapping.fromJS(val, {}));
                    });
                }
                pageObj.materialsReceiving(temp_material_rec_list);
                if (!isPageLoaded) {
                    pageObj.materialsReceivingInfoToDisplay(pageObj.materialsReceiving()[0]);
                    pageObj.currentShipment("0");
                }
                else {
                    pageObj.materialsReceivingInfoToDisplay(pageObj.materialsReceiving()[pageObj.currentShipment()]);
                }
            }, function (error_response) {
                showErrorResponseText(error_response,true);
            });
            if (!isPageLoaded) {
                ko.applyBindings(pageObj);
                isPageLoaded = true;
                if (pageObj.materialsReceiving().length > 1) {
                    $('#btnCombined').trigger('click');
                }
            }
            $('#ulSkidLocations').listview('refresh');
            $('#ctrlGrpShipments').trigger('create');
            $('#ctrlGrpShipments').find('a').first().addClass('ui-first-child');
            $('#ctrlGrpShipments').find('a').last().addClass('ui-last-child');

        }, function (error_response) {
            pageObj = new adMaterialVM();
            ko.applyBindings(pageObj);
            showErrorResponseText(error_response,true);
        });

    }
    else {
    }
    //    $.getJSON(gServiceMaterialUrl, function (data) {
    //        gServiceMaterialData = data;
    //        ko.applyBindings(new adMaterialVM().materialDataList());
    //    });
}

var skidLocationsVM = function (skid_location) {
    var self = this;
    ko.mapping.fromJS(skid_location, {}, self);
    self.computedSkidName = ko.computed(function () {
        return self.aisle() + '.' + self.row() + '.' + self.level();
    });
};

var adMaterialVM = function () {
    var self = this;
    self.materialDataList = ko.observableArray([]);
    self.materialsReceiving = ko.observableArray([]);
    self.materialInfoToDisplay = ko.observable({});
    self.assignedSkidLocations = ko.observableArray([]);
    self.removedSkidLocations = ko.observableArray([]);
    self.selectedSkidsToAdd = ko.observableArray([]);
    self.materialsReceivingInfoToDisplay = ko.observable();
    self.previousShipmentInfo = ko.observable({});
    self.currentShipment = ko.observable();
    self.previousCommonMaterialInfo = ko.observableArray([]);
    //Generating Materials info objects - START ---
    var temp_material_list = [];
    $.each(gServiceMaterialData, function (key, val) {
        val.lastReceivedDate = $.datepicker.formatDate('mm/dd/yy', new Date(val.lastReceivedDate))
        //self.previousCommonMaterialInfo.push(val);
        temp_material_list.push(ko.mapping.fromJS(val, {}));
        self.previousCommonMaterialInfo.push(ko.mapping.fromJS(val, {}));
    });
    self.materialDataList(temp_material_list);
    self.materialInfoToDisplay(self.materialDataList()[0]);
    if (self.materialDataList().length > 0) {
        $('#btnEditShipment').css('display', '');
        //$("#grdInsertDetails :input").attr("readonly", "true");
        //        $("#grdInsertDetails :input").css('color', 'black');
        //if ((self.materialInfoToDisplay().height() !== "") || (self.materialInfoToDisplay().width !== "") || (self.materialInfoToDisplay().thickness !== "") || (self.materialInfoToDisplay().thickness !== "") || (self.materialInfoToDisplay().shortage !== "") || (self.materialInfoToDisplay().comments !== "")) {
        $('#btnSaveMaterials').addClass('ui-disabled');
        $('#btnSaveMaterials')[0].disabled = true;
    }
    //Generating Materials info objects - END ---

    //Generating skid locations object -- START ---
    var temp_skid_locations = [];
    $.each(gAssignedSkidLocations, function (key, val) {
        temp_skid_locations.push(new skidLocationsVM(val));
    });
    self.assignedSkidLocations(temp_skid_locations);
    //Generating skid locations object -- END ---

    self.printers = ko.observableArray([]);
    self.printers(printersList);

    self.isCommonDataChanged = function () {
        var temp_data = ko.mapping.toJS(self.materialInfoToDisplay(), mapping);
        var temp_prev_data = ko.mapping.toJS(self.previousCommonMaterialInfo()[0], mapping);
        if ((parseFloat((temp_data.height == null || temp_data.height == "") ? 0 : temp_data.height) !== parseFloat((temp_prev_data.height == null || temp_prev_data.height == "") ? 0 : temp_prev_data.height)) ||
                (parseFloat((temp_data.width == null || temp_data.width == "") ? 0 : temp_data.width) !== parseFloat((temp_prev_data.width == null || temp_prev_data.width == "") ? 0 : temp_prev_data.width)) ||
                (parseFloat((temp_data.thickness == null || temp_data.thickness == "") ? 0 : temp_data.thickness) !== parseFloat((temp_prev_data.thickness == null || temp_prev_data.thickness == "") ? 0 : temp_prev_data.thickness)) ||
                (parseInt((temp_data.pageCount == null || temp_data.pageCount == "") ? 0 : temp_data.pageCount) !== parseInt((temp_prev_data.pageCount == null || temp_prev_data.pageCount == "") ? 0 : temp_prev_data.pageCount)) ||
                (parseInt((temp_data.shortage == null || temp_data.shortage == "") ? 0 : temp_data.shortage) !== parseInt((temp_prev_data.shortage == null || temp_prev_data.shortage == "") ? 0 : temp_prev_data.shortage)) ||
                (temp_data.comments !== temp_prev_data.comments)) {
            $('#btnSaveMaterials')[0].disabled = false;
            $('#btnSaveMaterials').removeClass('ui-disabled');
        }
        else {
            $('#btnSaveMaterials')[0].disabled = true;
            $('#btnSaveMaterials').addClass('ui-disabled');
        }
    };

    self.getPrinterName = function (printer_id) {
        var printer_name = "";
        $.each(printersList, function (key, val) {
            if (val.printId == printer_id) {
                printer_name = val.printerName;
                return false;
            }
        });
        return printer_name;
    };

    self.saveData = function () {
        var temp_data = ko.mapping.toJS(self.materialInfoToDisplay(), mapping);
        if (Object.keys(temp_data).length > 0) {
            $.each(temp_data, function (key, val) {
                if (key.toLowerCase() != "orderid" && key.toLowerCase() != "height" && key.toLowerCase() != "width" && key.toLowerCase() != "thickness" && key.toLowerCase() != "shortage" && key.toLowerCase() != "comments") {
                    delete temp_data[key];
                }
                else {
                    if (key.toLowerCase() == "orderid")
                        temp_data[key] = sessionStorage.adOrderId;
                    else if (key.toLowerCase() != "comments")
                        temp_data[key] = parseFloat(temp_data[key]);
                }
                temp_data["userName"] = sessionStorage.username;
            });
            postCORS(gServiceMaterialPostUrl, JSON.stringify(temp_data), function (data) {
                //$('#btnSaveMaterials').attr('disabled', 'disabled');
                $('#btnSaveMaterials').addClass('ui-disabled');
                $('#btnSaveMaterials')[0].disabled = true;
                //$("#grdInsertDetails :input").attr("disabled", true);
                $('#alertmsg').html('Your order has been saved successfully');
                $('#popupDialog').popup('open');
            }, function (error_response) {
                showErrorResponseText(error_response,true);
            });
        }
    };

    self.navigateShipment = function (data, el) {
        var ctrl = el.currentTarget || el.target;
        if ($(ctrl).data('value') == "combined") {
            self.currentShipment("combined");
            var temp_data = ko.mapping.toJS(self.materialsReceiving(), mapping);
            var printer_name = "";

            $.each(temp_data, function (key1, val1) {
                if (val1.printerId != undefined && val1.printerId != null && val1.printerId != "") {
                    printer_name += (printer_name != "") ? ', ' + self.getPrinterName(val1.printerId) : self.getPrinterName(val1.printerId);
                }
            });

            temp_rece_shipment = {
                "receivedPk": -1,
                "printerId": "",
                "adorderId": sessionStorage.adOrderId,
                "receivedDate": self.materialDataList()[0].lastReceivedDate(),
                "receivedBOL": self.materialDataList()[0].totalReceivedBOL(),
                "weightVerifiedAmount": self.materialDataList()[0].totalWeightVerifiedAmount(),
                "verifiedWeight": "",
                "userName": sessionStorage.username,
                "printerName": printer_name
            };
            pageObj.materialsReceivingInfoToDisplay(ko.mapping.fromJS(temp_rece_shipment));
            $('#btnEditShipment').css('display', 'none');
            $('#dvPieceWgtLbs').css('display', 'none');
        }
        else {
            var shipment_id = $(ctrl).data('value');
            self.currentShipment(shipment_id);
            //self.materialInfoToDisplay(self.materialsReceiving()[shipment_id]);
            self.materialsReceivingInfoToDisplay(self.materialsReceiving()[shipment_id]);
            $('#btnEditShipment').css('display', '');
            $('#dvPieceWgtLbs').css('display', 'block');
        }
        $('#ctrlGrpShipments').find('a').buttonMarkup({ theme: 'f' });
        $('#' + ctrl.id).buttonMarkup({ theme: 'a' });
    };

    self.cancelShipment = function () {
        if (gMaterialReceivedList.length > 0 && self.materialsReceiving().length > 0) {
            if ($('#btnPopupAddOrUpdateShipment').text().indexOf('Add') > -1) {
                //self.materialsReceiving().splice(self.materialsReceiving().length - 1, 1);
                pageObj.materialsReceiving.pop(pageObj.materialsReceivingInfoToDisplay());
                if (pageObj.materialsReceiving().length > 0)
                    pageObj.materialsReceivingInfoToDisplay(self.materialsReceiving()[0]);
                var shipment_id = pageObj.materialsReceiving().length;
                $('#ctrlGrpShipments').trigger('create');
                $('#ctrlGrpShipments').find('a').buttonMarkup({ theme: 'f' });
                $('#btnShipment' + (shipment_id - 1)).buttonMarkup({ theme: 'a' });
                $('#ctrlGrpShipments').find('a').removeClass('ui-first-child');
                $('#ctrlGrpShipments').find('a').removeClass('ui-last-child');
                $('#ctrlGrpShipments').find('a').first().addClass('ui-first-child');
                $('#ctrlGrpShipments').find('a').last().addClass('ui-last-child');
            }
            else {
                self.materialsReceiving()[self.currentShipment()] = ko.mapping.fromJS(self.previousShipmentInfo(), {});
                pageObj.materialsReceivingInfoToDisplay(self.materialsReceiving()[self.currentShipment()]);
            }
        }
        else {
            if (self.materialsReceiving()[0].receivedPk() == -1) {
                var temp_rece_shipment = {};
                temp_rece_shipment = {
                    "receivedPk": -1,
                    "printerId": "",
                    "adorderId": sessionStorage.adOrderId,
                    "receivedDate": "",
                    "receivedBOL": "",
                    "weightVerifiedAmount": "",
                    "verifiedWeight": "",
                    "userName": sessionStorage.username,
                    "printerName": ""
                };
                self.materialsReceiving()[0] = ko.mapping.fromJS(temp_rece_shipment, {});
                self.materialsReceivingInfoToDisplay(ko.mapping.fromJS(temp_rece_shipment, {}));
            }
        }
        $('#popupShipment').popup('close');
    };

    self.makeNewShipment = function () {
        if (self.materialsReceiving().length > 0 && self.materialsReceiving()[0].receivedPk() != -1) {
            self.addNewShipment();
        }
        $('#btnPopupAddOrUpdateShipment').text('Add Shipment');
        $('#popupShipment').popup('open');
    };

    self.addNewShipment = function () {
        var temp_rece_shipment = {};
        temp_rece_shipment = {
            "receivedPk": -1,
            "printerId": "",
            "adorderId": sessionStorage.adOrderId,
            "receivedDate": "",
            "receivedBOL": "",
            "weightVerifiedAmount": "",
            "verifiedWeight": "",
            "userName": sessionStorage.username,
            "printerName": ""
        };
        pageObj.materialsReceiving.push(ko.mapping.fromJS(temp_rece_shipment));
        var shipment_id = self.materialsReceiving().length;
        pageObj.materialsReceivingInfoToDisplay(pageObj.materialsReceiving()[shipment_id - 1]);
        pageObj.currentShipment(shipment_id - 1);
        $('#dvPieceWgtLbs').css('display', 'block');
        $('#ctrlGrpShipments').trigger('create');
        $('#ctrlGrpShipments').find('a').buttonMarkup({ theme: 'f' });
        $('#btnShipment' + (shipment_id - 1)).buttonMarkup({ theme: 'a' });
        $('#ctrlGrpShipments').find('a').removeClass('ui-first-child');
        $('#ctrlGrpShipments').find('a').removeClass('ui-last-child');
        $('#ctrlGrpShipments').find('a').first().addClass('ui-first-child');
        $('#ctrlGrpShipments').find('a').last().addClass('ui-last-child');
    };

    self.editShipment = function (data) {
        if (self.currentShipment() !== "" && self.currentShipment() != "combined") {
            self.previousShipmentInfo(ko.mapping.toJS(self.materialsReceivingInfoToDisplay(), mapping));
            $('#btnPopupAddOrUpdateShipment').text('Update Shipment');
            $('#popupShipment').popup('open');
        }
    };

    self.addUpdateShipment = function () {
        if (materialFieldsValidation())
            self.saveMaterialsReceivingInfo();
    };

    self.saveMaterialsReceivingInfo = function () {
        var materials_receiving_json = ko.mapping.toJS(self.materialsReceivingInfoToDisplay(), mapping);
        delete materials_receiving_json["printerName"];
        postCORS(gServiceMaterialReceivingSaveUrl, JSON.stringify(materials_receiving_json), function (data) {
            msg = "Shipment ";
            if ($('#btnPopupAddOrUpdateShipment').text().indexOf('Add') > -1)
                msg += "added";
            else
                msg += "updated";
            msg += " successfully.";
            $('#alertmsg').html(msg);
            $('#popupShipment').popup('close');
            $('#popupDialog').popup('open');

            makeMaterialRecvData();
            //Updating the objects in VM -- START --
            var temp_material_list = [];
            $.each(gServiceMaterialData, function (key, val) {
                val.lastReceivedDate = $.datepicker.formatDate('mm/dd/yy', new Date(val.lastReceivedDate))
                //self.previousCommonMaterialInfo.push(val);
                temp_material_list.push(ko.mapping.fromJS(val, {}));
                self.previousCommonMaterialInfo.push(val);
            });
            self.materialDataList(temp_material_list);
            //updating the objects in VM -- END --

            //if ($('#btnPopupAddOrUpdateShipment').find('.ui-btn-text').text().indexOf('Add') > -1) {
            $('#ctrlGrpShipments').trigger('create');
            $('#ctrlGrpShipments').find('a').buttonMarkup({ theme: 'f' });
            $('#btnShipment' + (self.currentShipment())).buttonMarkup({ theme: 'a' });
            $('#ctrlGrpShipments').find('a').removeClass('ui-first-child');
            $('#ctrlGrpShipments').find('a').removeClass('ui-last-child');
            $('#ctrlGrpShipments').find('a').first().addClass('ui-first-child');
            $('#ctrlGrpShipments').find('a').last().addClass('ui-last-child');
            // }
        }, function (error_response) {
            showErrorResponseText(error_response,true);
        });
    };

    self.deleteSkidLocations = function (data, el) {
        var ctrl = el.currentTarget || el.target;
        var location_index = $(ctrl).attr('data-index');
        self.assignedSkidLocations.splice(location_index, 1);
        self.removedSkidLocations.push(data);
        var temp_skid_location_json = ko.mapping.toJS(data, mapping);
        var selected_locations_json =
            {
                "adOrderId": sessionStorage.adOrderId,
                "locationPk": (temp_skid_location_json.pk == undefined) ? temp_skid_location_json.locationPk : temp_skid_location_json.pk,
                "userName": sessionStorage.username
            };
        if (Object.keys(temp_skid_location_json).length > 0) {
            postCORS(gServiceSkidLocDeleteUrl, JSON.stringify(selected_locations_json), function (data) {
                $('#alertmsg').html('Material Location has been deleted successfully');
                $('#popupDialog').popup('open');
            }, function (error_response) {
                showErrorResponseText(error_response,true);
            });
        }
        $('#ulSkidLocations').listview('refresh');
    };
    self.selectASkid = function () {
        var skid_to_add = $('#hdnSelectedSkidInfo').val();
        if (skid_to_add != "") {
            skid_to_add = jQuery.parseJSON(skid_to_add);
            self.selectedSkidsToAdd.push(new skidLocationsVM(skid_to_add));
            $('#txtSkidLocation').val('');
            $('#hdnSelectedSkidInfo').val('');
            $('#txtSkidLocation').removeAttr('data-value');
            $('#ulSelectedSkidLocations').listview('refresh');
        }
    };

    self.addSelectedSkidLocations = function () {
        var temp_selected_locations = ko.mapping.toJS(self.selectedSkidsToAdd, mapping);
        var found_locations = [];
        $.each(temp_selected_locations, function (key, val) {
            $.each(self.removedSkidLocations(), function (key1, val1) {
                if (val1.aisle() + '.' + val1.row() + '.' + val1.level() == val.aisle + '.' + val.row + '.' + val.level) {
                    self.removedSkidLocations.splice(key1, 1);
                    return false;
                }
            });
        });

        var locations_post_json = [];
        var selected_locations_json = {};
        var temp_loc = {};
        var loc_indx;
        $('#popupSkidLocations').popup('close');
        $.each(temp_selected_locations, function (key, val) {
            var loc_details = [];
            loc_details = $(this).text().split('.');
            selected_locations_json =
            {
                "adOrderId": sessionStorage.adOrderId,
                "locationPk": val.locationPk,
                "userName": sessionStorage.username
            };
            temp_loc = val;
            loc_indx = key;
            if (Object.keys(selected_locations_json).length > 0) {
                postCORS(gServiceSkidLocSaveUrl, JSON.stringify(selected_locations_json), function (data) {
                    getCORS(gServiceSkidLocAssignedUrl + sessionStorage.adOrderId, null, function (data) {
                        gAssignedSkidLocations = data;
                        //Generating skid locations object -- START ---
                        var temp_skid_locations = [];
                        $.each(gAssignedSkidLocations, function (key, val) {
                            temp_skid_locations.push(new skidLocationsVM(val));
                        });
                        self.assignedSkidLocations(temp_skid_locations);
                        //Generating skid locations object -- END ---
                    }, function (error_response) {
                        showErrorResponseText(error_response,true);
                    });
                    //self.assignedSkidLocations.push(new skidLocationsVM(temp_loc));
                    if (loc_indx == temp_selected_locations.length - 1) {
                        msg = 'Material Location(s) have been saved successfully.';
                        //                        if (found_locations.length > 0)
                        //                            msg += '<br /> The following Skid Locations have already been selected: <br />' + found_locations.join(',');
                        $('#alertmsg').html(msg);
                        $('#popupDialog').popup('open');
                        self.selectedSkidsToAdd([]);
                        $('#ulSkidLocations').listview('refresh');
                    }
                }, function (error_response) {
                    showErrorResponseText(error_response,true);
                });
            }

        });
    };

    self.viewInAdOrder = function () {
        window.location.href = "../jobAdOrder/jobAdOrder.html"
    };
};

var skid_obj = {};
var addSkidOrPrinter = function (type) {
    if (type == "printer") {
        var printer_to_add = $('#txtAddPrinter').val();
        if (printer_to_add != "") {
            var printer_item = "<li id='li" + printer_to_add.replace(/ /g, '') + "' data-value='" + printer_to_add.replace(/ /g, '') + "'><a href='' data-icon='delete' onclick='removePrinter(this,\"" + printer_to_add + "\");'>" + printer_to_add + "</a></li>";
            $('#ulAddedPrinters').append(printer_item).listview('refresh');
            $('#txtAddPrinter').val('');
        }
    }
}

var removePrinter = function (ctrl, printer_name) {
    $('#ulAddedPrinters').find('li[id=li' + printer_name.replace(/ /g, '') + ']').remove();
};

var updatePrintersList = function () {
    var added_printers = $('#ulAddedPrinters li');
    var printer_options = "";
    $.each(added_printers, function (key, val) {
        printer_options += "<option value='" + $(this).data('value') + "'>" + $(this).find('a').text() + "</option>";
    });
    if (printer_options != "") {
        $('#ddlPopupPrinter').html(printer_options).selectmenu('refresh');
    }
    $('#popupPrinters').popup('close');
    $('#popupShipment').popup('open');
}

var editPrinterList = function () {
    if ($('#ddlPopupPrinter').find('option').length > 0) {
        var printer_items = "";
        $.each($('#ddlPopupPrinter').find('option'), function (key, val) {
            printer_items += "<li id='li" + $(this).text().replace(/ /g, '') + "' data-value='" + $(this).text().replace(/ /g, '') + "'><a href='' data-icon='delete' onclick='removePrinter(this,\"" + $(this).text() + "\");'>" + $(this).text() + "</a></li>";
        });
        $('#ulAddedPrinters').empty();
        $('#ulAddedPrinters').append(printer_items).listview('refresh');
    }
    $('#popupShipment').popup('close');
    $('#popupPrinters').popup('open');
}

var searchSkidLocations = function () {
    $('#ulSelectedSkidLocations').empty();
    $('#popupSkidLocations').popup('open');
}

var materialFieldsValidation = function () {
    var msg;
    var empty_fields = [];
    //    if ($('#txtProjection').val() == "") {
    //        empty_fields.push("Projection");
    //    }
    if (!$('#popupShipment').parent().hasClass('ui-popup-hidden')) {
        if ($('#txtDateTimeRecd').val() == "") {
            empty_fields.push("Date Time Recd");
        }

        if ($('#txtBOLRecd').val() == "") {
            empty_fields.push("BOL Recd");
        }

        if ($('#txtWgtAmtRecd').val() == "") {
            empty_fields.push("Wgt Amt Recd");
        }

        if ($('#txtPieceWgtLbs').val() == "") {
            empty_fields.push("Piece Wgt Lbs");
        }
    }
    else {
        if ($('#txtHeight').val() == "") {
            empty_fields.push("Height");
        }

        if ($('#txtWidth').val() == "") {
            empty_fields.push("Width");
        }

        if ($('#txtThickness').val() == "") {
            empty_fields.push("Thickness");
        }
    }

    //    if ($('#txtPageSize').val() == "") {
    //        empty_fields.push("Page Size");
    //    }

    //    if ($('#ddlFormat').val() == "-1") {
    //        empty_fields.push("Format");
    //    }

    //    if (!($('#rdoSpecificationInSpec')[0].checked || $('#rdoSpecificationOutOfSpec')[0].checked)) {
    //        empty_fields.push("Specification");
    //    }

    //    if ($('#ddlDisposition').val() == "-1") { //Commented temporarily.
    //        empty_fields.push("Disposition");
    //    }

    if (empty_fields.length > 0) {
        var msg_to_display = "The " + (!$('#popupShipment').parent().hasClass('ui-popup-hidden') ? 'Shipment' : 'Advertiser') + " cannot be saved until all required fields are entered. Please enter data for: <br />"
        var empty_fields_list = empty_fields.join(',');
        if (empty_fields_list.indexOf(',') > -1)
            empty_fields_list = empty_fields_list.substring(0, empty_fields_list.lastIndexOf(',')) + ' and ' + empty_fields_list.substring(empty_fields_list.lastIndexOf(',') + 1);
        $('#alertmsg').html(msg_to_display + ' ' + empty_fields_list);
        if (!$('#popupShipment').parent().hasClass('ui-popup-hidden')) {
            $('#popupShipment').popup('close');
            $("#okBut").attr("onclick", "$('#popupDialog').popup('close');$('#popupShipment').popup('open');");
        }
        else {
            $("#okBut").attr("onclick", "$('#popupDialog').popup('close');");
        }
        $("#popupDialog").popup('close');
        $("#popupDialog").popup('open');

        return false;
    }

    $("#okBut").attr("onclick", "$('#popupDialog').popup('close');");
    return true;
}

//Checks the given string ends with a specific (given character).
String.prototype.endsWith = function (suffix) {
    return this.indexOf(suffix, this.length - suffix.length) !== -1;
};
var companyList;
var searchSkids = function () {
    companyList = $("#txtSkidLocation").autocomplete({
        source: function (request, response) {
            var search_string = $("#txtSkidLocation").val();
            if (search_string != "") {
                search_string = search_string.replace(/\./g, '_');
                if (search_string.endsWith('_')) {
                    search_string = search_string.substring(0, (search_string.length - 1));
                }
                var url = serviceURLDomainIPInternal + "api/MaterialLocation/" + search_string;
                $.ajax({
                    type: "GET",
                    url: url,
                    dataType: "json",
                    data: {},
                    async: (!isMobile && isSafari) ? true : false,
                    //async: false,
                    beforeSend: function (req) {
                        req.setRequestHeader("Authorization", sessionStorage.authString);
                    },
                    success: function (data) {
                        response($.map(data, function (item) {
                            return {
                                "label": item.aisle + '.' + item.row + '.' + item.level,
                                "value": item.locationPk,
                                "options": item
                            }
                        }));
                    },
                    error: function (data) {
                        var a = data;
                    }
                });
            }
        },
        //delay: 300,
        minLength: 0,
        select: function (event, ui) {
            $("#txtSkidLocation").val(ui.item.label);
            $("#txtSkidLocation").attr('data-value', ui.item.value);
            //$("#txtSkidLocation").attr('data-options', JSON.stringify(ui.item.options).replace(/'/g, "\\'"));
            $('#hdnSelectedSkidInfo').val(JSON.stringify(ui.item.options).replace(/'/g, "\\'"));
            return false;
        },
        response: function (event, ui) {
            if (ui.content.length === 0) {
                $('#error-message').css("visibility", "visible");
            } else {
                $('#error-message').css("visibility", "hidden");
            }
        }
    });
};
//calls the webservice to show the SSRS reports based on the user selection report
function fnDisplayReports() {
    window.open("http://s-reportserver/reportserver?/Shared Mail - Materials/Receiving Form&rs:Command=Render&rs:ClearSession=true&adOrderID=" + sessionStorage.adOrderId + "&rc:Toolbar=false&rs:Format=PDF");
    //    var report_URL = reportsURL + "api/reportv2_setreportinfo";

    //    var json_report = {};
    //    json_report["ssrsFolder"] = "Shared Mail-Materials";
    //    json_report["outputType"] = "pdf";
    //    json_report["reportName"] = "Receiving Form";
    //    json_report["parameters"] = [{ "name": "adOrderId", "value": sessionStorage.adOrderId}];
    //    postCORS(report_URL, JSON.stringify(json_report), function (response) {
    //        windowOpen("pdf", json_report.reportName);
    //        setTimeout(function getDelay() { setTimeout }, 500);
    //        //removeLoadingImg();
    //    }, function (response_error) {
    //        //removeLoadingImg();
    //        $('#alertmsg').text((response_error.responseText != undefined) ? response_error.responseText : response_error.statusText);
    //        setTimeout(function getDelay() {
    //            $('#popupDialog').popup('open');
    //        }, 1000);

    //    });
}
//to open the reports in a window (iframe), this method is called in "fnDisplayReports".
//function windowOpen(response, output_type) {
function windowOpen(output_type, report_name) {
    var content_type = '';
    var htmlText = "";
    var report_preview_url = reportsURL + "api/reportv2/" + sessionStorage.username + "_" + report_name.replace(/\s/g, "_");
    //removeLoadingImg();
    $('#popPDF').popup('close');
    switch (output_type) {
        case "pdf":
            content_type = ' type="application/pdf"'
            break;
        case "doc":
            content_type = ' type="application/msword"'
            break;
        case "xls":
            content_type = ' type="application/x-msexcel"'
            break;
        case "image":
            content_type = ' type="image/tiff" src="data:image/tiff;base64,'
            break;
    }

    $('#popPDF').popup('close');
    var htmlText = "";
    //if (output_type == 'pdf' || output_type == 'xml') {
    htmlText = '<iframe width=850 HEIGHT=500 id="rptFrame"'
                      + content_type
                      + ' src="' + report_preview_url
                      + '"></iframe>';
    //}
    //    else {
    //        htmlText = '<iframe width=50 HEIGHT=50 id="rptFrame"'
    //                      + content_type
    //                      + ' src="' + report_preview_url
    //                      + '"></iframe>';
    //    }
    if (output_type == 'pdf' || output_type == 'xml') {
        $('#popupPDFContent').html(htmlText);
    } else {
        $('#popupPDFContent').html(htmlText);
    }

    var iframe = document.getElementById('rptFrame');

    //DOMContentLoaded
    if (iframe.addEventListener) {
        iframe.addEventListener('onload', callback(output_type), true);
    } else {
        iframe.attachEvent('onload', callback(output_type));
    }

    if (output_type == 'pdf' || output_type == 'xml') {
        //var wait_ctrl = '<div id="dvWaitImage" >';
        //$('#waitPopUp').popup('open');
        //$('#waitPopUp').popup('close');
        //wait_ctrl += $('#waitPopUp').html() + "</div>";
        //$('#popupPDFContent').prepend(wait_ctrl);
        var maxHeight = $('#popPDF').height() / 2 - 190 + "px";
        var maxWidth = $('#popPDF').width() / 2 - 80 + "px";
        //$('#dvWaitImage').css('position', 'absolute');
        //$("#dvWaitImage").css("margin-top", maxHeight);
        //$("#dvWaitImage").css("margin-left", maxWidth);

        $('#popPDF').popup('open', { positionTo: 'window' });
    }
}
var timeInterval = "";
function callback(output_type) {
    var is_loaded = false;
    timeInterval = setInterval(function () {
        //$('#rptFrame')[0].contentDocument.message == "Access is denied.\r\n"
        //if (((navigator.userAgent.indexOf("MSIE") > -1 && !window.opera) && typeof ($('#rptFrame')[0].contentDocument) == "unknown") ||
        //        ((navigator.userAgent.indexOf("MSIE") == -1) && (typeof ($('#rptFrame')[0].contentDocument) == "object"))) {
        is_loaded = true;
        clearInterval(timeInterval);
        timeInterval = "";

        if (output_type != 'pdf' && output_type != 'xml') {
            if (window.location.href.toString().toLowerCase().indexOf('regionalhome') > -1) {
                setTimeout(function getDelay() {
                    //$('#popPDF').popup('open');
                    $('#popPDF').popup('close');
                    //$('#waitPopUp').popup('open');
                    //$('#waitPopUp').popup('close');
                }, 8500);
            }
            else {
                setTimeout(function getDelay() {
                    $('#popPDF').popup('close');
                    //$('#waitPopUp').popup('open');
                    //$('#waitPopUp').popup('close');
                }, 3000);
            }
        }
        else {
            //if (navigator.userAgent.indexOf("MSIE") > -1 && !window.opera) {

            setTimeout(function setTimer() {
                //$('#waitPopUp').popup('open');
                //$('#waitPopUp').popup('close');
                // removeLoadingImg();
                // $('#dvWaitImage').empty();
            }, 5000);
            //                }
            //                else {
            //                    $('#dvWaitImage').empty();
            //                }
        }
        //}
    }, 1000);
}
//******************** Page Load Events End **************************
