﻿
var gLogListUrl = serviceURLDomain + 'api/Logging/' + sessionStorage.facilityId + '/' + jobCustomerNumber + '/' + sessionStorage.jobNumber;
var gServersInfoUrl = 'JSON/_servers_drive_space_info.JSON';
gServersInfoUrl = serviceURLDomain + "api/DashboardServer_diskSpace";
var gFacilityServiceUrl = "../JSON/_facilityList.JSON";
var gPrintStreamConnectionsURL = serviceURLDomain + "api/DashboardServer_getMonitorList";
var primaryFacilityData;
var serversInfo = [];

//$.getJSON(gServersInfoUrl, function (result) {
//    serversInfo = result.servers;
//});

getCORS(gServersInfoUrl, null, function (data) {
    //serversInfo = data.serverStatsList;
    $.each(data.serverStatsList, function (key1, val1) {
        //var name = val1.name;
        var name = "";
        var drives_info = [];
        //if (Object.keys(val1.serverDrivePathSpaceDict).length > 2) {
        //    val1.serverDrivePathSpaceDict["\\\\s-whalers-vm\\f$"] = { "freeSpaceGb": 12.9524775, "totalSpaceGb": 42.94757 };
        //    val1.serverDrivePathSpaceDict["\\\\s-whalers-vm\\f1$"] = { "freeSpaceGb": 12.9524775, "totalSpaceGb": 42.94757 };
        //    val1.serverDrivePathSpaceDict["\\\\s-whalers-vm\\f2$"] = { "freeSpaceGb": 12.9524775, "totalSpaceGb": 42.94757 };
        //}
        $.each(val1.serverDrivePathSpaceDict, function (key2, val2) {
            name = key2.substring(2, key2.length - 1);
            name = name.substring(0, name.lastIndexOf('\\'));
            var drive = (key2.lastIndexOf('$') > -1) ? key2.substring(key2.lastIndexOf('\\') + 1, key2.lastIndexOf('$')) : key2.substring(key2.lastIndexOf('\\') + 1);

            var drive_info = {};
            drive_info["drive"] = drive;
            drive_info["totalSpace"] = val2.totalSpaceGb;
            drive_info["freeSpace"] = val2.freeSpaceGb;
            drives_info.push(drive_info);
        });
        serversInfo.push({
            "domain": val1.domain,
            "name": name,
            "drivesInfo": drives_info
        });
    });

}, function (error_response) {
    showErrorResponseText(error_response, true);
});

$.getJSON(gFacilityServiceUrl, function (result) {
    primaryFacilityData = result.facility;
});

$('#_systemDashboard').live('pagebeforecreate', function (event) {
    displayMessage('_systemDashboard');
    ko.setTemplateEngine(new ko.nativeTemplateEngine);
});

$(document).on('pageshow', '#_systemDashboard', function (event) {

    try {
        window.setTimeout(function () {
            ko.applyBindings(new dashBoardViewModel());

            $('ul[data-server]').listview();

            $('div[class=ui-block-c] ul').listview();
            $('#dvPrintstreamConnections div[data-role=collapsible-set] div[data-role=collasible] ul').listview();
            $('#dvPrintstreamConnections div[data-role=collapsible]').find('.ui-collapsible-content').css('padding-bottom', '0px');
            $('#dvPrintstreamConnections div[data-role=collapsible-set] div[data-role=collasible]').collapsible();
            $('#dvPrintstreamConnections div[data-role=collapsible-set]').collapsibleset();
            $('#dvPrintstreamConnections div[data-role=collapsible] div[data-role=collapsible] h2').find('a.ui-collapsible-heading-toggle').css('padding-left', '8px');
            $('#dvPrintstreamConnections').collapsibleset('refresh');
            $('#dvPrintstreamConnections div[data-role=collapsible]').eq(0).collapsible("disable");

            drawChart(serversInfo);
        }, 1000);
    } catch (e) {
        alert(e);
    }

});

var printStreamConnectionsModel = function (print_steam_info) {
    var self = this;
    ko.mapping.fromJS(print_steam_info, {}, self);
    self.name = ko.computed(function () {
        var facility_name = getTribuneFacilityName(self.facilityId().toString());
        var computed_name = "";
        var temp_date = new Date(self.monitorDateTime());
        computed_name = facility_name + " (" + self.psUser() + ") - " + formatDate(self.monitorDateTime());
        return computed_name;
    });
    self.xmlCreator = ko.computed(function () {
        var is_active = self.isXmlCreatorActive();
        return (is_active) ? 'Active' : 'In-active';
    });
};

function formatDate(date) {
    var day = date.substring(date.lastIndexOf('-') + 1, date.lastIndexOf('-') + 3);
    var month = date.substring(date.indexOf('-') + 1, date.lastIndexOf('-'));
    var year = date.substring(0, date.indexOf('-'));
    var hours = date.substring(date.indexOf('T') + 1, date.indexOf(':'));
    var minutes = date.substring(date.indexOf(':') + 1, date.lastIndexOf(':'));
    //var hours = date.getHours();
    //var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    //minutes = minutes < 10 ? '0' + minutes : minutes;
    minutes = minutes < 10 ? minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    //return date.getMonth() + 1 + "/" + date.getDate() + "/" + date.getFullYear() + "  " + strTime;
    return month + "/" + day + "/" + year + "  " + strTime;
}

var dashBoardViewModel = function () {
    var self = this;
    self.facilities = ko.observableArray([]);
    self.serversInfo = ko.observableArray([]);

    self.printStreamConnections = ko.observableArray([]);

    getCORS(gPrintStreamConnectionsURL, null, function (data) {
        var temp_connections = [];
        $.each(primaryFacilityData, function (key_facility, val_facility) {
            temp_connections = $.grep(data, function (obj) {
                return val_facility.facilityValue == obj.facilityId;
            });
            //if (temp_connections.length > 0) {
            var temp = {};
            var facility_name = "";
            var latest_log_item = "";
            var is_active_error = "";
            $.each(temp_connections, function (key, val) {
                if (key == 0) {
                    facility_name = getTribuneFacilityName(val.facilityId.toString());
                    temp["facility"] = facility_name;
                    temp["connectionLogs"] = [];
                    latest_log_item = val.monitorDateTime;
                }
                if (temp.connectionLogs != undefined && temp.connectionLogs != null) {
                    if (val.message != null && val.message != "")
                        val.message = val.message.replace(val.facilityId.toString(), facility_name);
                    temp.connectionLogs.push(new printStreamConnectionsModel(val));
                }
                is_active_error = (!(val.isXmlCreatorActive) || (val.error != null && val.error != "")) ? 0 : 1;
                if (latest_log_item < val.monitorDateTime) {
                    latest_log_item = val.monitorDateTime;
                }
                if (temp_connections.length == key + 1) {
                    latest_log_item = formatDate(latest_log_item);
                    temp["facility"] = facility_name + ' ' + latest_log_item;
                    temp["currentStatus"] = is_active_error;
                }
            });
            if (Object.keys(temp).length > 0) {
                self.printStreamConnections.push(temp);
                facility_name = "";
                latest_log_item = "";
                is_active_error = "";
                temp = {};
            }
            //}
        });
    }, function (error_response) {
        showErrorResponseText(error_response, true);
    });

    $.each(serversInfo, function (server, value) {
        self.serversInfo.push(ko.mapping.fromJS(value));
    });

    //$.each(primaryFacilityData, function (key, val) {
    //    self.facilities.push(ko.mapping.fromJS(val, {}));
    //});

    //Rediret to System Dashboard.
    self.redirectToSystemDashboard = function (data, e) {
        window.setTimeout(function () {
            window.location.href = '../systemsDashboard/systemsDashboard.html';
        }, 500);
    };
    self.showHomePage = function () {
        window.location.href = "../index.htm";
    }

    self.clickAction = function () {
        $('#popupMenu').popup('close');
        window.setTimeout(function () {
            $('#popupNLogRetrieval').popup('open');
        }, 1000);
    };
};

