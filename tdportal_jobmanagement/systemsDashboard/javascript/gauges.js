function drawChart(serversInfo) {
    var options = {
        chartArea: { left: 0, right: 25 },
        height: '80',
        width: '110',
        titleTextStyle: {
            fontSize: 9, // 12, 18 whatever you want (don't specify px)
            bold: true    // true or false
        },
        colors: ['#c0def5', '#1370b5'],
        legend: 'none',
        pieSliceText: "none",
        tooltip: { textStyle: { fontName: 'arial', fontSize: 10 } }
    };
    $.each(serversInfo, function (server, value) {
        var temp_data = [['Label', 'Value']];
        $.each(value.drivesInfo, function (drive_key, drive_value) {
            var used_space = drive_value.totalSpace - drive_value.freeSpace;
                temp_data.push(['Free Space', drive_value.freeSpace]);
                temp_data.push(['Used Space', used_space]);
            //options["title"] = drive_value.drive.toUpperCase() + ': ' + drive_value.totalSpace.toFixed(2) + ' GB';
                options["title"] = drive_value.drive.toUpperCase() + ': ' + drive_value.freeSpace.toFixed(3) + ' GB Free';
                var data = new google.visualization.arrayToDataTable(temp_data);
                var chart = new google.visualization.PieChart(document.getElementById(value.name + '_' +drive_value.drive));
                chart.draw(data, options);
                temp_data = [['Label', 'Value']];
        });
    });
}
