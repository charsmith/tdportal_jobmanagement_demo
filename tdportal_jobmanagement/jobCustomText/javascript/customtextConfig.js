﻿hideCustomTextPreviewMailing = function () {
    var customer = getCustomerNumber();
    return ([SCOTTS, SPORTSKING, ACE, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);
}

showAddRemoveLocationDemoHint = function () {
    var customer = getCustomerNumber();
    return ([KUBOTA, SPORTSKING, GWA, SCOTTS, ACE, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);
}

isNewCustomerTextCustomers = function () {
    var customer = getCustomerNumber();
    return ([SPORTSKING, SCOTTS, ACE, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);
}

showReplicateEvents = function () {
    var customer = getCustomerNumber();
    return ([ANDERSEN, AAG, DCA, KUBOTA, SPORTSKING, GWA, SCOTTS, ACE, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);
}

hideOrderedLocations = function () {
    var customer = getCustomerNumber();
    return ([ANDERSEN, DCA, KUBOTA, SPORTSKING, GWA, SCOTTS, ACE, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);
}

showCustomTextDemoHint = function () {
    var customer = getCustomerNumber();
    return ([KUBOTA, SPORTSKING, GWA, SCOTTS, ACE, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);
}

showLocationsDropdown = function () {
    var customer = getCustomerNumber();
    return ([SCOTTS, ANDERSEN, SHERWINWILLIAMS, ACE, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);
}

isNewVariableFieldListCustomers = function () {
    var customer = getCustomerNumber();
    return ([SCOTTS, ACE, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);
}

hideACELogoCheck = function () {
    var customer = getCustomerNumber();
    return ([ACE, MILWAUKEE].indexOf(customer) > -1);
}

hideTrueValueLogoCheck = function () {
    var customer = getCustomerNumber();
    return ([ACE, MILWAUKEE].indexOf(customer) > -1);
}

hideDoItBestLogoCheck = function () {
    var customer = getCustomerNumber();
    return ([ACE, MILWAUKEE].indexOf(customer) > -1);
}

hideSetupCompleteValidation = function () {
    var customer = getCustomerNumber();
    return ([AAG, DCA, KUBOTA, SPORTSKING, GWA, SCOTTS, ACE, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) == -1);
}

isValidateCompanyInfo = function () {
    var customer = getCustomerNumber();
    return ([SPORTSKING, SCOTTS, ACE, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) == -1);
}

isStateAndZipNeededInLocation = function () {
    var customer = getCustomerNumber();
    return ([ANDERSEN, SHERWINWILLIAMS, ACE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);
}