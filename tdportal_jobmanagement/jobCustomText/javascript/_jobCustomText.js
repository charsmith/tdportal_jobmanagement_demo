﻿$.mobile.popup.prototype.options.history = false;
jQuery.support.cors = true;
var mapping = { 'ignore': ["__ko_mapping__"] }

//******************** Data URL Variables Start **************************
var pageObj;
var gCompanyInfoServiceUrl = "../jobCompanyInfo/JSON/_companyInfo.JSON";
var gStatesListUrl = "../JSON/_listStateCodes.JSON";
var gLocationSourcesUrl = "JSON/_locationSources.JSON";
var gServiceCompanyData;
var gOutputData;
var pageObj;
var states = [];
var location_sources = [];
var companyData;
var primaryFacilityData;
var loanOfficersList = [];
var allUserLocations = [];
var jobLoInfo;
var dontShowHintsAgain = (localStorage.getItem('doNotShowHintsAgain') != undefined && localStorage.getItem('doNotShowHintsAgain') != null && localStorage.getItem('doNotShowHintsAgain') != "" && localStorage.getItem('doNotShowHintsAgain') != "false") ? true : false;
//******************** Data URL Variables End **************************
$.getJSON(gStatesListUrl, function (result) {
    states = result;
});
$.getJSON(gLocationSourcesUrl, function (result) {
    location_sources = result;
});
//Sports KING Default JSON
var temp_cmp_info = [{
    "name": "locationHint",
    "type": "string",
    "value": "LOCATED INSIDE HOMETOWN SQUARE"
}, {
    "name": "address1",
    "type": "string",
    "value": "123 Main Street"
}, {
    "name": "address2",
    "type": "string",
    "value": ""
}, {
    "name": "city",
    "type": "string",
    "value": "Anytown"
}, {
    "name": "state",
    "type": "string",
    "value": "US"
}, {
    "name": "zip",
    "type": "string",
    "value": "12345"
}, {
    "name": "phone",
    "type": "string",
    "value": "(888) 888-8888"
}, {
    "name": "website",
    "type": "string",
    "value": "SportsKing.com"
}, {
    "name": "frontText",
    "type": "string",
    "value": "We love baseball almost as much as you do."
}, {
    "name": "backHeader",
    "type": "string",
    "value": "Bat a Thousand.\nShop SportsKing."
}, {
    "name": "backText",
    "type": "string",
    "value": "We know how much you love baseball.\nWe love it too.\nthat's why we carry everything the avid baseball player needs — all at a great low price."
}];

/*var temp_cmp_info = {
    "companyInfo": {
        "locationHint": "LOCATED INSIDE HOMETOWN SQUARE", "address1": "123 Main Street", "address2": "", "city": "Anytown",
        "state": "US", "zip": "12345", "phone": "(888) 888-8888", "website": "SportsKing.com"
    },
    //"variableDict": {
    //    "frontText": "We love baseball almost as much as you do.",
    //    "backHeader": "Bat a Thousand.\nShop SportsKing.",
    //    "backText": "We know how much you love baseball.\nWe love it too.\nthat's why we carry everything the avid baseball player needs — all at a great low price."
    //},
    "isEnabled": true,
    "name": "companyInfo"
};
if (jobCustomerNumber != SCOTTS) {
    temp_cmp_info["variableFieldList"] = {
        "frontText": "We love baseball almost as much as you do.",
        "backHeader": "Bat a Thousand.\nShop SportsKing.",
        "backText": "We know how much you love baseball.\nWe love it too.\nthat's why we carry everything the avid baseball player needs — all at a great low price."
    };
}*/
//******************** Page Load Events Start **************************
$('#_jobCustomText').live('pagebeforecreate', function (event) {
    ko.setTemplateEngine(new ko.nativeTemplateEngine);
    displayMessage('_jobCustomText');
    createConfirmMessage("_jobCustomText");
    loadingImg('_jobCustomText');

    if (jobCustomerNumber == AAG) {
        $.getJSON("../JSON/_aagEnhancementsJobTicket.JSON", function (data) {
            jobLoInfo = data;
        });
    }
    setTimeout(function () {
        getCompanyInfo();
    }, (jobCustomerNumber == AAG) ? 1800 : 0);
    displayNavLinks();
    $("#navLinksPanel").on("panelopen", function (event, ui) {
        sessionStorage.isNavPanelOpen = true;
    });
    setDemoHintsSliderValue();
    createDemoHints("jobCustomText");
    if (jobCustomerNumber == AAG && appPrivileges.roleName == "user") {
        $('a[title="Loan Officers"]').hide();
        $('a[title="Ordered Locations"]').hide();
    } else if (jobCustomerNumber != AAG) {
        $('a[title="Loan Officers"]').hide();
    }
    else {
        $('a[title="Ordered Locations"]').hide();
    }
    if (isNewCustomerTextCustomers()) {
        $("#customTextHints").bind({
            popupafterclose: function (event, ui) {
                if (!dontShowHintsAgain && (showAddRemoveLocationDemoHint())
                       && (sessionStorage.showDemoHints == undefined || sessionStorage.showDemoHints == null || sessionStorage.showDemoHints == "" || sessionStorage.showDemoHints == "on")) {
                    $('#addRemoveLocationHints').popup('open', { positionTo: '#dvCmpInfo' });
                }
            }
        });
    }
});

$(document).on('pageshow', '#_jobCustomText', function (event) {
    var pageObj_time = (sessionStorage.setUpTwoDate != undefined && sessionStorage.setUpTwoDate != null && sessionStorage.setUpTwoDate != "" && JSON.parse(sessionStorage.setUpTwoDate) == true) ? 2300 : 300;
    window.setTimeout(function () {
        pageObj = new loadCompanyInfo();
        //pageObj.getSelectedInfo();
        ko.applyBindings(pageObj);
        //pageObj.loanOfficerChanged();
        $('#ddlLoanOfficers').trigger('change');
        $('input[type=checkbox]').trigger('create');
        $('#dvCmpInfo').trigger('create');
        $('#navbar').trigger('create');
        $('#dvNavBar div ul li a').first().addClass('ui-btn-active');
        $('#chkReplicate').checkboxradio('refresh');
        $('#ddlLocations').selectmenu('refresh');
        //window.setTimeout(function () {
        $('input[type=text]').parent().css('margin', '0px');
        $('select').parent().parent().css('margin', '0px');
        //}, 1000);

        jQuery('input[data-fieldType=date]').datepicker({
            minDate: new Date(2010, 0, 1),
            dateFormat: 'mm/dd/yy',
            constrainInput: true,
            beforeShowDay: function (date) {
                return displayBlockDays(date, 0);
            }
        });
        jQuery('input[data-fieldType=phone]').mask("(999) 999-9999");
        jQuery('input[data-fieldType=zip]').mask("99999");

    }, pageObj_time);

    if (hideCustomTextPreviewMailing()) {
        $('#btnPreviewMailing').css('display', 'none');
        $('#btnSaveTop').addClass('ui-first-child');
    }
    if (showReplicateEvents()) {
        if (sessionStorage.setUpTwoDate != undefined && sessionStorage.setUpTwoDate != null && sessionStorage.setUpTwoDate != "" && JSON.parse(sessionStorage.setUpTwoDate) == true) {
            $('#dvNavBar').css('display', 'block');
            $("label[for='chkReplicate']").text("Replicate Company Info For All Events");
        }
        else {
            $('#dvNavBar').css('display', 'none');
            $('#dvReplicate').css('display', 'none');
        }
        if (hideOrderedLocations()) {
            $('a[data-icon="myapp-locations"]').css('display', 'none');
            $('a[data-icon="eye"]').addClass('ui-first-child');
        }
    }
    persistNavPanelState();
    if (!dontShowHintsAgain && (showCustomTextDemoHint())
                          && (sessionStorage.showDemoHints == undefined || sessionStorage.showDemoHints == null || sessionStorage.showDemoHints == "" ||
                              sessionStorage.showDemoHints == "on")
                          && (sessionStorage.isCustomTextDemoHintsDisplayed == undefined || sessionStorage.isCustomTextDemoHintsDisplayed == null ||
                              sessionStorage.isCustomTextDemoHintsDisplayed == "false")) {
        sessionStorage.isCustomTextDemoHintsDisplayed = true;
        $('#customTextHints').popup('close');
        window.setTimeout(function loadHints() {
            $('#customTextHints').popup('open', { positionTo: '#dvCmpInfo' });
        }, 500);
    }
    $('div[data-role=popup] div[data-role=header]').removeClass('ui-corner-top ui-header ui-bar-d ui-header-fixed slidedown ui-panel-fixed-toolbar ui-panel-animate ui-panel-page-content-position-left ui-panel-page-content-display-push ui-panel-page-content-open');
    $('div[data-role=popup] div[data-role=header]').addClass('ui-corner-top ui-header ui-bar-d');
    fixAlertHeader();
});

function validateInput(event) {
    if (event.id == 'txtAwarded_Amount:_$')
        event.value = event.value.replace(/[^0-9]+/g, '');
    
    return true;
}

var getCompanyInfo = function () {
    if (sessionStorage.jobSetupOutput != undefined && sessionStorage.jobSetupOutput != null && sessionStorage.jobSetupOutput != "") {
        gOutputData = $.parseJSON(sessionStorage.jobSetupOutput);


        if ((jobCustomerNumber == SPORTSKING) && gOutputData.variableTextAction != undefined) {
            if (gOutputData.variableTextAction.variableFieldList != undefined && gOutputData.variableTextAction.variableFieldList != null && Object.keys(gOutputData.variableTextAction.variableFieldList).length == 0) {
                gOutputData.variableTextAction.variableFieldList = temp_cmp_info;
            }
            //Need to be updated based on the dictionary returned from service.
            //self.gOutputData.variableTextAction.companyInfoDict = temp_cmp_info.companyInfo;
        }


        if (isNewCustomerTextCustomers()) {
            if (gOutputData.variableTextAction != undefined && gOutputData.variableTextAction != null && gOutputData.variableTextAction != "") {
                gServiceCompanyData = gOutputData.variableTextAction;
            }
        }
        else {
            if (gOutputData.companyInfoAction != undefined && gOutputData.companyInfoAction != null && gOutputData.companyInfoAction != "") {
                gServiceCompanyData = gOutputData.companyInfoAction;
            }
        }
    }
    else if (sessionStorage.templateSetupConfigJSON != undefined && sessionStorage.templateSetupConfigJSON != null && sessionStorage.templateSetupConfigJSON != "") {
        gServiceCompanyData = $.parseJSON(sessionStorage.templateSetupConfigJSON);
    } else {
        $.getJSON(gCompanyInfoServiceUrl, function (data) {
            $.each(data, function (key, val) {
                if (key.indexOf(sessionStorage.customerNumber) > -1) {
                    gServiceCompanyData = val;
                }
            });
        });
    }
};

ko.bindingHandlers.fireChange = {
    update: function (element, valueAccessor, allBindingsAccessor) {
        var bindings = allBindingsAccessor();
        if (bindings.value != null) {
            if ($(element).val().length > 200 || $(element).val().indexOf('\n') > -1) {
                $(element).css("height", "180px");
            }
        }
    }
};

var locationVm = function (store_id, store_name, store_pk) {
    var self = this;
    self.locationId = store_pk;
    if (store_id != undefined && store_id != null && store_id != "")
        self.locationName = store_id + ' - ' + store_name;
    else
        self.locationName = store_name;
};

var fieldInfo = function (label, value, is_required, show_chkbox) {
    var self = this;
    self.label = ko.observable('');
    self.label(label);
    self.value = ko.observable('');
    self.value(value);
    self.isRequired = ko.observable('');
    self.isRequired(is_required);
    self.isEditable = ko.observable('');
    self.isEditable("true");
    //if (label.toLowerCase() == "toll-free number" && appPrivileges.roleName == "admin")
    //    self.isEditable("true");
    //else
    //if (label.toLowerCase() == "toll-free number" && appPrivileges.roleName == "user") {
    //    self.isEditable("false");
    //}

    //else if (label.toLowerCase() == "toll-free number" && appPrivileges.roleName != "admin")
    //self.isEditable(false);
};

var loanOfficer = function (user_id, first_name, last_name) {
    var self = this;
    self.loanOfficerId = user_id;
    self.loanOfficerName = first_name + ' ' + last_name;
};

var loadCompanyInfo = function () {
    var self = this;
    self.locationsList = ko.observableArray([]);
    self.customTextLocationsList = ko.observableArray([]);
    self.varCompanyInfo = ko.observable({});
    self.prevVarCompanyInfo = ko.observable({});
    self.selectedInHomeStoreCompanyInfo = ko.observable({});
    self.selectedInhomeInfo = ko.observable({});
    self.inHomeDatesList = ko.observableArray([]);
    self.currentInHome = ko.observable();
    self.selectedLocation = ko.observable({});
    self.isFirstTime = ko.observable(true);
    self.selectedStore = ko.observable({});
    self.isReplicated = ko.observable();
    self.loanOffiersList = ko.observableArray([]);
    self.selectedLoanOfficer = ko.observable({});
    self.previousSelectedStore = ko.observable({});
    self.statesList = ko.observableArray();
    ko.mapping.fromJS(states, [], self.statesList);
    self.locationSources = ko.observableArray();
    ko.mapping.fromJS(location_sources, [], self.locationSources);
    self.selectedLocationSourceType = ko.observable('select');
    self.addLocationType = ko.observable('');
    self.mailingNewLocation = ko.observableArray([]);
    var temp_arr = [];
    var temp_arr1 = ko.observableArray([]);
    var temp_key = "";
    var index = 0;
    var temp_company_info = {};
    var show_chk = false;
    var is_object = false;
    var temp_key2 = "";
    var company_info = {};

    self.getPageDisplayName = function (page_name) {
        var PATTERN = /^[ABCDEFGHIJKLMNOPQRSTUVWXYZ]/;
        var page_display_name = '';
        $.each(new String(page_name), function (key, value) {
            if (key > 0 && value.match(PATTERN)) {
                page_display_name += ((!page_display_name.endsWith(' ') && !page_display_name.endsWith('-')) ? ' ' : '') + value;
            }
            else {
                page_display_name += value;
            }
        });
        return page_display_name;
    };
    if (sessionStorage.loanOffiersList != undefined && sessionStorage.loanOffiersList != null && sessionStorage.loanOffiersList != "") {
        loanOfficersList = $.parseJSON(sessionStorage.loanOffiersList);
        $.each(loanOfficersList, function (key, val) {
            if (key == 0) {
                self.selectedLoanOfficer(val.userId);
            }
            self.loanOffiersList.push(new loanOfficer(val.userId, val.firstName, val.lastName));
        });
    }

    if (sessionStorage.jobSetupOutput != undefined && sessionStorage.jobSetupOutput != null && sessionStorage.jobSetupOutput != "") {
        gOutputData = $.parseJSON(sessionStorage.jobSetupOutput);
        if ((gOutputData.selectedLocationsAction != undefined && gOutputData.selectedLocationsAction != null && gOutputData.selectedLocationsAction != "") && (gOutputData.selectedLocationsAction.selectedLocationsList.length > 0)) {
            $.each(gOutputData.selectedLocationsAction.selectedLocationsList, function (key, val) {
                if (key == 0) {
                    self.selectedLocation(val.pk);
                    self.selectedStore(val.pk);
                    self.previousSelectedStore(self.selectedStore());
                }
                self.locationsList.push(new locationVm(val.storeId, ((val.storeName) ? val.storeName : ((val.location) ? val.location : "")), val.pk));
            });
        }
        if (showLocationsDropdown()) {
            var post_json = {};
            post_json["approvalStatusDesc"] = "Available,Opted-In,Pending";
            post_json["locationListSize"] = 15; //As per jim instructions, location list size should be 15.
            var gLocationsSearchUrl = serviceURLDomain + 'api/Locations/' + sessionStorage.facilityId + "/" + jobCustomerNumber + "/" + gOutputData.jobNumber + "/0/0/" + gOutputData.jobTypeId;
            postCORS(gLocationsSearchUrl, JSON.stringify(post_json), function (data) {
                if (data.locationsAvailable.length > 0) {
                    $.each(data.locationsAvailable, function (k, v) {
                        allUserLocations.push(v);
                        self.customTextLocationsList.push(new locationVm(v.storeId, ((v.storeName) ? v.storeName : ((v.location) ? v.location : "")), v.pk));
                    });
                }
                if (data.locationsSelected.length > 0) {
                    $.each(data.locationsSelected, function (k, v) {
                        allUserLocations.push(v);
                        self.customTextLocationsList.push(new locationVm(v.storeId, ((v.storeName) ? v.storeName : ((v.location) ? v.location : "")), v.pk));
                    });
                }
            }, function (error_response) {
                showErrorResponseText(error_response, false);
            });
        }
    }

    if (gOutputData != undefined && gOutputData != null) {
        var nav_bar_items = '<div data-role="navbar" class="ui-body-o" data-iconpos="right"><ul>';
        var cnt = -1;
        //if (sessionStorage.setUpTwoDate != undefined && sessionStorage.setUpTwoDate != null && sessionStorage.setUpTwoDate != "" && JSON.parse(sessionStorage.setUpTwoDate) == true) {
        if (jobCustomerNumber == AAG) {
            $.each(gOutputData.companyInfoAction, function (key, val) {
                if (key.toLowerCase().indexOf('event') > -1) {
                    var temp_list = {};
                    cnt++;
                    temp_list[key] = "Event " + (cnt + 1);
                    self.inHomeDatesList.push({
                        "showCheckIcon": false,
                        'dateValue': "Event " + (cnt + 1)
                    });

                    if (cnt == 0)
                        nav_bar_items += '<li><a href="#" data-icon="check">Event ' + (cnt + 1) + ' </a></li>';
                    else
                        nav_bar_items += '<li><a href="#" data-icon="edit">Event ' + (cnt + 1) + ' </a></li>';
                }
            });
        }
        else {
            if (!isNewCustomerTextCustomers()) {
                $.each(gOutputData, function (key, val) {
                    if (key.toLowerCase().indexOf('inhome') > -1) {
                        var temp_list = {};
                        temp_list[key] = val;
                        self.inHomeDatesList.push({
                            "showCheckIcon": false,
                            'dateValue': val
                        });
                        cnt++;
                        if (cnt == 0)
                            nav_bar_items += '<li><a href="#" data-icon="check">' + val + ' In Home</a></li>';
                        else
                            nav_bar_items += '<li><a href="#" data-icon="edit">' + val + ' In Home</a></li>';
                    }
                });
            }
        }

        nav_bar_items += '</ul></div>';
        self.currentInHome(self.inHomeDatesList()[0]);
        if (jobCustomerNumber == AAG) {
            //if (sessionStorage.setUpTwoDate != undefined && sessionStorage.setUpTwoDate != null && sessionStorage.setUpTwoDate != "" && JSON.parse(sessionStorage.setUpTwoDate) == true) {
            //    var idx = 0;
            //    var main_idx = 0;
            //    var temp_id = {};
            //$.each(jobLoInfo.customText, function (key1, val1) {
            //    $.each(gOutputData.companyInfoAction, function (key2, val2) {
            //        $.each(val2, function (key3, val3) {
            //            //if (idx == main_idx) {
            //            temp_id[main_idx]=
            //            val3[main_idx].companyInfo = val1[0].companyInfo;
            //            idx++;
            //            // }
            //        });
            //        idx = 0;
            //    });
            //    main_idx++;
            //});


            $.each(gOutputData.companyInfoAction, function (key1, val1) {
                if (key1 != "isReplicated") {
                    gOutputData.companyInfoAction[key1] = {};
                    gOutputData.companyInfoAction[key1] = jobLoInfo.customText;
                    //$.each(val1, function (key2, val2) {
                    //    val2 = jobLoInfo.customText;
                    //});
                }
            });


            gServiceCompanyData = gOutputData.companyInfoAction;
            //} else {
            //    //gOutputData.companyInfoAction[self.currentInHome().dateValue][self.selectedStore()] = jobLoInfo.customText[self.selectedLoanOfficer()];
            //    //gServiceCompanyData[self.currentInHome().dateValue][self.selectedStore()] = jobLoInfo.customText[self.selectedLoanOfficer()];
            //    $.each(gOutputData.companyInfoAction, function (key1, val1) {
            //        if (key1 != "isReplicated") {
            //            gOutputData.companyInfoAction[key1] = {};
            //            gOutputData.companyInfoAction[key1] = jobLoInfo.customText;
            //            //$.each(val1, function (key2, val2) {
            //            //    val2 = jobLoInfo.customText;
            //            //});
            //        }
            //    });
            //}
        }
    }

    self.buildCompanyInfo = function () {
        if (isNewVariableFieldListCustomers()) {
            var temp_company_info = {};
            var logos = [];
            $.each(gServiceCompanyData, function (key, val) {
                if (key == "variableFieldList") {
                    var locations = [];
                    var location_list = [];
                    var is_new_location = true;
                    var index = 1;
                    $.each(val, function (key1, val1) {
                        if (val1.name.toLowerCase().indexOf('wantslogo') > -1) {
                            if (location_list.length > 0) {
                                locations.push(location_list);
                            }
                            location_list = [];
                            logos.push(val1);
                        }
                        else {
                            if (key1 == 0) {
                                index = val1.name.match(/\d+/)[0];
                            }
                            if (val1.name.indexOf("store" + index) > -1) {
                                location_list.push(val1);
                                if (key1 == (val.length - 1)) {
                                    locations.push(location_list);
                                }
                            }
                            else {
                                if (location_list.length > 0) {
                                    locations.push(location_list);
                                }
                                location_list = [];
                                location_list.push(val1);
                                index = val1.name.match(/\d+/)[0];
                            }
                        }
                    });
                    if (logos.length > 0) {
                        locations.push(logos);
                    }
                    temp_company_info[key] = locations;
                }
                else {
                    temp_company_info[key] = val;
                }
            });

            ko.mapping.fromJS(temp_company_info, {}, company_info);
        }
        else if (jobCustomerNumber == SPORTSKING) {
            var temp_company_info = {};
            var logos = [];
            //$.each(gServiceCompanyData, function (key, val) {
            //    if (key == "variableFieldList") {
            //        var locations = [];
            //        var location_list = [];
            //        var is_new_location = true;
            //        var index = 1;
            //        $.each(val, function (key1, val1) {
            //            if (val1.name.toLowerCase().indexOf('wantslogo') > -1) {
            //                if (location_list.length > 0) {
            //                    locations.push(location_list);
            //                }
            //                location_list = [];
            //                logos.push(val1);
            //            }
            //            else {
            //                if (key1 == 0) {
            //                    index = val1.name.match(/\d+/)[0];
            //                }
            //                if (val1.name.indexOf("store" + index) > -1) {
            //                    location_list.push(val1);
            //                    if (key1 == (val.length - 1)) {
            //                        locations.push(location_list);
            //                    }
            //                }
            //                else {
            //                    if (location_list.length > 0) {
            //                        locations.push(location_list);
            //                    }
            //                    location_list = [];
            //                    location_list.push(val1);
            //                    index = val1.name.match(/\d+/)[0];
            //                }
            //            }
            //        });
            //        if (logos.length > 0) {
            //            locations.push(logos);
            //        }
            //        temp_company_info[key] = locations;
            //    }
            //    else {
            //        temp_company_info[key] = val;
            //    }
            //});

            ko.mapping.fromJS(gServiceCompanyData, {}, company_info);
            //var temp_company_info = {};
            //temp_company_info["isReplicated"] = false;
            //$.each(gServiceCompanyData, function (key, val) {
            //    if (key.toLowerCase() != "isreplicated" && key.toLowerCase() != "isenabled" && key.toLowerCase() != "name" && key.toLowerCase() != "type") {
            //        var temp_arr = ko.observableArray([]);
            //        var lbl = "";
            //        var field_arr = ko.observableArray([]);
            //        $.each(val, function (key1, val1) {
            //            if (jobCustomerNumber == SCOTTS && key != 'companyInfoDict' && key != 'companyInfo') {
            //                $.each(val1, function (key2, val2) {
            //                    //lbl = self.getPageDisplayName(key2).initCap();
            //                    //field_arr.push(new fieldInfo(lbl, val2, true, false));
            //                    field_arr.push(new fieldInfo(key2, val2, true, false));
            //                });
            //                temp_arr.push(field_arr);
            //                field_arr = ko.observableArray([]);
            //            } else {
            //                lbl = self.getPageDisplayName(key1).initCap();
            //                temp_arr.push(new fieldInfo(lbl, val1, true, false));
            //            }
            //        });
            //        //Need to be updated below based on the dictionary returned from service.
            //        if (key == "companyInfoDict") {
            //            //temp_company_info["companyInfo"] = temp_arr;
            //        }
            //        else
            //            temp_company_info["companyInfo"] = temp_arr;
            //        //temp_company_info["customInfo"] = temp_arr;
            //    }
            //});
            //var temp = [];
            //temp.push(temp_company_info);
            //company_info = temp;
        }
        else {
            $.each(gServiceCompanyData, function (key, val) {
                if (key.toLowerCase() != "isreplicated") {
                    company_info[key] = {};
                    $.each(val, function (key1, val1) {
                        $.each(val1, function (key2, val2) {
                            getDataFromJSON(val2, key, key1, val1, key2, val2);
                            //if (typeof (val1[key2]) == "object") {
                            //    if (temp_key == "")
                            //        temp_key = key2;
                            //    if (temp_key != key2) {
                            //        temp_company_info[temp_key] = temp_arr;
                            //        temp_key = key2;
                            //        temp_arr = [];
                            //    }
                            //    if (temp_key == "companyInfo")
                            //        show_chk = true;
                            //    else
                            //        show_chk = false;
                            //    if (key2 != "name" && key2 != "isEnabled") {
                            //        $.each(val2, function (inner_key1, inner_val1) {
                            //            temp_arr.push(new fieldInfo(inner_val1.label, inner_val1.value, inner_val1.isRequired, show_chk));
                            //        });
                            //        if (index == (Object.keys(val1).length - 1)) {
                            //            temp_company_info[temp_key] = temp_arr;
                            //            temp_key = key2;
                            //            temp_arr = [];
                            //        };
                            //    }
                            //    index++;
                            //}
                            //else {
                            //    if (temp_key == "")
                            //        temp_key = key2;
                            //    if (temp_key != key2) {
                            //        temp_company_info[temp_key] = temp_arr;
                            //        temp_key = key2;
                            //        temp_arr = [];
                            //    }
                            //    if (temp_key == "companyInfo")
                            //        show_chk = true;
                            //    else
                            //        show_chk = false;
                            //    if (key2 != "name" && key2 != "isEnabled") {
                            //        $.each(val2, function (inner_key1, inner_val1) {
                            //            temp_arr.push(new fieldInfo(inner_val1.label, inner_val1.value, inner_val1.isRequired, show_chk));
                            //        });
                            //        if (index == (Object.keys(val1).length - 1)) {
                            //            temp_company_info[temp_key] = temp_arr;
                            //            temp_key = key2;
                            //            temp_arr = [];
                            //        };
                            //    }
                            //    index++;
                            //}
                        });

                        if (!is_object) {
                            company_info[key][key1] = ko.observable({});
                            if (Object.keys(temp_company_info).length > 0) {
                                company_info[key][key1](temp_company_info);
                                temp_company_info = {};
                            }
                        }
                    });
                }
                else {
                    if (jobCustomerNumber != AAG)
                        if (val == true)
                            self.isReplicated(true);
                        else
                            self.isReplicated(false);
                }
            });
        }
        if (Object.keys(company_info).length > 0) {
            self.varCompanyInfo(company_info);
            var temp = ko.toJS(company_info);
            self.prevVarCompanyInfo(temp);
            company_info = {};
        }
    }

    self.buildCompanyInfo();

    function getDataFromJSON(data, key, key1, val1, key2, val2) {
        //if (jobCustomerNumber == AAG) {
        index = 0;
        if (key2 != "name" && key2 != "isEnabled") {
            is_object = true;
            temp_key2 = key2;
            var is_list = false;
            $.each(val2, function (key3, val3) {
                // $.each(val3, function (key4, val4) {
                if (temp_key == "")
                    temp_key = key3;
                if (temp_key != key3) {
                    temp_company_info[temp_key] = temp_arr;
                    temp_key = key3;
                    temp_arr = [];
                }
                if (temp_key == "companyInfo")
                    show_chk = true;
                else
                    show_chk = false;
                if (typeof (val3) == "object" && val3.length > 0)
                    is_list = true;
                else
                    is_list = false;
                temp_arr = (is_list) ? [] : {};
                if (key3 != "name" && key3 != "isEnabled") {
                    $.each(val3, function (inner_key1, inner_val1) {
                        temp_arr.push(new fieldInfo(inner_val1.label, inner_val1.value, inner_val1.isRequired, show_chk));
                    });
                    if (index == (Object.keys(val2).length - 1)) {
                        temp_company_info[temp_key] = temp_arr;
                        temp_key = key3;
                        temp_arr = (is_list) ? [] : {};
                    };
                }
                else {
                    temp_company_info[temp_key] = val3;
                }

                index++;
                //});
            });
            if (company_info[key][key1] == undefined) {
                company_info[key][key1] = [];
                //company_info[key][key1] = {};
            }
            //company_info[key][key1][temp_key2] = ko.observable({});
            if (Object.keys(temp_company_info).length > 0) {
                //company_info[key][key1][temp_key2](temp_company_info);
                //if(is_list)
                company_info[key][key1].push(temp_company_info);
                //else
                //                      company_info[key][key1][temp_key2](temp_company_info);
                //company_info[key][key1][temp_key2].push(temp_company_info);
                temp_company_info = {};
                is_list = false;
            }
            temp_key = "";
        }
        else {
            //if (temp_key == "")
            temp_key = key2;
            //if (temp_key != key2) {
            company_info[key][key1][temp_key] = temp_arr;
            temp_key = key2;
            temp_arr = [];
            //}
        }

        //}
        //else {
        //    if (temp_key == "")
        //        temp_key = key2;
        //    if (temp_key != key2) {
        //        temp_company_info[temp_key] = temp_arr;
        //        temp_key = key2;
        //        temp_arr = [];
        //    }
        //    if (temp_key == "companyInfo")
        //        show_chk = true;
        //    else
        //        show_chk = false;
        //    if (key2 != "name" && key2 != "isEnabled") {
        //        $.each(val2, function (inner_key1, inner_val1) {
        //            temp_arr.push(new fieldInfo(inner_val1.label, inner_val1.value, inner_val1.isRequired, show_chk));
        //        });
        //        if (index == (Object.keys(val1).length - 1)) {
        //            temp_company_info[temp_key] = temp_arr;
        //            temp_key = key2;
        //            temp_arr = [];
        //        };
        //    }
        //    index++;
        //}

    };

    self.confirmContinue = function (current_page, page_name, i_count, click_type) {
        var ctrls = $('#dvNavBar div ul li a.ui-icon-edit');
        if (ctrls.length > 0 && click_type == "onlycontinue" && hideSetupCompleteValidation()) {
            var tmp_list = "";
            $.each(ctrls, function (key, val) {
                tmp_list += (tmp_list != "") ? (key < (ctrls.length - 1)) ? ', ' : ' and ' + $(this).attr('data-inHome') : $(this).attr('data-inHome');
            });

            $('#confirmMsg').html('You have not completed setup for ' + tmp_list + '. Do you want to stay on this page to continue setup or go to the next page?');
            $('#okButConfirm').text('Go To Next Page');
            $('#cancelButConfirm').text('Continue Setup');

            $('#cancelButConfirm').bind('click', function () {
                $('#cancelButConfirm').unbind('click');
                $('#cancelButConfirm').attr('onclick', '$(\'#popupConfirmDialog\').popup(\'close\');');
                $('#popupConfirmDialog').popup('close');
            });
            $('#okButConfirm').bind('click', function () {
                $('#okButConfirm').unbind('click');
                $('#popupConfirmDialog').popup('close');
                goToNextPage("../jobcompanyInfo/jobCompanyInfo.html", "../jobbaseSelection/jobBaseSelection.html", 5, 'onlycontinue');
                self.updateCompanyInfo(current_page, page_name, i_count, click_type);
            });
            $('#popupConfirmDialog').popup('open');
            return false;
        }
        else {
            //goToNextPage("../jobcompanyInfo/jobCompanyInfo.html", "../jobbaseSelection/jobBaseSelection.html", 5, 'onlycontinue');
            return self.updateCompanyInfo(current_page, page_name, i_count, click_type);
        }
    };

    self.getUpdatedCustomTextLocations = function (click_type) {        
        var facility_id = getFacilityId();
        var job_number = (gOutputData.jobNumber != undefined && gOutputData.jobNumber != null && gOutputData.jobNumber != "") ? gOutputData.jobNumber : getJobNumber();

        var gEditJobService = serviceURLDomain + "api/JobTicket/";

        getCORS(gEditJobService + facility_id + "/" + job_number + "/asdf" /*+ sessionStorage.username*/, null, function (data) {
            gOutputData.variableTextAction = data.variableTextAction;
            gServiceCompanyData = data.variableTextAction;

            sessionStorage.jobSetupOutput = JSON.stringify(gOutputData);
            sessionStorage.jobSetupOutputCompare = JSON.stringify(gOutputData);

            if (self.locationSources != undefined && self.locationSources.length == 0) {
                ko.mapping.fromJS(location_sources, [], self.locationSources);
            }

            if (click_type == "save") {
                self.buildCompanyInfo();
                self.getSelectedInfo();
            }
            jQuery('input[data-fieldType=phone]').mask("(999) 999-9999");
            jQuery('input[data-fieldType=zip]').mask("99999");
        }, function (error_response) {
            showErrorResponseText(error_response, true);
        });
    };

    self.updateCompanyInfo = function (current_page, page_name, i_count, click_type) {
        if (isValidateCompanyInfo())
            self.validateForDataExistency();
        var is_valid = false;
        var msg = "";
        // Call validation function to validate the data in the fields.  If it returns true proceed further else stop further execution.
        var temp_json = ko.mapping.toJS(self.varCompanyInfo);
        var is_wants_upper_case = false;
        var output_json = [];
        var final_json = {};
        if ([SCOTTS, ACE, ANDERSEN, MILWAUKEE, SHERWINWILLIAMS, BENJAMINMOORE].indexOf(jobCustomerNumber) > -1) {
            if (temp_json.variableFieldList.length == 0) {
                $('#alertmsg').text("Please add at least one location.");
                $('#popupDialog').bind('okBut', function () {
                    $('#popupDialog').popup('close');
                });
                $('#popupDialog').popup('open');
                return false;
            }
            else {
                var is_empty_field, is_junk_field, is_invalid_phone;
                var tooltip_message = '', error_message = '';
                var controlsToValidate = temp_json.variableFieldList;
                $.each(controlsToValidate, function (key, val) {
                    $.each(val, function (key1, val1) {
                        if (val1.name.toLowerCase().indexOf('wantslogo') == -1) {
                            if (val1.value.trim() == '') {
                                tooltip_message = "Please enter all the location fields."
                                error_message = tooltip_message;
                                $('input:text[id=txt' + val1.name + ']').css('border', 'solid 2px yellow');
                                $('input:text[id=txt' + val1.name + ']')[0].title = tooltip_message;
                                is_empty_field = true;

                            }
                            else if (validateJunkCharacters(val1.value.trim()).length > 0) {
                                tooltip_message = "Please enter valid data.";
                                if (error_message == "")
                                    error_message = tooltip_message;
                                $('input:text[id=txt' + val1.name + ']').css('border', 'solid 2px yellow');
                                $('input:text[id=txt' + val1.name + ']')[0].title = tooltip_message;
                                is_junk_field = true;

                            }
                            else
                                $('input:text[id=txt' + val1.name + ']').css('border', 'none');

                            if (val1.name.toLowerCase().indexOf('phone') > -1) {
                                if (!validateGmcPhone(val1.value)) {
                                    tooltip_message = "Invalid Phone Number.";
                                    if (error_message == "")
                                        error_message = tooltip_message;
                                    $('input:text[id=txt' + val1.name + ']').css('border', 'solid 2px yellow');
                                    $('input:text[id=txt' + val1.name + ']')[0].title = tooltip_message;
                                    is_invalid_phone = true;
                                }
                                else
                                    $('input:text[id=txt' + val1.name + ']').css('border', 'none');
                            }
                        }
                    })
                })

                if (is_invalid_phone || is_empty_field || is_junk_field) {
                    $('#alertmsg').text(error_message);
                    $('#popupDialog').bind('okBut', function () {
                        $('#popupDialog').popup('close');
                    });
                    $('#popupDialog').popup('open');
                    return false;
                }
                var is_variableDict_updated = false;
                var diff = [];
                var g_output_data_compare = $.parseJSON(sessionStorage.jobSetupOutputCompare);
                var output_compare_list = [];
                if (g_output_data_compare.variableTextAction.variableFieldList != undefined && g_output_data_compare.variableTextAction.variableFieldList != null &&
                    g_output_data_compare.variableTextAction.variableFieldList != "" && g_output_data_compare.variableTextAction.variableFieldList.length > 0) {
                    output_compare_list = g_output_data_compare.variableTextAction.variableFieldList;                   
                }
                $.each(temp_json.variableFieldList, function (key, val) {
                    $.each(val, function (key1, val1) {
                        output_json.push(val1);
                        if (output_compare_list.length > 0 && !is_variableDict_updated) {
                            $.each(output_compare_list, function (k, v) {
                                if (v.name == val1.name && v.type == val1.type && v.value != val1.value) {
                                    is_variableDict_updated = true;
                                    diff.push(val1);
                                }
                            });
                        }                        
                    });
                });
                if (JSON.parse(g_output_data_compare.variableTextAction.wantsUpperCase) != JSON.parse(temp_json.wantsUpperCase)) {
                    is_wants_upper_case = true;
                    diff.push(output_json);
                }
                if (diff.length == 0 && ((output_compare_list.length == 0 && output_json.length > 0) || (output_json.length != output_compare_list.length))) {
                    diff.push(output_json);
                }
                if (diff.length > 0 && click_type != "save") {
                    var message = "Changes to the Custom Text have not been saved. Please save or discard your changes now.";
                    $('#confirmMsg').html(message);
                    $('#okButConfirm').text('Save');
                    $('#cancelButConfirm').text('Discard');
                    $('#okButConfirm').bind('click', function () {
                        $('#cancelButConfirm').unbind('click');
                        $('#okButConfirm').unbind('click');
                        $('#cancelButConfirm').attr('onclick', '$(\'#popupConfirmDialog\').popup(\'close\');');
                        $('#okButConfirm').text('Ok');
                        $('#cancelButConfirm').text('Cancel');
                        $('#popupConfirmDialog').popup('close');
                        gOutputData.variableTextAction.wantsUpperCase = temp_json.wantsUpperCase;
                        gOutputData.variableTextAction.variableFieldList = output_json;
                        gOutputData.isGmcDataUpdated = true;
                        sessionStorage.jobSetupOutput = JSON.stringify(gOutputData);
                        postJobSetUpData('save');
                        sessionStorage.isJobTicketUpdate = true;
                        if (is_wants_upper_case) {
                            self.getUpdatedCustomTextLocations("onlycontinue");
                        }                        
                        if (click_type == "onlycontinue" || click_type == undefined)
                            window.location.href = (page_name != undefined && page_name != null && page_name != "") ? page_name : current_page;
                        return true;
                    });
                    $('#cancelButConfirm').bind('click', function () {
                        $('#cancelButConfirm').unbind('click');
                        $('#okButConfirm').unbind('click');
                        $('#cancelButConfirm').attr('onclick', '$(\'#popupConfirmDialog\').popup(\'close\');');
                        $('#okButConfirm').unbind('click');
                        $('#okButConfirm').text('Ok');
                        $('#cancelButConfirm').text('Cancel');
                        $('#popupConfirmDialog').popup('close');
                        sessionStorage.jobSetupOutput = JSON.stringify(gOutputData);
                        if (click_type == "onlycontinue" || click_type == undefined)
                            window.location.href = (page_name != undefined && page_name != null && page_name != "") ? page_name : current_page;
                        return true;
                    });
                    $('#popupConfirmDialog').popup('open');
                    disableSaveForClosedJobs();
                    return false;
                }
                else {
                    if (diff.length > 0) {
                        gOutputData.isGmcDataUpdated = true;
                        sessionStorage.isJobTicketUpdate = true;
                    }
                    gOutputData.variableTextAction.wantsUpperCase = temp_json.wantsUpperCase;
                    gOutputData.variableTextAction.variableFieldList = output_json;
                }
            }
        }
        else if (jobCustomerNumber == SPORTSKING) {
            //temp_json = (temp_json.length > 0) ? temp_json[0] : [];
            //var tmp_json = {};
            //var key_name = "";
            //$.each(temp_json, function (key, val) {
            //    tmp_json = {};
            //    if (key != "isReplicated") {
            //            $.each(val, function (key1, val1) {
            //                key_name = val1.name.replace(' ', '').initLower();
            //                tmp_json[key_name] = val1.value;
            //            });
            //            //if (key != "companyInfo") { //This should not be changed.
            //            if (key == "companyInfo") { //This should not be changed.
                            
            //            }
            //            else {
            //                //Need to be updated below based on the dictionary returned from service.
            //                //gOutputData.variableTextAction["companyInfoDict"] = tmp_json;
            //            }

            //    }
            //});
            gOutputData.variableTextAction = temp_json;
        } else {
            gOutputData.companyInfoAction = temp_json;
        }
        if (msg != "")
            return false;
        sessionStorage.jobSetupOutput = JSON.stringify(gOutputData);
        if (click_type == "onlycontinue" || click_type == undefined)
            window.location.href = (page_name != undefined && page_name != null && page_name != "") ? page_name : current_page;
        else if (isNewCustomerTextCustomers() && is_wants_upper_case) { 
            postJobSetUpData('save');
            self.getUpdatedCustomTextLocations("save");
            return false;
        }
        return true;
    };
    self.navBarItemClick = function (data, event) {
        self.validateForDataExistency();
        $('div.ui-grid-a').find('label,input,textarea').remove();
        var ele = event.target || event.targetElement;
        $('#dvNavBar div ul li a').removeClass('ui-btn-active');
        $(ele).addClass('ui-btn-active');
        if ((gOutputData.selectedLocationsAction != undefined && gOutputData.selectedLocationsAction != null && gOutputData.selectedLocationsAction != "") && (gOutputData.selectedLocationsAction.selectedLocationsList.length > 1)) {
            self.selectedInHomeStoreCompanyInfo({});
        }
        self.currentInHome(ko.dataFor(ele));
        self.getSelectedInfo();
    };

    self.locationChanged = function (data, event) {
        self.validateForDataExistency();
        var ele;
        var selected_store;
        var ele = event.target || event.targetElement;
        var selected_store = $(ele).val();
        self.selectedStore(self.selectedLocation());
        self.getSelectedInfo();

        var curr_info = ko.toJS(self.varCompanyInfo());
        var prev_info = ko.toJS(self.prevVarCompanyInfo());
        var diff_between_json_objects = [];
        var is_diff_found = false;
        $.each(curr_info, function (key1, val1) {
            $.each(prev_info, function (key2, val2) {
                if (key1 == key2) {
                    $.each(val1, function (key3, val3) {
                        var curr_temp1 = val3;
                        var prev_temp2 = val2[key3];
                        diff_between_json_objects = DiffObjects(jQuery.parseJSON(JSON.stringify(curr_temp1)), jQuery.parseJSON(JSON.stringify(prev_temp2)));
                        if (diff_between_json_objects.length != 0) {
                            is_diff_found = true;
                            return false;
                        }
                    });
                }
                if (is_diff_found) {
                    return false;
                }
            });
            if (is_diff_found) {
                return false;
            }
        });

        if (is_diff_found) {
            self.isReplicated(false);
            $('#chkReplicate').checkboxradio('refresh');
        }
        else {
            self.isReplicated(true);
            $('#chkReplicate').checkboxradio('refresh');
        }
    };

    self.logoValueChanged = function (data, event) {
        var ele = event.target || event.targetElement;
        var ctrl_id = $(ele).attr('id');
        if (ctrl_id.toLowerCase().indexOf('ace') > -1) {
            data.value($(ele).is(':checked') ? "ace" : "");
        }
        else if (ctrl_id.toLowerCase().indexOf('truevalue') > -1) {
            data.value($(ele).is(':checked') ? "truevalue" : "");
        }
        else if (ctrl_id.toLowerCase().indexOf('doitbest') > -1) {
            data.value($(ele).is(':checked') ? "doitbest" : "");
        }
    };

    self.loanOfficerChanged = function (data, event) {
        var ele;
        var selected_store;
        var ele = event.target || event.targetElement;
        displayJobInfo();
        var header_text = $('#navHeader').text();
        header_text = header_text + (($("#ddlLoanOfficers option:selected").text() != '') ? ' (' + $("#ddlLoanOfficers option:selected").text() + ')' : '');
        $('#navHeader').text(header_text);
        var selected_lo = $(ele).val();
        self.selectedLoanOfficer(selected_lo);
        //self.prevVarCompanyInfo({});
        self.selectedInHomeStoreCompanyInfo({});
        self.getSelectedInfo();
    };

    self.loadSelectedLoanOfficerInfo = function () {
        //if (sessionStorage.setUpTwoDate != undefined && sessionStorage.setUpTwoDate != null && sessionStorage.setUpTwoDate != "" && JSON.parse(sessionStorage.setUpTwoDate) == true) {
        //    gOutputData.companyInfoAction["Event 1"][self.selectedStore()] = jobLoInfo.customText[self.selectedLoanOfficer()];
        //    gOutputData.companyInfoAction["Event 2"][self.selectedStore()] = jobLoInfo.customText[self.selectedLoanOfficer()];
        //    gServiceCompanyData["Event 1"][self.selectedStore()] = jobLoInfo.customText[self.selectedLoanOfficer()];
        //    gServiceCompanyData["Event 2"][self.selectedStore()] = jobLoInfo.customText[self.selectedLoanOfficer()];
        //}
        //else {
        //    gOutputData.companyInfoAction[self.currentInHome().dateValue][self.selectedStore()] = jobLoInfo.customText[self.selectedLoanOfficer()];
        //    gServiceCompanyData[self.currentInHome().dateValue][self.selectedStore()] = jobLoInfo.customText[self.selectedLoanOfficer()];
        //}
        //self.varCompanyInfo()[self.currentInHome().dateValue][self.selectedStore()] = [];
        //self.prevVarCompanyInfo({});
        //self.selectedInHomeStoreCompanyInfo({});
        //self.buildCompanyInfo();
        //self.getSelectedInfo();
        $('#popUpLoanOfficers').popup('close');
    }

    self.getSelectedInfo = function () {
        //if (jobCustomerNumber == AAG) {
        //    self.getSeletedLoanOfficerInfo();
        //}
        var is_found = false;
        self.selectedInHomeStoreCompanyInfo({});
        $('div.ui-grid-a').find('label,input,textarea').remove();
        if (!isNewCustomerTextCustomers()) {
            var selected_inhome = self.currentInHome().dateValue;

            $.each(self.varCompanyInfo(), function (key, val) {
                if (key == selected_inhome) {
                    self.selectedInhomeInfo(val);
                    $.each(val, function (key1, val1) {
                        //if (typeof (ko.toJS(val1)) == "object") {
                        //if (jobCustomerNumber == AAG) {
                        // $.each(val1, function (key2, val2) {
                        //if (sessionStorage.setUpTwoDate != undefined && sessionStorage.setUpTwoDate != null && sessionStorage.setUpTwoDate != "" && JSON.parse(sessionStorage.setUpTwoDate) == true) {
                        //    //var temp_arr = ko.observableArray([]);
                        //    //temp_arr.push(val1[self.selectedLoanOfficer() - 1]);
                        //    self.selectedInHomeStoreCompanyInfo([]);
                        //    self.selectedInHomeStoreCompanyInfo.push(val1[self.selectedLoanOfficer() - 1]);
                        //    is_found = true;
                        //    return false;
                        //}
                        //else
                        if (jobCustomerNumber == AAG) {
                            if (key1 == self.selectedLoanOfficer()) {
                                self.selectedInHomeStoreCompanyInfo(val1);
                                is_found = true;
                                return false;
                            }
                        }
                        else {
                            if (key1 == self.selectedStore()) {
                                self.selectedInHomeStoreCompanyInfo(val1);
                                is_found = true;
                                return false;
                            }
                        }
                        //});
                        //}
                        //else {
                        //    if (key1 == self.selectedStore()) {
                        //        self.selectedInHomeStoreCompanyInfo(val1());
                        //        is_found = true;
                        //        return false;
                        //    }
                        //}
                    });
                    if (is_found)
                        return false;
                }
            });
            var temp = ko.toJS(self.varCompanyInfo());
            self.updateInHomeNavBarUI(temp);
            $('div').trigger('create');
        }
        else {
            self.createComputedLogoChecks();
            self.selectedInHomeStoreCompanyInfo({});
            self.selectedInHomeStoreCompanyInfo(self.varCompanyInfo());
            $('#dvCmpInfo').trigger('create');
            if (isNewVariableFieldListCustomers()) {
                window.setTimeout(function () {
                    self.updateMyLocationDropdown();
                }, 100);
            }
        }
    };

    self.createComputedLogoChecks = function () {
        if (isNewVariableFieldListCustomers()) {
            //Compute ACE Logo Check
            self.varCompanyInfo().showACELogoCheck = ko.computed(function () {
                var show_logo = false;
                if (!hideACELogoCheck()) {
                    $.each(self.varCompanyInfo().variableFieldList(), function (key, val) {
                        $.each(val, function (key1, val1) {
                            if ((val1.name().toLowerCase().indexOf('name') > -1) && (val1.value().toLowerCase().indexOf('ace') > -1)) {
                                show_logo = true;
                                return false;
                            }
                        });
                        if (show_logo) {
                            $.each(self.varCompanyInfo().variableFieldList(), function (k, v) {
                                if (v.length == 1 && v[0].name().toLowerCase().indexOf('wantslogo') > -1) {
                                    v[0].value("ace");
                                    return false;
                                }
                            });
                            return false;
                        }
                    });
                }
                return show_logo;
            });
            //Compute True Value Logo Check
            self.varCompanyInfo().showTrueValueLogoCheck = ko.computed(function () {
                var show_logo = false;
                if (!hideTrueValueLogoCheck()) {
                    $.each(self.varCompanyInfo().variableFieldList(), function (key, val) {
                        $.each(val, function (key1, val1) {
                            if ((val1.name().toLowerCase().indexOf('name') > -1) && (val1.value().toLowerCase().replace(/ /g, '').indexOf('truevalue') > -1)) {
                                show_logo = true;
                                return false;
                            }
                        });
                        if (show_logo) {
                            $.each(self.varCompanyInfo().variableFieldList(), function (k, v) {
                                if (v.length == 1 && v[0].name().toLowerCase().indexOf('wantslogo') > -1) {
                                    v[0].value("truevalue");
                                    return false;
                                }
                            });
                            return false;
                        }
                    });
                }
                return show_logo;
            });
            //Compute Do It Best Logo Check
            self.varCompanyInfo().showDoItBestLogoCheck = ko.computed(function () {
                var show_logo = false;
                if (!hideDoItBestLogoCheck()) {
                    $.each(self.varCompanyInfo().variableFieldList(), function (key, val) {
                        $.each(val, function (key1, val1) {
                            if ((val1.name().toLowerCase().indexOf('name') > -1) && (val1.value().toLowerCase().replace(/ /g, '').indexOf('doitbest') > -1)) {
                                show_logo = true;
                                return false;
                            }
                        });
                        if (show_logo) {
                            $.each(self.varCompanyInfo().variableFieldList(), function (k, v) {
                                if (v.length == 1 && v[0].name().toLowerCase().indexOf('wantslogo') > -1) {
                                    v[0].value("doitbest");
                                    return false;
                                }
                            });
                            return false;
                        }
                    });
                }
                return show_logo;
            });
        }
    };

    self.updateInHomeNavBarUI = function (temp) {
        $.each(temp, function (key, val) {
            var is_valid = self.verifyData(val);
            if (is_valid) {
                $('#dvNavBar div ul li a[data-inHome="' + key + '"]').attr('data-icon', 'check').addClass('ui-icon-check').removeClass('ui-icon-edit');
            }
            else {
                $('#dvNavBar div ul li a[data-inHome="' + key + '"]').attr('data-icon', 'edit').addClass('ui-icon-edit').removeClass('ui-icon-check');
            }
        });
    };

    self.replicateChanged = function (data, event) {
        var ele = event.target || event.targetElement;
        $('#confirmMsg').html('Replicating Setup Entries will overwrite any previous information entered.');
        $('#okButConfirm').text('Continue');

        var curr_info = ko.toJS(self.varCompanyInfo());
        var prev_info = ko.toJS(self.prevVarCompanyInfo());
        var is_diff_found = false;
        $.each(curr_info, function (key1, val1) {
            $.each(prev_info, function (key2, val2) {
                if (key1 == key2) {
                    $.each(val1, function (key3, val3) {
                        var curr_temp1 = val3;
                        var prev_temp2 = val2[key3];
                        diff_between_json_objects = DiffObjects(jQuery.parseJSON(JSON.stringify(curr_temp1)), jQuery.parseJSON(JSON.stringify(prev_temp2)));
                        if (diff_between_json_objects.length != 0) {
                            is_diff_found = true;
                            return false;
                        }
                    });
                }
                if (is_diff_found) {
                    return false;
                }
            });
            if (is_diff_found) {
                return false;
            }
        });
        $('#cancelButConfirm').bind('click', function () {
            $('#cancelButConfirm').unbind('click');
            $('#okButConfirm').unbind('click');
            $('#cancelButConfirm').attr('onclick', '$(\'#popupConfirmDialog\').popup(\'close\');');
            self.isReplicated(false);
            $('#chkReplicate').checkboxradio('refresh');
            $('#popupConfirmDialog').popup('close');
        });
        $('#okButConfirm').bind('click', function () {
            $('#cancelButConfirm').unbind('click');
            $('#okButConfirm').unbind('click');
            $('#cancelButConfirm').attr('onclick', '$(\'#popupConfirmDialog\').popup(\'close\');');
            self.replicateData();
            self.isReplicated(true);
            $('#chkReplicate').checkboxradio('refresh');
            $('#popupConfirmDialog').popup('close');
        });
        if ($(ele).is(':checked') && is_diff_found) {
            $('#popupConfirmDialog').popup('open');
        }
    };
    self.replicateData = function () {
        var selected_inhome = self.currentInHome().dateValue;
        $.each(self.selectedInhomeInfo(), function (key1, val1) {
            var temp_key = key1;
            $.each(self.varCompanyInfo(), function (key2, val2) {
                if (selected_inhome != key2) {
                    //val2[temp_key](val1());
                    val2[temp_key] = val1;
                }
            });
        });
        //self.varCompanyInfo().isReplicated(true);
        var temp = ko.toJS(self.varCompanyInfo());
        self.prevVarCompanyInfo(temp);
        $.each(temp, function (key, val) {
            if (key != "isReplicated") {
                var is_valid = self.verifyData(val);
                if (is_valid) {
                    $('#dvNavBar div ul li a[data-inHome="' + key + '"]').attr('data-icon', 'check').addClass('ui-icon-check').removeClass('ui-icon-edit');
                }
                else {
                    $('#dvNavBar div ul li a[data-inHome="' + key + '"]').attr('data-icon', 'edit').addClass('ui-icon-edit').removeClass('ui-icon-check');
                }
            }
        });
    };

    self.validateForDataExistency = function () {
        var temp_info = ko.toJS(self.selectedInhomeInfo);
        var is_valid = self.verifyData(temp_info);
        var selected_inhome = self.currentInHome().dateValue;
        if (is_valid) {
            $('#dvNavBar div ul li a[data-inHome="' + selected_inhome + '"]').attr('data-icon', 'check').addClass('ui-icon-check').removeClass('ui-icon-edit');
        }
        else {
            $('#dvNavBar div ul li a[data-inHome="' + selected_inhome + '"]').attr('data-icon', 'edit').addClass('ui-icon-edit').removeClass('ui-icon-check');
        }
    };

    self.verifyData = function (temp_info) {
        var is_data_found = false;
        $.each(temp_info, function (key1, val1) {
            $.each(val1, function (key2, val2) {
                $.each(val2, function (key3, val3) {
                    if (key3.toLowerCase() == "companyinfo" || key3.toLowerCase() == "custominfo") {
                        $.each(val3, function (key4, val4) {
                            if (val4.value == "") {
                                is_data_found = false;
                                return false;
                            }
                            else {
                                is_data_found = true;
                            }
                        });
                        if (!is_data_found) {
                            return false;
                        }
                    }
                });
            });
            if (!is_data_found) {
                return false;
            }
        });
        return is_data_found;
    };

    self.showOrderedLocations = function () {
        $('#popUpLocations').popup('open');
    };
    self.showLoanOfficers = function () {
        $('#popUpLoanOfficers').popup('open');
    };

    self.manageLocationsPopup = function (data, event) {
        var ele = event.target || event.targetElement;
        if ($(ele).attr('data-btnType') == "submit") {
            self.previousSelectedStore(self.selectedStore());
        }
        else {
            self.selectedStore(self.previousSelectedStore());
            self.selectedLocation(self.previousSelectedStore())
        }
        $('#ddlLocations').selectmenu('refresh');
        displayJobInfo();
        var header_text = $('#navHeader').text();
        var sel_location = $('#ddlLocations option:selected').text();
        header_text = header_text + ' (' + sel_location + ')';
        $('#navHeader').text(header_text);
        $('#popUpLocations').popup('close');
    };
    self.previewMailing = function () {
        //Todo: Need to update pdf file for Kubota customer
        var encodedFileName = "DCA%2041941%20Jupiter%20Dental%20_Postcard_Dr.%20Vera_MECH.pdf";
        if ([SPORTSKING, KUBOTA, GWA, SCOTTS, ALLIED].indexOf(appPrivileges.customerNumber) > -1) {
            previewMailing();
            return false;
        }
        if (appPrivileges.customerNumber == DCA) {
            encodedFileName = "DCA%2041941%20Jupiter%20Dental%20_Postcard_Dr.%20Vera_MECH.pdf";
        }
        if (appPrivileges.customerNumber == AAG) {
            encodedFileName = "TCA_10232_AAG_Lunch_Learn_2_Presenters-2_Events_Lenfers_Culbreth_Let.pdf";
        }
        else if (appPrivileges.customerNumber == ALLIED) {
            encodedFileName = "GW_Berkheimer_Jansen.pdf";
        }
        getNextPageInOrder();
        var preview_proof = "../jobApproval/previewMailing.html?fileName=" + encodedFileName + "&jobNumber=" + sessionStorage.jobNumber;
        window.location.href = preview_proof;
    };

    self.locationSourceTypeChanged = function (data, event) {
        var ele = event.target || event.targetElement;
        var location_source_type = $(ele).val();
        switch (location_source_type) {
            case "new":
                $("#dvNewLocation").trigger('create');
            case "mylocations":
                $("#ddlMyLocations").selectmenu('refresh');
            default:
                break;
        }
        self.addLocationType(location_source_type);
    };

    self.updateMyLocationDropdown = function () {
        if ($.browser.msie)
            $("#ddlMyLocations option").prop('disabled', false);
        else {
            $("#ddlMyLocations").children("option").prop('disabled', false);
            $('#ddlMyLocations').children('option').show();
        }
        if (self.selectedInHomeStoreCompanyInfo().variableFieldList().length > 1) {
            var variable_field_list = ko.mapping.toJS(self.selectedInHomeStoreCompanyInfo().variableFieldList);
            $.each(variable_field_list, function (key, val) {
                $.each(val, function (key1, val1) {
                    if (val1.name.toLowerCase().indexOf('pk') > -1) {
                        if ($.browser.msie)
                            $("#ddlMyLocations option[value=" + val1.value + "]").prop('disabled', true);
                        else {
                            $("#ddlMyLocations").children("option[value=" + val1.value + "]").prop('disabled', true);
                            $('#ddlMyLocations').children('option[value=' + val1.value + ']').hide();
                        }
                    }
                });
            });
        }
    };

    self.showAddLocationPopup = function () {
        if (self.selectedInHomeStoreCompanyInfo().variableFieldList().length == (getCustomTextLocationsLimit() + 1)) {//1 extra collect wantslogo is added so we have to validate (max_limit + 1) (not max_limit)
            $('#alertmsg').text("Maximum " + getCustomTextLocationsLimit() + " locations are allowed on the postcard.");
            $('#popupDialog').bind('okBut', function () {
                $('#popupDialog').popup('close');
            });
            $('#popupDialog').popup('open');
            return false;
        }
        $("#ddllocationSource").val('').selectmenu('refresh').trigger('change');
        $("#ddlMyLocations").val('').selectmenu('refresh');
        self.createNewLocation();
        $("#dvNewLocation").trigger('create');
        window.setTimeout(function () {
            $('#popUpAddLocation').popup('open');
        });
    };

    self.createNewLocation = function () {
        var idx = self.getNextStoreIndex();

        self.mailingNewLocation([]);
        var fields = ["Name", "Address", "City", "Phone"];
        if (isStateAndZipNeededInLocation()) {
            fields = ["Name", "Address", "City", "State", "ZipCode", "Phone"];
        }
        var new_location_fields = [];
        for (var i = 0; i < fields.length; i++) {
            new_location_fields.push({
                "name": "store" + idx + fields[i],
                "value": "",
                "type": "string"
            });
        }
        
        ko.mapping.fromJS(new_location_fields, [], self.mailingNewLocation);
        jQuery('input[data-fieldType=phone]').mask("(999) 999-9999");
        if (isStateAndZipNeededInLocation()) {
            jQuery('input[data-fieldType=zip]').mask("99999");
        }
    };

    self.getNextStoreIndex = function () {
        var variable_dict;
        if (self.selectedInHomeStoreCompanyInfo().variableFieldList != undefined && self.selectedInHomeStoreCompanyInfo().variableFieldList != null && self.selectedInHomeStoreCompanyInfo().variableFieldList().length > 0) {
            variable_dict = ko.mapping.toJS(self.selectedInHomeStoreCompanyInfo().variableFieldList);
        }
        return getNextStoreIndex(variable_dict, false);
    };

    self.addNewLocation = function (data, e) {
        var selected_source = $("#ddllocationSource").val();
        if (selected_source == "") {
            $('#popUpAddLocation').popup('close');
            $('#alertmsg').html("Please select add location type.");
            $('#okBut').unbind('click');
            $('#okBut').bind('click', function () {
                $('#okBut').unbind('click');
                $('#popupDialog').popup('close');
                window.setTimeout(function () { $('#popUpAddLocation').popup('open'); }, 300);
            });
            $('#popupDialog').popup('open');
            return false;
        }
        var new_location = ko.observableArray([]);
        if (selected_source == "mylocations") {
            var selected_location_id = $("#ddlMyLocations").val();
            if (selected_location_id == "") {
                $('#popUpAddLocation').popup('close');
                $('#alertmsg').html("Please select at least one location from My Locations.");
                $('#okBut').unbind('click');
                $('#okBut').bind('click', function () {
                    $('#okBut').unbind('click');
                    $('#popupDialog').popup('close');
                    window.setTimeout(function () { $('#popUpAddLocation').popup('open'); }, 300);
                });
                $('#popupDialog').popup('open');
                return false;
            }
            var selected_location;
            $.each(allUserLocations, function (key, val) {
                if (val.pk == selected_location_id) {
                    selected_location = val;
                    return false;
                }
            });
            var idx = self.getNextStoreIndex();
            if (selected_location != undefined && selected_location != null && selected_location != "") {
                var fields = ["Name", "Address", "City", "Phone"];
                if (isStateAndZipNeededInLocation()) {
                    fields = ["Name", "Address", "City", "State", "Zip", "Phone"];
                }
                var mailing_new_location = [];
                for (var i = 0; i < fields.length; i++) {
                    switch (fields[i]) {
                        case "Name":
                            mailing_new_location.push({
                                "name": "store" + idx + "Name",
                                "value": selected_location.storeName != undefined ? selected_location.storeName : selected_location.location,
                                "type": "string"
                            });
                            break;
                        case "Address":
                            mailing_new_location.push({
                                "name": "store" + idx + "Address",
                                "value": selected_location.address1 != undefined ? selected_location.address1 : selected_location.address,
                                "type": "string"
                            });
                            break;
                        case "City":
                            mailing_new_location.push({
                                "name": "store" + idx + "City",
                                "value": selected_location.city,
                                "type": "string"
                            });
                            break;
                        case "Phone":
                            mailing_new_location.push({
                                "name": "store" + idx + "Phone",
                                "value": getFormattedPhone(selected_location.areaCode, ((selected_location.phone != undefined) ? selected_location.phone : selected_location.phoneNumber)),
                                "type": "string"
                            });
                            break;
                        case "Pk":
                            mailing_new_location.push({
                                "name": "store" + idx + "Pk",
                                "value": selected_location.pk,
                                "type": "string"
                            });
                            break;
                        case "State":
                            mailing_new_location.push({
                                "name": "store" + idx + "State",
                                "value": selected_location.state,
                                "type": "string"
                            });
                            break;
                        case "Zip":
                            mailing_new_location.push({
                                "name": "store" + idx + "Zip",
                                "value": selected_location.zip != undefined ? selected_location.zip : selected_location.zipCode,
                                "type": "string"
                            });
                            break;
                        default:
                            break;
                    }
                }
                ko.mapping.fromJS(mailing_new_location, [], new_location);
            }
        }
        else if (selected_source == "new") {
            var phone_num;
            var isEmptyField, isJunkField;
            var error_message = '';
            $.each(self.mailingNewLocation(), function (key, val) {
                if (val.value().trim() == '')
                    isEmptyField = true;
                else if (validateJunkCharacters(val.value().trim()).length > 0)
                    isJunkField = true;

                if (val.name().toLowerCase().indexOf("phone") > 1) {
                    phone_num = val.value();
                }
            })

            if (isEmptyField)
                error_message = "Please enter all the location fields.";
            else if (isJunkField)
                error_message = "Please enter valid data.";
            else if (!validateGmcPhone(phone_num))
                error_message = "Invalid Phone Number.";

            if (error_message != '') {
                $('#popUpAddLocation').popup('close');
                window.setTimeout(function () {
                    $('#alertmsg').html(error_message);
                    $('#okBut').unbind('click');
                    $('#okBut').bind('click', function () {
                        $('#okBut').unbind('click');
                        $('#popupDialog').popup('close');
                        window.setTimeout(function () { $('#popUpAddLocation').popup('open'); }, 300);
                    });
                    $('#popupDialog').popup('open');
                }, 100);
                return false;
            }
            else
                new_location = self.mailingNewLocation;
        }
        if (new_location().length > 0) {
            if (data.selectedInHomeStoreCompanyInfo().variableFieldList != undefined && data.selectedInHomeStoreCompanyInfo().variableFieldList().length == 0) {
                var logo_details = [
                    {
                        "name": "wantsLogo",
                        "value": "",
                        "type": "string"
                    }
                ];
                var logo_dict = ko.observableArray([]);
                ko.mapping.fromJS(logo_details, [], logo_dict);
                data.selectedInHomeStoreCompanyInfo().variableFieldList.push(logo_dict());
            }
            data.selectedInHomeStoreCompanyInfo().variableFieldList.push(new_location());
            $("#dvCmpInfo").trigger('create');
            window.setTimeout(function () {
                self.updateMyLocationDropdown();
            }, 100);
            jQuery('input[data-fieldType=phone]').mask("(999) 999-9999");
			jQuery('input[data-fieldType=zip]').mask("99999");
        }

        $('#popUpAddLocation').popup('close');
    };

    self.cancelNewLocation = function () {
        self.mailingNewLocation([]);
        $('#popUpAddLocation').popup('close');
    };

    self.removeLocation = function (data, event) {
        var ele = event.target || event.targetElement;
        var data_index = ko.contextFor(ele).$index();
        if (self.selectedInHomeStoreCompanyInfo().variableFieldList().length == 2) {
            window.setTimeout(function () {
                $('#alertmsg').html("At least one location is required to generate your proof.");
                $('#okBut').unbind('click');
                $('#okBut').bind('click', function () {
                    $('#okBut').unbind('click');
                    $('#popupDialog').popup('close');
                });
                $('#popupDialog').popup('open');
            }, 100);
            return false;
        }
        self.selectedInHomeStoreCompanyInfo().variableFieldList.splice(data_index, 1);
        if (self.selectedInHomeStoreCompanyInfo().variableFieldList().length == 1) {
            self.selectedInHomeStoreCompanyInfo().variableFieldList.splice(0, 1); //delete wantslogo flag from list
        }
        $("#dvCmpInfo").trigger('create');
        window.setTimeout(function () {
            self.updateMyLocationDropdown();
            jQuery('input[data-fieldType=phone]').mask("(999) 999-9999");
            jQuery('input[data-fieldType=zip]').mask("99999");
        }, 100);
    };
};

function showHomePage(type) {
    if (type == "home") {
        window.location.href = "../index.htm";
    }
    else if ((type == "template")) {
        window.location.href = " ../adminTemplateSetup/templateSetup.html";
    }
    else if ((type == "vcc")) {
        window.location.href = " ../adminTemplateSetup/variableCompanyInfo.html";
    }
};
String.prototype.endsWith = function (suffix) {
    return this.indexOf(suffix, this.length - suffix.length) !== -1;
};
//Capitalizes the first letter in the given string.
String.prototype.initCap = function () {
    return this.replace(/(?:^|\s)[a-z]/g, function (m) {
        return m.toUpperCase();
    });
};
String.prototype.initLower = function () {
    return this.replace(/(?:^|\s)[A-Z]/, function (m) {
        return m.toLowerCase();
    });
};
