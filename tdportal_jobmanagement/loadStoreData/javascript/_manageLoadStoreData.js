﻿var manageUploadLists = function () {
    var self = this;
    ko.utils.extend(self, new uploadFilesCommon(self));
    new dragAndDropFiles(self);
    //******************** Global Variables Start **************************
    self.filesToUpload = [];
    self.pagePrefs = "";
    self.selectedStates = [];
    self.uploadedSourceFilesCount = 0;
    self.uploadedSeedFilesCount = 0;
    self.uploadedDnmFilesCount = 0;

    //******************** Global Variables End **************************
    //******************** Public Functions Start **************************
    //Get the list of files uploaded from Job Ticket JSON session and displays over the page.
    //If the file list is not available then it creates an empty list.
    self.displayListFileInfo = function () {
        if (sessionStorage.storeDataList == undefined && sessionStorage.storeDataList == undefined) {
            self.makeGOutputData();
        }
        else {
            if (sessionStorage.storeDataList != undefined) {
                self.storeDataList = jQuery.parseJSON(sessionStorage.storeDataList);
                self.getFileListInfoFromSession();
            }
        }
    }

    //Gets the uploaded file list from Job Ticket JSON session.
    self.getFileListInfoFromSession = function () {
        var file_data;
        file_data = (self.storeDataList != undefined) ? self.storeDataList : [];

        if (file_data.length == 0) {
            file_data.push({
                sourceType: "",
                fileName: "",
                fileMap: "",
                priority: "",
                fileSizePretty: "",
                fileStatus: ""
            });
            self.buildOutputLists(false);
            if (sessionStorage.filesToUpload != undefined) {
                self.filesToUpload = jQuery.parseJSON(sessionStorage.filesToUpload);
                self.makeFileListBeforePost(self.filesToUpload);
            }
        }
    };
    //Generates the info if the Job Ticket JSON for upload is not available.
    self.makeGOutputData = function () {
        self.getFileListInfoFromSession();
    }

    //Creates the uploaded files list and updates the UI.
    self.makeFileList = function (array_name) {
        var array = eval(array_name);
        var list_info = "";
        var mail_files = "";
        var mail_files_cnt = 0;
        var dnm_files = "";
        var dnm_files_cnt = 0;
        var seed_files = "";
        var seed_files_cnt = 0;
        var source_type = "";
        $("#uploadList").empty();

        $("#btnMapSelected").hide();
        $("#btnRemoveSelected").hide();
        var replaceable_extensions = ['gz', 'zip'];
        $.each(array_name, function (key, val) {
            if (val.fileName != "") {
                source_type = getFileType(val.sourceType.toString());
                var file_ext = val.fileName.substring(val.fileName.lastIndexOf('.') + 1, val.fileName.length);
                if (replaceable_extensions.indexOf(file_ext.toLowerCase()) > -1) file_ext = file_ext.toLowerCase().replace('gz', 'txt').replace('zip', 'txt');
                var file_name = val.fileName.substring(0, val.fileName.lastIndexOf('.') + 1) + file_ext;
                var file_list = '<li><h3>' + file_name + '</h3><p>';
                file_list += 'File Size: ' + val.fileSizePretty + ' | File Status:' + val.fileStatus;
                '</p><a href="#" data-icon="delete" onclick="return pageObj.canDeleteListFile(\'' + file_name + '\',\'' + source_type + '\',' + getFileTypeCode(source_type) + ',\'' + '\');">Delete List</a></li>';
                mail_files += file_list;
                mail_files_cnt++;                

                $("#btnMapSelected").show();
                $("#btnRemoveSelected").show();
                $('#tblFiles').show();
            }
            else {
                $("#lnkUploadFile").show();
            }
        });
        self.uploadedSourceFilesCount = mail_files_cnt;
        self.uploadedSeedFilesCount = seed_files_cnt;
        self.uploadedDnmFilesCount = dnm_files_cnt;
        if (mail_files != "") {
            mail_grid_info = '<div id="dvSourceFiles" data-role="collapsible" data-theme="f" data-collapsed-icon="carat-r" data-expanded-icon="carat-d" data-collapsed="false"><h2>Source<span class="ui-li-has-count ui-li-count ui-btn-up-c ui-btn-corner-all" style="right:15px;float:right">' + mail_files_cnt + '</span></h2>';
            mail_grid_info += '<ul id="ulSourceFiles" data-role="listview" data-theme="c" data-divider-theme="c" data-split-icon="delete" data-split-theme="e" >';
            list_info += mail_grid_info + mail_files;
            list_info += '</ul></div>';
        }
        
        if (list_info != "") {
            $("#uploadList").empty();
            $("#uploadList").append(list_info);
            $("div ul").each(function (i) {
                $(this).listview();
            });
            if (appPrivileges.customerNumber == DCA || appPrivileges.customerNumber == CASEYS) {
                $("#dvSourceFiles").css('display', 'none');
            }
            $("#uploadList").collapsibleset('refresh');
            $("#dvSourceFiles h2 a").attr('href', '#sourceFiles');
            window.setTimeout(function () {
                $("#dvSourceFiles").collapsible('expand');
                $('#hidSourceType').val('');
            }, 1000);
        }
        if (self.gOutputData != null) {
            getBubbleCounts(self.gOutputData);
        }
    };
   
    self.browseClick = function () {
        self.resetFileUploadControl(self.filesToUpload);
        self.resetFields();
        getFile();
    };

    //Navigates to file mapper page when user (admin) clicks on the list item.
    self.navigateMapper = function (url, event) {
        $('#divError').text('');
        sessionStorage.continueToOrder = window.location.href.substring(window.location.href.lastIndexOf('/') + 1);
        window.location.href = url;
    };

    //Updates the list in the UI with the uploaded files list.
    self.updateFileGrid = function () {
        $.each(self.filesToUpload, function (key, value) {
            self.addFileInfo(value.sourceType, value.fileName);
        });
        //reset the fields...
        self.resetFields();
        $('#dvPopupListDefinitionContent').hide();
        $('#dvAdavancedPreference').hide();
        $('#fieldType').get(0).selectedIndex = 0;
        //if (appPrivileges.roleName == "admin")
        //    $('#select-choice-1').get(0).selectedIndex = 0;
        $('#ddlFilesUploaded').val('select').selectmenu('refresh');
        var upload_form = ($.browser.msie) ? window.frames[0].document.getElementById('spnFileInput') : document.getElementById('spnFileInput');
        var file_ctrls = $(upload_form).find('input[type=file]');
        if (file_ctrls.length > 0) {
            var newFileInput = file_ctrls[0].cloneNode(true);
            newFileInput.value = null;
            newFileInput.style.display = "";
            newFileInput.id = "mailFiles[]";
            newFileInput.name = "mailFiles[]";
            $((($.browser.msie) ? window.frames[0].document.getElementById('spnFileInput') : document.getElementById('spnFileInput'))).find('input[type=file]').remove();
            $((($.browser.msie) ? window.frames[0].document.getElementById('spnFileInput') : document.getElementById('spnFileInput'))).append(newFileInput);
            (($.browser.msie) ? $(window.frames[0].frames.frameElement).contents().find('input[type=file]') : $('input[type=file]')).bind('change', function (e) {
                self.navigate(e, $(e.target))
            });
            self.fileCount = 0;
        }
        self.filesToUpload = [];
        $("#divFilesToUpload").hide();
        sessionStorage.removeItem("filesToUpload");
    };
    //Gets the file info when the user clicks on edit button before upload the files.
    self.getFileSettings = function (file_name, source_type) {
        $('#btnUpdate').attr('onclick', "pageObj.updateEditFileInfo(this,'','','','')");
        $('#divError').text('');
        $('#dvPopupListDefinitionContent').show();
        $('#testButtonOk').hide();
        $((($.browser.msie) ? window.frames[0].document.getElementById('spnFileInput') : document.getElementById('spnFileInput')))[0].style.display = "none";
        $('#btnUpdate').show();
        $.each(self.filesToUpload, function (key, val) {
            if (getFileType(val.sourceType).toLowerCase() == source_type.toLowerCase() && val.fileName.substring(val.fileName.lastIndexOf("\\") + 1).toLowerCase() == file_name.toLowerCase()) {
                $('#fieldName').eq(0).closest('.ui-select').find('.ui-btn-text').find('span').html('Upload a New File');
                if (sessionStorage.roleName != "user" && sessionStorage.roleName != "power")
                    $('#fieldType').val(getFileType(val.sourceType)).selectmenu('refresh').trigger('onchange');
                if (!isMultiple && !isDragDrop) {
                    $('#name').val(val.fileName.substring(val.fileName.lastIndexOf("\\") + 1));
                }
                else if (isMultiple || isDragDrop) {
                    var file_type = ($("#fieldType").val() != "select") ? $("#fieldType").val() : "";
                    var file_found = [];
                    $.each(data, function (key1, val1) {
                        if (val1.name.toLowerCase() === val.fileName.substring(val.fileName.lastIndexOf("\\") + 1).toLowerCase() && (val1.type.toLowerCase() === getFileType(val.sourceType).toLowerCase())) {
                            file_found.push(val1);
                            return false;
                        }
                    });
                    //                    var file_found = $.grep(data, function (obj) {

                    //                    });
                    temp_data = [];
                    if (file_found.length > 0) {
                        temp_data.push(file_found[0]);
                        self.displayFileList();
                    }
                }
                $('#hidEditUploadFile').val(val.fileName.substring(val.fileName.lastIndexOf("\\") + 1) + '^' + val.sourceType);
            
                selectedKey = key;
            }
        });
        $('#hidMode').val('edit');
        self.popUpListDefinitionDisplay();
    };


    //File open dialog opens, and validates for the allowed types when user selects the file and displays a message if invalid file is selected.
    self.navigate = function (event, ctrl) {
        if ($(ctrl)[0].files != undefined && $(ctrl)[0].files != null && $(ctrl)[0].files.length > 0) {
            isMultiple = true;
            self.resetFileUploadControl(self.filesToUpload);
            self.fileCount = (self.fileCount <= 0) ? self.fileCount = 0 : self.fileCount - 1;
            var files_count = $(ctrl)[0].files.length;
            self.makeDrop(event, true, ctrl);
        }
        else if ($(ctrl)[0].files != undefined && $(ctrl)[0].files != null && $(ctrl)[0].files.length == 0) {
            return false;
        }
        else {
            var file_name = $(ctrl).val();
            var check_file_name = ($(ctrl).val().indexOf('\\') > -1) ? $(ctrl).val().substring($(ctrl).val().lastIndexOf('\\') + 1, $(ctrl).val().length) : $(ctrl).val();
            var myRegexp = /^([a-zA-Z0-9_\.\-\^\ ])+$/ig;
            if (!myRegexp.test(check_file_name.substring(0, check_file_name.lastIndexOf('.')))) {
                self.resetFileUploadControl(self.filesToUpload);
                self.fileCount = (self.fileCount <= 0) ? self.fileCount = 0 : self.fileCount - 1;
                $('#popupListDefinition').popup('close');
                $('#alertmsg').text('Illegal character in file name. Uploading cannot continue.');
                $('#alertmsg').addClass('wordwrap');
                $('#popupDialog').popup('open');
                return false;
            }
            else {
                var extension = file_name.split('.');
                var extension = file_name.substring(file_name.lastIndexOf('.') + 1);
                var allowedExtension = ["txt", "csv", "dat", "data", "lst", "xls", "xlsx", "doc", "docx", "gz", "zip"];
                var isValidFileFormat = false;
                var uploading_file_name = "";
                if (typeof (extension) != 'undefined' && extension.length > 1) {
                    for (var i = 0; i < allowedExtension.length; i++) {
                        if (extension.toLowerCase() == allowedExtension[i]) {
                            isValidFileFormat = true;
                            //var file_type = (sessionStorage.roleName == "user" || sessionStorage.roleName == "power") ? 'source' : $("#fieldType").val();
                            var file_type = $("#fieldType").val();
                            $('#hidUploadFile').val(file_name + '|' + file_type);
                            break;
                        }
                    }
                    if (!isValidFileFormat) {
                        self.resetFileUploadControl(self.filesToUpload);
                        self.fileCount = (self.fileCount <= 0) ? self.fileCount = 0 : self.fileCount - 1;
                        $('#popupListDefinition').popup('close');
                        $('#alertmsg').text('Invalid File Type...Only "TXT","CSV","DAT","DATA","LST","XLS","XLSX","DOC","GZ","ZIP" and "DOCX" types are accepted.');
                        $('#alertmsg').addClass('wordwrap');
                        $('#popupDialog').popup('open');
                        return true;
                    }
                    else if (self.verifyFileExistencyInUploads(check_file_name)) {
                        self.resetFileUploadControl(self.filesToUpload);
                        self.fileCount = (self.fileCount <= 0) ? self.fileCount = 0 : self.fileCount - 1;
                        $('#alertmsg').html('The selected file already exists in the list. Please select another file.');
                        $('#popupListDefinition').popup('close');
                        $("#popupDialog").popup('open');
                    }
                }
                var filemappername = file_name.substring(file_name.lastIndexOf('\\') + 1, file_name.lastIndexOf('.')) + "_map.xml";
                $('#name').val(file_name.substring(file_name.lastIndexOf("\\") + 1));
                //populate the file map field
                $('#txtFileMap').val(filemappername);
                if ($('#hidMode').val().toLowerCase() == "edit") {
                    $('#testButtonOk').hide();
                    $('#btnUpdate').show();
                }
                else {
                    $('#testButtonOk').show();
                    $('#btnUpdate').hide();
                }
            }
        }
    };

    self.resetFields = function () {
        $('#btnUpdate').attr('onclick', "pageObj.updateEditFileInfo(this,'','','','')");
        $('#fieldName').val('select').selectmenu('refresh');
        $('#fieldType').val('select').selectmenu('refresh').trigger('onchange');
        $('#ddlFilesUploaded').val('select').selectmenu('refresh');
        $('#name').val("");
        $('#btnDelete').hide();
        $('#dvText').show();
        $('#dvBrowse').show();
        $('#drop-area').html('Drop files here to select them <br /> or');
        $('#dvPopupListDefinitionContent').css('display', 'block');
        if ('draggable' in document.createElement('span')) {
            $("#drop-area").html('Drop files here to select them <br /> or');
            $('#dvDragDrop').css('display', 'block');
            $('#dvText').css('display', 'none');
            $('#testButtonOk').text('Upload');
        }
        else {
            $('#dvDragDrop').css('display', 'none');
            $('#dvText').css('display', 'block');
            $('#testButtonOk').text('Select');
        }
    };

    //Sets the headings for popup header based on the type selected.
    self.setHeadings = function (ctrl) {
        $("#popupListDefinition h3[id=popupHeader]").html('Select Data File');

        if ('draggable' in document.createElement('span')) {
            self.filesToUpload = [];
            self.uploadingFiles = "";
            data = [];
            temp_data = [];
            $('#drop-area').empty().html("Drop files here to select them <br />or");
        }

        var source_files_to_upload = 0;       
        $.each(self.filesToUpload, function (a, b) {
            source_files_to_upload++;
        });       
    }

    //Show/Hide popup controls in popup based on the type
    self.showHidePopupContent = function (ddlType) {
        var entityType = $(ddlType).val();
        if (entityType == "") {
            self.resetFields();
            $('#dvPopupListDefinitionContent').hide();
        }
        else if (entityType == 'add') {
            $('#dvPopupListDefinitionContent').show();
        }
        else if (entityType == 'upload') {
            $('#popupListDefinition').popup('close');
            $('#dvPopupListDefinitionContent').show();
        }
        self.popUpListDefinitionDisplay();

    };

    //Adds the new file information to the job ticket session..
    self.addFileInfo = function (source_type, filename) {
        $('#tblFiles').show();
        var file_name = filename;
        file_name = file_name.substring(file_name.lastIndexOf("\\") + 1);
        //file_name = file_name.substring(0, file_name.lastIndexOf('.') + 1) + file_name.substring(file_name.lastIndexOf('.') + 1).toLowerCase().replace('gz', 'txt');
        //file_name = file_name.substring(0, file_name.lastIndexOf('.') + 1) + file_name.substring(file_name.lastIndexOf('.') + 1).replace('gz', 'txt').replace('GZ', 'txt');
        var replaceable_extensions = ['gz', 'zip'];
        var file_ext = file_name.substring(file_name.lastIndexOf('.') + 1, file_name.length);
        if (replaceable_extensions.indexOf(file_ext.toLowerCase()) > -1) file_ext = file_ext.toLowerCase().replace('gz', 'txt').replace('zip', 'txt');
        file_name = file_name.substring(0, file_name.lastIndexOf('.') + 1) + file_ext;

        var is_file_exists = false;

        var file_info;

        var selected_link = getSessionData('advertiser_session');
        file_info = (self.storeDataList != undefined) ? self.storeDataList : 0;

        //eval(getURLDecode(pagePrefs.scriptBlockDict.addFileInfo));

        if (file_info.length > 0) {
            var file_list = [];
            file_list = jQuery.grep(file_info, function (obj) {
                return (parseInt(obj.sourceType) === parseInt(source_type));
            });
            if (!isNaN(parseInt(self.selectedKey)) && file_list.length > 0) {
                file_info[selectedKey].fileName = file_name;
                file_info[selectedKey].sourceType = source_type;
            }
            else {
                if (file_list.length > 0) {
                    $.each(file_list, function (key1, val1) {
                        if (val1.fileName == file_name && parseInt(source_type) == parseInt(val1.sourceType)) {
                            val1.fileName = file_name;
                            is_file_exists = true;
                            val1.fileSizePretty = "";
                            val1.fileStatus = "";
                        }
                    });
                }

                if (file_list.length == 0 && file_info[0].sourceType == "" && file_info[0].fileName == "") {
                    file_info[0].fileName = file_name;
                    file_info[0].sourceType = source_type;
                }
                else if (!is_file_exists) {
                    var temp_file = {};
                    temp_file = {
                        sourceType: source_type,
                        fileName: file_name,
                        fileMap: "",
                        priority: "",
                        fileSizePretty: "",
                        fileStatus: "",
                    };
                    file_info.push(temp_file);
                }
                else {
                    var temp_file = {
                        sourceType: source_type,
                        fileName: file_name,
                        fileMap: "",
                        priority: "",
                        fileSizePretty: "",
                        fileStatus: ""
                    };
                    file_info.push(temp_file);
                }
            }
        }
        self.selectedKey = '';
        sessionStorage.storeDataList = JSON.stringify(self.storeDataList);
    };

    //Updates the selected file information to the temporary session before upload and validates if file already exists.
    self.updateFileInfo = function (file_name, source_type) {
        var source_type_code = getFileTypeCode(source_type);
        source_type_code = 1;

        var file_info = jQuery.grep(self.filesToUpload, function (obj) {
            return (obj.fileName === file_name && parseInt(obj.sourceType) === parseInt(source_type_code));
        });

        if (file_info.length > 0) {
            file_info[0].sourceType = source_type_code;
            file_info[0].fileName = file_name;

            self.uploadingFiles = (self.uploadingFiles.split('|').length > 0) ? self.uploadingFiles.substring(0, self.uploadingFiles.lastIndexOf('|')) : "";
            //$('#validationAlertmsg').html('The selected file already exists in the list. Please select another file.');
            //$('#popupListDefinition').popup('close');
            //$("#validationPopupDialog").popup('open');
        }
        else {
            if (source_type_code != undefined && file_name != undefined) {
                var file_list_info = {};

                file_list_info["sourceType"] = source_type_code;
                file_list_info["fileName"] = file_name;
                self.filesToUpload.push(file_list_info);
            }
            else {
                if (!isMultiple && !isDragDrop)
                    self.resetFileUploadPopup();

                $('#popupListDefinition').popup('close');
                $('#hidUploadFile').val('');
            }
        }
        sessionStorage.filesToUpload = JSON.stringify(self.filesToUpload);
    };
     
    //Updates the selected file information in edit mode.
    self.updateEditFileInfo = function (ctrl, file_name, source_type, source_type_code, update_type) {
        if (update_type == "") {
            if ((!isMultiple && !isDragDrop) && !self.uploadValidation(true)) return false;
            if ((isMultiple || isDragDrop) && !self.uploadMultiplesValidation()) return false;


            var file_name = "";
            if (!isMultiple && !isDragDrop) {
                file_name = $('#name').val();
            }
            else {
                if (temp_data.length > 0) {
                    file_name = temp_data[0].name;
                }
            }
            //var source_type = (sessionStorage.roleName == "user" || sessionStorage.roleName == "power") ? 'source' : $('#fieldType').val();
            var source_type = ($('#fieldType').val() != "") ? $('#fieldType').val() : "";

            var source_type_code = getFileTypeCode(source_type);

            var old_file_name = $('#hidEditUploadFile').val().split('^')[0];
            var old_file_type = $('#hidEditUploadFile').val().split('^')[1];

            var file_info = jQuery.grep(self.filesToUpload, function (obj) {
                return (obj.fileName.substring(obj.fileName.lastIndexOf("\\") + 1) === old_file_name && obj.sourceType === old_file_type);
            });
            if (file_info.length > 0) {
                file_info[0].sourceType = source_type_code;
                file_info[0].fileName = file_name;
                $('#popupListDefinition').popup('close');
                if (!isMultiple && !isDragDrop)
                    self.resetFileUploadPopup();
            }
            if (isMultiple || isDragDrop) {
                var data_file_info = jQuery.grep(data, function (obj) {
                    return (obj.name.substring(obj.name.lastIndexOf("\\") + 1) === old_file_name && obj.type === getFileType(old_file_type));
                });

                if (data_file_info.length > 0 && temp_data.length > 0) {
                    data_file_info[0].name = temp_data[0].name;
                    data_file_info[0].type = source_type;
                    data_file_info[0].file = temp_data[0].file;
                }
            }
            temp_data = [];
            var uploading_files = self.uploadingFiles.split('|');
            var temp_uploading_files = "";
            $.each(uploading_files, function (key, val) {
                var file_parts = val.split('^');
                if (file_parts[0].toLowerCase() == old_file_name.toLowerCase()) {
                    file_parts[0] = file_name;
                    val = file_parts[0] + '^' + source_type_code;
                    temp_uploading_files = (temp_uploading_files != "") ? temp_uploading_files + '|' + val : val;
                    //return false;
                }
                else {
                    temp_uploading_files = (temp_uploading_files != "") ? temp_uploading_files + '|' + val : val;
                }
            });
            self.uploadingFiles = temp_uploading_files;
            self.makeFileListBeforePost(self.filesToUpload);
            sessionStorage.filesToUpload = JSON.stringify(self.filesToUpload);
        }
        else {
            self.updatePostUploadFile('list', file_name, source_type, source_type_code);
            postJobSetUpData('save');
        }
    };

    //Removes the selected file from the Uploaded files list.
    self.removeAFile = function (file_name, source_type, source_type_code) {
        $('#divError').text('');
        var file_delete_url = serviceURLDomain + "api/FileUpload/delete/" + self.facilityId + "/" + self.jobNumber + "/";
        var selected_link = getSessionData('advertiser_session');
        file_delete_url = file_delete_url + source_type_code + "/" + file_name;
        getCORS(file_delete_url, null, function (prod_data) {
            var file_info;
            if (prod_data != "" && $.parseJSON(prod_data).Message != undefined && $.parseJSON(prod_data).Message.indexOf('error') > -1) {
                $('#popupListDefinition').popup('close');
                $('#popupConfirmDialog').popup('close');
                $('#alertmsg').html('An error occurred. Please contact Administrator.');
                $('#popupDialog').popup('open', { positionTo: 'window' });
                //window.setTimeout(function displayErrorMsg() { $('#popupDialog').popup('open', { positionTo: 'window' }); }, 200);
                return false;
            }

            file_info = (self.storeDataList != undefined) ? self.storeDataList : 0;
            var remove_files = [];
            $.each(file_info, function (key1, val1) {
                if (val1.sourceType == source_type_code && val1.fileName.toLowerCase() == file_name.toLowerCase()) {
                    remove_files.push(key1);
                }
            });

            if (remove_files.length > 0) {
                $.each(remove_files, function (key2, val2) {                    
                    file_info.splice(val2, 1);
                    if (file_info.length == 0) {
                        file_info.push({
                            sourceType: "",
                            fileName: "",
                            fileMap: "",
                            priority: "",
                            fileSizePretty: "",
                            fileStatus: "",
                        });
                    }
                });
            }
            sessionStorage.storeDataList = JSON.stringify(self.storeDataList);
            self.buildOutputLists(false);
        }, function (error_response) {
            showErrorResponseText(error_response, true);
        });
        $('#popupListDefinition').popup('close');
        $('#popupConfirmDialog').popup('close');
    };

    //Resets the file upload popup.
    self.resetFileUploadPopup = function () {
        $('#fieldName').val('select').selectmenu('refresh');
        $('#fieldType').val('select').selectmenu('refresh');
        $('#ddlFilesUploaded').val('select').selectmenu('refresh');
        $('#name').val("");      
        $("#drop-area").html('Drop files here to select them <br /> or');
        $('#dvPopupListDefinitionContent').hide();
        $('#dvAdavancedPreference').hide();
        $('#fieldType').get(0).selectedIndex = 0;
    };

    //Hides the file upload settings popup.
    self.popUpListDefinitionHide = function () {
        var file_name_to_remove = $('#name').val();
        //var filetype_to_remove = (sessionStorage.roleName == "user" || sessionStorage.roleName == "power") ? 'source' : $('#fieldType').val();
        var filetype_to_remove = $('#fieldType').val();
        var source_type_code = getFileTypeCode(filetype_to_remove);
        var count = 0;
        var index = 0;
        if ($('#hidMode').val().toLowerCase() != "edit") {
            //remove the file from the local session.
            if (!isMultiple && !isDragDrop) {
                var files_to_remove = jQuery.grep(self.filesToUpload, function (v) {
                    count++;
                    //eval(getURLDecode(pagePrefs.scriptBlockDict.popUpListDefinitionHide));
                    if ((v.sourceType == source_type_code) && (v.fileName.substring(v.fileName.lastIndexOf("\\") + 1) == file_name_to_remove))
                        index = count - 1;
                    return (v.sourceType == source_type_code) ? ((v.fileName.substring(v.fileName.lastIndexOf("\\") + 1) == file_name_to_remove) ? false : true) : true;

                });
                if (($('#hidMode').val().toLowerCase() != "")) {
                    self.filesToUpload = files_to_remove;
                    if (self.filesToUpload.length == 0) {
                        $("#divFilesToUpload").hide();
                        var tdFileInputsTemp = ($.browser.msie) ? window.frames[0].document.getElementById('formUpload') : document.getElementById('formUpload');
                        var number_of_files_selected = $(tdFileInputsTemp).find('input[type=file]').length;
                        var fileInput = $(tdFileInputsTemp).find('input[type=file]')[number_of_files_selected - 1];
                        // Create a new file input
                        var newFileInput = fileInput.cloneNode(true);
                        newFileInput.value = null;
                        var id = "mailFiles[]";
                        newFileInput.id = id // A unique id
                        newFileInput.name = newFileInput.id;
                        newFileInput.style.display = "";
                        $($(tdFileInputsTemp).find('input[type=file]')[number_of_files_selected - 1]).remove();
                        var spnFileInputs = ($.browser.msie) ? window.frames[0].document.getElementById('spnFileInput') : document.getElementById('spnFileInput');
                        spnFileInputs.appendChild(newFileInput);
                        (($.browser.msie) ? $(window.frames[0].frames.frameElement).contents().find('input[type=file]') : $('input[type=file]')).bind('change', function (e) {
                            self.navigate(e, $(e.target))
                        });
                        self.fileCount = 0;
                    }
                    else if (!isMultiple && !isDragDrop) {
                        //remove the file control from the form.
                        self.resetFileUploadControl(self.filesToUpload);
                        self.fileCount = (self.fileCount <= 0) ? 0 : self.fileCount - 1;
                        self.makeFileListBeforePost(self.filesToUpload)
                    }
                }
                else if (!isMultiple && !isDragDrop) {
                    self.resetFileUploadControl(self.filesToUpload);
                    self.fileCount = (self.fileCount <= 0) ? 0 : self.fileCount - 1;
                    self.makeFileListBeforePost(self.filesToUpload)
                }
            }
            else {
                if (isMultiple || isDragDrop) {
                    //if (temp_edit_file.length === 0) {
                    //    $.each(temp_data, function (key, val) {
                    //        var file_index = "";
                    //        for (var i = 0; i < data.length; i++) {
                    //            if (data[i].name.toLowerCase() == val.name.toLowerCase() && data[i].type.toLowerCase() == val.type.toLowerCase()) {
                    //                file_index = i;
                    //            }
                    //        }
                    //        if (file_index !== "") {
                    //            data.splice(file_index, 1);
                    //            file_index = "";
                    //        }

                    //    });
                    //}
                    //if (temp_edit_file.length > 0 && $('#hidMode').val() == "edit") {
                    //    data.push(temp_edit_file[0]);
                    //    temp_edit_file = [];
                    //} else if (temp_data.length > 0 && $('#hidMode').val() == "edit") {
                    //    data.push(temp_data[0]);
                    //}
                    data = [];
                    temp_data = [];
                    self.filesToUpload = [];
                    self.uploadingFiles = "";
                }
            }
        }
        else {
            if (isMultiple || isDragDrop) {
                //if (temp_edit_file.length === 0) {
                //    $.each(temp_data, function (key, val) {
                //        var file_index = "";
                //        for (var i = 0; i < data.length; i++) {
                //            if (data[i].name.toLowerCase() == val.name.toLowerCase() && data[i].type.toLowerCase() == val.type.toLowerCase()) {
                //                file_index = i;
                //            }
                //        }
                //        if (file_index !== "") {
                //            data.splice(file_index, 1);
                //            file_index = "";
                //        }

                //    });
                //}
                //if (temp_edit_file.length > 0 && $('#hidMode').val() == "edit") {
                //    data.push(temp_edit_file[0]);
                //    temp_edit_file = [];
                //}
                //else if (temp_data.length > 0 && $('#hidMode').val() == "edit") {
                //    data.push(temp_data[0]);
                //}
                data = [];
                temp_data = [];
                self.filesToUpload = [];
                self.uploadingFiles = "";
            }
        }
        sessionStorage.filesToUpload = JSON.stringify(self.filesToUpload);
        if (!isMultiple && !isDragDrop)
            self.resetFileUploadPopup();
        $('#hidMode').val('');
        $('#popupListDefinition').popup('open');
        $('#popupListDefinition').popup('close');
        $('#hidUploadFile').val('');
    };

    //Creates the list of files selected to upload and display in UI.
    self.makeFileListBeforePost = function (array_name) {
        var array = eval(array_name);
        var list_info = "";
        var mail_files = "";
        var dnm_files = "";
        var seed_files = "";
        var select_files = "";
        $("#ulFilesToUpload").empty();
        var row_to_add = (array_name.length > 0) ? '<li data-role="list-divider" >Selected List Files for Upload</li>' : '';

        $.each(array_name, function (key, val) {
            if (val.fileName != "") {

                var file_name = val.fileName;
                file_name = file_name.substring(file_name.lastIndexOf("\\") + 1);
                var source_type = getFileType(val.sourceType);

                var drop_all_states_except = "";
                row_to_add += '<li><a href="#popupListDefinition" data-rel="popup" data-position-to="window" data-transition="pop" onclick="pageObj.getFileSettings(\'' + file_name + '\', \'' + source_type + '\')">';
                row_to_add += '<h3>' + file_name + '</h3>';
                row_to_add += '<p>';
                row_to_add += (jobCustomerNumber == CW || jobCustomerNumber == BRIGHTHOUSE || jobCustomerNumber == OH) ? 'List Type: ' + getDisplayFileType(source_type) + '</p>' : 'List Type: ' + getDisplayFileType(source_type) + '</p>'; 

                row_to_add += '</a><a href="#" onclick="pageObj.removeFileBeforeUpload(event,\'' + file_name + '^' + getFileType(val.sourceType) + '\')" >Remove List</a></li>';

                $("#popupListDefinition").popup("close");
            }
            else {
                $("#lnkUploadFile").show();
            }
        });
        if (row_to_add != "") {
            $("#ulFilesToUpload").append(row_to_add);

            $("#ulFilesToUpload").listview('refresh');
            $("#divFilesToUpload").show();
            $('#dvSubmit').css('display', 'block');
        }
        else {
            $('#dvSubmit').css('display', 'none');
        }
    };

    //Validates the file selection settings.
    self.uploadValidation = function (validate_all) {
        var msg = "<ul>";
        if ((!isMultiple && !isDragDrop) && !('draggable' in document.createElement('span'))) {
            if ($('#fieldType').val() == "select")// && (appPrivileges.roleName != "user" && sessionStorage.roleName != "power"))
                msg += "<li>Please select List type.</li>";
            if (validate_all) {
                if ($('#name').val() == "")
                    msg += "<li>Please choose a file to upload.</li>";
            }
        }
        msg += "</ul>";

        if (msg != "<ul></ul>") {
            $("#popupListDefinition").popup("close");
            $('#validationAlertmsg').html(msg);
            window.setTimeout(function getDelay() {
                $('#validationPopupDialog').popup('open');
                return false;
            }, 200);

        }
        else
            return true;
    };

    //Updates the list of files to be uploaded.
    self.updateFileList = function () {
        if ((!isMultiple && !isDragDrop) && !self.uploadValidation(true)) return false;
        if ((isMultiple || isDragDrop || ('draggable' in document.createElement('span'))) && !self.uploadMultiplesValidation()) return false;

        var file_info = "";
        var file_name = ""
        var source_type = "";
        var source_type_code = "";
        if (!isMultiple && !isDragDrop) {
            var file_info = $('#hidUploadFile').val();
            var file_name = file_info.split('|')[0];
            var source_type = file_info.split('|')[1];
            var source_type_code = getFileTypeCode(source_type);
            source_type_code = 1;
            var uploading_file_name = "";
            if (file_name != "") {
                uploading_file_name = file_name.substring(file_name.lastIndexOf("\\") + 1);
                if (!self.verifyFileExistence(uploading_file_name, source_type_code)) {
                    $('#popupListDefinition').popup('close');
                    $('#alertmsg').text('The selected file(s) already exist(s) in the list. Please select another file(s).');
                    self.resetFileUploadControl(self.filesToUpload);
                    self.fileCount = (self.fileCount <= 0) ? self.fileCount = 0 : self.fileCount - 1;
                    window.setTimeout(function getDelay() {
                        $('#popupDialog').find('a[id=okBut]').bind('click', function () {
                            $("#popupDialog").popup("close");
                            $('#popupDialog').find('a[id=okBut]').attr('onclick', "$(\'#popupDialog\').popup(\'close\')");
                        });
                        $('#popupDialog').popup('open');
                        return false;
                    }, 200);
                    return false;
                }
                else {
                    if ($('#fieldType').val() == "replace") {
                        $('#popupListDefinition').popup('close');
                        $('#popupEditList').popup('close');
                        $('#confirmMsg').html('Replacing the store data will delete all of the current store data. Are you sure you want to Replace?');
                        $('#okButConfirm').text('Continue');
                        $('#okButConfirm').unbind('click');
                        $('#okButConfirm').bind('click', function () {
                            $('#okButConfirm').unbind('click');
                            self.uploadingFiles += (self.uploadingFiles != "") ? "|" + uploading_file_name + "^" + source_type_code : uploading_file_name + "^" + source_type_code;
                            self.updateFileInfo(file_name, source_type);
                            self.makeFileListBeforePost(self.filesToUpload);
                            $("#popupListDefinition").popup("close");
                        });
                        $('#cancelButConfirm').text('Cancel');
                        $('#cancelButConfirm').unbind('click');
                        $('#cancelButConfirm').bind('click', function () {
                            $('#cancelButConfirm').unbind('click');
                            window.setTimeout(function () {
                                $("#popupListDefinition").popup("open");
                            }, 300);
                        });
                        $('#popupConfirmDialog').popup('open', { positionTo: 'window' });
                    }
                    else {
                        self.uploadingFiles += (self.uploadingFiles != "") ? "|" + uploading_file_name + "^" + source_type_code : uploading_file_name + "^" + source_type_code;
                        self.updateFileInfo(file_name, source_type);
                        self.makeFileListBeforePost(self.filesToUpload);
                        $("#popupListDefinition").popup("close");
                    }
                }
            }
            else {
                $("#popupListDefinition").popup("close");
            }
        }
        else {
            $('#btnUploadFiles').trigger('click');
        }
    };

    //Verifies the existence of the file in the list of files selected to upload.
    self.verifyFileExistence = function (file_name, source_type_code) {
        var list_files_to_upload = self.filesToUpload;
        var list_files_uploaded = (self.storeDataList != undefined) ? self.storeDataList : [];
        var to_upload = [];
        if ((!isMultiple && !isDragDrop)) {
            to_upload = $.grep(self.filesToUpload, function (obj) {
                return (obj.fileName.substring(obj.fileName.lastIndexOf('\\') + 1) === file_name && parseInt(obj.sourceType) === parseInt(source_type_code));
            });
        }
        var upload_files = [];
        upload_files = $.grep(list_files_uploaded, function (obj) {
            return (obj.fileName === file_name && parseInt(obj.sourceType) === parseInt(source_type_code));
        });
        if (to_upload.length > 0 || Object.keys(upload_files).length > 0) {
            return false;
        }
        return true;
    };

    //Removes the file from the temp session and the UI list files before upload
    self.removeFileBeforeUpload = function (e, file_name) {
        $('#divError').text('');
        var spnFileInputs = ($.browser.msie) ? window.frames[0].document.getElementById('spnFileInput') : document.getElementById('spnFileInput');
        var file_name_to_remove = file_name.split('^')[0];
        var filetype_to_remove = file_name.split('^')[1]

        var source_type_code = "";
        source_type_code = getFileTypeCode(filetype_to_remove);
        var count = 0;
        var index = -1;

        //remove the file from the local session.
        var files_to_remove = jQuery.grep(self.filesToUpload, function (v) {
            count++;
            if ((v.sourceType == source_type_code) && (v.fileName.substring(v.fileName.lastIndexOf("\\") + 1) == file_name_to_remove))
                index = count - 1;
            return (v.sourceType == source_type_code) ? ((v.fileName.substring(v.fileName.lastIndexOf("\\") + 1) == file_name_to_remove) ? false : true) : true;
        });
        if (index > -1) {
            var uploading_files = self.uploadingFiles.split('|');
            self.uploadingFiles = [];
            $.each(uploading_files, function (a, b) {
                var file_info = b.split('^');
                if (file_name_to_remove.toLowerCase() == file_info[0].toLowerCase() && file_info[1] == source_type_code) {
                }
                else {
                    self.uploadingFiles += (self.uploadingFiles != "") ? '|' + b : b;
                }
            });

        }
        if (isMultiple || isDragDrop) {
            count = 0;
            var data_files_to_remove = jQuery.grep(data, function (v) {
                count++;
                if ((v.type == filetype_to_remove) && (v.name.substring(v.name.lastIndexOf("\\") + 1) == file_name_to_remove))
                    index = count - 1;
                return (v.type == filetype_to_remove) ? ((v.name.substring(v.name.lastIndexOf("\\") + 1) == file_name_to_remove) ? false : true) : true;
            });
            if (index > -1) {
                data.splice(index, 1);
            }
        }
        self.filesToUpload = files_to_remove;
        if (isMultiple || isDragDrop) {
            self.fileCount = (self.fileCount <= 0) ? 0 : self.fileCount - 1;
        } else if (self.filesToUpload.length == 0 && (!isMultiple && !isDragDrop)) {
            $("#divFilesToUpload").hide();
            var tdFileInputsTemp = ($.browser.msie) ? window.frames[0].document.getElementById('formUpload') : document.getElementById('formUpload');
            var number_of_files_selected = $(tdFileInputsTemp).find('input[type=file]').length;
            var fileInput = $(tdFileInputsTemp).find('input[type=file]')[number_of_files_selected - 1];
            // Create a new file input
            var newFileInput = fileInput.cloneNode(true);
            newFileInput.value = null;
            var id = "mailFiles[]";
            newFileInput.id = id // A unique id
            newFileInput.name = newFileInput.id;
            newFileInput.style.display = "";
            $($(tdFileInputsTemp).find('input[type=file]')[number_of_files_selected - 1]).remove();
            var spnFileInputs = ($.browser.msie) ? window.frames[0].document.getElementById('spnFileInput') : document.getElementById('spnFileInput');
            spnFileInputs.appendChild(newFileInput);
            if (!isMultiple && !isDragDrop)
                self.resetFileUploadPopup();
            (($.browser.msie) ? $(window.frames[0].frames.frameElement).contents().find('input[type=file]') : $('input[type=file]')).bind('change', function (e) {
                self.navigate($(e.target))
            });
            fileCount = 0;
        }
        else {
            //remove the file control from the form.
            $($((($.browser.msie) ? window.frames[0].document.getElementById('formUpload') : document.getElementById('formUpload'))).find('input[type=file]')[index]).remove();
            var number_of_files_selected = $('input[type=file]').length;
            if (number_of_files_selected > 0) {
                $($((($.browser.msie) ? window.frames[0].document.getElementById('formUpload') : document.getElementById('formUpload'))).find('input[type=file]')[number_of_files_selected - 1]).attr("id", "mailFiles[]");
                $($((($.browser.msie) ? window.frames[0].document.getElementById('formUpload') : document.getElementById('formUpload'))).find('input[type=file]')[number_of_files_selected - 1]).attr("name", "mailFiles[]");
            }
            self.fileCount = (self.fileCount <= 0) ? 0 : self.fileCount - 1;
        }
        self.makeFileListBeforePost(self.filesToUpload);
        sessionStorage.filesToUpload = JSON.stringify(self.filesToUpload);
    };

    //Opens the file upload popup with file uploaded control added to UI dynamically.
    self.addNewFile = function () {
        $('#divError').text('');
        $('#hidMode').val('');
        self.openNewFileDialog();
    };

    //Opens the File upload popup and validates before opening for maximum files that can be uploaded to a job/order.
    self.openNewFileDialog = function () {
        if (self.storeDataList == undefined)
            self.storeDataList = jQuery.parseJSON(sessionStorage.storeDataList);
        var source_files_to_upload = 0;
        $.each(self.filesToUpload, function (a, b) {
            if (getFileType(b.sourceType) == "source")
                source_files_to_upload++;
        });
        //(appPrivileges.roleName.toLowerCase() == "user" || appPrivileges.roleName.toLowerCase() == "power") && 
        if (((source_files_to_upload > self.maxNumberOfFiles) || (self.uploadedSourceFilesCount >= self.maxNumberOfFiles) || ((source_files_to_upload + self.uploadedSourceFilesCount > self.maxNumberOfFiles)))) {
            var msg = "The maximum number of files to be uploaded per order has been reached. Please remove a previously uploaded file in order to upload a new file to this order. If this order requires more than " + self.maxNumberOfFiles + " files, please contact your Account Manager.";
            $('#alertmsg').text(msg);
            $('#popupDialog').popup('open');
            return false;
        }
        else {
            if (!('draggable' in document.createElement('span'))) {
                self.addFileInput();
            }
            self.resetFields();
            $('#testButtonOk').show();
            $(($.browser.msie) ? window.frames[0].document.getElementById('spnFileInput') : document.getElementById('spnFileInput'))[0].style.display = "";
            $('#btnUpdate').hide();
            $('#hidUploadFile').val('');
            self.popUpListDefinitionDisplay();
        }
    };

    //Validates the selected files in list have been uploaded to the job or not.
    self.validateUploadingListFiles = function () {
        if (self.filesToUpload.length > 0) {
            var msg = "The files you have selected have not been uploaded to this order. Please click the 'Upload Selected Lists' button to upload the selected files or Remove selected files you do not want to upload.";
            $('#alertmsg').text(msg);
            $('#popupDialog').popup('open');
            return false;
        }
        else {
            self.persistDropAllStatesExcept();
            return true;
        }
    };

    //Closes the popup dialog.
    self.closeDialog = function () {
        $('#popupDialog').popup('open');
        $('#popupDialog').popup('close');
    }

    //Gets user confirmation for deleting a list file.
    self.canDeleteListFile = function (file_name, source_type, source_type_code) {
        $('#popupListDefinition').popup('close');
        $('#confirmMsg').html('Are you sure you want to delete selected file?');
        $('#okButConfirm').attr('onclick', 'return pageObj.removeAFile(\'' + file_name + '\',\'' + source_type + '\',' + source_type_code + '\');');
        $('#okButConfirm').text('Delete');
        $('#popupConfirmDialog').popup('open', { positionTo: 'window' });
    };

    self.verifyFileExistencyInUploads = function (file_name) {
        var is_file_exists = false;
        if ('draggable' in document.createElement('span')) {
            if (data.length > 0) {
                $.each(data, function (key, val) {
                    if (val.name.toLowerCase() == file_name.toLowerCase())
                        is_file_exists = true;
                });
            }
        }

        file_info = (self.storeDataList != undefined) ? self.storeDataList : 0;
        if (file_info.length > 0) {
            $.each(file_info, function (key, val) {
                if (val.fileName.toLowerCase().substring(val.fileName.lastIndexOf('\\') + 1, val.fileName.length) == file_name.toLowerCase())
                    is_file_exists = true;
            });
        }
        if (self.filesToUpload.length > 0) {
            $.each(self.filesToUpload, function (key, val) {
                if (val.fileName.toLowerCase().substring(val.fileName.lastIndexOf('\\') + 1, val.fileName.length) == file_name.toLowerCase())
                    is_file_exists = true;
            });
        }

        return is_file_exists;
    }

    //Resets the Upload control after the replace action is completed.
    self.resetReplaceFileCtrl = function () {
        //reset the fields...
        self.resetFields();
        if ('draggable' in document.createElement('span')) {
            var upload_form = ($.browser.msie) ? window.frames[0].document.getElementById('spnFileInput') : document.getElementById('spnFileInput');
            var file_ctrls = $(upload_form).find('input[type=file]');
            if (file_ctrls.length > 0) {
                var newFileInput = file_ctrls[0].cloneNode(true);
                newFileInput.value = null;
                newFileInput.style.display = "";
                newFileInput.id = "mailFiles[]";
                newFileInput.name = "mailFiles[]";
                $((($.browser.msie) ? window.frames[0].document.getElementById('spnFileInput') : document.getElementById('spnFileInput'))).find('input[type=file]').remove();
                $((($.browser.msie) ? window.frames[0].document.getElementById('spnFileInput') : document.getElementById('spnFileInput'))).append(newFileInput);
                (($.browser.msie) ? $(window.frames[0].frames.frameElement).contents().find('input[type=file]') : $('input[type=file]')).bind('change', function (e) {
                    self.navigate(e, $(e.target))
                });
                self.fileCount = 0;
            }
        }
    };

    //******************** Public Functions End **************************
};