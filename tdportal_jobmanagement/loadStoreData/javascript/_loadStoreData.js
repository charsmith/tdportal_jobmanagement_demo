﻿$.mobile.popup.prototype.options.history = false;
jQuery.support.cors = true;
var pageObj;
var isDragDrop = false;
var isMultiple = false;
var formData;
var data = [];
var temp_data = [];
var temp_edit_file = [];
var dontShowHintsAgain = (localStorage.getItem('doNotShowHintsAgain') != undefined && localStorage.getItem('doNotShowHintsAgain') != null && localStorage.getItem('doNotShowHintsAgain') != "" && localStorage.getItem('doNotShowHintsAgain') != "false") ? true : false;
//******************** Page Load Events Start **************************

$('#_loadStoreData').live('pagebeforecreate', function (event) {
    displayMessage('_loadStoreData');
    pageObj = new manageUploadLists();
    pageObj.displayListFileInfo();
    pageObj.displayValidationMessage('_loadStoreData');
    //confirmMessage();
    createConfirmMessage('_loadStoreData');

    //load IFRAME for IE to upload files.
    loadUploadIFrame();
    //test for mobility...
    //loadMobility();
    sessionStorage.removeItem('filesToUpload');
});

$(document).keyup(function (e) {
    if (e.keyCode == 27) {
        return false;
    }
});

$(window).on('popupbeforeposition', 'div:jqmData(role="popup")', function () {
    var notDismissible = $(this).jqmData('dismissible') === false;
    if (notDismissible) {
        $('.ui-popup-screen').off();
    }
});

$(document).on('pageshow', '#_loadStoreData', function (event) {

    window.setTimeout(function () {
        ko.applyBindings(pageObj);
    }, 1000);

    (($.browser.msie) ? $(window.frames[0].frames.frameElement).contents().find('input[type=file]') : $('input[type=file]')).bind('change', function (e) {
        pageObj.navigate(e, $(e.target))
    });
    $('#spnBtnText').html('"Upload"');
    $('#okBut').attr('onclick', 'pageObj.closeDialog();');

    //Setting Display Environment
    if (window.location.host.toString().indexOf('tddev') > -1 || window.location.host.toString().indexOf('192.168') > -1) {
        $('#spnEnvironment').html('Development');
    }
    else if (window.location.host.toString().indexOf('tddemo') > -1) {
        $('#spnEnvironment').html('Demo');
    }
    else if (window.location.host.toString().indexOf('tdlive') > -1) {
        $('#spnEnvironment').html('Live');
    }

    $('#sldrShowHints').slider("refresh");
    $.each($('input[type=checkbox]'), function (a, b) {
        $(this).checkboxradio();
    });
    $.each($('fieldset[data-role="controlgroup"]'), function (a, b) {
        $(this).controlgroup();
    });
});


function showHomePage(type) {
    if (type == "home") {
        window.location.href = "../index.htm";
    }
    else {
        window.location.href = " ../loadStoreData/loadStoreData.html";
    }
}

//Checks the given string ends with a specific (given character).
String.prototype.endsWith = function (suffix) {
    return this.indexOf(suffix, this.length - suffix.length) !== -1;
};
//******************** Page Load Events End **************************
