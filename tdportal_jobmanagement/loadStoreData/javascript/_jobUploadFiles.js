﻿var uploadFilesCommon = function (self) {
    //******************** Global Variables Start **************************
    self.percent = 0;
    self.gFileUploadServiceUrl = '';
    self.gData;
    self.gOutputServiceUrl = '../JSON/_jobSetupOutput.JSON';
    self.gStateServiceUrl = '../JSON/_listStates.JSON';
    self.storeDataList = [];
    self.gOutputData;
    self.uploadingFiles = "";
    self.artApprovalTypes = "";
    self.customerNumber = appPrivileges.customerNumber;
    self.jobNumber = "";
    self.facilityId = "";
    self.maxNumberOfFiles = 10; // 5;
    self.jobNumber = getSessionData("jobNumber");
    self.jobNumber = window.location.href.toLocaleLowerCase().indexOf('tddev') > -1 ? '-18172' : "-18099";
    self.facilityId = sessionStorage.facilityId;
    self.fileCount = 0;
    self.form_xhr;
    self.selectedKey = '';
    self.isComplete = false;
    self.xhr_req;
    self.is_aborted = false;
    self.percent_uploaded = 0;

    //var gFileRemoveURL = serviceURLDomain + "api/FileUpload/delete/" + facilityId + "/" + jobNumber + "/";
    //******************** Global Variables End **************************

    //******************** Public Functions Start **************************
    //Gets the uploaded files list from Job Ticket JSON and displays over the page.
    self.buildOutputLists = function () {
        var file_data;
        file_data = (self.storeDataList != undefined) ? self.storeDataList : [];

        if (file_data.length > 0) {
            self.makeFileList(file_data);
        }
        sessionStorage.storeDataList = JSON.stringify(self.storeDataList);
    };

    //Initiatest the upload file process
    self.uploadFiles = function (e) {
        $('#btnUploadFiles').attr('disabled', 'disabled');
        $('#divError').text('');
        var file_names = self.uploadingFiles.split('|');
        if (file_names.length == 1) file_names[0] = self.uploadingFiles;
        var auth_file_names = "";

        var edit_file_operation = ($('#hidEditFileOperation') != undefined && $('#hidEditFileOperation') != null && $('#hidEditFileOperation').val() != "") ? $('#hidEditFileOperation').val() : '';

        $.each(file_names, function (a, b) {
            auth_file_names += (auth_file_names != "") ? '^' + b.substring(0, b.lastIndexOf('^')) : b.substring(0, b.lastIndexOf('^'));
        });
        var message = '';
        var authorise_file_url = "api/FileUpload/";
        //authorise_file_url = (edit_file_operation != undefined && edit_file_operation != null && edit_file_operation != "") ? authorise_file_url + 'markReplaceArtworkFileName/' + auth_file_names + "/" + edit_file_operation.split('|')[1] + "/" + edit_file_operation.split('|')[2] : authorise_file_url + 'markFileNames/' + auth_file_names;

        var art_upload_authorise_file_url = "api/FileUpload/";

        if (window.location.href.toString().indexOf('jobUploadArtwork') > -1) {
            //if (appPrivileges.roleName == "admin" && $('#ddlApprovalType').val() != "select") // to be discussed and updated accordingly.
            if (edit_file_operation != undefined && edit_file_operation != null && edit_file_operation != "")
                authorise_file_url = authorise_file_url + 'markReplaceArtworkFileName/' + auth_file_names + "/" + edit_file_operation.split('|')[1] + "/" + edit_file_operation.split('|')[2]
            else if ($('#ddlApprovalType').val() != "select") // to be discussed and updated accordingly.
                authorise_file_url = authorise_file_url + 'markProofFileNames/' + auth_file_names + '/' + self.artApprovalTypes;
            else
                authorise_file_url = authorise_file_url + 'markFileNames/' + auth_file_names;
        }
        else {
            authorise_file_url = authorise_file_url + 'markFileNames/' + auth_file_names;
        }

        //getCORS(serviceURLDomain + "api/FileUpload/markFileNames/" + auth_file_names, null, function (response) {
        getCORS(serviceURLDomain + authorise_file_url, null, function (response) {
            self.formSubmit(e);
        }, function (response_error) {
            showErrorResponseText(response_error, true);
        });
    };

    //Removes the close button from the pop up.
    self.removeCloseButton = function () {
        $('#btnClose').remove();
        $('#okBut').show();
        $('#popupDialog').popup('close');
    };

    //Uploading file starts and displays the progress bar.
    self.formSubmit = function (e) {
        if (isDragDrop || isMultiple) {
            number_of_files_uploading = (typeof (data) != 'undefined' && data != undefined && data != null && data.length > 0) ? data.length : self.uploadingFiles.split('|').length; //$("#drop-area ul li").length;
        } else {
            var input_file_list = $(($.browser.msie) ? $(window.frames[0].document.getElementById('formUpload')) : $('#formUpload')).find('input[type="file"]');
            //var input_file_list = $('#formUpload').find('input[type="file"]');
            var uploading_files = jQuery.grep(input_file_list, function (obj) {
                return obj.value != "";
            });
            number_of_files_uploading = (uploading_files != undefined && uploading_files.length > 0) ? uploading_files.length : 0;
        }
        self.isComplete = false;
        var start = '';
        e.preventDefault();
        var timer_id = 0;
        var time_stamp = Math.floor(+new Date().getTime() / 1000);
        var increment = 1;
        var timer = (function () {
            var timerElement;
            var timeoutRef;
            var percent_uploaded = 0;
            return {
                start: function (id) {
                    if (id) {
                        timerElement = document.getElementById(id);
                    }
                    timer.run();
                },
                run: function () {
                    if (timerElement) {
                        $('#dvProgressBar').popup('open', { positionTo: 'window' });
                        $('#fileName').text(number_of_files_uploading);
                        //Sets the style of progress bar and displays the percent uploaded
                        $('#uploadProgressBar').css('width', percent_uploaded + '%');
                        $('#percentUploaded').empty();
                        $('#percentUploaded').get(0).innerHTML = percent_uploaded + '%';
                    }

                    if (!self.isComplete) {
                        timeoutRef = setTimeout(timer.run, 500); //This run function will be called for every 2 secs.

                        //Web service call for progress bar data
                        var pbar_data_url = serviceURLDomain + "api/FileUpload/progress/" + self.jobNumber + "^" + sessionStorage.username + "^" + time_stamp + "^" + self.facilityId;

                        getCORS(pbar_data_url, null, function (data) {
                            if (typeof (data) != "object" && parseInt(data) != NaN) {
                                if (percent_uploaded < 100)
                                    percent_uploaded = JSON.stringify(data); //assigns the values returned from web service.
                            }
                            else {
                                showErrorResponseText(data, true);
                            }
                        }, function (error_response) {
                            showErrorResponseText(error_response, true);
                        });
                    }
                    else {
                        window.clearTimeout(timeoutRef);
                        $('#dvProgressBar').popup('close');
                        if (!self.is_aborted) {
                            if (window.location.href.toLowerCase().indexOf('summary') == -1 && window.location.href.toLowerCase().indexOf('logoselection') == -1 && window.location.href.toLowerCase().indexOf('imagelibrary') == -1) {
                                self.updateFileGrid();
                                self.buildOutputLists();
                                data = [];
                            }
                            formData = null;
                            isMultiple = false;
                            isDragDrop = false;
                        }
                        self.is_aborted = false;
                        return true;
                    }
                }
            }
        }());
        if (('draggable' in document.createElement('span')) && ((window.location.href.toLowerCase().indexOf('summary') > -1 || window.location.href.toLowerCase().indexOf('logoselection') > -1 || window.location.href.toLowerCase().indexOf('imagelibrary') > -1) || isDragDrop || isMultiple)) {
            formData = new FormData();
            var files_count = (window.location.href.toLowerCase().indexOf('summary') > -1 || window.location.href.toLowerCase().indexOf('logoselection') > -1 || window.location.href.toLowerCase().indexOf('imagelibrary') > -1) ? self.uploadingFiles.split('|').length : data.length;
            var files_list = (window.location.href.toLowerCase().indexOf('summary') == -1 && window.location.href.toLowerCase().indexOf('logoselection') == -1 && window.location.href.toLowerCase().indexOf('imagelibrary') == -1) ? data : [];
            if (window.location.href.toLowerCase().indexOf('summary') > -1 || window.location.href.toLowerCase().indexOf('logoselection') > -1 || window.location.href.toLowerCase().indexOf('imagelibrary') > -1) {
                var temp_files_list = [];
                files_list = [];
                $.each(self.fileToDropBox(), function (key, val) {
                    $.each(val.filesList(), function (key1, val1) {
                        files_list.push(val1.fileObj);
                    });

                });
            }
            for (var i = 0; i < files_count; i++) {
                var file_info = (window.location.href.toLowerCase().indexOf('summary') > -1 || window.location.href.toLowerCase().indexOf('logoselection') > -1 || window.location.href.toLowerCase().indexOf('imagelibrary') > -1) ? files_list[i] : files_list[i].file;
                formData.append("file" + i, file_info);
            }
            $.ajax({
                url: serviceURLDomain + 'api/FileUpload/' + self.jobNumber + '^' + sessionStorage.username + '^' + time_stamp + '^' + self.facilityId + '/' + self.uploadingFiles,
                type: 'POST',
                xhr: function () {  // Custom XMLHttpRequest
                    var myXhr = $.ajaxSettings.xhr();
                    //                    if (myXhr.upload) { // Check if upload property exists
                    //                        myXhr.upload.addEventListener('progress', progressHandlingFunction, false); // For handling the progress of the upload
                    //                    }
                    return myXhr;
                },
                //Ajax events
                beforeSend: function (xhr) {
                    start = new Date().getTime();
                    self.form_xhr = xhr;
                    if (window.location.href.toLowerCase().indexOf('summary') > -1 || window.location.href.toLowerCase().indexOf('logoselection') > -1 || window.location.href.toLowerCase().indexOf('imagelibrary') > -1)
                        $('#popupSetTaskStatus').popup('close');
                    $('#popupListDefinition').popup('close');
                    $('#dvProgressBar').popup('open', { positionTo: 'window' });
                    timer.start('uploadProgressBar'); //Starts the timer just before the upload starts
                    xhr.setRequestHeader("Authorization", sessionStorage.authString);
                },
                success: function (xhr) {
                    self.isComplete = true;
                    if ($.browser.msie) {
                        var status_url = serviceURLDomain + "api/FileUpload/error/" + self.jobNumber + "^" + sessionStorage.username + "^" + time_stamp + "^" + self.facilityId;
                        getCORS(status_url, null, function (data) {
                            var status = data;
                            if (typeof (status) != "object" && status != undefined && status != null && status != "\"\"") {
                                self.is_aborted = true;
                                $('#dvProgressBar').popup('close');
                                $('#alertmsg').text(status);
                                if (window.location.href.toLowerCase().indexOf('summary') > -1 || window.location.href.toLowerCase().indexOf('logoselection') > -1 || window.location.href.toLowerCase().indexOf('imagelibrary') > -1) {
                                    $('#okBut').bind('click', function () {
                                        $('#okBut').unbind('click');
                                        $('#popupDialog').popup('close');
                                        $('#okBut').attr('onclick', '$(\'#popupDialog\').popup(\'close\');');
                                        window.setTimeout(function setPopups() {
                                            $('#popupSetTaskStatus').popup('open');
                                        }, 400);

                                        $("#dvUploadedFilesList div ul").each(function (i) {
                                            $(this).listview();
                                        });
                                        $("#dvUploadedFilesList").collapsibleset('refresh');
                                    });
                                }
                                windows.setTimeout(function showMsg() {
                                    $('#popupDialog').popup('open');
                                }, 200);
                                $('#btnUploadFiles').removeAttr('disabled');
                                //$('#divError').html(status);
                                //$('#divError').css('display', 'block');
                            } else if (typeof (status) != "object" && status == "\"\"" && !self.is_aborted) {
                                self.uploadingFiles = "";
                                self.artApprovalTypes = "";
                            }
                            else {
                                self.is_aborted = true;
                                $('#dvProgressBar').popup('close');
                                showErrorResponseText(status, true);
                            }
                        }, function (error_response) {
                            self.is_aborted = true;
                            //showErrorResponseText(error_response);
                            //if (error_response.responseText != undefined && error_response.responseText != null && error_response.responseText != "") {
                            $('#dvProgressBar').popup('close');
                            showErrorResponseText(error_response, false);
                            //if (error_response.status == 500 || error_response.status == 417) {
                            //    $('#alertmsg').text("Unknown Error: Contact a Site Administrator");
                            //}
                            //else {
                            //    $('#alertmsg').text(error_response.responseText);
                            //}
                            //$('#okBut').attr('onclick', "$('#popupDialog').popup('open');$('#popupDialog').popup('close');");
                            //$('#popupDialog').popup('open');
                            $('#btnUploadFiles').removeAttr('disabled');
                            //    }
                        });
                    }
                    else {
                        if (xhr.toLowerCase().indexOf('success') > -1 || xhr.toLowerCase() == "ok" || xhr.toLowerCase() == "n/a") {
                            if (window.location.href.toLowerCase().indexOf('summary') > -1 || window.location.href.toLowerCase().indexOf('logoselection') > -1 || window.location.href.toLowerCase().indexOf('imagelibrary') > -1) {
                                var msg = "Your files have been successfully uploaded and saved to this task.";
                                $('#alertmsg').text(msg);
                                $('#dvProgressBar').popup('close');
                                $('#popupSetTaskStatus').popup('close');
                                $('#okBut').bind('click', function () {
                                    $('#okBut').unbind('click');
                                    $('#popupDialog').popup('close');
                                    $('#okBut').attr('onclick', '$(\'#popupDialog\').popup(\'close\');');
                                    if (window.location.href.toLowerCase().indexOf('summary') > -1) {
                                        window.setTimeout(function setPopups() {
                                            $('#popupSetTaskStatus').popup('open');
                                        }, 400);
                                    }

                                    $("#dvUploadedFilesList div ul").each(function (i) {
                                        $(this).listview();
                                    });
                                    $("#dvUploadedFilesList").collapsibleset('refresh');
                                });
                                window.setTimeout(function setPopups() {
                                    $('#popupDialog').popup('open');
                                }, 200);
                            }
                            self.uploadingFiles = "";
                            self.artApprovalTypes = "";
                            isDragDrop = false;
                            isMultiple = false;
                            data = [];
                        }
                    }
                    $('#dvProgressBar').popup('close');
                },
                error: function (error_response) {
                    if (!($.browser.msie)) {
                        self.isComplete = true;
                        self.is_aborted = true;
                        //if (error_response.responseText != undefined && error_response.responseText != null && error_response.responseText != "") {
                        $('#dvProgressBar').popup('close');
                        showErrorResponseText(error_response, false);
                        //if (error_response.status == 500 || error_response.status == 417) {
                        //    $('#alertmsg').text("Unknown Error: Contact a Site Administrator");
                        //}
                        //else {
                        //    $('#alertmsg').text(error_response.responseText);
                        //}
                        //$('#okBut').attr('onclick', "$('#popupDialog').popup('open');$('#popupDialog').popup('close');");
                        //$('#popupDialog').popup('open');
                        $('#btnUploadFiles').removeAttr('disabled');
                        //}
                        //showErrorResponseText(error_response);
                        if (('draggable' in document.createElement('span'))) {
                            self.filesToUpload = [];
                            self.artFilesToUpload = [];
                            data = [];
                            self.uploadingFiles = "";
                            self.artApprovalTypes = "";
                        }
                    }
                    else {
                        self.is_aborted = true;
                        self.isComplete = true;
                        $('#dvProgressBar').popup('close');
                        showErrorResponseText(error_response, false);
                    }
                },
                // Form data
                data: formData,
                //Options to tell jQuery not to process data or worry about content-type.
                cache: false,
                contentType: false,
                processData: false
            });
        }
        else {
            //Submitting the form to upload
            var form_obj = ($.browser.msie) ? $(window.frames[0].document.getElementById('formUpload')) : $('#formUpload');
            $(form_obj).attr('action', serviceURLDomain + 'api/FileUpload/' + self.jobNumber + '^' + sessionStorage.username + '^' + time_stamp + '^' + self.facilityId + '/' + self.uploadingFiles);
            var form = $(form_obj).ajaxSubmit(
                                        {
                                            type: 'post',
                                            beforeSend: function (xhr) {
                                                start = new Date().getTime();
                                                self.form_xhr = xhr;
                                                if (window.location.href.toLowerCase().indexOf('summary') > -1 || window.location.href.toLowerCase().indexOf('logoselection') > -1 || window.location.href.toLowerCase().indexOf('imagelibrary') > -1)
                                                    $('#popupSetTaskStatus').popup('close');
                                                $('#popupListDefinition').popup('close');
                                                $('#dvProgressBar').popup('open', { positionTo: 'window' });
                                                timer.start('uploadProgressBar'); //Starts the timer just before the upload starts
                                                xhr.setRequestHeader("Authorization", sessionStorage.authString);
                                            },
                                            complete: function (xhr) {
                                                self.isComplete = true;
                                                if ($.browser.msie) {
                                                    var status_url = serviceURLDomain + "api/FileUpload/error/" + self.jobNumber + "^" + sessionStorage.username + "^" + time_stamp + "^" + self.facilityId;
                                                    getCORS(status_url, null, function (data) {
                                                        var status = data;
                                                        if (typeof (status) != "object" && status != undefined && status != null && status != "\"\"") {
                                                            self.is_aborted = true;
                                                            $('#dvProgressBar').popup('close');
                                                            $('#alertmsg').text(status);
                                                            if (window.location.href.toLowerCase().indexOf('summary') > -1 || window.location.href.toLowerCase().indexOf('logoselection') > -1 || window.location.href.toLowerCase().indexOf('imagelibrary') > -1) {
                                                                $('#okBut').bind('click', function () {
                                                                    $('#okBut').unbind('click');
                                                                    $('#popupDialog').popup('close');
                                                                    $('#okBut').attr('onclick', '$(\'#popupDialog\').popup(\'close\');');
                                                                    window.setTimeout(function setPopups() {
                                                                        $('#popupSetTaskStatus').popup('open');
                                                                    }, 400);

                                                                    $("#dvUploadedFilesList div ul").each(function (i) {
                                                                        $(this).listview();
                                                                    });
                                                                    $("#dvUploadedFilesList").collapsibleset('refresh');
                                                                });
                                                            }
                                                            $('#popupDialog').popup('open');
                                                            $('#btnUploadFiles').removeAttr('disabled');
                                                            //$('#divError').html(status);
                                                            //$('#divError').css('display', 'block');
                                                        } else if (typeof (status) != "object" && status == "\"\"" && !self.is_aborted) {
                                                            self.uploadingFiles = "";
                                                            self.artApprovalTypes = "";
                                                            if (window.location.href.toLowerCase().indexOf('summary') > -1 || window.location.href.toLowerCase().indexOf('logoselection') > -1 || window.location.href.toLowerCase().indexOf('imagelibrary') > -1) {
                                                                $('#popupSetTaskStatus').popup('close');
                                                                $('#alertmsg').text("Your files have been successfully uploaded and saved.");
                                                                $('#okBut').bind('click', function () {
                                                                    $('#okBut').unbind('click');
                                                                    $('#popupDialog').popup('close');
                                                                    $('#okBut').attr('onclick', '$(\'#popupDialog\').popup(\'close\');');
                                                                    window.setTimeout(function setPopups() {
                                                                        $('#popupSetTaskStatus').popup('open');
                                                                    }, 400);

                                                                    $("#dvUploadedFilesList div ul").each(function (i) {
                                                                        $(this).listview();
                                                                    });
                                                                    $("#dvUploadedFilesList").collapsibleset('refresh');
                                                                });
                                                                window.setTimeout(function setPopups() {
                                                                    $('#popupDialog').popup('open');
                                                                }, 200);
                                                            }
                                                        }
                                                        else {
                                                            self.is_aborted = true;
                                                            $('#dvProgressBar').popup('close');
                                                            showErrorResponseText(status, true);
                                                        }
                                                    }, function (error_response) {
                                                        self.is_aborted = true;
                                                        //showErrorResponseText(error_response);
                                                        //if (error_response.responseText != undefined && error_response.responseText != null && error_response.responseText != "") {
                                                        $('#dvProgressBar').popup('close');
                                                        showErrorResponseText(error_response, false);
                                                        //if (error_response.status == 500 || error_response.status == 417) {
                                                        //    $('#alertmsg').text("Unknown Error: Contact a Site Administrator");
                                                        //}
                                                        //else {
                                                        //    $('#alertmsg').text(error_response.responseText);
                                                        //}
                                                        //$('#okBut').attr('onclick', "$('#popupDialog').popup('open');$('#popupDialog').popup('close');");
                                                        //$('#popupDialog').popup('open');
                                                        $('#btnUploadFiles').removeAttr('disabled');
                                                        // }
                                                    });
                                                }
                                                else {
                                                    if (xhr.statusText.toLowerCase().indexOf('success') > -1 || xhr.statusText.toLowerCase() == "ok" || xhr.statusText.toLowerCase() == "n/a") {
                                                        self.uploadingFiles = "";
                                                        self.artApprovalTypes = "";
                                                    }
                                                }
                                                $('#dvProgressBar').popup('close');
                                            },
                                            error: function (error_response) {
                                                if (!($.browser.msie)) {
                                                    self.isComplete = true;
                                                    self.is_aborted = true;
                                                    //if (error_response.responseText != undefined && error_response.responseText != null && error_response.responseText != "") {
                                                    $('#dvProgressBar').popup('close');
                                                    showErrorResponseText(error_response, false);
                                                    //if (error_response.status == 500 || error_response.status == 417) {
                                                    //    $('#alertmsg').text("Unknown Error: Contact a Site Administrator");
                                                    //}
                                                    //else {
                                                    //    $('#alertmsg').text(error_response.responseText);
                                                    //}
                                                    //$('#okBut').attr('onclick', "$('#popupDialog').popup('open');$('#popupDialog').popup('close');");
                                                    //$('#popupDialog').popup('open');
                                                    $('#btnUploadFiles').removeAttr('disabled');
                                                    // }
                                                    //showErrorResponseText(error_response);
                                                }
                                                else {
                                                    $('#dvProgressBar').popup('close');
                                                }
                                            }
                                        });
        }
    };

    //Cancels the file upload.
    self.cancelUpload = function () {
        self.is_aborted = true;
        self.form_xhr.abort();
        if (!('draggable' in document.createElement('span'))) {
            $('#btnUploadFiles').removeAttr('disabled');
        }
        //else {
        //    self.popUpListDefinitionDisplay();
        //}
    };

    //Displays the file map name.
    self.displayFileMapName = function (ctrl) {
        var file_name = $(ctrl).val();
        var extension = file_name.split('.');
        if (extension.length < 2) {
            file_name = file_name + "_map.xml";
        }
        if (typeof (extension) != 'undefined' && extension.length > 1 && extension[1].indexOf('xml') != -1 && extension[0].indexOf('_map') == -1) {

            file_name = extension[0] + "_map." + extension[1];
        }
        $('#txtFileMap').val(file_name);
    };
    //Show/Hide the deDupe slider.
    self.deDupeOnOff = function (ctrlSliderDeDupe) {
        var isFileMapOn = $('#' + ctrlSliderDeDupe).val();
        if (isFileMapOn == 'off') {
            $('#dvDeDupe').show();
        }
        else {
            $('#dvDeDupe').hide();
        }
    };
    //Shows/Hides file map
    self.fileMapOnOff = function (ctrlSliderFileMap) {
        var isFileMapOn = $('#' + ctrlSliderFileMap).val();
        if (isFileMapOn == 'off') {
            $('#dvFileMap').show();
        }
        else {
            $('#dvFileMap').hide();
        }
    };

    //updates the upload file list
    self.updateUploadList = function () {
        var job_setup_json = jQuery.parseJSON(sessionStorage.jobSetupOutput);
        var template = (job_setup_json.template != undefined) ? job_setup_json.template : job_setup_json.templateName;
        if (template == "") {
            $('#alertmsg').text('Please select a Template');
            $("#popupDialog").popup('open');
            return false;
        }
        self.bindData();
        return;
    };

    //Adds File upload control dynamically into the page.
    self.addFileInput = function () {
        if (self.fileCount >= 1) {
            var tdFileInputsTemp = ($.browser.msie) ? window.frames[0].document.getElementById('spnFileInput') : document.getElementById('spnFileInput'); // document.getElementById('spnFileInput');
            var number_of_files_selected = $(tdFileInputsTemp).find('input[type=file]').length;
            var fileInput = $(tdFileInputsTemp).find('input[type=file]')[number_of_files_selected - 1];
            if (fileInput.value != "") {
                // Create a new file input
                var newFileInput = fileInput.cloneNode(true);
                newFileInput.value = null;
                var id = fileInput.id.replace('[]', self.fileCount);
                id = id + '[]';
                fileInput.id = id;
                fileInput.name = id;
                newFileInput.id = "mailFiles[]";
                newFileInput.name = "mailFiles[]";
                $(tdFileInputsTemp).find('input[type=file]')[number_of_files_selected - 1].style.display = "none";
                tdFileInputsTemp.appendChild(newFileInput);
                self.resetFileUploadPopup();
                self.fileCount = self.fileCount + 1;
                (($.browser.msie) ? $(window.frames[0].frames.frameElement).contents().find('input[type=file]') : $('input[type=file]')).bind('change', function (e) {
                    self.navigate(e, $(e.target));
                });
            }
        }
        if (self.fileCount == 0) {
            self.fileCount = self.fileCount + 1;
        }
    };
    //Displays upload file selection popup.
    self.popUpListDefinitionDisplay = function () {
        $('#dvPopupListDefinitionContent').css('display', 'block');
        $('#popupListDefinition').popup('open', { positionTo: 'window' });
    };
 
    //Displays validation report popup.
    self.fnOpenPopup = function () {
        $('#validationPopupDialog').popup('open');
        $("#validationPopupDialog").popup('close')
        //$("#popupListDefinition").popup("open")
        self.popUpListDefinitionDisplay();
    };
    //Resets the File upload control properties.
    self.resetFileUploadControl = function (file_list) {
        if (!isMultiple && !isDragDrop) {
            var tdFileInputsTemp = ($.browser.msie) ? window.frames[0].document.getElementById('formUpload') : document.getElementById('formUpload');
            var number_of_files_selected = $(tdFileInputsTemp).find('input[type=file]').length;
            if (file_list.length == 0) {
                $("#divFilesToUpload").hide();
                //var tdFileInputsTemp = document.getElementById('formUpload');
                var number_of_files_selected = $(tdFileInputsTemp).find('input[type=file]').length;
                var fileInput = $(tdFileInputsTemp).find('input[type=file]')[number_of_files_selected - 1];
                // Create a new file input
                var newFileInput = fileInput.cloneNode(true);
                newFileInput.value = null;
                var id = "mailFiles[]";
                newFileInput.id = id // A unique id
                newFileInput.name = newFileInput.id;
                newFileInput.style.display = "";
                $($(tdFileInputsTemp).find('input[type=file]')[number_of_files_selected - 1]).remove();
                var spnFileInputs = ($.browser.msie) ? window.frames[0].document.getElementById('spnFileInput') : document.getElementById('spnFileInput');
                spnFileInputs.appendChild(newFileInput);
                if (!isMultiple && !isDragDrop)
                    self.resetFileUploadPopup();
                (($.browser.msie) ? $(window.frames[0].frames.frameElement).contents().find('input[type=file]') : $('input[type=file]')).bind('change', function (e) {
                    self.navigate(e, $(e.target));
                });
                self.fileCount = 0;
            }
            else {
                //remove the file control from the form.
                var upload_form = ($.browser.msie) ? window.frames[0].document.getElementById('formUpload') : document.getElementById('formUpload');
                if ($(tdFileInputsTemp).find('input[type=file]').length > 1)
                    $($(tdFileInputsTemp).find('input[type=file]')[number_of_files_selected - 1]).remove();
                else {
                    var fileInput = $(tdFileInputsTemp).find('input[type=file]')[number_of_files_selected - 1];
                    if (fileInput.value != "") {
                        var newFileInput = fileInput.cloneNode(true);
                        newFileInput.value = null;
                        newFileInput.id = "mailFiles[]";
                        newFileInput.name = "mailFiles[]";
                        $(tdFileInputsTemp).find('input[type=file]').remove();
                        var spnFileInputs = ($.browser.msie) ? window.frames[0].document.getElementById('spnFileInput') : document.getElementById('spnFileInput');
                        spnFileInputs.appendChild(newFileInput);
                        (($.browser.msie) ? $(window.frames[0].frames.frameElement).contents().find('input[type=file]') : $('input[type=file]')).bind('change', function (e) {
                            self.navigate(e, $(e.target));
                        });
                    }
                }
                number_of_files_selected = $(tdFileInputsTemp).find('input[type=file]').length;
            }
        }
    };
    //Added the Validation Message popup to UI.
    self.displayValidationMessage = function (page_name) {
        var msg_box = '';
        msg_box = '<div data-role="popup" id="validationPopupDialog" data-overlay-theme="d" data-theme="c" style="max-width:400px;" class="ui-corner-all" data-history="false">';
        msg_box += '<div data-role="header" data-theme="d" class="ui-corner-top" id="dialogbox">';
        msg_box += '<h1>Alert</h1>';
        msg_box += '</div>';
        msg_box += '<div data-role="content" data-theme="d" class="ui-corner-bottom ui-content">';
        msg_box += '<div id="validationAlertmsg"></div>';
        msg_box += '<a href="#" data-role="button" data-inline="true" data-theme="a" id="btnOk" onclick="pageObj.fnOpenPopup();">Ok</a>';
        msg_box += '</div></div>';
        $("#" + page_name).append(msg_box);
    }
    //******************** Public Functions End **************************
};