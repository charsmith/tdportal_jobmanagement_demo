﻿$.mobile.popup.prototype.options.history = false;
jQuery.support.cors = true;

//******************** Data URL Variables Start **************************
var pageObj;
var gCompanyInfoServiceUrl = "../adminTemplateSetup/JSON/_companyInfoConfig.JSON";
var gServiceCompanyData;
var pageObj;
var companyData;
var primaryFacilityData;
//******************** Data URL Variables End **************************

//******************** Page Load Events Start **************************
$('#_variableCompanyInfo').live('pagebeforecreate', function (event) {
    ko.setTemplateEngine(new ko.nativeTemplateEngine);
    displayMessage('_variableCompanyInfo');
    createConfirmMessage("_variableCompanyInfo");
    getCompanyInfo();
    $("#navLinksPanel").on("panelopen", function (event, ui) {
        sessionStorage.isNavPanelOpen = true;
    });
});

$(document).on('pageshow', '#_variableCompanyInfo', function (event) {
    window.setTimeout(function () {
        pageObj = new loadCompanyInfo();
        ko.applyBindings(pageObj);
        //window.setTimeout(function () {
        $('input[type=checkbox]').trigger('create');//.checkboxradio();
        $('div').trigger('create');
        //}, 500);

    }, 500);
});

var getCompanyInfo = function () {
    //$.getJSON(gCompanyInfoServiceUrl, function (data) {
    //    gServiceCompanyData = data;
    //});
    if (sessionStorage.templateSetupConfigJSON != undefined && sessionStorage.templateSetupConfigJSON != null && sessionStorage.templateSetupConfigJSON != "") {
        gServiceCompanyData = $.parseJSON(sessionStorage.templateSetupConfigJSON);
    }
};

var fieldInfo = function (label, value, is_required, show_chkbox) {
    var self = this;
    self.label = ko.observable('');
    self.label(label);
    self.value = ko.observable('');
    self.value(value);
    self.isRequired = ko.observable('');
    self.isRequired(is_required);
    self.showCheckBox = ko.observable('');
    self.showCheckBox(show_chkbox);
};

var loadCompanyInfo = function () {
    var self = this;
    self.varCompanyInfo = ko.observable({});
    //ko.mapping.fromJS(gServiceCompanyData, {}, self.companyInfo);
    var temp_arr = [];
    var temp_arr1 = ko.observableArray([]);
    var temp_key = "";
    var index = 0;
    var temp_company_info = {};
    var show_chk = false;

    $.each(gServiceCompanyData, function (key, val) {
        if (temp_key == "")
            temp_key = key;
        if (temp_key != key) {
            temp_company_info[temp_key] = temp_arr;
            //temp_company_info[temp_key] = temp_arr1;
            temp_key = key;
            //self.companyInfo.push(temp);
            temp_arr = [];
            //temp_arr1([]);
        }
        if (temp_key == "companyInfo")
            show_chk = true;
        else
            show_chk = false;
        $.each(val, function (key1, val1) {
            //ko.mapping.fromJS(val, [], temp_arr);
            temp_arr.push(new fieldInfo(val1.label, val1.value, val1.isRequired, show_chk));
            //temp_arr1.push(new fieldInfo(val1.label, val1.value, val1.isRequired, show_chk));
        });
        if (index == (Object.keys(gServiceCompanyData).length - 1)) {
            temp_company_info[temp_key] = temp_arr;
            //temp_company_info[temp_key] = temp_arr1;
            temp_key = key;
            //self.companyInfo.push(temp);
            temp_arr = [];
            //temp_arr1([]);
        };
        index++;
    });
    if (Object.keys(temp_company_info).length > 0) {
        self.varCompanyInfo(temp_company_info);
        temp_company_info = {};
    }

    self.showOrHideField = function (data, event) {
        var ele = event.targetElement || event.target;
        data.isRequired($(ele).is(':checked'));
        $('input[type=checkbox]').checkboxradio('refresh');
    };
    self.addCustomFieldClick = function (data, event) {
        $('#popupNewCustomField').popup('open');
    };
    self.addCustomField = function (data, event) {
        var label_name = $('#txtLabel').val();
        var temp_field = {
            "label": label_name,
            "value": "",
            "isRequired": "true"
        };
        var my_cust_field = {};
        ko.mapping.fromJS(temp_field, {}, my_cust_field);
        //$.each(self.varCompanyInfo(), function (key, val) {

        //    if (key == "customInfo") {
        //        val.push(my_cust_field);
        //        return false;
        //    }
        //});
        data.varCompanyInfo().customInfo.push(new fieldInfo(temp_field.label, temp_field.value, temp_field.isRequired, "true"));

        $('#popupNewCustomField').popup('close');
    };
};

function showHomePage(type) {
    if (type == "home") {
        window.location.href = "../index.htm";
    }
    else if ((type == "template")) {
        window.location.href = " ../adminTemplateSetup/templateSetup.html";
    }
    else if ((type == "vcc")) {
        window.location.href = " ../adminTemplateSetup/variableCompanyInfo.html";
    }
};

