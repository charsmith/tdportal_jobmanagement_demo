﻿var customers = [], facilities = [], platform = [], servers = [], statuses = [], milestones = [], job_type = [], quantity = [], IS = [], production = [], flags = [], finance = [];
var acct_manager = [], channel = [], sae = [], client = []; // values from service 'api/JobSearch_defaults'
var jobsList = [];
var mapping = { 'ignore': ["__ko_mapping__"] }

$('#_jobManagement').live('pagebeforecreate', function (event) {
    loadingImg("_jobManagement");
    displayMessage('_jobManagement');    
    ko.setTemplateEngine(new ko.nativeTemplateEngine);

    // LOADS CUSTOMERS
    getCORS(serviceURLDomain + "api/Customer/", null, function (data) {
        var temp = [];
        $.each(data, function (key, val) {
            temp.push({
                "name": val.customerName,
                "value": val.customerNumber
            });
        });
        customers = temp;
    }, function (error_response) {
        showErrorResponseText(error_response, true);
    });
    $.getJSON("../JSON/_facilityList.JSON", function (data) {
        var temp = [];
        $.each(data.facility, function (key, val) {
            if (key < 4) {
                if (val.facilityName == "Chicago") {
                    val.facilityName = "Northlake";
                }
                temp.push({
                    "name": val.facilityName,
                    "value": val.facilityValue
                });
            }
        });
        temp.push({
            "name": 'None',
            "value": '0'
        });
        facilities = temp;
    });

    $.getJSON("JSON/_advSearchInfo.JSON", function (data) {
        platform = data.platform;
        servers = data.servers;
        statuses = data.statuses;
        job_type = data.jobtype;
        quantity = data.quantity;
        IS = data.IS;
        production = data.production;
        flags = data.flags;
        finance = data.finance;
    });

    //Load Job Search Defaults   
    if (sessionStorage.jobSearchDefaults != undefined && sessionStorage.jobSearchDefaults != null && sessionStorage.jobSearchDefaults != "") {
        var data = $.parseJSON(sessionStorage.jobSearchDefaults);
        acct_manager = data.acctMgr;
        client = data.client;
        sae = data.sae;
        channel = getSearchGroupValues(data.channel, "channel");
    }
    else {
        getCORS(serviceURLDomain + "api/JobSearch_defaults", null, function (data) {
            sessionStorage.jobSearchDefaults = JSON.stringify(data);
            acct_manager = data.acctMgr;
            client = data.client;
            sae = data.sae;
            channel = getSearchGroupValues(data.channel, "channel");
        }, function (error_response) {
            showErrorResponseText(error_response, true);
        });
    }

    function getSearchGroupValues(data, search_group) {
        var temp = [];
        $.each(data, function (key, val) {
            temp.push({
                "name": val,
                "value": val.replace(/ /g, '')
            });
        });
        return temp;
    }

    // LOADS MILESTONES
    getCORS(serviceURLDomain + "/api/NexusIndex_config/" + appPrivileges.customerNumber, null, function (data) {
        milestones = data.Milestones;
    }, function (error_response) {
        showErrorResponseText(error_response, true);
    });

    //$.getJSON("JSON/_jobMgmtJobs.JSON", function (data) {
    //    jobsList = data.jobs;
    //});

    $("#pnlSearch").on("panelbeforeclose", function (event, ui) {
        $('div[data-blocktype=advSearchButton] a').text('Search Filters');
        $('#dvSearchTerms').css('display', 'none');
    });

    if (featureLogAccountReceivables()) {
        $("#rdoLogAccountReceivables").css('display', 'block');
        $("label[for=rdoLogAccountReceivables]").css('display', 'block');
    }
});

$(document).on('pageshow', '#_jobManagement', function (event) {
    window.setTimeout(function () {
        var pageObj = new jobManagementViewModel();
        ko.applyBindings(pageObj);

        //pageObj.search();
        window.setTimeout(function () {
            $('select').selectmenu();
            $('input[type=text]').trigger('create');
            $('input[type=checkbox]').checkboxradio();
            $('ul li div[data-role=controlgroup]').controlgroup();
            $('ul li div[data-role=controlgroup]').find('input[type=text]').parent().trigger('create');
            //$('fieldset div').trigger('create');
            $('div[data-role=collapsible] ul').listview();
            $('div[data-role=collapsible]').trigger('create').collapsible();
            $('div[data-role=collapsible-set]').collapsibleset();
            $(function () {
                $("input[type=text][data-dateType*=Date]").datepicker();
            });
            $('#dvJobsList').trigger('create').css('display', 'block');
            pageObj.bindAutoComplete('txtClient');
            pageObj.bindAutoComplete('txtAccountManager');
            pageObj.bindAutoComplete('txtSales');
            pageObj.advancedSearchClick();
            pageObj.search(false);
            pageObj.getAdvSearchResults();
            pageObj.disableStaticSearchDefaults();
            $('div[data-role=collapsible]').bind('collapsibleexpand', function (event) {
                var ele = event.target || event.currentTarget || event.targetElement;
                var caption = $(ele).find('h3').text();
                caption = caption.substring(0, caption.indexOf('click') - 1);
                switch (caption) {
                    case "Account Manager":
                    case "Client":
                    case "Sales":
                        pageObj.bindShowAllList(caption);
                        break;
                    default:
                        break;
                }
            });
        }, 50);
    }, 200);
});

var jobModel = function (job_info) {
    var self = this;
    ko.mapping.fromJS(job_info, [], self);
    self.jobAttrs = ko.computed(function () {
        var attrs_string = "";
        var display_list_items = ["jobNumber", "quantity", "jobDueDate", "inHomeDate", "facilityId", "isArchived"]
        for (var key in self) {
            if (key != '__ko_mapping__' && key != 'jobName' && display_list_items.indexOf(key) > -1) {
                switch (key) {
                    case "jobNumber":
                        attrs_string += ((attrs_string != "") ? ' | ' : '') + 'Job #' + ': ' + self[key]();
                        if (self["tempJobNumber"] != undefined && self["tempJobNumber"] != null && self["tempJobNumber"]() != null && self["tempJobNumber"]() != "" && self["tempJobNumber"]() != self["jobNumber"]())
                            attrs_string += ' (' + self["tempJobNumber"]() + ')';
                        break;
                    case "jobDueDate":
                        var job_due_date = new Date(self[key]());
                        var temp_month = job_due_date.getMonth() + 1;
                        var temp_day = job_due_date.getDate();
                        var temp_date = ((temp_month.toString().length == 1) ? '0' + temp_month : temp_month) + "/" + ((temp_day.toString().length == 1) ? '0' + temp_day : temp_day) + "/" + job_due_date.getFullYear();
                        attrs_string += ((attrs_string != "") ? ' | ' : '') + 'In-Home' + ': ' + temp_date;
                        break;
                    case "isArchived":
                        attrs_string += ((attrs_string != "") ? ' | ' : '') + 'Archived' + ': ' + self[key]();
                        break;
                    case "facilityId":
                        attrs_string += ((attrs_string != "") ? ' | ' : '') + 'Facility' + ': ' + self[key]();
                        break;
                    default:
                        attrs_string += ((attrs_string != "") ? ' | ' : '') + getDisplayLabel(key).initCap() + ': ' + self[key]();
                        break;

                }
            }
        }
        return attrs_string;
    });
};

var jobManagementViewModel = function () {
    var self = this;

    //properties for selected terms
    self.definition = ko.observable({});
    var temp_definition = {
        "jobName": "",
        "jobNumber": "",
        "inHomeDateFrom": "",
        "inHomeDateTo": ""
    }
    ko.mapping.fromJS(temp_definition, {}, self.definition);
    //For Checkbox and Radio Buttons
    self.selectedCustomer = ko.observableArray();
    self.selectedCustomer.push(jobCustomerNumber);
    self.selectedFacilities = ko.observable();
    self.selectedFacilities(sessionStorage.facilityId);
    self.selectedPlatform = ko.observableArray();
    self.selectedChannels = ko.observableArray();
    self.selectedServer = ko.observableArray();
    self.selectedStatus = ko.observableArray();
    self.selectedMilestones = ko.observableArray();
    self.quantity = ko.observable({});
    //self.selectedCSR = ko.observableArray();
    self.selectedAccountMgr = ko.observableArray();
    self.selectedSales = ko.observableArray();
    self.selectedIS = ko.observableArray();
    self.selectedProduction = ko.observableArray();
    self.selectedFlags = ko.observableArray();
    self.selectedFinance = ko.observableArray();

    self.pagingData = ko.observableArray([]).extend({ rateLimit: { method: "notifyWhenChangesStop", timeout: 200 } });
    self.selectedPageValue = ko.observable();
    //var selected_value = (self.selectedPageValue() != undefined && self.selectedPageValue() != null && self.selectedPageValue() != "") ? self.selectedPageValue() : "0";
    self.selectedPageValue(0);

    self.showMoreOption = ko.observable();
    var is_more_visible = (self.pagingData().length > 0 && parseInt(self.selectedPageValue()) < self.pagingData().length) ? true : false;
    self.showMoreOption(is_more_visible);

    self.selectedClient = ko.observableArray([]);
    self.filteredAccountMgr = ko.observableArray();
    self.filteredClient = ko.observableArray();
    self.isDisplayAccountManagers = ko.observable(false);
    self.isDisplayClients = ko.observable(false);
    self.isDisplaySales = ko.observable(false);
    var temp_quantity = {
        "quantityFrom": "",
        "quantityTo": ""
    }
    ko.mapping.fromJS(temp_quantity, {}, self.quantity);

    self.accountManagers = ko.observableArray([]);

    self.selectedDefaultOptions = ko.observable({});
    var temp_default = {
        "customer": jobCustomerNumber,
        "userType": "",
        "options": []
    }
    ko.mapping.fromJS(temp_default, {}, self.selectedDefaultOptions);

    self.selectedJobType = ko.observable({});
    var temp_jobType = {
        "jobTypeID": ""
    }
    ko.mapping.fromJS(temp_jobType, {}, self.selectedJobType);

    self.jobsList = ko.observableArray([]);
    self.advanceSearchGroups = ko.observableArray([]);
    // LOADS Definition
    definition = [{
        "name": "Job Name",
        "value": ""
    },
                    {
                        "name": "Job #",
                        "value": ""
                    }, {
                        "name": "In-Home Date From",
                        "value": ""
                    }, {
                        "name": "In-Home Date To",
                        "value": ""
                    }];

    var temp_definition = { "searchGroup": "definition", "values": ko.observableArray([]) };
    ko.mapping.fromJS(definition, [], temp_definition.values);
    self.advanceSearchGroups.push(temp_definition);

    var temp_cust = { "searchGroup": "customer", "values": ko.observableArray([]) };
    ko.mapping.fromJS(customers, [], temp_cust.values);
    self.advanceSearchGroups.push(temp_cust);

    // LOADS FACILITIES
    var temp_facility = { "searchGroup": "facility", "values": ko.observableArray([]) };
    ko.mapping.fromJS(facilities, [], temp_facility.values);
    self.advanceSearchGroups.push(temp_facility);

    // LOADS SERVERS, PLATFORMS, STATUSES, IS, Production, Flags, Channel, Account Manager, Sae, Finance
    var temp_platform = { "searchGroup": "platform", "values": ko.observableArray([]) };
    ko.mapping.fromJS(platform, [], temp_platform.values);
    self.advanceSearchGroups.push(temp_platform);

    var temp_channels = { "searchGroup": "channel", "values": ko.observableArray([]) };
    ko.mapping.fromJS(channel, [], temp_channels.values);
    self.advanceSearchGroups.push(temp_channels);

    var temp_servers = { "searchGroup": "server", "values": ko.observableArray([]) };
    ko.mapping.fromJS(servers, [], temp_servers.values);
    self.advanceSearchGroups.push(temp_servers);

    var jobtype_values = [{
        "name": "jobTypeID",
        "values": job_type
    }];

    var temp_job_type = { "searchGroup": "jobtype", "values": ko.observableArray([]) };
    ko.mapping.fromJS(jobtype_values, {}, temp_job_type.values);
    self.advanceSearchGroups.push(temp_job_type);

    var temp_statuses = { "searchGroup": "status", "values": ko.observableArray([]) };
    ko.mapping.fromJS(statuses, {}, temp_statuses.values);
    self.advanceSearchGroups.push(temp_statuses);

    // LOADS MILESTONES
    var temp_milstones = { "searchGroup": "milestones", "values": ko.observableArray([]) };
    ko.mapping.fromJS(milestones, [], temp_milstones.values);
    self.advanceSearchGroups.push(temp_milstones);

    var temp_quantities = { "searchGroup": "quantity", "values": ko.observableArray([]) };
    ko.mapping.fromJS(quantity, [], temp_quantities.values);
    self.advanceSearchGroups.push(temp_quantities);

    var temp_acct_mgr_data = [{
        "name": "Account Manager",
        "value": ''
    }];

    var temp_acct_mgr = { "searchGroup": "account Manager", "values": ko.observableArray([]) };
    ko.mapping.fromJS(temp_acct_mgr_data, [], temp_acct_mgr.values);
    self.advanceSearchGroups.push(temp_acct_mgr);

    var temp_sales_data = [{
        "name": "Sales",
        "value": ''
    }];

    var temp_sales = { "searchGroup": "sales", "values": ko.observableArray([]) };
    ko.mapping.fromJS(temp_sales_data, [], temp_sales.values);
    self.advanceSearchGroups.push(temp_sales);

    self.salesList = ko.observableArray([]);

    var temp_IS = { "searchGroup": "IS", "values": ko.observableArray([]) };
    ko.mapping.fromJS(IS, [], temp_IS.values);
    self.advanceSearchGroups.push(temp_IS);

    var temp_production = { "searchGroup": "production", "values": ko.observableArray([]) };
    ko.mapping.fromJS(production, [], temp_production.values);
    self.advanceSearchGroups.push(temp_production);

    var temp_flags = { "searchGroup": "flags", "values": ko.observableArray([]) };
    ko.mapping.fromJS(flags, [], temp_flags.values);
    self.advanceSearchGroups.push(temp_flags);

    if (featureLogAccountReceivables()) {
        var temp_finance = { "searchGroup": "finance", "values": ko.observableArray([]) };
        ko.mapping.fromJS(finance, [], temp_finance.values);
        self.advanceSearchGroups.push(temp_finance);
    }

    var temp_client_data = [{
        "name": 'Client',
        "value": ''
    }];

    var temp_client = { "searchGroup": "client", "values": ko.observableArray([]) };
    ko.mapping.fromJS(temp_client_data, [], temp_client.values);
    self.advanceSearchGroups.push(temp_client);

    self.clients = ko.observableArray([]);

    var default_customers = [];
    default_customers = [
            {
                "name": "Customer",
                "values": customers
            },
            {
                "name": "User Type",
                "values": [{ "name": "Admin", "value": "admin" }, { "name": "Power User", "value": "power" }, { "name": "User", "value": "user" }]
            },
            {
                "name": "",
                "values": [{ "name": "Definition", "value": "definition" }, { "name": "Customer", "value": "customer" }, { "name": "Facility", "value": "facility" }, { "name": "Platform", "value": "platform" }, { "name": "Server", "value": "server" }, { "name": "Status", "value": "status" }, { "name": "Milestones", "value": "milestones" }]
            }
    ];
    var temp_cust_defaults = { "searchGroup": "setCustomerSearchDefaults", "values": ko.observableArray([]) };
    ko.mapping.fromJS(default_customers, {}, temp_cust_defaults.values);
    self.advanceSearchGroups.push(temp_cust_defaults);

    self.displaySelectedList = function (data, e) {
        var dialog_text = $('#displayAllListPop').find('h3').text();
        if (dialog_text == "All Account Managers") {
            var selected_items = $('#dvAccountManagers input[type="checkbox"]:checked');
            $.each(selected_items, function (key, val) {
                self.selectedAccountMgr.push(val.value);
            });
        }
        else if (dialog_text == "All Clients") {
            var selected_items = $('#dvClients input[type="checkbox"]:checked');
            $.each(selected_items, function (key, val) {
                self.selectedClient.push(val.value);
            });
        }
        else {
            var selected_items = $('#dvSales input[type="checkbox"]:checked');
            $.each(selected_items, function (key, val) {
                self.selectedSales.push(val.value);
            });
        }
        $('#displayAllListPop').popup('close');
        self.search();
    };

    //Loads JOBS list
    $.each(jobsList, function (key, val) {
        self.jobsList.push(new jobModel(val));
    });
    $('#chkSelectAll').is(':checked')

    self.showAllList = function (data, e) {
        self.isDisplayAccountManagers(false);
        self.isDisplayClients(false);
        self.isDisplaySales(false);
        if (data.name() == "Account Manager") {
            self.isDisplayAccountManagers(true);
            $('#displayAllListPop').find('h3').text('All Account Managers');
            $('#dvAccountManagers input[type=checkbox]').checkboxradio();
        }
        else if (data.name() == "Client") {
            self.isDisplayClients(true);
            $('#displayAllListPop').find('h3').text('All Clients');
            $('#dvClients input[type=checkbox]').checkboxradio();
        }
        else {
            self.isDisplaySales(true);
            $('#displayAllListPop').find('h3').text('All Sales');
            $('#dvSales input[type=checkbox]').checkboxradio();
        }
        //window.setTimeout(function () {            
        $('#displayAllListPop').popup('open');
        //}, 300);
    };

    self.validateBeforeRunAction = function () {
        var msg = '';
        var is_complete_job_details_checked = $('#actionsMenu input[type=radio][id=rdoCompleteJobDetails]').is(':checked');
        if ($('#dvJobsList input[type=checkbox]:checked').length > 1 && ($('#actionsMenu input[type=radio][action-type=single]:checked').length > 0))
            msg = "This action may only be run with a single job selected. Please select only one job and try again."
        else if ($('#actionsMenu input[type=radio]:checked').length == 0)
            msg = "Please select at least one action."

        if (msg != '') {
            $('#alertmsg').text(msg);
            $('#actionsMenu').popup('close');
            window.setTimeout(function () {
                $('#popupDialog').popup('open');
            }, 300);
            return false;
        }
        $('#actionsMenu').popup('close');
        if ($('#actionsMenu input[type=radio][id*=JobDiagnostics]:checked').length > 0 || is_complete_job_details_checked) {
            var job_name = $('#dvJobsList input[type=checkbox]:checked').parent().find('label span').eq(0).text();
            var job_number = $('#dvJobsList input[type=checkbox]:checked').attr('id').replace('chk', '');
            var job_info = {};
            job_info["jobName"] = job_name;
            job_info["jobNumber"] = job_number;
            sessionStorage.selectedJobInfo = JSON.stringify(job_info);
            window.setTimeout(function () {
                if (is_complete_job_details_checked)
                    window.location.href = "../jobDetails/jobDetails.html";
                else
                    window.location.href = "../jobDiagnostics/jobHealth.html";
            }, 300);
        }
        else if ($('#actionsMenu input[type=radio][id*=AccountReceivables]:checked').length > 0) {
            var job_name = $('#dvJobsList input[type=checkbox]:checked').parent().find('label span').eq(0).text();
            var job_number = $('#dvJobsList input[type=checkbox]:checked').attr('id').replace('chk', '');
            var job_info = {};
            job_info["jobName"] = job_name;
            job_info["jobNumber"] = job_number;
            sessionStorage.selectedJobInfo = JSON.stringify(job_info);
            window.setTimeout(function () {
                window.location.href = "../adminFinance/jobFinanceInfo.html";
            }, 300);
        }
    };

    self.bindShowAllList = function (caption) {
        switch (caption) {
            case "Account Manager":
                self.accountManagers(acct_manager);
                break;
            case "Client":
                self.clients(client);
                break;
            case "Sales":
                self.salesList(sae);
                break;
            default:
                break;
        }
    };

    self.openActionsMenu = function () {
        //$('#dvJobsList input[type=checkbox]').attr('checked', false).checkboxradio('refresh');
        $('#actionsMenu input[type=radio]').attr('checked', false).checkboxradio('refresh');
        $('#actionsMenu').popup('open');
    };
    self.advSearchJson = ko.observable({});
    self.advancedSearchClick = function () {
        var caption = $('div[data-blocktype=advSearchButton] a').text();
        if (caption == "Search Filters") {
            $('div[data-blocktype=advSearchButton] a').text('Search');
            $('#pnlSearch').panel('open');
        }
        else if (caption == "Search") {
            var msg = self.validateSearchData();
            if (msg != "") {
                $('#alertmsg').html(msg);
                $('#popupDialog').popup('open');
                return false;
            }
            else {
                self.selectedPageValue(0);
                self.getAdvSearchResults();
            }
        }

        //else if (caption == "Search") {
        //    $('div[data-blocktype=advSearchButton] a').text('Advanced Search');
        //}
    };

    self.disableStaticSearchDefaults = function () {
        var disable_searchGroups = ['Platform', 'Server', 'Job Type', 'Status', 'Milestones', 'IS', 'Production', 'Flags', 'Finance', 'Set Customer Search Defaults'];
        $.each(disable_searchGroups, function (key, search_group) {
            $('[data-role="collapsible"] h3:contains("' + search_group + '")').next('div').children(':first').css('display', 'block');
            $('[data-role="collapsible"] h3:contains("' + search_group + '")').next('div').children(':last').attr('disabled', 'disabled');
            $('[data-role="collapsible"] h3:contains("' + search_group + '")').next('div').find('input').attr('disabled', 'disabled');
            $('[data-role="collapsible"] h3:contains("' + search_group + '")').next('div').find('select').attr('disabled', 'disabled');
        });
    };

    self.clearSearchClick = function () {
        self.definition({});
        self.selectedCustomer.removeAll();
        self.selectedCustomer.push(jobCustomerNumber);
        self.selectedFacilities(null);
        self.selectedFacilities(sessionStorage.facilityId);
        self.selectedPlatform.removeAll();
        self.selectedChannels.removeAll();
        self.selectedServer.removeAll();
        self.selectedStatus.removeAll();
        self.selectedMilestones.removeAll();
        self.quantity({});
        self.selectedAccountMgr.removeAll();
        self.selectedSales.removeAll();
        self.selectedIS.removeAll();
        self.selectedProduction.removeAll();
        self.selectedFlags.removeAll();
        self.selectedFinance.removeAll();
        self.selectedClient.removeAll();
        self.filteredAccountMgr.removeAll();
        self.filteredClient.removeAll();
        self.advSearchJson({});
        self.advanceSearchGroups = ko.observableArray([]);

        $('input[type=checkbox]').checkboxradio('refresh');
        var temp_cust = { "searchGroup": "customer", "values": ko.observableArray([]) };
        ko.mapping.fromJS(customers, [], temp_cust.values);
        self.advanceSearchGroups.push(temp_cust);

        // LOADS FACILITIES
        var temp_facility = { "searchGroup": "facility", "values": ko.observableArray([]) };
        ko.mapping.fromJS(facilities, [], temp_facility.values);
        self.advanceSearchGroups.push(temp_facility);

        $('#spnSearchTerms').html('');
        $('#dvSearchTerms').css('display', 'none');
    };

    self.search = function (show_terms) {
        var temp_terms = "";
        var adv_search_json = {};
        //Definition
        var temp_definition = ko.mapping.toJS(self.definition, mapping);
        var temp_def = "";
        var temp_json = {};
        $.each(temp_definition, function (key, val) {
            switch (key) {
                case "jobName":
                    if (val != "") {
                        temp_def += (temp_def != "") ? ', ' + '<b>Job Name:</b>' + val : '<b>Job Name:</b>' + val;
                        temp_json[key] = val;
                        adv_search_json[key] = val;
                    }
                    break;
                case "jobNumber":
                    if (val != "") {
                        temp_def += (temp_def != "") ? ', ' + '<b>Job #:</b>' + val : '<b>Job #:</b>' + val;
                        temp_json["jobNumberList"] = val;
                        var neg_job_nos = [];
                        var job_nos = [];
                        var temp_job_nos = null;
                        if (val.indexOf(',') > -1) {
                            temp_job_nos = [];
                            temp_job_nos = val.split(',');
                            $.each(temp_job_nos, function (key, val) {
                                if (val < 0) {
                                    neg_job_nos.push(val);
                                }
                                else if (val > 0) {
                                    job_nos.push(val);
                                }
                            });
                        } else {
                            if (val < 0) {
                                neg_job_nos.push(val);
                            }
                            else if (val > 0) {
                                job_nos.push(val);
                            }
                            //temp_job_nos.push(val);
                        }
                        if (job_nos.length > 0)
                            adv_search_json["jobNumberList"] = job_nos;
                        if(neg_job_nos.length>0)
                            adv_search_json["tempJobNumberList"] = neg_job_nos;
                    }
                    break;
                case "inHomeDateFrom":
                    if (val != "") {
                        temp_def += (temp_def != "") ? ', ' + '<b>In-Home Date From:</b>' + val : '<b>In-Home Date From:</b>' + val;
                        temp_json[key] = val;
                        adv_search_json[key] = val;
                    }
                    break;
                case "inHomeDateTo":
                    if (val != "") {
                        temp_def += (temp_def != "") ? ', ' + '<b>In-Home Date To:</b>' + val : '<b>In-Home Date To:</b>' + val;
                        temp_json[key] = val;
                        adv_search_json[key] = val;
                    }
                    break;
                default:
                    break;
            }
        });
        if (temp_def != "") {
            temp_terms += '<b>Definition- </b>:[' + temp_def + ']';
            //adv_search_json["definition"] = temp_json;
        } /*
        //Quantity
        var temp_quantity = ko.mapping.toJS(self.quantity, mapping);
        var temp_def = "";
        var temp_json = {};
        $.each(temp_quantity, function (key, val) {
            switch (key) {
                case "quantityFrom":
                    if (val != "") {
                        temp_def += (temp_def != "") ? ', ' + '<b>Qty From:</b>' + val : '<b>Qty From:</b>' + val;
                        temp_json[key] = val;
                    }
                    break;
                case "quantityTo":
                    if (val != "") {
                        temp_def += (temp_def != "") ? ', ' + '<b>Qty To:</b>' + val : '<b>Qty To:</b>' + val;
                        temp_json[key] = val;
                    }
                    break;
                default:
                    break;
            }
        });
        if (temp_def != "") {
            temp_terms += '<b>Quantity- </b>:[' + temp_def + ']';
            adv_search_json["quantity"] = temp_json;
        }*/
        //Customer
        var cnt = 0;
        var temp_customer = ko.mapping.toJS(self.selectedCustomer, mapping);
        if (temp_customer.length > 0) {
            adv_search_json["customerNumberList"] = temp_customer;//.join(',');
            temp_terms += (temp_terms != '') ? ', <b>Customer- </b>' : '<b>Customer- </b>';
            cnt = 0;
            $.each(customers, function (key, val) {
                if (temp_customer.indexOf(val.value) > -1) {
                    temp_terms += (cnt > 0) ? ', ' + val.name : val.name;
                    cnt++;
                }
            });
        }

        //Facilities
        var temp_facilities = ko.mapping.toJS(self.selectedFacilities, mapping);
        if (temp_facilities.length > 0) {
            adv_search_json["facilityId"] = temp_facilities;
            temp_terms += (temp_terms != '') ? ', <b>Facility- </b>' : '<b>Facility- </b>';
            cnt = 0;
            $.each(facilities, function (key, val) {
                if (temp_facilities == val.value) {
                    temp_terms += val.name;
                    return false;
                }
            });
        }
        /*
        
        //Channel
        var temp_channel = ko.mapping.toJS(self.selectedChannels, mapping);
        if (temp_channel.length > 0) {
            adv_search_json["channelList"] = temp_channel;
            temp_terms += (temp_terms != '') ? ', <b>Channel- </b>' : '<b>Channel- </b>';
            cnt = 0;
            $.each(channel, function (key, val) {
                if (temp_channel.indexOf(val.value) > -1) {
                    temp_terms += (cnt > 0) ? ', ' + val.name : val.name;
                    cnt++;
                }
            });
        }
      
        //Platform
        var temp_platform = ko.mapping.toJS(self.selectedPlatform, mapping);
        if (temp_platform.length > 0) {
            adv_search_json["platform"] = temp_platform.join(',');
            temp_terms += (temp_terms != '') ? ', <b>Platform- </b>' : '<b>Platform- </b>';
            cnt = 0;
            $.each(platform, function (key, val) {
                if (temp_platform.indexOf(val.value) > -1) {
                    temp_terms += (cnt > 0) ? ', ' + val.name : val.name;
                    cnt++;
                }
            });
        }

        //Server
        var temp_server = ko.mapping.toJS(self.selectedServer, mapping);
        if (temp_server.length > 0) {
            adv_search_json["server"] = temp_server.join(',');
            temp_terms += (temp_terms != '') ? ', <b>Server- </b>' : '<b>Server- </b>';
            cnt = 0;
            $.each(servers, function (key, val) {
                if (temp_server.indexOf(val.value) > -1) {
                    temp_terms += (cnt > 0) ? ', ' + val.name : val.name;
                    cnt++;
                }
            });
        }
        //Job Type ID
        var temp_jobTypeId = ko.mapping.toJS(self.selectedJobType().jobTypeID, mapping);
        if (temp_jobTypeId != undefined) {
            adv_search_json["jobtypeid"] = temp_jobTypeId;
            temp_terms += (temp_terms != '') ? ', <b>Job Type ID- </b>' : '<b>Job Type ID- </b>';
            temp_terms += temp_jobTypeId;
        }
        //Status
        var temp_status = ko.mapping.toJS(self.selectedStatus, mapping);
        if (temp_status.length > 0) {
            adv_search_json["status"] = temp_status.join(',');
            temp_terms += (temp_terms != '') ? ', <b>Status- </b>' : '<b>Status- </b>';
            cnt = 0;
            $.each(statuses, function (key, val) {
                if (temp_status.indexOf(val.value) > -1) {
                    temp_terms += (cnt > 0) ? ', ' + val.name : val.name;
                    cnt++;
                }
            });
        }
        //Milestones
        var temp_milestone = ko.mapping.toJS(self.selectedMilestones, mapping);
        if (temp_milestone.length > 0) {
            adv_search_json["milestones"] = temp_milestone.join(',');

            temp_terms += (temp_terms != '') ? ', <b>Milestones- </b>' : '<b>Milestones- </b>';
            cnt = 0;
            $.each(milestones, function (key, val) {
                if (temp_milestone.indexOf(val.value) > -1) {
                    temp_terms += (cnt > 0) ? ', ' + val.name : val.name;
                    cnt++;
                }
            });
        }

        //Account Manager
        var temp_acct_mgr = ko.mapping.toJS(self.selectedAccountMgr, mapping);
        if (temp_acct_mgr.length > 0) {
            adv_search_json["acctMgrList"] = temp_acct_mgr;
            temp_terms += (temp_terms != '') ? ', <b>Account Manager- </b>' : '<b>Account Manager- </b>';
            temp_terms += temp_acct_mgr.join(',');
        }

        //Client
        var temp_client = ko.mapping.toJS(self.selectedClient, mapping);
        if (temp_client.length > 0) {
            adv_search_json["clientList"] = temp_client;
            temp_terms += (temp_terms != '') ? ', <b>Client- </b>' : '<b>Client- </b>';
            temp_terms += temp_client.join(',');
        }

        //Sae
        var temp_sae = ko.mapping.toJS(self.selectedSales, mapping);
        if (temp_sae.length > 0) {
            adv_search_json["saeList"] = temp_sae;
            temp_terms += (temp_terms != '') ? ', <b>Sales- </b>' : '<b>Sales- </b>';
            temp_terms += temp_sae.join(',');
        }
        //IS
        var temp_IS = ko.mapping.toJS(self.selectedIS, mapping);
        if (temp_IS.length > 0) {
            adv_search_json["is"] = temp_IS.join(',');
            temp_terms += (temp_terms != '') ? ', <b>IS- </b>' : '<b>IS- </b>';
            cnt = 0;
            $.each(IS, function (key, val) {
                if (temp_IS.indexOf(val.value) > -1) {
                    temp_terms += (cnt > 0) ? ', ' + val.name : val.name;
                    cnt++;
                }
            });
        }
        //Production
        var temp_production = ko.mapping.toJS(self.selectedProduction, mapping);
        if (temp_production.length > 0) {
            adv_search_json["production"] = temp_production.join(',');
            temp_terms += (temp_terms != '') ? ', <b>Production- </b>' : '<b>Production- </b>';
            cnt = 0;
            $.each(production, function (key, val) {
                if (temp_production.indexOf(val.value) > -1) {
                    temp_terms += (cnt > 0) ? ', ' + val.name : val.name;
                    cnt++;
                }
            });
        }
        //Flags
        var temp_flags = ko.mapping.toJS(self.selectedFlags, mapping);
        if (temp_flags.length > 0) {
            adv_search_json["flags"] = temp_flags.join(',');
            temp_terms += (temp_terms != '') ? ', <b>Flags- </b>' : '<b>Flags- </b>';
            cnt = 0;
            $.each(flags, function (key, val) {
                if (temp_flags.indexOf(val.value) > -1) {
                    temp_terms += (cnt > 0) ? ', ' + val.name : val.name;
                    cnt++;
                }
            });
        }
        //Finance
        var temp_finance = ko.mapping.toJS(self.selectedFinance, mapping);
        if (temp_finance.length > 0) {
            adv_search_json["finance"] = temp_finance.join(',');
            temp_terms += (temp_terms != '') ? '. <b>Finance- </b>' : '<b>Finance- </b>';
            cnt = 0;
            $.each(finance, function (key, val) {
                if (temp_finance.indexOf(val.value) > -1) {
                    temp_terms += (cnt > 0) ? ', ' + val.name : val.name;
                    cnt++;
                }
            });
        }
        */
        if (show_terms != false) {
            $('#spnSearchTerms').html(temp_terms);
            $('#dvSearchTerms').css('display', 'block');
        }
        self.advSearchJson(adv_search_json);
    };

    self.deleteSelectedItem = function (data, e) {
        var ctrl = e.target || e.targetElement || e.currentTarget;
        var control_name = ko.contextFor(ctrl).$parent.name();
        if (control_name.indexOf('Client') > -1)
            self.selectedClient.remove(data);
        else if (control_name.indexOf('Account Manager') > -1)
            self.selectedAccountMgr.remove(data);
        else
            self.selectedSales.remove(data);

        self.search();

        $('#ul' + control_name.replace(/ /g, '_')).listview('refresh');
    };

    self.validateSearchData = function () {
        var msg = "";
        if (new Date($('#txtIn-HomeDateFrom').val()) > new Date($('#txtIn-HomeDateTo').val())) {
            msg = "From Date cannot be greater than To Date."
        }
        if ($('#txtQtyFrom').val() > $('#txtQtyTo').val()) {
            msg = ((msg != "") ? msg + "<br />" : "") + "From Quantity cannot be greater than To Quantity."
        }
        return msg;
    };

    self.getAdvSearchResults = function () {
        $('#loadingText1').text('Loading...');
        $('#waitPopUp').popup('open');
        window.setTimeout(function () {
            //$('div[data-blocktype=advSearchButton] a').text('Advanced Search');
            var adv_search_url = serviceURLDomain + "api/JobSearch_doSearch/" + self.selectedPageValue();
            var search_json = ko.mapping.toJS(self.advSearchJson);
            /*if (search_json["definition"] != undefined && search_json["definition"] != null) {
                var def = search_json.definition;
                $.each(def, function (key, val) {
                    search_json[key] = val;
                });
                delete search_json["definition"];
            }
            if (search_json["quantity"] != undefined && search_json["quantity"] != null) {
                var qty = search_json.quantity;
                $.each(qty, function (key, val) {
                    search_json[key] = parseInt(val);
                });
                delete search_json["quantity"];
            }
            if (search_json["sales"] != undefined && search_json["sales"] != null) {
                search_json['saeList'] = [];
                $.each(search_json["sales"].split(','), function (key, val) {
                    search_json['saeList'].push(val);
                });
                delete search_json["sales"];
            }
            if (search_json["csr"] != undefined && search_json["csr"] != null) {
                search_json['acctMgrList'] = [];
                $.each(search_json["csr"].split(','), function (key, val) {
                    search_json['acctMgrList'].push(val);
                });
                delete search_json["csr"];
            }*/
            postCORS(adv_search_url, JSON.stringify(search_json), function (data) {
                jobsList = data.searchMatchList;
                self.pagingData.removeAll();
                self.pagingData(createPagerObject1(data.searchMatchCount, data.totalPages));

                var selected_value = (self.selectedPageValue() != undefined && self.selectedPageValue() != null && self.selectedPageValue() != "") ? self.selectedPageValue() : "0";
                //self.selectedPageValue(0);

                var is_more_visible = self.pagingData().length > 0 ? true : false;
                self.showMoreOption(is_more_visible);

                self.jobsList.removeAll();

                $.each(data.searchMatchList, function (key, val) {
                    self.jobsList.push(new jobModel(val));
                });
                $('select').selectmenu();
                $('input[type=text]').trigger('create');
                $('input[type=checkbox]').checkboxradio();

                $('div[data-role=collapsible] ul').listview();
                $('ul li div[data-role=controlgroup]').controlgroup();
                $('ul li div[data-role=controlgroup]').find('input[type=text]').parent().trigger('create');
                $('#waitPopUp').popup('close');
                //$('#pnlSearch').panel('close');

            }, function (error_response) {
                $('#waitPopUp').popup('close');
            });
        }, 200);
    };

    self.moreClick = function (el, e) {
        if (e.originalEvent) {
            $('#loadingText1').text('Loading...');
            $('#waitPopUp').popup('open');
            window.setTimeout(function () {
                $('#pageSelectPopup').popup('close');
                var ctrl = (e.targetElement || e.currentTarget || e.target);
                var ctrl_id = ctrl.id;
                if (ctrl_id == "")
                    ctrl_id = $(ctrl).attr('data-selectedPageOption');
                // var id_suffix = ctrl_id.substring(ctrl_id.lastIndexOf('_') + 1);


                var page_no = "";
                if (ctrl_id.indexOf('Prev') > -1)
                    page_no = parseInt(self.selectedPageValue()) - 1;
                else if (ctrl_id.indexOf("Next") > -1)
                    page_no = parseInt(self.selectedPageValue()) + 1;
                else if (ctrl_id.indexOf('txt') > -1) {
                    if (parseInt(e.target.value) > self.pagingData().length)
                        page_no = (self.pagingData().length - 1);
                    else if (parseInt(e.target.value) < 1)
                        page_no = 0;
                    else
                        page_no = parseInt(e.target.value) - 1;
                }
                //else
                //    page_no = id_suffix - 1;

                self.selectedPageValue(page_no);

                self.getAdvSearchResults();

            }, 500);
        }
    };

    self.bindAutoComplete = function (control_name) {
        var data_obj = control_name.indexOf('txtClient') > -1 ? client : (control_name.indexOf('txtAccountManager') > -1 ? acct_manager : sae);
        $("#" + control_name).autocomplete({
            source: function (request, response) {
                response($.map(data_obj, function (item) {
                    if (item.toLowerCase().indexOf($("#" + control_name).val().toLowerCase()) > -1) {
                        return item;
                    }
                }));
            },
            minLength: 3,
            select: function (event, ui) {
                var disp_name = getDisplayLabel(control_name.replace('txt', '')).replace(/ /g, '_');
                $("#" + control_name).val('');
                if (control_name.indexOf('txtClient') > -1) {
                    self.selectedClient.push(ui.item.label);
                }
                else if (control_name.indexOf('txtAccountManager') > -1) {
                    self.selectedAccountMgr.push(ui.item.label);
                }
                else
                    self.selectedSales.push(ui.item.label);
                self.search();
                $('#ul' + disp_name).listview('refresh');
                return false;
            },
            response: function (event, ui) {
                if (ui.content.length === 0) {
                    $('#error-message').css("visibility", "visible");
                } else {
                    $('#error-message').css("visibility", "hidden");
                }
            }
        });
    };
}

//Capitalizes the first letter in the given string.
String.prototype.initCap = function () {
    return this.replace(/(?:^|\s)[a-z]/g, function (m) {
        return m.toUpperCase();
    });
};
String.prototype.initLower = function () {
    return this.replace(/(?:^|\s)[a-z]/g, function (m) {
        return m.toLowerCase();
    });
};
//Gets the display name of each piece attribute labels.
function getDisplayLabel(attr_name) {
    var PATTERN = /^[ABCDEFGHIJKLMNOPQRSTUVWXYZ]/;
    var display_name = '';
    $.each(new String(attr_name), function (key, value) {
        if (key > 0 && value.match(PATTERN)) {
            if (attr_name[key - 1] != undefined && attr_name[key - 1].match(PATTERN))
                display_name += value;
            else
                display_name += ' ' + value;
        }
        else {
            if (parseInt(value) > -1 && (attr_name[key - 1] != undefined && attr_name[key - 1].toUpperCase().match(PATTERN)))
                display_name += (key == 0) ? value : '-' + value;
            else
                display_name += (key == 0 || attr_name[key - 1] == " ") ? value.toUpperCase() : value;
        }
    });
    return display_name.replace('( ', '(');
};

function createPagerObject1(total_jobs, pages_count) {
    //var pager_length = Math.ceil(total_jobs / 30);
    var pager_object = [];
    if (pages_count > 1) {
        for (var j = 0; j < pages_count; j++) {
            pager_object.push({ 'name': 'Page ' + (j + 1), 'value': (j) });
        }
    }
    return pager_object;
}