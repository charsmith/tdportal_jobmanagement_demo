﻿var mapping = { 'ignore': ["__ko_mapping__"] }
var configInfo = [];
var customersRoles = {};
var modules = [];
var newModuleTemplate = {
    "moduleId": ko.observable(""),
    "moduleName": ko.observable(""),
    "isDefault": ko.observable("")
};

$.getJSON("JSON/_customersRoles.JSON", function (data) {
    customersRoles = data;
    //console.log(customersRoles);
});

$.getJSON("JSON/_moduleConfiguration.JSON", function (data) {
    configInfo = data;
    //console.log(configInfo);
});

$.getJSON("JSON/modules.JSON", function (data) {
    modules = data;
});

$('#_moduleConfiguration').live('pagebeforecreate', function (event) {
    loadingImg("_moduleConfiguration");
    displayMessage('_moduleConfiguration');
    createConfirmMessage('_moduleConfiguration');
    ko.setTemplateEngine(new ko.nativeTemplateEngine);
});

$(document).on('pageshow', '#_moduleConfiguration', function (event) {
    $(".ui-table-columntoggle-btn").text("Customers...");
    $(".ui-table-columntoggle-btn").removeClass('ui-btn-b').addClass('ui-btn-a');
    $('#dvSelectDefault').trigger('create');
    $('div.ui-slider').css('margin-left', '10px');
    window.setTimeout(function () {
        ko.applyBindings(new moduleConfigurationModel());
        $('#configTable').table('refresh');
        $('#configTable').css('display', 'block');
        $('#configTable').css('display', '');
        //window.setTimeout(function () {
        //$('input[type=checkbox]').trigger('create');
        $('input[type=checkbox]').checkboxradio();

        $('#selectModules').selectmenu('refresh', true);
        $('#ddlSelectConfiguration').selectmenu('refresh');
        //}, 1000);
    }, 1000);
});

var moduleConfigurationModel = function () {
    var self = this;
    self.customerRoles = customersRoles;
    self.modules = ko.observableArray([]);
    self.selectedModuleConfig = ko.observable({});
    self.selectedModule = ko.observable('templates');
    self.selectedModuleToEditOrAdd = ko.observable({});
    self.selectedConfiguration = ko.observable('visibility');
    self.selectedModuleControlToEditOrAdd = ko.observable({});
    self.selectedAddType = ko.observable('control');
    var modules_temp = [];
    ko.mapping.fromJS(modules, [], self.modules);

    var tmp = {};//ko.observable({});
    ko.mapping.fromJS(configInfo, {}, tmp);

    self.selectedModuleConfig(tmp);
    self.selectedModule = ko.observable();
    self.moduleConfigChange = function (data, e) {
        var ele = e.target || e.currentTarget;
        var ele_checked = $(ele).is(":checked");
        var current_role = $(ele).attr('data-customerRole');
        var customer = data.customer();

        if (ele_checked) {
            var message = "Do you want to show all controls to <b>" + current_role + "</b> userrole?";
            $('#confirmMsg').html(message);
            $('#okButConfirm').bind('click', function () {
                $('#okButConfirm').unbind('click');
                $('#popupConfirmDialog').popup('close');
                self.updateModuleControlVisibility(customer, current_role, true);
            });
            $('#cancelButConfirm').bind('click', function () {
                $('#cancelButConfirm').unbind('click');
                $("input[type=checkbox][data-control^=" + customer + "_" + current_role + "_]").removeAttr('disabled').checkboxradio('refresh', true);
                $('#popupConfirmDialog').popup('close');
            });
            $('#popupConfirmDialog').popup('open', { positionTo: 'window' });
        }
        else {
            self.updateModuleControlVisibility(customer, current_role, false);
        }
    };

    self.updateModuleControlVisibility = function (customer, role, show_item) {
        $.each(self.selectedModuleConfig().variableItems(), function (key, val) {
            $.each(val.itemConfig(), function (key1, val1) {
                if (val1.customer() == customer) {
                    switch (role) {
                        case "super":
                            val1.showItem.super(show_item);
                            break;
                        case "admin":
                            val1.showItem.admin(show_item);
                            break;
                        case "power":
                            val1.showItem.power(show_item);
                            break;
                        case "user":
                            val1.showItem.user(show_item);
                            break;
                        case "basic":
                            val1.showItem.basic(show_item);
                            break;
                        default: break;
                    }
                }
            });
        });
        $('input[type=checkbox]').checkboxradio('refresh', true);
        window.setTimeout(function () {
            if (show_item)
                $("input[type=checkbox][data-control^=" + customer + "_" + role + "_]").removeAttr('disabled').checkboxradio('refresh', true);
            else
                $("input[type=checkbox][data-control^=" + customer + "_" + role + "_]").prop('disabled', 'disabled').checkboxradio('refresh', true);;
        }, 500);
    }
    
    self.addModule = function (data, event) {
        var tmp = {};
        var temp = ko.mapping.toJS(self.selectedModuleToEditOrAdd, mapping);
        ko.mapping.fromJS(temp, {}, tmp);
        self.modules.push(self.selectedModuleToEditOrAdd());
        self.selectedModuleToEditOrAdd({});
        $('#selectModules').selectmenu('refresh');
        $('#popUpAddModuleControl').popup('close');
    }

    self.cancelModuleControl = function (data, event) {
        self.selectedModuleControlToEditOrAdd({});
        $('#popUpAddModuleControl').popup('close');
    };

    self.cancelModule = function (data, event) {
        self.selectedModuleToEditOrAdd({});
        $('#popUpAddModuleControl').popup('close');
    }

    self.moduleChanged = function (data, event) {
        //Business logic
    };

    self.addTypeChanged = function (data, event) {
        var ele = event.target || event.currentTarget;
        var add_type = $(ele).val();
        self.selectedAddType(add_type);
        $("#popUpAddModuleControl h3").text(add_type === 'module' ? 'Add Module' : 'Add Module Control');
    };

    self.formatOnBlur = function (data, event) {
        var ele = event.target || event.currentTarget;
        $(ele).parent().removeClass('ui-shadow-inset');
        $(ele).parent().css('border-width', '0px');
        $(ele).parent().css('border-style', 'none');
        $(ele).parent().css("-webkit-box-shadow", "none !important");
        $(ele).parent().css("box-shadow", "none !important");
        $(ele).parent().css("background-color", "transparent");
    }

    self.formatOnFocus = function (data, event) {
        var ele = event.target || event.currentTarget;
        $(ele).parent().addClass('ui-shadow-inset');
        $(ele).parent().css('border-width', '1px');
        $(ele).parent().css('border-style', 'solid');
        $(ele).parent().css("background-color", "#ffffff");
    }

    self.configurationChanged = function () {
        if (self.selectedConfiguration() == "labels") {
            $('input[type=text][id*=_label]').textinput();
            $('#configTable').find('div.ui-input-text').removeClass('ui-shadow-inset');
            $('#configTable').find('div.ui-input-text').css('border-width', '0px');
            $('#configTable').find('div.ui-input-text').css('border-style', 'none');
            $('#configTable').find('div.ui-input-text').css("-webkit-box-shadow", "none !important");
            $('#configTable').find('div.ui-input-text').css("box-shadow", "none !important");
            $('#configTable').find('div.ui-input-text').css("background-color", "transparent");
            $('#configTable').find('div.ui-input-text').css("color", "#191919");
            $('#configTable').removeClass('controlDetails');//.addClass('controlLabels');
        }
        else {
            $('#configTable').table('refresh');
            $('#configTable').css('display', 'block');
            $('#configTable').css('display', '');
            $('#configTable input[type=checkbox]').checkboxradio();
            $('#configTable').addClass('controlDetails');
        }
    }

    self.addModuleControlClick = function () {
        var new_var_control = {
            "item": ko.observable(""),
            "controlName": ko.observable(""),
            "controlId": ko.observable(""),
            "controlType": ko.observable(""),
            "isDefault": ko.observable(""),
            "itemConfig": ko.observableArray([]),
        }
        $('#selectAddType').selectmenu('refresh');
        $('#dvAddType').show();
        //var temp = {};
        //ko.mapping.fromJS(new_var_control, {}, temp);
        self.selectedModuleToEditOrAdd(newModuleTemplate);
        self.selectedModuleControlToEditOrAdd(new_var_control);
        //$('#btnAddModule').css('display', 'block');
        //$('#btnUpdateModule').css('display', 'none');
        $('#btnAddModuleControl').text('Add');
        $('#popUpAddModuleControl h3').text('Add Module Control');
        $('#popUpAddModuleControl').popup('open');
    };

    self.addModuleControl = function (data, event) {
        var mode = $('#btnAddModuleControl').text();
        if (mode.toLowerCase() == "update")
        {
            self.selectedModuleControlToEditOrAdd({});
        }
        else {
            var temp_new_control = ko.mapping.toJS(self.selectedModuleControlToEditOrAdd, mapping);
            temp_new_control.item = (temp_new_control.controlName.substring(0, 1) + temp_new_control.controlName.substring(1).replace(/ /g, ''));
            $.each(customersRoles.customers, function (key, val) {
                var item_config = {
                    "customer": val.customerId,
                    "label": temp_new_control.controlName,
                    "showItem": {
                        "super": "1",
                        "admin": temp_new_control.isDefault,
                        "power": temp_new_control.isDefault,
                        "user": temp_new_control.isDefault,
                        "basic": temp_new_control.isDefault
                    }
                }
                temp_new_control.itemConfig.push(item_config);
            });
            var temp = {};
            ko.mapping.fromJS(temp_new_control, {}, temp);
            self.selectedModuleConfig().variableItems.push(temp);
            if ($('#ddlSelectConfiguration').val() == "visibility")
                $('input[type=checkbox][id*=_' + temp_new_control.item + '_]').checkboxradio();
            else {
                $('input[type=text][id*=_label]').textinput().blur();
                $('#configTable').find('div.ui-input-text').removeClass('ui-shadow-inset');
                $('#configTable').find('div.ui-input-text').css('border-width', '0px');
                $('#configTable').find('div.ui-input-text').css('border-style', 'none');
                $('#configTable').find('div.ui-input-text').css("-webkit-box-shadow", "none !important");
                $('#configTable').find('div.ui-input-text').css("box-shadow", "none !important");
                $('#configTable').find('div.ui-input-text').css("background-color", "transparent");
                $('#configTable').find('div.ui-input-text').css("color", "#191919");
                $('#configTable').removeClass('controlDetails');//.addClass('controlLabels');
            }
            self.selectedModuleControlToEditOrAdd({});
        }
        $('#popUpAddModuleControl').popup('close');
    }

    self.editModuleControl = function (data, event) {
        $('#popupModifyControl').popup('close');
        self.selectedAddType('control');
        $('#dvAddType').hide();
        window.setTimeout(function () {
            $('#btnAddModuleControl').text('Update');
            //$('#btnAddModule').css('display', 'none');
            //$('#btnUpdateModule').css('display', 'block');
            $('#popUpAddModuleControl h3').text('Edit Module Control');
            $('#popUpAddModuleControl').popup('open');
        }, 200);
    }

    self.openActionsMenu = function (data, event) {
        self.selectedModuleControlToEditOrAdd(data);
        window.setTimeout(function () {
            $('#popupModifyControl').popup('open', { positionTo: event.target });
        }, 200);
    };
};



function getPageDisplayName(page_name) {
    var PATTERN = /^[ABCDEFGHIJKLMNOPQRSTUVWXYZ]/;
    var page_display_name = '';
    $.each(new String(page_name), function (key, value) {
        if (key > 0 && value.match(PATTERN)) {
            page_display_name += ' ' + value;
        }
        else {
            page_display_name += value;
        }
    });
    return page_display_name;
}

String.prototype.initCap = function () {
    return this.replace(/(?:^|\s)[a-z]/g, function (m) {
        return m.toUpperCase();
    });
};
