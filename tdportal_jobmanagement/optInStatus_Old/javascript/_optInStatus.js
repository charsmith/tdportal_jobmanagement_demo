﻿jQuery.support.cors = true;

//******************** Global Variables Start **************************
var gData;
var jobNumber = "";
var facilityId = "";
jobNumber = getSessionData("jobNumber");
facilityId = sessionStorage.facilityId;

var serviceJSONURL = "../optInStatus/JSON/_optInStatus.JSON";
var serviceURL = serviceURLDomain + "api/CheckoutCaseys/" + jobCustomerNumber + "/" + facilityId + "/" + sessionStorage.jobNumber;
var gOutputData;
var checkoutSummary = [];
var pendingLocations = [];
var approvedLocations = [];
var optOutLocations = [];
var optInLocations = [];
var gPageSize = 5;
var gDataStartIndex = 0;
var gDataList = [];
var dontShowHintsAgain = (localStorage.getItem('doNotShowHintsAgain') != undefined && localStorage.getItem('doNotShowHintsAgain') != null && localStorage.getItem('doNotShowHintsAgain') != "" && localStorage.getItem('doNotShowHintsAgain') != "false") ? true : false;
//******************** Global Variables End **************************

//******************** Page Load Events Start **************************
$('#_optInStatus').live('pagebeforecreate', function (event) {
    displayNavLinks();
    displayMessage('_optInStatus');
    if (sessionStorage.ApprovalCheckoutPrefs == undefined || sessionStorage.ApprovalCheckoutPrefs == null || sessionStorage.ApprovalCheckoutPrefs == "")
        getPagePreferences('ApprovalCheckout.html', sessionStorage.facilityId, jobCustomerNumber);
    else
        managePagePrefs(jQuery.parseJSON(sessionStorage.ApprovalCheckoutPrefs));

    makeGData();
    makeGOutputData();
    //test for mobility...
    //loadMobility();
    $("#navLinksPanel").on("panelopen", function (event, ui) {
        sessionStorage.isNavPanelOpen = true;
    });
    if (jobCustomerNumber == JETS) {
        setDemoHintsSliderValue();
    }
    else if (jobCustomerNumber == CASEYS || jobCustomerNumber == AAG) {
        setDemoHintsSliderValue();
        //window.setTimeout(function loadHints() {
        createDemoHints("jobOptInStatus");
        // }, 100);
    }
});

$(document).on('pageshow', '#_optInStatus', function (event) {
    persistNavPanelState();
    //    if (jobCustomerNumber == JETS) {
    //        $('#divBottom').css('display', 'block');
    //        //$('#tdShowHints').attr('style', 'width:95px;height:55px;');
    //        $('#btnSave').css('display', 'none');
    //        $('#btnContinue').css('display', 'none');
    //    }
    if (jobCustomerNumber == CASEYS || jobCustomerNumber == AAG) {
        $('#optInOutStatus p a').text('Turn Hints Off');
        $('#pendingApproved p a').text('Turn Hints Off');
    }
    if (!dontShowHintsAgain && (jobCustomerNumber == CASEYS || jobCustomerNumber == AAG) && (sessionStorage.showDemoHints == undefined || sessionStorage.showDemoHints == null || sessionStorage.showDemoHints == "" || sessionStorage.showDemoHints == "on") && (sessionStorage.isOptInStatusHintsDisplayed == undefined || sessionStorage.isOptInStatusHintsDisplayed == null || sessionStorage.isOptInStatusHintsDisplayed == "false")) {
        sessionStorage.isOptInStatusHintsDisplayed = true;
        $('#optInOutStatus').popup('close');
        window.setTimeout(function loadHints() {
            $('#optInOutStatus').popup('open', { positionTo: '#tblCheckOutrGrid' });
        }, 1000);
    }
});

function closeInitialPopup(popup_ctrl) {
    $('#' + popup_ctrl).popup('close');
    if (jobCustomerNumber == CASEYS) {
        $('#pendingApproved').popup('open', { positionTo: "#dvOptOutPendingLocations" });
    }
}

//jQuery(window).bind('resize', function () {
//    var width = calculateGridWidth();
//    if (width > 0 &&
//    // Only resize if new width exceeds a minimal threshold
//    // Fixes IE issue with in-place resizing when mousing-over frame bars
//        Math.abs(width - jQuery('#gridMain').width()) > 5) {
//        jQuery('#gridMain').setGridWidth(width);
//    }
//}).trigger('resize');

//$(document).ready(function () {
//    window.setTimeout(function setDelay() {
//        var grid_width = calculateGridWidth();
//        if (grid_width > 0 &&
//        // Only resize if new width exceeds a minimal threshold
//        // Fixes IE issue with in-place resizing when mousing-over frame bars
//        Math.abs(grid_width - jQuery('#gridMain').width()) > 5) {
//            jQuery('#gridMain').setGridWidth(grid_width);
//        }
//        else
//            jQuery('#gridMain').setGridWidth("100%");
//    }, 500);
//});
//******************** Page Load Events End **************************

//******************** Public Functions Start **************************

function makeGOutputData() {
    if (sessionStorage.jobSetupOutput != undefined && sessionStorage.jobSetupOutput != null)
        gOutputData = jQuery.parseJSON(sessionStorage.jobSetupOutput);
    if (gOutputData != null) {
        getUploadedFilesCount();
    }
}

function makeGData() {

    if (jobCustomerNumber == JETS || jobCustomerNumber == AAG) {
        $.getJSON(serviceJSONURL, function (data) {
            rows = [];
            gData = data;
            pendingLocations = gData.pendingApproval;
            approvedLocations = gData.approvedLocations;
            optOutLocations = gData.optOutLocations;
            optInLocations = gData.optInLocations;

            var postage_storeid = [];
            var postage_quantity = [];
            var postage_store_name = [];
            $.each(approvedLocations, function (a, b) {
                postage_storeid[a] = b.storeId;
                postage_quantity[a] = (b.qty != undefined) ? b.qty.replace(/,/g, "") : 0;
                postage_store_name[a] = b.storeName;
            });


            rows.push({
                "inHomeDate": gData.approvalCheckoutSummary.rows[0].inHomeDate,
                "optOutLocations": gData.approvalCheckoutSummary.rows[0].optOutLocations,
                "optInLocations": gData.approvalCheckoutSummary.rows[0].optInLocations,
                "pendingLocations": gData.approvalCheckoutSummary.rows[0].pendingLocations,
                "approvedLocations": gData.approvalCheckoutSummary.rows[0].approvedLocations,
                //"approvedQty": "",
                "costPerPiece": gData.approvalCheckoutSummary.rows[0].costPerPiece,
                "total": gData.approvalCheckoutSummary.rows[0].total
            });
            //if (sessionStorage.userRole == "admin")
            //   rows["billingInvoice"] = "<img id='imgBilling' src='../ApprovalCheckout/images/btn_open_file.png' onmouseover='changeImage(this)' onmouseout='changeImageBack(this)'  onclick='fnBillingInvoice(\"" + postage_storeid + "\",\"" + postage_quantity + "\",\"" + postage_store_name + "\")'\>"

            //$.each(checkoutSummary.rows, function (a, b) {
            //b["postageInvoice"] = "<img id='imgPostage" + a + "' src='../ApprovalCheckout/images/btn_open_file.png' onmouseover='changeImage(this)' onmouseout='changeImageBack(this)' onclick='fnPostageInvoice(\"" + postage_storeid + "\",\"" + postage_quantity + "\")'\>";
            //if (sessionStorage.userRole == "admin")
            //b["billingInvoice"] = "<img id='imgBilling" + a + "' src='../ApprovalCheckout/images/btn_open_file.png' onmouseover='changeImage(this)' onmouseout='changeImageBack(this)'  onclick='fnBillingInvoice(\"" + postage_storeid + "\",\"" + postage_quantity + "\",\"" + postage_store_name + "\")'\>";
            //});
            var gridCols = ["In-Home Date", "Opted Out Locations", "Opted In Locations", "Pending Locations", "Approved Locations", "Cost Per Piece", "Total"];
            //buildCheckoutSummaryGrid(rows);
            $("#tblCheckOutrGrid").css("display", "block");
            $("#tblCheckOutrGrid").css("display", "");
            ko.applyBindings(new checkOutSummaryVM(rows, gridCols));
            $("#tblCheckOutrGrid").table("refresh");
            buildLocations(pendingLocations, approvedLocations, optOutLocations, optInLocations);

        });
    } else {
        getCORS(serviceURL, null, function (data) {
            rows = [];
            gData = data;
            pendingLocations = (gData.locationsDict.pending != undefined && gData.locationsDict.pending != null) ? gData.locationsDict.pending : [];
            approvedLocations = (gData.locationsDict.approved != undefined && gData.locationsDict.approved != null) ? gData.locationsDict.approved : [];
            optOutLocations = (gData.locationsDict["opted-out"] != undefined && gData.locationsDict["opted-out"] != null) ? gData.locationsDict["opted-out"] : [];
            optInLocations = (gData.locationsDict["opted-in"] != undefined && gData.locationsDict["opted-in"] != null) ? gData.locationsDict["opted-in"] : [];

            var postage_storeid = [];
            var postage_quantity = [];
            var postage_store_name = [];
            $.each(approvedLocations, function (a, b) {
                postage_storeid[a] = b.storeId;
                postage_quantity[a] = (b.qty != undefined) ? b.qty.replace(/,/g, "") : 0;
                postage_store_name[a] = b.storeName;
            });


            rows.push({
                "inHomeDate": gData.inHomeDate,
                "optOutLocations": gData.optedOutTotal,
                "optInLocations": gData.optedInTotal,
                "pendingLocations": gData.pendingTotal,
                "approvedLocations": gData.approvedTotal,
                //"approvedQty": "",
                "costPerPiece": gData.costPerPiece,
                "total": gData.total,
                "postageInvoice": "<img id='imgPostage' src='../optInStatus/images/btn_open_file.png' onmouseover='changeImage(this)' onmouseout='changeImageBack(this)' onclick='fnPostageInvoice(\"" + postage_storeid + "\",\"" + postage_quantity + "\")'\>"
            });
            if (sessionStorage.userRole == "admin")
                rows["billingInvoice"] = "<img id='imgBilling' src='../optInStatus/images/btn_open_file.png' onmouseover='changeImage(this)' onmouseout='changeImageBack(this)'  onclick='fnBillingInvoice(\"" + postage_storeid + "\",\"" + postage_quantity + "\",\"" + postage_store_name + "\")'\>"

            //$.each(checkoutSummary.rows, function (a, b) {
            //b["postageInvoice"] = "<img id='imgPostage" + a + "' src='../optInStatus/images/btn_open_file.png' onmouseover='changeImage(this)' onmouseout='changeImageBack(this)' onclick='fnPostageInvoice(\"" + postage_storeid + "\",\"" + postage_quantity + "\")'\>";
            //if (sessionStorage.userRole == "admin")
            //b["billingInvoice"] = "<img id='imgBilling" + a + "' src='../optInStatus/images/btn_open_file.png' onmouseover='changeImage(this)' onmouseout='changeImageBack(this)'  onclick='fnBillingInvoice(\"" + postage_storeid + "\",\"" + postage_quantity + "\",\"" + postage_store_name + "\")'\>";
            //});
            var gridCols = ["In-Home Date", "Opted Out Locations", "Opted In Locations", "Pending Locations", "Approved Locations", "Cost Per Piece", "Total"];
            //buildCheckoutSummaryGrid(rows);
            $("#tblCheckOutrGrid").css("display", "block");
            $("#tblCheckOutrGrid").css("display", "");
            ko.applyBindings(new checkOutSummaryVM(rows, gridCols));
            $("#tblCheckOutrGrid").table("refresh");
            buildLocations(pendingLocations, approvedLocations, optOutLocations, optInLocations);
        }, function (error_response) {
            showErrorResponseText(error_response);
            $("#tblCheckOutrGrid").css("display", "none");
        });
    }
}

var checkOutSummaryVM = function (rows, gridCols) {
    var self = this;
    self.gridRows = rows;
    self.gridColumns = gridCols;
}


function changeImage(ctrl) {
    $('#' + ctrl.id).attr("src", "../optInStatus/images/btn_open_file_lit.png");
}
function changeImageBack(ctrl) {
    $('#' + ctrl.id).attr("src", "../optInStatus/images/btn_open_file.png");
}

//******************** Public Functions End **************************

function buildCheckoutSummaryGrid(checkout_summary) {
    var show_billing = true;
    var show_postage = true;

    $.each(jQuery.parseJSON(sessionStorage.ApprovalCheckoutPrefs).buttonPrefsDict, function (a, b) {
        if ($('#' + b.name) == "imgBilling" && parseInt(b.showControl) == 0)
            var show_billing = false;
        if ($('#' + b.name) == "imgPostage" && parseInt(b.showControl) == 0)
            var show_postage = false;
    });
    if (checkout_summary != null && checkout_summary != undefined) {
        jQuery("#gridMain").jqGrid({
            datatype: "local",
            data: checkout_summary,
            viewrecords: true,
            sortable: false,
            columnReordering: false,
            width: "100%",
            height: "40%",
            hoverrows: false,
            shrinkToFit: true,
            //colNames: ["In-Home Date", "Opted Out Locations", "Opted In Locations", 'Pending Locations', "Approved Locations", "Approved Qty", "Cost Per Piece", "Total", "Postage Invoice", "Billing Invoice"],
            colNames: ["In-Home Date", "Opted Out Locations", "Opted In Locations", 'Pending Locations', "Approved Locations", "Cost Per Piece", "Total", "Postage Invoice", "Billing Invoice"],
            //colNames: ["In-Home Date", "Opted Out Locations", "Opted In Locations", 'Pending Locations', "Approved Locations", "Cost Per Piece", "Total"],
            colModel: [
                { name: 'inHomeDate', width: '120%', sortable: false },
                { name: 'optOutLocations', width: '120%', sortable: false },
                { name: 'optInLocations', width: '120%', sortable: false },
                { name: 'pendingLocations', width: '100%', sortable: false },
                { name: 'approvedLocations', width: '120%', sortable: false },
            //{ name: 'approvedQty', width: '100%', sortable: false },
                { name: 'costPerPiece', width: '120%', sortable: false },
                { name: 'total', width: '100%', sortable: false, align: "right", formatter: 'string' },
            { name: 'postageInvoice', width: '60%', sortable: false, align: "center", hidden: show_billing },
            { name: 'billingInvoice', width: '60%', sortable: false, align: "center", hidden: show_postage }
            ]
        });
    }
}

function buildLocations(pending_locations, approved_locations, optout_locations, optin_locations) {
    buildLists(pending_locations, "PendingLocations");
    buildLists(approved_locations, "ApprovedLocations");
    buildLists(optout_locations, "OptOutLocations");
    buildLists(optin_locations, "OptInLocations");
}

function buildLists(locations, list_name) {
    var pager = ""
    var facility_counter = 0;
    var column_counter = 0;
    var temp_facility_data = [];
    var selector_id = "", list_div = "", caption = "";

    if (jobCustomerNumber != JETS && jobCustomerNumber != AAG) {
        locations = locations.sort(function (a, b) {
            return ((a.state.toLowerCase() + ', ' + a.city.toLowerCase() + ' - ' + a.storeId.toLowerCase() > b.state.toLowerCase() + ', ' + b.city.toLowerCase() + ' - ' + b.storeId.toLowerCase()) && (parseInt(a.storeId) > parseInt(b.storeId)) ? 1 : -1);
        });
    }

    $.each(locations, function (facility_key, facitlity_val) {
        facility_counter++;
        var facility_jobs = facitlity_val.jobDict;
        switch (list_name) {
            case "PendingApproval":
                caption = "Pending Approval";
                break;
            case "ApprovedLocations":
                caption = "Approved Locations";
                break;
            case "ApprovedLocations":
                caption = "Approved Locations";
                break;
            case "OptOutLocations":
                caption = "Opt-out Locations";
                break;
            case "OptInLocations":
                caption = "Opt-inLocations";
                break;
        }

        list_div = list_name;
        var qty = 0;
        var cost = 0;
        qty = (facitlity_val.quantity != undefined && facitlity_val.quantity != null) ? facitlity_val.quantity : 0;
        cost = (facitlity_val.cost != undefined && facitlity_val.cost != null) ? facitlity_val.cost : 0;
        var job_counter = 0;
        var navlink_counter = '';
        var filter_text = facitlity_val.storeId + ' ' + facitlity_val.state + ' ' + facitlity_val.city + ' ' + qty + ' ' + cost;

        var li = "";
        li = '<li id=' + facitlity_val.storeId + '" data-filtertext="' + filter_text + '" data-icon="false">';

        li += '<p>';
        li += '<b>' + facitlity_val.state + ", " + facitlity_val.city + " - " + facitlity_val.storeId + ' </b>';

        li += '<br /><font size=1>Qty:' + qty + ' | Cost:' + cost + '</font>';
        li += '</p>';
        li += '</li>';

        temp_facility_data.push(li);
    });
    gDataList.push({
        "list": list_name,
        "items": temp_facility_data
    });
    makeSoreList(temp_facility_data, selector_id, list_div, caption, column_counter);
    //    $("div ul").each(function (i) {
    //        $(this).listview();
    //    });
}

function fnOpenApproval() {
    window.location = "../CaseysDistrict/CaseysDistrictApproval.html";
}



function makeSoreList(array_name, selector_id, list_div, caption) {
    var header;
    var selector;
    var array = eval(array_name);

    var selector = "";

    var page_size = Math.round(array.length / gPageSize);
    if (page_size * gPageSize < array.length) {
        page_size++;
    }
    if (gDataStartIndex > array.length) gDataStartIndex = 0;
    var selected_index = $('#ddl' + list_div).prop("selectedIndex");
    selected_index = (selected_index == undefined) ? 0 : selected_index;
    var more_list_id = (selected_index == undefined) ? 0 : selected_index;
    if ((more_list_id + 1) > page_size) {
        more_list_id = selected_index = 0;
    }
    var selector = '<div id="containDiv" style="position: absolute; top: 0; right: 0; margin: 0; padding: 0; width:120px;" data-role="fieldgroup" data-type="horizontal" data-mini="true" ><select name="ddl' + list_div + '" id="ddl' + list_div + '" data-theme="f" data-overlay-theme="d" data-native-menu="true" data-mini="true" onchange="loadSelectedPageData(\'' + list_div + '\',\'' + list_div + '\',\'' + caption + '\');">';
    for (var i = 0; (parseInt(i) < parseInt(page_size)) ; i++) {
        selector = (i == selected_index) ? selector + '<option selected value="' + i + '">Page ' + (1 + i) + '</option>' : selector + '<option value="' + i + '">Page ' + (1 + i) + '</option>';
    }
    selector += '</select></div>';

    var temp_array = (array.length > gDataStartIndex) ? array.slice(gDataStartIndex, gDataStartIndex + gPageSize) : array;

    var search_field = '<li data-theme="c" ><input type="search" onfocus="setSearchFocus(\'' + list_div + '\');" style="width:80%" name="search' + list_div + '" id="search' + list_div + '" value="" onkeyup="storeSearchHistory(\'' + list_div + '\',\'' + list_div + '\',\'' + caption + '\');" onchange="storeSearchHistory(\'' + list_div + '\',\'' + list_div + '\',\'' + caption + '\')"/></li>';
    temp_array.splice(0, 0, search_field);

    if (list_div == "PendingApproval") {
        //header = '<li data-role="list-divider"><a target="_self" href="#" onclick="fnFacilityClick(\'' + list_div + '\');" style="text-decoration:none; color:#fff;">' + caption.toUpperCase() + '</a></li>'
        header = '<li data-role="list-divider"><a target="_self" href="#" style="text-decoration:none; color:#fff;">' + caption + '</a>' + selector + '</li>'
        temp_array.splice(0, 0, header)
    }
    else {
        header = '<li style="height:20px" data-theme="c">' + selector + '</li>'
        temp_array.splice(0, 0, header)
    }

    //if (more_list_id > -1 && (more_list_id + 1) < page_size) {
    if ((more_list_id + 1) < page_size) {
        more_list_id = (more_list_id == -1) ? 0 : more_list_id;
        //var more_list = '<li data-theme="c" ><a style="text-decoration:none;" href="#" onclick="getMoreData(\'' + selector_id + '\',\'' + list_div + '\',\'' + (more_list_id + 1) + '\',\'' + caption + '\',\'' + column + '\');">More...</a></li>';
        var more_list = '<li data-theme="c" ><a style="text-decoration:none;" href="#" onclick="getMoreStores(\'ddl' + list_div + '\',\'' + (more_list_id + 1) + '\');">More...</a></li>';
        temp_array.splice(temp_array.length, 0, more_list);
    }
    createStoreLists(list_div, temp_array.join(' '));
    var ctrl = $('#ul' + list_div)[0];
    //showHideListItems(ctrl, false);
}

function setSearchFocus(ctrl) {
    var v = $("#search" + ctrl).val();
    $("#search" + ctrl).val('');
    $("#search" + ctrl).val(v);
    $('#search' + ctrl)[0].focus();
}

function createStoreLists(facility_name, job_list) {
    var search_value = "";
    if ($.find('#ul' + facility_name).length > 0) {
        search_value = $('#search' + facility_name).val();
        $('#ul' + facility_name).empty();
        $('#ul' + facility_name).append(job_list).listview('refresh');
        if (!isMobile && isSafari) $('#ul' + facility_name).listview('refresh');
        $('.ui-page').trigger('create');
        $('#search' + facility_name).val(search_value);
        if (search_value != "") {
            if (jQuery.browser.msie)
                window.setTimeout(function getDelay() { $('#search' + facility_name)[0].focus(); }, 5);
            else
                $('#search' + facility_name)[0].focus();
        }
        else
            $('#search' + facility_name)[0].blur();

    }
}

function storeSearchHistory(selector_id, list_div, caption) {
    var filterd_list = [];
    var search_value = $('#search' + list_div).val();
    var req_reg_data = jQuery.grep(gDataList, function (a, b) {
        return a.list.toLowerCase() === list_div.toLowerCase();
    });
    if (search_value != "" && search_value != undefined) {
        var searched_li = jQuery.grep(req_reg_data[0].items, function (a, b) {
            var filter_text = a.substring(a.indexOf('data-filtertext'), a.indexOf('><p>') - 1);
            filter_text = filter_text.substring(filter_text.indexOf('=') + 2);
            if (filter_text.toLowerCase().indexOf($('#search' + list_div).val().toLowerCase()) > -1) {
                filterd_list.push(a);
            }
            return (filter_text.toLowerCase().indexOf($('#search' + list_div).val().toLowerCase()) > -1);
        });
        makeSoreList(filterd_list, selector_id, list_div, caption);
    }
    else {
        gDataStartIndex = 0;
        $('#ddl' + selector_id).val(gDataStartIndex);
        var page_array = [];
        $.each(gDataList, function (facility_key, facitlity_val) {
            if (facitlity_val.list != null && (facitlity_val.list.toLowerCase() == list_div.toLowerCase()))
                page_array = facitlity_val.items;
        });
        makeSoreList(page_array, selector_id.replace(/ /g, '').replace('.', ''), list_div, caption);
    }
    $('#ul' + list_div).each(function (i) {
        $(this).listview();
    });
    $('#ul' + list_div).listview('refresh');
}

function loadSelectedPageData(selector_id, list_div, caption) {
    var multiplier = parseInt($('#ddl' + list_div).prop("selectedIndex"));
    gDataStartIndex = multiplier * gPageSize;
    var page_array = [];

    if ($('#search' + list_div).val() != "" && $('#search' + list_div).val() != undefined) {
        storeSearchHistory(selector_id, list_div, caption);
    }
    else {
        $.each(gDataList, function (facility_key, facitlity_val) {
            if (facitlity_val.list != null && (facitlity_val.list.toLowerCase() == list_div.toLowerCase()))
                page_array = facitlity_val.items;
        });
        if ((multiplier * gPageSize) > page_array.length) {
            multiplier = parseInt(0);
            gDataStartIndex = multiplier * gPageSize;
            $('#hid' + list_div + 'PageNumber')[0].value = "0";
        }
        makeSoreList(page_array, selector_id, list_div, caption);
    }
    $('#ul' + list_div).each(function (i) {
        $(this).listview();
    });
    $('#ul' + list_div).listview('refresh');
    //    if (!jQuery.browser.msie) {
    //        $("div select").each(function (i) {
    //            $(this).selectmenu();
    //        });
    //    }
    $("#containDiv").css({ "position": "absolute", "top": "0", "right": "0", "margin": "0", "padding": "0", "width": "120px" });
    //$("#containDiv").attr({ "data-role": "controlgroup", "data-type": "horizontal", "data-mini": "true" });
}

function getMoreStores(selector_id, page_id) {
    $('#' + selector_id).val(page_id);
    $('#' + selector_id).trigger('onchange');
}

function showHideListItems(ctrl, is_clicked) {
    $.each($('div[data-role="collapsible-set"] div'), function (a, b) {
        var is_collapsed = $('#' + b.id).find('.ui-collapsible-heading-collapsed').length > 0;
        if (b.id.toLowerCase().indexOf(ctrl.id.toLowerCase().substring(2, ctrl.id.length)) > -1) {
            var $search_ctrl = $('#' + b.id).find('input[id^=search]');
            if (!is_collapsed)
                $('#' + b.id).attr('data-collapsed', "false");
        }
        else
            $('#' + b.id).attr('data-collapsed', "true");

        if (b.id != "")
            $('#' + b.id + ' ul').listview('refresh');
    });
}

function fnPostageInvoice(values_storeid, values_quantity) {
    //var newwindow = window.open("../Reports/83549 May Pizza Postage Breakout[1].xlsx", "window2", "")
    var frmSubmit = "<form name='frmPost' method='post' id='frmPost' action='' target='_blank'>";
    frmSubmit += "<input type='hidden' name='frmPage' id='frmPage' value='optInStatus' />";
    frmSubmit += "<input type='hidden' name='invoiceType' id='invoiceType' value='postage' />";
    frmSubmit += "<input type='hidden' name='parameterStoreId' id='parameterStoreId' value='" + values_storeid + "' />";
    frmSubmit += "<input type='hidden' name='parameterQuantity' id='parameterQuantity' value='" + values_quantity + "' />";
    frmSubmit += "<input type='hidden' name='estimatedPostage' id='estimatedPostage' value='0.164' />";
    frmSubmit += "<input type='hidden' name='estimatedInHome' id='estimatedInHome' value='" + gOutputData.inHomeDate + "' />";
    frmSubmit += "<input type='hidden' name='jobName' id='jobName' value='" + gOutputData.jobName + "' />";
    frmSubmit += "</form>";
    $("#tmpDiv").html(frmSubmit);
    document.frmPost.action = '../pages/caseysReports.aspx';
    document.frmPost.submit();
    return false;
}
function fnBillingInvoice(values_storeid, values_quantity, values_store_name) {
    //var newwindow = window.open("../Reports/Invoice Q1 Postcard April 2013[1].xlsx", "window2", "")
    var frmSubmit = "<form name='frmPost' method='post' id='frmPost' action='' target='_blank'>";
    frmSubmit += "<input type='hidden' name='frmPage' id='frmPage' value='optInStatus' />";
    frmSubmit += "<input type='hidden' name='invoiceType' id='invoiceType' value='billing' />";
    frmSubmit += "<input type='hidden' name='parameterStoreId' id='parameterStoreId' value='" + values_storeid + "' />";
    frmSubmit += "<input type='hidden' name='parameterQuantity' id='parameterQuantity' value='" + values_quantity + "' />";
    frmSubmit += "<input type='hidden' name='parameterStoreName' id='parameterStoreName' value='" + values_store_name + "' />";
    frmSubmit += "<input type='hidden' name='estimatedService' id='estimatedService' value='0.064' />";
    frmSubmit += "<input type='hidden' name='estimatedPostage' id='estimatedPostage' value='0.1431' />";
    frmSubmit += "<input type='hidden' name='estimatedInHome' id='estimatedInHome' value='" + gOutputData.inHomeDate + "' />";
    frmSubmit += "<input type='hidden' name='jobName' id='jobName' value='" + gOutputData.jobName + "' />";
    frmSubmit += "</form>";
    $("#tmpDiv").html(frmSubmit);
    document.frmPost.action = '../pages/caseysReports.aspx';
    document.frmPost.submit();
    return false;
}