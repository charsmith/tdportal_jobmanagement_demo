﻿var mapping = { 'ignore': ["__ko_mapping__"] }
var gLogListUrl = serviceURLDomain + 'api/Logging/' + sessionStorage.facilityId + '/' + jobCustomerNumber + '/' + sessionStorage.jobNumber;
var gShortagesInfo = 'JSON/_shortages.JSON';

var gAdvertizersInfo = 'JSON/advertizers.JSON';
var gProductsInfo = 'JSON/products.JSON';
var gCorrectionsInfo = 'JSON/corrections.JSON';

gServersInfoUrl = serviceURLDomain + "api/DashboardServer_diskSpace";
var gFacilityServiceUrl = "../JSON/_facilityList.JSON";
var gPrintStreamConnectionsURL = serviceURLDomain + "api/DashboardServer_getMonitorList";
var primaryFacilityData;
var shortagesInfo = [];
var allAdvertizers = []
var allProducts = []
var advertizers = [];
var products = [];

var allCorrections = [];
var zips = [];

$.getJSON(gShortagesInfo, function (result) {
    shortagesInfo = result;
});

$.getJSON(gAdvertizersInfo, function (result) {
    $.each(result.advertizers, function (key, val) {
        if (key == 0)
            advertizers.push({ 'advertizer': 'Select', 'advertizerCode': '' });
        advertizers.push({ 'advertizer': val.advertiser, 'advertizerCode': val.advertiser });
    });
    allAdvertizers = result;
});

$.getJSON(gProductsInfo, function (result) {
    //$.each(result.advertisers, function (key, val) {
    //    advertizers.push({ 'productId': val.productCode, 'advertizerCode': val.description });
    //});
    //products = result;
    $.each(result.products, function (key, val) {
        if (key == 0)
            products.push({ 'productCode': '', 'product': 'Select' });
        products.push({ 'productCode': val.productCode, 'product': val.description });
    });
    allProducts = result;
});


$.getJSON(gCorrectionsInfo, function (result) {
    var is_zip_found = false;
    $.each(result.corrections, function (key, val) {
        if (key == 0)
            zips.push({ 'zipCode': '', 'zip': 'Select' });
        is_zip_found = false;
        $.each(zips, function (key1, val1) {
            if (val1.zipCode == val.zipCode) {
                is_zip_found = true;
                return false;
            }
        });
        if (!is_zip_found)
            zips.push({ 'zipCode': val.zipCode, 'zip': val.zipCode });
    });
    allCorrections = result;
});

$('#_shortageCorrections').live('pagebeforecreate', function (event) {
    displayMessage('_floorShortages');
    ko.setTemplateEngine(new ko.nativeTemplateEngine);
});

$(document).on('pageshow', '#_shortageCorrections', function (event) {

    try {
        window.setTimeout(function () {
            ko.applyBindings(new shortagesModel());
        }, 1000);
    } catch (e) {
        alert(e);
    }

});

//var shortagesModel = function (print_steam_info) {
//    var self = this;
//    ko.mapping.fromJS(print_steam_info, {}, self);
//    self.name = ko.computed(function () {
//        var facility_name = getTribuneFacilityName(self.facilityId().toString());
//        var computed_name = "";
//        var temp_date = new Date(self.monitorDateTime());
//        computed_name = facility_name + " (" + self.psUser() + ") - " + formatDate(self.monitorDateTime());
//        return computed_name;
//    });
//    self.xmlCreator = ko.computed(function () {
//        var is_active = self.isXmlCreatorActive();
//        return (is_active) ? 'Active' : 'In-active';
//    });
//};

var shortagesModel = function () {
    var self = this;
    self.shortagesInfo = ko.observableArray([]);
    self.shortages = ko.observableArray([]);


    self.corrections = ko.observableArray([]);
    self.selectedProducedFor = ko.observable('');
    self.selectedIssueDate = ko.observable('');
    self.logData = ko.observable({});
    self.tempEditLog = ko.observable({});

    self.advertizers = advertizers;
    self.versions = ko.observableArray([]);
    self.zipCodes = zips;
    self.zoneGroups = ko.observableArray([]);
    self.products = products;

    self.zones = ko.observableArray([]);
    self.channels = ko.observableArray([]);
    self.correctionProducts = ko.observableArray([]);

    self.isNewEdit = ko.observable('');

    self.selectedAdvertizer = ko.observable('');

    //$.each(shortagesInfo, function (shortageType, value) {
    //    if (shortageType == 0) {
    //        $.each(value.midweekTribLocalValues, function (key, val) {
    //            self.shortages.push(ko.mapping.fromJS(val));
    //        });
    //    }
    //});

    self.producedForChanged = function () {
        //self.selectedProducedFor
        self.shortages([]);
        $.each(shortagesInfo, function (shortageType, value) {
            if (value[self.selectedProducedFor()]) {
                $.each(value[self.selectedProducedFor()], function (key, val) {
                    self.shortages.push(ko.mapping.fromJS(val));
                });
            }
        });
    };

    self.logCorrections = function (data, event) {
        var ele = event.target || event.currentTarget;
        var parent_context = ko.contextFor($(ele)[0]).$parent;
        self.logData({});
        self.correctionProducts.removeAll();
        self.zones.removeAll();
        self.channels.removeAll();

        if ($(ele).attr('data-btnType') == "add") {
            var new_log_shortage = {};
            new_log_shortage["zipCode"] = "";
            new_log_shortage["zone"] = "";
            new_log_shortage["channel"] = "";
            new_log_shortage["productCode"] = "";
            new_log_shortage["description"] = "";
            new_log_shortage["requiredAmount"] = "";
            new_log_shortage["insertedAmount"] = "";
            new_log_shortage["adjustedAmount"] = "";
            new_log_shortage["correctionType"] = "";
            self.logData(ko.mapping.fromJS(new_log_shortage));
            self.isNewEdit('new')
        }
        else if ($(ele).attr('data-btnType') == "edit") {
            //self.logData(data);
            //var temp = ko.toJS(data, mapping);
            //self.tempEditLog(temp);
            //self.isNewEdit('edit');
        }

        $('#dvLogInfo').trigger('create');
        $('#popupLogCorrections').popup('open');

    };

    self.correctionTypeChanged = function () {
        $('#dvLogInfo').trigger('create');
    };

    self.cancelLog = function () {
        //if (self.selectedProducedFor().indexOf('mid') > -1) {
        self.logData().zipCode(self.tempEditLog().zipCode);
        self.logData().zone(self.tempEditLog().zone);
        self.logData().channel(self.tempEditLog().channel);
        self.logData().productCode(self.tempEditLog().productCode);
        self.logData().description(self.tempEditLog().description);
        self.logData().requiredAmount(self.tempEditLog().requiredAmount);
        self.logData().insertedAmount(self.tempEditLog().insertedAmount);
        self.logData().adjustedAmount(self.tempEditLog().adjustedAmount);
        self.logData().correctionType(self.tempEditLog().correctionType);
        //}
        //else if (self.selectedProducedFor().indexOf('mid') == -1) {
        //    self.logData().productCode(self.tempEditLog().productCode);
        //    self.logData().description(self.tempEditLog().description);
        //    self.logData().zipCode(self.tempEditLog().zipCode);
        //    self.logData().zoneGroup(self.tempEditLog().zoneGroup);
        //    self.logData().shortageAmt(self.tempEditLog().shortageAmt);
        //    self.logData().comments(self.tempEditLog().comments);
        //}
        self.tempEditLog({});
        $('#popupLogCorrections').popup('close');
    };

    self.logCorrection = function () {
        var temp = ko.mapping.toJS(self.logData, mapping);
        self.corrections.push(ko.mapping.fromJS(temp));
        self.logData({});
        self.tempEditLog({});
        $('#popupLogCorrections').popup('close');
    };

    self.zipCodeChanged = function () {
        //self.logData().advertiser
        self.zones.removeAll();
        $.each(allCorrections.corrections, function (key, val) {
            if (key == 0) {
                self.zones.push({ 'zone': 'Select', 'zoneCode': '' });
            }
            if (val.zipCode.toLowerCase() == self.logData().zipCode().toLowerCase()) {
                var is_zone_found = false;
                var is_channel_found = false;
                var is_correctionsProduct_found = false;
                $.each(self.zones(), function (key1, val1) {
                    if (val1.zoneCode.toLowerCase() == val.zone.toLowerCase()) {
                        is_zone_found = true;
                        return false;
                    }
                });

                if (!is_zone_found)
                    self.zones.push({ 'zone': val.zone, 'zoneCode': val.zone });
            }
        });
    };

    self.zoneChanged = function () {
        self.channels.removeAll();
        $.each(allCorrections.corrections, function (key, val) {
            if (key == 0) {
                self.channels.push({ 'channel': 'Select', 'channelCode': '' });
            }
            if (self.logData().zone() != undefined && val.zone.toLowerCase() == self.logData().zone().toLowerCase()) {
                var is_channel_found = false;
                //var is_correctionsProduct_found = false;
                $.each(self.channels(), function (key1, val1) {
                    if (val1.channelCode.toLowerCase() == val.channel.toLowerCase()) {
                        is_channel_found = true;
                        return false;
                    }
                });
                if (!is_channel_found)
                    self.channels.push({ 'channel': val.channel, 'channelCode': val.channel });
            }
        });
    };

    self.channelChanged = function () {
        if (self.logData().channel() != undefined) {
            self.correctionProducts.removeAll();
            $.each(allCorrections.corrections, function (key, val) {
                if (key == 0) {
                    self.correctionProducts.push({ 'product': 'Select', 'productCode': '' });
                }
                if (val.channel.toLowerCase() == self.logData().channel().toLowerCase()) {
                    var is_channel_found = false;
                    //var is_correctionsProduct_found = false;
                    $.each(self.correctionProducts(), function (key1, val1) {
                        if (val1.productCode.toLowerCase() == val.product.toLowerCase()) {
                            is_channel_found = true;
                            return false;
                        }
                    });
                    if (!is_channel_found)
                        self.correctionProducts.push({ 'product': val.product, 'productCode': val.product, 'description': val.description });
                }
            });
        }
    };


    self.productChanged = function (data, e) {
        var ctrl = e.tatgetElement || e.target || e.currentTarget;
        if (self.logData().productCode() != undefined) {
            $.each(allCorrections.corrections, function (key, val) {
                if (val.product.toLowerCase() == self.logData().productCode().toLowerCase()) {
                    //$.each(self.correctionProducts(), function (key, val) {
                    //    if (val.productCode == $(ctrl).val()) {
                            
                    //        return false;
                    //    }
                    //});
                    self.logData().description(val.description);
                    self.logData().requiredAmount(val.scheduledQuantity);
                    self.logData().insertedAmount(val.completedQuantity);
                    self.logData().adjustedAmount(val.adjustment);
                }
            });
        }
    };

    //Rediret to System Dashboard.
    self.redirectTofloorShortages = function (data, e) {
        window.setTimeout(function () {
            window.location.href = '../shortagesAndCorrections/floorShortages.html';
        }, 500);
    };
    //Rediret to System Dashboard.
    self.redirectToShortageCorrections = function (data, e) {
        window.setTimeout(function () {
            window.location.href = '../shortagesAndCorrections/shortageCorrections.html';
        }, 500);
    };
    self.showHomePage = function () {
        window.location.href = "../index.htm";
    }

    self.clickAction = function () {
        $('#popupMenu').popup('close');
        window.setTimeout(function () {
            $('#popupNLogRetrieval').popup('open');
        }, 1000);
    };
};

