﻿var mapping = { 'ignore': ["__ko_mapping__"] }
var gLogListUrl = serviceURLDomain + 'api/Logging/' + sessionStorage.facilityId + '/' + jobCustomerNumber + '/' + sessionStorage.jobNumber;
var gShortagesInfo = 'JSON/_shortages.JSON';

var gAdvertizersInfo = 'JSON/advertizers.JSON';
var gProductsInfo = 'JSON/products.JSON';

gServersInfoUrl = serviceURLDomain + "api/DashboardServer_diskSpace";
var gFacilityServiceUrl = "../JSON/_facilityList.JSON";
var gPrintStreamConnectionsURL = serviceURLDomain + "api/DashboardServer_getMonitorList";
var primaryFacilityData;
var shortagesInfo = [];
var allAdvertizers = [];
var allProducts = [];
var advertizers = [];
var products = [];

$.getJSON(gShortagesInfo, function (result) {
    shortagesInfo = result;
});

$.getJSON(gAdvertizersInfo, function (result) {
    $.each(result.advertizers, function (key, val) {
        if (key == 0)
            advertizers.push({ 'advertizer': 'Select', 'advertizerCode': '' });
        var is_advertiser_found = false;
        $.each(advertizers, function (key1, val1) {
            if (val1.advertizerCode.toLowerCase() == val.advertiser.toLowerCase()) {
                is_advertiser_found = true;
                return false;
            }
        });
        if (!is_advertiser_found)
            advertizers.push({ 'advertizer': val.advertiser, 'advertizerCode': val.advertiser });
    });
    allAdvertizers = result;
});

$.getJSON(gProductsInfo, function (result) {
    //$.each(result.advertisers, function (key, val) {
    //    advertizers.push({ 'productId': val.productCode, 'advertizerCode': val.description });
    //});
    //products = result;
    $.each(result.products, function (key, val) {
        if (key == 0)
            products.push({ 'productCode': '', 'product': 'Select' });
        var is_product_found = false;
        $.each(products, function (key1, val1) {
            if (val1.productCode.toLowerCase() == val.productCode.toLowerCase()) {
                is_product_found = true;
                return false;
            }
        });
        if (!is_product_found)
            products.push({ 'productCode': val.productCode, 'product': val.description });
    });
    allProducts = result;
});

$('#_floorShortages').live('pagebeforecreate', function (event) {
    displayMessage('_floorShortages');
    ko.setTemplateEngine(new ko.nativeTemplateEngine);
});

$(document).on('pageshow', '#_floorShortages', function (event) {

    try {
        window.setTimeout(function () {
            ko.applyBindings(new shortagesModel());
        }, 1000);
    } catch (e) {
        alert(e);
    }

});

//var shortagesModel = function (print_steam_info) {
//    var self = this;
//    ko.mapping.fromJS(print_steam_info, {}, self);
//    self.name = ko.computed(function () {
//        var facility_name = getTribuneFacilityName(self.facilityId().toString());
//        var computed_name = "";
//        var temp_date = new Date(self.monitorDateTime());
//        computed_name = facility_name + " (" + self.psUser() + ") - " + formatDate(self.monitorDateTime());
//        return computed_name;
//    });
//    self.xmlCreator = ko.computed(function () {
//        var is_active = self.isXmlCreatorActive();
//        return (is_active) ? 'Active' : 'In-active';
//    });
//};

var shortagesModel = function () {
    var self = this;
    self.shortagesInfo = ko.observableArray([]);

    self.shortages = ko.observableArray([]);
    self.selectedProducedFor = ko.observable('');
    self.selectedIssueDate = ko.observable('');
    self.logData = ko.observable({});
    self.tempEditLog = ko.observable({});

    self.advertizers = advertizers;
    self.versions = ko.observableArray([]);
    self.zipCodes = ko.observableArray([]);
    self.zoneGroups = ko.observableArray([]);
    self.products = products;
    self.isNewEdit = ko.observable('');

    self.selectedAdvertizer = ko.observable('');

    //$.each(shortagesInfo, function (shortageType, value) {
    //    if (shortageType == 0) {
    //        $.each(value.midweekTribLocalValues, function (key, val) {
    //            self.shortages.push(ko.mapping.fromJS(val));
    //        });
    //    }
    //});

    self.producedForChanged = function () {
        //self.selectedProducedFor
        self.shortages([]);
        $.each(shortagesInfo, function (shortageType, value) {
            if (value[self.selectedProducedFor()]) {
                $.each(value[self.selectedProducedFor()], function (key, val) {
                    self.shortages.push(ko.mapping.fromJS(val));
                });
            }
        });
    };

    self.addEditLogClick = function (data, event) {
        var ele = event.target || event.currentTarget;
        var parent_context = ko.contextFor($(ele)[0]).$parent;
        if ($(ele).attr('data-btnType') == "add") {
            var new_log_shortage = {};
            if (self.selectedProducedFor().indexOf('mid') > -1) {
                new_log_shortage["advertiser"] = "";
                new_log_shortage["version"] = "";
                new_log_shortage["zipCode"] = "";
                new_log_shortage["qty"] = "";
                new_log_shortage["zoneGroup"] = "";
                new_log_shortage["comments"] = "";
            }
            else if (self.selectedProducedFor().indexOf('mid') == -1) {
                new_log_shortage["productCode"] = "";
                new_log_shortage["description"] = "";
                new_log_shortage["zipCode"] = "";
                new_log_shortage["zoneGroup"] = "";
                new_log_shortage["shortageAmt"] = "";
                new_log_shortage["comments"] = "";
            }
            self.logData(ko.mapping.fromJS(new_log_shortage));
            self.isNewEdit('new')
        }
        else if ($(ele).attr('data-btnType') == "edit") {
            self.logData(data);
            var temp = ko.toJS(data, mapping);
            self.tempEditLog(temp);
            self.isNewEdit('edit');
        }

        $('#dvLogInfo').trigger('create');
        $('#popupShortageLog').popup('open');

    };

    self.cancelLog = function () {
        if (self.selectedProducedFor().indexOf('mid') > -1) {
            self.logData().advertiser(self.tempEditLog().advertiser);
            self.logData().version(self.tempEditLog().version);
            self.logData().zipCode(self.tempEditLog().zipCode);
            self.logData().qty(self.tempEditLog().qty);
            self.logData().zoneGroup(self.tempEditLog().zoneGroup);
            self.logData().comments(self.tempEditLog().comments);
        }
        else if (self.selectedProducedFor().indexOf('mid') == -1) {
            self.logData().productCode(self.tempEditLog().productCode);
            self.logData().description(self.tempEditLog().description);
            self.logData().zipCode(self.tempEditLog().zipCode);
            self.logData().zoneGroup(self.tempEditLog().zoneGroup);
            self.logData().shortageAmt(self.tempEditLog().shortageAmt);
            self.logData().comments(self.tempEditLog().comments);
        }
        self.tempEditLog({});
        $('#popupShortageLog').popup('close');
    };

    self.logShortage = function () {

        self.shortages.push(self.logData);
        //self.logData({});
        self.tempEditLog({});
        $('#popupShortageLog').popup('close');
    };

    self.advertiserChanged = function () {
        //self.logData().advertiser
        self.versions.removeAll();
        self.zipCodes.removeAll();
        self.zoneGroups.removeAll();
        $.each(allAdvertizers.advertizers, function (key, val) {
            if (key == 0) {
                self.versions.push({ 'version': 'Select', 'versionCode': '' });
                self.zipCodes.push({ 'zip': 'Select', 'zipCode': '' });
                self.zoneGroups.push({ 'zoneGroup': 'Select', 'zoneGroupCode': '' });
            }
            if (val.advertiser.toLowerCase() == self.logData().advertiser().toLowerCase()) {
                var is_version_found = false;
                var is_zc_found = false;
                var is_zg_found = false;
                $.each(self.versions(), function (key1, val1) {
                    if (val1.version.toLowerCase() == val.version.toLowerCase()) {
                        is_version_found = true;
                        return false;
                    }
                });
                $.each(self.zipCodes(), function (key1, val1) {
                    if (val1.zip.toLowerCase() == val.zipCode.toLowerCase()) {
                        is_zc_found = true;
                        return false;
                    }
                });
                $.each(self.zoneGroups(), function (key1, val1) {
                    if (val1.zoneGroup.toLowerCase() == val.zoneGroup.toLowerCase()) {
                        is_zg_found = true;
                        return false;
                    }
                });
                if (!is_version_found)
                    self.versions.push({ 'version': val.version, 'versionCode': val.version });
                if (!is_zc_found)
                    self.zipCodes.push({ 'zip': val.zipCode, 'zipCode': val.zipCode });
                if (!is_zg_found)
                    self.zoneGroups.push({ 'zoneGroup': val.zoneGroup, 'zoneGroupCode': val.zoneGroup });
            }


        });
    };

    self.productChanged = function () {
        //self.logData().advertiser
        self.zipCodes.removeAll();
        self.zoneGroups.removeAll();
        $.each(allProducts.products, function (key, val) {
            if (key == 0) {
                self.zipCodes.push({ 'zip': 'Select', 'zipCode': '' });
                self.zoneGroups.push({ 'zoneGroup': 'Select', 'zoneGroupCode': '' });
            }
            if (val.productCode.toLowerCase() == self.logData().productCode().toLowerCase()) {
                self.logData().description(val.description);
                var is_zc_found = false;
                var is_zg_found = false;
                $.each(self.zipCodes(), function (key1, val1) {
                    if (val1.zip.toLowerCase() == val.zipCode.toLowerCase()) {
                        is_zc_found = true;
                        return false;
                    }
                });
                $.each(self.zoneGroups(), function (key1, val1) {
                    if (val1.zoneGroup.toLowerCase() == val.zoneGroup.toLowerCase()) {
                        is_zg_found = true;
                        return false;
                    }
                });
                if (!is_zc_found)
                    self.zipCodes.push({ 'zip': val.zipCode, 'zipCode': val.zipCode });
                if (!is_zg_found)
                    self.zoneGroups.push({ 'zoneGroup': val.zoneGroup, 'zoneGroupCode': val.zoneGroup });
            }


        });
    };


    //Rediret to System Dashboard.
    self.redirectTofloorShortages = function (data, e) {
        window.setTimeout(function () {
            window.location.href = '../shortagesAndCorrections/floorShortages.html';
        }, 500);
    };
    //Rediret to System Dashboard.
    self.redirectToShortageCorrections = function (data, e) {
        window.setTimeout(function () {
            window.location.href = '../shortagesAndCorrections/shortageCorrections.html';
        }, 500);
    };
    self.showHomePage = function () {
        window.location.href = "../index.htm";
    }

    self.clickAction = function () {
        $('#popupMenu').popup('close');
        window.setTimeout(function () {
            $('#popupNLogRetrieval').popup('open');
        }, 1000);
    };
};

