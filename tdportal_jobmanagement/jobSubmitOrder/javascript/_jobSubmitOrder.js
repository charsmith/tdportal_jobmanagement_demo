﻿jQuery.support.cors = true;
var mapping = { 'ignore': ["__ko_mapping__"] }

//******************** Global Variables Start **************************
var serviceURL = "../jobSubmitOrder/JSON/_submitOrder.JSON";
var customerEmailsURL = "../jobSubmitOrder/JSON/_customerEmails.JSON";
var gTemplateUrlForAll = serviceURLDomain + 'api/Template';
var gCheckoutServiceURL = serviceURLDomain + "api/CheckOut/";
//var gCheckoutServiceURL = serviceURLDomainInternal + "api/CheckOut/";
var gOutputServiceUrl = '../JSON/_jobSetupOutput.JSON';
var hasLoaded = false;
var pageObj;
var pricingInfo = {};
var customerDetailsData = [];
var approversList = [];
var listStoreApprovers = [];
var listStoreApproversCompare = [];

var dontShowHintsAgain = (localStorage.getItem('doNotShowHintsAgain') != undefined && localStorage.getItem('doNotShowHintsAgain') != null && localStorage.getItem('doNotShowHintsAgain') != "" && localStorage.getItem('doNotShowHintsAgain') != "false") ? true : false;
//******************** Page Load Events Start **************************

var pricingText = {
    "155370": "Postage is not included in the totals shown above. A Postage Advance will be emailed when the order has been submitted.<br />Shipping will be added to Standard Class Mail at $0.0205 per piece with a minimum of $150 per order.",
    "105,369,385": "*Total counts may vary by 10-20 pieces.<br />**Some costs not reflected at the time of order submission; e.g. shipping and special order items."
}

$(window).on('popupbeforeposition', 'div:jqmData(role="popup")', function () {
    var notDismissible = $(this).jqmData('dismissible') === false;
    if (notDismissible) {
        $('.ui-popup-screen').off();
    }
});

$.getJSON(customerEmailsURL, function (data) {
    customerDetailsData = data;
});

$('#_jobSubmitOrder').live('pagebeforecreate', function (event) {
    displayNavLinks();
    displayMessage('_jobSubmitOrder');
    loadingImg("_jobSubmitOrder");
    confirmNavigationMessage('_jobSubmitOrder');
    createConfirmMessage('_jobSubmitOrder');
    updateTotalValidationMessage('_jobSubmitOrder');
    if (sessionStorage.pricingListInfo != undefined && sessionStorage.pricingListInfo != null && sessionStorage.pricingListInfo != "") {
        pricingInfo = $.parseJSON(sessionStorage.pricingListInfo);
    }
    showExecuteConfirmationToAdmin();
    window.setTimeout(function () {
        pageObj = new jobSubmitOrder();
        //pageObj.showExecuteConfirmationToAdmin();
        //pageObj.getApproversList();
        pageObj.makeEmails();
    }, 1500);

    //pageObj.displayOrderSummary();
    //if (sessionStorage.approvalCheckoutPrefs == undefined || sessionStorage.approvalCheckoutPrefs == null || sessionStorage.approvalCheckoutPrefs == "")
    //    if (appPrivileges.customerNumber == "203" || appPrivileges.customerNumber == BRIGHTHOUSE || appPrivileges.customerNumber == BBB || appPrivileges.customerNumber == AAG || appPrivileges.customerNumber == TRIBUNEPUBLISHING || appPrivileges.customerNumber == SANDIEGO || jobCustomerNumber == SPORTSAUTHORITY || jobCustomerNumber == WALGREENS || jobCustomerNumber == DATAWORKS || jobCustomerNumber == ALLIED || jobCustomerNumber == REDPLUM || jobCustomerNumber == SAFEWAY || jobCustomerNumber == LOWES || jobCustomerNumber == DCA || jobCustomerNumber == KUBOTA || jobCustomerNumber == OH || jobCustomerNumber == SPORTSKING || jobCustomerNumber == GWA || jobCustomerNumber == REDPLUM || jobCustomerNumber == BUFFALONEWS || jobCustomerNumber == PIONEERPRESS || jobCustomerNumber == EQUINOX || isCWSubmitOrderPreferences())
    //        getPagePreferences('ApprovalCheckout.html', sessionStorage.facilityId, CW);
    //    else
    //        getPagePreferences('ApprovalCheckout.html', sessionStorage.facilityId, appPrivileges.customerNumber);
    //else
    //    managePagePrefs(jQuery.parseJSON(sessionStorage.approvalCheckoutPrefs));

    if (sessionStorage.userRole != 'admin') {
        $('#btnCountApprovalClear').hide();
        $('#btnArtApprovalClear').hide();
        $('#btnExecute').css('visibility', 'hidden');
        $('#btnExecute').css("display", "");
        $('#btnPricingStructure').hide();
    }
    if (hidePricingStructure())
        $('#btnPricingStructure').hide();
    if (hideProductionReleaseButton()) {
        $('#btnProductionRelease').css('display', 'none');
    }
    if (hideConfirmationMailsDiv()) {        
        if (hideSubmitOrder())
            $('#btnSubmitOrder').css('display', 'none');
        //$('#pSubmitInfoMsg').text('Please review your order below. If both artwork and list upload sections have been completed you are ready to submit your order. Click the Submit Order button below and a confirmation and approval email will be sent.');
        window.setTimeout(function loadHints() {
            createDemoHints("jobSubmitOrder");
        }, 50);
        setDemoHintsSliderValue();
        if (hideConfirmationMailsDiv()) {
            //$('#dvOrderSummary').css('display', 'none');
            
            if (sessionStorage.userRole == 'user')
                $('#dvDisplayEmail').css('display', 'none');
        }
    }
    else {
        $('#dvOrderSummary').css('display', 'none');       
    }

    $("#navLinksPanel").on("panelopen", function (event, ui) {
        sessionStorage.isNavPanelOpen = true;
    });

    if (sessionStorage.userRole == 'admin') {
        $('#fsApprovalTypes').css("display", "block"); //display approval checkboxes only for admins
    }
});

$("#configTable").on("tablecreate", function (event, ui) {
    $("#configTable-popup [type=checkbox]:eq(1)") /* second checkbox */
                   .prop("checked", pageObj.wantsActualColumns())            /* uncheck it */
                   .checkboxradio("refresh")          /* refresh UI */
                   .trigger("change");                /* trigger change to update table */
});


$(document).on('pageshow', '#_jobSubmitOrder', function (event) {
    if (hasLoaded) {
        return false;
    }
    hasLoaded = true;
    window.setTimeout(function () {
        pageObj.getJobReadinessStatus("");
        pageObj.makeGData();
        persistNavPanelState();
        $('#sldrShowHints').slider("refresh");
        if (jobCustomerNumber == CASEYS || jobCustomerNumber == CW || jobCustomerNumber == AAG) {
            $('#approveEmailDemoHints p a').text('Turn Hints Off');
        }
        if (!dontShowHintsAgain && (showApproveEmailDemoHintsPopup()) && (sessionStorage.showDemoHints == undefined || sessionStorage.showDemoHints == null || sessionStorage.showDemoHints == "" || sessionStorage.showDemoHints == "on") && (sessionStorage.isSubmitOrderDemoHintsDisplayed == undefined || sessionStorage.isSubmitOrderDemoHintsDisplayed == null || sessionStorage.isSubmitOrderDemoHintsDisplayed == "false")) {
            window.setTimeout(function () {
                $('#approveEmailDemoHints').popup('open', { positionTo: '#txtEmails' });
                sessionStorage.isSubmitOrderDemoHintsDisplayed = true;
            }, 2500);
        }
        disableSaveForClosedJobs();
    }, 1500);
    if (jobCustomerNumber == BBB || jobCustomerNumber == AAG || jobCustomerNumber == TRIBUNEPUBLISHING || jobCustomerNumber == SANDIEGO || jobCustomerNumber == SPORTSAUTHORITY || jobCustomerNumber == WALGREENS || jobCustomerNumber == DATAWORKS || jobCustomerNumber == SAFEWAY || jobCustomerNumber == LOWES || jobCustomerNumber == OH) {// || jobCustomerNumber == SPORTSKING
        $("#tblOrderGrid").css("display", "none");
        $('#tblOrderGrid thead').css('display', 'none');
        $("#tblOrderGridTotals").css("display", "none");
        $('#tblOrderGridTotals thead').css('display', 'none');
		$('#btnPricingStructure').hide();
    }
    //if (sessionStorage.userRole != 'admin') {
    //    $('#btnSaveTop').addClass('ui-first-child');
    //}
    $('#btnSaveTop').addClass('ui-last-child');

    //Hidden continue for all users check later
    $('#btnContinueTop').css('display', 'none');
    $('#btnContinue').css('display', 'none');
    $('#btnPreviewMailing').css('display', 'none'); //PreviewMailing hidden for all customers
    if (appPrivileges.customerNumber == AAG && sessionStorage.userRole == 'admin') {
        //Hidden PreviewMailing for admin aag
        $('#btnPreviewMailing').css('display', 'none');
        $('#btnSaveTop').addClass('ui-first-child');
    }
    var submit_order_msg = "Please review your order below. If both artwork and list upload sections have been completed you are ready to submit your order. Click the Submit Order button below and a confirmation and approval email will be sent.";
    if (jobCustomerNumber == CW) {
        submit_order_msg = 'Please review your order below. Click the Submit Order button below and a confirmation and approval email will be sent.';
    }
    if (jobCustomerNumber == OH) {
        submit_order_msg = 'Please review your order below. If the list and art upload sections have been completed, you are ready to submit your order. Click the Submit Order button and wait for the success message.';
    }
    if (jobCustomerNumber == AAG || jobCustomerNumber == TRIBUNEPUBLISHING || jobCustomerNumber == SANDIEGO || jobCustomerNumber == SPORTSAUTHORITY || jobCustomerNumber == WALGREENS || jobCustomerNumber == AH || jobCustomerNumber == REDPLUM || jobCustomerNumber == SAFEWAY || jobCustomerNumber == LOWES) {
        submit_order_msg = 'Please review your order below. If the list upload section has been completed you are ready to submit your order. Click the Submit Order button and wait for the success message. Once submitted you will receive a confirmation email.';
        if (appPrivileges.roleName == "user") {
            submit_order_msg = "Please review your order below. If all variable areas have been completed you are ready to submit your order. Click the Submit Order button below and a confirmation and approval email will be sent.";
        }
    }
    if (jobCustomerNumber == DCA || jobCustomerNumber == KUBOTA || jobCustomerNumber == SPORTSKING || jobCustomerNumber == GWA || jobCustomerNumber == REDPLUM || jobCustomerNumber == BUFFALONEWS || jobCustomerNumber == PIONEERPRESS || jobCustomerNumber == EQUINOX || showCostAndQuantityWarningMessage()) {
        submit_order_msg = 'Please review your order below.  If the cost and quantity are correct for each location, click "Submit Order".  Confirmation and approval emails will be sent to the appropriate individuals.';
    }
    $('#pSubmitInfoMsg').html(submit_order_msg);
    //    var hint_text = 'When you submit your order, by clicking the "Submit Order" button, an email will be sent to the user placing the order. This will include a Job Summary Report';
    //    hint_text += (jobCustomerNumber == BBB) ? ' and link to the Proofing Module to review your artwork' : '.';
    //    $('#hintText').html(hint_text);

    getNextPageInOrder();
    if (showTotalsText()) {
        $('#spTotalsText').html("**Totals");
        //$('#spScottsText').css('display', 'block');        
    }
    $.each(pricingText, function (key, val) {
        if (key.indexOf(jobCustomerNumber) > -1) {
            $('#spPricingText').html(val);
            $('#spPricingText').css('display', 'block');
            return false;
        }
    });

    $('#navLinksPanel').bind('panelopen', function () {
        window.setTimeout(function () {
            $('#tblOrderGridTotals').width($('#tblOrderGrid').width());
            $.each($('#tblOrderGrid thead tr th'), function (key, val) {
                $($('#tblOrderGridTotals thead tr th')[key]).width($(this).width());
            });
        }, 200);
    });
    $('#navLinksPanel').bind('panelclose', function () {
        window.setTimeout(function () {
            $('#tblOrderGridTotals').width($('#tblOrderGrid').width()); 
            $.each($('#tblOrderGrid thead tr th'), function (key, val) {
                $($('#tblOrderGridTotals thead tr th')[key]).width($(this).width());
            });
        }, 200);
    });
    createPlaceHolderforIE();
    $('.ui-table-columntoggle-btn').on('click', function (event, ui) {
        window.setTimeout(function () {
            $("#configTable-popup [type=checkbox]:eq(1)").bind('change', function () {
                if (this.checked)
                    pageObj.wantsProductionCostPerPiece(true);
                else
                    pageObj.wantsProductionCostPerPiece(false);
            });
        }, 500);
    });
});

function showExecuteConfirmationToAdmin() {
    var msg_box = '';
    msg_box = '<div data-role="popup" data-position-to="window" data-transition="pop"  id="confirmAdmin" data-overlay-theme="d" data-history="false" data-theme="c" data-dismissible="false" style="max-width:520px;" class="ui-corner-all">' +
                '<div data-role="header" data-theme="d" class="ui-corner-top" id="dialogHeader">' +
                '	<h1>Warning!</h1>' +
                '</div>' +
                '<div data-role="content" data-theme="d" class="ui-corner-bottom ui-content">' +
                '<div id="dvMsg"></div>' +
                '	<div align="right"><a href="#" data-role="button" data-inline="true" data-mini="true" id="btnCancelPopup"  data-theme="a"  onclick="$(\'#confirmAdmin\').popup(\'close\') ">Cancel</a>' +
                '	<a href="#" data-role="button" data-inline="true" data-mini="true"  id="btnContinuePopup" data-theme="a" data-transition="flow" onclick="ko.contextFor(this).$data.getJobReadinessStatus(\'submit\');" >Submit Order</a></div>' +
                '</div>' +
            '</div>';
    $("#_jobSubmitOrder").append(msg_box);
};

function updateTotalValidationMessage(page_name) {
    var msg_box = '';
    msg_box = '<div data-role="popup" data-position-to="window" data-transition="pop"  id="popupUpdateTotalsValidation" data-overlay-theme="d" data-theme="c" style="text-align:left;max-width:500px;" class="ui-corner-all" data-history="false">';
    msg_box += '<div data-role="header" data-theme="d" class="ui-corner-top" id="navdialogbox">';
    msg_box += '<h1>Confirm</h1>';
    msg_box += '</div>';
    msg_box += '<div data-role="content" data-theme="c" class="ui-corner-bottom ui-content">';
    msg_box += '<div id="confirmValidationMsg"></div>';
    msg_box += '<div align="right">';
    msg_box += '<a href="#" data-role="button" data-inline="true" data-theme="a" id="cancelBut" data-mini="true" onclick="$(\'#popupUpdateTotalsValidation\').popup(\'close\');">Cancel</a>';
    msg_box += '<a href="#" data-role="button" data-inline="true" data-theme="a" id="updateTotalsBut" data-mini="true" data-bind="click:$root.updateGridData">Update Totals</a>';
    msg_box += '</div>';
    msg_box += '</div></div>';
    $("#" + page_name).append(msg_box);
}

//Converts the first letter into small letter in the given string.
String.prototype.initSmall = function () {
    return this.replace(/(?:^|\s)[A-Z]/g, function (m) {
        return m.toLowerCase();
    });
};
//******************** Page Load Events End **************************