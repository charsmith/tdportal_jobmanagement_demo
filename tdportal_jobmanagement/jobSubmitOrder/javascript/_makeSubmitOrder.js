﻿var makeSubmitOrder = function (self) {

    window.onresize = function () {
        window.setTimeout(function () {
            $('#tblOrderGridTotals').width($('#tblOrderGrid').width());
            $.each($('#tblOrderGrid thead tr th'), function (key, val) {
                $($('#tblOrderGridTotals thead tr th')[key]).width($(this).width());
            });
        }, 100);
    }

    self.getPricing = function (qty) {
        var price = "0.000";
        $.each(pricingInfo, function (key1, val1) {
            price = "0.000";
            $.each(val1, function (key2, val2) {
                if (parseFloat(qty) >= parseFloat(val2.minCount.replace(',', '')) && parseFloat(qty) <= parseFloat(val2.maxCount.replace(',', ''))) {
                    price = val2.value;
                    return false;
                }
                else if (val2.maxCount == "unbound" && parseFloat(qty) >= parseFloat(val2.minCount.replace(',', ''))) {
                    price = val2.value;
                    return false;
                }
            });
            if (parseFloat(price) > 0.000)
                return false;

        });
        return ((price.toString().indexOf('$') == -1) ? '$' : '') + price;
    }
    self.makeGData = function () {
        var job_number = getSessionData("jobNumber");
        if (jobCustomerNumber != AAG) {
            getCORS(serviceURLDomain + "api/JobSummary_get/" + appPrivileges.facility_id + "/" + appPrivileges.customerNumber + "/" + job_number, null, function (data) {
                /* Need to remove after the ANDERSEN demo START*/
                //if (jobCustomerNumber == ANDERSEN || jobCustomerNumber == SHERWINWILLIAMS)
                //   data.wantsEstimateColumns = 0;
                /* Need to remove after the demo START*/
                self.wantsActualColumns(((data.wantsActualColumns == 1) ? true : false));
                self.wantsEstimateColumns(((data.wantsEstimateColumns == 1) ? true : false));
                if (jobCustomerNumber == SPORTSKING)
                    self.wantsEstimateColumns(false);
                self.wantsPostageCostPerPiece(((data.wantsPostageCostPerPiece == 1) ? true : false));
                if (data.wantsActualColumns == 1) {
                    self.actPriority('1');
                    self.estPriority('2');
                }
                else if (data.wantsEstimateColumns == 1) {
                    self.estPriority('1');
                    self.actPriority('2');
                }
                else if ((data.wantsActualColumns == 1 && data.wantsEstimateColumns == 1) || (data.wantsActualColumns == 0 && data.wantsEstimateColumns == 0)) {
                    self.estPriority('1');
                    self.actPriority('2');
                }
                var estQty_found = false;
                var acttQty_found = false;

                if (data.summaryStoreList) {
                    $.each(data.summaryStoreList, function (key, val) {
                        if (job_number < -1 && (val.actualQuantity == null || val.actualQuantity == 0) && (data.wantsActualColumns == 1 && data.wantsEstimateColumns == 0)) {
                            val.actualQuantity = val.estimatedQuantity;
                            if (key == 0) {
                                self.isActualQtyNull(1);
                            }
                        }
                        else
                            if (key == 0) {
                                self.isActualQtyNull(0);
                            }

                        $.each(data.storeIdToPkDict, function (storeKey, storeVal) {
                            if (val.storePk == storeVal) {
                                val["storeId"] = storeKey;
                                return false;
                            }

                        });

                        if (!val.tblSummaryAdditionalExpenses || val.tblSummaryAdditionalExpenses.length == 0) {
                            val.tblSummaryAdditionalExpenses = [{
                                expenseName: "Additional Expense",
                                costType: "",
                                cost: "",
                                applyTo: ""
                            }];
                        }
                        else {
                            $.each(val.tblSummaryAdditionalExpenses, function (key1, val1) {
                                if (!val1.applyTo)
                                    val1["applyTo"] = "";

                            });
                        }
                        if (val.estimatedQuantity != null && val.estimatedQuantity > 0)
                            estQty_found = true;
                        if (val.actualQuantity != null && val.actualQuantity > 0)
                            acttQty_found = true;
                        val.templateName = self.templateName();
                        self.orderStoreIds.push(val['storeId']);
                        //self.calculateSubTotals(val);

                        var act_postage_cost_per_piece = 0.000;
                        if ((val.actualTotalPostage != undefined && val.actualTotalPostage != null) && val.actualQuantity != null && val.actualQuantity > 0)
                            act_postage_cost_per_piece = val.actualTotalPostage / val.actualQuantity;
                        val["actualPostageCostPerPiece"] = parseFloat(act_postage_cost_per_piece);

                    });

                    if (!estQty_found && !acttQty_found)
                        self.wantsProductionCostPerPiece(false);
                    else if (estQty_found || acttQty_found)
                        self.wantsProductionCostPerPiece(true);

                    self.getLoadStoreApprovers();

                    self.gData = data;
                    self.loadSubmitOrder();
                    $("#tblOrderGrid").css("display", "block");
                    $("#tblOrderGrid").css("display", "");
                    ko.applyBindings(self);

                    $("#tblOrderGrid").table("refresh");
                    $("#tblOrderGridTotals").table("refresh");
                    if (self.listStoreApprovers != undefined && self.listStoreApprovers().length > 0) {
                        $('#dvStoreApproverContent').trigger('create');
                        $('#dvStoreApproverContent').css('border-top-left-radius', '2px');
                        $('#dvStoreApprovers').css('display', 'block');
                    }

                    self.makeGOutputData();
                    if (parseInt(job_number) > -1 && featurePostageInvoice())
                        self.isOrderSubmitted(true);
                    $("#tblOrderGrid").find('[type=text]').textinput();
                    $("#configTable").find('[type=text]').textinput();
                    $('#dvConfirmationEmails').trigger('create');
                    self.hideActuals = ko.computed(function () {
                        return self.wantsActualColumns();
                    });
                    window.setTimeout(function () {
                        $('#tblOrderGridTotals').width($('#tblOrderGrid').width());
                        $.each($('#tblOrderGrid thead tr th'), function (key, val) {
                            $($('#tblOrderGridTotals thead tr th')[key]).width($(this).width());
                        });
                        $('#configTable').table('refresh');
                        $("#configTable-popup [type=checkbox]:eq(1)").prop("checked", self.wantsActualColumns()).checkboxradio("refresh").trigger("change");
                        window.setTimeout(function () {
                            if ($("#configTable-popup [type=checkbox]:eq(0)")[0]) {
                                if (!self.wantsEstimateColumns()) {
                                    $("#configTable-popup [type=checkbox]:eq(0)").prop("checked", self.wantsEstimateColumns()).checkboxradio("refresh").trigger("change");
                                    $("#configTable-popup [type=checkbox]:eq(0)").parent().addClass('ui-disabled');
                                    if ($("#configTable-popup [type=checkbox]:eq(0)")[0])
                                        $("#configTable-popup [type=checkbox]:eq(0)")[0].disabled = true;
                                } else {
                                    $("#configTable-popup [type=checkbox]:eq(0)").prop("checked", self.wantsEstimateColumns()).checkboxradio("refresh").trigger("change");
                                }
                            }
                        }, 500);
                        $('.th-groups th').css('background-color', $('.ui-bar-a').css('background-color'));
                        $('.th-groups th').css('color', $('.ui-bar-a').css('color'));
                        $('.th-groups th').css('text-shadow', $('.ui-bar-a').css('text-shadow'));
                        $('#configTable').table('refresh');
                    }, 100);


                } else {
                    self.gData = data;
                    self.loadSubmitOrder();
                    ko.applyBindings(self);
                    $("#tblOrderGrid").css("display", "none");
                    $("#tblOrderGridTotals").css("display", "none");
                }
            }, function (error_response) {
                showErrorResponseText(error_response, true);
            });
        } else {
            //self.gData = data;
            self.loadSubmitOrder();
            self.makeGOutputData();
            ko.applyBindings(self);
            $("#tblOrderGrid").css("display", "none");
            $("#tblOrderGridTotals").css("display", "none");
        }
    };

    self.getLoadStoreApprovers = function () {
        //populate Co-op Approver data
        if ((isLoadStoreStatusApprover()) && self.orderStoreIds.length > 0) {
            var job_customer_number = (sessionStorage.jobCustomerNumber != undefined && sessionStorage.jobCustomerNumber != null && sessionStorage.jobCustomerNumber != "") ? sessionStorage.jobCustomerNumber : appPrivileges.customerNumber;
            var p_coop_approval_url = serviceURLDomain + "api/StoreStatus_approver/" + sessionStorage.facilityId + "/" + job_customer_number;
            postCORS(p_coop_approval_url, JSON.stringify(self.orderStoreIds), function (response) {
                listStoreApprovers = response.assignedStoreApproverList;
                listStoreApproversCompare = response.assignedStoreApproverList;
                if (response.possibleApproversDict != undefined) {
                    $.each(response.possibleApproversDict, function (key, val) {
                        approversList.push({
                            "approverName": key,
                            "approverEmail": val
                        });
                    });
                }
            }, function (response_error) {
                showErrorResponseText(response_error, false);
            });
        }
    }

    self.makeGOutputData = function () {
        if (sessionStorage.jobSetupOutput == undefined && sessionStorage.jobSetupOutput == undefined)
            $.getJSON(gOutputServiceUrl, function (dataOutput) {
                self.gOutputData = dataOutput;
                self.gOutputData.checkOutAction = self.gData.checkOutAction;
            });
        else {
            self.gOutputData = jQuery.parseJSON(sessionStorage.jobSetupOutput);
            self.isEstimatedQuantityVisible((self.gOutputData.jobTypeId >= 71 && self.gOutputData.jobTypeId <= 74) ? false : true)
            var is_firsttime = false;
            if (self.gOutputData.checkOutAction == undefined) { self.gOutputData["checkOutAction"] = {}; is_firsttime = true; }
            self.gOutputData.checkOutAction = (self.gData) ? self.gData.checkOutAction : {};

            if (!is_firsttime) {
                updateStorePaymentInfoInSession();
                self.gOutputData = jQuery.parseJSON(sessionStorage.jobSetupOutput);
                if (self.gData) {
                    self.gData.checkOutAction = self.gOutputData.checkOutAction;
                    if (self.gData.checkOutAction.storePaymentInfoList != undefined && self.gData.checkOutAction.storePaymentInfoList != null && self.gData.checkOutAction.storePaymentInfoList != "") {
                        $.each(self.gData.checkOutAction.storePaymentInfoList, function (key, val) {
                            $.each(val, function (key1, val1) {
                                if (key1.indexOf('Pretty') > -1) {
                                    if (val1.indexOf('$') == -1)
                                        self.gData.checkOutAction.storePaymentInfoList[key][key1] = '$' + val1;
                                    if (val1.indexOf('.00') == -1)
                                        self.gData.checkOutAction.storePaymentInfoList[key][key1] = self.gData.checkOutAction.storePaymentInfoList[key][key1] + '.00';

                                }
                            });
                        });
                    }
                }
            }

            if (sessionStorage.jobSetupOutputCompare != undefined) {
                var goutput_compare_data = jQuery.parseJSON(sessionStorage.jobSetupOutputCompare);
                goutput_compare_data.checkOutAction = self.gOutputData.checkOutAction;
                sessionStorage.jobSetupOutputCompare = JSON.stringify(goutput_compare_data);
            }
        }
        delete self.gOutputData.checkoutAction; //TODO: to be discussed with Jim. We are getting this attribute in templateOnchange web service

        //if (self.gOutputData.modulesAction != undefined && self.gOutputData.modulesAction != null && self.gOutputData.modulesAction != "" && self.gOutputData.modulesAction.modulesDict != null && self.gOutputData.modulesAction.modulesDict != null && self.gOutputData.modulesAction.modulesDict != "") {
        //    //getCustNavLinks();
        //    //createNavLinks();
        //    displayNavLinks();
        //}
        //else {
        //    displayNavLinks();
        //}
        if (self.gOutputData != null) {
            getBubbleCounts(self.gOutputData);
        }
       // if (appPrivileges.customerNumber != KUBOTA)
            self.loadPricingStructure();

        self.displayOrderSummary();
    };

    self.loadPricingStructure = function () {
        if (sessionStorage.templates == undefined) {
            var templates_list_url = gTemplateUrlForAll + "/" + sessionStorage.facilityId + "/" + jobCustomerNumber + "/" + "1";
            getCORS(templates_list_url, null, function (data) {
                sessionStorage.templates = JSON.stringify(data);
            }, function (error_response) {
                showErrorResponseText(error_response, true);
            });
        }
        if (sessionStorage.templates != undefined && sessionStorage.templates != null) {
            var template_data = jQuery.parseJSON(sessionStorage.templates);
            var pricing_info = '<table style="padding-top:5px;text-align:middle;">'
            $.each(template_data, function (key, val) {
                if (template_data[key].value == self.gOutputData.templateName) {
                    self.templateName(template_data[key].text);
                    pricing_info += "<tr><td align='left' colspan=4 style='font-weight:bold'>" + template_data[key].text + "</td></tr>";
                    if (template_data[key].pricingList != undefined && template_data[key].pricingList != null) {
                        $.each(template_data[key].pricingList, function (key1, val1) {
                            pricing_info += "<tr>";
                            if (template_data[key].pricingList.length == 1) {
                                pricing_info += "<td align='left'>" + val1.value + ' each' + "</td>";
                            }
                            else {
                                pricing_info += "<td align='right'>" + val1.minCount + ((val1.maxCount == '') ? '+' : '') + "</td>";
                                pricing_info += "<td>" + ((val1.maxCount != '') ? ' - ' : '') + "</td>";
                                pricing_info += "<td align='right'>" + val1.maxCount + "</td>";
                                pricing_info += "<td align='right'>&nbsp;&nbsp;" + val1.value + ' each' + "</td>";
                            }
                            pricing_info += "</tr>";
                        });
                    }
                    return false;
                }
            });
            pricing_info += '</table>'
            self.tieredPricing(pricing_info);
            var biz_rules = [
                "Production CPP is calculated by first finding the tiered cost based on quantity (see below).The base Production cost then has a Data Processing cost ($0.004) added along with Shipping costs ($0.02150) for Standard class mailings.",
                "Postage CPP is set based on the Mail Class value in Piece Attributes. Either $0.372 for First Class or $0.217 for Standard Class.",
                "The total of the Postage CPP and Production CPP are added and then multiplied by the quantity to provide the Subtotal for each location.These Subtotals are then added to get the Total."
            ];
            $.each(biz_rules, function (key, val) {
                self.listBizRules.push(val);
            });
        }
    };

    //to get Customer email JSON
    self.makeEmails = function () {
        //$.getJSON(customerEmailsURL, function (data) {
        //self.customerDetailsData = data;
        self.customerDetailsData = customerDetailsData;
        //});
    };

    self.previewMailing = function () {
        if (appPrivileges.customerNumber == DCA || appPrivileges.customerNumber == KUBOTA || appPrivileges.customerNumber == SPORTSKING || appPrivileges.customerNumber == GWA) {//Todo: Need to update pdf Kubota customer 
            encodedFileName = "DCA%2041941%20Jupiter%20Dental%20_Postcard_Dr.%20Vera_MECH.pdf";
        }
        if (appPrivileges.customerNumber == AAG) {
            encodedFileName = "TCA_10232_AAG_Lunch_Learn_2_Presenters-2_Events_Lenfers_Culbreth_Let.pdf";
        } else if (appPrivileges.customerNumber == ALLIED) {
            encodedFileName = "GW_Berkheimer_Jansen.pdf";
        }
        getNextPageInOrder();
        var preview_proof = "../jobApproval/previewMailing.html?fileName=" + encodedFileName + "&jobNumber=" + sessionStorage.jobNumber;
        window.location.href = preview_proof;
    }

    self.getApproversList = function () {
        var approvers_list_url = serviceURLDomain + "api/Tagging_approvers/" + sessionStorage.facilityId + "/" + jobCustomerNumber + "/" + self.jobNumber + "/null";
        getCORS(approvers_list_url, null, function (response_data) {
            self.approversList = (response_data != undefined && response_data != null && response_data != "" && response_data.Approvers != undefined && response_data.Approvers != null && response_data.Approvers != "" && Object.keys(response_data.Approvers).length > 0) ? response_data.Approvers : [];
            if (self.approversList.length == 0) {
                self.approversList = { "artApprover": "", "listViewer": "", "artViewer": "", "listApprover": "" };
            }
            if (jobCustomerNumber == ALLIED) {
                response_data.Approvers.artApprover = "admin_al";
                response_data.Approvers.artViewer = "admin_al";
            }
            if (self.approversList["artViewer"] == undefined) self.approversList["artViewer"] = "";
            if (self.approversList["listViewer"] == undefined) self.approversList["listViewer"] = "";
            if (self.approversList["artApprover"] == undefined) self.approversList["artApprover"] = "";
            if (self.approversList["listApprover"] == undefined) self.approversList["listApprover"] = "";
        }, function (error_response) {
            showErrorResponseText(error_response, true);
        });
    };

    //self.showExecuteConfirmationToAdmin = function () {
    //    var msg_box = '';
    //    msg_box = '<div data-role="popup" data-position-to="window" data-transition="pop"  id="confirmAdmin" data-overlay-theme="d" data-history="false" data-theme="c" data-dismissible="false" style="max-width:520px;" class="ui-corner-all">' +
    //                '<div data-role="header" data-theme="d" class="ui-corner-top" id="dialogHeader">' +
    //                '	<h1>Warning!</h1>' +
    //                '</div>' +
    //                '<div data-role="content" data-theme="d" class="ui-corner-bottom ui-content">' +
    //                '<div id="dvMsg"></div>' +
    //                '	<div align="right"><a href="#" data-role="button" data-inline="true" data-mini="true" id="btnCancelPopup"  data-theme="a"  onclick="$(\'#confirmAdmin\').popup(\'close\') ">Cancel</a>' +
    //                '	<a href="#" data-role="button" data-inline="true" data-mini="true"  id="btnContinuePopup" data-theme="a" data-transition="flow" onclick="ko.contextFor(this).$data.getJobReadinessStatus(\'submit\');" >Submit Order</a></div>' +
    //                '</div>' +
    //            '</div>';
    //    $("#_jobSubmitOrder").append(msg_box);
    //};


    //self.calculateSubTotals = function (data) {
    //    var sub_total = "0.000";
    //    var additional_exp_cost = "0.000";

    //    if (data.costPerPiece) {
    //        sub_total = data.costPerPiece * data.actualQuantity;
    //        $.each(data.listSummaryAdditionalExpenses, function (key, val) {
    //            if (val.costType == "costPerPiece") {
    //                sub_total += data.actualQuantity * val.cost;
    //            }
    //            else {
    //                sub_total += val.cost;
    //            }
    //        });
    //        data.subtotal=sub_total;
    //    }
    //    else if (data.actualPostageCostPerPiece && data.actualPostageCostPerPiece) {
    //        sub_total = (data.actualPostageCostPerPiece + data.productionCostPerPiece) * data.actualQuantity;
    //        $.each(data.listSummaryAdditionalExpenses, function (key, val) {
    //            if (val.costType == "costPerPiece") {
    //                sub_total += data.actualQuantity * val.cost;
    //            }
    //            else {
    //                sub_total += val.cost;
    //            }
    //        });
    //        data.subtotal = sub_total;
    //    }
    //    else if (data.estimatedPostageCostPerPiece && data.estimatedPostageCostPerPiece) {
    //        sub_total = (data.estimatedPostageCostPerPiece + data.productionCostPerPiece) * data.estimatedQuantity;
    //        $.each(data.listSummaryAdditionalExpenses, function (key, val) {
    //            if (val.costType == "costPerPiece") {
    //                sub_total += data.estimatedQuantity * val.cost;
    //            }
    //            else {
    //                sub_total += val.cost;
    //            }
    //        });
    //        data.subtotal = sub_total;
    //    }
    //};

    //self.calculateSubTotals1 = function (data) {
    //    var sub_total = "0.000";
    //    var additional_exp_cost = "0.000";

    //    if (data.costPerPiece && data.costPerPiece()) {
    //        sub_total = data.costPerPiece() * data.actualQuantity();
    //        $.each(data.listSummaryAdditionalExpenses(), function (key, val) {
    //            if (val.costType() == "costPerPiece") {
    //                sub_total += data.actualQuantity() * val.cost();
    //            }
    //            else {
    //                sub_total += val.cost();
    //            }
    //        });
    //    }
    //    else if (data.actualPostageCostPerPiece && data.actualPostageCostPerPiece()) {
    //        sub_total = (data.actualPostageCostPerPiece() + data.productionCostPerPiece()) * data.actualQuantity();
    //        $.each(data.listSummaryAdditionalExpenses(), function (key, val) {
    //            if (val.costType() == "costPerPiece") {
    //                sub_total += data.actualQuantity() * val.cost();
    //            }
    //            else {
    //                sub_total += val.cost();
    //            }
    //        });
    //    }
    //    else if (data.estimatedPostageCostPerPiece && data.estimatedPostageCostPerPiece()) {
    //        sub_total = (data.estimatedPostageCostPerPiece() + data.productionCostPerPiece()) * data.estimatedQuantity();
    //        $.each(data.listSummaryAdditionalExpenses(), function (key, val) {
    //            if (val.costType() == "costPerPiece") {
    //                sub_total += data.estimatedQuantity() * val.cost();
    //            }
    //            else {
    //                sub_total += val.cost();
    //            }
    //        });
    //    }
    //    return sub_total;
    //};


    ////Calculate final EstQty and final Invoice amount when changing estimated quantity.
    //self.calculateSubTotal = function (data, event) {
    //    var ctrl = event.currentTarget || event.target;
    //    var context = ko.contextFor(ctrl).$parent;
    //    if (data.quantityPretty != undefined && data.quantityPretty != null && data.quantityPretty() == 0) {
    //        if (jobCustomerNumber == CASEYS)
    //            data.totalPretty('$' + (ctrl.value.replace('$', '').replace(',', '') * parseFloat(data.productionCPPPretty().replace('$', ''))).toFixed(3).replace(/\B(?=(\d{3})+(?!\d))/g, ","));
    //        else
    //            data.totalPretty('$' + (ctrl.value.replace('$', '').replace(',', '') * (parseFloat(data.productionCPPPretty().replace('$', '')) + parseFloat(data.postageCPPPretty().replace('$', '')))).toFixed(3).replace(/\B(?=(\d{3})+(?!\d))/g, ","));
    //        //calculate final est quantity
    //        var final_est_qty = 0.00;
    //        $.each($('#tbRowData input[type=text]'), function () {
    //            final_est_qty += parseFloat(($(this).val() != "" ? $(this).val() : 0).toString().replace('$', '').replace(',', ''));
    //        });
    //        context.finalEstdQuantity(final_est_qty.toFixed(3).replace(/\B(?=(\d{3})+(?!\d))/g, ","));
    //        //calculate final invoice amount
    //        var final_invoice_amount = 0.00;
    //        $.each($('#tbRowData tr'), function (key, val) {
    //            final_invoice_amount += parseFloat($(this).find('td:nth-child(' + parseInt($(val).find('td').length) + ')').attr('data-val').replace('$', '').replace(',', ''));
    //        });
    //        context.finalInvoiceAmount('$' + final_invoice_amount.toFixed(3).replace(/\B(?=(\d{3})+(?!\d))/g, ","));
    //    }
    //}
};