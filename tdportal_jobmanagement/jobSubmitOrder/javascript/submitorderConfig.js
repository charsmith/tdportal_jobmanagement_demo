﻿isCWSubmitOrderPreferences = function () {
    var customer = getCustomerNumber();
    return ([DKI, ICYNENE].indexOf(customer) > -1);
}

hideProductionReleaseButton = function () {
    var hide_production_release = false;
    var customer = getCustomerNumber();
    var role = getUserRole();
    switch (role) {
        case 'admin':
            hide_production_release = ([DCA, KUBOTA, SPORTSKING, GWA, REDPLUM, BUFFALONEWS, PIONEERPRESS, EQUINOX, DKI, ICYNENE].indexOf(customer) == -1);
            break;
        case 'user':
            hide_production_release = true;
            break;
        default: break;
    }

    return hide_production_release; 
}

hideConfirmationMailsDiv = function () {
    var customer = getCustomerNumber();   
    return ([DKI, ICYNENE, BBB, AAG, TRIBUNEPUBLISHING, SANDIEGO, AH, SPORTSAUTHORITY, WALGREENS, REDPLUM, SAFEWAY, LOWES, CW, DCA, KUBOTA, OH, SPORTSKING, GWA, CASEYS, SCOTTS, ACE, REDPLUM, BUFFALONEWS, PIONEERPRESS, EQUINOX, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);
}

showCostAndQuantityWarningMessage = function () {
    var customer = getCustomerNumber();
    return ([DKI, ICYNENE].indexOf(customer) > -1);
}

showApproveEmailDemoHintsPopup = function () {
    var customer = getCustomerNumber();
    return ([DKI, ICYNENE, BBB, AAG, TRIBUNEPUBLISHING, SANDIEGO, SPORTSAUTHORITY, WALGREENS, REDPLUM, SAFEWAY, LOWES, CW, DCA, KUBOTA, OH, SPORTSKING, GWA, CASEYS, SCOTTS, ACE, ANDERSEN, REDPLUM, BUFFALONEWS, PIONEERPRESS, EQUINOX, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);
}

showSiteInDemoMessage = function () {
    var customer = getCustomerNumber();
    return ([DKI, CASEYS, ICYNENE, SPORTSKING, KUBOTA, ANDERSEN, EQUINOX].indexOf(customer) > -1);
}

isUpdateOutputDataNeeded = function () {
    var customer = getCustomerNumber();
    return ([DKI, ICYNENE].indexOf(customer) > -1);
}

showUploadedArtFilesInDivSummary = function () {
    var customer = getCustomerNumber();
    return ([DKI, SCOTTS, ICYNENE, ACE, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) == -1);
}

showUploadedListFilesInDivSummary = function () {
    var customer = getCustomerNumber();
    return ([DKI, ICYNENE].indexOf(customer) > -1);
}

hidePricingStructure = function(){
    var customer = getCustomerNumber();
    return ([DKI, ICYNENE, KUBOTA, ALLIED, SCOTTS, ACE, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER,CW].indexOf(customer) > -1);
}

hideSubmitOrder = function() {
    var customer = getCustomerNumber();
    var hide_submit_order = false;
    var customer = getCustomerNumber();
    var role = getUserRole();
    switch (role) {
        case 'user':
            hide_submit_order = ([SPORTSAUTHORITY, WALGREENS, SAFEWAY, LOWES].indexOf(customer) > -1);
            break;        
        default: break;
    }

    return hide_submit_order;
}

isOfferSetupValidationRequired = function () {
    var customer = getCustomerNumber();
    return ([MILWAUKEE].indexOf(customer) == -1);
}

showTotalsText = function() {
    var customer = getCustomerNumber();
    return ([SCOTTS, ACE, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);
}

isProductionCustomers = function() {
    var customer = getCustomerNumber();
    return ([SCOTTS, ACE, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);
}

showThankyouPage = function() {
    var customer = getCustomerNumber();
    return ([SCOTTS, ACE, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);
} 

isLoadStoreStatusApprover = function() {
    var customer = getCustomerNumber();
    return ([SCOTTS, ACE, ANDERSEN, SHERWINWILLIAMS, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);
}

showLoginPage = function() {
    var customer = getCustomerNumber();
    return ([SCOTTS, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);
}

getCustomMessage = function () {
    var customer = getCustomerNumber();
    var msg = "";
    switch (customer) {
        case SCOTTS:
            msg = 'Both you and your co-op approver will receive an approval email. Click the "Proofing &amp; Approval" button to review a final proof of your postcard, counts and cost. Approval is required by both you and your co-op approver three days past the end of the opt-in period.';
            break;
        case MILWAUKEE:
            msg = 'By submitting this order you are releasing it for production and delivery for the selected in-home date. If you have any questions or have submitted this job in error please email <a href="mailto:GizmoTeam@conversionalliance.com">GizmoTeam@conversionalliance.com</a>.<br /><br />To start another order please return to the home page.';
            break;
        case CASEYS:
            msg = 'By submitting this order you are releasing it for production and delivery for the selected in-home date. If you have any questions or have submitted this job in error please email <a href="mailto:CA-CaseysTeam@conversionalliance.com">CA-CaseysTeam@conversionalliance.com</a>.<br /><br />To start another order please return to the home page.';
            break;
        default:
            msg = 'Both you and your co-op approver will receive an approval email. Click the "Proofing &amp; Approval" button to review a final proof of your postcard, counts and cost. Approval is required by both you and your co-op approver three days past the end of the opt-in period.';
            break;
    }
    return msg;
}