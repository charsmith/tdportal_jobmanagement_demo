﻿//Alert message
var homeButtonClick = function (id) {   
    if (id == 'signout') {
        clearAllSession();
        if (showLoginPage()) {
            var url1 = '../login.htm?q=' + appPrivileges.customerNumber;
            //window.location.href = "../login_new.htm";
            window.location.href = url1;
        }
        else
            window.location.href = '../login.htm';
        return false;
    }
    return true;
}

$(document).on('pageshow', '#_jobThankYou', function (event) {
    var msg = getCustomMessage();
    $('#spnThankYouText').html(msg);
});
