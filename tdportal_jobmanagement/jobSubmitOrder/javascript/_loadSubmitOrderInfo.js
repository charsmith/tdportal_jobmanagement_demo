﻿//var loadSubmitOrderInfo = function (self) {
//******************** View Model Start ******************************
var jobSubmitOrder = function () {
    var self = this;
    //******************** Global Variables Start **************************
    self.gData;
    self.jobNumber = getSessionData("jobNumber");
    self.facilityId = sessionStorage.facilityId;
    //self.approversList = [];
    self.quantity;
    self.estQuantity;
    self.cost;
    self.gOutputData;
    self.postJobURL;
    self.PAGE_NAME = 'jobSubmitOrder.html';
    self.customerDetailsData = "";
    self.listBizRules = ko.observableArray([]);
    self.tieredPricing = ko.observable();
    self.isOrderSubmitted = ko.observable(false);
    self.isBillGenerationClicked = featureBillingInvoice() ? ko.observable(true) : ko.observable(false);
    self.wantsActualColumns = ko.observable(true);
    self.wantsEstimateColumns = ko.observable(true);
    self.wantsPostageCostPerPiece = ko.observable(true);
    self.wantsProductionCostPerPiece = ko.observable(true);
    self.isOldOptInJob = ko.observable((sessionStorage.isClosedJob != undefined && sessionStorage.isClosedJob != null && sessionStorage.isClosedJob != "") ? JSON.parse(sessionStorage.isClosedJob) : false);
    self.estPriority = ko.observable('1');
    self.actPriority = ko.observable('2');
    self.canShowPostageInvoice = ko.observable(false);
    self.orderStoreIds = [];
    self.isActualQtyNull = ko.observable("0");
    var gEditJobService = serviceURLDomain + "api/JobTicket/" + sessionStorage.facilityId + "/" + sessionStorage.jobNumber + '/asdf' /*+ sessionStorage.username;*/;
    //******************** Global Variables End **************************
    new makeSubmitOrder(self);
    //self.getApproversList();    
    var Order = function (order_data) {
        var selfOrder = this;
        ko.mapping.fromJS(order_data, [], selfOrder);
        selfOrder.estSubTotal = ko.computed(function (data) {
            //if (selfOrder.subtotal() != null && selfOrder.subtotal() != "") {
            //    return '$' + parseInt(selfOrder.subtotal()).toFixed(3);
            //} else {
            //    return '$0.000';
            //}
            var sub_total = 0.000;
            var additional_exp_cost = 0.000;
            if (selfOrder.estimatedPostageCostPerPiece && selfOrder.estimatedPostageCostPerPiece() != null) {
                sub_total = (((self.wantsPostageCostPerPiece()) ? selfOrder.estimatedPostageCostPerPiece() : 0) + selfOrder.productionCostPerPiece()) * selfOrder.estimatedQuantity();
                $.each(selfOrder.tblSummaryAdditionalExpenses(), function (key, val) {
                    if (val.costType() == "costPerPiece") {
                        sub_total += selfOrder.estimatedQuantity() * val.cost();
                    }
                    else {
                        sub_total += (val.cost() ? parseFloat(val.cost()) : "");
                    }
                });
            }
            return parseFloat(sub_total).toFixed(2);
        });
        selfOrder.actSubTotal = ko.computed(function (data) {
            var sub_total = 0.000;
            var additional_exp_cost = 0.000;
            if (selfOrder.actualPostageCostPerPiece && selfOrder.actualPostageCostPerPiece() != null) {
                sub_total = (((self.wantsPostageCostPerPiece()) ? selfOrder.actualPostageCostPerPiece() : 0) + selfOrder.productionCostPerPiece()) * selfOrder.actualQuantity();
                $.each(selfOrder.tblSummaryAdditionalExpenses(), function (key, val) {
                    if (val.costType() == "costPerPiece") {
                        sub_total += selfOrder.actualQuantity() * val.cost();
                    }
                    else {
                        sub_total += (val.cost() ? parseFloat(val.cost()) : "");
                    }
                });
            }

            return parseFloat(sub_total).toFixed(2);
        });
    }
    self.ordersData = ko.observableArray([]);
    self.headers = ko.observableArray([]);
    self.totalHeaders = ko.observableArray([]);
    self.tempTotalHeaders = ko.observableArray([]);
    self.listStoreApprovers = ko.observableArray([]);
    self.colspan = ko.observable();
    self.isCostPerPieceVisible = ko.observable(true);
    self.isProductionCostPerPieceVisible = ko.observable(true);
    self.isPostageCostPerPieceVisible = ko.observable(true);
    self.isActualQuantityVisible = ko.observable(true);
    self.isEstimatedQuantityVisible = ko.observable(true);
    self.templateName = ko.observable("");
    self.coOpApprovers = ko.observableArray();
    self.numberOfAddlExps = ko.observable(0);

    self.qtyChanged = ko.observable(false);
    self.isJobSaved = ko.observable("");
    var diff_between_json_objects = [];
    var curr_job_obj = jQuery.parseJSON(getSessionData("jobSetupOutput")).mailstreamAction;
    var actual_job_obj = jQuery.parseJSON(getSessionData("jobSetupOutputCompare")).mailstreamAction;
    diff_between_json_objects = DiffObjects(curr_job_obj, actual_job_obj);

    self.loadSubmitOrder = function () {
        if (self.gData) {
            var displayEmail = "";
            var customerDetails = ko.utils.arrayFilter(self.customerDetailsData, function (customer) {
                return customer.jobCustomerNumber == jobCustomerNumber;
            });
            var tempList = {};
            self.ordersData.removeAll();
            self.jobCustomerNumber = jobCustomerNumber;
            self.displayEmail = (customerDetails.length > 0) ? customerDetails[0].email : "";

            if (customerDetails != null && customerDetails[0].gridCols != undefined && customerDetails[0].gridCols != null)
                self.headers(customerDetails[0].gridCols);

            if (customerDetails != null && customerDetails[0].gridColTotals != undefined && customerDetails[0].gridColTotals != null)
                self.totalHeaders(customerDetails[0].gridColTotals);


            //self.estColspan = ko.computed(function () {
            //    var est_colspan_cnt = (self.headers().length - self.totalHeaders().length) + 1;
            //    est_colspan_cnt = (self.wantsPostageCostPerPiece() == 0) ? est_colspan_cnt - 1 : est_colspan_cnt;
            //    est_colspan_cnt = (self.wantsProductionCostPerPiece() == "false") ? est_colspan_cnt - 1 : est_colspan_cnt;
            //    return est_colspan_cnt;
            //});
            //self.colspan((self.headers().length - self.totalHeaders().length) + 1);
            var total_index = "";
            $.each(self.headers(), function (key, val) {
                if (val.toLowerCase().indexOf('subtotal') > -1) {
                    total_index = key;
                    return false;
                }
            });

            var cpp_cnt = 0;
            var pcpp_cnt = 0;
            var act_qty_cnt = 0;
            if (self.gData.summaryStoreList != undefined && self.gData.summaryStoreList != null) {
                $.each(self.gData.summaryStoreList, function (a, b) {
                    var names = Object.getOwnPropertyNames(b);
                    if (b.costPerPiece == 0)
                        cpp_cnt++;
                    if (b.productionCostPerPiece == 0 && b.estimatedPostageCostPerPiece == 0)
                        pcpp_cnt++;
                    if (b.actualQuantity == 0)
                        act_qty_cnt++;
                    $.each(Object.getOwnPropertyNames(b), function (c, d) {
                        if (customerDetails[0].gridColIds.indexOf(d) > -1)
                            tempList[d] = b[d];
                    });
                    var addl_exp_cnt = 0;
                    if (a == 0) {
                        if (b.tblSummaryAdditionalExpenses.length > 0)
                            var idx = self.headers().indexOf('Additional Expense');
                        var exp_val = "";
                        var apply_to_val = "";
                        $.each(b.tblSummaryAdditionalExpenses, function (key, val) {
                            addl_exp_cnt += 1;
                            total_index = self.headers().lastIndexOf('Subtotal');
                            if (exp_val != val.cost) {
                                for (var i = 0; i < b.tblSummaryAdditionalExpenses.length - key; i++) {
                                    b.tblSummaryAdditionalExpenses[i].applyTo = "applyToSingle";
                                }
                            }
                            else if ((b.tblSummaryAdditionalExpenses.length - 1 == key) && exp_val == val.cost) {
                                for (var i = 0; i < b.tblSummaryAdditionalExpenses.length - key; i++) {
                                    b.tblSummaryAdditionalExpenses[i].applyTo = "applyToAll";
                                }
                            }
                            if (key == 0) {
                                exp_val = val.cost;
                                if (idx > -1)
                                    self.headers().splice(idx, 1)
                            }
                            self.headers().splice(idx, 0, ((val.expenseName == "") ? "Additional Expense" : val.expenseName));
                            if (val.expenseName == "Additional Expense")
                                val.expenseName = "";
                            idx = idx + 1;
                        });
                        self.numberOfAddlExps(addl_exp_cnt);
                    }

                    $.each(b.tblSummaryAdditionalExpenses, function (key, val) {
                        if (val.cost != undefined && val.cost != null && val.cost != "")
                            val.cost = val.cost.toFixed(2);
                    });
                    //self.ordersData.push(new Order(tempList));
                    self.ordersData.push(new Order(b));
                });
                if (cpp_cnt > 0 && self.gData.summaryStoreList.length == cpp_cnt)
                    self.isCostPerPieceVisible(false);
                if (pcpp_cnt > 0 && self.gData.summaryStoreList.length == pcpp_cnt) {
                    self.isProductionCostPerPieceVisible(false);
                    self.isPostageCostPerPieceVisible(false);
                }
                if (act_qty_cnt > 0 && self.gData.summaryStoreList.length == act_qty_cnt)
                    self.isActualQuantityVisible(false);
            }
            self.colspan = ko.computed(function () {
                var colspan_cnt = (self.headers().length - self.totalHeaders().length) + 1;
                colspan_cnt = (self.wantsPostageCostPerPiece() == 0) ? colspan_cnt - 1 : colspan_cnt;
                colspan_cnt = (self.wantsProductionCostPerPiece() == "false") ? colspan_cnt - 1 : colspan_cnt;
                return colspan_cnt;
            });

            if (listStoreApprovers.length > 0) {
                ko.mapping.fromJS(approversList, [], self.coOpApprovers);//dropdown approvers list
                $.each(listStoreApprovers, function (key, val) {
                    var tmp = {};
                    ko.mapping.fromJS(val, {}, tmp);
                    self.listStoreApprovers.push(tmp);
                });
            }

            self.estQtyTotal = ko.computed(function () {
                var est_qty_total = 0;
                $.each(self.ordersData(), function (key, val) {
                    est_qty_total += parseInt((val.estimatedQuantity() != "" && val.estimatedQuantity() != null) ? val.estimatedQuantity() : 0);
                });
                return est_qty_total;
            });

            self.actQtyTotal = ko.computed(function () {
                var act_qty_total = 0;
                $.each(self.ordersData(), function (key, val) {
                    act_qty_total += parseInt((val.actualQuantity() != "" && val.actualQuantity() != null) ? val.actualQuantity() : 0);
                });
                return act_qty_total;
            });

            self.estTotal = ko.computed(function () {
                var est_total = 0;
                $.each(self.ordersData(), function (key, val) {
                    est_total += parseFloat(val.estSubTotal());
                });
                return est_total.toFixed(2);
            });

            self.validateApprovers = function () {
                var msg = "";
                var approvers_info = ko.toJS(self.listStoreApprovers, mapping);
                $.each(approvers_info, function (key, val) {
                    if (val.approverEmail == "" || val.approverEmail == undefined || val.approverEmail == null) {
                        msg += "<li>" + (val.storeId + " - " + val.storeName) + "</li>";
                    }
                });
                msg = (msg != "") ? "<b>Please select approvers for stores</b><br/><ul>" + msg + "</ul>" : msg;
                return msg;
            };

            self.actTotal = ko.computed(function () {
                var act_total = 0;
                $.each(self.ordersData(), function (key, val) {
                    act_total += parseFloat(val.actSubTotal());
                });
                return act_total.toFixed(2);
            });

            self.finalInvoiceAmount = ko.observable(self.gData.totalCost != undefined && self.gData.totalCost != "" ? self.gData.totalCost.toFixed(2) : 0.000);
            self.finalEstdQuantity = ko.observable(self.gData.totalEstimatedQuantity);
            self.finalQuantity = ko.observable(self.gData.totalQuantity);
            self.finalCost = self.cost;
            self.emails = ko.observable();
            //self.approversViewersList = ko.observable({});
            //self.approversViewersList(self.approversList);
            self.isEditable = ko.observable((sessionStorage.userRole == 'admin') ? 1 : 0);
            self.otherComments = ko.observable('');
            self.selectedStore = ko.observable({});
            self.newAdditionalExpense = ko.observable(false);
            self.selectedStoreForEdit = ko.observable({});
            
            //diff_between_json_objects = DiffObjects(jQuery.parseJSON(getSessionData("jobSetupOutput")), jQuery.parseJSON(getSessionData("jobSetupOutputCompare")));

            var curr_job_obj = jQuery.parseJSON(getSessionData("jobSetupOutput")).mailstreamAction;
            var actual_job_obj = jQuery.parseJSON(getSessionData("jobSetupOutputCompare")).mailstreamAction;
            diff_between_json_objects = DiffObjects(curr_job_obj, actual_job_obj);

            if (diff_between_json_objects.length != 0) {
                self.qtyChanged(true);
                self.isJobSaved(false);
                $('#dvPricingGrid').css('display', 'block');
            } else {
                self.qtyChanged(false);
                self.isJobSaved(true);
                $("#tblOrderGrid").css('display', 'block');
                $("#tblOrderGrid").css('display', '');
                $('#dvPricingGrid').css('display', 'block');
                $('#dvOtherComments').css('display', 'block');
            }

            self.onQtyChanged = function (data, event) {
                var ctrl = event.targetElement || event.taret || event.currentTarget;
                if (ctrl.value > 0 && $("#configTable-popup [type=checkbox]:eq(1)").attr('checked'))
                    self.wantsProductionCostPerPiece(true);
                else
                    self.wantsProductionCostPerPiece(false);
                self.qtyChanged(true);
                //self.isOrderSubmitted(true);//demo purpose check and remove this condition later - Postage invoice should visible only for positive jobs 
            };
            self.editAdditionalCharges = function (data, event) {
                var temp_data = ko.mapping.toJS(data);
                self.selectedStoreForEdit(ko.mapping.toJS(data));
                var obs_temp_data = {};
                ko.mapping.fromJS(temp_data, {}, obs_temp_data);
                self.selectedStore(temp_data);

                //self.selectedStoreForEdit(obs_temp_data);
                $('#additionalChargesPopup div[data-role=collapsibleset] div fieldset').trigger('create');
                $('#additionalChargesPopup div[data-role=collapsibleset] div').trigger('create');
                $('#additionalChargesPopup div[data-role=collapsibleset]').collapsibleset('refresh');
                $('#additionalChargesPopup').popup('open');
                createPlaceHolderforIE();
            };

            self.addAdditionExpense = function (data, event) {
                var selected_store_info = ko.mapping.toJS(data.selectedStore);
                selected_store_info.tblSummaryAdditionalExpenses.push({
                    expenseName: "",
                    costType: "",
                    cost: "",
                    applyTo: ""
                });
                self.newAdditionalExpense(true);
                var obs_temp_data = {};
                self.selectedStore(selected_store_info);
                $('#additionalChargesPopup div[data-role=collapsibleset] div fieldset').trigger('create');
                $('#additionalChargesPopup div[data-role=collapsibleset] div').trigger('create');
                $('#additionalChargesPopup div[data-role=collapsibleset]').collapsibleset('refresh');
                createPlaceHolderforIE();
            };

            self.cancelAddExpenses = function (data, event) {
                data.selectedStoreForEdit({});
                $('#additionalChargesPopup').popup('close');
            };

            self.updateSelectedExpenseAndStore = function (source_val, destination_val, index) {
                if (index == 0)
                    destination_val.tblSummaryAdditionalExpenses.removeAll();
                var temp_exp = {};
                ko.mapping.fromJS(source_val, {}, temp_exp);
                destination_val.tblSummaryAdditionalExpenses.push(temp_exp);
            };

            self.validateAdditionalExpenses = function () {
                //Expense Name
                //Cost (Radio button and text box)
                //Apply to
                //Remove with apply to

                var temp_selected_store_info = self.selectedStoreForEdit();
                var validation_regex = /^[ A-Za-z0-9,._-]*$/;
                var msg = "";
                var is_apply_all = false;
                $.each($('#additionalChargesPopup div[data-role=collapsibleset]'), function () {
                    $.each($(this).find('div[data-role=collapsible]'), function () {
                        var ctl_expense_name = $(this).find('input[type=text][id^=txtAdditional]');
                        var ctl_txt_cost = $(this).find('input[type=text][id^=txtCost]');
                        var ctl_cost_type = $(this).find('input[type=radio][name^=rdoRateType]:checked');
                        var ctl_apply_to = $(this).find('input[type=radio][name^=rdoApplyTo]:checked');
                        var ctl_chk_remove = $(this).find('input[type=checkbox][id^=chkRemove]');

                        if (!$(ctl_chk_remove).is(':checked')) {
                            if (($(ctl_expense_name).attr('placeholder') == $(ctl_expense_name).val()) || $(ctl_expense_name).val() == "") {
                                msg += "<li>Please enter Expense Name.</li>";
                            }
                            else if (!validation_regex.test($(ctl_expense_name).val())) {
                                msg += 'Please enter a valid Expense Name. The following characters are not allowed "\/@#$%^&*(){}[]|?<>~`+\'".';
                            }

                            $("[name^=rdoRateType]:checked")
                            if (!$(ctl_expense_name).attr('disabled') && $(ctl_cost_type).length == 0) {
                                msg += "<li>Please select the option to apply the cost to a \"Cost Per Piece\" or \"Flat Rate\"</li>";
                            }

                            if (!$(ctl_expense_name).attr('disabled') && ($(ctl_txt_cost).val() == "" || $(ctl_txt_cost).val() == "0" || $(ctl_txt_cost).val() == 0)) {
                                msg += "<li>Please enter Cost.</li>";
                            }

                            if (!$(ctl_expense_name).attr('disabled') && $(ctl_apply_to).length == 0) {
                                msg += "<li>Please select the option to add this expense to a \"Single Location\" or \"All Locations\"</li>";
                            }
                            if ($(ctl_apply_to).val() == "applyToAll")
                                is_apply_all = true;
                            // }
                        }
                        else {
                            if ($(ctl_apply_to).length == 0) {
                                msg += "<li>Please select the option to remove this expense from a \"Single Location\" or \"All Locations\"</li>";
                            }
                        }
                        if (msg != "" || is_apply_all)
                            return false;
                    });
                    if (msg != "" || is_apply_all)
                        return false;
                });

                if (msg != "") {
                    msg = '<ul>' + msg + '</ul>';
                    $('#additionalChargesPopup').popup('close');
                    $('#okBut').unbind('click');
                    $('#alertmsg').html(msg);
                    window.setTimeout(function () {
                        $('#okBut').bind('click', function () {
                            $('#popupDialog').popup('close');
                            $('#okBut').unbind('click');
                            $('#okBut').attr('onclick', '$(\'#popupDialog\').popup(\'close\');');
                            window.setTimeout(function () {
                                $('#additionalChargesPopup').popup('open');
                            }, 200);
                        });

                        $('#popupDialog').popup('open');
                    }, 200);
                    return false;
                }
                else
                    return true;
            };

            self.approverChanged = function (data, event) {
                var ele = event.target || event.currentTarget;
                var ctrl_id = $(ele).attr('id');
                var approver_name = $("#" + ctrl_id)[0].value;
                if ($('#chkSetAllApprovers').is(':checked') && ctrl_id.indexOf('primary') > 0) {
                    var approver_dropdowns = $('div [data-fieldtype=approverList]').not(':first');
                    if (approver_dropdowns.length > 0) {
                        $.each(approver_dropdowns, function (index, ctrl) {
                            ctrl.selectedIndex = ele.selectedIndex;
                            $(ctrl).selectmenu('refresh');
                        });
                    }
                }
                var approvers_data = ko.mapping.toJS(self.coOpApprovers, mapping);
                $.each(approvers_data, function (key, val) {
                    if (val.approverEmail == approver_name) {
                        data.approverName(val.approverName);
                        return false;
                    }
                });
            }

            self.setAllApproversClick = function (data, event) {
                var ele = event.target || event.currentTarget;
                if ($(ele).is(':checked')) {
                    var first_approver_option = $('select[data-fieldtype=approverList]:first');
                    var approver_email = first_approver_option[0].value;
                    var other_approver_options = $('select[data-fieldtype=approverList]').not(':first');
                    if (other_approver_options.length > 0) {
                        $.each(other_approver_options, function (index, ctrl) {
                            ctrl.selectedIndex = first_approver_option[0].selectedIndex;
                            $(ctrl).addClass('ui-disabled');
                            ctrl.disabled = true;
                        });
                        $(other_approver_options).selectmenu('refresh');
                        var approvers_data = ko.mapping.toJS(self.coOpApprovers, mapping);
                        var approver_name = "";
                        $.each(approvers_data, function (key, val) {
                            if (val.approverEmail == approver_email) {
                                approver_name = val.approverName;
                                return false;
                            }
                        });
                        $.each(self.listStoreApprovers(), function (key, val) {
                            val.approverName(approver_name);
                            val.approverEmail(approver_email);
                        });
                    }
                }
                else {
                    var approver_dropdowns = $('select[data-fieldtype=approverList]');
                    if (approver_dropdowns.length > 0) {
                        $.each(approver_dropdowns, function (index, ctrl) {
                            ctrl.disabled = false;
                            $(ctrl).removeClass('ui-disabled');
                        });
                        $(approver_dropdowns).selectmenu('refresh');
                    }
                }
            };

            self.submitExpensesInfo = function (data, event) {
                var cnt_temp = "";
                var str_indx = "";
                var selected_store_info = data.selectedStore();
                var idx_remove = "";
                var apply_remove_status = [];
                var idx = "";
                var prev_idx = "";
                var is_apply_all = false;
                var is_remove_all = "";
                var chk_apply_to = $('input[name=rdoApplyTo]:checked').val();

                var removing_column_header = "";
                var chk_apply_list = "";
                var chk_remove_list = false;
                var temp_selected_store_info = self.selectedStoreForEdit();
                var new_added_expense = (self.newAdditionalExpense() == true) ? selected_store_info.tblSummaryAdditionalExpenses[selected_store_info.tblSummaryAdditionalExpenses.length - 1] : {};

                if (!self.validateAdditionalExpenses())
                    return false;
                var is_duplicate = false;
                $.each(self.ordersData(), function (key, val) {
                    $.each(selected_store_info.tblSummaryAdditionalExpenses, function (add_key, add_val) {
                        chk_apply_list = $('#additionalChargesPopup div[data-role=collapsibleset] div fieldset input[name=rdoApplyTo' + add_key + ']:checked').val();
                        chk_remove_list = $('input[id=chkRemove_' + add_key + ']').is(':checked');
                        var is_found = false;
                        if (key == 0) {
                            $.each(val.tblSummaryAdditionalExpenses(), function (opt_key, opt_val) {
                                if (!$('input[id=chkRemove_' + opt_key + ']').is(':checked') && self.newAdditionalExpense() == true && opt_val.expenseName() == new_added_expense.expenseName && add_key != (selected_store_info.tblSummaryAdditionalExpenses.length - 1)) {
                                    is_found = true;
                                }
                            });
                        }
                        if (is_found) {
                            self.duplicateAdditionalExpense();
                            is_duplicate = true;
                            return false;
                        }
                        else {
                            if (chk_apply_list == "applyToSingle") {
                                var is_found = false;
                                if ((val.storeId() == selected_store_info.storeId) || val.tblSummaryAdditionalExpenses().length != selected_store_info.tblSummaryAdditionalExpenses.length) {
                                    $.each(val.tblSummaryAdditionalExpenses(), function (opt_key1, opt_val1) {
                                        if (opt_key1 == add_key && (val.storeId() == selected_store_info.storeId)) {// && (val.storeId() == selected_store_info.storeId)
                                            if (!chk_remove_list) {
                                                opt_val1.expenseName(add_val.expenseName);
                                                opt_val1.cost(add_val.cost);
                                                opt_val1.costType(add_val.costType);
                                                opt_val1.applyTo(add_val.applyTo);
                                            }
                                            else {
                                                opt_val1.expenseName(add_val.expenseName);
                                                opt_val1.cost("");
                                                opt_val1.costType("");
                                                opt_val1.applyTo("");
                                            }
                                            is_found = true;
                                            return false;
                                        }
                                        else {
                                            if (opt_key1 == add_key)
                                                opt_val1.expenseName(add_val.expenseName);
                                            //    opt_val1.applyTo("aplyToSingle");
                                        }
                                        if (is_found)
                                            return false;
                                    });
                                    if (!is_found && (val.storeId() == selected_store_info.storeId)) {
                                        var temp_info = ko.mapping.fromJS(add_val, {});
                                        val.tblSummaryAdditionalExpenses.push(temp_info);
                                    }
                                    else {
                                        if (!is_found) { //&& val.listSummaryAdditionalExpenses().length != selected_store_info.listSummaryAdditionalExpenses.length && !chk_remove_list
                                            var temp_info = {
                                                expenseName: add_val.expenseName,
                                                costType: "",
                                                cost: "",
                                                applyTo: ""
                                            };
                                            temp_info = ko.mapping.fromJS(temp_info, {});
                                            val.tblSummaryAdditionalExpenses.push(temp_info);
                                        }
                                    }
                                }
                            }
                            else
                                if (chk_apply_list == "applyToAll") {
                                    if (!chk_remove_list && val.tblSummaryAdditionalExpenses().length == selected_store_info.tblSummaryAdditionalExpenses.length) {
                                        $.each(val.tblSummaryAdditionalExpenses(), function (opt_key, opt_val) {
                                            if (opt_key == add_key) {
                                                opt_val.expenseName(add_val.expenseName)
                                                opt_val.cost(add_val.cost);
                                                opt_val.costType(add_val.costType);
                                                opt_val.applyTo(add_val.applyTo);
                                                return false;
                                            }
                                        });

                                    } else
                                        if (!chk_remove_list) {
                                            var is_found = false;
                                            $.each(val.tblSummaryAdditionalExpenses(), function (opt_key, opt_val) {
                                                if (opt_val.expenseName() == add_val.expenseName) {
                                                    is_found = true;
                                                }
                                            });
                                            if (!is_found) {
                                                self.updateSelectedExpenseAndStore(add_val, val, add_key);
                                            }
                                        }
                                        else {
                                            var remove_idx = "";
                                            $.each(val.tblSummaryAdditionalExpenses(), function (opt_key, opt_val) {
                                                if (opt_val.expenseName() == add_val.expenseName) {
                                                    remove_idx = opt_key;
                                                }
                                            });
                                            if (remove_idx.toString() != "") {
                                                val.tblSummaryAdditionalExpenses.splice(remove_idx, 1);
                                            }
                                        }
                                }
                        }
                    });
                    if (is_duplicate)
                        return false;
                    if (val.tblSummaryAdditionalExpenses().length == 0) {
                        var temp_info = {
                            expenseName: "Additional Expense",
                            costType: "",
                            cost: "",
                            applyTo: ""
                        };
                        temp_info = ko.mapping.fromJS(temp_info, {});
                        val.tblSummaryAdditionalExpenses.push(temp_info);
                    }
                });
                if (is_duplicate)
                    return false;
                //var st_ind = "";
                //if (self.headers().lastIndexOf('Postage CPP') > -1)
                //    st_ind = self.headers.lastIndexOf('Postage CPP') + 1;
                if (self.headers().lastIndexOf('Production CPP') > -1)
                    st_ind = self.headers().lastIndexOf('Production CPP') + 1;
                else if (self.headers().indexOf('Cost Per Piece') > -1)
                    st_ind = self.headers().indexOf('Cost Per Piece') + 1;
                var end_ind = self.headers().lastIndexOf('Subtotal');
                self.headers.splice(st_ind, (end_ind - st_ind));

                $.each(self.ordersData(), function (a, b) {
                    if (sessionStorage.userRole == 'admin' && a == 0) {
                        var is_header_found = false;
                        var header_idx_toremove = [];
                        $.each(self.headers(), function (header_key, header_val) {
                            if (header_val == removing_column_header) {
                                header_idx_toremove.push(header_key);
                                return false;
                            }
                        });
                        if (header_idx_toremove.length > 0) {
                            $.each(header_idx_toremove, function (key, val) {
                                self.headers.splice(val, 1);
                            });
                        }
                        $.each(b.tblSummaryAdditionalExpenses(), function (key, val) {
                            if (self.headers().indexOf(val.expenseName()) == -1) {
                                if (val.expenseName() != "")
                                    self.headers.splice((self.headers().lastIndexOf('Subtotal')), 0, val.expenseName());
                            }
                        });
                    }
                });
                //var colspan_cnt = (self.headers().length - self.totalHeaders().length) + 1;
                //colspan_cnt = (self.wantsPostageCostPerPiece() == 0) ? colspan_cnt - 1 : colspan_cnt;
                //self.colspan(colspan_cnt);
                //self.colspan((self.headers().length - self.totalHeaders().length) + 1);
                self.newAdditionalExpense(false);
                $('#additionalChargesPopup').popup('close');

                window.setTimeout(function () {
                    $("#tblOrderGrid").table("refresh");
                    $("#tblOrderGridTotals").table("refresh");
                    $('#configTable').table('refresh');
                    self.qtyChanged(true);
                    window.setTimeout(function () {
                        $('#tblOrderGridTotals').width($('#tblOrderGrid').width());
                        $.each($('#tblOrderGrid thead tr th'), function (key, val) {
                            $($('#tblOrderGridTotals thead tr th')[key]).width($(this).width());
                        });
                    }, 100);
                }, 1000);
            };
            //to filter customer details based on logged in customer job number
            var customerDetails = ko.utils.arrayFilter(self.customerDetailsData, function (customer) {
                return customer.jobCustomerNumber == jobCustomerNumber;
            });
            if (sessionStorage.progressJob != undefined && sessionStorage.progressJob != null && sessionStorage.progressJob != "" && sessionStorage.progressJob == sessionStorage.jobNumber) {
                $('#btnSubmitOrder').addClass('ui-disabled');
                $('#btnSubmitOrder').attr('disabled', 'disabled');
                $('#btnSubmitOrder')[0].disabled = true;
            }
        }
        else {
            if (diff_between_json_objects.length != 0)
                self.isJobSaved(false);
            else
                self.isJobSaved(true);
        }
    };

    ko.bindingHandlers.numeric = {
        init: function (element, valueAccessor) {
            $(element).on("keydown", function (event) {
                // Allow: backspace, delete, tab, escape, and enter
                if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||
                    // Allow: Ctrl+A
                    (event.keyCode == 65 && event.ctrlKey === true) ||
                    // Allow: . ,
                    (event.keyCode == 188 || event.keyCode == 190 || event.keyCode == 110) ||
                    // Allow: home, end, left, right
                    (event.keyCode >= 35 && event.keyCode <= 39)) {
                    // let it happen, don't do anything
                    return;
                }
                else {
                    // Ensure that it is a number and stop the keypress
                    if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
                        event.preventDefault();
                    }
                }
            });
        }
    };

    self.calculateTotalQtys = function (data, event) {
        var reg = /^\d+$/;
        var ctrl = event.targetElement || event.taret || event.currentTarget;
        var key = String.fromCharCode(event.keyCode);
        if (event.which == 13) {
            $(ctrl).trigger('change');
            self.updateGridData();
            $(ctrl).trigger('blur');
        } else if (!reg.test(key))
            return false
        else return true;
    };

    var mapping = { 'ignore': ["__ko_mapping__"] }
    self.updateGridData = function () {
        $('#popupUpdateTotalsValidation').popup('close');
        if (!self.isJobSaved()) {
            postJobSetUpData('save');
        }
        window.setTimeout(function () {
            $('#loadingText1').text('Updating total...');
            $('#waitPopUp').popup('open');
            var diff_between_json_objects = [];
            diff_between_json_objects = DiffObjects(jQuery.parseJSON(getSessionData("jobSetupOutput")), jQuery.parseJSON(getSessionData("jobSetupOutputCompare")));
            if (diff_between_json_objects.length == 0) {
                self.isJobSaved(true);
                self.qtyChanged(false);
            }

            var temp_data = ko.mapping.toJS(self.ordersData, mapping);
            self.gData.summaryStoreList = temp_data;
            $.each(self.gData.summaryStoreList, function (key, val) {
                if (val.actualPostageCostPerPiece != undefined)
                    delete val["actualPostageCostPerPiece"];
                if (self.isActualQtyNull() == 1) {
                    self.isActualQtyNull(0);
                    val.actualQuantity = null;
                }
            });
            //self.gData.myTblSummaryJob.totalCost = self.finalInvoiceAmount();
            //self.gData.totalQuantity = self.finalQuantity();
            //self.gData.totalEstimatedQuantity = self.finalEstdQuantity();

            //$.each(self.gData.myTblSummaryJob.listSummaryStores, function (key, val) {
            //    self.calculateSubTotals(val);
            //    delete val.templateName;
            //});

            window.setTimeout(function () {
                postCORS(serviceURLDomain + "api/JobSummary_post/" + self.facilityId + '/' + jobCustomerNumber + '/' + self.jobNumber, JSON.stringify(self.gData), function (data) {
                    var job_number = getSessionData("jobNumber");
                    getCORS(serviceURLDomain + "api/JobSummary_get/" + appPrivileges.facility_id + "/" + appPrivileges.customerNumber + "/" + job_number, null, function (data) {
                        $.each(data.summaryStoreList, function (key, val) {
                            if (!val.tblSummaryAdditionalExpenses || val.tblSummaryAdditionalExpenses.length == 0) {
                                //val.tblSummaryAdditionalExpenses = (sessionStorage.userRole != 'admin') ? '$0.00' : [{
                                //    expenseName: "Additional Expense",
                                //    costType: "",
                                //    cost: "",
                                //    applyTo: ""
                                //}];
                                val.tblSummaryAdditionalExpenses = [{
                                    expenseName: "Additional Expense",
                                    costType: "",
                                    cost: "",
                                    applyTo: ""
                                }];
                            }
                            else {
                                $.each(val.tblSummaryAdditionalExpenses, function (key1, val1) {
                                    if (!val1.applyTo)
                                        val1["applyTo"] = "";
                                    if (val1.cost != undefined && val1.cost != null && val1.cost != "")
                                        val1.cost = val1.cost.toFixed(2);

                                });
                            }
                            //self.calculateSubTotals(val);
                            var act_postage_cost_per_piece = 0.000;
                            if ((val.actualTotalPostage != undefined && val.actualTotalPostage != null) && val.actualQuantity != null && val.actualQuantity > 0)
                                act_postage_cost_per_piece = val.actualTotalPostage / val.actualQuantity;
                            val["actualPostageCostPerPiece"] = parseFloat(act_postage_cost_per_piece);
                            if (job_number < -1 && (val.actualQuantity == null || val.actualQuantity == 0) && (data.wantsActualColumns == 1 && data.wantsEstimateColumns == 0)) {
                                val.actualQuantity = val.estimatedQuantity;
                                if (key == 0) {
                                    self.isActualQtyNull(1);
                                }
                            }
                            else
                                if (key == 0) {
                                    self.isActualQtyNull(0);
                                }
                        });
                       
                        self.gData = data;
                        var customerDetails = ko.utils.arrayFilter(self.customerDetailsData, function (customer) {
                            return customer.jobCustomerNumber == jobCustomerNumber;
                        });
                        var tempList = {};
                        var st_ind;
                        //if (self.isPostageCostPerPieceVisible() && self.headers().lastIndexOf('Postage CPP') > -1)
                        //    st_ind = self.headers().lastIndexOf('Postage CPP') + 1;
                        if (self.isPostageCostPerPieceVisible() && self.headers().lastIndexOf('Production CPP') > -1)
                            st_ind = self.headers().lastIndexOf('Production CPP') + 1;
                        else if (self.isCostPerPieceVisible() && self.headers().indexOf('Cost Per Piece') > -1)
                            st_ind = self.headers().indexOf('Cost Per Piece') + 1;
                        if (st_ind == undefined) {
                            st_ind = self.headers().lastIndexOf('Postage CPP') + 1;
                        }
                        var end_ind = self.headers().lastIndexOf('Subtotal');
                        self.headers.splice(st_ind, (end_ind - st_ind));
                        $.each(self.gData.summaryStoreList, function (a, b) {
                            var names = Object.getOwnPropertyNames(b);
                            $.each(Object.getOwnPropertyNames(b), function (c, d) {
                                if (customerDetails[0].gridColIds.indexOf(d) > -1)
                                    tempList[d] = b[d];
                            });
                            if (a == 0) {
                                var exp_val = "";
                                var apply_to_val = "";
                                $.each(b.tblSummaryAdditionalExpenses, function (key, val) {
                                    total_index = self.headers().lastIndexOf('Subtotal');
                                    self.headers.splice(total_index, 0, ((val.expenseName == "") ? "Additional Expense" : val.expenseName));
                                });
                            }

                            var temp_add_exp = ko.observable();
                            var store_idx = '';
                            $.each(self.ordersData(), function (key, val) {
                                if (val.storePk() == b.storePk) {
                                    store_idx = key;
                                    temp_add_exp = new Order(b);
                                    val.actualQuantity(temp_add_exp.actualQuantity());
                                    val.estimatedQuantity(temp_add_exp.estimatedQuantity());
                                    //val.costPerPiece(temp_add_exp.costPerPiece());
                                    val.estimatedPostageCostPerPiece(temp_add_exp.estimatedPostageCostPerPiece());
                                    val.actualPostageCostPerPiece(temp_add_exp.actualPostageCostPerPiece());
                                    val.productionCostPerPiece(temp_add_exp.productionCostPerPiece());
                                    //val.subtotal(temp_add_exp.subtotal());
                                    val.tblSummaryAdditionalExpenses(temp_add_exp.tblSummaryAdditionalExpenses());
                                    return false;
                                }
                            });
                        });
                        //self.finalInvoiceAmount(self.gData.totalCost.toFixed(3));
                        //self.finalEstdQuantity(self.gData.totalEstimatedQuantity);
                        //self.finalQuantity(self.gData.totalQuantity);

                        $('#waitPopUp').popup('close');
                        window.setTimeout(function () {
                            self.qtyChanged(false);
                            $('#alertmsg').text('Totals have been updated successfully.');
                            $('#popupDialog').popup('open');
                            try {
                                $("#tblOrderGrid").table("refresh");
                                $("#tblOrderGridTotals").table("refresh");
                                $("#configTable-popup [type=checkbox]:eq(1)").prop("checked", self.wantsActualColumns()).checkboxradio("refresh").trigger("change");
                                window.setTimeout(function () {
                                    if (!self.wantsEstimateColumns()) {
                                        $("#configTable-popup [type=checkbox]:eq(0)").prop("checked", self.wantsEstimateColumns()).checkboxradio("refresh").trigger("change");
                                        $("#configTable-popup [type=checkbox]:eq(0)").parent().addClass('ui-disabled');
                                        $("#configTable-popup [type=checkbox]:eq(0)")[0].disabled = true;
                                    } else {
                                        $("#configTable-popup [type=checkbox]:eq(0)").prop("checked", self.wantsEstimateColumns()).checkboxradio("refresh").trigger("change");
                                    }
                                }, 500);
                                $('.th-groups th').css('background-color', $('.ui-bar-a').css('background-color'));
                                $('.th-groups th').css('color', $('.ui-bar-a').css('color'));
                                $('.th-groups th').css('text-shadow', $('.ui-bar-a').css('text-shadow'));
                                $('#configTable').table('refresh');
                            }
                            catch (e) {
                                $("#tblOrderGrid").table();
                                $("#tblOrderGridTotals").table();
                                $("#configTable-popup [type=checkbox]:eq(1)").prop("checked", self.wantsActualColumns()).checkboxradio("refresh").trigger("change");
                                window.setTimeout(function () {
                                    if (!self.wantsEstimateColumns()) {
                                        $("#configTable-popup [type=checkbox]:eq(0)").prop("checked", self.wantsEstimateColumns()).checkboxradio("refresh").trigger("change");
                                        $("#configTable-popup [type=checkbox]:eq(0)").parent().addClass('ui-disabled');
                                        $("#configTable-popup [type=checkbox]:eq(0)")[0].disabled = true;
                                    } else {
                                        $("#configTable-popup [type=checkbox]:eq(0)").prop("checked", self.wantsEstimateColumns()).checkboxradio("refresh").trigger("change");
                                    }
                                }, 500);
                                $('.th-groups th').css('background-color', $('.ui-bar-a').css('background-color'));
                                $('.th-groups th').css('color', $('.ui-bar-a').css('color'));
                                $('.th-groups th').css('text-shadow', $('.ui-bar-a').css('text-shadow'));
                                $('#configTable').table('refresh');
                            }
                        }, 1000);
                        $('#waitPopUp').popup('close');
                    }, function (error_response) {
                        showErrorResponseText(error_response, true);
                    });

                }, function (error_response) {
                    showErrorResponseText(error_response, true);
                });
            }, 1000);
        }, 1500);
    };

    self.getColWidth = function (data) {
        var k = ($('#tblOrderGrid').width() / $($('#tblOrderGrid thead th')[data]).width()) + '%';
        return k;
    };

    self.duplicateAdditionalExpense = function () {
        $('#additionalChargesPopup').popup('close');
        var msg = "The added additional expense already exists.";
        $('#alertmsg').text(msg);
        $('#popupDialog').popup('open', { positionTo: 'window' });
        return false;
    };

    self.productionRelease = function () {
        var msg = "Orders cannot be Released to Production while the site is in Beta.";
        $('#alertmsg').text(msg);
        $('#popupDialog').popup('open', { positionTo: 'window' });
        return false;
    };

    self.isOrdersQuantityValid = function () {
        var msg = "";
        var valid_order_quantity = true;
        var storeId_pk_dicts = self.gData.storeIdToPkDict;
        if (storeId_pk_dicts != undefined && storeId_pk_dicts != null && storeId_pk_dicts != "") {
            var data = self.gData;
            $.each(data.summaryStoreList, function (key, val) {
                if (val.actualQuantity == null && val.estimatedQuantity == null) {
                    valid_order_quantity = false;
                    return false;
                }
            });
            if (!valid_order_quantity) {
                msg = "In order to submit, you must select targets and save mailing lists for each of the locations in the order.";
                $('#alertmsg').text(msg);
                window.setTimeout(function () {
                    $('#okBut').unbind('click');
                    $('#okBut').bind('click', function () {
                        $('#popupDialog').popup('close');
                    });
                    $('#popupDialog').popup('open');
                    return false;
                }, 1000);
            }
        }

        return valid_order_quantity;
    }

    self.submitOrder = function () {
        if (self.validateUpdateTotals('submit', '')) {
            if (isProductionCustomers()) {
                if (sessionStorage.progressJob != undefined && sessionStorage.progressJob != null && sessionStorage.progressJob != "" && sessionStorage.progressJob == sessionStorage.jobNumber) {
                    return false;
                }
                var submit_flag = "submit";
                var job_customer_number = (sessionStorage.jobCustomerNumber != undefined && sessionStorage.jobCustomerNumber != null && sessionStorage.jobCustomerNumber != "") ? sessionStorage.jobCustomerNumber : appPrivileges.customerNumber;
                if (job_customer_number == MILWAUKEE) {
                    submit_flag = "save"; //Submit stopped for beta release on 02/Aug/2017
                    self.submitType('save');
                }
                else {
                    var msg = self.validateApprovers();
                    if (msg != "") {
                        $('#alertmsg').html(msg);
                        window.setTimeout(function () {
                            $('#okBut').unbind('click');
                            $('#okBut').bind('click', function () {
                                $('#popupDialog').popup('close');
                            });
                            $('#popupDialog').popup('open');
                            return false;
                        }, 1000);
                    }
                }
                if (!isOrderLocationExists('_jobSubmitOrder')) { return false; }
                else if (!isVariableFieldListValid('_jobSubmitOrder')) { return false; }
                else if (isOfferSetupValidationRequired() && !isOfferListValid('_jobSubmitOrder')) { return false; }
                else if (!self.isOrdersQuantityValid()) { return false; }
                else {
                    if (job_customer_number == MILWAUKEE) {
                        if (self.reGenerateApprovalWithGmc()) {
                            self.fnJobFinal('save');
                        }
                    }
                    else {
                        $('#loadingText1').html('Please do not leave this page or close the window <br/>until you see the success message. <br/>It will take a minute or two for us to <br/>generate a final proof for your Co-op Approver.');
                        $('#waitPopUp').popup('open');
                        window.setTimeout(function () {
                            var tmp_updated_store_approvers = [];
                            var updated_approvers_info = ko.mapping.toJS(self.listStoreApprovers, mapping);
                            $.each(listStoreApproversCompare, function (key, val) {
                                $.each(updated_approvers_info, function (key1, val1) {
                                    if (val1.storeId == val.storeId && val1.approverEmail != val.approverEmail) {
                                        tmp_updated_store_approvers.push(val1);
                                    }
                                });
                            });
                            //if (tmp_updated_store_approvers.length > 0) {

                            var p_coop_approval_url = serviceURLDomain + "api/StoreStatus_approver_update/" + sessionStorage.facilityId + "/" + job_customer_number + "/" + self.jobNumber;
                            postCORS(p_coop_approval_url, JSON.stringify(tmp_updated_store_approvers), function (response) {
                                self.getLoadStoreApprovers();
                                if (self.reGenerateApprovalWithGmc()) {
                                    self.getJobReadinessStatus("submit");
                                }
                            }, function (response_error) {
                                $('#waitPopUp').popup('close');
                                showErrorResponseText(response_error, false);
                            });
                            //}
                            //else {
                            //    if (self.reGenerateApprovalWithGmc()) {
                            //        self.getJobReadinessStatus("submit");
                            //    }
                            //}
                            $('#waitPopUp').popup('close');
                        }, 300);
                    }
                }
                return false;
            }
            else if (showSiteInDemoMessage()) {
                var msg = "Orders cannot be Submitted to Production while the site is in Demo.";
                $('#alertmsg').text(msg);
                $('#popupDialog').popup('open', { positionTo: 'window' });
                return false;
            }

            var qty_msg = "";
            qty_msg = self.validateEstimatedQuantities();

            if (qty_msg != "") {
                $('#alertmsg').text(qty_msg);
                $('#popupDialog').popup('open', { positionTo: 'window' });
                return false;
            }

            var msg = "";

            if ([BBB, TRIBUNEPUBLISHING, SANDIEGO, AH, SPORTSAUTHORITY, WALGREENS, REDPLUM, SAFEWAY, LOWES].indexOf(jobCustomerNumber) > -1) {
                if (!self.artListUploaded()) return false;
            }
            if ($("#chkNoArtApprove").is(":checked") || $("#chkNoListApprove").is(":checked"))
                $("#btnContinuePopup").text("Continue");
            else
                $("#btnContinuePopup").text("Submit Order");

            var is_list_uploaded = false;
            var is_art_uploaded = false;
            var is_needs_count = false;

            if (self.gOutputData.standardizeAction.standardizeFileList != undefined && self.gOutputData.standardizeAction.standardizeFileList.length > 0 && self.gOutputData.standardizeAction.standardizeFileList[0].fileName != "")
                is_list_uploaded = true;

            if (self.gOutputData.artworkAction.artworkFileList != undefined && Object.keys(self.gOutputData.artworkAction.artworkFileList).length > 0)
                is_art_uploaded = true;

            if (self.gOutputData != undefined && self.gOutputData.selectedLocationsAction != undefined && self.gOutputData.selectedLocationsAction.selectedLocationsList != undefined && self.gOutputData.selectedLocationsAction.selectedLocationsList.length > 0) {
                $.each(self.gOutputData.selectedLocationsAction.selectedLocationsList, function (key, val) {
                    if (val.needsCount) {
                        is_needs_count = (jobCustomerNumber == DCA || jobCustomerNumber == SPORTSKING) ? false : true;
                        return false;
                    }
                });
            }
            if ($("#ulJobLinks li[id*=liListSelection]").length > 0) {
                if ((jobCustomerNumber != AAG) || (jobCustomerNumber == AAG && appPrivileges.roleName != "user"))
                    if (!is_art_uploaded || is_needs_count)
                        msg = "All artwork files should be upload and list selections made prior to submitting your order. Submitting your order will send Confirmation and Approval emails to the users listed on this page. Do you want to Continue to Submit this Order?";
            }
            else if (!is_list_uploaded || (!is_art_uploaded && [AAG, TRIBUNEPUBLISHING, SANDIEGO, AH, SPORTSAUTHORITY, WALGREENS, REDPLUM, SAFEWAY, LOWES].indexOf(jobCustomerNumber) == -1)) {
                var art_work_msg = (([AAG, TRIBUNEPUBLISHING, SANDIEGO, AH, REDPLUM].indexOf(jobCustomerNumber) > -1) ? '' : 'and artwork files ');
                var approval_email_msg = (([AAG, TRIBUNEPUBLISHING, SANDIEGO, AH, SPORTSAUTHORITY, WALGREENS, REDPLUM, SAFEWAY, LOWES].indexOf(jobCustomerNumber) > -1) ? '' : 'and Approval ');
                msg = "All lists " + art_work_msg + "should be upload prior to submitting your order. Submitting your order will send Confirmation " + approval_email_msg + "emails to the users listed on this page. Do you want to Continue to Submit this Order?";
            }
            if (msg != "") {
                $('#dvMsg').html(msg);
                $('#confirmAdmin').popup('open');
            }
            else {
                self.getJobReadinessStatus("submit");
            }
        }
    };

    self.validateEstimatedQuantities = function () {
        var est_not_found = false;
        var actual_qty_not_found = false;
        var qty_msg = "";
        $.each($('#configTable input[type="text"]'), function (key, val) {
            if ($(val).val() == "" || $(val).val() == "0" || $(val).val() == 0) est_not_found = true;
            var act_qty = $(val).parent().parent().parent().find('td[data-colType=actualQty]').text();
            if (act_qty == null || act_qty == "" || act_qty == "0" || act_qty == 0) actual_qty_not_found = true;
            if (est_not_found && actual_qty_not_found) {
                qty_msg = "The order cannot be " + ((self.submitType() == 'submit') ? 'submitted ' : 'saved ') + "without a quantity for each location. Please enter an estimated quantity if you do not have actual quantity values.";
                return false;
            }
        });
        return qty_msg
    };

    self.reGenerateApprovalWithGmc = function () {
        var is_regenerate_gmc_succeed = true;
        var job_customer_number = (sessionStorage.jobCustomerNumber != undefined && sessionStorage.jobCustomerNumber != null && sessionStorage.jobCustomerNumber != "") ? sessionStorage.jobCustomerNumber : appPrivileges.customerNumber;
        getCORS(serviceURLDomain + "api/Gmc/" + sessionStorage.facilityId + "/" + job_customer_number + "/" + sessionStorage.jobNumber + "/1", null, function (data) {
            is_regenerate_gmc_succeed = true;
        }, function (error_response) {
            is_regenerate_gmc_succeed = false;
            showErrorResponseText(error_response, true);
        });
        return is_regenerate_gmc_succeed;
    };

    self.getJobReadinessStatus = function (submit_flag) {
        diff = DiffObjects(jQuery.parseJSON(sessionStorage.jobSetupOutput), jQuery.parseJSON(sessionStorage.jobSetupOutputCompare));
        if (diff.length > 0) {
            $('#btnSubmitOrder').attr('disabled', '');
            $('#btnSubmitOrder')[0].disabled = false;
            $('#btnSubmitOrder').removeClass('ui-disabled');
            if (submit_flag == "submit")
                self.fnJobFinal("submit");
        }
        else {
            var submit_log_info = serviceURLDomain + "api/Readiness_submit/" + self.facilityId + '/' + jobCustomerNumber + "/" + self.jobNumber + '/';
            //var submit_log_info = serviceURLDomain + "api/Readiness_submit/" + self.facilityId + '/6000420/602098/';
            getCORS(submit_log_info, null, function (data) {
                if (submit_flag == "submit") {
                    if (data.status.toLowerCase() != "ok") {
                        self.loadLogsInfo(data);
                        $('#postReminder').popup('open', { positionTo: 'window' });
                    }
                    else {
                        self.fnJobFinal("submit");
                    }
                }
                else {
                    if (data.status.toLowerCase() != "ok") {
                        $('#btnSubmitOrder').attr('disabled', 'disabled');
                        $('#btnSubmitOrder')[0].disabled = true;
                        $('#btnSubmitOrder').addClass('ui-disabled');
                    }
                    else {
                        $('#btnSubmitOrder').attr('disabled', '');
                        $('#btnSubmitOrder')[0].disabled = false;
                        $('#btnSubmitOrder').removeClass('ui-disabled');
                    }

                }
            }, function (response_error) {
                showErrorResponseText(response_error, false);
            });
        }
    };

    self.listJobDetails = ko.observableArray([]);
    self.lastSubmittedDate = ko.observable({});
    self.loadLogsInfo = function (data) {
        //This function will display details info for selected logging
        self.listJobDetails([]);
        self.lastSubmittedDate(data.last_submitted_time);
        $.each(data.logList, function (a, b) {
            //for (i = 0; i < b.length; i++) {
            self.listJobDetails.push(b);
            //}
        });
    };

    self.fnJobFinal = function (action_type) {
        $('#confirmAdmin').popup('close');
        // post approvers info
        var approvers_info = {};
        $('#postReminder').popup('close');

        //submit job
        if ((appPrivileges.customerNumber === CW || appPrivileges.customerNumber === ALLIED || appPrivileges.customerNumber === DCA || appPrivileges.customerNumber === KUBOTA || appPrivileges.customerNumber === SPORTSKING || appPrivileges.customerNumber == GWA || appPrivileges.customerNumber == EQUINOX || isUpdateOutputDataNeeded()) && self.jobNumber != "-1") //Biz rules for needsBillingSave flag
            self.gOutputData = fnNeedsBillingSave(self.gOutputData);

        if (isProductionCustomers()) {
            self.gOutputData.isGmcDataUpdated = false;
        }

        if (appPrivileges.customerNumber === OH)
            postJobURL = serviceURLDomain + 'api/JobTicket_ohEmail/' + self.facilityId + '/' + appPrivileges.customerNumber + '/' + self.jobNumber + '/' + sessionStorage.username;
        else
            postJobURL = serviceURLDomain + 'api/JobTicket/' + self.facilityId + '/' + self.jobNumber + '/' + sessionStorage.username + '/' + action_type + '/ticket';

        postCORS(postJobURL, JSON.stringify(self.gOutputData), function (response) {
            var msg = (sessionStorage.jobNumber == "-1") ? 'Your job was saved.' : 'Your job was updated.';

            if (response != "") {
                if (!isNaN(parseInt(response.replace(/"/g, "")))) {
                    if (response.replace(/"/g, "").toLowerCase() != "success")
                        self.jobNumber = parseInt(response.replace(/"/g, ""));
                    sessionStorage.jobNumber = self.jobNumber;
                    self.gOutputData.jobNumber = self.jobNumber;
                    sessionStorage.jobDesc = (self.gOutputData.jobName != undefined) ? self.gOutputData.jobName : "";
                    $('#liJobSummary').css('display', "block");
                    displayJobInfo();
                    sessionStorage.jobSetupOutput = JSON.stringify(self.gOutputData);
                    sessionStorage.jobSetupOutputCompare = JSON.stringify(self.gOutputData);
                    $('#btnSubmitOrder').attr('disabled', 'disabled');
                    $('#btnSubmitOrder')[0].disabled = true;
                    $('#btnSubmitOrder').addClass('ui-disabled');
                }
                else
                    msg = response;

                if (appPrivileges.customerNumber == MILWAUKEE) {
                    action_type = "submit";
                }
                if (action_type != "submit")
                    $('#alertmsg').text(msg);
                else
                    $('#alertmsg').html(msg);

                //self.makeGData();
                window.setTimeout(function getDelay() {
                    if (action_type != "submit" && (msg == 'Your job was saved.' || msg == 'Your job was updated.')) {
                        $("#okBut").attr("onclick", "$('#popupDialog').popup('close');");
                        $('#popupDialog').popup('open', { positionTo: 'window' });
                    }

                    if (action_type == "submit") {
                        if (showThankyouPage()) {
                            if (jobCustomerNumber != MILWAUKEE) {
                                sessionStorage.progressJob = sessionStorage.jobNumber;
                                sessionStorage.progressJobMsgDisplayed = "true";//No need to display message in index page.
                            }
                            window.location.href = "thankYou.html";
                        }
                        else /*if (jobCustomerNumber == AAG && (is_valid_domain || appPrivileges.roleName == "admin") || jobCustomerNumber != AAG) */{
                            if (self.jobNumber < -1) {
                                sessionStorage.progressJob = sessionStorage.jobNumber;
                                sessionStorage.progressJobMsgDisplayed = "false";
                            } else {
                                sessionStorage.progressJob = response;
                                sessionStorage.progressJobMsgDisplayed = "false";
                            }

                            window.location.href = "../index.htm";
                        }
                    }

                }, 500);
            }
            else {
                var msg = 'Job creation failed.';
                $('#alertmsg').text(msg);
                $('#popupDialog').popup('open', { positionTo: 'window' });
            }
        }, function (response_error) {
            if (jobCustomerNumber != DCA && jobCustomerNumber != SPORTSKING) {
                var msg = 'Job creation failed.';
                msg = response_error.responseText;
                if (response_error.status == 409) {
                    msg = "Your job <b>" + self.gOutputData.jobName + "</b> could not be saved as there is already a job in the system with that name. Please rename your job and try again.";
                    $('#alertmsg').html(msg);
                }
                else
                    showErrorResponseText(response_error, false);
            }
            else {
                var msg = "Thank you! your order has been saved but will not be submitted for approval while in Beta.";
                $('#alertmsg').html(msg);
                $('#popupDialog').popup('open', { positionTo: 'window' });
            }

        });
    };

    self.displayOrderSummary = function () {
        var session_data = $.parseJSON(sessionStorage.jobSetupOutput)
        var order_summary = '';
        if (featureBillingInvoice()) {
            self.verifyBillingGenerationDate(session_data.inHomeDate);
        }

        if (session_data.jobNumber == "-1") {
            //order_summary = ' <h3>Order Summary</h3>';
            order_summary += '<div style="padding-bottom:12px;padding-top:10px;"><b>Job Name:</b> ' + session_data.jobName + ' <br /></div>';
            order_summary += '<div style="padding-bottom:12px;"><b>In-Home Date:</b> ' + session_data.inHomeDate + '<br /></div>';
            order_summary += '<div style="padding-bottom:12px;"><b>Template:</b> ' + self.templateName() + '<br /></div>';
            if (jobCustomerNumber != REDPLUM)
                order_summary += '<div style="padding-bottom:12px;"><b>Uploaded List Files: </b><br />NA<br /></div>';
            if (jobCustomerNumber != AAG && jobCustomerNumber != TRIBUNEPUBLISHING && jobCustomerNumber != SANDIEGO && jobCustomerNumber != AH && jobCustomerNumber != SPORTSAUTHORITY && jobCustomerNumber != WALGREENS && jobCustomerNumber != REDPLUM && jobCustomerNumber != SAFEWAY && jobCustomerNumber != LOWES) {
                order_summary += '<div style="padding-bottom:12px;"><b>Uploaded Art Files: </b><br />NA<br /></div>';
            }
        }
        else {
            //getCORS(gEditJobService, null, function (data) {
            var list_files_data = (session_data.standardizeAction.standardizeFileList == undefined || session_data.standardizeAction.standardizeFileList == null || session_data.standardizeAction.standardizeFileList == "" || Object.keys(self.gOutputData.standardizeAction.standardizeFileList).length == 0 || self.gOutputData.standardizeAction.standardizeFileList[0].fileName == "") ? [] : session_data.standardizeAction.standardizeFileList;
            var art_files_data = (session_data.artworkAction.artworkFileList == undefined || session_data.artworkAction.artworkFileList == null || session_data.artworkAction.artworkFileList == "" || Object.keys(session_data.artworkAction.artworkFileList).length == 0) ? [] : session_data.artworkAction.artworkFileList;
            //order_summary = ' <h3>Order Summary</h3>';
            order_summary += '<div style="padding-bottom:12px;padding-top:10px;"><b>Job Name:</b> ' + session_data.jobName + ' <br /></div>';
            order_summary += '<div style="padding-bottom:12px;"><b>In-Home Date:</b> ' + session_data.inHomeDate + '<br /></div>';
            order_summary += '<div style="padding-bottom:12px;"><b>Template:</b> ' + self.templateName() + '<br /></div>';
            //if (jobCustomerNumber != REDPLUM && (jobCustomerNumber == AAG && appPrivileges.roleName == "admin")) {
            if (jobCustomerNumber != REDPLUM && (jobCustomerNumber == AAG)) {// && (is_valid_domain || appPrivileges.roleName == "admin")
                order_summary += '<div style="padding-bottom:12px;"><b>Uploaded List Files: </b><br />';
                if (list_files_data.length > 0) {
                    $.each(list_files_data, function (key, val) {
                        order_summary += '<span>' + val.fileName + '</span><br />';
                    });
                }
                else {
                    order_summary += '<span>NA</span><br />';
                }
                order_summary += '</div>';
            }
            if (jobCustomerNumber != AAG && jobCustomerNumber != TRIBUNEPUBLISHING && jobCustomerNumber != SANDIEGO && jobCustomerNumber != AH && jobCustomerNumber != SPORTSAUTHORITY && jobCustomerNumber != WALGREENS && jobCustomerNumber != REDPLUM && jobCustomerNumber != SAFEWAY && jobCustomerNumber != LOWES && jobCustomerNumber != SPORTSKING && jobCustomerNumber != EQUINOX && ((jobCustomerNumber != KUBOTA || (jobCustomerNumber == KUBOTA && appPrivileges.roleName != "user"))) && showUploadedArtFilesInDivSummary()) {
                order_summary += '<div style="padding-bottom:12px;"><b>Uploaded Art Files: </b>';
                if (Object.keys(art_files_data).length > 0) {
                    order_summary += '<br />';
                    $.each(art_files_data, function (key, val) {
                        order_summary += '<span>' + val.fileName + '</span><br />';
                    });
                }
                else {
                    order_summary += '<span>NA</span><br />';
                }
                order_summary += '</div>';
            }
            if (jobCustomerNumber == CW || jobCustomerNumber == DCA || jobCustomerNumber == KUBOTA || jobCustomerNumber == TRIBUNEPUBLISHING || jobCustomerNumber == SANDIEGO || jobCustomerNumber == OH || jobCustomerNumber == GWA || jobCustomerNumber == EQUINOX || showUploadedListFilesInDivSummary()) {
                if (jobCustomerNumber == CW || jobCustomerNumber == TRIBUNEPUBLISHING || jobCustomerNumber == SANDIEGO || jobCustomerNumber == OH || jobCustomerNumber == KUBOTA || jobCustomerNumber == GWA || jobCustomerNumber == EQUINOX || showUploadedListFilesInDivSummary()) {
                    order_summary += '<div style="padding-bottom:12px;"><b>Uploaded Source Files: </b>';
                    if (Object.keys(list_files_data).length > 0) {
                        var is_found = false;
                        $.each(list_files_data, function (key, val) {
                            if (val.sourceType == 1) {
                                if (!is_found) { order_summary += '<br />'; }
                                order_summary += '<span>' + val.fileName + '</span><br />';
                                is_found = true;
                            }
                        });
                        if (!is_found)
                            order_summary += '<span>NA</span><br />';
                    }
                    else {
                        order_summary += '<span>NA</span><br />';
                    }
                    order_summary += '</div>';
                }

                order_summary += '<div style="padding-bottom:12px;"><b>Loaded Seed Files: </b>';
                if (Object.keys(list_files_data).length > 0) {
                    var is_found = false;
                    $.each(list_files_data, function (key, val) {
                        if (val.sourceType == 6) {
                            if (!is_found) { order_summary += '<br />'; }
                            order_summary += '<span>' + val.fileName + '</span><br />';
                            is_found = true;
                        }
                    });
                    if (!is_found)
                        order_summary += '<span>NA</span><br />';
                }
                else {
                    order_summary += '<span>NA</span><br />';
                }
                order_summary += '</div>';

                order_summary += '<div style="padding-bottom:12px;"><b>Loaded Do Not Mail Files: </b>';
                if (Object.keys(list_files_data).length > 0) {
                    var is_found = false;
                    $.each(list_files_data, function (key, val) {
                        if (val.sourceType == 2) {
                            if (!is_found) { order_summary += '<br />'; }
                            order_summary += '<span>' + val.fileName + '</span><br />';
                            is_found = true;
                        }
                    });
                    if (!is_found)
                        order_summary += '<span>NA</span><br />';
                }
                else {
                    order_summary += '<span>NA</span><br />';
                }
                order_summary += '</div>';
            }

        }
        $('#dvOrderContent').append(order_summary);
        $('#dvOrderSummary').css('display', 'block');
    };

    self.getEmailLists = function () {
        window.setTimeout(function delay() {
            $.each($('#dvEmailRecipients div[data-role=collapsible]'), function () {
                $(this).on("collapsiblecollapse", function (e) {
                    $(this).data("previous-state", "collapsed");
                    //$(this).find('.ui-input-clear').unbind('click');
                });
                $(this).on("collapsibleexpand", function (e) {
                    $(this).data("previous-state", "expanded");
                    //$('.ui-content').on('click', '.ui-input-clear', function (e) {
                    self.getData(this.id);
                });
                $(this).find('.ui-input-clear').bind('click', function (e) {
                    if (e.originalEvent) {
                        vm.search();
                    }
                });
            });
        }, 500);
        $('#dvEmailRecipients').collapsibleset('refresh');
        $('#dvEmailRecipients').trigger('create');
        $('#editEmailListPopup').popup('open');
    };

    self.artListUploaded = function () {
        var msg = '';
        if (self.gOutputData.standardizeAction.standardizeFileList == undefined || self.gOutputData.standardizeAction.standardizeFileList == null || self.gOutputData.standardizeAction.standardizeFileList == "" || Object.keys(self.gOutputData.standardizeAction.standardizeFileList).length == 0) {
            if (msg != undefined && msg != "" && msg != null)
                msg += "<br />Please select at least one List file.";
            else
                msg = "Please select at least one List file.";
        }
        else {
            var list_file_cnt = 0;
            $.each(self.gOutputData.standardizeAction.standardizeFileList, function (key, val) {
                if (val.fileName != "") {
                    var file_source_type = getFileType(val.sourceType);
                    if (file_source_type.toLowerCase() == "source") {
                        list_file_cnt++;
                    }
                }
            });
            if (list_file_cnt == 0) {
                if (msg != undefined && msg != "" && msg != null)
                    msg += "<br />Please select at least one List (Source) file.";
                else
                    msg = "Please select at least one List file.";
            }
        }
        if ((jobCustomerNumber != AAG && jobCustomerNumber != TRIBUNEPUBLISHING && jobCustomerNumber != SANDIEGO && jobCustomerNumber != AH && jobCustomerNumber != SPORTSAUTHORITY && jobCustomerNumber != WALGREENS && jobCustomerNumber != REDPLUM && jobCustomerNumber != SAFEWAY && jobCustomerNumber != LOWES) && (self.gOutputData.artworkAction.artworkFileList == undefined || self.gOutputData.artworkAction.artworkFileList == null || self.gOutputData.artworkAction.artworkFileList == "" || Object.keys(self.gOutputData.artworkAction.artworkFileList).length == 0)) {
            if (msg != undefined && msg != "" && msg != null)
                msg += "<br />Please select at least one Art file";
            else
                msg = "Please select at least one Art file";
        }
        if (msg != "") {
            $('#alertmsg').html(msg);
            $('#alertmsg').css('text-align', 'left');
            $("#popupDialog").popup('open');
            $("#okBut").attr("onclick", "$('#popupDialog').popup('close');$('#popupComments').popup('open');");
            return false;
        }
        return true;
    };

    self.populateInvoiceOutputForm = function (report_name, data, event) {
        $("#hdnInvoiceSelected").val(report_name);
        $("#selectOutputType").val('pdf').selectmenu('refresh');
        report_name = report_name.replace(/_/g, '');
        switch (report_name) {
            case "PostageInvoice":
                self.getStoreIdList();
                $("#popupInvoiceOutput h3").html("Postage Invoice");
                $("#popupInvoiceOutput").popup("open");
                break;
            case "BillingInvoice":
                self.getStoreIdList();
                $("#popupInvoiceOutput h3").html("Billing Invoice");
                $("#popupInvoiceOutput").popup("open");
                break;
            case "GenerateBill":
                self.viewInvoiceDetails();
                break;
        };
    };

    //Create store list that were ordered for that job.
    self.getStoreIdList = function () {
        if (self.gOutputData != undefined) {
            $('#divSelectStoreId').empty();
            var store_ids = '<label for="ddlStoreId">StoreID:</label><select id="ddlStoreId" name="ddlStoreId" data-mini="true" data-theme="f">';
            store_ids += '<option value="select">Select</option>';
            $.each(self.gOutputData.selectedLocationsAction.selectedLocationsList, function (key, obj) {
                //store_ids += '<option value= "' + obj.storeId + '">' + obj.storeId + '</option>';
                store_ids += '<option value= "' + obj.pk + '">' + obj.storeId + '</option>';
            });
            store_ids += '</select>'
            $('#divSelectStoreId').append(store_ids);
            $('#divSelectStoreId').trigger('create');
        }
    };

    self.viewInvoiceDetails = function () {
        var invoice_selected = $('#hdnInvoiceSelected').val();
        // var selected_store = '';
        var input_report_params = [];
        var report_name = invoice_selected.replace(/_/g, '');
        var output_type = $("#selectOutputType").val();
        $("#popupInvoiceOutput").popup("close");
        switch (report_name) {
            case "PostageInvoice":
                //selected_store = $('select[name=ddlStoreId]').val();
                input_report_params.push({
                    "name": "customerId",
                    "value": jobCustomerNumber
                });
                input_report_params.push({
                    "name": "facilityId",
                    "value": (typeof pageObj != 'undefined' && typeof pageObj != 'null') ? pageObj.facilityId : facilityId
                });
                input_report_params.push({
                    "name": "jobNumber",
                    "value": (typeof pageObj != 'undefined' && typeof pageObj != 'null') ? pageObj.jobNumber : jobNumber
                });
                input_report_params.push({
                    "name": "storePk",
                    "value": $("#ddlStoreId").val()
                });
                break;
            case "BillingInvoice":
                input_report_params.push({
                    "name": "customerId",
                    "value": jobCustomerNumber
                });
                input_report_params.push({
                    "name": "facilityId",
                    "value": (typeof pageObj != 'undefined' && typeof pageObj != 'null') ? pageObj.facilityId : facilityId
                });
                input_report_params.push({
                    "name": "jobNumber",
                    "value": (typeof pageObj != 'undefined' && typeof pageObj != 'null') ? pageObj.jobNumber : jobNumber
                });
                input_report_params.push({
                    "name": "storePk",
                    "value": $("#ddlStoreId").val()
                });
                //selected_store = $('select[name=ddlStoreId]').val();
                break;
            case "GenerateBill":
                //selected_store = $('select[name=ddlStoreId]').val();
                input_report_params.push({
                    "name": "customerId",
                    "value": jobCustomerNumber
                });
                input_report_params.push({
                    "name": "facilityId",
                    "value": (typeof pageObj != 'undefined' && typeof pageObj != 'null') ? pageObj.facilityId : facilityId
                });
                input_report_params.push({
                    "name": "jobNumber",
                    "value": (typeof pageObj != 'undefined' && typeof pageObj != 'null') ? pageObj.jobNumber : jobNumber
                });
                break;
        };
        self.createJSONForReport(report_name, output_type, input_report_params, 'gizmo', sessionStorage.jobNumber);
    };

    self.createJSONForReport = function (report_name, output_type, input_report_params, ssrsFolder, jobNumber) {
        var json_report = {};
        json_report["ssrsFolder"] = ssrsFolder;
        json_report["outputType"] = output_type;
        json_report["reportName"] = report_name.replace(/_/g, ' ');
        json_report["parameters"] = self.createReportParamJSON(report_name, input_report_params, jobNumber);
        //$('#overlay').css('display', 'block');
        $('#waitPopUp').popup('open');
        setTimeout(function () { self.fnDisplayReports(json_report, output_type); }, 1000);
    }

    //create a array of input parameters for the selected report and return input parameters array.
    self.createReportParamJSON = function (rpt_name, param_json, jobNumber) {
        var customer_number = appPrivileges.customerNumber;
        switch (rpt_name.replace(/_/g, ' ').toLowerCase()) {
            case "billing invoice":
                var facility_id = appPrivileges.facility_id;
                param_json.push({
                    "name": "customerNumber",
                    "value": customer_number
                });
                param_json.push({
                    "name": "jobNumber",
                    "value": jobNumber
                });
                param_json.push({
                    "name": "facilityId",
                    "value": facility_id
                });
                break;
        }
        return param_json;
    }

    //calls the webservice to show the SSRS reports based on the user selection report
    self.fnDisplayReports = function (json_report, output_type) {
        var report_URL = reportsURL + "api/reportv2_setreportinfo";
        window.setTimeout(function () { $('#waitPopUp').popup('close'); }, 300);
        postCORS(report_URL, JSON.stringify(json_report), function (response) {
            window.setTimeout(function () { $('#waitPopUp').popup('close'); }, 300);
            //remove this line after service implementation is done.
            //    if (json_report.reportName == "GenerateBill")//remove this line after service implementation is done.
            //        self.isBillGenerationClicked(true);//remove this line after service implementation is done.

            self.windowOpen(output_type, json_report.reportName);

            //    //Uncomment when service implementation is completed
            //    //if (json_report.reportName == "GenerateBill")
            //    //    self.isBillGenerationClicked(true);
            //    //setTimeout(function getDelay() { setTimeout }, 500);
            //    //window.setTimeout(function () { $('#waitPopUp').popup('close'); }, 300);

        }, function (response_error) {
            window.setTimeout(function () { $('#waitPopUp').popup('close'); }, 300);
            showErrorResponseText(response_error, false);
        });
    };

    //to open the reports in a window (iframe), this method is called in "fnDisplayReports".
    self.windowOpen = function (output_type, report_name) {
        var content_type = '';
        var htmlText = "";
        var report_preview_url = reportsURL + "api/reportv2/" + ((sessionStorage.customerNumber == DCA || appPrivileges.customerNumber == SPORTSKING) ? sessionStorage.userRole + "_td1" : sessionStorage.username) + "_" + report_name.replace(/\s/g, "_");
        //removeLoadingImg();
        $('#popPDF').popup('close');
        switch (output_type) {
            case "pdf":
                content_type = ' type="application/pdf"'
                break;
            case "doc":
                content_type = ' type="application/msword"'
                break;
            case "xls":
                content_type = ' type="application/x-msexcel"'
                break;
            case "image":
                content_type = ' type="image/tiff" src="data:image/tiff;base64,'
                break;
        }

        $('#popPDF').popup('close');
        var htmlText = "";

        htmlText = '<iframe width=850 HEIGHT=500 id="rptFrame"'
                          + content_type
                          + ' src="' + report_preview_url
                          + '"></iframe>';

        if (output_type == 'pdf' || output_type == 'xml') {
            $('#popupPDFContent').html(htmlText);
        } else {
            $('#popupPDFContent').html(htmlText);
        }

        var iframe = document.getElementById('rptFrame');

        //DOMContentLoaded
        if (iframe.addEventListener) {
            iframe.addEventListener('onload', callback(output_type), true);
        } else {
            iframe.attachEvent('onload', callback(output_type));
        }

        if (output_type == 'pdf' || output_type == 'xml') {
            var wait_ctrl = '<div id="dvWaitImage" >';
            $('#waitPopUp').popup('open');
            $('#waitPopUp').popup('close');
            wait_ctrl += $('#waitPopUp').html() + "</div>";
            $('#popupPDFContent').prepend(wait_ctrl);
            //var maxHeight = ($('#popPDF').height() / 2) - 190 + "px";
            //var maxWidth = ($('#popPDF').width() / 2 - 80) + "px";
            var maxHeight = ($('#popPDF').height() / 2) - 190 + "px";
            var maxWidth = ($('#popPDF').width() / 2) + 340 + "px";
            $('#dvWaitImage').css('position', 'absolute');
            $("#dvWaitImage").css("margin-top", maxHeight);
            $("#dvWaitImage").css("margin-left", maxWidth);

            $('#popPDF').popup('open', { positionTo: 'window' });
        }
    };

    self.validateUpdateTotals = function (submit_type, click_type) {
        if (submit_type == "submit")
            self.submitType('submit');
        else
            self.submitType('save');

        if ([AAG].indexOf(jobCustomerNumber) > -1)
            return true;
        if (self.wantsEstimateColumns()) {
            var qty_msg = "";
            qty_msg = self.validateEstimatedQuantities();

            if (qty_msg != "") {
                $('#alertmsg').text(qty_msg);
                $('#popupDialog').popup('open', { positionTo: 'window' });
                return false;
            }
        }
        if (self.qtyChanged()) {
            $('#confirmValidationMsg').html("You must update totals before you continuing.");
            $('#popupUpdateTotalsValidation').popup('open');
            return false;
        }
        else {
            var job_customer_number = (sessionStorage.jobCustomerNumber != undefined && sessionStorage.jobCustomerNumber != null && sessionStorage.jobCustomerNumber != "") ? sessionStorage.jobCustomerNumber : appPrivileges.customerNumber;
            if (submit_type != "submit" && (job_customer_number == SCOTTS || job_customer_number == ACE)) {
                var tmp_updated_store_approvers = [];
                var updated_approvers_info = ko.mapping.toJS(self.listStoreApprovers, mapping);
                $.each(listStoreApproversCompare, function (key, val) {
                    $.each(updated_approvers_info, function (key1, val1) {
                        if (val1.storeId == val.storeId && val1.approverEmail != val.approverEmail) {
                            tmp_updated_store_approvers.push(val1);
                        }
                    });
                });
                //if (tmp_updated_store_approvers.length > 0) {
                var is_approvers_updated = false;
                var p_coop_approval_url = serviceURLDomain + "api/StoreStatus_approver_update/" + sessionStorage.facilityId + "/" + job_customer_number + "/" + self.jobNumber;
                postCORS(p_coop_approval_url, JSON.stringify(tmp_updated_store_approvers), function (response) {
                    is_approvers_updated = true;
                    self.getLoadStoreApprovers();
                }, function (response_error) {
                    showErrorResponseText(response_error, false);
                    is_approvers_updated = false;
                });
                if (!is_approvers_updated) {
                    $('#alertmsg').html("Failed to update store approvers");
                    $("#okBut").attr("onclick", "$('#popupDialog').popup('close');");
                    window.setTimeout(function () {
                        $("#popupDialog").popup('open');
                    }, 200);
                    return false;
                }
                //}
            }
            $('#popupUpdateTotalsValidation').popup('close');
            return true;
        }
    };
    self.submitType = ko.observable('');
    self.validationContinueClick = function () {
        $('#popupUpdateTotalsValidation').popup('close');
        window.setTimeout(function () {
            if (self.submitType() == 'save') {
                self.submitType('');
                postJobSetUpData('save');
            }
            else {
                pageObj.submitOrder();
            }
        }, 500);
    };

    //0 - Sunday, 3 - Wednesday, 4 - Thursday
    self.verifyBillingGenerationDate = function (in_home_date) {
        var today = new Date();
        var no_of_prior_days = 3;
        var date_parts = null;
        if (in_home_date.indexOf('/') > -1) {
            date_parts = in_home_date.split('/');
        }
        else if (in_home_date.indexOf('-') > -1) {
            date_parts = in_home_date.split('-');
        }
        if (date_parts != null && date_parts.length > 2) {
            if (date_parts[2].length == 2)
                date_parts[2] = '20' + date_parts[2];
            var billing_generation_date = new Date(Number(date_parts[2]), Number(date_parts[0]) - 1, Number(date_parts[1]), 0, 0, 0, 0);
            if (billing_generation_date.getDay() - 3 <= 0)
                no_of_prior_days = 4;
            billing_generation_date.setDate(billing_generation_date.getDate() - no_of_prior_days);
            self.isBillGenerationClicked(getNumberOfDaysBetweenDates(billing_generation_date, today) >= 0 ? true : false);
        }
    };

    //Populate Selected Report Parameters form
    self.populateSelectedReportParameters = function (data, event) {
        if (sessionStorage.jobReportsData != undefined && sessionStorage.jobReportsData != null && sessionStorage.jobReportsData != "") {
            var current_element = event.target || event.currentTarget;
            var ssrs_name = $(current_element).attr("report-type");
            var reportsInfo = $.parseJSON(sessionStorage.jobReportsData);
            $('#btnCancelReportGeneration').attr('data-ReportType', ssrs_name);
            $('#btnSubmitReportGeneration').attr('data-ReportType', ssrs_name);

            $("#dvReportParameters").empty();
            $("#dvOutputType").empty();
            var report_obj;
            $.each(reportsInfo, function (key, val) {
                if (key == "reports" && val.reportList != undefined && val.reportList != null) {
                    $.each(val.reportList, function (rpt_key, rpt_val) {
                        if (rpt_val.ssrsName == ssrs_name) {
                            report_obj = rpt_val;
                            return false;
                        }
                    });
                    return false;
                }
            });
            if (report_obj != undefined && report_obj != null && report_obj != "") {
                $('#btnCancelReportGeneration').attr('data-ReportName', report_obj.name);
                $('#btnSubmitReportGeneration').attr('data-ReportName', report_obj.name);
                var report_params = "";
                $.each(report_obj.reportParamList, function (key, val) {
                    switch (val.controlType.toLowerCase()) {
                        case "dropdown":
                            report_params += '<div data-role="fieldcontain">';
                            report_params += '<label for="' + val.name + '">' + val.label + ': </label>';
                            report_params += '<select id="' + val.name + '" name="' + val.name + '" data-native-menu="false" data-theme="f" data-mini="true">';
                            report_params += '</select>';
                            report_params += '</div>';
                            break;
                        case "text":
                            report_params += '<div data-role="fieldcontain">';
                            report_params += '<label for="' + val.name + '">' + val.label + ': </label>';
                            report_params += '<input type="text" id="' + val.name + '" value="' + val.value + '" data-mini="true" />';
                            report_params += '</div>';
                            break;
                        default:
                            break;
                    }
                });
                if (report_params != "") {
                    var dvReportParameters = $("#dvReportParameters");
                    $(report_params).appendTo(dvReportParameters);
                    $("#dvReportParameters").trigger('create');
                }
                report_params = "";
                if (report_obj.outputType.controlType.toLowerCase() == "dropdown") {
                    $.each(report_obj.outputType, function (key, val) {
                        if (key == "reportOptionList") {
                            report_params += '<div data-role="fieldcontain">';
                            report_params += '<label for="' + report_obj.outputType.name + '">' + report_obj.outputType.label + ': </label>';
                            report_params += '<select id="' + report_obj.outputType.name + '" name="' + report_obj.outputType.name + '" data-native-menu="false" data-theme="f" data-mini="true">';
                            $.each(val, function (reportOptionkey, reportOptionValue) {
                                if (reportOptionValue.selected == "1")
                                    report_params += '<option value="' + reportOptionValue.name + '" selected>' + reportOptionValue.label + '</option>'
                                else {
                                    report_params += '<option value="' + reportOptionValue.name + '">' + reportOptionValue.label + '</option>'
                                }
                            });
                            report_params += '</select>';
                            report_params += '</div>';
                        }
                    });
                    if (report_params != "") {
                        var dvReportParameters = $("#dvOutputType");
                        $(report_params).appendTo(dvReportParameters);
                        $("#dvOutputType").trigger('create');
                    }
                }
            }
            else {
                $('#alertmsg').html("Report doesn't exist");
                $('#alertmsg').css('text-align', 'left');
                $("#okBut").attr("onclick", "$('#popupDialog').popup('close');");
                window.setTimeout(function () {
                    $("#popupDialog").popup('open');
                }, 200);
                return false;
            }
            window.setTimeout(function () {
                $("#popupReportParameters").popup("open");
            }, 200);
        }
    };

    self.submitReportGenerationClick = function (data, e) {
        if (sessionStorage.jobReportsData != undefined && sessionStorage.jobReportsData != null && sessionStorage.jobReportsData != "") {
            var reportsInfo = $.parseJSON(sessionStorage.jobReportsData);
            var report_name = $('#btnSubmitReportGeneration').attr('data-ReportName');
            var ssrs_name = $('#btnSubmitReportGeneration').attr('data-ReportType');
            var output_type = $('#dvOutputType select').val();
            $('#popupReportParameters').popup('close');
            $('#popupPDFContent').html('<iframe width=850 height=500 id="rptFrame"></iframe>');
            var report_json = {};
            var is_found = false;
            $.each(reportsInfo, function (key, val) {
                if (key == "reports" && val.reportList != undefined && val.reportList != null) {
                    $.each(val.reportList, function (rpt_key, rpt_val) {
                        if (rpt_val.ssrsName == ssrs_name) {
                            report_json = rpt_val;
                            return false;
                        }
                    });
                    return false;
                }
            });

            if (Object.keys(report_json).length > 0 && report_json.reportParamList.length > 0) {
                $.each(report_json.reportParamList, function (param_key, param_val) {
                    param_val.value = $('#' + param_val.name).val();
                });
            }

            window.setTimeout(function () {
                $('#waitPopUp').popup('open');
                openReport(report_name, output_type, report_json);
            }, 1000);
        }
    };

    function callback(output_type) {
        var is_loaded = false;
        timeInterval = setInterval(function () {
            //$('#rptFrame')[0].contentDocument.message == "Access is denied.\r\n"
            //if (((navigator.userAgent.indexOf("MSIE") > -1 && !window.opera) && typeof ($('#rptFrame')[0].contentDocument) == "unknown") ||
            //        ((navigator.userAgent.indexOf("MSIE") == -1) && (typeof ($('#rptFrame')[0].contentDocument) == "object"))) {
            is_loaded = true;
            clearInterval(timeInterval);
            timeInterval = "";

            if (output_type != 'pdf' && output_type != 'xml') {
                if (window.location.href.toString().toLowerCase().indexOf('regionalhome') > -1) {
                    setTimeout(function getDelay() {
                        //$('#popPDF').popup('open');
                        $('#popPDF').popup('close');
                        $('#waitPopUp').popup('open');
                        $('#waitPopUp').popup('close');
                    }, 8500);
                }
                else {
                    setTimeout(function getDelay() {
                        $('#popPDF').popup('close');
                        $('#waitPopUp').popup('open');
                        $('#waitPopUp').popup('close');
                    }, 3000);
                }
            }
            else {
                //if (navigator.userAgent.indexOf("MSIE") > -1 && !window.opera) {

                setTimeout(function setTimer() {
                    $('#waitPopUp').popup('open');
                    $('#waitPopUp').popup('close');
                    removeLoadingImg();
                    $('#dvWaitImage').empty();
                }, 5000);
                //                }
                //                else {
                //                    $('#dvWaitImage').empty();
                //                }
            }
            //}
        }, 1000);
    }
}
