﻿showSelectLocationValidation = function () {
    var customer = getCustomerNumber();
    return ([DKI, ICYNENE].indexOf(customer) > -1);
}

showSelectedArtworkDemoHint = function () {
    var customer = getCustomerNumber();    
    return ([DKI, ICYNENE, BBB, MOTIV8, CW, DCA, KUBOTA, SPORTSKING, GWA, CASEYS, SCOTTS, ACE, ANDERSEN, EQUINOX, ALLIED, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);
}

isCWUploadArtPreferences = function () {
    var customer = getCustomerNumber();
    return ([DKI, ICYNENE, "203", BRIGHTHOUSE, BBB, DATAWORKS, MOTIV8, DCA, KUBOTA, OH, SPORTSKING, GWA, CASEYS, EQUINOX, ALLIED, REGIS, SCOTTS, ACE, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);
}

isCreateUploadArtDemoHints = function () {
    var customer = getCustomerNumber();    
    return ([DKI, ICYNENE, BBB, MOTIV8, CW, DCA, KUBOTA, OH, SPORTSKING, GWA, CASEYS, SCOTTS, ACE, ANDERSEN, EQUINOX, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER, ALLIED].indexOf(customer) > -1);
}

showUploadArtDemoHints = function () {
    var customer = getCustomerNumber();     
    return ([DKI, ICYNENE, BBB, MOTIV8, CW, DCA, KUBOTA, OH, SPORTSKING, GWA, CASEYS, SCOTTS, ACE, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER, EQUINOX, ALLIED].indexOf(customer) > -1);
}

changeUploadArtHeaderTextNeeded = function () {
    var customer = getCustomerNumber();    
    return ([DKI, ICYNENE, DCA, SCOTTS, ACE, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER, KUBOTA, OH, SPORTSKING, GWA, CASEYS, EQUINOX, ALLIED].indexOf(customer) > -1);
}

isLoadOrderedLocations = function (customer_number) {
    var customer = customer_number;
    if (customer == "" || customer == undefined || customer == null) {
        customer = getCustomerNumber();
    }    
    return ([DKI, ICYNENE, CW, SCOTTS, ACE, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER, DCA, KUBOTA, SPORTSKING, GWA, CASEYS, EQUINOX, ALLIED].indexOf(customer) > -1);
}

isLocationsExist = function(){
    var customer = getCustomerNumber();
    return ([CW, SCOTTS, BRIGHTHOUSE, BBB, DATAWORKS, DCA, KUBOTA, SPORTSKING, GWA, CASEYS, EQUINOX, ALLIED, ACE, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);     
}

isOrderLocationsNeeded = function (customer) {
    if (customer == undefined || customer == "" || customer == null) {
        customer = getCustomerNumber();
    }
    return ([DKI, ICYNENE, CW, SCOTTS, ACE, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER, DCA, KUBOTA, SPORTSKING, GWA, CASEYS, EQUINOX, ALLIED].indexOf(customer) > -1);
}

hideVersionCode = function () {
    var hide_version_code = false;
    var customer = getCustomerNumber();
    var role = getUserRole();

    switch (role) {       
        case 'admin':
            hide_version_code = ([DKI, ICYNENE, MOTIV8, DCA, KUBOTA, SPORTSKING, GWA, CASEYS, CW, SCOTTS, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER, EQUINOX, ALLIED, ACE].indexOf(customer) > -1);
            break;
        case 'power':
            hide_version_code = ([DKI, ICYNENE, MOTIV8, DCA, KUBOTA, SPORTSKING, GWA, CASEYS, CW, SCOTTS, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER, EQUINOX, ALLIED, ACE].indexOf(customer) > -1);
            break;
        case 'user':
            hide_version_code = true;
            break;
        default: break;
    }
    return hide_version_code;
}

showUploadSelectedArtDemoHint = function () {
    var customer = getCustomerNumber();    
    return ([DKI, ICYNENE, BBB, MOTIV8, CW, DCA, KUBOTA, SPORTSKING, GWA, CASEYS, SCOTTS, EQUINOX, ALLIED, ACE, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);
}

isRedirectFromArtworkToApproval = function () {
    var customer = getCustomerNumber();
    return ([DKI, ICYNENE, DCA, SCOTTS, ANDERSEN, KUBOTA, SPORTSKING, GWA, CASEYS, EQUINOX, ALLIED, ACE, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);
}

hideEditLocationAssignment = function () {
    var customer = getCustomerNumber();
    return ([DKI, ICYNENE].indexOf(customer) == -1);
}

showPreviousArtworkAssignmentMessage = function () {
    var customer = getCustomerNumber();    
    return ([DKI, ICYNENE, CW, SCOTTS, ANDERSEN, DCA, KUBOTA, SPORTSKING, GWA, CASEYS, EQUINOX, ALLIED, ACE, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);
}

showReplaceArtworkConfirmation = function () {
    var customer = getCustomerNumber();
    return ([DKI, ICYNENE, CW, SCOTTS, ANDERSEN, DCA, KUBOTA, SPORTSKING, GWA, CASEYS, EQUINOX, ALLIED, ACE, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);
}

showRemoveArtworkConfirmation = function () {
    var customer = getCustomerNumber();
    return ([DKI, ICYNENE, CW, SCOTTS, ANDERSEN, DCA, KUBOTA, SPORTSKING, GWA, CASEYS, EQUINOX, ALLIED, ACE, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);
}

showArtworkReplacementValidation = function () {
    var customer = getCustomerNumber();
    return ([SCOTTS, ACE, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);
}

showReplacementDuplicateValidation = function () {
    var customer = getCustomerNumber();    
    return ([SCOTTS, ACE, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);
}

showReplacementsUploadedValidation = function () {
    var customer = getCustomerNumber();
    return ([SCOTTS, ACE, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);
}

isArtworkReplacementsCustomer = function (customer_number) {
    var customer = customer_number;
    if (customer == "" || customer == undefined || customer == null) {
        customer = getCustomerNumber();
    }    
    return ([SCOTTS, ACE, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);
}

showLocationRequiredValidation = function () {
    var customer = getCustomerNumber();
    return ([SCOTTS, ACE, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) == -1);
}

showApprovalType = function () {
    var show_approval_type = false;
    var customer = getCustomerNumber();
    var role = getUserRole();

    switch (role) {        
        case 'power':
            show_approval_type = ([REGIS, SCOTTS, ACE, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);
            break;
        case 'user':
            show_approval_type = ([REGIS, SCOTTS, ACE, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);
            break;
        default: break;
    }

    return show_approval_type;
}

hideClientProofing = function () {
    var customer = getCustomerNumber();
    return ([DCA, KUBOTA, OH, SPORTSKING, GWA, CASEYS, EQUINOX, ALLIED].indexOf(customer) > -1);
}

var isCheckSelectedLocations = function () {
    var customer = getCustomerNumber();
    return ([SCOTTS, ACE, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);
}
