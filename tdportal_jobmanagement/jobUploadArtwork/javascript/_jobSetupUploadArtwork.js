﻿//******************** Global Variables Start **************************
var pagePrefs = "";
var pageObj;
var isDragDrop = false;
var isMultiple = false;
var formData;
var data = [];
var temp_data = [];
var temp_edit_file = [];
var dontShowHintsAgain = (localStorage.getItem('doNotShowHintsAgain') != undefined && localStorage.getItem('doNotShowHintsAgain') != null && localStorage.getItem('doNotShowHintsAgain') != "" && localStorage.getItem('doNotShowHintsAgain') != "false") ? true : false;
//******************** Global Variables End **************************

//******************** Page Load Events Start **************************

$(window).on('popupbeforeposition', 'div:jqmData(role="popup")', function () {
    var notDismissible = $(this).jqmData('dismissible') === false;
    if (notDismissible) {
        $('.ui-popup-screen').off();
    }
});

$(document).keyup(function (e) {
    if (e.keyCode == 27) {
        return false;
    }
});

$('#_jobSetupUploadArtwork').live('pagebeforecreate', function (event) {
    displayNavLinks();
    $("#estimatedQuantity").ForceNumericOnly();
    displayMessage('_jobSetupUploadArtwork');

    pageObj = new manageUploadArtwork();
    pageObj.displayValidationMessage('_jobSetupUploadArtwork');
    if (jobCustomerNumber == GWA) {
        ko.applyBindings(pageObj);
    }
    //confirmMessage();
    createConfirmMessage('_jobSetupUploadArtwork');
    if (sessionStorage.uploadArtworkPrefs == undefined || sessionStorage.uploadArtworkPrefs == null || sessionStorage.uploadArtworkPrefs == "")
        if (isCWUploadArtPreferences())
            getPagePreferences('uploadArtwork.html', sessionStorage.facilityId, CW);
        else
            getPagePreferences('uploadArtwork.html', sessionStorage.facilityId, jobCustomerNumber);
    else {
        managePagePrefs(jQuery.parseJSON(sessionStorage.uploadArtworkPrefs));
    }
    //load IFRAME for IE to upload files.
    loadUploadIFrame();
    //test for mobility...
    //loadMobility();
    loadingImg("_jobSetupUploadArtwork");
    $("#popPDF").on({
        popupbeforeposition: function () {
            var maxHeight = $(window).height() - 80 + "px";
            $("#popPDF img").css("max-height", maxHeight);
            $("#popPDF embed").css("max-height", maxHeight);
            var maxWidth = $(window).width() - 80 + "px";
            $("#popPDF img").css("max-width", maxWidth);
            $("#popPDF embed").css("max-width", maxWidth);
        }
    });
    sessionStorage.removeItem('artFilesToUpload');
    $("#navLinksPanel").on("panelopen", function (event, ui) {
        sessionStorage.isNavPanelOpen = true;
    });
    if (isCreateUploadArtDemoHints()) {
        setDemoHintsSliderValue();
        window.setTimeout(function loadHints() {
            createDemoHints("jobUploadArtwork");
        }, 100);
    }
});

$(document).on('pageshow', '#_jobSetupUploadArtwork', function (event) {
    $('#popupListDefinition').keypress(function (e) {
        if (e.which == 13) {
            return false;
        }
    });
    $('#popupListDefinition').on("popupafteropen", function (event, ui) {
        window.setTimeout(function () {
            $('#testButtonCancel')[0].focus();
        }, 1000); 
    });
    if (jobCustomerNumber == CASEYS || jobCustomerNumber == CW) {
        $('#uploadArtDemoHints p a').text('Turn Hints Off');
        $('#uploadSelectedArtDemoHints p a').text('Turn Hints Off');
        $('#uploadedArtDemoHint p a').text('Turn Hints Off');
    }
    if (!dontShowHintsAgain && (showUploadArtDemoHints()) && (sessionStorage.showDemoHints == undefined || sessionStorage.showDemoHints == null || sessionStorage.showDemoHints == "" || sessionStorage.showDemoHints == "on") && (sessionStorage.isArtUploadDemoHintsDisplayed == undefined || sessionStorage.isArtUploadDemoHintsDisplayed == null || sessionStorage.isArtUploadDemoHintsDisplayed == "false")) {
        sessionStorage.isArtUploadDemoHintsDisplayed = true;
        openDemoHints();
    }

    (($.browser.msie) ? $(window.frames[0].frames.frameElement).contents().find('input[type=file]') : $('input[type=file]')).bind('change', function (e) {
        pageObj.navigate(e, $(e.target))
    });
    pagePrefs = jQuery.parseJSON(sessionStorage.uploadArtworkPrefs);

    if (!fnVerifyScriptBlockDict(pagePrefs))
        return false;

    //if (pagePrefs != null) {
    //pageObj.displayListFileInfo();
    //eval(getURLDecode(pagePrefs.scriptBlockDict.pageshow));
    // }
    if (changeUploadArtHeaderTextNeeded()) {
        $('div[data-role="content"] h2').html("Art Upload");
        //if (jobCustomerNumber == DCA) {
        //    $('input[type=file][id="mailFiles[]"]').removeAttr('multiple');
        //}
    }
    if ('draggable' in document.createElement('span'))
        $('#spnBtnText').html('"Upload"');
    else
        $('#spnBtnText').html('"Upload Selected Artwork"');

    pageObj.displayArtFileInfo();
    $('#dvVersionCode').show();
    //if (pageObj.customerNumber == CW) {
    //    pageObj.loadSelectedStores('Art');
    //    pageObj.loadOrderedLocations();
    //    $('#dvVersionCode').show();
    //    $('#dvLocations').show();
    //}
    //else
    if (isLoadOrderedLocations(pageObj.customerNumber)) {
        pageObj.loadOrderedLocations();
        $('#dvVersionCode').hide();
        $('#dvLocations').show();
        if (pageObj.customerNumber == SCOTTS || pageObj.customerNumber == ACE || pageObj.customerNumber == ANDERSEN) {
            $('#lnkArtworkRequirements').css('display', 'block');
        }
    }
    else {
        $('#dvLocations').hide();
        $('#dvDoNotAssign').hide();
        if ((pageObj.customerNumber == BBB || jobCustomerNumber == DATAWORKS || jobCustomerNumber == MOTIV8) && appPrivileges.roleName == "user")
            $('#dvVersionCode').hide();
        else
            $('#dvVersionCode').show();
    }


    $.each($('input[type=checkbox]'), function (a, b) {
        $(this).checkboxradio();
    });
    $.each($('fieldset[data-role="controlgroup"]'), function (a, b) {
        $(this).controlgroup();
    });

    $('#loadingText1').text("Loading Artwork.....");
    $('#okBut').attr('onclick', 'pageObj.closeDialog();');
    window.setTimeout(function putDelay() {
        getNextPageInOrder();
    }, 2000);
    //    if (appPrivileges.roleName == "admin") $('#dvChkApproveJobArtwork').css('display', 'block');
    //    window.setTimeout(function makeDelay() {
    //        if (pageObj.gOutputData.milestoneAction.milestoneDict != undefined && JSON.stringify(pageObj.gOutputData.milestoneAction.milestoneDict).length > 2) {
    //            if (pageObj.gOutputData.milestoneAction.milestoneDict.artarrival != undefined && pageObj.gOutputData.milestoneAction.milestoneDict.artarrival != null && pageObj.gOutputData.milestoneAction.milestoneDict.artarrival.completedDate != "" && pageObj.gOutputData.milestoneAction.milestoneDict.artarrival.completedBy != "")
    //                $('#chkApproveJobArtwork').attr('checked', true).checkboxradio('refresh');
    //        }
    //    }, 1000);

    if ($('#uploadedArtCount').text() == "0")
        $('#dvChkApproveJobArtwork').css('display', 'none');

    if (sessionStorage.isOpenedFromEmail != undefined && sessionStorage.isOpenedFromEmail != null && sessionStorage.isOpenedFromEmail == "true") {
        $('a[data-icon=home]').hide();
        $('a[id^=btnSave]').hide();
        $('a[id=btnContinue]').hide();
        window.setTimeout(function removeButton() {
            $('#footer').find('a[id=btnRFC]').hide();
            $('#footer').removeClass('ui-bar');
            $('#footer').append('<p class="footerText">&nbsp;</p>');
        }, 500);
    }
    persistNavPanelState();

    $('#sldrShowHints').slider("refresh");
});

var openDemoHints = function () {
    window.setTimeout(function openHintPopup() {
        $('#uploadArtDemoHints').popup('open', { positionTo: '#navHeader' });
    }, 2000);
};
function openHint() {
    $('#popupDialog').popup('close');
    window.setTimeout(function openHintPopup() {
        $('#uploadedArtDemoHint').popup('open', { positionTo: '#uploadList' });
    }, 1000);
}

//Checks the given string ends with a specific (given character).
String.prototype.endsWith = function (suffix) {
    return this.indexOf(suffix, this.length - suffix.length) !== -1;
};
//******************** Page Load Events End **************************

