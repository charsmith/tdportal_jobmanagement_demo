﻿//******************** Public Functions Start **************************
var manageUploadArtwork = function () {
    var self = this;
    new uploadFilesCommon(self);
    new performEditActions(self);
    new dragAndDropFiles(self);

    if (jobCustomerNumber == GWA) {
        new confirmationEmails(self);
        self.loadApproversAndViewersKOUI($('#dvConfirmationEmails'));
        self.getApproversList();
    }
    self.maxNumberOfFiles = 10;  //This applies to artwork file upload count only.
    self.artFilesToUpload = [];
    self.replaceClickEvent = null;

    ////Loads the locations selected in an order.
    //self.loadOrderedLocations = function () {
    //    var selected_locations_info = [];
    //    //if (gOutputData != undefined && gOutputData.selectedLocationsAction != undefined)
    //    if (self.gOutputData != null && self.gOutputData.selectedLocationsAction != null && self.gOutputData.selectedLocationsAction != undefined && self.gOutputData.selectedLocationsAction.selectedLocationsList != undefined && self.gOutputData.selectedLocationsAction.selectedLocationsList != null)
    //        selected_locations_info = self.gOutputData.selectedLocationsAction.selectedLocationsList;
    //    var selected_stores = ''
    //    //selected_stores += '<label>Apply Art to Location(s):</label>';
    //    if (selected_locations_info.length > 0) {
    //        selected_stores += '<fieldset data-role="controlgroup" data-mini="true" data-inline="true" data-inset="true"><label for="chkAssignToAll" value="all">Assign to All</label><input type="checkbox" id="chkAssignToAll" value="all" data-mini="true" data-theme="c" onchange="pageObj.checkAllLocations();"/>';
    //        $.each(selected_locations_info, function (key, val) {
    //            var location_name = (val.location != undefined && val.location != null && val.location != "") ? val.location : ((val.storeName != undefined && val.storeName != null && val.storeName != '') ? val.storeName : "");
    //            selected_stores += '<label for="chk' + val.pk + '">' + val.storeId + ' - ' + location_name + '</label>';
    //            selected_stores += '<input type="checkbox" id="chk' + val.pk + '" name="chk' + val.pk + '" value="' + val.pk + '" data-mini="true" data-inset="true" data-theme="c" onchange="pageObj.locationCheckedChanged(event)"/>';
    //        });
    //        selected_stores += '</fieldset>';
    //        $('#dvSelectedLocations').empty();
    //        $('#dvSelectedLocations').html(selected_stores);
    //    }
    //    if (selected_locations_info.length > 4) {
    //        $('#dvSelectedLocations').css('height', '200px');
    //        $('#dvSelectedLocations').css('overflow-y', 'auto');
    //    }
    //    $('#dvSelectedLocations').addClass('ui-corner-all');
    //    $('#dvSelectedLocations').css('width', '300px');
    //    //$('#dvSelectedLocations').css('text-align', 'center');
    //    //$('<label id="lblLocations">Apply Art to Location(s):</label>').insertAfter($('#dvSelectedLocations').parent().find('h3'));
    //    //$('#dvSelectedLocations').css('border', '1px solid')
    //};

    ////Selects / Deselects all locations
    //self.checkAllLocations = function () {
    //    if ($('#chkAssignToAll').is(':checked'))
    //        $('#dvSelectedLocations fieldset input[type="checkbox"]:not([id^=chkAssignToAll])').attr('checked', true).checkboxradio('refresh');
    //    else
    //        $('#dvSelectedLocations fieldset input[type="checkbox"]:not([id^=chkAssignToAll])').attr('checked', false).checkboxradio('refresh');
    //};

    ////Selects / Deselects all locations
    //self.locationCheckedChanged = function (e) {
    //    var src = e.target;
    //    if ($(src).is(':checked')) {
    //        if ($('#dvSelectedLocations fieldset input[type="checkbox"]:not([id^=chkAssignToAll])').length == $('#dvSelectedLocations fieldset input[type="checkbox"]:checked:not([id^=chkAssignToAll])').length)
    //            $('#chkAssignToAll').attr('checked', true).checkboxradio('refresh');
    //    }
    //    else
    //        $('#chkAssignToAll').attr('checked', false).checkboxradio('refresh');

    //    if (self.artFilesToUpload.length > 0 && ('draggable' in document.createElement('span'))) {
    //        self.artFilesToUpload = [];
    //        self.uploadingFiles = "";
    //        data = [];
    //        temp_data = [];
    //        $('#drop-area').empty().html("Drop files here to select them <br />or");
    //    }
    //};


    //self.checkAssignedLocations = function (selected_store) {
    //    var assigned_artworks = [];
    //    var is_loc_assigned = false;
    //    if (self.gOutputData != undefined && self.gOutputData.artworkAction.artworkFileList != undefined && Object.keys(self.gOutputData.artworkAction.artworkFileList).length > 0) {
    //        assigned_artworks = self.gOutputData.artworkAction.artworkFileList;
    //        $.each(assigned_artworks, function (key, val) {
    //            //if ($('#hidMode').val() == "reassign" && $('#hidEditUploadFile').val() != val.fileName)
    //            if (val.locationId != null && val.locationId.indexOf(selected_store) > -1) {// && $('#hidMode').val() != "reassign"
    //                is_loc_assigned = true;
    //                return false;
    //            }
    //        });
    //    }
    //    return is_loc_assigned;
    //};

    //self.checkAssignedLocationToRessign = function (selected_store) {
    //    var assigned_artworks = [];
    //    var is_loc_assigned = false;
    //    if (self.gOutputData != undefined && self.gOutputData.artworkAction.artworkFileList != undefined && Object.keys(self.gOutputData.artworkAction.artworkFileList).length > 0) {
    //        assigned_artworks = self.gOutputData.artworkAction.artworkFileList;
    //        var reassigning_art = assigned_artworks[$('#hidEditUploadFile').val()];
    //        if (reassigning_art.locationId.indexOf(selected_store) == -1) {
    //            $.each(assigned_artworks, function (key, val) {
    //                if ($('#hidEditUploadFile').val() != val.fileName && val.locationId != null && val.locationId.indexOf(selected_store) > -1) {
    //                    is_loc_assigned = true;
    //                    return false;
    //                }
    //            });
    //        }
    //    }
    //    return is_loc_assigned;
    //};

    //Gets and Displays Uploaded artwork files information from Job Ticket JSON.
    self.displayArtFileInfo = function () {
        if (sessionStorage.jobSetupOutput == undefined && sessionStorage.jobSetupOutput == undefined) {
            self.makeGOutputData();
        }
        else {
            if (sessionStorage.jobSetupOutput != undefined) {
                self.gOutputData = jQuery.parseJSON(sessionStorage.jobSetupOutput);

                var file_data = [];

                file_data = (self.gOutputData != undefined && self.gOutputData.artworkAction.artworkFileList != undefined) ? self.gOutputData.artworkAction.artworkFileList : {};

                if (Object.keys(file_data).length == 0) {
                    //Upload Artwork is not working, so demo purpose hardcoded 02/22/2017 - Remove later
                    if (sessionStorage.customerNumber == ICYNENE) {
                        file_data = {
                            "Closed Cell Product Portfolio - 2017 - USA - Print.pdf": {
                                "locationId": "none",
                                "fileName": "Closed Cell Product Portfolio - 2017 - USA - Print.pdf",
                                "uuid": null,
                                "fileSizePretty": "2,983K",
                                "fileStatus": "OK",
                                "versionCode": "",
                                "linkedTo": "",
                                "filePk": 0,
                                "approvalType": null,
                                "overallStatus": 0,
                                "approvalStatus": null,
                                "versionCount": 0,
                                "versionDate": null
                            }
                        };
                    }
                    self.gOutputData["artworkAction"] = {
                        "artworkFileList": {},
                        "name": "artwork1",
                        "type": "artwork",
                        "isEnabled": true
                    };
                    self.gOutputData.artworkAction.artworkFileList = file_data;
                    if (sessionStorage.jobSetupOutputCompare != undefined) {
                        var goutput_compare_data = jQuery.parseJSON(sessionStorage.jobSetupOutputCompare);
                        if (goutput_compare_data.artworkAction != undefined && goutput_compare_data.artworkAction != null & goutput_compare_data.artworkAction != "null" && goutput_compare_data.artworkAction.artworkFileList != undefined && JSON.stringify(goutput_compare_data.artworkAction.artworkFileList).length == 2) {
                            goutput_compare_data.artworkAction["artworkFileList"] = file_data;
                            sessionStorage.jobSetupOutputCompare = JSON.stringify(goutput_compare_data);
                        }
                    }
                }
                self.buildOutputLists(true);
            }
        }
        if (sessionStorage.artFilesToUpload != undefined) {
            self.artFilesToUpload = jQuery.parseJSON(sessionStorage.artFilesToUpload);
            self.makeFileListBeforePost(self.artFilesToUpload);
        }
        if (self.gOutputData != null) {           
            getBubbleCounts(self.gOutputData);
        }
        //getMilestoneInfo(); //loads milestone info into goutputdata if it does not exists.
    }

    //Adds new file upload control to interface and opens the File upload popup dialog when user clicks on "Select.." button.
    self.addNewFile = function () {
        $('#divError').text('');
        self.openNewFileDialog();
    }

    //Opens File upload popup dialog
    self.openNewFileDialog = function () {
        if (self.gOutputData == undefined)
            self.gOutputData = jQuery.parseJSON(sessionStorage.jobSetupOutput);
        if ((showReplacementsUploadedValidation()) && Object.keys(self.gOutputData.artworkAction.artworkFileList).length == 2) {
            var msg = "Files has already been uploaded for both the placements. To delete or replace the uploaded file click the actions menu icon to the right of the uploaded file.";
            $('#alertmsg').text(msg);
            $('#popupDialog').popup('open');
            return false;
        }
        if ((self.artFilesToUpload.length < self.maxNumberOfFiles) && (Object.keys(self.gOutputData.artworkAction.artworkFileList).length < self.maxNumberOfFiles) && ((self.artFilesToUpload.length + ((Object.keys(self.gOutputData.artworkAction.artworkFileList).length >= 1 && (self.gOutputData.artworkAction.artworkFileList[0] != undefined && self.gOutputData.artworkAction.artworkFileList[0] != null && self.gOutputData.artworkAction.artworkFileList[0].fileName != "")) ? Object.keys(self.gOutputData.artworkAction.artworkFileList.length) : 0)) < self.maxNumberOfFiles)) {
            if (!('draggable' in document.createElement('span'))) {
                self.addFileInput();
            }
            self.resetFields();
            self.showHidePopupDataOnLocationChange('');
            $('#testButtonOk').show();
            $('#btnUpdate').hide();
            $('#hidUploadFile').val('');
            self.popUpListDefinitionDisplay();
        }
        else {
            var msg = "The maximum number of artwork files to be uploaded per order has been reached. Please remove a previously uploaded file in order to upload a new file to this order. If this order requires more than " + self.maxNumberOfFiles + " artwork files, please contact your Account Manager.";
            $('#alertmsg').text(msg);
            $('#popupDialog').popup('open');
            return false;
        }
    }

    //Gets the Job Ticket Json, verifies for the artworkAction attribute, if not, creates it.
    //Also creates updates the Job Ticket Json compare object.
    self.makeGOutputData = function () {
        var file_data = [];
        file_data = (self.gOutputData != undefined && self.gOutputData.artworkAction.artworkFileList != undefined) ? self.gOutputData.artworkAction.artworkFileList : {};
        if (Object.keys(file_data).length == 0) {
            //Upload Artwork is not working, so demo purpose hardcoded 02/22/2017 - Remove later
            if (sessionStorage.customerNumber == ICYNENE) {
                file_data = {
                    "Closed Cell Product Portfolio - 2017 - USA - Print.pdf": {
                        "locationId": "none",
                        "fileName": "Closed Cell Product Portfolio - 2017 - USA - Print.pdf",
                        "uuid": null,
                        "fileSizePretty": "2,983K",
                        "fileStatus": "OK",
                        "versionCode": "",
                        "linkedTo": "",
                        "filePk": 0,
                        "approvalType": null,
                        "overallStatus": 0,
                        "approvalStatus": null,
                        "versionCount": 0,
                        "versionDate": null
                    }
                };
            }
            self.gOutputData["artworkAction"] = {
                "artworkFileList": {},
                "name": "artwork1",
                "type": "artwork",
                "isEnabled": true
            };
            self.gOutputData.artworkAction.artworkFileList = file_data;
            if (sessionStorage.jobSetupOutputCompare != undefined) {
                var goutput_compare_data = jQuery.parseJSON(sessionStorage.jobSetupOutputCompare);
                if (goutput_compare_data.artworkAction.artworkFileList != undefined && JSON.stringify(goutput_compare_data.artworkAction.artworkFileList).length == 2) {
                    goutput_compare_data.artworkAction["artworkFileList"] = file_data;
                    sessionStorage.jobSetupOutputCompare = JSON.stringify(goutput_compare_data);
                }
            }
        }
        self.buildOutputLists(true);
    }

    //Generates the Approvers and Viewers list
    self.getApproversList = function (artwork_obj) {
        var approvers_list = "";
        var viewers_list = "";
        //var temp_approvers_list = $.extend(true, [], approvers_list);
        if (artwork_obj.approverList != undefined && artwork_obj.approverList != null && artwork_obj.approverList.length > 0) {
            $.each(artwork_obj.approverList, function (key, val) {
                if (val.approvalLevel == 1)
                    approvers_list += (approvers_list != "") ? ', ' + val.email + ' is ' + getApprovalStatus(parseInt(val.approvalStatus)) : 'Approvers: ' + val.email + ' is ' + getApprovalStatus(parseInt(val.approvalStatus));
                else if (val.approvalLevel == 0) {
                    if (val.email != undefined && val.email != null && val.email != "")
                        viewers_list += (viewers_list != "") ? ', ' + val.email : 'Viewers: ' + val.email;
                }
            });
        }
        return ((approvers_list != "") ? approvers_list + '<br />' : "") + ((viewers_list != "") ? viewers_list : "");
    }

    //Creates the list of uploaded artwork files and displays.
    self.makeFileList = function (array_name) {
        var array = eval(array_name);
        var list_info = "";
        var art_files = "";
        var art_files_back = "";
        var art_files_front = "";
        var select_files = "";
        $("#ulUploadList").empty();
        $("#btnMapSelected").hide();
        $("#btnRemoveSelected").hide();

        if ((isOrderLocationsNeeded(self.customerNumber)) && $('#ddlSelectLocations input[type=checkbox]').length <= 1)
            self.loadOrderedLocations();
        var idx = 0;
        $.each(array_name, function (key, val) {
            if (isArtworkReplacementsCustomer()) {
                if (idx == 1) {
                    val["placement"] = "front";
                }
                else if (idx == 0) {
                    val["placement"] = "back";
                }
            }           
            idx++;
            if (val.fileName != "") {
                var approval_icon = "";
                if (val.overallStatus == undefined || val.overallStatus == null || val.overallStatus == "") {
                    val.overallStatus = 4;
                }
                switch (val.overallStatus) {
                    case 6:
                        approval_icon = "icon_approved_with_changes.png";
                        break;
                    case 4:
                        approval_icon = "icon_pending.png";
                        break;
                    case 1:
                        approval_icon = "icon_approved.png";
                        break;
                    case 0:
                        approval_icon = "icon_rejected.png";
                        break;
                }
                var approval_status = getApprovalStatus(parseInt(val.overallStatus));
                if (appPrivileges.roleName == "admin")
                    art_files += '<li><a><h3 style="cursor:default">' + val.fileName + '</h3><p style="cursor:default">';
                    //if (self.customerNumber == DCA || self.customerNumber == KUBOTA || self.customerNumber == SPORTSKING || self.customerNumber == GWA || self.customerNumber == CASEYS || self.customerNumber == EQUINOX || self.customerNumber == ALLIED)
                    //    art_files += '<li><a><h3 style="cursor:default">' + val.fileName + '</h3><p style="cursor:default">';
                    //else
                    //    art_files += '<li><a><img src="../images/' + approval_icon + '" alt="' + val.approvalStatus + '" title="' + approval_status + '" class="ui-li-icon ui-corner-none"><h3 style="cursor:default">' + val.fileName + '</h3><p style="cursor:default">';
                    ////                else if ((jobCustomerNumber == REGIS ||jobCustomerNumber == SCOTTS || jobCustomerNumber == "99997") && (appPrivileges.roleName == "user" || appPrivileges.roleName == "power"))  //added this else if to display to normal users of Regis.
                    ////                    art_files += '<li><a><img src="../images/' + approval_icon + '" alt="' + val.approvalStatus + '" title="' + approval_status + '" class="ui-li-icon ui-corner-none"><h3 style="cursor:default">' + val.fileName + '</h3><p style="cursor:default">';
                else
                    art_files += '<li><a' + ((val.fileStatus.toLowerCase() == "missing") ? '' : ' onclick="pageObj.displayArtwork(event,\'' + val.fileName + '\')"') + ' href="#"><h3>' + val.fileName + '</h3><p>';

                art_files += ((val.locationId != "none" && (isOrderLocationsNeeded(self.customerNumber)))) ? '<span class="word_wrap" style="width:70%"> Location(s): ' + self.getLocationNamesWithComma(val.locationId) + "</span> <br /> " : "";
                art_files += 'Format: ' + val.fileName.substring(val.fileName.lastIndexOf('.') + 1);
                art_files += (val.versionCode != undefined && val.versionCode != "") ? ' | Version : ' + val.versionCode : "";
                art_files += ' | File Size: ' + val.fileSizePretty + ' | File Status : ' + val.fileStatus;
                art_files += (jobCustomerNumber == "203" || jobCustomerNumber == BRIGHTHOUSE) ? ' | Linked To: ' + ((val.linkedTo != undefined && val.linkedTo != null) ? val.linkedTo : "") : "";

                if (appPrivileges.roleName == "admin") {
                    //art_files += " | Versions: " + ((val.versionCount != undefined && val.versionCount != null && val.versionCount.toString() != "") ? val.versionCount : "");
                    //art_files += " | Uploaded: " + ((val.versionDate != undefined && val.versionDate != null && val.versionDate != "") ? val.versionDate : "");
                    art_files += "<br /> " + self.getApproversList(val);
                    art_files += (val.approvalsNeeded != undefined && val.approvalsNeeded != null && val.approvalsNeeded != "") ? "<br /> Approvals Needed:" + val.approvalsNeeded : "";
                }
                else if (showApprovalType()) { //added this else if to display to normal users of Regis.
                    art_files += '<br /> Approval Type: ' + ((val.approvalType != undefined && val.approvalType != null && val.approvalType != "") ? ((val.approvalType.indexOf('both') > -1) ? "Proof/Production" : ((typeof (val.approvalType) == "number") ? getApprovalType(parseInt(val.approvalType)) : val.approvalType)) : "");
                    //art_files += " | Versions: " + ((val.versionCount != undefined && val.versionCount != null && val.versionCount != "") ? val.versionCount : "");
                    //art_files += " | Uploaded: " + ((val.versionDate != undefined && val.versionDate != null && val.versionDate != "") ? val.versionDate : "");
                    art_files += "<br /> " + self.getApproversList(val);
                    art_files += (val.approvalsNeeded != undefined && val.approvalsNeeded != null && val.approvalsNeeded != "") ? "<br /> Approvals Needed:" + val.approvalsNeeded : "";
                }
                var temp_loc = "";
                temp_loc = (val.locationId != null && val.locationId.indexOf(',') > -1) ? val.locationId.replace(/,/g, '_') : val.locationId;
                if (appPrivileges.roleName == "admin") {
                    art_files += '</p>';
                    if (val.fileName.substring(val.fileName.lastIndexOf('.') + 1) != "html" && val.fileName.substring(val.fileName.lastIndexOf('.') + 1) != "htm") {
                        if (hideClientProofing())
                            art_files += (val.fileStatus.toLowerCase() == "missing") ? '</a>' : '<span data-theme="b" style="font-size:14px;font-weight:bold;text-decoration:underline;cursor:hand;color:#497BAE" onclick="pageObj.displayArtwork(event,\'' + val.fileName + '\')">Preview Artwork</span></a>';
                        else
                            art_files += (val.fileStatus.toLowerCase() == "missing") ? '</a>' : '<span data-theme="b" style="font-size:14px;font-weight:bold;text-decoration:underline;cursor:hand;color:#497BAE" onclick="pageObj.displayArtwork(event,\'' + val.fileName + '\')">Preview Artwork</span></a>';
                        //art_files += ((val.fileStatus.toLowerCase() == "missing") ? '' : '<span data-theme="b" style="font-size:14px;font-weight:bold;text-decoration:underline;cursor:hand;color:#497BAE" onclick="pageObj.displayArtwork(event,\'' + val.fileName + '\')">Preview Artwork</span> |') + '<span data-theme="b" style="font-size:14px;font-weight:bold;text-decoration:underline;cursor:hand;color:#497BAE" onclick="pageObj.displayArtwork(event,\'' + val.fileName + '\')">Client Proofing & Markup</span></a>';
                    }
                    art_files += '<a href="#" data-icon="bars" onclick="pageObj.displayPostUploadEditDialog(\'' + val.fileName + '\',\'' + temp_loc + '\',\'' + val.versionCode + '\',\'' + val.linkedTo + '\',\'postupdate\');">Edit Artwork</a></li>';
                }
                else if (showApprovalType()) { //added this else if to display proofing link to normal users of Regis.                
                    art_files += '</p>' + ((val.fileStatus.toLowerCase() == "missing") ? '' : '<span data-theme="b" style="font-size:14px;font-weight:bold;text-decoration:underline;cursor:hand;color:#497BAE" onclick="pageObj.displayArtwork(event,\'' + val.fileName + '\')">Preview Artwork</span></span></a>');
                    //art_files += '</p>' + ((val.fileStatus.toLowerCase() == "missing") ? '' : '<span data-theme="b" style="font-size:14px;font-weight:bold;text-decoration:underline;cursor:hand;color:#497BAE" onclick="pageObj.displayArtwork(event,\'' + val.fileName + '\')">Preview Artwork</span> |') + '<span data-theme="b" style="font-size:14px;font-weight:bold;text-decoration:underline;cursor:hand;color:#497BAE" onclick="pageObj.displayArtwork(event,\'' + val.fileName + '\')">Client Proofing & Markup</span></a>';
                    art_files += '<a href="#" data-icon="bars" onclick="pageObj.displayPostUploadEditDialog(\'' + val.fileName + '\',\'' + temp_loc + '\',\'' + val.versionCode + '\',\'' + val.linkedTo + '\',\'postupdate\');">Edit Artwork</a></li>';
                }
                else {
                    art_files += '</p></a>';
                    art_files += '<a href="#" data-icon="bars" onclick="pageObj.displayPostUploadEditDialog(\'' + val.fileName + '\',\'' + temp_loc + '\',\'' + val.versionCode + '\',\'' + val.linkedTo + '\',\'postupdate\');">Edit Artwork</a></li>';
                }
                $("#btnMapSelected").show();
                $("#btnRemoveSelected").show();
                if (isArtworkReplacementsCustomer()) {
                    var art_placement = (val.placement != undefined && val.placement != null && val.placement == "front") ? 'front' : ((val.placement != undefined && val.placement != null && val.placement == "back") ? 'back' : '');
                    if (art_placement != '' && art_placement == 'back') {
                        art_files_back += art_files;
                        art_files = "";
                    } else if (art_placement != '' && art_placement == 'front') {
                        art_files_front += art_files;
                        art_files = "";
                    }
                }                
            }
            else {
                $("#lnkUploadFile").show();
            }
        });
        is_file_map_displayed = '<th>File Type</th><th>File Size</th><th>File Status</th></theader>';
        if (art_files != "") {
            //if (appPrivileges.roleName == "admin")
            //    art_files = '<li data-role="list-divider" >Uploaded Artwork Files' + ((jobCustomerNumber == DCA || jobCustomerNumber == SCOTTS || jobCustomerNumber == KUBOTA || jobCustomerNumber == SPORTSKING || jobCustomerNumber == GWA || jobCustomerNumber == CASEYS || jobCustomerNumber == EQUINOX || jobCustomerNumber == ALLIED) ? ('</li>' + art_files) : '<a href="#popupUploadedInfo" data-rel="popup" data-position-to="window" data-transition="pop" class="ui-btn-icon-right ui-icon-info" style="color:white;float:right;margin-right: -2px;"></a></li>' + art_files);
            //else if ((jobCustomerNumber == REGIS || jobCustomerNumber == SCOTTS) && (appPrivileges.roleName == "user" || appPrivileges.roleName == "power"))  //added this else if to display to normal users of Regis.
            //    art_files = '<li data-role="list-divider" >Uploaded Artwork Files<a href="#popupUploadedInfo" data-rel="popup" data-position-to="window" data-transition="pop" class="ui-btn-icon-right ui-icon-info" style="color:white;float:right;margin-right: -2px;"></a></li>' + art_files;
            //else
            art_files = '<li data-role="list-divider" >Uploaded Artwork Files</li>' + art_files;
            $("#ulUploadList").append(art_files);
            $("#ulUploadList").listview('refresh');
            $("#dvUploadList").show();
            $('#dvUploadPlacements').css('display', 'none');
        }
        else {
            if (art_files_front != "") {
                var front_placement = "";
                mail_grid_info = '<div id="dvFrontPlacement" data-role="collapsible" data-theme="f" data-collapsed-icon="carat-r" data-expanded-icon="carat-d" data-collapsed="false"><h2>Front Placement - Must include a 4" wide by 2.125" tall white area in the lower right corner for the mailing address to appear in.</h2>';
                mail_grid_info += '<ul id="ulFrontPlacement" data-role="listview" data-theme="c" data-divider-theme="c" data-split-icon="delete" data-split-theme="e" >';
                front_placement += mail_grid_info + art_files_front;
                front_placement += '</ul></div>';
                $("#dvUploadFrontPlacements").empty();
                $("#dvUploadFrontPlacements").append(front_placement);
                if (front_placement != "") {
                    $('#dvUploadFrontPlacements').css('display', 'block');
                    $("div ul").each(function (i) {
                        $(this).listview();
                    });
                    $("#dvUploadFrontPlacements").collapsibleset('refresh');
                    window.setTimeout(function () {
                        $("#dvFrontPlacement").collapsible('expand');
                    }, 1000);
                    front_placement = "";
                }
            }
            if (art_files_back != "") {
                var back_placement = "";
                seed_grid_info = '<div id="dvBackPlacement" data-role="collapsible" data-theme="f" data-collapsed-icon="carat-r" data-expanded-icon="carat-d" data-collapsed="false"><h2>Back Placement</h2>';
                seed_grid_info += '<ul id="ulBackPlacement" data-role="listview" data-theme="c" data-divider-theme="c" data-split-icon="delete" data-split-theme="e" >';
                back_placement += seed_grid_info + art_files_back;
                back_placement += '</ul></div>';
                $("#dvUploadBackPlacements").empty();
                $("#dvUploadBackPlacements").append(back_placement);
                if (back_placement != "") {
                    $('#dvUploadBackPlacements').css('display', 'block');
                    $("div ul").each(function (i) {
                        $(this).listview();
                    });
                    $("#dvUploadBackPlacements").collapsibleset('refresh');
                    window.setTimeout(function () {
                        $("#dvBackPlacement").collapsible('expand');
                    }, 1000);
                    back_placement = "";
                }
            }

           
        }
        if (self.gOutputData != null) {
            getBubbleCounts(self.gOutputData);
        }
    }

    //Updates the files list with the uploaded artwork files.
    self.updateFileGrid = function () {
        $.each(self.artFilesToUpload, function (key, value) {
            self.addFileInfo(value);
        });
        //reset the fields...
        self.resetFields();
        var upload_form = ($.browser.msie) ? window.frames[0].document.getElementById('spnFileInput') : document.getElementById('spnFileInput');
        var file_ctrls = $(upload_form).find('input[type=file]');
        if (file_ctrls.length > 0) {
            var newFileInput = file_ctrls[0].cloneNode(true);
            newFileInput.value = null;
            newFileInput.style.display = "";
            newFileInput.id = "mailFiles[]";
            newFileInput.name = "mailFiles[]";
            $((($.browser.msie) ? window.frames[0].document.getElementById('spnFileInput') : document.getElementById('spnFileInput'))).find('input[type=file]').remove();
            $((($.browser.msie) ? window.frames[0].document.getElementById('spnFileInput') : document.getElementById('spnFileInput'))).append(newFileInput);
            (($.browser.msie) ? $(window.frames[0].frames.frameElement).contents().find('input[type=file]') : $('input[type=file]')).bind('change', function (e) {
                self.navigate(e, $(e.target))
            });
            self.fileCount = 0;
        }
        self.artFilesToUpload = [];
        $("#divFilesToUpload").hide();
        sessionStorage.removeItem("artFilesToUpload");
    }

    //Gets the selected file information from the session object when user clicks on edit button.
    self.getFileSettings = function (file_name, file_type, location_id) {
        if ($('#dvUploadedListFiles select option').length == 0)
            $('#dvUploadedListFiles').empty();
        $('#btnUpdate').attr('onclick', "pageObj.updateEditFileInfo(this,'','','')");
        $('#divError').text('');
        $('#testButtonOk').hide();
        $((($.browser.msie) ? window.frames[0].document.getElementById('spnFileInput') : document.getElementById('spnFileInput')))[0].style.display = "none";
        $('#btnUpdate').show();
        $.each(self.artFilesToUpload, function (key, val) {
            if (val.fileName.substring(val.fileName.lastIndexOf('.') + 1).toLowerCase() == file_type.toLowerCase() && val.fileName.substring(val.fileName.lastIndexOf('\\') + 1).toLowerCase() == file_name.toLowerCase()) {
                if (jobCustomerNumber == CW || jobCustomerNumber == BRIGHTHOUSE || jobCustomerNumber == BBB || jobCustomerNumber == DATAWORKS) {
                    if (val.locationId.toLowerCase() == location_id.toLowerCase()) {
                        if (!isMultiple && !isDragDrop) {
                            $('#name').val(val.fileName.substring(val.fileName.lastIndexOf('\\') + 1));
                        }
                        else if (isMultiple || isDragDrop) {
                            var file_found = $.grep(data, function (obj) {
                                return (obj.name.toLowerCase() === val.fileName.substring(val.fileName.lastIndexOf("\\") + 1).toLowerCase())
                            });
                            temp_data = [];
                            if (file_found.length > 0) {
                                temp_data.push(file_found[0]);
                                self.displayFileList();
                                //return false;
                            }
                        }
                        $('#hidEditUploadFile').val(val.fileName);
                        var selected_locations = val.locationId.split(',');
                        $.each(selected_locations, function (a, b) {
                            if ($('input[type=checkbox][id=chk' + b + ']').length > 0)
                                $('input[type=checkbox][id=chk' + b + ']').attr('checked', true).checkboxradio('refresh');
                        });

                        $('#hidEditUploadLocation').val(location_id);
                        $('#txtVersionCode').val((val.versionCode != undefined && val.versionCode != null) ? val.versionCode : "");
                        $('#ddlFilesUploaded').val((val.linkedTo != undefined && (val.linkedTo != "" && val.linkedTo != null) ? val.linkedTo : 'select')).selectmenu('refresh');
                        self.selectedKey = key;
                    }
                }
                else {
                    $('#hidEditUploadFile').val(val.fileName);
                    if (!isMultiple && !isDragDrop) {
                        $('#name').val(val.fileName.substring(val.fileName.lastIndexOf('\\') + 1));
                    }
                    else if (isMultiple || isDragDrop) {
                        var file_found = $.grep(data, function (obj) {
                            return (obj.name.toLowerCase() === val.fileName.substring(val.fileName.lastIndexOf("\\") + 1).toLowerCase())
                        });
                        temp_data = [];
                        if (file_found.length > 0) {
                            temp_data.push(file_found[0]);
                            self.displayFileList();
                            return false;
                        }
                    }
                    $('#txtVersionCode').val((val.versionCode != undefined && val.versionCode != null) ? val.versionCode : "");
                    $('#txtFileName').val(((val.renamedFileName != undefined && val.renamedFileName != null) ? val.renamedFileName : ""));
                    $('#ddlApprovalType').val(((val.approvalType != undefined && val.approvalType != null && val.approvalType != "") ? val.approvalType : "select")).selectmenu('refresh');
                    $('#hidEditUploadFile').val(val.fileName);
                    self.selectedKey = key;
                }
            }
        });
        $('#hidMode').val('edit');
        self.popUpListDefinitionDisplay();
    }

    //Opens the file open dialog and allows the user to browse the system folders and select the required file.
    //Validates the type of file(s) that is/are being selected to upload.
    //Validates the name of the file(s) that is/are being selected to upload.
    self.navigate = function (event, ctrl) {
        if ($(ctrl)[0].files != undefined && $(ctrl)[0].files != null && $(ctrl)[0].files.length > 0) {
            isMultiple = true;
            self.resetFileUploadControl(self.artFilesToUpload);
            self.fileCount = (self.fileCount <= 0) ? self.fileCount = 0 : self.fileCount - 1;
            var files_count = $(ctrl)[0].files.length;
            self.makeDrop(event, true, ctrl);
        }
        else if ($(ctrl)[0].files != undefined && $(ctrl)[0].files != null && $(ctrl)[0].files.length == 0) {
            return false;
        }
        else if ((jobCustomerNumber == CW || jobCustomerNumber == DCA || jobCustomerNumber == KUBOTA || jobCustomerNumber == SPORTSKING || jobCustomerNumber == GWA || jobCustomerNumber == CASEYS || jobCustomerNumber == EQUINOX || jobCustomerNumber == ALLIED || showSelectLocationValidation()) && ($('#dvSelectedLocations input[type=checkbox]:not([value=all]):checked').length == 0 && $('#hidMode').val() != "replace") && !$('#dvDoNotAssign input[type=checkbox]').is(":checked")) {
            $("#popupListDefinition").popup("close");
            $('#validationAlertmsg').html("Please select at least one location.");
            window.setTimeout(function getDelay() {
                self.resetFileInput();
                $('#validationPopupDialog').popup('open');
                return false;
            }, 200);
            return false;
        }
        else if ((isArtworkReplacementsCustomer()) && $('input[type=radio][name=rdoArtPlacement]:checked').length == 0) {
            $("#popupListDefinition").popup("close");
            $('#validationAlertmsg').html("Please select artwork placement.");
            window.setTimeout(function getDelay() {
                self.resetFileInput();
                $('#validationPopupDialog').popup('open');
                return false;
            }, 200);
            return false;
        }
        else {
            var file_name = $(ctrl).val();
            var check_file_name = ($(ctrl).val().indexOf('\\') > -1) ? $(ctrl).val().substring($(ctrl).val().lastIndexOf('\\') + 1, $(ctrl).val().length) : $(ctrl).val();
            var myRegexp = /^([a-zA-Z0-9_\.\-\^\ ])+$/ig;
            if (!myRegexp.test(check_file_name.substring(0, check_file_name.lastIndexOf('.')))) {
                self.resetFileUploadControl(self.artFilesToUpload);
                self.fileCount = (self.fileCount <= 0) ? self.fileCount = 0 : self.fileCount - 1;
                $('#popupListDefinition').popup('close');
                $('#alertmsg').text('Illegal character in file name. Uploading cannot continue.');
                $('#alertmsg').addClass('wordwrap');
                $('#popupDialog').popup('open');
                return false;
            }
            if (Object.keys(self.gOutputData.artworkAction.artworkFileList).length > 0 && !$('#dvDoNotAssign input[type=checkbox]').is(":checked") && $('#hidMode').val() != "replace") {
                if (isArtworkReplacementsCustomer()) {
                    var is_placement_artwork_added = false;
                    var selected_placement = $('input[type=radio][name=rdoArtPlacement]:checked').val();
                    $.each(self.gOutputData.artworkAction.artworkFileList, function (key, val) {
                        if (val.placement == selected_placement) {
                            is_placement_artwork_added = true;
                            return false;
                        }
                    });
                    if (is_placement_artwork_added) {                        
                        var msg = "A file has already been uploaded for the <b>" + selected_placement + "</b> placement. To delete or replace the uploaded file click the actions menu icon to the right of the uploaded file."
                        $('#popupListDefinition').popup('close');
                        $('#alertmsg').html(msg);
                        $('#alertmsg').addClass('wordwrap');
                        $('#popupDialog').popup('open');
                        return false;
                    }
                }
                else {
                    var selected_stores = $('#dvSelectedLocations input[type=checkbox]:not([value="all"]):checked');
                    var selected_stores_info = [];
                    if (selected_stores.length > 0) {
                        var is_assigned = false;
                        $.each(selected_stores, function (key, val) {
                            is_assigned = self.checkAssignedLocations($(val).val(), "Art");
                            if (is_assigned) {
                                selected_stores_info.push($('label[for="chk' + $(val).val() + '"]').text());
                            }
                        });
                    }
                    if (selected_stores_info.length > 0) {
                        var msg = "Artwork has already been assigned to these locations: <br /><b> " + selected_stores_info.join('<br />') + "</b>.  <br />Please select another locations.";
                        $('#popupListDefinition').popup('close');
                        $('#alertmsg').html(msg);
                        $('#alertmsg').addClass('wordwrap');
                        $('#popupDialog').popup('open');
                        return false;
                    }
                }                
            }
            var extension = file_name.substring(file_name.lastIndexOf('.') + 1);
            var allowedExtension = ["jpg", "jpeg", "png", "pdf", "gif", "tif", "tiff", "svg", "html", "htm", "zip"];
            var isValidFileFormat = false;
            if (typeof (extension) != 'undefined' && extension.length > 1) {
                var checked_ctrls = $('fieldset[data-role=controlgroup]').find('input[type=checkbox][checked=checked]');
                var selected_location = '';
                if (checked_ctrls.length > 0)
                    $.each(checked_ctrls, function (a, b) {
                        selected_location += (selected_location != '') ? ',' + b.value : b.value;
                    });
                for (var i = 0; i < allowedExtension.length; i++) {
                    if (extension.toLowerCase() == allowedExtension[i]) {
                        isValidFileFormat = true;
                        var file_type = (file_name != "") ? file_name.split('.')[1] : "";
                        $('#hidUploadFile').val(file_name + '|' + file_type + '|' + selected_location);
                        break;
                    }
                }
                if (!isValidFileFormat) {
                    self.resetFileUploadControl(self.artFilesToUpload);
                    self.fileCount = (self.fileCount <= 0) ? self.fileCount = 0 : self.fileCount - 1;
                    $('#popupListDefinition').popup('close');
                    var alert_msg = (jobCustomerNumber == DCA || jobCustomerNumber == SPORTSKING) ? 'Invalid file type selection. Please upload a PDF file.' : 'Invalid File Type...Only "JPG","JPEG","PNG","PDF","GIF","TIF","TIFF","ZIP","SVG","HTML" and "HTM" types are accepted.';
                    $('#alertmsg').text(alert_msg);
                    $('#alertmsg').addClass('wordwrap');
                    $('#popupDialog').popup('open');
                    return true;
                }
            }
            $('#name').val(file_name.substring(file_name.lastIndexOf("\\") + 1));
            $('#txtFileName').val(file_name.substring(file_name.lastIndexOf("\\") + 1));
        }
    }

    //Verifies the existence of the file in the list of files selected to upload.
    self.verifyFileExistence = function (file_name) {
        var art_files_to_upload = self.artFilesToUpload;
        var art_files_uploaded = (self.gOutputData.artworkAction.artworkFileList != undefined) ? self.gOutputData.artworkAction.artworkFileList : {};
        var to_upload = [];
        if ((!isMultiple && !isDragDrop)) {
            to_upload = $.grep(self.artFilesToUpload, function (obj) {
                return obj.fileName.substring(obj.fileName.lastIndexOf('\\') + 1) === file_name;
            });
        }
        var file_name_verify = file_name.replace('jpg', 'pdf').replace('gif', 'pdf').replace('tiff', 'pdf').replace('tiff', 'pdf').replace('png', 'pdf');
        var upload_files = (Object.keys(art_files_uploaded).length > 0) ? ((art_files_uploaded[file_name_verify] != undefined && art_files_uploaded[file_name_verify] != null) ? art_files_uploaded[file_name_verify] : {}) : {};
        if (to_upload.length > 0 || Object.keys(upload_files).length > 0) {
            return false;
        }
        return true;
    }

    self.checkFileExistencyLocationWise = function (file_name) {
        var art_files_to_upload = self.artFilesToUpload;
        var art_files_uploaded = (self.gOutputData.artworkAction.artworkFileList != undefined) ? self.gOutputData.artworkAction.artworkFileList : {};
        var to_upload = [];
        if ((!isMultiple && !isDragDrop)) {
            to_upload = $.grep(self.artFilesToUpload, function (obj) {
                return obj.fileName.substring(obj.fileName.lastIndexOf('\\') + 1) === file_name;
            });
        }
        var file_name_verify = file_name.replace('jpg', 'pdf').replace('gif', 'pdf').replace('tiff', 'pdf').replace('tiff', 'pdf').replace('png', 'pdf');
        var upload_files = (Object.keys(art_files_uploaded).length > 0) ? ((art_files_uploaded[file_name_verify] != undefined && art_files_uploaded[file_name_verify] != null) ? art_files_uploaded[file_name_verify] : {}) : {};
        /*if (to_upload.length > 0 || Object.keys(upload_files).length > 0 && jobCustomerNumber == DCA) {
            if (upload_files.fileName == file_name && upload_files.locationId == $('#ddlSelectLocations').val())
                return "srd"; // simple replace/delete.
            else if (upload_files.fileName == file_name && upload_files.locationId != "all" && (upload_files.locationId != $('#ddlSelectLocations').val() && (upload_files.locationId.indexOf($('#ddlSelectLocations').val()) == -1 || upload_files.locationId.indexOf($('#ddlSelectLocations').val()) > -1)))
                return "crd"; //Confirm Msg (Charlie and proceed to change the association save the job.
            //else if (upload_files.fileName == file_name && upload_files.locationId == "all" && (upload_files.locationId != $('#ddlSelectLocations').val() && (upload_files.locationId.indexOf($('#ddlSelectLocations').val()) == -1 || upload_files.locationId.indexOf($('#ddlSelectLocations').val()) > -1)))
            //    return "crd"; //Confirm Msg (Charlie and proceed to change the association save the job.
            //else if (upload_files.fileName != file_name && upload_files.locationId == "all" && (upload_files.locationId != $('#ddlSelectLocations').val() && (upload_files.locationId.indexOf($('#ddlSelectLocations').val()) == -1 || upload_files.locationId.indexOf($('#ddlSelectLocations').val()) > -1)))
            //    return "crd"; //Confirm Msg (Charlie and proceed to change the association save the job.
        }*/
        return "ok";
    };

    //Resets the file upload settings popup window.
    self.resetFields = function () {       
        if ($('#dvUploadedListFiles select option').length == 0)
            $('#dvUploadedListFiles').empty();
        $('#btnUpdate').attr('onclick', "pageObj.updateEditFileInfo(this,'','','')");
        if ($('#dvUploadedListFiles select option').length == 0)
            $('#dvUploadedListFiles').empty();
        $('#dvSelectedLocations').trigger('create');
        var checked_ctrls = $('#dvSelectedLocations input[type=checkbox]');
        if (checked_ctrls.length > 0)
            checked_ctrls.attr('checked', false).checkboxradio('refresh');
        if (checked_ctrls.length == 2) {//Assign To All, Single Location
            checked_ctrls.attr('checked', true).checkboxradio('refresh');
        }
        $('#name').val("");
        $('#dvFileActions').hide();
        $('#ddlFileActions').val('select').selectmenu('refresh');
        $('#txtVersionCode').val('');
        $('#popupListDefinition h3').text('Select Art Files');
        $('#chkUnAssign').attr('checked', false).checkboxradio('refresh').trigger('change');
        if (hideVersionCode()) {
            $('#dvVersionCode').hide();
            $('#dvSelectedLocations').parent().css('padding', '1px 1px 1px 1px');
            //$('#chkAssignToAll').attr('checked', true).checkboxradio('refresh').trigger('change');
            $('#dvLocations').show();
            $('#dvDoNotAssign').show();
            //$('#ddlSelectLocations').val('select').selectmenu('refresh');
        }
        else {
            $('#dvVersionCode').show();
            $('#dvLocations').hide();
        }
        if (jobCustomerNumber == OH) {
            $('#dvVersionCode').hide();
            $('#dvLocations').hide();
            $('#dvDoNotAssign').hide();
        }
        $('#ddlFilesUploaded').val('select').selectmenu('refresh');
        $('#dvText').show();
        $('#dvBrowse').show();
        $('#txtFileName').val("").show();
        $('#txtFileName').show();
        $('#dvFileName').hide();
        $('#drop-area').html('Drop files here to select them <br />or');
        if ('draggable' in document.createElement('span')) {
            $("#drop-area").html('Drop files here to select them <br />or');
            $('#dvDragDrop').css('display', 'block');
            $('#dvText').css('display', 'none');
            $('#testButtonOk').text('Upload');
        }
        else {
            $('#dvDragDrop').css('display', 'none');
            $('#dvText').css('display', 'block');
            $('#testButtonOk').text('Select');
        }
        if (showApprovalType()) {
            $('#dvApprovalType').hide();
            $('#ddlApprovalType').val('select').selectmenu('refresh');
        }
        else if (appPrivileges.roleName == "user" || appPrivileges.roleName == "power") {
            $('#dvApprovalType').hide();
        }
        else {
            $('#dvApprovalType').hide();
            $('#ddlApprovalType').val('select').selectmenu('refresh');
        }
        if (self.gOutputData.templateName == getCustomArtworkTemplate() || self.gOutputData.jobTypeId == 97 || (self.gOutputData.jobTypeId == ACE_CustomUpload_JobType)) {
            $('#dvArtPlacement').css('display', 'block');
            $('#lnkArtworkRequirements').css('display', 'block');            
            $('#dvLocations').hide();
            $('#dvDoNotAssign').hide();
        }
    }

    //Shows/hides the file upload settings popup window.
    self.showHidePopupContent = function (ddlType) {
        var entityType = $(ddlType).val();
        if (entityType == 'upload') {
            $('#popupListDefinition').popup('close');
        }
        else if (entityType == '') {
            self.resetFields();
        }
        self.popUpListDefinitionDisplay();

    }

    //Adds the uploaded file info to the job ticket json and saves to session.
    self.addFileInfo = function (uploading_file_info) {
        var file_type = uploading_file_info.fileName.substring(uploading_file_info.fileName.lastIndexOf('.') + 1);
        var locationId = uploading_file_info.locationId;
        var version_code = uploading_file_info.versionCode;
        var linked_to = "";
        if (uploading_file_info.linkedTo == "") {
            linked_to = (jobCustomerNumber == CW || jobCustomerNumber == BRIGHTHOUSE || jobCustomerNumber == OH) ? (($('#ddlFilesUploaded').val() != "select") ? $('#ddlFilesUploaded').val() : "") : "";
        }
        else linked_to = uploading_file_info.linkedTo;

        var file_name = uploading_file_info.fileName;
        file_name = (file_name.indexOf("\\") > -1) ? file_name.substring(file_name.lastIndexOf("\\") + 1) : file_name;  //file_name.substring(file_name.lastIndexOf("\\") + 1);
        //file_name = (file_name.indexOf('.jpg') || file_name.indexOf('.png')) ? file_name.replace('.png', '.pdf').replace('.jpg', '.pdf') : file_name;
        var is_file_exists = false;
        linked_to = (linked_to != "" && linked_to != "select") ? linked_to : "";
        var file_info = {};
        file_info = (self.gOutputData.artworkAction.artworkFileList != undefined) ? self.gOutputData.artworkAction.artworkFileList : {};
        var file_list = {};
        if (file_info[file_name] != undefined && file_info[file_name] != null && Object.keys(file_info).length > 0) {
            file_list = (file_info[file_name].fileName === file_name && file_info[file_name].fileName.substring(file_info[file_name].fileName.lastIndexOf('.') + 1) === file_type) ? file_info[file_name] : {};
        }
        if (Object.keys(file_list).length > 0) {
            if (file_list.fileName == file_name && file_type == file_list.fileName.substring(file_list.fileName.lastIndexOf('.') + 1)) {
                file_list.fileName = file_name;
                is_file_exists = true;
            }
        }
        //uploading_file_info.renamedFileName = (uploading_file_info.renamedFileName.indexOf('.jpg') || uploading_file_info.renamedFileName.indexOf('.png')) ? uploading_file_info.renamedFileName.replace('.png', '.pdf').replace('.jpg', '.pdf') : uploading_file_info.renamedFileName;
        if (Object.keys(file_list).length == 0 && (Object.keys(file_info).length == 0)) {
            file_list["fileName"] = file_name;
            file_list["locationId"] = locationId;
            if (self.gOutputData.templateName == getCustomArtworkTemplate() || self.gOutputData.jobTypeId == 97 || (self.gOutputData.jobTypeId == ACE_CustomUpload_JobType))
                file_list['placement'] = uploading_file_info.placement;
            file_list["versionCode"] = version_code;
            file_list["linkedTo"] = linked_to;
            file_list["renamedFileName"] = uploading_file_info.renamedFileName;
            file_list["approvalType"] = uploading_file_info.approvalType;
            file_list["approvalStatus"] = "pending";

            file_info[file_name] = file_list;
        }
        else if (!is_file_exists) {
            file_info[file_name] = {
                "fileName": file_name,
                "fileSizePretty": "",
                "fileStatus": "",
                "uploadFileType": file_type,
                "locationId": locationId,
                "versionCode": version_code,
                "linkedTo": linked_to,
                "renamedFileName": uploading_file_info.renamedFileName,
                "approvalType": uploading_file_info.approvalType,
                "approvalStatus": "pending"
            };

            if (self.gOutputData.templateName == getCustomArtworkTemplate() || self.gOutputData.jobTypeId == 97 || (self.gOutputData.jobTypeId == ACE_CustomUpload_JobType))
                file_info[file_name]["placement"] = uploading_file_info.placement
        }
        self.selectedKey = '';
        self.setLinkToUploadedFiles("art", file_name, linked_to)
        sessionStorage.jobSetupOutput = JSON.stringify(self.gOutputData);
    }

    //Updates the list of files to upload with the file selected to upload or edited.
    self.updateFileInfo = function (file_name, file_type) {
        var checked_ctrls = $('fieldset[data-role=controlgroup]').find('input[type=checkbox]:not([value=all]):checked');
        var linked_to = $('#ddlFilesUploaded').val();
        linked_to = (linked_to != undefined && linked_to != null && linked_to != "" && linked_to != "select") ? linked_to : "";
        var selected_location = '';
        if (checked_ctrls.length > 0) { 
            $.each(checked_ctrls, function (a, b) {
                selected_location += (selected_location != '') ? ',' + b.value : b.value;
            });
        }
        else if (isCheckSelectedLocations()) {
            checked_ctrls = $('#dvSelectedLocations').find('input[type=checkbox]:not([value=all])'); //All locations should be assigned to Artwork as per charlie note . 03/22/2017
            $.each(checked_ctrls, function (a, b) {
                selected_location += (selected_location != '') ? ',' + b.value : b.value;
            });
        }
        //Here locationId holds locationPk/pk
        var file_info = jQuery.grep(self.artFilesToUpload, function (obj) {
            return (isLocationsExist()) ? obj.fileName === file_name && obj.fileName.substring(obj.fileName.lastIndexOf('.') + 1) === file_type && obj.locationId.toLowerCase() == selected_location.toLowerCase() : obj.fileName === file_name && obj.fileName.substring(obj.fileName.lastIndexOf('.') + 1) === file_type;
        });
        if (file_info.length > 0) {
            file_info[0].fileName = file_name;
            file_info[0].locationId = selected_location;
            if (self.gOutputData.templateName == getCustomArtworkTemplate() || self.gOutputData.jobTypeId == 97 || (self.gOutputData.jobTypeId == ACE_CustomUpload_JobType))
                file_info[0].placement = $('input[type=radio][name=rdoArtPlacement]:checked').val();
            file_info[0].versionCode = $('#txtVersionCode').val();
            file_info[0].linkedTo = linked_to;
            file_info[0].renamedFileName = $('#txtFileName').val();
            file_info[0].approvalType = ($('#ddlApprovalType').val() != 'select') ? $('#ddlApprovalType').val() : "";
            var tdFileInputsTemp = ($.browser.msie) ? window.frames[0].document.getElementById('spnFileInput') : document.getElementById('spnFileInput'); // document.getElementById('spnFileInput');
            var number_of_files_selected = $(tdFileInputsTemp).find('input[type=file]').length;
            var fileInput = (number_of_files_selected > 1) ? $(tdFileInputsTemp).find('input[type=file]')[(number_of_files_selected - 1) - 1] : $(tdFileInputsTemp).find('input[type=file]')[(number_of_files_selected - 1)];
            $(fileInput).remove();
            $(tdFileInputsTemp).find('input[type=file]')[$(tdFileInputsTemp).find('input[type=file]').length - 1].style.display = "";
            self.uploadingFiles = (self.uploadingFiles.split('|').length > 0) ? self.uploadingFiles.substring(0, self.uploadingFiles.lastIndexOf('|')) : "";
            self.artApprovalTypes = (self.artApprovalTypes != undefined && self.artApprovalTypes != null && self.artApprovalTypes.split('^').length > 0) ? self.artApprovalTypes.substring(0, self.artApprovalTypes.lastIndexOf('^')) : "";
            $('#popupListDefinition').popup('close');
            $('#validationAlertmsg').html('The selected file already exists in the list. Please select another file.');
            $("#validationPopupDialog").popup('open');
        }
        else {
            if (file_type != undefined && file_name != undefined) {
                var file_list_info = {};
                file_list_info["fileName"] = file_name;
                file_list_info["locationId"] = selected_location;
                if (self.gOutputData.templateName == getCustomArtworkTemplate() || self.gOutputData.jobTypeId == 97 || (self.gOutputData.jobTypeId == ACE_CustomUpload_JobType))
                    file_list_info['placement'] = $('input[type=radio][name=rdoArtPlacement]:checked').val();
                file_list_info["versionCode"] = $('#txtVersionCode').val();
                file_list_info["linkedTo"] = linked_to;
                file_list_info["renamedFileName"] = $('#txtFileName').val();
                file_list_info["approvalType"] = ($('#ddlApprovalType').val() != 'select') ? $('#ddlApprovalType').val() : "";
                self.artFilesToUpload.push(file_list_info);
            }
            else {
                if (!isMultiple && !isDragDrop)
                    self.resetFileUploadPopup();
                $('#popupListDefinition').popup('close');
                $('#hidUploadFile').val('');
            }
        }
        sessionStorage.artFilesToUpload = JSON.stringify(self.artFilesToUpload);
    };

    //Resets the upload file setting popup window after the actin is completed.
    self.resetFileUploadPopup = function () {
        var checked_ctrls = $('fieldset[data-role=controlgroup]').find('input[type=checkbox][checked=checked]');
        if (checked_ctrls.length > 0)
            checked_ctrls.attr('checked', false).checkboxradio('refresh');
        $('#name').val("");
        $('#txtFileName').val("");
        $('#ddlApprovalType').val('select').selectmenu('refresh');
    }

    //Controls the File Upload settings popup up.
    self.popUpListDefinitionHide = function () {
        var file_info = $('#hidUploadFile').val();
        var file_name = file_info.split('|')[0];
        var file_type = file_info.split('|')[1];
        var location_id = file_info.split('|')[2];
        var count = 0;
        var index = 0;
        if ($('#hidMode').val().toLowerCase() != "edit") {
            if (!isMultiple && !isDragDrop) {
                //remove the file from the local session.
                var files_to_remove = jQuery.grep(self.artFilesToUpload, function (v) {
                    count++;
                    if (jobCustomerNumber == CW || jobCustomerNumber == BRIGHTHOUSE || jobCustomerNumber == BBB || jobCustomerNumber == DATAWORKS) {
                        if ((v.fileName.substring(v.fileName.lastIndexOf('.') + 1) == file_type) && (v.locationId.toLowerCase() == location_id.toLowerCase()) && (v.fileName.substring(v.fileName.lastIndexOf("\\") + 1) == file_name))
                            index = count - 1;
                        return (v.fileName.substring(v.fileName.lastIndexOf('.') + 1) == file_type) ? ((v.locationId.toLowerCase() == location_id.toLowerCase()) && (v.fileName.substring(v.fileName.lastIndexOf("\\") + 1) == file_name) ? false : true) : true;
                    }
                    else {
                        if ((v.fileName.substring(v.fileName.lastIndexOf('.') + 1) == file_type) && (v.fileName.substring(v.fileName.lastIndexOf("\\") + 1) == file_name))
                            index = count - 1;
                        return (v.fileName.substring(v.fileName.lastIndexOf('.') + 1) == file_type) ? ((v.fileName.substring(v.fileName.lastIndexOf("\\") + 1) == file_name) ? false : true) : true;
                    }
                });
                if ($('#hidMode').val().toLowerCase() != "") {
                    self.artFilesToUpload = files_to_remove;
                    if (self.artFilesToUpload.length == 0) {
                        $("#divFilesToUpload").hide();
                        var tdFileInputsTemp = ($.browser.msie) ? window.frames[0].document.getElementById('formUpload') : document.getElementById('formUpload');
                        var number_of_files_selected = $(tdFileInputsTemp).find('input[type=file]').length;
                        var fileInput = $(tdFileInputsTemp).find('input[type=file]')[number_of_files_selected - 1];
                        // Create a new file input
                        var newFileInput = fileInput.cloneNode(true);
                        newFileInput.value = null;
                        var id = "mailFiles[]";
                        newFileInput.id = id // A unique id
                        newFileInput.name = newFileInput.id;
                        newFileInput.style.display = "";
                        $($(tdFileInputsTemp).find('input[type=file]')[number_of_files_selected - 1]).remove();
                        var spnFileInputs = ($.browser.msie) ? window.frames[0].document.getElementById('spnFileInput') : document.getElementById('spnFileInput');
                        spnFileInputs.appendChild(newFileInput);
                        (($.browser.msie) ? $(window.frames[0].frames.frameElement).contents().find('input[type=file]') : $('input[type=file]')).bind('change', function (e) {
                            self.navigate(e, $(e.target))
                        });
                        self.fileCount = 0;
                    }
                    else if (!isMultiple && !isDragDrop) {
                        //remove the file control from the form.
                        self.resetFileUploadControl(self.artFilesToUpload);
                        self.fileCount = (self.fileCount <= 0) ? 0 : self.fileCount - 1;
                        self.makeFileListBeforePost(self.artFilesToUpload)
                    }
                }
                else if (!isMultiple && !isDragDrop) {
                    self.resetFileUploadControl(self.artFilesToUpload);
                    self.fileCount = (self.fileCount <= 0) ? 0 : self.fileCount - 1;
                    self.makeFileListBeforePost(self.artFilesToUpload)
                }
            }
            else {
                //if (temp_edit_file.length == 0) {
                //    $.each(temp_data, function (key, val) {

                //        var file_index = "";
                //        for (var i = 0; i < data.length; i++) {
                //            if (data[i].name.toLowerCase() == val.name.toLowerCase() && data[i].type.toLowerCase() == val.type.toLowerCase()) {
                //                file_index = i;
                //            }
                //        }
                //        if (file_index !== "") {
                //            data.splice(file_index, 1);
                //            file_index = "";
                //        }
                //    });
                //}

                //if (temp_edit_file.length > 0 && $('#hidMode').val() == "edit") {
                //    data.push(temp_edit_file[0]);
                //    temp_edit_file = [];
                //} else if (temp_data.length > 0 && $('#hidMode').val() == "edit") {
                //    data.push(temp_data[0]);
                //}
                data = [];
                temp_data = [];
                self.artFilesToUpload = [];
                self.uploadingFiles = "";
            }
        }
        else {
            if (isMultiple || isDragDrop) {
                //if (temp_edit_file.length == 0) {
                //    $.each(temp_data, function (key, val) {
                //        var file_index = "";
                //        for (var i = 0; i < data.length; i++) {
                //            if (data[i].name.toLowerCase() == val.name.toLowerCase() && data[i].type.toLowerCase() == val.type.toLowerCase()) {
                //                file_index = i;
                //            }
                //        }
                //        if (file_index !== "") {
                //            data.splice(file_index, 1);
                //            file_index = "";
                //        }
                //    });
                //}
                //if (temp_edit_file.length > 0 && $('#hidMode').val() == "edit") {
                //    data.push(temp_edit_file[0]);
                //    temp_edit_file = [];
                //} else if (temp_data.length > 0 && $('#hidMode').val() == "edit") {
                //    data.push(temp_data[0]);
                //}
                data = [];
                temp_data = [];
                self.artFilesToUpload = [];
                self.uploadingFiles = "";
            }
        }

        sessionStorage.artFilesToUpload = JSON.stringify(self.artFilesToUpload);
        $('#hidMode').val('');
        $('#popupListDefinition').popup('close');
        $('#hidUploadFile').val('');
        $('#hidEditUploadLocation').val('');
    };

    //Creates and displays the list of files when user selects the files to upload.
    self.makeFileListBeforePost = function (array_name) {
        var array = eval(array_name);
        var list_info = "";
        var art_files = "";
        var select_files = "";
        $("#ulFilesToUpload").empty();
        var row_to_add = (array_name.length > 0) ? '<li data-role="list-divider" >Selected Artwork Files for Upload</li>' : '';

        $.each(array_name, function (key, val) {
            if (val.uploadFileName != "") {
                var file_name = val.fileName;
                file_name = file_name.substring(file_name.lastIndexOf("\\") + 1);
                row_to_add += '<li><a href="#popupListDefinition" data-rel="popup" data-position-to="window" data-transition="pop" onclick="pageObj.getFileSettings(\'' + file_name + '\', \'' + val.fileName.substring(val.fileName.lastIndexOf('.') + 1) + '\', \'' + val.locationId + '\')">';
                row_to_add += '<h3>' + file_name + '</h3>';
                var version_key_code_display = (val.versionCode != "") ? ' | Version: ' + val.versionCode : "";
                row_to_add += ((val.approvalType != undefined && val.approvalType != null && val.approvalType != "") ? '<p>Approval Type: ' + ((val.approvalType.indexOf('both') > -1) ? "Proof/Production" : getApprovalType(parseInt(val.approvalType))) + " | " : "<p>");
                row_to_add += (jobCustomerNumber == BRIGHTHOUSE || jobCustomerNumber == OH) ? 'Format: ' + val.fileName.substring(val.fileName.lastIndexOf('.') + 1) + version_key_code_display + '<br /> Linked To: ' + val.linkedTo + '</p></br>' : ' Format: ' + val.fileName.substring(val.fileName.lastIndexOf('.') + 1) + version_key_code_display + '</p>';
                row_to_add += '</a><a href="#" onclick="pageObj.removeFileBeforeUpload(event,\'' + file_name + '\',\'' + val.locationId + '\')" >Remove List</a></li>';
                $("#popupListDefinition").popup("close");
            }
            else {
                $("#lnkUploadFile").show();
            }
        });
        if (row_to_add != "") {
            $("#ulFilesToUpload").append(row_to_add);
            //$('#tblFilesToUpload').show();
            $("#ulFilesToUpload").listview('refresh');
            $("#divFilesToUpload").show();
            $('#dvSubmit').css('display', 'block');
        } else {
            $('#dvSubmit').css('display', 'none');
        }
    }

    //Validates the file upload settings when user clicks on submit button.
    self.uploadArtValidation = function () {
        var msg = "<ul>";
        if ((!isMultiple && !isDragDrop) && !('draggable' in document.createElement('span'))) {
            var upload_type = ($('#ddlFileActions').val() != "select") ? $('#ddlFileActions').val() : "";
            if ($('#name').val() == "" && $('#hidMode').val() != "reassign")
                msg += "<li>Please choose a file to upload.</li>";
        }
        if ((jobCustomerNumber == CW || jobCustomerNumber == DCA || jobCustomerNumber == KUBOTA || jobCustomerNumber == SPORTSKING || jobCustomerNumber == GWA || jobCustomerNumber == CASEYS || jobCustomerNumber == EQUINOX || jobCustomerNumber == ALLIED || showSelectLocationValidation()) && ($('#dvSelectedLocations input[type=checkbox]:checked').length == 0 && $('#hidMode').val() != "replace") && !$('#dvDoNotAssign input[type=checkbox]').is(":checked")) {
            msg += "<li>Please select at least one location.</li>";
        }
        msg += "</ul>";
        if (msg != "<ul></ul>") {
            $("#popupListDefinition").popup("close");
            $('#validationAlertmsg').html(msg);
            window.setTimeout(function getDelay() {
                $('#validationPopupDialog').popup('open');
                return false;
            }, 200);
        }
        else
            return true;
    }

    //Validates selected file info and adds selected file to temporary list.
    self.updateFileList = function () {
        var upload_type = $('#ddlFileActions').val();
        if ((!isMultiple && !isDragDrop) && !self.uploadArtValidation()) return false;
        if ((isMultiple || isDragDrop || ('draggable' in document.createElement('span'))) && !self.uploadMultiplesValidation()) return false;

        var file_info = "";
        var file_name = ""
        var file_type = "";
        var linked_to = $('#ddlFilesUploaded').val();
        linked_to = (linked_to != undefined && linked_to != null && linked_to != "" && linked_to != "select") ? linked_to : "";

        if (!isMultiple && !isDragDrop) {
            file_info = $('#hidUploadFile').val();
            file_name = file_info.split('|')[0];
            file_type = file_info.split('|')[1];

            var check_file_name = (file_name.indexOf('\\') > -1) ? file_name.substring(file_name.lastIndexOf('\\') + 1, file_name.length) : file_name;

            if (!self.verifyFileExistence(check_file_name)) {
                $('#popupListDefinition').popup('close');
                $('#alertmsg').text('A file with the same name as an uploaded file cannot be uploaded. Either delete the original file or replace it by clicking the edit button on the uploaded file.');
                self.resetFileUploadControl(self.artFilesToUpload);
                self.fileCount = (self.fileCount <= 0) ? self.fileCount = 0 : self.fileCount - 1;
                window.setTimeout(function getDelay() {
                    $('#popupDialog').find('a[id=okBut]').bind('click', function () {
                        $("#popupDialog").popup("close");
                        $('#popupDialog').find('a[id=okBut]').attr('onclick', "$(\'#popupDialog\').popup(\'close\')");
                    });
                    $('#popupDialog').popup('open');
                    return false;
                }, 200);
                return false;
            }
            else {
                self.addTheSelectedFileToUpload(check_file_name, file_type);
            }
        }
        else {
            $('#btnUploadFiles').trigger('click');
        }
    }

    //Adds the selected file to the temporary list.
    self.addTheSelectedFileToUpload = function (file_name, file_type) {
        var uploading_file_name = "";
        if (file_name != "") {
            uploading_file_name = file_name.substring(file_name.lastIndexOf("\\") + 1);
            self.uploadingFiles += (self.uploadingFiles != "") ? "|" + uploading_file_name + "^" + getFileTypeCode('artwork') : uploading_file_name + "^" + getFileTypeCode('artwork');
            self.artApprovalTypes += ((self.artApprovalTypes != "") ? "^" : "") + (($('#ddlApprovalType').val() != "select") ? $('#ddlApprovalType').val() : "");
            self.updateFileInfo(uploading_file_name, file_type);
            self.makeFileListBeforePost(self.artFilesToUpload);
            if (!dontShowHintsAgain && (sessionStorage.showDemoHints == undefined || sessionStorage.showDemoHints == null || sessionStorage.showDemoHints == "" || sessionStorage.showDemoHints == "on") && (sessionStorage.isArtFileSelectedFirstTime == undefined || sessionStorage.isArtFileSelectedFirstTime == null || sessionStorage.isArtFileSelectedFirstTime == "" || sessionStorage.isArtFileSelectedFirstTime == "false") && self.artFilesToUpload.length == 1 && (showUploadSelectedArtDemoHint())) {
                sessionStorage.isArtFileSelectedFirstTime = true
                $('#uploadSelectedArtDemoHints').popup('open', { positionTo: '#btnUploadFiles' });
            }
        }
        else {
            $("#popupListDefinition").popup("close");
        }
    }

    //Removes the selected file from the list of files to upload (before upload).
    self.removeFileBeforeUpload = function (e, file_name, location_id) {
        $('#divError').text('');
        var spnFileInputs = ($.browser.msie) ? window.frames[0].document.getElementById('spnFileInput') : document.getElementById('spnFileInput');
        var file_name_to_remove = file_name;
        var filetype_to_remove = file_name.substring(file_name.lastIndexOf('.') + 1);
        var count = 0;
        var index = 0;

        //remove the file from the local session.
        var files_to_remove = jQuery.grep(self.artFilesToUpload, function (v) {
            count++;
            if (jobCustomerNumber == CW || jobCustomerNumber == BBB || jobCustomerNumber == DATAWORKS) {
                if ((v.fileName.substring(v.fileName.lastIndexOf('.') + 1) === filetype_to_remove) && (v.fileName.substring(v.fileName.lastIndexOf('\\') + 1) === file_name_to_remove) && (v.locationId.toLowerCase() == location_id.toLowerCase()))
                    index = count - 1;
                return (v.fileName.substring(v.fileName.lastIndexOf('.') + 1) === filetype_to_remove) ? ((v.locationId.toLowerCase() == location_id.toLowerCase()) && (v.fileName.substring(v.fileName.lastIndexOf('\\') + 1) === file_name_to_remove) ? false : true) : true;
            }
            else {
                if ((v.fileName.substring(v.fileName.lastIndexOf('.') + 1) === filetype_to_remove) && (v.fileName === file_name_to_remove))
                    index = count - 1;
                return (v.fileName.substring(v.fileName.lastIndexOf('.') + 1) === filetype_to_remove) ? ((v.fileName === file_name_to_remove) ? false : true) : true;
            }
        });
        var temp_uploading_files = self.uploadingFiles.split('|');
        var file_index = -1;
        self.uploadingFiles = "";
        $.each(temp_uploading_files, function (a, b) {
            if (b.substring(0, b.indexOf('^')).toLowerCase() != file_name_to_remove.toLowerCase()) {
                self.uploadingFiles = (self.uploadingFiles != "") ? self.uploadingFiles + "|" + b : b;
            }
            else if (b.substring(0, b.indexOf('^')).toLowerCase() == file_name_to_remove.toLowerCase())
                file_index = a;
        });

        var temp_art_app_types = (self.artApprovalTypes != undefined && self.artApprovalTypes != null) ? self.artApprovalTypes.split('^') : "";
        self.artApprovalTypes = "";
        $.each(temp_art_app_types, function (a, b) {
            if (a != file_index)
                self.artApprovalTypes = (self.artApprovalTypes != undefined && self.artApprovalTypes != null && self.artApprovalTypes != "") ? '^' + b : b;
        });

        if (isMultiple || isDragDrop) {
            count = 0;
            var data_files_to_remove = jQuery.grep(data, function (v) {
                count++;
                if ((v.type == filetype_to_remove) && (v.name.substring(v.name.lastIndexOf("\\") + 1) == file_name_to_remove))
                    index = count - 1;
                return (v.type == filetype_to_remove) ? ((v.name.substring(v.name.lastIndexOf("\\") + 1) == file_name_to_remove) ? false : true) : true;
            });
            if (index > -1) {
                data.splice(index, 1);
            }
        }

        self.artFilesToUpload = files_to_remove;
        if (isMultiple || isDragDrop) {
            self.fileCount = (self.fileCount <= 0) ? 0 : self.fileCount - 1;
        } else if (self.artFilesToUpload.length == 0 && (!isMultiple && !isDragDrop)) {
            $("#divFilesToUpload").hide();
            var tdFileInputsTemp = ($.browser.msie) ? window.frames[0].document.getElementById('formUpload') : document.getElementById('formUpload');
            var number_of_files_selected = $(tdFileInputsTemp).find('input[type=file]').length;
            var fileInput = $(tdFileInputsTemp).find('input[type=file]')[number_of_files_selected - 1];
            // Create a new file input
            var newFileInput = fileInput.cloneNode(true);
            newFileInput.value = null;
            var id = "mailFiles[]";
            newFileInput.id = id // A unique id
            newFileInput.name = newFileInput.id;
            newFileInput.style.display = "";
            $($(tdFileInputsTemp).find('input[type=file]')[number_of_files_selected - 1]).remove();
            var spnFileInputs = ($.browser.msie) ? window.frames[0].document.getElementById('spnFileInput') : document.getElementById('spnFileInput');
            spnFileInputs.appendChild(newFileInput);
            if (!isMultiple && !isDragDrop)
                self.resetFileUploadPopup();
            (($.browser.msie) ? $(window.frames[0].frames.frameElement).contents().find('input[type=file]') : $('input[type=file]')).bind('change', function (e) {
                self.navigate(e, $(e.target))
            });
            self.fileCount = 0;
            $('#ulFilesToUpload').empty();
        }
        else {
            //remove the file control from the form.
            $($((($.browser.msie) ? window.frames[0].document.getElementById('formUpload') : document.getElementById('formUpload'))).find('input[type=file]')[index]).remove();
            var number_of_files_selected = $('input[type=file]').length;
            if (number_of_files_selected > 0) {
                $($((($.browser.msie) ? window.frames[0].document.getElementById('formUpload') : document.getElementById('formUpload'))).find('input[type=file]')[number_of_files_selected - 1]).attr("id", "mailFiles[]");
                $($((($.browser.msie) ? window.frames[0].document.getElementById('formUpload') : document.getElementById('formUpload'))).find('input[type=file]')[number_of_files_selected - 1]).attr("name", "mailFiles[]");
            }
            self.fileCount = (self.fileCount <= 0) ? 0 : self.fileCount - 1;
        }
        self.makeFileListBeforePost(self.artFilesToUpload)
        sessionStorage.artFilesToUpload = JSON.stringify(self.artFilesToUpload);
    }

    //Validates the selected files (before upload list) have been uploaded or not, when user trying to navigate to other pages.
    self.validateUploadingArtFiles = function () {
        if (self.artFilesToUpload.length > 0) {
            var msg = "The files you have selected have not been uploaded to this order. Please click the 'Upload Selected Artwork' button to upload the selected files or Remove selected files you do not want to upload.";
            $('#alertmsg').text(msg);
            $('#popupDialog').popup('open');
            return false;
        }
        else {
            return true;
        }
    };
    //Closes the popup window.
    self.closeDialog = function () {
        $('#popupDialog').popup('open');
        $('#popupDialog').popup('close');
    }
    //******************** Public Functions End **************************
};