﻿$('#_sharedMailDashboard').live('pagebeforecreate', function (event) {
    displayMessage('_sharedMailDashboard');
    ko.setTemplateEngine(new ko.nativeTemplateEngine);
});

$(document).on('pageshow', '#_sharedMailDashboard', function (event) {
    window.setTimeout(function () {
        ko.applyBindings(new sharedMailVM());
    }, 1000);
});

var sharedMailVM = function () {
    var self = this;
    self.sharedMailInfo = ko.observable();
};