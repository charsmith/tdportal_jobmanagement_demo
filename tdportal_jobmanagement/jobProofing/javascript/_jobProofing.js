jQuery.support.cors = true;

//******************** Global Variables Start **************************
//Variables used when page loads initially from email link -- START --

var gServiceUrl = serviceURLDomain + "api/Authorization";
var gEditJobService = serviceURLDomain + "api/JobTicket/";

//Variables used when page loads from email link -- END --

var crocodocViewURL = "https://crocodoc.com/view/";
var gOutputData;
var jobNumber = "";
var facilityId = "";
jobNumber = getSessionData("jobNumber");
facilityId = getSessionData("facilityId");
var urlString = unescape(window.location)
if (appPrivileges != undefined && appPrivileges != null) {
    if (appPrivileges.customerNumber == ALLIED) {
        urlString = "../jobProofing/jobProofing.html?fileName=58683_GWBerkheimerOLSONCOMFORT.pdf&jobNumber=" + sessionStorage.jobNumber;
        getNextPageInOrder();
    }
    else if (appPrivileges.customerNumber == AAG) {
        urlString = "../jobProofing/jobProofing.html?fileName=TCA_10232_AAG_Lunch_Learn_2_Presenters-2_Events_Lenfers_Culbreth_Let.pdf&jobNumber=" + sessionStorage.jobNumber;
        getNextPageInOrder();
    }
    else if (appPrivileges.customerNumber == DCA || appPrivileges.customerNumber == KUBOTA || appPrivileges.customerNumber == SPORTSKING) {//Todo: Need to update pdf for Kubota customer
        urlString = "../jobProofing/jobProofing.html?fileName=DCA%2041941%20Jupiter%20Dental%20_Postcard_Dr.%20Vera_MECH.pdf&jobNumber=" + sessionStorage.jobNumber;
        if (sessionStorage.jobSetupOutput != undefined && sessionStorage.jobSetupOutput != null && sessionStorage.jobSetupOutput != "") {
            gOutputData = $.parseJSON(sessionStorage.jobSetupOutput);
            if (gOutputData.templateName.indexOf('_11x6_') > -1) {
                urlString = "../jobProofing/jobProofing.html?fileName=DCA_42754_DoubleDentRite_DM_Jupiter.pdf&jobNumber=" + sessionStorage.jobNumber;
            }
        }
        getNextPageInOrder();
    }
}
var queryString = urlString.substr(urlString.indexOf("?") + 1, urlString.length)
var params = queryString.split("&");
if (params.length >= 1){
    var approval_link = urlString.replace(/jobProofing/g, 'jobApproval');
    window.location.href = approval_link;
}
var artworkFileInfo = [];
var encodedFileName = getURLParameter("fileName");
var proofJobNumber = getURLParameter("jobNumber");
var proofFacilityId = getURLParameter("facilityId");
var crocSession = "";
var fileName = "";
var windowType = "";
var version = "";
var userName = "";
var proofingFileVersionList = [];
var encryptedString = (params[0] != undefined && params[0] != "") ? params[0] : "";
if ((window.opener != undefined && window.opener != null && window.opener.length > 0) && sessionStorage.isNewWindow == "true")
    windowType = "blank";
var approvalUserEmail = (sessionStorage.isOpenedFromEmail != undefined && sessionStorage.isOpenedFromEmail != null && sessionStorage.isOpenedFromEmail == "true") ? sessionStorage.approvalUserEmail : "";
var userApprovalLevel = "";
var isMarkupClosed = false;

setDemosProof();

//******************** Global Variables End **************************

//******************** Page Load Events Start **************************
$(window).on('popupbeforeposition', 'div:jqmData(role="popup")', function () {
    var notDismissible = $(this).jqmData('dismissible') === false;
    if (notDismissible) {
        $('.ui-popup-screen').off();
    }
});

$('#_jobProofing').live('pagebeforecreate', function (event) {
    displayMessage('_jobProofing');
    loadingImg('_jobProofing');
    createConfirmDialog();
    displayNavLinks();
    if (sessionStorage.userRole == "proofUser") $('#btnProofForward').hide(); //hides Forward button if the user is proof user.
    var image_path = "../images/gizmo_load_anim.gif";
    var over = '<br/><br/><br/><br/><br/><br/><br/><br/><center style="top:800px"><span id="spnWaitImage" style="width:500px;height:200px;"><img id="loading1" alt="Loading...." src="' + image_path + '" /><p><span id="loadingText1" style="font-weight:bold;color:#4a80af;font-family:Tahoma,verdana, Arial;font-size:large;">Loading....</span></p></span></center>';
    $('#viewFrame')[0].contentWindow.document.body.innerHTML = over;
});

$(document).bind('pageshow', '#_jobProofing', function (event) {
    if (sessionStorage.isFirstTime == undefined || sessionStorage.isFirstTime == null)
        sessionStorage.isFirstTime = true;
    gOutputData = (sessionStorage.jobSetupOutput != undefined && sessionStorage.jobSetupOutput != null) ? jQuery.parseJSON(sessionStorage.jobSetupOutput) : [];
    if (gOutputData != undefined && gOutputData != null && gOutputData.length > 0 && (params.length > 1)) {
        getMilestoneInfo();
        getBubbleCounts(gOutputData);
    }
    if (!checkVersion()) {
        event.preventDefault();
        $('#dvProofButtons').hide();
        $('#btnContinue').hide();
        $('#viewFrame').css('display', 'none');
        $('#dvProofFileName').css('display', 'none');
        var frame_height = (window.screen.availHeight > 860) ? window.screen.availHeight - 154 : '100%';
        $('#dvBrowserInfo').css('display', 'block');
        $('#dvBrowserInfo').css('height', frame_height);
        $('#dvBrowserInfo').css('width', '100%');
        $('#dvBrowserInfo').css('position', 'absolute');
        $('#dvBrowserInfo').css('top', '100px');
        $('#dvBrowserInfo').css('text-align', 'center');
        $('#alertmsg').text("This proofing system does not support Internet Explorer version 8 or older versions. Please use a current version of Internet Explorer, Chrome, Firefox or Safari.");
        $('#popupDialog').popup('open');
    }
    else {
        $('#dvEmailLinkFor').css('display', 'none');
        $('#btnProofForward').css('display', 'block');
        if (params.length == 1) {
            sessionStorage.isOpenedFromEmail = true;
            $('#btnContinue').css('display', 'none');
            $('#btnProofForward').css('display', 'none');
            authorizeAndGetProof();
        }
        else {
            if (windowType != "blank")
                getAuthorizedProof();
        }

        $('.ui-panel-content-wrap').removeClass('ui-panel-content-wrap');
        if (windowType == "blank") {
            selected_version = (sessionStorage.selectedVersion != undefined && sessionStorage.selectedVersion != null && sessionStorage.selectedVersion != "") ? jQuery.parseJSON(sessionStorage.selectedVersion) : [];
            if (selected_version.length > 0) {
                if ((sessionStorage.isOpenedFromEmail != undefined && sessionStorage.isOpenedFromEmail != null && sessionStorage.isOpenedFromEmail != "" && sessionStorage.isOpenedFromEmail == "true") || (sessionStorage.approvalUserEmail != undefined && sessionStorage.approvalUserEmail != null && sessionStorage.approvalUserEmail != ""))
                    approvalUserEmail = sessionStorage.approvalUserEmail;
                jobNumber = selected_version[0].jobNumber;
                facilityId = selected_version[0].facilityId;
                fileName = selected_version[0].fileName;
                $('#artApproval').css('display', 'block')
                if (selected_version[0].index > 0) $('#artApproval').css('display', 'none');;
                if (selected_version[0].proofUUID != "")
                    viewProof(selected_version[0].fileName, selected_version[0].proofCreatedDate, selected_version[0].proofUUID, selected_version[0].facilityId, selected_version[0].jobNumber, 'blank', selected_version[0].index);
                else {

                    getAuthorizedProof('job_proofs');
                }
            }
        }
        $('#btnProofForward').css('display', 'block');
        if (sessionStorage.isOpenedFromEmail == "true") {
            $('#btnContinue').css('display', 'none');
            $('#btnProofForward').css('display', 'none');
        }
        window.setTimeout(function a() {
            $('#footer').find('a').remove();
        }, 200);
    }
    $.each($('#dvProofButtons'), function (a, b) {
        $(b).controlgroup();
    });
    window.setTimeout(function a() {
        updateFooterInfo();
    }, 1000);
});

function authorizeAndGetProof() {
    var post_data = {};
    post_data["encryptedString"] = "";
    post_data.encryptedString = queryString;

    postCORS(serviceURLDomain + "api/ProofV2/bob", JSON.stringify(post_data), function (response) {
        if (response != undefined && response != null && response != "") {
            loginAuthorization(response);
            $('#btnContinue').attr('onclick', 'continueToOrder()');
            $('#btnContinue').css('display', 'none');
            $('#btnProofForward').css('display', 'none');
            window.setTimeout(function hideButtons() {
                if (response.isPowerUser == 1) {
                    var job_customer_number = (sessionStorage.jobCustomerNumber != undefined && sessionStorage.jobCustomerNumber != null && sessionStorage.jobCustomerNumber != "") ? sessionStorage.jobCustomerNumber : appPrivileges.customerNumber;
                    if ((job_customer_number != BBB && job_customer_number != AAG && job_customer_number != ALLIED) || ((job_customer_number == BBB || job_customer_number == AAG || job_customer_number == ALLIED) && appPrivileges.roleName == "admin")) {
                        $('#btnContinue').css('display', 'block');
                        $('#btnProofForward').css('display', 'block');
                    }
                    else {
                        $('#btnContinue').css('display', 'none');
                        $('#btnProofForward').css('display', 'none');
                        $('#demoInstructions1').addClass('ui-first-child ui-last-child');
                    }
                    //$('#btnContinue').show();
                    $.each($('#dvProofButtons'), function (a, b) {
                        $(b).controlgroup();
                    });
                }
            }, 2000);
        }

    }, function (response_error) {
        $('#viewFrame')[0].contentWindow.document.body.innerHTML = "";
        showErrorResponseText(response_error, false);
        //if (!(jQuery.browser.msie)) {
        //    $('#alertmsg').text((response_error.responseText != undefined) ? response_error.responseText : response_error.statusText);
        //}
        //else {
        //    $('#alertmsg').text((response_error.errorMessage != undefined) ? response_error.errorMessage : ((response_error.responseText != undefined) ? response_error.responseText : response_error.Message));
        //}

        //$('#popupDialog').popup('open');
    });
}

function getAuthorizedProof(window_origin) {
    if (queryString.split("=")[1].indexOf('&') > -1)
        fileName = queryString.split("=")[1].substring(0, queryString.split("=")[1].indexOf('&'));
    else
        fileName = queryString.split("=")[1];

    if (params.length > 2)
        version = params[2].substring(params[2].indexOf('=') + 1, params[2].length)

    getCrocdocProofing(encodedFileName, window_origin);

    $('#btnContinue').attr('onclick', 'continueToOrder()');
    if (windowType != "blank")
        displayJobInfo();

    if (sessionStorage.jobSetupOutput != undefined) {
        gOutputData = jQuery.parseJSON(sessionStorage.jobSetupOutput);
        if (gOutputData.artworkAction != undefined && gOutputData.artworkAction != null) {
            if (Object.keys(gOutputData.artworkAction.artworkFileList).length > 0) {
                artworkFileInfo[0] = (gOutputData.artworkAction.artworkFileList[fileName] != undefined && gOutputData.artworkAction.artworkFileList[fileName] != null && Object.keys(gOutputData.artworkAction.artworkFileList[fileName]).length > 0) ? gOutputData.artworkAction.artworkFileList[fileName] : {};
            }
            $('#btnJobProofs').show();
            if (Object.keys(gOutputData.artworkAction.artworkFileList).length == 1) {
                $('#btnJobProofs').hide();
                $.each($('#dvProofButtons'), function (a, b) {
                    $(b).controlgroup();
                });
            }
            artworkFileInfo = (artworkFileInfo[0] != undefined && artworkFileInfo[0] != null) ? artworkFileInfo[0] : [];
        }
    }
}

function getCrocdocProofing(encoded_file_name, window_origin) {
    var job_customer_number = (sessionStorage.jobCustomerNumber != undefined && sessionStorage.jobCustomerNumber != null && sessionStorage.jobCustomerNumber != "") ? sessionStorage.jobCustomerNumber : appPrivileges.customerNumber;
    if (sessionStorage.isOpenedFromEmail == "true") job_customer_number = sessionStorage.customerNumber;
    var user_email_id = (sessionStorage.approvalUserEmail != undefined && sessionStorage.approvalUserEmail != null && sessionStorage.approvalUserEmail != "") ? sessionStorage.approvalUserEmail : "null";
    //if (params.length >= 3 && params[2].substring(0, params[2].indexOf('=')) == "isJobProof" && params[2].substring(params[2].indexOf('=') + 1) == "true" && sessionStorage.isOpenedFromEmail == "true") {
    //if (sessionStorage.isOpenedFromEmail == "true") {
    getCORS(serviceURLDomain + "api/ProofV3/" + facilityId + "/" + job_customer_number + "/" + proofJobNumber + "/" + encoded_file_name + "/" + user_email_id, null, function (data) {
        loadProofsInfo(data, window_origin);
    }, function (error_response) {
        $('#viewFrame')[0].contentWindow.document.body.innerHTML = "";
        showErrorResponseText(error_response, true);
        //if (!(jQuery.browser.msie)) {
        //    $('#alertmsg').text((error_response.responseText != undefined) ? error_response.responseText : error_response.statusText);
        //}
        //else {
        //    $('#alertmsg').text((error_response.errorMessage != undefined) ? error_response.errorMessage : ((error_response.responseText != undefined) ? error_response.responseText : error_response.Message));
        //}
        //$('#popupDialog').popup('open');
    });
    //    }
    //    else {
    //        getCORS(serviceURLDomain + "api/ProofV2/" + facilityId + "/" + job_customer_number + "/" + proofJobNumber + "/" + encoded_file_name, null, function (data) {
    //            loadProofsInfo(data, window_origin);
    //        }, function (error_response) {
    //            $('#viewFrame')[0].contentWindow.document.body.innerHTML = "";
    //            if (!(jQuery.browser.msie)) {
    //                $('#alertmsg').text((error_response.responseText != undefined) ? error_response.responseText : error_response.statusText);
    //            }
    //            else {
    //                $('#alertmsg').text((error_response.errorMessage != undefined) ? error_response.errorMessage : ((error_response.responseText != undefined) ? error_response.responseText : error_response.Message));
    //            }
    //            $('#popupDialog').popup('open');
    //        });
    //    }
}

function loadProofsInfo(data, window_origin) {
    var version_list = [];
    if (data != undefined && data != null && data != "") {
        //jobNumber = data.jobNumber;
        //facilityId = data.facitlityId;
        //fileName = data.origProofName;
        var visited_proofs = [];
        visited_proofs = (sessionStorage.visitedProofs != undefined && sessionStorage.visitedProofs != null) ? $.parseJSON(sessionStorage.visitedProofs) : [];
        approvalUserEmail = (data.email != undefined && data.email != null) ? data.email : "";
        if (sessionStorage.isOpenedFromEmail != undefined && sessionStorage.isOpenedFromEmail != null && sessionStorage.isOpenedFromEmail != "" && sessionStorage.isOpenedFromEmail == "true" && sessionStorage.approvalUserEmail != undefined && sessionStorage.approvalUserEmail != null && sessionStorage.approvalUserEmail != "")
            approvalUserEmail = sessionStorage.approvalUserEmail;
        if (data.proofingFileVersionList != undefined && data.proofingFileVersionList != null && data.proofingFileVersionList.length > 0) {
            var current_view_proof = {};

            userApprovalLevel = data.approvalLevel;
            $.each(data.proofingFileVersionList.reverse(), function (key, val) {
                var visited_file = $.grep(visited_proofs, function (obj) {
                    return obj.fileName == val.origFileName;
                });
                var is_styled = false;
                if (key == 0)
                    is_styled = true;
                version_list.push({
                    "id": "lnk" + val.uuid,
                    "proofUUID": val.uuid,
                    "versionText": val.createdDate + "\n\r" + val.origFileName,
                    "fileName": val.origFileName,
                    "createdDate": val.createdDate,
                    "style": (is_styled || visited_file.length > 0) ? "color:#0033CC" : "",
                    "facilityId": facilityId,
                    "jobNumber": jobNumber,
                    "index": key
                });
                if (key == 0) {
                    if (visited_file.length == 0)
                        visited_proofs.push({
                            "fileName": val.origFileName
                        });
                    sessionStorage.visitedProofs = JSON.stringify(visited_proofs);
                    current_view_proof = $.extend(true, {}, val);
                    current_view_proof["index"] = key;
                    //current_view_proof = val;
                }
            });
            $('#ulHistory').empty();
            $('#ulHistory').append($("#historyTemplate").tmpl({ versions: version_list }));
            $('#ulHistory').append($("#historyTemplate").tmpl(version_list));
            $('#ulHistory').listview('refresh');
            $('#btnProofHistory').show();
            if (data.proofingFileVersionList.length == 1) {
                $('#btnProofHistory').hide();
                $.each($('#dvProofButtons'), function (a, b) {
                    $(b).controlgroup();
                });
            }
            if (data.proofingFileVersionList != undefined && data.proofingFileVersionList != null && data.proofingFileVersionList.length > 0) {
                sessionStorage.proofingFileVersionList = JSON.stringify(data.proofingFileVersionList);
                viewProof(current_view_proof.origFileName, current_view_proof.createdDate, current_view_proof.uuid, facilityId, jobNumber, '', window_origin, current_view_proof.index);
            }
            $('#artApproval').show();
            var job_customer_number = (sessionStorage.jobCustomerNumber != undefined && sessionStorage.jobCustomerNumber != null && sessionStorage.jobCustomerNumber != "") ? sessionStorage.jobCustomerNumber : appPrivileges.customerNumber;
            if (data.approvalLevel != undefined && data.approvalLevel != null && data.approvalLevel == 0 && job_customer_number != BBB && job_customer_number != AAG && job_customer_number != ALLIED) {
                $('#artApproval').hide();
            }
            window.setTimeout(function toggleButtons() {
                if (data.isPowerUser != undefined && data.isPowerUser != null && data.isPowerUser == "1") {
                    //if (data.customerNumber != BBB)
                    if ((job_customer_number != BBB && job_customer_number != AAG && job_customer_number != ALLIED) || ((job_customer_number == BBB || job_customer_number == AAG || job_customer_number == ALLIED) && appPrivileges.roleName == "admin"))
                        $('#btnContinue').css('display', 'block');
                    else {
                        $('#btnContinue').css('display', 'none');
                        $('#btnProofForward').css('display', 'none');
                        $('#demoInstructions1').addClass('ui-first-child ui-last-child');
                    }
                    //if (!isMarkupClosed && data.customerNumber != BBB)
                    if (!isMarkupClosed && (job_customer_number != BBB && job_customer_number != ALLIED) || ((job_customer_number == BBB || job_customer_number == AAG || job_customer_number == ALLIED) && appPrivileges.roleName == "admin"))
                        $('#btnProofForward').css('display', 'block');
                    $.each($('#dvProofButtons'), function (a, b) {
                        $(b).controlgroup();
                    });
                }
                else
                    $.each($('#dvProofButtons'), function (a, b) {
                        $(b).controlgroup();
                    });
            }, 2000);
            $.each($('#dvProofButtons'), function (a, b) {
                $(b).controlgroup();
            });
        }
        else {
            $('#viewFrame')[0].contentWindow.document.body.innerHTML = "";
            $('#alertmsg').text("Preview not available.");
            $('#popupDialog').popup('open');
        }
    }
}

function viewProof(proof_file_name, proof_create_date, proof_uuid, facility_id, job_number, window_type, window_origin, proof_index) {
    var visited_proofs = [];
    visited_proofs = (sessionStorage.visitedProofs != undefined && sessionStorage.visitedProofs != null) ? $.parseJSON(sessionStorage.visitedProofs) : [];

    if (gOutputData != undefined && gOutputData != null && (params.length > 1))
        getMilestoneInfo();

    if (gOutputData != undefined && gOutputData != null && Object.keys(gOutputData.artworkAction.artworkFileList).length == 1) {
        $('#btnJobProofs').hide();
        var available_buttons = $('#dvProofButtons').find('a[data-role=button]');
        $.each(available_buttons, function (key, val) {
            if (key == 0) {
                $(available_buttons[0]).addClass('ui-corner-lt');
                $(available_buttons[0]).addClass('ui-corner-lb');
            }
            if (key == (available_buttons.length - 1)) {
                $(available_buttons[0]).addClass('ui-corner-rt');
                $(available_buttons[0]).addClass('ui-corner-rb');
            }
        });
    }

    else {
        $('#btnJobProofs').show();
        $('#artApproval').css('display', 'block')
        if (proof_index > 0) $('#artApproval').css('display', 'none');;
    }
    if (proof_index > 0) $('#btnProofForward').css('display', 'none'); else ((sessionStorage.isOpenedFromEmail == "true") ? $('#btnProofForward').css('display', 'none') : $('#btnProofForward').css('display', 'block'));
    var proof_service_url = "";

    if (sessionStorage.isOpenedFromEmail == "true")
        sessionStorage.approvalUserEmail = approvalUserEmail;

    if (params.length == 1) {
        var current_proof_json = {};
        current_proof_json["encryptedString"] = queryString;
        current_proof_json["uuid"] = proof_uuid;
        postCORS(serviceURLDomain + "api/Proof_uuid_enc", JSON.stringify(current_proof_json), function (data) {
            crocSession = (data != "" && data.length > 1) ? $.parseJSON(data) : "";
            displayProof(proof_file_name, proof_create_date, proof_uuid, facility_id, job_number, window_type, window_origin, visited_proofs);
        }, function (error_response) {
            $('#popupHistory').popup('close');
            showErrorResponseText(error_response, false);
            //if (!(jQuery.browser.msie)) {
            //    $('#alertmsg').text((error_response.responseText != undefined) ? error_response.responseText : error_response.statusText);
            //}
            //else {
            //    $('#alertmsg').text((error_response.errorMessage != undefined) ? error_response.errorMessage : ((error_response.responseText != undefined) ? error_response.responseText : error_response.Message));
            //}
            //$('#popupDialog').popup('open');
        });
    }
    else {
        if (params.length >= 3 && params[2].substring(0, params[2].indexOf('=')) == "isJobProof" && params[2].substring(params[2].indexOf('=') + 1) == "true" && sessionStorage.isOpenedFromEmail == "true") {
            getCORS(serviceURLDomain + "api/Proof_uuid_email/" + facility_id + "/" + job_number + "/" + proof_uuid + "/" + sessionStorage.approvalUserEmail, null, function (data) {
                crocSession = data;
                displayProof(proof_file_name, proof_create_date, proof_uuid, facility_id, job_number, window_type, window_origin, visited_proofs);
            }, function (error_response) {
                $('#popupHistory').popup('close');
                if (!(jQuery.browser.msie)) {
                    $('#alertmsg').text((error_response.responseText != undefined) ? error_response.responseText : error_response.statusText);
                }
                else {
                    $('#alertmsg').text((error_response.errorMessage != undefined) ? error_response.errorMessage : ((error_response.responseText != undefined) ? error_response.responseText : error_response.Message));
                }
                $('#popupDialog').popup('open');
            });

        }
        else
            getCORS(serviceURLDomain + "api/Proof_uuid_email/" + facility_id + "/" + job_number + "/" + proof_uuid + "/" + sessionStorage.approvalUserEmail, null, function (data) { ////To Be Removed Later
                //getCORS(serviceURLDomain + "api/Proof_uuid/" + facility_id + "/" + job_number + "/" + proof_uuid, null, function (data) { //To Be Opened Later
                crocSession = data;
                displayProof(proof_file_name, proof_create_date, proof_uuid, facility_id, job_number, window_type, window_origin, visited_proofs);
            }, function (error_response) {
                $('#popupHistory').popup('close');
                showErrorResponseText(error_response, false);
                //if (!(jQuery.browser.msie)) {
                //    $('#alertmsg').text((error_response.responseText != undefined) ? error_response.responseText : error_response.statusText);
                //}
                //else {
                //    $('#alertmsg').text((error_response.errorMessage != undefined) ? error_response.errorMessage : ((error_response.responseText != undefined) ? error_response.responseText : error_response.Message));
                //}
                //$('#popupDialog').popup('open');

            });
    }
}


function displayProof(proof_file_name, proof_create_date, proof_uuid, facility_id, job_number, window_type, window_origin, visited_proofs) {
    $('#popupHistory').popup('close');
    if (crocSession != null) {
        var ipage = crocodocViewURL + crocSession;
        window.setTimeout(function setDelay() {
            $('#viewFrame').attr('src', '' + ipage + '');
            $('#lnk' + proof_uuid).css('color', '#0033CC');
            var frame_height = (window.screen.availHeight > 860) ? window.screen.availHeight - 154 : '100%';
            $('#viewFrame').css('height', frame_height);
        }, 1000);
        fileName = proof_file_name;
        sessionStorage.currentProofUUID = proof_uuid;
    }
    var visited_file = $.grep(visited_proofs, function (obj) {
        return obj.fileName == proof_file_name;
    });
    if (visited_file.length == 0)
        visited_proofs.push({
            "fileName": proof_file_name
        });
    sessionStorage.visitedProofs = JSON.stringify(visited_proofs);

    var current_file_version = [];
    current_file_version = getCurrentProofVersion();


    var markup_closed_text = "";
    if (gOutputData.milestoneAction.milestoneDict != undefined && JSON.stringify(gOutputData.milestoneAction.milestoneDict).length > 2) {
        if (gOutputData.milestoneAction.milestoneDict.artarrival != undefined && gOutputData.milestoneAction.milestoneDict.artarrival != null && gOutputData.milestoneAction.milestoneDict.artarrival.completedDate != "" && gOutputData.milestoneAction.milestoneDict.artarrival.completedBy != "") {
            markup_closed_text = "<span  class='ui-li-aside'><img src='../images/alert_icon_whitee.png'/>&nbsp;MARKUP & APPROVALS HAVE BEEN CLOSED</span>"
            $('#artApproval').css('display', 'none');
            $('#artApproval').hide();
            $('#btnProofForward').css('display', 'none');
            $('#btnProofForward').hide();
            $.each($('#dvProofButtons'), function (a, b) {
                $(b).controlgroup();
            });
            isMarkupClosed = true;
        }
    }


    var file_name_header = "<b>Name: </b> " + proof_file_name + "<b> | Uploaded: </b>" + proof_create_date;
    var proof_status_text = '';

    if (current_file_version.length > 0) {
        var approval_type = "";
        if (current_file_version[0].approvalType != undefined && current_file_version[0].approvalType != null && current_file_version[0].approvalType != "") {
            if (current_file_version[0].approvalType == 2)
                approval_type = "Print";
            else
                approval_type = "Lettershop";
        }
        file_name_header += "<b> | Approval Type: </b>" + approval_type + "<b> | User: </b>" + ((approvalUserEmail.toLowerCase().indexOf('senecaglobal') > -1) ? "" : approvalUserEmail);
        //        if (current_file_version[0].approvalStatus != undefined && current_file_version[0].approvalStatus != null && markup_closed_text == "") {
        //            if (current_file_version[0].approvalStatus == "1" || current_file_version[0].approvalStatus == "6") {
        //                $('#dvProofFileName').removeClass('ui-bar-a');
        //                $('#dvProofFileName').addClass('ui-bar-i');
        //            }
        //            else if (current_file_version[0].approvalStatus == "0") {
        //                $('#dvProofFileName').removeClass('ui-bar-a');
        //                $('#dvProofFileName').addClass('ui-bar-h');
        //            }
        //            var approval_icon = "";
        //            switch (current_file_version[0].approvalStatus) {
        //                case 6:
        //                    approval_icon = "icon_approved_with_changes_white.png";
        //                    break;
        //                case 1:
        //                    approval_icon = "icon_approved_white.png";
        //                    break;
        //                case 0:
        //                    approval_icon = "icon_rejected_white.png";
        //                    break;
        //            }
        //            var approval_status = getApprovalStatus(parseInt(current_file_version[0].approvalStatus));
        //            if (approval_icon != "")
        //                proof_status_text = '<div style="width:11%;float:right;padding-right:5px"><div class="ui-grid-c" style="width:70%"><div class="ui-block-a" style="width:auto;"><img src="images/' + approval_icon + '" title="' + approval_status + '" class="ui-li-icon ui-corner-none" style="padding-right:4px"/></div><div class="ui-block-b" style="width:auto;"><b>' + approval_status.initCap() + '</b></div><div class="ui-block-c" style="width:auto;"><b> ' + '</b></div></div></div>';

        //            //file_name_header += "<b> | Approval Type: </b>" + approval_type + "<b> | Approved By: </b>" + current_file_version[0].approvalLastUpdatedBy;
        //            //file_name_header += "<b> | Approval Type: </b>" + approval_type + "<b> | User: </b>" + current_file_version[0].approvalLastUpdatedBy;
        //            file_name_header += "<b> | Approval Type: </b>" + approval_type + "<b> | User: </b>" + approvalUserEmail;
        //        }

    }
    var artworks_files_length = 0;
    if (sessionStorage.jobSetupOutput != undefined) {
        gOutputData = jQuery.parseJSON(sessionStorage.jobSetupOutput);
        if (gOutputData.artworkAction != undefined && gOutputData.artworkAction != null) {
            if (Object.keys(gOutputData.artworkAction.artworkFileList).length > 0) {
                artworks_files_length = Object.keys(gOutputData.artworkAction.artworkFileList).length;
                artworkFileInfo[0] = (gOutputData.artworkAction.artworkFileList[fileName] != undefined && gOutputData.artworkAction.artworkFileList[fileName] != null && Object.keys(gOutputData.artworkAction.artworkFileList[fileName]).length > 0) ? gOutputData.artworkAction.artworkFileList[fileName] : {};
                artworkFileInfo = (artworkFileInfo[0] != undefined && artworkFileInfo[0] != null) ? artworkFileInfo[0] : [];

                if (artworkFileInfo != undefined && artworkFileInfo != null && Object.keys(artworkFileInfo).length > 0 && artworkFileInfo.approverList != undefined && artworkFileInfo.approverList != null && artworkFileInfo.approverList.length > 0) {
                    $.each(artworkFileInfo.approverList, function (key, val) {
                        if (val.email == approvalUserEmail) {
                            if (val.approvalStatus != undefined && val.approvalStatus != null) {
                                if (val.approvalStatus == "1" || val.approvalStatus == "6") {
                                    $('#dvProofFileName').removeClass('ui-bar-a');
                                    $('#dvProofFileName').addClass('ui-bar-i');
                                }
                                else if (val.approvalStatus == "0") {
                                    $('#dvProofFileName').removeClass('ui-bar-a');
                                    $('#dvProofFileName').addClass('ui-bar-h');
                                }
                                var approval_icon = "";
                                switch (val.approvalStatus) {
                                    case 6:
                                        approval_icon = "icon_approved_with_changes_white.png";
                                        break;
                                    case 1:
                                        approval_icon = "icon_approved_white.png";
                                        break;
                                    case 0:
                                        approval_icon = "icon_rejected_white.png";
                                        break;
                                }
                                var approval_status = getApprovalStatus(parseInt(val.approvalStatus));
                                if (approval_icon != "")
                                    proof_status_text = '<div style="width:11%;float:right;padding-right:5px"><div class="ui-grid-c" style="width:70%"><div class="ui-block-a" style="width:auto;"><img src="images/' + approval_icon + '" title="' + approval_status + '" class="ui-li-icon ui-corner-none" style="padding-right:4px"/></div><div class="ui-block-b" style="width:auto;"><b>' + approval_status.initCap() + '</b></div><div class="ui-block-c" style="width:auto;"><b> ' + '</b></div></div></div>';

                                //file_name_header += "<b> | Approval Type: </b>" + approval_type + "<b> | Approved By: </b>" + current_file_version[0].approvalLastUpdatedBy;
                                //file_name_header += "<b> | Approval Type: </b>" + approval_type + "<b> | User: </b>" + current_file_version[0].approvalLastUpdatedBy;
                            }
                            return false;
                        }
                    });
                }
            }
        }
    }


    $('#dvProofFileName').html(file_name_header + ((markup_closed_text == "") ? markup_closed_text + proof_status_text : markup_closed_text));

    if (window_type == "self" || window_type == "blank" || window_origin == 'job_proofs') {
        if (window_type == "blank" || window_origin == 'job_proofs') {
            $('div[data-role="header"]').hide();
        }
    }
    window.setTimeout(function setDelay() {
        if (sessionStorage.isFirstTime != undefined && sessionStorage.isFirstTime != null && sessionStorage.isFirstTime == "true") {
            sessionStorage.isFirstTime = false;
            if (markup_closed_text != "")
                $('#popupClosedProofingInstructios').popup('open');
            else {
                if (gOutputData.artworkAction != undefined && gOutputData.artworkAction != null)
                    if (Object.keys(gOutputData.artworkAction.artworkFileList).length == 0)
                        $('#multipleProofsIns').css('display', 'none');
                if (userApprovalLevel == 0)
                    $('#approverIns').css('display', 'none');
                $('#popupProofingInstructions').popup('open');
            }
        }
        if (markup_closed_text != "") {
            $('#artApproval').css('display', 'none');
            //$('#artApproal').hide();
            $('#btnProofForward').css('display', 'none');
            //$('#btnProofForward').hide();
            $.each($('#dvProofButtons'), function (a, b) {
                $(b).controlgroup();
            });
        }
        if (jobCustomerNumber == BBB || jobCustomerNumber == AAG || jobCustomerNumber == ALLIED) {
            if (appPrivileges.roleName != "admin")
                $('#btnProofForward').css('display', 'none');
            if (artworks_files_length > 1)
                $('#btnJobProofs').addClass('ui-first-child');
            else {
                $('#btnJobProofs').css('display', 'none');
                $('#demoInstructions1').addClass('ui-first-child ui-last-child');
            }
        }
    }, 2000);
    $('#historyPanel').panel('close', 'optionsHash');
    $('#jobProofsPanel').panel('close', 'optionsHash');
}

function fnApprovalValidation(type) {
    if ((type == undefined || type == null || type == "") && $("input[type=radio]:checked").val() === undefined) {
        return false;
    }
    else if (type === "approval" && $("input[type=radio]:checked").val() === undefined) {
        $('#alertmsg').text('Please choose any one of the approval type.');
        $('#popupApproval').popup('close');
        $('#popupDialog').popup('open');
        $('#okBut').attr('onclick', '$("#popupDialog").popup("close");$("#popupApproval").popup("open", { positionTo: "#artApproval" });');
        return false;
    }
    return true;
}


function applyToAll() {
    if (!fnApprovalValidation('approval')) return false;
    $('#okButConfirm').text('Submit');
    $('#okButConfirm').attr('onclick', 'continueApplyToAll()');
    $('#cancelButConfirm').attr('onclick', 'cancelApplyToAll()');
    $('#confirmMsg').text('The selected approval status will be applied to all proofs in this job if you submit.');
    $("#popupApproval").popup("close");
    $('#popupConfirmDialog').popup('open');
}

function cancelApplyToAll() {
    $('#popupConfirmDialog').popup('close');
    $("#popupApproval").popup("close");
    $("#popupApproval").popup("open", { positionTo: '#artApproval' });
}

function continueApplyToAll() {
    if (fnApprovalValidation("")) {
        $('#popupConfirmDialog').popup('close');
        //$("#popupApproval").popup("open", { positionTo: '#artApproal' }); ;
        //$('#btnSubmitApprovals').attr('onclick', 'submitApprovals("approval",true)');
        submitApprovals("approval", true);
    }
    else {
        $('#btnSubmitApprovals').attr('onclick', 'submitApprovals("approval",false)');
        $('#alertmsg').text('You must select an approval status to apply to all of the proofs.');
        $('#popupConfirmDialog').popup('close');
        $('#popupDialog').popup('open');
        $('#okBut').attr('onclick', '$("#popupDialog").popup("close");$("#popupApproval").popup("open", { positionTo: "#artApproval" });');
    }
}

function submitApprovals(type, is_apply_to_all) {
    var job_customer_number = (sessionStorage.jobCustomerNumber != undefined && sessionStorage.jobCustomerNumber != null && sessionStorage.jobCustomerNumber != "") ? sessionStorage.jobCustomerNumber : appPrivileges.customerNumber;

    if (!fnApprovalValidation(type)) return false; //checking whether any one of the approval radio control selected

    if ((job_customer_number == BBB || job_customer_number == AAG || job_customer_number == ALLIED) && appPrivileges.roleName != "admin") {
        $('#alertmsg').text("Thanks, but this is a demo. No further action will be taken.");
        $('#popupApproval').popup('close');
        $('#popupDialog').popup('open');
        $('#okBut').attr('onclick', '$("#popupDialog").popup("close");');
        return false;
    }

    var selected_proofs_list = $('#dvProofsToApprove input[type=checkbox]:not([id=chkSelectAllProofs_approve])');
    var selected_proofs = [];
    $.each(selected_proofs_list, function (a, b) {
        selected_proofs.push({
            "fileName": $(b).val(),
            "requiredApproval": ($(b)[0].checked) ? 1 : 0
        });
    });

    var proofing_file_versions_list = [];
    proofing_file_versions_list = (sessionStorage.proofingFileVersionList != undefined && sessionStorage.proofingFileVersionList != null && sessionStorage.proofingFileVersionList != "") ? jQuery.parseJSON(sessionStorage.proofingFileVersionList) : [];
    var selected_file_version = (sessionStorage.currentProofUUID != undefined && sessionStorage.currentProofUUID != null && sessionStorage.currentProofUUID != "") ? sessionStorage.currentProofUUID : "";
    var current_file_version = [];
    if (selected_file_version != undefined && selected_file_version != null && selected_file_version != "") {
        current_file_version = $.grep(proofing_file_versions_list, function (obj) {
            return obj.uuid === selected_file_version && obj.origFileName === fileName;
        });
    }
    if (current_file_version.length > 0) {
        if (current_file_version[0].approvalStatus != undefined && current_file_version[0].approvalStatus != null)
            current_file_version[0].approvalStatus = $("input[type=radio]:checked").val();

        //current_file_version[0].approvalType = "3"; // NOT REQUIRED TO UPDATE AT PRESENT. DT.04/21/2014.

        //current_file_version[0].approvalLastUpdatedBy = $('#txtApproverName').val();
        current_file_version[0].approvalLastUpdatedBy = $('#txtApproverName').text();
        //current_file_version[0].wantsApproveAll = (is_apply_to_all) ? "1" : "0";
        current_file_version[0].applyApprovalsTo = selected_proofs;
    }
    sessionStorage.proofingFileVersionList = JSON.stringify(proofing_file_versions_list);

    //postCORS(serviceURLDomain + "api/Proof_approve/" + facilityId + "/" + appPrivileges.customerNumber + "/" + jobNumber, JSON.stringify(current_file_version[0]), function (response) {

    if (sessionStorage.isOpenedFromEmail == "true") job_customer_number = sessionStorage.customerNumber;
    postCORS(serviceURLDomain + "api/Proof_approve/" + facilityId + "/" + job_customer_number + "/" + jobNumber, JSON.stringify(current_file_version[0]), function (response) {
        if (response != undefined && response != null && response != "") {
            //            if (sessionStorage.jobSetupOutput != undefined) {
            //                gOutputData = jQuery.parseJSON(sessionStorage.jobSetupOutput);

            //                if (gOutputData.artworkAction.artworkFileList.length > 0) {
            //                    file_list = gOutputData.artworkAction.artworkFileList;
            //                }
            //                if (file_list.length > 0) {

            //                    $.each(file_list, function (key, val) {
            //                        if (val.fileName == fileName) {
            //                            val.overallStatus = (typeof (parseInt(response.replace(/"/g, "")) == "number") ? parseInt(response.replace(/"/g, "")) : val.overallStatus);
            //                            return false;
            //                        }
            //                    });
            //                }
            //                sessionStorage.jobSetupOutput = JSON.stringify(gOutputData);
            //            }

            var edit_job_service = serviceURLDomain + "api/JobTicket/" + facilityId + "/" + jobNumber + "/asdf"/* + sessionStorage.username*/;
            getCORS(edit_job_service, null, function (data) {
                gOutputData = data;
                if (gOutputData.artworkAction != undefined && gOutputData.artworkAction != null) {
                    if (Object.keys(gOutputData.artworkAction.artworkFileList).length > 0) {
                        artworkFileInfo[0] = (gOutputData.artworkAction.artworkFileList[fileName] != undefined && gOutputData.artworkAction.artworkFileList[fileName] != null && Object.keys(gOutputData.artworkAction.artworkFileList[fileName]).length > 0) ? gOutputData.artworkAction.artworkFileList[fileName] : {};
                        //                        artworkFileInfo = $.grep(gOutputData.artworkAction.artworkFileList, function (obj) {
                        //                            return obj.fileName === fileName;
                        //                        });
                    }
                    artworkFileInfo = (artworkFileInfo[0] != undefined && artworkFileInfo[0] != null) ? artworkFileInfo[0] : [];
                }
                sessionStorage.jobSetupOutput = JSON.stringify(gOutputData);
                sessionStorage.jobSetupOutputCompare = JSON.stringify(gOutputData); //this session is to use in approval checkout page to get 

                var msg = "Your proof approvals have been saved successfully.";
                //$('#dialogbox').prepend('<a id="btnClose" href="#" data-role="button" data-theme="a" data-icon="delete" data-iconpos="notext" class="ui-btn-right" onclick="continueToOrder();">Close</a>');
                //$('#dialogbox').prepend('<a id="btnClose" href="#" data-role="button" data-theme="a" data-icon="delete" data-iconpos="notext" class="ui-btn-right" onclick="$(\'#popupDialog\').popup(\'close\');">Close</a>');
                //$('#okBut').hide();
                $('#alertmsg').text(msg);
                $('#popupApproval').popup('close');
                $('#popupDialog').popup('open');
                $('#okBut').attr('onclick', '$("#popupDialog").popup("close");');
                //$('#popupDialog a').each(function (i) {
                //    $(this).button();
                //});

            }, function (error_response) {
                showErrorResponseText(error_response, true);
            });
        }

    }, function (response_error) {
        $('#popupApproval').popup('close');
        showErrorResponseText(response_error, false);
        //if (!(jQuery.browser.msie)) {
        //    $('#alertmsg').text((response_error.responseText != undefined) ? response_error.responseText : response_error.statusText);
        //}
        //else {
        //    $('#alertmsg').text((response_error.errorMessage != undefined) ? response_error.errorMessage : ((response_error.responseText != undefined) ? response_error.responseText : response_error.Message));
        //}

        //$('#popupDialog').popup('open');
    });
    $('#btnSubmitApprovals').attr('onclick', 'submitApprovals("approval",false)');
}

function closePopUps(popup_id) {
    if (popup_id == "popupApproval") {
        $('input[type=radio]').attr('checked', false).checkboxradio('refresh');
        $('#txtApproverName').val('');
    }
    else if (popup_id == "popupForward") {
        $('#txtEmailLink').val('');
    }
    $('#' + popup_id).popup('close');
}

function loadProofsToApprove(file_list) {
    if (Object.keys(file_list).length > 0) {
        var selected_proofs = "";
        var id = "";
        $.each(file_list, function (key, val) {
            var a = key;
            $.each(val.approverList, function (key1, val1) {
                if ((val1.email == approvalUserEmail && val1.approvalLevel == "1") || (sessionStorage.isOpenedFromEmail == "false" && approvalUserEmail == "")) {
                    id = val.fileName.replace(/ /g, '_').substring(0, val.fileName.lastIndexOf('.')) + '_approve';
                    selected_proofs += '<input type="checkbox" name="chk' + id + '" id="chk' + id + '" data-theme="c" value="' + val.fileName + '" onchange="makeProofSelection(this,\'dvProofsToApprove\')"/><label for="chk' + id + '">' + val.fileName + '</label>';
                }
            });

        });

        if (selected_proofs != "") {
            var proof_header = '<div data-role="collapsible-set" data-theme="a" data-content-theme="d" data-mini="true" style="padding-left: 8px; padding-right: 8px;">';
            proof_header += '<div data-role="collapsible" data-collapsed-icon="carat-r" data-expanded-icon="carat-d" data-theme="a" data-content-theme="d" data-collapsed="true">';
            proof_header += '<h3>Select Proofs to Apply Approvals</h3><fieldset data-role="controlgroup" data-mini="true" data-theme="a">';
            selected_proofs = proof_header + '<input type="checkbox" name="chkSelectAllProofs_approve" id="chkSelectAllProofs_approve" data-theme="a" onchange="selectAllProofs(this,\'dvProofsToApprove\')"/><label for="chkSelectAllProofs_approve"> Select All Proofs</label>' + selected_proofs;
            selected_proofs += '</fieldset></div></div>';
            $('#dvProofsToApprove').empty();
            $('#dvProofsToApprove').append(selected_proofs);
            $('#dvProofsToApprove').find('div[data-role=collapsible-set]').collapsibleset().trigger('create');
            $('#dvProofsToApprove').css('display', 'block');
            $('div.ui-collapsible-content', $('#dvProofsToApprove').find('div[data-role=collapsible-set]')).trigger('expand');
        }
    }
}

function openPopups(popup_id) {
    if (popup_id == "popupHistory") {
        $('#historyPanel').panel('open', 'optionsHash');
    }
    else if (popup_id == "popupApproval") {
        getCORS(gEditJobService + facilityId + "/" + jobNumber + "/asdf" /*+ userName*/, null, function (data) {
            var g_output_data = data;
            if (gOutputData.artworkAction != undefined && gOutputData.artworkAction != null) {
                gOutputData.artworkAction.artworkFileList = data.artworkAction.artworkFileList;
            }
            loadProofsToApprove(gOutputData.artworkAction.artworkFileList)

        });

        $('input[type=radio]').attr('checked', false).checkboxradio('refresh');
        $('#txtApproverName').val('');
        var current_file_version = [];
        current_file_version = getCurrentProofVersion();

        if (current_file_version.length > 0) {
            if (current_file_version[0].approvalType != undefined && current_file_version[0].approvalType != null) {
                if (current_file_version[0].approvalType == 2)
                    $('#spnApprovalType').text('Print');
                else
                    $('#spnApprovalType').text('Lettershop');
            }

            //if (current_file_version[0].approvalStatus != undefined && current_file_version[0].approvalStatus != null)
            //$('input:radio[value="' + current_file_version[0].approvalStatus + '"]').attr('checked', true).checkboxradio('refresh');

            //var approver_list = (artworkFileInfo[fileName] != undefined && artworkFileInfo[fileName] != null && artworkFileInfo[fileName].approverList != undefined && artworkFileInfo[fileName].approverList != null && artworkFileInfo[fileName].approverList.length > 0) ? artworkFileInfo[fileName].approverList : [];
            if (artworkFileInfo != undefined && artworkFileInfo != null && Object.keys(artworkFileInfo).length > 0 && artworkFileInfo.approverList != undefined && artworkFileInfo.approverList != null && artworkFileInfo.approverList.length > 0) {
                $.each(artworkFileInfo.approverList, function (key, val) {
                    if (val.email == approvalUserEmail) {
                        if (val.approvalStatus != undefined && val.approvalStatus != null)
                            $('input:radio[value="' + val.approvalStatus + '"]').attr('checked', true).checkboxradio('refresh');
                        return false;
                    }
                });
            }
            $('#txtApproverName').text(((approvalUserEmail.toLowerCase().indexOf('senecaglobal') > -1) ? "" : approvalUserEmail));
            //if (current_file_version[0].approvalLastUpdatedBy != undefined && current_file_version[0].approvalLastUpdatedBy != null)
            //$('#txtApproverName').val(current_file_version[0].approvalLastUpdatedBy);

        }
    }
    else if (popup_id == "popupForward") {
        loadSelectedProofs();
        $('#txtEmailLink').val('');
        $('#txtMessage').val('');
        $('#chkRequireApprovals').attr('checked', false).checkboxradio('refresh');
        $('#chkEnablePowerUser').hide();
        if (appPrivileges.roleName == "admin") {
            $('#chkEnablePowerUser').attr('checked', false).checkboxradio('refresh');
            $('#chkEnablePowerUser').show();
        }
        if (artworkFileInfo.proofForwardEmailLinks != undefined && artworkFileInfo.proofForwardEmailLinks != null && artworkFileInfo.proofForwardEmailLinks != "")
            $('#txtEmailLink').val(artworkFileInfo.proofForwardEmailLinks);
    }
    createPlaceHolderforIE();
    //$('#' + popup_id).popup("open", { positionTo: '#artApproal' });
    $('#' + popup_id).popup("open", { positionTo: '#' + $("a[id*='" + popup_id.substring(popup_id.indexOf('popup') + 5) + "']").attr('id') });

}

function getCurrentProofVersion() {
    var current_file_version = [];
    var proofing_file_versions_list = (sessionStorage.proofingFileVersionList != undefined && sessionStorage.proofingFileVersionList != null && sessionStorage.proofingFileVersionList != "") ? jQuery.parseJSON(sessionStorage.proofingFileVersionList) : [];
    var selected_file_version = (sessionStorage.currentProofUUID != undefined && sessionStorage.currentProofUUID != null && sessionStorage.currentProofUUID != "") ? sessionStorage.currentProofUUID : "";

    if (selected_file_version != undefined && selected_file_version != null && selected_file_version != "") {
        current_file_version = $.grep(proofing_file_versions_list, function (obj) {
            return obj.uuid === selected_file_version && obj.origFileName === fileName;
        });
    }
    return current_file_version;
}

function createPlaceHolderforIE() {
    if ($.browser.msie) {
        $('input[placeholder]').each(function () {
            var input = $(this);
            if ($(input).val() == "") $(input).val(input.attr('placeholder'));
            if ($(input).val() == input.attr('placeholder'))
                $(input).css('color', 'grey');
            $(input).focus(function () {
                if (input.val() == input.attr('placeholder')) {
                    input.val('').css('color', 'black');
                }
            });
            $(input).blur(function () {
                if (input.val() == '' || input.val() == input.attr('placeholder')) {
                    input.val(input.attr('placeholder')).css('color', 'grey');
                }
            });
        });
    };
}
function getURLParameter(name) {
    return decodeURI(
        (RegExp(name + '=' + '(.+?)(&|$)').exec(location.search) || [, null])[1]
    );
}

//HISTORY functionality -- START --
function openProofInNewWindow(proof_file_name, proof_create_date, proof_uuid, facility_id, job_number, proof_index, window_type) {
    var selected_version = [];
    selected_version.push({
        "facilityId": facility_id,
        "fileName": proof_file_name,
        "jobNumber": job_number,
        "proofCreatedDate": proof_create_date,
        "proofUUID": proof_uuid,
        "windowType": window_type,
        "index": proof_index
    });
    sessionStorage.selectedVersion = JSON.stringify(selected_version);

    var visited_proofs = [];
    visited_proofs = (sessionStorage.visitedProofs != undefined && sessionStorage.visitedProofs != null) ? $.parseJSON(sessionStorage.visitedProofs) : [];

    var visited_file = $.grep(visited_proofs, function (obj) {
        return obj.fileName == proof_file_name;
    });
    if (visited_file.length == 0)
        visited_proofs.push({
            "fileName": proof_file_name
        });
    sessionStorage.visitedProofs = JSON.stringify(visited_proofs);

    var query_string = "fileName=" + proof_file_name + "&jobNumber=" + jobNumber;
    var new_url_string = urlString.substr(0, urlString.indexOf("?") + 1);
    new_url_string = new_url_string + query_string;
    $('#popupHistory').popup('close');
    $('#lnk' + proof_uuid).css('color', '#0033CC');

    $('#historyPanel').panel('close', 'optionsHash');

    //if (sessionStorage.isOpenedFromEmail == "true")
    sessionStorage.approvalUserEmail = approvalUserEmail;
    //else
    //  sessionStorage.approvalUserEmail = approvalUserEmail
    sessionStorage.isNewWindow = true;
    window.open(new_url_string);
}
//HISTORY functionality -- END --

function fnCheckEmailFormat(email_id) {
    //    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    //    var msg = "";
    //    if (email_id == "")
    //        msg = 'Please provide an email address';
    //    else if (!filter.test(email_id))
    //        msg = 'Please provide a valid email address';

    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    var msg = "";
    if (email_id == "")
        msg = 'Please provide an email address';
    else {
        var email_ids = email_id;
        if (email_ids.indexOf(',') > -1) {
            email_ids = email_ids.split(',');
            $.each(email_ids, function (a, b) {
                if (!filter.test(b))
                    msg = 'Please provide a valid email address';
            });
        }
        else {
            if (!filter.test(email_id))
                msg = 'Please provide a valid email address';
        }
    }

    $('#okBut').attr('onclick', '$("#popupDialog").popup("close");');
    if (msg != "") {
        $('#popupForward').popup('close');
        $('#alertmsg').text(msg);
        $('#popupDialog').popup('open');
        $('#okBut').attr('onclick', '$("#popupDialog").popup("close");$("#popupForward").popup("open",{positionTo:"#btnProofForward"});');
        return false;
    }
    return true;
}

function sendProofEmail() {
    //---------- START ----------- 
    //The below code has to be used when sending optional message to service is enabled as post else need to use message as parameter to the service as email_links are being sent.
    if (!fnCheckEmailFormat($('#txtEmailLink').val()))
        return false;
    var forward_proof_json = {};
    forward_proof_json["uuid"] = (sessionStorage.currentProofUUID != undefined && sessionStorage.currentProofUUID != null && sessionStorage.currentProofUUID != "") ? sessionStorage.currentProofUUID : "";
    forward_proof_json["proofName"] = fileName; //encodedFileName;
    forward_proof_json["emailTos"] = $('#txtEmailLink').val();
    forward_proof_json["emailMessage"] = $('#txtMessage').val();
    forward_proof_json["requireApprovals"] = ($('#chkRequireApprovals').attr('checked') == "checked") ? true : false;

    var selected_proofs_list = $('#dvSelectedProofs input[type=checkbox]:not([id=chkSelectAllProofs])');
    var selected_proofs = [];
    $.each(selected_proofs_list, function (a, b) {
        selected_proofs.push({
            "fileName": $(b).val(),
            "requiredApproval": ($(b)[0].checked) ? 1 : 0
        });
    });
    forward_proof_json["selectedProofs"] = selected_proofs;

    forward_proof_json["isPowerUser"] = ($('#chkEnablePowerUser')[0].checked) ? 1 : 0;

    //---------- END ----------- 
    //var email_links = $('#txtEmailLink').val();
    //getCORS(serviceURLDomain + "api/Proof/" + sessionStorage.customerNumber + "/" + facilityId + "/" + jobNumber + "/" + encodedFileName + "/" + encodeURI(email_links), null, function (data) {
    var job_customer_number = (sessionStorage.jobCustomerNumber != undefined && sessionStorage.jobCustomerNumber != null && sessionStorage.jobCustomerNumber != "") ? sessionStorage.jobCustomerNumber : appPrivileges.customerNumber;
    postCORS(serviceURLDomain + "api/Proof_post/" + job_customer_number + "/" + facilityId + "/" + jobNumber, JSON.stringify(forward_proof_json), function (response) {
        if (response == "") {
            var edit_job_service = serviceURLDomain + "api/JobTicket/" + facilityId + "/" + jobNumber + "/asdf"/* + sessionStorage.username*/;
            getCORS(edit_job_service, null, function (data) {
                gOutputData = data;
                if (gOutputData.artworkAction != undefined && gOutputData.artworkAction != null) {
                    if (Object.keys(gOutputData.artworkAction.artworkFileList).length > 0) {
                        artworkFileInfo[0] = (gOutputData.artworkAction.artworkFileList[fileName] != undefined && gOutputData.artworkAction.artworkFileList[fileName] != null && Object.keys(gOutputData.artworkAction.artworkFileList[fileName]).length > 0) ? gOutputData.artworkAction.artworkFileList[fileName] : {};
                        //                        artworkFileInfo = $.grep(gOutputData.artworkAction.artworkFileList, function (obj) {
                        //                            return obj.fileName === fileName;
                        //                        });
                    }
                    artworkFileInfo = (artworkFileInfo[0] != undefined && artworkFileInfo[0] != null) ? artworkFileInfo[0] : [];
                }
                sessionStorage.jobSetupOutput = JSON.stringify(gOutputData);
                sessionStorage.jobSetupOutputCompare = JSON.stringify(gOutputData); //this session is to use in approval checkout page to get 

                $('#popupForward').popup('close');
                $('#alertmsg').text("Proof link has been forwarded successfully.");
                $('#popupDialog').popup('open');
                //                $('#popupDialog a').each(function (i) {
                //                    $(this).button();
                //                });

            }, function (error_response) {
                showErrorResponseText(error_response, true);
            });

        }
    }, function (error_response) {
        $('#popupForward').popup('close');
        showErrorResponseText(error_response, false);
        //if (!(jQuery.browser.msie)) {
        //    $('#alertmsg').text((error_response.responseText != undefined) ? error_response.responseText : error_response.statusText);
        //}
        //else {
        //    $('#alertmsg').text((error_response.errorMessage != undefined) ? error_response.errorMessage : ((error_response.responseText != undefined) ? error_response.responseText : error_response.Message));
        //}
        ////$('#alertmsg').text("An error occurred while sending an email. Please contact administrator");

        //$('#popupDialog').popup('open');
    });
}

//Funtions to authorise the user and get the job info.  --- START ---
function makeBasicAuth(user, password) {
    var tok = user + ':' + password;
    var hash = Base64.encode(tok);
    return "Basic " + hash;
}

function loginAuthorization(response) {
    if (response != undefined && response != null && response != "") {
        userName = response.userName;
        jobNumber = response.jobNumber;
        facilityId = response.facilityId;
        sessionStorage.authString = makeBasicAuth(userName, response.password);
        if (response.origProofName.indexOf('TCA') > -1 || response.origProofName.indexOf('AAG') > -1) {
            userName = "user_aag";
            jobNumber = "-17032";
            facilityId = "2";
            sessionStorage.authString = makeBasicAuth(userName, "welcome123$");
        }
        getCORS(gServiceUrl, null, function (data) {
            if (data.Message != undefined) {
                $('#alertmsg').text(data.Message);
                $('#popupDialog').popup('open');
                return false;
            }
            else {
                captureAndNavigate(data, response);
            }
        }, function (response_error) {
            $('#viewFrame')[0].contentWindow.document.body.innerHTML = "";
            showErrorResponseText(response_error, false);
            //if (!(jQuery.browser.msie)) {
            //    $('#alertmsg').text((response_error.responseText != undefined) ? response_error.responseText : response_error.statusText);
            //}
            //else {
            //    $('#alertmsg').text((response_error.errorMessage != undefined) ? response_error.errorMessage : ((response_error.responseText != undefined) ? response_error.responseText : response_error.Message));
            //}
            //$('#popupDialog').popup('open');
        });
    }
}

function getEditJobData() {
    getCORS(gEditJobService + facilityId + "/" + jobNumber + "/" + sessionStorage.username, null, function (data) {
        var g_output_data = data;
        sessionStorage.jobSetupOutput = JSON.stringify(g_output_data);
        sessionStorage.jobSetupOutputCompare = JSON.stringify(g_output_data);
        gOutputData = (sessionStorage.jobSetupOutput != undefined && sessionStorage.jobSetupOutput != null) ? jQuery.parseJSON(sessionStorage.jobSetupOutput) : [];
        sessionStorage.jobDesc = g_output_data.jobName;

        if (sessionStorage.jobSetupOutput != undefined) {
            gOutputData = jQuery.parseJSON(sessionStorage.jobSetupOutput);
            if (gOutputData.artworkAction != undefined && gOutputData.artworkAction != null) {
                if (Object.keys(gOutputData.artworkAction.artworkFileList).length > 0) {
                    artworkFileInfo[0] = (gOutputData.artworkAction.artworkFileList[fileName] != undefined && gOutputData.artworkAction.artworkFileList[fileName] != null && Object.keys(gOutputData.artworkAction.artworkFileList[fileName]).length > 0) ? gOutputData.artworkAction.artworkFileList[fileName] : {};
                    //                    artworkFileInfo = $.grep(gOutputData.artworkAction.artworkFileList, function (obj) {
                    //                        return obj.fileName === fileName;
                    //                    });
                }
                artworkFileInfo = (artworkFileInfo[0] != undefined && artworkFileInfo[0] != null) ? artworkFileInfo[0] : [];
                $('#btnJobProofs').show();
                if (Object.keys(gOutputData.artworkAction.artworkFileList).length == 1) {
                    $('#btnJobProofs').hide();
                    var available_buttons = $('#dvProofButtons').find('a[data-role=button]');
                    $.each(available_buttons, function (key, val) {
                        if (key == 0) {
                            $(available_buttons[0]).addClass('ui-corner-lt');
                            $(available_buttons[0]).addClass('ui-corner-lb');
                        }
                        else if (key == (available_buttons.length - 1)) {
                            $(available_buttons[0]).addClass('ui-corner-rt');
                            $(available_buttons[0]).addClass('ui-corner-rb');
                        }
                    });
                    //$('#artApproal').addClass('.ui-corner-rt');
                }
            }
            getMilestoneInfo();
        }

        displayJobInfo();
    }, function (error_response) {
        $('#viewFrame')[0].contentWindow.document.body.innerHTML = "";
        //if (!(jQuery.browser.msie)) {
        //    $('#alertmsg').text((error_response.responseText != undefined) ? error_response.responseText : error_response.statusText);
        //}
        //else {
        //    $('#alertmsg').text((error_response.errorMessage != undefined) ? error_response.errorMessage : ((error_response.responseText != undefined) ? error_response.responseText : error_response.Message));
        //}
        //$('#popupDialog').popup('open');
        showErrorResponseText(error_response, false);
    });
}

function captureAndNavigate(data, response) {
    var customer_number = data["customerNumber"];
    sessionStorage.username = userName;
    sessionStorage.userRole = (data["roleName"].indexOf('_') > -1) ? data["roleName"].split('_')[0] : data["roleName"];
    sessionStorage.companyName = data["companyName"].toLowerCase();
    sessionStorage.customerNumber = (sessionStorage.isOpenedFromEmail == "true") ? response.customerNumber : data["customerNumber"];
    sessionStorage.jobCustomerNumber = response.customerNumber;
    facilityId = data.facility_id;
    sessionStorage.facilityId =facilityId;
    sessionStorage.jobNumber = jobNumber;
    if (response.isPowerUser == 1) {
        var link = data;
        $.each(data, function (key, val) {
            if (key.toLowerCase().indexOf('_page_') > -1 && key.toLowerCase().indexOf('artwork') == -1)
                delete data[key];
        });
    }
    sessionStorage.nextPageInOrder = "../jobUploadArtwork/jobUploadArtwork.html";
    saveSessionData("appPrivileges", JSON.stringify(data));
    appPrivileges = jQuery.parseJSON(getSessionData("appPrivileges"));
    sessionStorage.nextPageInOrder = '../jobUploadArtwork/jobUploadArtwork.html';
    if (appPrivileges.roleName != "admin" || response.isPowerUser == 0)
        $('#btnContinue').css('display', 'none');

    window.setTimeout(function setDelay() {
        loadProofsInfo(response, '');
    }, 2000);
   // setDemosProof();
    getEditJobData();
}

function getMilestoneInfo() {
    var job_customer_number = (sessionStorage.jobCustomerNumber != undefined && sessionStorage.jobCustomerNumber != null && sessionStorage.jobCustomerNumber != "") ? sessionStorage.jobCustomerNumber : appPrivileges.customerNumber;
    if (sessionStorage.isOpenedFromEmail == "true") job_customer_number = sessionStorage.customerNumber;
    if (gOutputData["milestoneAction"] == null && gOutputData["milestoneAction"] == undefined) gOutputData["milestoneAction"] = {};
    if (gOutputData.milestoneAction.milestoneDict == undefined) gOutputData.milestoneAction["milestoneDict"] = {};
    if (gOutputData.milestoneAction.milestoneDict != undefined && JSON.stringify(gOutputData.milestoneAction.milestoneDict).length == 2) {
        var inhome_date_param = (sessionStorage.jobSetupOutput != undefined && sessionStorage.jobSetupOutput != null) ? jQuery.parseJSON(sessionStorage.jobSetupOutput).inHomeDate.replace(/\//g, "_") : "";
        get_milestone_url = serviceURLDomain + "api/Milestone/" + facilityId + "/" + sessionStorage.jobNumber + "/" + job_customer_number + "/null/" + inhome_date_param;

        getCORS(get_milestone_url, null, function (data) {
            gOutputData.milestoneAction["milestoneDict"] = [];
            gOutputData.milestoneAction["milestoneDict"] = (data.milestoneDict != undefined) ? data.milestoneDict : [];
        });
    }
}

//Funtion to authorise the user and get the job info.  --- END ---

//Functions to load and manage job proofs -- START ---
function loadJobProofsList() {
    var visited_proofs = [];
    visited_proofs = (sessionStorage.visitedProofs != undefined && sessionStorage.visitedProofs != null) ? $.parseJSON(sessionStorage.visitedProofs) : [];

    var file_list = {};
    if (gOutputData.artworkAction != undefined && gOutputData.artworkAction != null) {
        if (Object.keys(gOutputData.artworkAction.artworkFileList).length > 0) {
            file_list = gOutputData.artworkAction.artworkFileList;
        }
    }
    else {
        if (sessionStorage.jobSetupOutput != undefined) {
            gOutputData = jQuery.parseJSON(sessionStorage.jobSetupOutput);
            if (gOutputData.artworkAction != undefined && gOutputData.artworkAction != null) {
                if (Object.keys(gOutputData.artworkAction.artworkFileList).length > 0) {
                    file_list = gOutputData.artworkAction.artworkFileList;
                }
            }
        }
    }
    if (Object.keys(file_list).length > 0) {
        var un_approved_list = [];
        var approved_list = [];
        $.each(file_list, function (key, val) {
            var proof_items = [];
            if (val.fileStatus != "archive") {
                created_date = ((val.versionDate != undefined && val.versionDate != null && val.versionDate != "") ? val.versionDate : "");
                if (val.overallStatus == "1") {
                    var new_item = {
                        "fileName": val.fileName,
                        "id": "lnk" + (val.fileName.substring(0, val.fileName.indexOf('.'))).replace(/ /g, '#'),
                        "createdDate": created_date,
                        "versionCode": val.versionCode
                    };

                    if (approved_list != undefined && approved_list != null) {
                        $.each(approved_list, function (a, b) {
                            proof_items = $.grep(b, function (obj) {
                                return obj.versionCode.toLowerCase() == val.versionCode.toLowerCase();
                            });
                            if (proof_items.length > 0) {
                                proof_items.push(new_item);
                                approved_list[a] = proof_items;
                                return false;
                            }

                        });
                    }
                    if (proof_items.length == 0)
                        proof_items.push(new_item);
                    if (Object.keys(proof_items).length == 1) {
                        approved_list.push(proof_items);
                    }
                }
                else {
                    var new_item = {
                        "fileName": val.fileName,
                        "id": "lnk" + (val.fileName.substring(0, val.fileName.indexOf('.'))).replace(/ /g, '#'),
                        "createdDate": created_date,
                        "versionCode": val.versionCode
                    };

                    if (un_approved_list != undefined && un_approved_list != null) {
                        $.each(un_approved_list, function (a, b) {
                            proof_items = $.grep(b, function (obj) {
                                return obj.versionCode.toLowerCase() == val.versionCode.toLowerCase();
                            });
                            if (proof_items.length > 0) {
                                proof_items.push(new_item);
                                un_approved_list[a] = proof_items;
                                return false;
                            }

                        });
                    }
                    if (proof_items.length == 0)
                        proof_items.push(new_item);
                    if (Object.keys(proof_items).length == 1) {
                        un_approved_list.push(proof_items);
                    }
                }
            }
        });
        $('#ulJobProofs').empty();
        var download_proofs_links = "";
        download_proofs_links += '<li data-theme="a" data-icon="delete"><a href="#" data-rel="close" style="font-size:14px" onclick="$(\'#jobProofsPanel\').panel(\'close\');">Order Elements</a></li>';
        download_proofs_links += '<li data-theme="c" data-icon="myapp-download"><a href="" style="font-size:12px;" onclick="openDownloadPopup(\'0\');">Download All Files</a></li>';
        download_proofs_links += '<li data-theme="c" data-icon="myapp-download-comments"><a href="" style="font-size:12px;"  onclick="openDownloadPopup(\'1\');">Download All Annotated PDFs</a></li>';
        $('#ulJobProofs').append(download_proofs_links);
        $('#ulJobProofs').append($("#unApprovedJobsTemplate").tmpl({ proofs: un_approved_list }));
        $('#ulJobProofs').append($("#approvedJobsTemplate").tmpl({ proofs: approved_list }));
        $.each(visited_proofs, function (key, val) {
            $('#ulJobProofs li a[id=lnk' + val.fileName.substring(0, val.fileName.indexOf('.')).replace(/ /g, '#') + ']').css('color', '#0033CC');
        });
        $('#ulJobProofs').listview('refresh');
    }
}

function openDownloadPopup(type) {
    $('#txtDownloadProofsEmailTo').val('');
    var user_email_id = (sessionStorage.approvalUserEmail != undefined && sessionStorage.approvalUserEmail != null && sessionStorage.approvalUserEmail != "") ? sessionStorage.approvalUserEmail : "";
    $('#txtDownloadProofsEmailTo').val(user_email_id);
    $('#btnDownload').attr('onclick', 'downloadProofs(' + type + ')');
    $('#popupDownloadProofs').popup('open');
}

function emailValidation(email_id) {
    var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    var msg = "";
    if (email_id == "")
        msg = 'Email address is mandatory to download Proofs.';
    else {
        //        var email_ids = email_id;
        //        if (email_ids.indexOf(',') > -1) {
        //            email_ids = email_ids.split(',');
        //            $.each(email_ids, function (a, b) {
        //                if (!expr.test($.trim(b)))
        //                    msg = 'Invalid email address.';
        //            });
        //        }
        //        else {
        //            if (!expr.test(email_id))
        //                msg = 'Invalid email address.';
        //        }
        if (!expr.test($.trim(email_id)))
            msg = 'Invalid email address.';
    }
    $('#okBut').attr('onclick', '$("#popupDialog").popup("close");');
    if (msg != "") {
        $('#popupDownloadProofs').popup('close');
        $('#alertmsg').text(msg);
        $('#popupDialog').popup('open');
        $('#okBut').attr('onclick', '$("#popupDialog").popup("close");$("#popupDownloadProofs").popup("open");');
        return false;
    }
    return true;
}

function downloadProofs(type) {
    if (!(emailValidation($("#txtDownloadProofsEmailTo").val()))) {
        return false;
    }
    if ($('#txtDownloadProofsEmailTo').val() != "") {
        $('#popupDownloadProofs').popup('close');
        $('#loadingText1').text('Please Wait..');
        $('#waitPopUp').popup('open');
        window.setTimeout(function downloadProofsByMail() {
            var job_customer_number = (sessionStorage.jobCustomerNumber != undefined && sessionStorage.jobCustomerNumber != null && sessionStorage.jobCustomerNumber != "") ? sessionStorage.jobCustomerNumber : appPrivileges.customerNumber;
            var url = serviceURLDomain + "api/Proof_download/" + job_customer_number + "/" + facilityId + "/" + jobNumber + "/" + $('#txtDownloadProofsEmailTo').val() + "/" + type;
            getCORS(url, null, function (data) {
                if (data == "success") {
                    $('#waitPopUp').popup('close');
                    $('#alertmsg').html("Proofs have been downloaded successfully");
                    $('#popupDownloadProofs').popup('close');
                    $('#okBut').attr('onclick', '$(\'#popupDialog\').popup(\'close\');');
                    $('#popupDialog').popup('open');

                }
            }, function (error_response) {
                $('#viewFrame')[0].contentWindow.document.body.innerHTML = "";
                //            if (!(jQuery.browser.msie)) {
                //                $('#alertmsg').text((error_response.responseText != undefined) ? error_response.responseText : error_response.statusText);
                //            }
                //            else {
                //                $('#alertmsg').text((error_response.errorMessage != undefined) ? error_response.errorMessage : ((error_response.responseText != undefined) ? error_response.responseText : error_response.Message));
                //            }
                $('#alertmsg').text('Failed to download proofs. Please contact Administrator.');
                $('#popupDownloadProofs').popup('close');
                $('#okBut').attr('onclick', '$(\'#popupDialog\').popup(\'close\');');
                $('#popupDialog').popup('open');
            });
        }, 1000);
    }
}

function viewJobProof(file_name, window_type) {
    var visited_proofs = [];
    visited_proofs = (sessionStorage.visitedProofs != undefined && sessionStorage.visitedProofs != null) ? $.parseJSON(sessionStorage.visitedProofs) : [];
    var query_string = "fileName=" + file_name + "&jobNumber=" + jobNumber + "&isJobProof=true";
    var new_url_string = urlString.substr(0, urlString.indexOf("?") + 1);
    new_url_string = new_url_string + query_string;

    var visited_file = $.grep(visited_proofs, function (obj) {
        return obj.fileName == file_name;
    });
    if (visited_file.length == 0)
        visited_proofs.push({
            "fileName": file_name
        });
    sessionStorage.visitedProofs = JSON.stringify(visited_proofs);

    if (sessionStorage.isOpenedFromEmail == "true")
        sessionStorage.approvalUserEmail = approvalUserEmail;

    if (window_type == "blank") {
        var selected_version = [];
        selected_version.push({
            "facilityId": facilityId,
            "fileName": file_name,
            "jobNumber": jobNumber,
            "proofCreatedDate": "",
            "proofUUID": "",
            "windowType": window_type

        });
        sessionStorage.selectedVersion = JSON.stringify(selected_version);
        $('#jobProofsPanel').panel('close', 'optionsHash');
        sessionStorage.isNewWindow = true;
        window.open(new_url_string);
    }
    else {
        sessionStorage.isNewWindow = false;
        window.location.href = new_url_string;
    }
}


function getInternetExplorerVersion() {
    var rv = -1; // Return value assumes failure.
    if (navigator.appVersion.indexOf('Trident') > -1 || navigator.appName == 'Microsoft Internet Explorer') {
        var ua = navigator.userAgent;
        var re = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
        var re1 = new RegExp("rv:([0-9]{1,}[\.0-9]{0,})");
        if (re.exec(ua) != null || re1.exec(ua) != null)
            rv = parseFloat(RegExp.$1);
    }
    return rv;
}

function checkVersion() {
    var ver = getInternetExplorerVersion();
    return (ver > -1) ? ((ver >= 9.0) ? true : false) : true;
}

function createConfirmDialog() {
    var msg_box = '';
    msg_box = '<div data-role="popup" id="popupConfirmDialog" data-overlay-theme="d" data-theme="c" style="max-width:400px;" class="ui-corner-all" data-history="false">';
    msg_box += '<div data-role="header" data-theme="a" class="ui-corner-top" id="dialogbox">';
    msg_box += '<h1>Confirm</h1>';
    msg_box += '</div>';
    msg_box += '<div data-role="content" data-theme="d" class="ui-corner-bottom ui-content">';
    msg_box += '<div id="confirmMsg"></div>';
    msg_box += '<div id="colId"></div>';
    msg_box += '<div id="valList"></div>';
    msg_box += '<a href="#" data-role="button" data-inline="true" data-theme="b" id="cancelButConfirm" data-mini="true">Cancel</a>';
    msg_box += '<a href="#" data-role="button" data-inline="true" data-theme="b" id="okButConfirm" data-mini="true">Continue</a>';
    msg_box += '</div></div>';
    $("#_jobProofing").append(msg_box);
}
//Functions to load and manage job proofs -- END ---

//funtion to load selected proofs for required approvals
function loadSelectedProofs() {
    var file_list = {};
    if (gOutputData.artworkAction != undefined && gOutputData.artworkAction != null) {
        if (Object.keys(gOutputData.artworkAction.artworkFileList).length > 0) {
            file_list = gOutputData.artworkAction.artworkFileList;
        }
    }
    else {
        if (sessionStorage.jobSetupOutput != undefined) {
            gOutputData = jQuery.parseJSON(sessionStorage.jobSetupOutput);
            if (gOutputData.artworkAction != undefined && gOutputData.artworkAction != null) {
                if (Object.keys(gOutputData.artworkAction.artworkFileList).length > 0) {
                    file_list = gOutputData.artworkAction.artworkFileList;
                }
            }
        }
    }
    if (Object.keys(file_list).length > 0) {
        var selected_proofs = "";
        var id = "";
        $.each(file_list, function (key, val) {
            id = val.fileName.replace(/ /g, '_').substring(0, val.fileName.lastIndexOf('.'));
            selected_proofs += '<input type="checkbox" name="chk' + id + '" id="chk' + id + '" data-theme="c" value="' + val.fileName + '" onchange="makeProofSelection(this,\'dvSelectedProofs\')"/><label for="chk' + id + '">' + val.fileName + '</label>';
        });

        if (selected_proofs != "") {
            var proof_header = '<div data-role="collapsible-set" data-theme="a" data-content-theme="d" data-mini="true" style="padding-left: 8px; padding-right: 8px;">';
            proof_header += '<div data-role="collapsible" data-collapsed-icon="carat-r" data-expanded-icon="carat-d" data-theme="a" data-content-theme="d" data-collapsed="true">';
            proof_header += '<h3>Selected Proofs</h3><fieldset data-role="controlgroup" data-mini="true">';
            selected_proofs = proof_header + '<input type="checkbox" name="chkSelectAllProofs" id="chkSelectAllProofs" data-theme="a" onchange="selectAllProofs(this,\'dvSelectedProofs\')"/><label for="chkSelectAllProofs"> Select All Proofs</label>' + selected_proofs;
            selected_proofs += '</fieldset></div></div>';
            $('#dvSelectedProofs').empty();
            $('#dvSelectedProofs').append(selected_proofs);
            $('#dvSelectedProofs').find('div[data-role=collapsible-set]').collapsibleset().trigger('create');
            $('#dvSelectedProofs').css('display', 'none');
        }
    }
}

function makeProofSelection(ctrl, dv_type) {
    var checked_proofs = $('#' + dv_type + ' input[type=checkbox]:checked:not([id=chkSelectAllProofs])');
    if (!ctrl.checked) {
        $('#chkSelectAllProofs').attr('checked', false).checkboxradio('refresh');
        if (checked_proofs.length == 0) $('#chkRequireApprovals').attr('checked', false).checkboxradio('refresh');
    }
    else {
        var all_proofs = $('#' + dv_type + ' input[type=checkbox]:not([id=chkSelectAllProofs])');
        if (all_proofs.length == checked_proofs.length) $('#chkSelectAllProofs').attr('checked', true).checkboxradio('refresh');
        if (dv_type.indexOf('Approve') == -1)
            $('#chkRequireApprovals').attr('checked', true).checkboxradio('refresh');
    }
}

function selectAllProofs(ctrl, dv_type) {
    if (ctrl.checked) {
        $('#' + dv_type + ' input[type=checkbox]:not([id=chkSelectAllProofs])').attr('checked', true).checkboxradio('refresh');
        if (dv_type.indexOf('Approve') == -1)
            $('#chkRequireApprovals').attr('checked', true).checkboxradio('refresh');
    }
    else {
        $('#' + dv_type + ' input[type=checkbox]:not([id=chkSelectAllProofs])').attr('checked', false).checkboxradio('refresh');
        if (dv_type.indexOf('Approve') == -1)
            $('#chkRequireApprovals').attr('checked', false).checkboxradio('refresh');
    }
}

function selectRequiredProofs(ctrl) {
    if (ctrl.checked) {
        $('#dvSelectedProofs').css('display', 'block');
        $('div.ui-collapsible-content', $('#dvSelectedProofs').find('div[data-role=collapsible-set]')).trigger('expand');
        $('#chkSelectAllProofs').attr('checked', true).checkboxradio('refresh').trigger('change');
    }
    else {
        $('#dvSelectedProofs').css('display', 'none');
        $('div.ui-collapsible-content', $('#dvSelectedProofs').find('div[data-role=collapsible-set]')).trigger('collapse');
        $('#chkSelectAllProofs').attr('checked', false).checkboxradio('refresh').trigger('change');
    }

}

function previewMailing() {
    if (appPrivileges.customerNumber == DCA || appPrivileges.customerNumber == KUBOTA || appPrivileges.customerNumber == SPORTSKING) {//Todo: Need to update pdf for Kubota customer
        encodedFileName = "DCA%2041941%20Jupiter%20Dental%20_Postcard_Dr.%20Vera_MECH.pdf";
    }
    if (appPrivileges.customerNumber == AAG) {
        encodedFileName = "TCA_10232_AAG_Lunch_Learn_2_Presenters-2_Events_Lenfers_Culbreth_Let.pdf";
    }
    //getNextPageInOrder();
    //var preview_proof = "../jobProofing/jobProofing.html?fileName=58683_GWBerkheimerOLSONCOMFORT.pdf&jobNumber=" + sessionStorage.jobNumber;
    var preview_proof = "../jobProofing/previewMailing.html?fileName=" + encodedFileName + "&jobNumber=" + sessionStorage.jobNumber;
    window.location.href = preview_proof;
}

function setDemosProof() {
    if (appPrivileges != undefined && appPrivileges != null) {
        if (appPrivileges.customerNumber == ALLIED || appPrivileges.customerNumber == DCA || appPrivileges.customerNumber == KUBOTA || appPrivileges.customerNumber == AAG || appPrivileges.customerNumber == SPORTSKING) {
            sessionStorage.authString = "Basic YWRtaW5fY3cxOmFkbWluX2N3MQ==";
            //encodedFileName = "58683_GWBerkheimerOLSONCOMFORT.pdf";
            //proofJobNumber = "-16288";
            facilityId = "1";
            jobCustomerNumber = CW;
            sessionStorage.jobCustomerNumber = CW;
        }

        if (appPrivileges.customerNumber == ALLIED) {
            encodedFileName = "58683_GWBerkheimerOLSONCOMFORT.pdf";
            proofJobNumber = "-16288";
        }
        else if (appPrivileges.customerNumber == DCA || appPrivileges.customerNumber == KUBOTA || appPrivileges.customerNumber == SPORTSKING) { //Todo: Need to update pdf for Kubota customer
            encodedFileName = "DCA%2041941%20Jupiter%20Dental%20_Postcard_Dr.%20Vera_MECH.pdf";
            if (sessionStorage.jobSetupOutput != undefined && sessionStorage.jobSetupOutput != null && sessionStorage.jobSetupOutput != "") {
                gOutputData = $.parseJSON(sessionStorage.jobSetupOutput);
                if (gOutputData.templateName.indexOf('_11x6_') > -1) {
                    encodedFileName = "DCA_42754_DoubleDentRite_DM_Jupiter.pdf";
                }
            }
            proofJobNumber = "-15565";
        }
        else if (appPrivileges.customerNumber == AAG) {
            encodedFileName = "TCA_10232_AAG_Lunch_Learn_2_Presenters-2_Events_Lenfers_Culbreth_Let.pdf";
            proofJobNumber = "-15565";
        }
    }
}

String.prototype.initCap = function () {
    return this.toLowerCase().replace(/(?:^|\s)[a-z]/g, function (m) {
        return m.toUpperCase();
    });
};
