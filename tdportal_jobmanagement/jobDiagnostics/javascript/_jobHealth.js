﻿
var gLogListUrl = serviceURLDomain + 'api/Logging/' + sessionStorage.facilityId + '/' + jobCustomerNumber + '/' + sessionStorage.jobNumber;
var gLogDetailsUrl = 'JSON/_logDetails.JSON';
var gFacilityServiceUrl = "../JSON/_facility.JSON";
var primaryFacilityData;

$.getJSON(gFacilityServiceUrl, function (result) {
    primaryFacilityData = result.facility;
});

$('#_jobHealth').live('pagebeforecreate', function (event) {
    displayMessage('_jobHealth');
    ko.setTemplateEngine(new ko.nativeTemplateEngine);
    getCORS(serviceURLDomain + "api/Customer/", null, function (data) {
        companyData = data;
        $('#companyTemplate').tmpl(companyData).appendTo('#ddlCustomer');
    }, function (error_response) {
        showErrorResponseText(error_response, true);
    });
});

$(document).on('pageshow', '#_jobHealth', function (event) {
    try {
        ko.applyBindings(new jobHealthViewModel());
        window.setTimeout(function () {
            $('#facilityTemplate').tmpl(primaryFacilityData).appendTo('#ddlFacility');
            $('#ddlFacility').val(sessionStorage.facilityId).selectmenu('refresh');
            $('#ddlCustomer').val(sessionStorage.customerNumber).selectmenu('refresh');
            if (appPrivileges.customerNumber != "1") {
               // $('#ddlCustomer').attr('disabled', 'disabled');
                $('#ddlFacility').attr('disabled', 'disabled');
            }
            $('div[data-role=collapsible] ul').listview();
            $('div[data-role=collapsible]').collapsible();
            $('div[data-role=collapsible-set]').collapsibleset();
            $('#dvJobHealthItems div[data-role=collapsible]').eq(0).collapsible("disable");
        }, 300);
    } catch (e) {
        alert(e);
    }
});

var jobHealthItemModel = function (item_name, item_value) {
    var self = this;
    self.itemName = ko.observable(item_name);
    self.itemValue = ko.observable({});
    ko.mapping.fromJS(item_value, {}, self.itemValue);
};

var jobHealthViewModel = function () {
    var self = this;
    self.selectedJobNumber = ko.observable("");
    self.selectedJobNumber("-10092014");
    self.selectedJobName = ko.observable("");
    self.selectedJobName("Sample Job");
    self.jobInfo = ko.computed(function () {
        return self.selectedJobName() + ' | ' + self.selectedJobNumber();
    });
    self.jobHealthInfo = ko.observableArray([]);
    self.loadJobHealthData = function () {
        var job_health_data = {};
        $.getJSON("JSON/_jobHealth.JSON", function (data) {
            $.each(data, function (key, val) {
                $.each(val, function (key1, val1) {
                    self.jobHealthInfo.push(new jobHealthItemModel(key1, val1));
                });
            });
        });
    };

    self.validateData = function () {
        var msg = "";
        // var regxCustFacility = /^[0-9]+$/;
        var regxJobNumber = /^[0-9 \-]+$/;  // /\-\d*$/;
        if ($("#txtJobNumber").val() == "" && $("#txtJobName").val() == "") {
            msg += (msg != undefined && msg != "" && msg != null) ? "<br />Please enter Job Number or Job Name" : "Please enter Job Number or Job Name";
        }
        else if ($("#txtJobNumber").val() != "" && !regxJobNumber.test($("#txtJobNumber").val())) {
            msg += (msg != undefined && msg != "" && msg != null) ? "<br />Please enter valid Job Number" : "Please enter valid Job Number";
        }
        if (appPrivileges.customerNumber == "1") {
            if ($("#ddlCompany").val() == "") {
                msg += (msg != undefined && msg != "" && msg != null) ? "<br />Please select Company Name." : "Please select Company Name.";
            }
            if ($("#ddlFacility").val() == "") {
                msg += (msg != undefined && msg != "" && msg != null) ? "<br />Please select Facility." : "Please select Facility."
            }
        }
        if (msg != "") {
            $('#alertmsg').html(msg);
            $('#alertmsg').css('text-align', 'left');
            $("#popupDialog").popup('open');
            $("#okBut").attr("onclick", "$('#popupDialog').popup('close')");
            return false;
        }
        return true;
    };

    self.displayJobHealthItems = function () {
        //if (!self.validateData()) return false;
        //if (sessionStorage.selectedJobInfo != undefined && sessionStorage.selectedJobInfo != null && sessionStorage.selectedJobInfo != "") {
        //    //$('#dvJobHealthItems h4').eq(0).text(self.selectedJobName() + ' | ' + self.selectedJobNumber());
        //}
        $('#dvJobHealthItems div').not(':first').remove();
        self.loadJobHealthData();
        window.setTimeout(function () {
            //$('#dvJobHealthItems').trigger('create');            
            $('#dvJobHealthItems div').trigger('create').collapsible();
            $('#dvJobHealthItems').collapsibleset();
            $('#dvJobHealthItems').css('display', 'block');
            $('#dvJobLogging').css('display', 'block');
        }, 200);


    };

    if (sessionStorage.selectedJobInfo != undefined && sessionStorage.selectedJobInfo != null && sessionStorage.selectedJobInfo != "") {
        var temp_json = $.parseJSON(sessionStorage.selectedJobInfo);
        self.selectedJobName(temp_json.jobName);
        self.selectedJobNumber(temp_json.jobNumber);
        self.displayJobHealthItems();
    }
    
    //Job Logging START

    self.gLogginData = [];
    self.gLogDetailsData = [];
    self.gOutputData = [];
    self.logging = ko.observableArray(self.gLogginData);
    self.selectedItems = ko.observable("1");
    self.listJobDetails = ko.observableArray([]);

    //This function will display details info for selected logging
    self.setJobDetailsInfo = function (event_id, current) {
        self.listJobDetails([]);
        getCORS(gLogListUrl + '/' + event_id + '/0/' + current, null, function (data) {
            $.each(data, function (a, b) {
                for (i = 0; i < b.length; i++) {
                    self.listJobDetails.push(b[i]);
                }
            });
            $('#jobListInfo').css('display', 'block');
            $("div ul").each(function (I) {
                $(this).listview();
            });
            $("#jobListInfo").collapsibleset('refresh');

            $('#jobListInfo').trigger('create');
            $('.ui-page').trigger('create').listview().listview("refresh");

        }, function (error_response) {
            showErrorResponseText(error_response, true);
        });
        if (sessionStorage.jobSetupOutput != undefined) {
            self.gOutputData = jQuery.parseJSON(sessionStorage.jobSetupOutput);
            getBubbleCounts(self.gOutputData);
        }
    }

    //This function will execute when logging dropdown changed
    self.loggingChanged = function (event, data) {
        var search_data;
        self.setJobDetailsInfo(self.selectedItems()[0], 0);
    }

    //This function will provide logging info
    self.getLoggingData = function () {
        getCORS(gLogListUrl + '/0/1/0', null, function (data) {
            $.each(data, function (a, b) {
                self.gLogginData.push(b[0]);
            });
        }, function (error_response) {
            showErrorResponseText(error_response, true);
        });
    }
    self.getLoggingData();

    //This function will load default logging information
    self.setJobDetailsInfo(0, 1);

    //This function will load default logging information
    self.loadLoggingInfo = function () {
        var logging = [];
        self.setJobDetailsInfo(0, 1);
        //ko.applyBindings(pageObj);
        //$('#jobListInfo').css('display', 'block');
        //$("div ul").each(function (I) {
        //    $(this).listview();
        //});
        //$("#jobListInfo").collapsibleset('refresh');

        //$('#jobListInfo').trigger('create');
        //$('.ui-page').trigger('create').listview().listview("refresh");

    }

    //Job Logging End
    self.makeActionPopup = function () {
        $('#editPub').popup('open');
    };

    self.getCustomer = function (data, e) {
        var customer_number = $(e.target).val();
        if (customer_number != "1") {
            sessionStorage.selectedCustomerNumber = customer_number;
            //Temporary purpose (jim's requirement on 10/oct/2016) remove following if condition later and uncomment pageshow customer disabled line
            //if (appPrivileges.customerNumber != "1" && sessionStorage.selectedCustomerNumber != "1") {
                self.getCustomerFacilityId(sessionStorage.selectedCustomerNumber);
            //}
        }
    };

    //Redirect from diagnostics to job management page.
    self.redirectToJobManagement = function (data, e) {
        window.setTimeout(function () {
            window.location.href = '../jobManagement/jobManagement.html';
        }, 500);        
    };

    ////Temporary purpose (jim's requirement on 10/oct/2016) remove later
    self.getCustomerFacilityId = function (customer_number) {
        var auth_string = sessionStorage.authString;
        sessionStorage.authString = makeBasicAuth('admin_td1', 'admin_td1');
        var nav_links_url = serviceURLDomain + "api/Authorization/" + customer_number;
        getCORS(nav_links_url, null, function (data) {
            if (data != null && data != "" && data.customerNumber == customer_number) {
                sessionStorage.selectedFacilityId = data.facility_id;
                $('#ddlFacility').val(sessionStorage.selectedFacilityId).selectmenu('refresh');
            }
            sessionStorage.authString = auth_string;
        }, function (error_response) {
            sessionStorage.authString = auth_string;
            showErrorResponseText(error_response, true);
        });
    };
};

