﻿$.mobile.popup.prototype.options.history = false;
jQuery.support.cors = true;
var mapping = { 'ignore': ["__ko_mapping__"] }

//******************** Data URL Variables Start **************************
var pageObj;
var gVarOfferInfoServiceUrl = "../jobOfferSetup/JSON/_jobOfferSetup.JSON";
var gServiceData;
var gServiceOffers = [];
var pageObj;
var companyData;
var primaryFacilityData;
var gOutputData;
var dontShowHintsAgain = (localStorage.getItem('doNotShowHintsAgain') != undefined && localStorage.getItem('doNotShowHintsAgain') != null && localStorage.getItem('doNotShowHintsAgain') != "" && localStorage.getItem('doNotShowHintsAgain') != "false") ? true : false;
//******************** Data URL Variables End **************************

//******************** Page Load Events Start **************************
$('#_jobOfferSetup').live('pagebeforecreate', function (event) {
    ko.setTemplateEngine(new ko.nativeTemplateEngine);
    displayMessage('_jobOfferSetup');
    createConfirmMessage("_jobOfferSetup");
    loadingImg('_jobOfferSetup');

    displayNavLinks();
    getOffersInfo();
    $("#navLinksPanel").on("panelopen", function (event, ui) {
        sessionStorage.isNavPanelOpen = true;
    });
    setDemoHintsSliderValue();
    createDemoHints("jobOfferSetup");
    //$('#sldrShowHints').val(sessionStorage.showDemoHints).slider('refresh');
});

$(document).on('pageshow', '#_jobOfferSetup', function (event) {

    window.setTimeout(function () {      
        pageObj = new loadOffersInfo();
        ko.applyBindings(pageObj);     
        var curr_date = new Date();
        var min_date = new Date(curr_date.getDate() + 1, curr_date.getMonth(), curr_date.getFullYear());
        $.each($('input[data-fieldtype=expiryDate]'), function () {

            $(this).datepicker({
                minDate: new Date(),
                dateFormat: 'mm/dd/yy',
                constrainInput: true
            });
        });
		if (gOutputData.jobTypeId != 114) {
            $('select[id^=limit]').find('option[value=""]').remove();
		}
		else {
		    $('select[id^=discountType]').find('option[value="Everyday Savings"]').remove();
		}
        //setting min date for exiration date 
        var min_exp_date = new Date(gOutputData.inHomeDate);
        min_exp_date.setDate(min_exp_date.getDate() + 1);
        $('input[data-fieldtype=expiryDate]').datepicker('option', 'minDate', min_exp_date);

        if (showNavBarDiv()) {
            $('#dvNavBar').trigger('create');
            $('#dvNavBar div ul li a').first().addClass('ui-btn-active');
        }
       
        if (pageObj.selectedStore() != "" && !isNewImplementationCustomers()) {
            $('#ddlLocations').selectmenu('refresh');
            $('#ddlLocations').trigger('change');
        }
       
        $('#dvOfferSetup').trigger('create');
        $('#dvOfferSetup').css('display', 'block');

        if (hidePreviewMailing()) {
            $('#btnPreviewMailing').css('display', 'none');
            $('#btnSaveTop').addClass('ui-first-child');
        }

        $.each($('input[type=radio]:checked'), function (key, val) {
            $(val).val($(this).val()).trigger('change');
        });
        $('#chkReplicate').checkboxradio('refresh');
        window.setTimeout(function () {
            $('div[data-role=collapsible]').collapsible('expand')
        }, 100);

    }, 500);
    if (!showNavBarDiv()) {
        $('#dvReplicate').css('display', 'none');
        $('#dvNavBar').css('display', 'none');
        if (hideOrderedLocations()) {
            $('a[data-icon="myapp-locations"]').css('display', 'none');
            $('a[data-icon="eye"]').addClass('ui-first-child');
        }
    }
    persistNavPanelState();
    if (!dontShowHintsAgain
            && (showOfferSetupHint())
            && (sessionStorage.showDemoHints == undefined || sessionStorage.showDemoHints == null || sessionStorage.showDemoHints == "" || sessionStorage.showDemoHints == "on")
            && (sessionStorage.isOfferSetupDemoHintsDisplayed == undefined || sessionStorage.isOfferSetupDemoHintsDisplayed == null || sessionStorage.isOfferSetupDemoHintsDisplayed == "false")) {
        sessionStorage.isOfferSetupDemoHintsDisplayed = true;
        $('#offerSetupHints').popup('close');
        window.setTimeout(function loadHints() {
            $('#offerSetupHints').popup('open', { positionTo: '#dvOfferSetup' });
        }, 500);
    }
    $('div[data-role=popup] div[data-role=header]').removeClass('ui-corner-top ui-header ui-bar-d ui-header-fixed slidedown ui-panel-fixed-toolbar ui-panel-animate ui-panel-page-content-position-left ui-panel-page-content-display-push ui-panel-page-content-open');
    $('div[data-role=popup] div[data-role=header]').addClass('ui-corner-top ui-header ui-bar-d');
    fixAlertHeader();
});

var getOffersInfo = function () {
    if (isNewImplementationCustomers()) {
        gServiceOffers = [];
        if (sessionStorage.jobSetupOutput != undefined && sessionStorage.jobSetupOutput != null && sessionStorage.jobSetupOutput != "") {
            gOutputData = $.parseJSON(sessionStorage.jobSetupOutput);
            var offer_List = gOutputData.variableOfferAction.offerList;
            if ((gOutputData.variableOfferAction != undefined && gOutputData.variableOfferAction != null && gOutputData.variableOfferAction != "") &&
                (gOutputData.variableOfferAction.offerList != undefined && gOutputData.variableOfferAction.offerList != null && gOutputData.variableOfferAction.offerList != "") && Object.keys(gOutputData.variableOfferAction.offerList).length > 0) {
                gServiceOffers = gOutputData.variableOfferAction.offerList;
            }
        }
        if (jobCustomerNumber == SPORTSKING && gServiceOffers.length == 0) {
            $.getJSON(gVarOfferInfoServiceUrl, function (data) {
                $.each(data, function (key, val) {
                    if (key.indexOf(jobCustomerNumber) > -1) {
                        if (jobCustomerNumber == SPORTSKING) {
                            gServiceOffers = val[0].offerList;
                        }
                    }
                });
            });
        }
    }
    else {       
        $.getJSON(gVarOfferInfoServiceUrl, function (data) {
            $.each(data, function (key, val) {
                if (key.indexOf(jobCustomerNumber) > -1) {
                    gServiceOffers = val;
                }
            });
        });
    }
}

var offerInfo = function (offer_name, offer_value, is_checked) {
    var self = this;
    self.offerName = ko.observable('');
    self.offerName(offer_name);
    self.offerValue = ko.observable('');
    self.offerValue(offer_value);
    self.isChecked = ko.observable('');
    self.isChecked(is_checked);
};

var selectedOfferInfo = function (offer_type, offer_name, offer_value, offer_expiry_date, offer_description, offer_amount, offer_own_text, percent, offer_redeem_at, online_code, purchase_amount, sale_price, discount_type , discount_amount, total_price, limit, item, product_size, product_coverage) {
    var self = this;
    self.offerType = ko.observable('');
    self.offerType(offer_type);
    self.offerName = ko.observable('');
    self.offerName(offer_name);
    self.offerValue = ko.observable('');
    self.offerValue(offer_value);
    self.offerAmount = ko.observable('');
    self.offerAmount(offer_amount);
    self.offerDescription = ko.observable('');
    self.offerDescription(offer_description);
    self.offerExpiryDate = ko.observable('');
    self.offerExpiryDate(offer_expiry_date);
    self.ownOfferText = ko.observable('');
    self.ownOfferText(offer_own_text);
    if (percent != undefined && percent != null) {
        self.offerPercent = ko.observable('');
        self.offerPercent(percent);
    }
    if (offer_redeem_at) {
        self.offerDisclaimer = ko.observable('');
        self.offerDisclaimer(offer_redeem_at);
    }
    if (online_code) {
        self.onlineCode = ko.observable('');
        self.onlineCode(online_code);
    }
    if (purchase_amount) {
        self.purchaseAmount = ko.observable('');
        self.purchaseAmount(purchase_amount);
    }
    if (sale_price != undefined && sale_price != null) {
        self.salePrice = ko.observable('');
        var temp_amount = ((sale_price != "n/a" && sale_price != "$n/a" && sale_price != "NA") ? '$' : '') + sale_price;
        self.salePrice(temp_amount);
    }
    if (discount_type != undefined && discount_type != null) {
        self.discountType = ko.observable();
        self.discountType(discount_type);
    }
    if (discount_amount != undefined && discount_amount != null) {
        self.discountAmount = ko.observable();
        self.discountAmount(discount_amount);
    }
    if (total_price != undefined && total_price != null) {
        self.totalPrice = ko.observable('');
        var temp_amount = '$' + total_price;
        self.totalPrice(temp_amount);
    }
    if (limit != undefined && limit != null) {
        self.limit = ko.observable();
        self.limit(limit);
    }
    if (item != undefined && item != null) {
        self.item = ko.observable();
        self.item(item);
    }
    if (product_size != undefined && product_size != null) {
        self.productSize = ko.observable();
        self.productSize(product_size);
    }
    if (product_coverage != undefined && product_coverage != null) {
        self.productCoverage = ko.observable();
        self.productCoverage(product_coverage);
    }
};

var offersList = function (offer_type, offers_list, amount, offer_description, offer_exp_date, own_offer_text, selected_offer, percent, offer_redeem_at, online_code, purchase_amount, offer_image, sale_price, discount_type , discount_amount, total_price, limit, item, product_size, product_coverage) {
    var self = this;
    self.offerType = ko.observable('');
    self.offerType(offer_type);
    //self.offersList = ko.observableArray([]);
    self.variableDict = ko.observableArray([]);
    //self.offersList(offers_list);
    self.variableDict(offers_list);
    self.offerAmount = ko.observable('');
    var temp_amount = '$' + amount;
    self.offerAmount(temp_amount);

    self.offerDisclaimer = ko.observable('');
    self.offerDisclaimer(offer_redeem_at);
    self.onlineCode = ko.observable('');
    self.onlineCode(online_code);
    self.offerDescription = ko.observable('');
    self.offerDescription(offer_description);
    if (offer_exp_date == undefined || offer_exp_date == null || offer_exp_date == "") {
        var tmp_date = new Date();
        tmp_date.setDate(tmp_date.getDate() + parseInt(30));
        offer_exp_date = tmp_date.getMonth() + 1 + "/" + tmp_date.getDate() + "/" + tmp_date.getFullYear();
    }
    if (offer_exp_date != undefined && offer_exp_date != null) {
        self.offerExpiryDate = ko.observable('');
        self.offerExpiryDate(offer_exp_date);
    }
    self.ownOfferText = ko.observable('');
    self.ownOfferText(own_offer_text);
    self.isOwnOfferText = ko.observable(false);
    self.isVariableAmount = ko.observable((amount != "") ? true : false);
    self.selectedOffer = ko.observable();
    self.selectedOffer(selected_offer);
    if (percent != undefined && percent != null) {
        self.offerPercent = ko.observable('');
        var temp_percent = percent + '%';
        self.offerPercent(temp_percent);
    }
    if (purchase_amount != undefined && purchase_amount != null) {
        self.purchaseAmount = ko.observable('');
        self.purchaseAmount((((purchase_amount != "n/a" && purchase_amount != "$n/a" && purchase_amount != "NA") ? '$' : '') + purchase_amount));
    }
    if (offer_image != undefined && offer_image != null) {
        self.offerImage = ko.observable('');              
        var offer_img = logoPath + offer_image;
        self.offerImage(offer_img);
    }
    if (sale_price != undefined && sale_price != null) {
        self.salePrice = ko.observable('');
        var temp_amount = ((sale_price != "n/a" && sale_price != "$n/a" && sale_price != "NA") ? '$' : '') + sale_price;
        self.salePrice(temp_amount);
    }
    if (discount_type != undefined && discount_type != null) {
        self.discountType = ko.observable();
        self.discountType(discount_type);
    }
    if (discount_amount != undefined && discount_amount != null) {
        self.discountAmount = ko.observable();
        self.discountAmount(discount_amount);
    }
    if (total_price != undefined && total_price != null) {
        self.totalPrice = ko.observable('');
        var temp_amount = '$' + total_price;
        self.totalPrice(temp_amount);
    }
    if (limit != undefined && limit != null) {
        self.limit = ko.observable();
        self.limit(limit);
    }
    if (item != undefined && item != null) {
        self.item = ko.observable();
        self.item(item);
    }
    if (product_size != undefined && product_size != null) {
        self.productSize = ko.observable();
        self.productSize(product_size);
    }
    if (product_coverage != undefined && product_coverage != null) {
        self.productCoverage = ko.observable();
        self.productCoverage(product_coverage);
    }
};

var locationVm = function (store_id, store_name, store_pk) {
    var self = this;
    self.locationId = store_pk;
    if (store_id != undefined && store_id != null && store_id != "")
        self.locationName = store_id + ' - ' + store_name;
    else
        self.locationName = store_name;
};

var selectedOfersListInfo = function (offers_list, is_replicated) {
    var self = this;
    self.isReplicated = ko.observable('');
    self.isReplicated(is_replicated);
    $.each(offers_list, function (key, val) {
        self[key] = ko.observable({});
        self[key](val);
    });
};

var selectedOffersInfo = function (offers_selected, is_enabled) {
    var self = this;
    //self.offersList = ko.observableArray([]);
    self.variableDict = ko.observableArray([]);
    //self.offersList(offers_selected);
    self.variableDict(offers_selected);
    self.isEnabled = ko.observable('');
    self.isEnabled(is_enabled);
    self.name = "offerSetup";
};

var loadOffersInfo = function () {
    var self = this;
    //self.offersList = ko.observableArray([]);
    self.variableDict = ko.observableArray([]);
    self.locationsList = ko.observableArray([]);
    self.selectedLocation = ko.observable({});

    self.offerSetupInfo = ko.observable([]);
    self.prevOfferSetupInfo = ko.observable([]);
    self.selectedInHomeStoreOfferInfo = ko.observable({});
    self.selectedInhomeInfo = ko.observable({});
    self.inHomeDatesList = ko.observableArray([]);
    self.currentInHome = ko.observable();
    self.selectedStore = ko.observable('');
    self.isReplicated = ko.observable();
    self.previousSelectedStore = ko.observable({});
    self.ignoreEmptyCheckValidations = false;

    if ((sessionStorage.ignoreDiscountValidations == undefined || sessionStorage.ignoreDiscountValidations == null || sessionStorage.ignoreDiscountValidations == "")) {

        sessionStorage.ignoreDiscountValidations = "false";
    }

    self.discountTypeChanged = function (data, event) {
        var ele = event.target || event.currentTarget;
        var current_offer_info = ko.contextFor(ele).$parent;
        var not_applicable_values = ['saleprice', 'discountamount', 'limit', 'expirationdate'];
        var current_val = (data.value().replace(/ /g, '').toLowerCase() == "everydaysavings") ? 'NA' : '';
        $.each(current_offer_info.fieldList(), function (key, val) {
            if (val.type().toLowerCase() != "image") {
                if (current_val == 'NA') {
                    if (val.DisplayField() != 'n/a' && not_applicable_values.indexOf(val.name().toLowerCase()) > -1) {
                        val.DisplayField(current_val);
                    }
                }
                else {
                    val.DisplayField(current_val);                    
                }
            }                        
        });
    };

    self.getExpirationDate = function () {
        var expiry_date;
        var date_parts = null;
        var tempOutputData = jQuery.parseJSON(sessionStorage.jobSetupOutput);
        var in_home_date = tempOutputData.inHomeDate;
        if (in_home_date.indexOf('/') > -1) {
            date_parts = in_home_date.split('/');
        }
        else if (in_home_date.indexOf('-') > -1) {
            date_parts = in_home_date.split('-');
        }
        if (date_parts != null && date_parts.length > 2) {
            if (date_parts[2].length == 2)
                date_parts[2] = '20' + date_parts[2];
            var expiry_date = new Date(Number(date_parts[2]), Number(date_parts[0]) - 1, Number(date_parts[1]), 0, 0, 0, 0);
            expiry_date.setDate(expiry_date.getDate() + 30);
            return ((expiry_date.getMonth() + 1) + "/" + expiry_date.getDate() + "/" + expiry_date.getFullYear());
        }
        return "";
    };

    //Loading all the default offers into the screen
    var temp_offers_list = [];
    var expiration_date = self.getExpirationDate();
    $.each(gServiceOffers, function (key, val) {        
        if (isNewImplementationCustomers()) {
            var offer_details = {};
            ko.mapping.fromJS(val, {}, offer_details);
            if (isNewVariableFieldListCustomer()) {
                var is_every_day_savings = false;
                var image_folder_path = "";
                switch (jobCustomerNumber) {
                    case ACE:
                        image_folder_path = "ACE";
                        break;
                    case ANDERSEN:
                        image_folder_path = "Andersen";
                        break;
                    case SHERWINWILLIAMS:
                        image_folder_path = "SherwinWilliams";
                        break;
                    case MILWAUKEE:
                        image_folder_path = "Milwaukee";
                        break;
                    case BENJAMINMOORE:
                        image_folder_path = "BenjaminMoore";
                        break;
                    case TRAEGER:
                        image_folder_path = "Traeger";
                        break;
                    default:
                        image_folder_path = "Scotts";
                        break;
                }                
                
                $.each(offer_details.fieldList(), function (key, val) {
                    if (val.type != undefined && val.type() == "image") {
                        val.FullImagePath = ko.computed(function () {
                            return logoPath + image_folder_path + '/offerImages/' + val.value();
                        });
                    }
                    else if (val.type() == "dateTime" && val.name() == "expirationDate" && val.value() == "") {
                        val.value(expiration_date);
                    }
                    if (val.type() != "image") {
                       val.DisplayField = ko.observable(val.value());                        
                    }
                    if (val.name().toLowerCase() == "discounttype" && val.value().replace(/ /g, '').toLowerCase() == 'everydaysavings') {
                        is_every_day_savings = true;
                    }
                });
                //Hide fields for Everyday Savings
                if (is_every_day_savings) {
                    $.each(offer_details.fieldList(), function (k, v) {
                        if (v.type() != "image" && ['discounttype', 'purchaseprice', 'itemnumber', 'productsize', 'productcoverage'].indexOf(v.name().toLowerCase()) == -1) {
                            v.DisplayField = ko.observable('NA');
                        }
                    });
                }
            }
            else {
                self.isPurchaseOffer = ko.computed(function () {
                    var is_purchase_offer = false;
                    $.each(offer_details.fieldList(), function (key, val) {
                        if (val.name != undefined && val.name() == "offerName") {
                            is_purchase_offer = (val.value().toLowerCase().indexOf("purchaseoffer") > -1) ? true : false;
                            return false;
                        }
                    });
                    return is_purchase_offer;
                });
            }
            self.variableDict.push(offer_details);
        }       
        else {
            var selected_offer = "";
            $.each(val.offersList, function (offer_key, offer_val) {
                temp_offers_list.push(new offerInfo(offer_val.name, offer_val.value, offer_val.isChecked));
                if (offer_val.isChecked == "true")
                    selected_offer = offer_val.value;               
            });
            self.variableDict.push(new offersList(val.offerType, temp_offers_list, val.amount, val.description, ((expiration_date != "") ? expiration_date : val.expirationDate), val.ownOfferText, selected_offer, val.percent, val.offerDisclaimer, val.onlineCode, val.purchaseAmount, val.offerImage, val.salePrice, val.discountType, val.discountAmount, val.totalPrice, val.limit, val.item, val.productSize, val.productCoverage));
            temp_offers_list = [];
        }
    });

    if (!isNewImplementationCustomers()) {
        if (sessionStorage.jobSetupOutput != undefined && sessionStorage.jobSetupOutput != null && sessionStorage.jobSetupOutput != "") {
            gOutputData = $.parseJSON(sessionStorage.jobSetupOutput);
            if (sessionStorage.jobSetupOutput != undefined && sessionStorage.jobSetupOutput != null && sessionStorage.jobSetupOutput != "") {
                gOutputData = $.parseJSON(sessionStorage.jobSetupOutput);
                //Loading all the selected stores into drop down.
                if ((gOutputData.selectedLocationsAction != undefined && gOutputData.selectedLocationsAction != null && gOutputData.selectedLocationsAction != "") && (gOutputData.selectedLocationsAction.selectedLocationsList.length > 0)) {
                    $.each(gOutputData.selectedLocationsAction.selectedLocationsList, function (key, val) {
                        if (key == 0) {
                            self.selectedLocation(val.pk);
                            self.selectedStore(val.pk);
                            self.previousSelectedStore(self.selectedStore());
                        }
                        self.locationsList.push(new locationVm(val.storeId, ((val.storeName) ? val.storeName : ((val.location) ? val.location : "")), val.pk));
                    });
                }
            }

            if ((gOutputData.offerSetupAction != undefined && gOutputData.offerSetupAction != null && gOutputData.offerSetupAction != "")) {
                var temp_inhomewise_list = {};
                var is_replicated = '';
                var index = 0;
                var temp_bases_list = {};
                var offers_list = [];
                $.each(gOutputData.offerSetupAction, function (key, val) {
                    if (is_replicated == '' && key == 'isReplicated')
                        is_replicated = val;
                    else {
                        temp_inhomewise_list[key] = {};
                        $.each(val, function (key1, val1) {
                            var temp_offers_list = []
                            var source_list = (val1.offersList) ? val1.offersList : val1.variableDict;
                            $.each(source_list, function (key2, val2) {
                                //$.each(val1.variableDict, function (key2, val2) {
                                temp_offers_list.push(new selectedOfferInfo(val2.offerType, val2.offerName, val2.offerValue, val2.offerExpiryDate, val2.offerDescription, val2.offerAmount, val2.ownOfferText, val2.offerPercent, val2.offerDisclaimer, val2.onlineCode, val2.purchaseAmount));
                            });
                            temp_inhomewise_list[key][key1] = new selectedOffersInfo(temp_offers_list, val1.isEnabled);
                        });
                    }
                });
                if (is_replicated == true)
                    self.isReplicated(true);
                else
                    self.isReplicated(false);
                self.offerSetupInfo(new selectedOfersListInfo(temp_inhomewise_list, is_replicated));
                self.prevOfferSetupInfo(new selectedOfersListInfo(temp_inhomewise_list, is_replicated));
            }
        }
        //Loading Inhome dates nav bar.
        if (gOutputData != undefined && gOutputData != null) {
            var nav_bar_items = '<div data-role="navbar" class="ui-body-o" data-iconpos="right"><ul>';
            var cnt = -1;
            $.each(gOutputData, function (key, val) {
                if (key.toLowerCase().indexOf('inhome') > -1) {
                    var temp_list = {};
                    temp_list[key] = val;
                    self.inHomeDatesList.push({
                        "showCheckIcon": false,
                        'dateValue': val
                    });
                    cnt++;
                    if (cnt == 0)
                        nav_bar_items += '<li><a href="#" data-icon="check">' + val + ' In Home</a></li>';
                    else
                        nav_bar_items += '<li><a href="#" data-icon="edit">' + val + ' In Home</a></li>';
                }
            });
            nav_bar_items += '</ul></div>';
            self.currentInHome(self.inHomeDatesList()[0]);
        }
    }
   
    //This function will use only for SportsKing - SK(293)
    //Verify with jim and uncomment later - I believe no need to clear the fields while changing the offer
    self.offerSelectionChanged = function (data, event) {  
        //if (data.value().toLowerCase() == "purchaseoffer") { //$XX Off Your Next Purchase
        //    $.each(self.variableDict(), function (k, v) {
        //        $.each(v.fieldList(), function (k1, v1) {
        //            if (v1.name().toLowerCase().indexOf('ownoffertext') > -1) {
        //                v1.value('');
        //                return false;
        //            }
        //        });
        //    });
        //}
        //else { //Write your own offer
        //    $.each(self.variableDict(), function (k, v) {
        //        $.each(v.fieldList(), function (k1, v1) {
        //            if (v1.name().toLowerCase().indexOf('amount') > -1 || v1.name().toLowerCase().indexOf('onlinecode') > -1) {
        //                v1.value('');                        
        //            }                   
        //        });
        //    });
        //}
    };

    self.offerChanged = function (data, event) {
        var ele = event.target || event.targetElement;
        var parent_context = ko.contextFor(ele).$parent;
        //if ($(ele).is('checked')) {
        //data.isChecked(true);
        if ($(ele).val().toLowerCase().indexOf('variableamount') > -1 || $(ele).val().toLowerCase().indexOf('purchaseoffer') > -1) {
            parent_context.isVariableAmount(true);
            parent_context.ownOfferText('');
            parent_context.isOwnOfferText(false);
            if (appPrivileges.customerNumber == SPORTSKING) {
                if (gOutputData.offerTextAction.variableDict != undefined && gOutputData.offerTextAction.variableDict != null && Object.keys(gOutputData.offerTextAction.variableDict).length == 0)
                    parent_context.offerDescription('A Special Offer from One Baseball Lover to Another');
            }
            parent_context.selectedOffer(ele.value);
        }
        else if ($(ele).val().toLowerCase().indexOf('ownoffer') > -1) {
            parent_context.isVariableAmount(false);
            //parent_context.offerAmount('');
            if (parent_context.offerAmount != undefined && parent_context.offerAmount != null)
                parent_context.offerAmount('');
            if (appPrivileges.customerNumber == SPORTSKING) {
                if (gOutputData.offerTextAction.variableDict != undefined && gOutputData.offerTextAction.variableDict != null && Object.keys(gOutputData.offerTextAction.variableDict).length == 0)
                    parent_context.offerDescription('');
            }
            parent_context.isOwnOfferText(true);
            parent_context.selectedOffer(ele.value);
        }
        else {
            //parent_context.ownOfferText('');
            //parent_context.offerAmount('$');
            parent_context.isOwnOfferText(false);
            parent_context.isVariableAmount(false);
            //parent_context.offerDescription('');
            //if (parent_context.offerAmount != undefined && parent_context.offerAmount != null)
            //    parent_context.offerAmount('%');
            parent_context.selectedOffer('');
            if (appPrivileges.customerNumber == ALLIED) {
                parent_context.selectedOffer(ele.value);
            }
        }
        //}
        $('#dvOfferSetup').trigger('create');
    };

    //Load the base info for the selected location in the current inhome.
    self.getSelectedInfo = function () {
        //$('div.ui-grid-a').find('label,input,textarea').remove();
        var selected_inhome = (!isNewImplementationCustomers()) ? self.currentInHome().dateValue : "";
        var is_found = false;
        if (self.offerSetupInfo()[selected_inhome] != undefined && self.offerSetupInfo()[selected_inhome]() != undefined && self.offerSetupInfo()[selected_inhome]() != null)
            self.selectedInhomeInfo(self.offerSetupInfo()[selected_inhome]());

        if (self.offerSetupInfo()[selected_inhome] != undefined && self.offerSetupInfo()[selected_inhome]()[self.selectedStore()] != undefined && self.offerSetupInfo()[selected_inhome]()[self.selectedStore()] != null) {
            self.selectedInHomeStoreOfferInfo(self.offerSetupInfo()[selected_inhome]()[self.selectedStore()]);
            if (ko.toJS(self.offerSetupInfo()[selected_inhome]()[self.selectedStore()].variableDict).length > 0) {
                var temp = ko.toJS(self.offerSetupInfo()[selected_inhome]()[self.selectedStore()].variableDict);
                $.each(temp, function (key, val) {
                    var is_found = false;
                    //$.each(val.offersList, function (key1, val1) {
                    $.each(self.variableDict(), function (key1, val1) {
                        if (val1.offerType() == val.offerType) {
                            $.each(val1.variableDict(), function (key2, val2) {
                                if (val2.offerName() == val.offerName) {
                                    val2.isChecked(true);
                                    val1.offerType(val.offerType);
                                    val1.offerDisclaimer(val.offerDisclaimer);
                                    val1.offerDescription(val.offerDescription);
                                    var amt = '$' + val.offerAmount;
                                    val1.offerAmount(amt);
                                    if (val1.offerExpiryDate != undefined && val1.offerExpiryDate != null)
                                        val1.offerExpiryDate(val.offerExpiryDate);
                                    val1.isVariableAmount(((val.offerAmount.substring(1) != "") ? true : false));
                                    val1.isOwnOfferText(((val.ownOfferText != "") ? true : false));
                                    val1.ownOfferText(val.ownOfferText);
                                    val1.selectedOffer(val.offerValue);
                                    if (val1.offerPercent != undefined && val1.offerPercent != null) {
                                        var pcnt = val.offerPercent + '%';
                                        val1.offerPercent(pcnt);
                                    }
                                    is_found = true;
                                }
                                if (is_found)
                                    return false;
                            });
                        }
                        if (is_found)
                            return false;
                    });
                    //});
                });
            }
        }
        $('#dvOfferSetup').css('display', 'block');
        $('#dvOfferSetup').trigger('create');
        var temp = ko.toJS(self.offerSetupInfo());
        self.updateInHomeNavBarUI(temp);
    };

    self.persistSelectedOfferInfo = function () {
        var selected_inhome = self.currentInHome().dateValue;

        if (selected_inhome != "") {// && self.selectedStore() != ""
            var temp_offers_list = ko.observableArray([]);
            var offers_list = ko.observableArray([]);
            var is_found = false;
            $.each(self.variableDict(), function (key, val) {
                $.each(val.variableDict(), function (key1, val1) {
                    //if (JSON.parse(val1.isChecked())) {
                    var selected_offer = val.selectedOffer();
                    if (val1.offerValue() == selected_offer) {
                        var temp = ko.toJS(val1);
                        var temp_val = ko.toJS(val);
                        var offer_percent = (temp_val.offerPercent != undefined && temp_val.offerPercent != null) ? temp_val.offerPercent.substring(0, temp_val.offerPercent.length - 1) : "";
                        temp = new selectedOfferInfo(temp_val.offerType, temp.offerName, temp.offerValue, temp_val.offerExpiryDate, temp_val.offerDescription, ((temp_val.offerAmount.indexOf('$') > -1) ? temp_val.offerAmount.substring(1) : temp_val.offerAmount), temp_val.ownOfferText, offer_percent, temp_val.offerDisclaimer, temp_val.onlineCode, ((temp_val.purchaseAmount != undefined && temp_val.purchaseAmount != null && temp_val.purchaseAmount.indexOf('$') > -1) ? temp_val.purchaseAmount.substring(1) : temp_val.purchaseAmount), ((temp_val.salePrice != undefined && temp_val.salePrice.indexOf('$') > -1) ? temp_val.salePrice.substring(1) : temp_val.salePrice), temp_val.discountType, temp_val.discountAmount, ((temp_val.totalPrice != undefined && temp_val.totalPrice.indexOf('$') > -1) ? temp_val.totalPrice.substring(1) : temp_val.totalPrice), temp_val.limit, temp_val.item, temp_val.productSize, temp_val.productCoverage);
                        temp_offers_list.push(temp);
                        val1.isChecked(true);
                        is_found = true;
                    }
                    if (is_found)
                        return false;
                });
                //var temp_amount = '$';
                if (is_found) {
                    //val.offerAmount('$');
                    //val.offerDescription('');
                    //if (val.offerExpiryDate != undefined && val.offerExpiryDate != null) val.offerExpiryDate('');
                    // if (val.offerPercent != undefined && val.offerPercent != null) val.offerPercent('%');
                    //val.ownOfferText('');
                    // val.isOwnOfferText(false);
                    // val.isVariableAmount(false);
                    //val.selectedOffer('');
                    is_found = false;
                }
            });
            if (ko.toJS(temp_offers_list).length > 0) {
                self.offerSetupInfo()[selected_inhome]()[self.selectedStore()].variableDict([]);
                self.offerSetupInfo()[selected_inhome]()[self.selectedStore()].variableDict(temp_offers_list);
            }

        }
    };

    //Loading the data based on the selected store in the current inhome.
    self.locationChanged = function (data, event) {
        //self.validateForDataExistency();
        var ele;
        var selected_store;
        var ele = event.target || event.targetElement;
        var selected_store = $(ele).val();
        var selected_inhome = self.currentInHome().dateValue;

        self.persistSelectedOfferInfo();

        self.selectedStore(self.selectedLocation());
        if (self.selectedLocation() != "") {
            self.getSelectedInfo();
            var curr_info = ko.toJS(self.offerSetupInfo());
            var prev_info = ko.toJS(self.prevOfferSetupInfo());
            var diff_between_json_objects = [];
            var is_diff_found = false;
            $.each(curr_info, function (key1, val1) {
                if (key1.toLowerCase() != 'isreplicated') {
                    $.each(prev_info, function (key2, val2) {
                        if (key2.toLowerCase() != 'isreplicated') {
                            $.each(val1, function (key3, val3) {
                                var curr_temp1 = val3;
                                var prev_temp2 = val2[key3];
                                diff_between_json_objects = DiffObjects(jQuery.parseJSON(JSON.stringify(curr_temp1)), jQuery.parseJSON(JSON.stringify(prev_temp2)));
                                if (diff_between_json_objects.length != 0) {
                                    is_diff_found = true;
                                    return false;
                                }
                            });
                        }
                        if (is_diff_found) {
                            return false;
                        }
                    });
                    if (is_diff_found) {
                        return false;
                    }
                }
            });

            var is_valid = false;
            $.each(curr_info, function (key, val) {
                if (key != "isReplicated") {
                    is_valid = self.verifyData(val);
                }
                if (!is_valid)
                    return false;
            });
            if (is_diff_found && !is_valid) {
                self.isReplicated(false);
                $('#chkReplicate').checkboxradio('refresh');
            }
            else if (is_valid) {
                self.isReplicated(true);
                $('#chkReplicate').checkboxradio('refresh');
            }
        }
    };

    self.navBarItemClick = function (data, event) {
        //$('div.ui-grid-a').find('label,input,textarea').remove();
        var ele = event.target || event.targetElement;
        $('#dvNavBar div ul li a').removeClass('ui-btn-active');
        $(ele).addClass('ui-btn-active');
        self.persistSelectedOfferInfo();
        self.currentInHome(ko.dataFor(ele));
        self.getSelectedInfo();
    };

    self.replicateChanged = function (data, event) {
        self.persistSelectedOfferInfo();
        var ele = event.target || event.targetElement;
        $('#confirmMsg').html('Replicating Setup Entries will overwrite any previous information entered.');
        $('#okButConfirm').text('Continue');

        var curr_info = ko.toJS(self.offerSetupInfo());
        var prev_info = ko.toJS(self.prevOfferSetupInfo());
        var is_diff_found = false;
        $.each(curr_info, function (key1, val1) {
            if (key1.toLowerCase() != 'isreplicated') {
                $.each(prev_info, function (key2, val2) {
                    if (key2.toLowerCase() != 'isreplicated') {
                        //if (key1 == key2) {
                        $.each(val1, function (key3, val3) {
                            var curr_temp1 = val3;
                            var prev_temp2 = val2[key3];
                            diff_between_json_objects = DiffObjects(jQuery.parseJSON(JSON.stringify(curr_temp1)), jQuery.parseJSON(JSON.stringify(prev_temp2)));
                            if (diff_between_json_objects.length != 0) {
                                is_diff_found = true;
                                return false;
                            }
                        });
                        //}
                        if (is_diff_found) {
                            return false;
                        }
                    }
                });
                if (is_diff_found) {
                    return false;
                }
            }
        });
        $('#cancelButConfirm').bind('click', function () {
            $('#cancelButConfirm').unbind('click');
            $('#okButConfirm').unbind('click');
            $('#cancelButConfirm').attr('onclick', '$(\'#popupConfirmDialog\').popup(\'close\');');
            self.isReplicated(false);
            $('#chkReplicate').checkboxradio('refresh');
            $('#popupConfirmDialog').popup('close');
        });
        $('#okButConfirm').bind('click', function () {
            $('#cancelButConfirm').unbind('click');
            $('#cancelButConfirm').attr('onclick', '$(\'#popupConfirmDialog\').popup(\'close\');');
            $('#okButConfirm').unbind('click');
            self.replicateData();
            self.isReplicated(true);
            $('#chkReplicate').checkboxradio('refresh');
            $('#popupConfirmDialog').popup('close');
        });
        if ($(ele).is(':checked') && is_diff_found) {
            $('#popupConfirmDialog').popup('open');
        }
    };

    self.replicateData = function () {
        var selected_inhome = self.currentInHome().dateValue;
        var temp_info = {};
        if (self.offerSetupInfo()[selected_inhome]() != undefined && self.offerSetupInfo()[selected_inhome]() != null && self.offerSetupInfo()[selected_inhome]() != "")
            temp_info = self.offerSetupInfo()[selected_inhome]();

        $.each(self.inHomeDatesList(), function (key2, val2) {
            if (selected_inhome != val2.dateValue) {
                self.offerSetupInfo()[val2.dateValue](temp_info);
            }
        });
        self.offerSetupInfo().isReplicated(true);
        self.getSelectedInfo();

        var temp = ko.toJS(self.offerSetupInfo());
        if ((temp != undefined && temp != null && temp != "")) {
            var temp_inhomewise_list = {};
            var is_replicated = '';
            var index = 0;
            var temp_bases_list = {};
            $.each(temp, function (key, val) {
                if (is_replicated == '' && key == 'isReplicated')
                    is_replicated = val;
                else {
                    temp_inhomewise_list[key] = {};
                    $.each(val, function (key1, val1) {
                        var temp_offers_list = []
                        $.each(val1.variableDict, function (key2, val2) {
                            temp_offers_list.push(new selectedOfferInfo(val2.offerType, val2.offerName, val2.offerValue, val2.offerExpiryDate, val2.offerDescription, val2.offerAmount, val2.ownOfferText, val2.offerDisclaimer, val2.onlineCode));
                        });
                        temp_inhomewise_list[key][key1] = new selectedOffersInfo(temp_offers_list, val1.isEnabled);
                    });
                }
            });
            self.prevOfferSetupInfo(new selectedOfersListInfo(temp_inhomewise_list, is_replicated));
        }
        self.updateInHomeNavBarUI(temp);
    };

    self.verifyData = function (temp_info) {
        var is_data_found = false;
        $.each(temp_info, function (key1, val1) {
            if (val1.variableDict != undefined && val1.variableDict.length > 0)
                is_data_found = true;
            else
                is_data_found = false;
            if (!is_data_found) {
                return false;
            }
        });
        return is_data_found;
    };

    self.updateInHomeNavBarUI = function (temp) {
        if (!isNewImplementationCustomers()) {
            $.each(temp, function (key, val) {
                if (key != "isReplicated") {
                    var is_valid = self.verifyData(val);
                    if (is_valid) {
                        $('#dvNavBar div ul li a[data-inHome="' + key + '"]').attr('data-icon', 'check').addClass('ui-icon-check').removeClass('ui-icon-edit');
                    }
                    else {
                        $('#dvNavBar div ul li a[data-inHome="' + key + '"]').attr('data-icon', 'edit').addClass('ui-icon-edit').removeClass('ui-icon-check');
                    }
                }
            });
        }
    };

    self.confirmContinue = function (current_page, page_name, i_count, click_type) {
        if (!isNewImplementationCustomers()) {
            self.persistSelectedOfferInfo();
            var temp = ko.toJS(self.offerSetupInfo());
            self.updateInHomeNavBarUI(temp);
            var ctrls = $('#dvNavBar div ul li a.ui-icon-minus');
            if (ctrls.length > 0 && click_type == "onlycontinue") {
                var tmp_list = "";
                $.each(ctrls, function (key, val) {
                    tmp_list += (tmp_list != "") ? (key < (ctrls.length - 1)) ? ', ' : ' and ' + $(this).attr('data-inHome') : $(this).attr('data-inHome');
                });

                $('#confirmMsg').html('You have not completed setup for ' + tmp_list + '. Do you want to stay on this page to continue setup or go to the next page?');
                $('#okButConfirm').text('Go To Next Page');
                $('#cancelButConfirm').text('Continue Setup');

                $('#cancelButConfirm').bind('click', function () {
                    $('#cancelButConfirm').unbind('click');
                    $('#okButConfirm').unbind('click');
                    $('#cancelButConfirm').attr('onclick', '$(\'#popupConfirmDialog\').popup(\'close\');');
                    $('#cancelButConfirm').text('Cancel');
                    $('#popupConfirmDialog').popup('close');
                });
                $('#okButConfirm').bind('click', function () {
                    $('#okButConfirm').unbind('click');
                    $('#cancelButConfirm').unbind('click');
                    $('#cancelButConfirm').attr('onclick', '$(\'#popupConfirmDialog\').popup(\'close\');');
                    $('#cancelButConfirm').text('Cancel');
                    $('#popupConfirmDialog').popup('close');
                    goToNextPage("../jobSubmitOrder/jobSubmitOrder.html", "../jobSubmitOrder/jobSubmitOrder.html", 5, 'onlycontinue');
                    self.updateOfferSelectionInfo(current_page, page_name, i_count, click_type);
                });
                $('#popupConfirmDialog').popup('open');
                return false;
            }
            else {
                return self.updateOfferSelectionInfo(current_page, page_name, i_count, click_type);
            }
        }
        else {
            return self.updateOfferSelectionInfo(current_page, page_name, i_count, click_type);
        }
    };

    self.updateOfferSelectionInfo = function (current_page, page_name, i_count, click_type) {
        var temp_json = ko.mapping.toJS(self.variableDict, mapping);
        if (isNewImplementationCustomers()) {
            if (isNewVariableFieldListCustomer()) {
                if (self.validateOffers(temp_json, current_page, page_name, i_count, click_type)) {
                    $.each(temp_json, function (key, val) {
                        var field_Dict = {};
                        $.each(val.fieldList, function (key1, val1) {
                            if (val1.type == "image")
                                delete val1["FullImagePath"];
							else
                                delete val1["DisplayField"];

                            if (val1.type == "dollars" && val1.name != "discountType" && val1.name != "limit" && (val1.value != "n/a" && val1.value != "$n/a" && val1.value != "NA")) {
                                if (val1.value != "") {
                                    if (val1.name == "discountAmount") {
                                        val1.value = '$' + ((val1.value.indexOf('$') > -1) ? val1.value.replace(/\$/g, '') : val1.value);
                                    }
                                    else {
                                        val1.value = '$' + ((val1.value.indexOf('$') > -1) ? (parseFloat(val1.value.replace(/\$/g, '')).toFixed(2)) : parseFloat(val1.value).toFixed(2));
                                    }
                                }
                            }
                        });
                    });
                }
                else return false;
            }
            var g_output_data_compare = $.parseJSON(sessionStorage.jobSetupOutputCompare);
            var diff = [];
            diff = DiffObjects(g_output_data_compare.variableOfferAction.offerList, temp_json);
            if (diff.length > 0 && click_type != "save") {
                var message = "Changes to the offers have not been saved. Please save or discard your changes now.";
                $('#confirmMsg').html(message);
                $('#okButConfirm').text('Save');
                $('#cancelButConfirm').text('Discard');
                $('#okButConfirm').bind('click', function () {
                    $('#cancelButConfirm').unbind('click');
                    $('#okButConfirm').unbind('click');
                    $('#cancelButConfirm').attr('onclick', '$(\'#popupConfirmDialog\').popup(\'close\');');
                    $('#okButConfirm').text('Ok');
                    $('#cancelButConfirm').text('Cancel');
                    $('#popupConfirmDialog').popup('close');
                    gOutputData.variableOfferAction["offerList"] = temp_json;
                    gOutputData.isGmcDataUpdated = true;
                    sessionStorage.jobSetupOutput = JSON.stringify(gOutputData);
                    postJobSetUpData('save');
                    sessionStorage.isJobTicketUpdate = true;
                    if (click_type == "onlycontinue" || click_type == undefined)
                        window.location.href = (page_name != undefined && page_name != null && page_name != "") ? page_name : current_page;
                    return true;
                });
                $('#cancelButConfirm').bind('click', function () {
                    $('#cancelButConfirm').unbind('click');
                    $('#okButConfirm').unbind('click');
                    $('#cancelButConfirm').attr('onclick', '$(\'#popupConfirmDialog\').popup(\'close\');');
                    $('#okButConfirm').unbind('click');
                    $('#okButConfirm').text('Ok');
                    $('#cancelButConfirm').text('Cancel');
                    $('#popupConfirmDialog').popup('close');
                    sessionStorage.jobSetupOutput = JSON.stringify(gOutputData);
                    if (click_type == "onlycontinue" || click_type == undefined)
                        window.location.href = (page_name != undefined && page_name != null && page_name != "") ? page_name : current_page;
                    return true;
                });
                $('#popupConfirmDialog').popup('open');
                disableSaveForClosedJobs();
                return false;
            }
            else {
                if (diff.length > 0) {
                    sessionStorage.isJobTicketUpdate = true;
                    gOutputData.isGmcDataUpdated = true;
                }
                gOutputData.variableOfferAction["offerList"] = temp_json;
            }
        }
        else {
            gOutputData.offerSetupAction = ko.toJS(self.offerSetupInfo);
        }
        sessionStorage.jobSetupOutput = JSON.stringify(gOutputData);

        if (click_type == "onlycontinue")
            window.location.href = (page_name != undefined && page_name != null && page_name != "") ? page_name : current_page;
        return true;
    };

    self.showOrderedLocations = function () {
        $('#popUpLocations').popup('open');
    };

    self.manageLocationsPopup = function (data, event) {
        var ele = event.target || event.targetElement;
        if ($(ele).attr('data-btnType') == "submit") {
            self.previousSelectedStore(self.selectedStore());
        }
        else {
            self.selectedStore(self.previousSelectedStore());
            self.selectedLocation(self.previousSelectedStore())
        }
        $('#ddlLocations').selectmenu('refresh');
        displayJobInfo();
        var header_text = $('#navHeader').text();
        var sel_location = $('#ddlLocations option:selected').text();
        header_text = header_text + ' (' + sel_location + ')';
        $('#navHeader').text(header_text);
        $('#popUpLocations').popup('close');
    };
    
    self.previewMailing = function () {
        //Todo: Need to update pdf file for Kubota customer
        if (showPreviewMailing()) {
            previewMailing();
            return false;
        }
        else if (appPrivileges.customerNumber == DCA) {
            encodedFileName = "DCA%2041941%20Jupiter%20Dental%20_Postcard_Dr.%20Vera_MECH.pdf";
        }
        if (appPrivileges.customerNumber == AAG) {
            encodedFileName = "TCA_10232_AAG_Lunch_Learn_2_Presenters-2_Events_Lenfers_Culbreth_Let.pdf";
        } else if (appPrivileges.customerNumber == ALLIED) {
            encodedFileName = "GW_Berkheimer_Jansen.pdf";
        } else if (appPrivileges.customerNumber == ALLIED) {
            encodedFileName = "GW_Berkheimer_Jansen.pdf";
        }
        getNextPageInOrder();
        //var preview_proof = "../jobProofing/jobProofing.html?fileName=58683_GWBerkheimerOLSONCOMFORT.pdf&jobNumber=" + sessionStorage.jobNumber;
        var preview_proof = "../jobApproval/previewMailing.html?fileName=" + encodedFileName + "&jobNumber=" + sessionStorage.jobNumber;
        window.location.href = preview_proof;
    };

    //Validates Offers entered by User.
    self.validateOffers = function (offersData, current_page, page_name, i_count, click_type) {
        var areAllOffersEntered = true;
        var areAllSalePricesCorrect = true;
        var areAllPriceFormatsCorrect = true;
        var offerNameWithWrongSalePrice = '<ul>';
        var discount_type = '';
        var input_type;


        $.each(offersData, function (key, eachOffer) {
            //don't validate with emty fields if self.ignoreEmptyCheckValidations is true 
            var controlsToValidate = $('#' + eachOffer.name + ' input[data-fieldtype=price]');
            discount_type = '';
            $.each(eachOffer.fieldList, function (fieldKey, fieldVal) {
                if (fieldVal.name == 'discountType') {
                    discount_type = fieldVal.value;
                }
            });

            $.each(controlsToValidate, function (ctrlKey, ctrl) {
                if ($(ctrl).val().trim() != "" && $(ctrl).val().trim().indexOf("n/a") == -1) {
                    if (discount_type.replace(/ /g, '').toLowerCase() != 'everydaysavings' || (($(ctrl)[0].name != 'salePrice') && ($(ctrl)[0].name != 'discountAmount')) || (($(ctrl)[0].name.indexOf('Line') == -1) && ($(ctrl)[0].name != 'storeName' && ($(ctrl)[0].name != 'caption')))) {
                        if (!priceValidation(ctrl)) {
                            if (areAllPriceFormatsCorrect == true) {
                                $('#' + eachOffer.name + ' input:text[id=purchasePrice]').parent().attr("tabindex", -1).focus();
                            }
                            areAllPriceFormatsCorrect = false;
                        }
                    }
                }
            });
        });
        if (areAllPriceFormatsCorrect == false) {
            return false;
        }
        if (self.ignoreEmptyCheckValidations == "" || self.ignoreEmptyCheckValidations == "false") {
            $.each(offersData, function (key, eachOffer) {   //*********Testing empty data
                $.each(eachOffer.fieldList, function (innerKey, eachOfferobject) {
                    input_type = eachOfferobject.name != 'discountType' ? 'text' : 'select';

                    //empty checking
                    if (((eachOfferobject.type == 'dollars' || eachOfferobject.type == 'string' || eachOfferobject.type == 'date time') && eachOfferobject.value.trim() == '' && eachOfferobject.value.trim() != 'n/a' && eachOfferobject.DisplayField != undefined && eachOfferobject.DisplayField != 'NA') || (eachOfferobject.name == 'discountType' && eachOfferobject.value.trim() == "")) {
                        if (input_type == 'text') {//for textbox
                            $('#' + eachOffer.name + ' input:' + input_type + '[id=' + eachOfferobject.name + ']').css('border', 'solid 2px yellow');
                            if (areAllOffersEntered == true) {
                                //setting focus on first control
                                $('#' + eachOffer.name + ' input:' + input_type + '[id=' + eachOfferobject.name + ']').parent().attr("tabindex",-1).focus();
                            }
                        }
                        else {//for select control 
                            $('#' + eachOffer.name + ' ' + input_type + '[id=' + eachOfferobject.name + ']').parent().css('border', 'solid 2px yellow');
                            if (areAllOffersEntered == true) {
                                //setting focus on first control
                                $('#' + eachOffer.name + ' ' + input_type + '[id=' + eachOfferobject.name + ']').parent().attr("tabindex",-1).focus();
                            }
                        }
                        areAllOffersEntered = false;
                    }
                    else if (eachOfferobject.value != 'n/a' && eachOfferobject.value != 'NA') {
                        if (input_type == 'text') {
                            $('#' + eachOffer.name + ' input:' + input_type + '[id=' + eachOfferobject.name + ']').css('border', 'none');
                        }
                        else {
                            $('#' + eachOffer.name + ' ' + input_type + '[id=' + eachOfferobject.name + ']').parent().css('border', 'none');
                        }
                    }
                });
            });
            // if all fields are not filled throw an alert else go for calculation validations 
            if (areAllOffersEntered == false) {
                //showing confirmation message 
                $('#confirmMsg').html('All offer fields have not been completed. Please complete all fields before generating a proof or submitting your job.');
                $('#okButConfirm').text('Save and Return Later');
                $('#okButConfirm').bind('click', function () {
                    $('#cancelButConfirm').unbind('click');
                    $('#okButConfirm').unbind('click');
                    $('#okButConfirm').text('Ok');
                    $('#cancelButConfirm').attr('onclick', '$(\'#popupConfirmDialog\').popup(\'close\');');
                    $('#popupConfirmDialog').popup('close');
                    self.ignoreEmptyCheckValidations = "true";
                    gOutputData.variableOfferAction["offerList"] = offersData;
                    sessionStorage.jobSetupOutput = JSON.stringify(gOutputData);
                    postJobSetUpData('save');
                    window.location.href = (page_name != undefined && page_name != null && page_name != "") ? page_name : current_page;
                    return false;

                });
                $('#cancelButConfirm').bind('click', function () {
                    $('#cancelButConfirm').unbind('click');
                    $('#okButConfirm').unbind('click');
                    $('#okButConfirm').text('Ok');
                    $('#cancelButConfirm').attr('onclick', '$(\'#popupConfirmDialog\').popup(\'close\');');
                    $('#popupConfirmDialog').popup('close');

                });
                $('#popupConfirmDialog').popup('open');
                return false;
            }
        }
        if (areAllPriceFormatsCorrect == true && sessionStorage.ignoreDiscountValidations == "false") {//***************** Testing Discount Validations
            // validate sales price 
            $.each(offersData, function (key, eachOffer) {

                if (!(self.ignoreEmptyCheckValidations == "true" && ($('#' + eachOffer.name + ' input:text[id=purchasePrice]').val().trim() == "" && $('#' + eachOffer.name + ' input:text[id=salePrice]').val().trim() == "" && $('#' + eachOffer.name + ' input:text[id=discountAmount]').val().trim() == ""))) {
                    if ((($('#' + eachOffer.name + ' select[id=discountType]').length > 0 && jobCustomerNumber != SHERWINWILLIAMS) && $('#' + eachOffer.name + ' select[id=discountType]')[0].value != 'Everyday Savings') && parseFloat((($('#' + eachOffer.name + ' input:text[id=purchasePrice]').length > 0 && jobCustomerNumber != SHERWINWILLIAMS) ? ($('#' + eachOffer.name + ' input:text[id=purchasePrice]').val().replace('$', '').trim()) : 0)) != (($('#' + eachOffer.name + ' input:text[id=salePrice]').length > 0 && $('#' + eachOffer.name + ' input:text[id=discountAmount]').length > 0 && jobCustomerNumber != SHERWINWILLIAMS) ? (parseFloat(($('#' + eachOffer.name + ' input:text[id=salePrice]').val().replace('$', '').trim() - $('#' + eachOffer.name + ' input:text[id=discountAmount]').val().replace('$', '').trim()).toFixed(2))) : 0)) {
                        $('#' + eachOffer.name + ' input:text[id=purchasePrice]').css('border', 'solid 2px yellow');

                        if (areAllSalePricesCorrect == true) {
                            //setting focus on first control
                            $('#' + eachOffer.name + ' input:text[id=purchasePrice]').parent().attr("tabindex",-1).focus();
                        }
                        areAllSalePricesCorrect = false;
                        offerNameWithWrongSalePrice += '<li> <b>' + eachOffer.value + '</b></li> ';
                    }
                    else {
                        $('#' + eachOffer.name + ' input:text[id=salePrice]').css('border', 'none');
                    }
                }

            });
            if (areAllSalePricesCorrect == false) {
                //showing confirmation message 
                $('#confirmMsg').html('Please check the Purchase Price for ' + offerNameWithWrongSalePrice + '</ul>' + 'When the Discount Amount is subtracted from the Regular Price it does not equal the Purchase Price.');
                $('#okButConfirm').text('Ignore');
                $('#cancelButConfirm').text('Cancel');
                $('#okButConfirm').bind('click', function () {
                    $('#cancelButConfirm').unbind('click');
                    $('#okButConfirm').unbind('click');
                    $('#cancelButConfirm').attr('onclick', '$(\'#popupConfirmDialog\').popup(\'close\');');
                    $('#popupConfirmDialog').popup('close');
                    sessionStorage.ignoreDiscountValidations = "true";
                    // $('#btnSaveTop').trigger('click');
                });
                $('#cancelButConfirm').bind('click', function () {
                    $('#cancelButConfirm').unbind('click');
                    $('#okButConfirm').unbind('click');
                    $('#cancelButConfirm').attr('onclick', '$(\'#popupConfirmDialog\').popup(\'close\');');
                    $('#popupConfirmDialog').popup('close');

                });
                $('#popupConfirmDialog').popup('open');
            }
        }
        return areAllOffersEntered && areAllSalePricesCorrect && areAllPriceFormatsCorrect;
    };

    self.priceOnBlur = function (data, e) {
        var ele = e.target || e.targetElement;
        var regexpr = /^\$[0-9]{1,2}?$/;
        var error_message = 'Enter price in $XX format';
        var field_name = data.name().toLowerCase();

        switch (field_name) {
            case field_name.indexOf("discount") > -1:
                regexpr = /^\$[0-9]{1,2}?$/;
                error_message = 'Enter price in $XX format';
                break;
            case field_name.indexOf("totalpurchaseprice") > -1:
                regexpr = /^\$[0-9]{1,3}?$/;
                error_message = 'Enter price in $XXX format';
                break;
            default:
                regexpr = /^\$[0-9]{1,3}(\.[0-9]{2,2})$/;
                error_message = 'Enter price in $XXX.XX format';
                break;
        }

        var entered_value = $(ele).val();
        var result = true;

        //formating $ symbol in the enetered value 
        if (data.value().length > 0) {
            data.value(((data.value() != 'n/a' && data.value() != '$n/a' && data.value() != 'NA') ? '$' : '') + data.value().replace(/\$/g, ''));


            //adding decimals  
            if (field_name.indexOf("discount") == -1 && field_name.indexOf("totalpurchaseprice") == -1) {
                if (data.value().replace(/\$/g, '').trim().length >= 1 && data.value().indexOf('.') == -1) {
                    data.value(data.value() + '.00');
                }
                else if (data.value().replace(/\$/g, '').trim().length >= 1 && data.value().indexOf('.') > -1 && data.value().split('.')[1].length == 0) {
                    data.value(data.value() + '00');
                }
                else if (data.value().replace(/\$/g, '').trim().length >= 1 && data.value().indexOf('.') > -1 && data.value().split('.')[1].length == 1) {
                    data.value(data.value() + '0');
                }
            }

            if (!regexpr.test(data.value())) {
                $(ele)[0].title = error_message;
                $(ele).css('border', 'solid 2px yellow');
                result = false;
            }
            else {
                $(ele)[0].title = "";
                $(ele).css('border', 'none');
            }
        }
        else {
            $(ele)[0].title = "";
            $(ele).css('border', 'none');
        }
    };

    self.priceOnkeyPress = function (data, e) {
        var ele = e.target || e.targetElement;
        var field_name = data.name().toLowerCase();
        var regexpr = /^[0-9$]+$/g;
        var char_length = 3;

        switch (field_name) {
            case field_name.indexOf("discount") > -1:
                regexpr = /^[0-9$]+$/g;
                char_length = 3;
                break;
            case field_name.indexOf("totalpurchaseprice") > -1:
                regexpr = /^[0-9$]+$/g;
                char_length = 4;
                break;
            default:
                regexpr = /^[0-9.$]+$/g;
                char_length = 7;
                break;
        }
        var key = String.fromCharCode(e.keyCode);
       
        if (field_name.indexOf("discount") == -1 && field_name.indexOf("totalpurchaseprice") == -1) {
            if ($(ele).val().replace(/\$/g, '').length == 3 && $(ele).val().indexOf('.') == -1 && key != '.') {
                data.value($(ele).val() + '.');
            }
            //restricting double '.'
            if ($(ele).val().indexOf('.') > -1 && key == '.') {
                return false;
            }
        }
        else {
            var max_length_to_verify = (field_name.indexOf("discount") > -1) ? 2 : 3;
            if ($(ele).val().replace(/\$/g, '').length == max_length_to_verify && $(ele).caret().text.replace(/\$/g, '').length == 0) {
                return false;
            }
        }
        if (!regexpr.test(key)) {
            return false;
        }
        else {
            return true;
        }
    };
};
