﻿//OfferSetup.js
isNewImplementationCustomers = function () {
    var customer = getCustomerNumber();
    return ([SPORTSKING, SCOTTS, ACE, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);
}

hidePreviewMailing = function () {
    var customer = getCustomerNumber();
    return ([SCOTTS, SPORTSKING, ANDERSEN, SHERWINWILLIAMS, ACE, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);
}

showNavBarDiv = function () {
    var customer = getCustomerNumber();
    return ([AAG, DCA, KUBOTA, SPORTSKING, GWA, SCOTTS, ACE, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) == -1);
}

hideOrderedLocations = function () {
    var customer = getCustomerNumber();
    return ([DCA, KUBOTA, SPORTSKING, GWA, SCOTTS, ANDERSEN, ACE, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);
}

showOfferSetupHint = function () {
    var customer = getCustomerNumber();
    return ([DCA, KUBOTA, SPORTSKING, GWA, SCOTTS, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);
}

isNewVariableFieldListCustomer = function () {
    var customer = getCustomerNumber();
    return ([SCOTTS, ACE, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);
}

showPreviewMailing = function () {
    var customer = getCustomerNumber();
    return ([SPORTSKING, KUBOTA, GWA, SCOTTS, ALLIED, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);
}