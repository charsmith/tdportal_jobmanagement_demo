﻿jQuery.support.cors = true;

//******************** Global Variables Start **************************
var serviceURL = "../jobApprovalCheckout/JSON/_approvalCheckout.JSON";
var gCheckoutServiceURL = serviceURLDomain + "api/CheckOut/";
//var gCheckoutServiceURL = serviceURLDomainInternal + "api/CheckOut/";
var gOutputServiceUrl = '../JSON/_jobSetupOutput.JSON';
var pageObj;
//******************** Global Variables End **************************

//******************** Page Load Events Start **************************

$(window).on('popupbeforeposition', 'div:jqmData(role="popup")', function () {
    var notDismissible = $(this).jqmData('dismissible') === false;
    if (notDismissible) {
        $('.ui-popup-screen').off();
    }
});

$('#_jobFinance').live('pagebeforecreate', function (event) {

    pageObj = new loadFinanceInfo();
    //displayQuantityConfirmDialog();
    displayMessage('_jobFinance');
    displayNavLinks();
    loadingImg("_jobFinance");
    $("#navLinksPanel").on("panelopen", function (event, ui) {
        sessionStorage.isNavPanelOpen = true;
    });
    if (jobCustomerNumber == CW) {
        setDemoHintsSliderValue();
        //window.setTimeout(function loadHints() {
        createDemoHints("jobSelectLocations");
        if (jobCustomerNumber == AAG && appPrivileges.roleName == "power") {
            $('#dvHeaderButtons div').prepend('<a href="#" data-rel="popup" data-position-to="window" data-transition="pop" data-role="button" data-icon="user" data-iconpos="notext" data-inline="true" data-mini="true" title="Loan Officers" data-bind="click:$root.showLoanOfficers">Loan Officers</a>');
        }
        // }, 100);
    }
});

$(document).on('pageshow', '#_jobFinance', function (event) {
    pageObj.makeGOutputData();
    persistNavPanelState();
    $('#sldrShowHints').slider("refresh");
    $('div[data-role=popup] div[data-role=header]').removeClass('ui-corner-top ui-header ui-bar-d ui-header-fixed slidedown ui-panel-fixed-toolbar ui-panel-animate ui-panel-page-content-position-left ui-panel-page-content-display-push ui-panel-page-content-open');
    $('div[data-role=popup] div[data-role=header]').addClass('ui-corner-top ui-header ui-bar-d');
});
