var map;
var gCrrtMarkersA = [];
var gCrrtsA;
var ctaLayer = null;
var defaultMapZoom = 9;
var infowindow;
var service;
var mapCenter;
var selectedDefaultZoom = 0;

var loadGoogleMap = function (self) {
    self.makeGoogleMap = function () {
        if (self.selectedLocation() != undefined && self.selectedLocation() != null && self.selectedLocation() != "") {
            var latitude = self.selectedLocationInfo().latitude();
            var longitude = self.selectedLocationInfo().longitude();

            $('#map').css('display', 'block');
            $('#map').show();

            mapCenter = (map == undefined || map == null || mapCenter == undefined) ? (new google.maps.LatLng(latitude, longitude)) : mapCenter;


            // if (map == undefined || map == null) {
            map = new google.maps.Map(document.getElementById('map'), {
                'zoom': defaultMapZoom,
                'center': mapCenter,
                'mapTypeId': google.maps.MapTypeId.ROADMAP,
                zoomControlOptions: { position: google.maps.ControlPosition.LEFT_BOTTOM },
                streetViewControlOptions: { position: google.maps.ControlPosition.LEFT_BOTTOM }
            });
            var store_id = self.selectedLocationZipsInfo().storeId;
            var contentString = (store_id != "") ? ("Store Id: " + store_id) : ($('#ddlLocations').find(":selected").text().substring(0, $('#ddlLocations').find(":selected").text().indexOf('-') - 1));

            infowindow = new google.maps.InfoWindow({
                content: contentString,
                maxWidth: 100
            });

            self.addStoreMarker(new google.maps.LatLng(latitude, longitude));
            if (Object.keys(tempAssignedZips).length > 0) {
                if (tempAssignedZips.locationsA.length > 0) {
                    var theInstructions = "return " + tempAssignedZips.locationsA;
                    var F = new Function(theInstructions);
                    self.addLocationMarkers(F());
                    //self.addLocationMarkers(tempAssignedZips.locationsA);
                }
            }
            //var county_selected = self.selectedCounty();
            //var kml_file_name = self.selectedLocationInfo().storeId() + ((county_selected != "") ? '_' + county_selected.replace(/ /g, '').toUpperCase() : "");
            //var kml_file = myProtocol + 'images.tribunedirect.com/lmaKml/_' + kml_file_name + '.kml';
            //ctaLayer = new google.maps.KmlLayer(String(kml_file), { suppressInfoWindows: false, preserveViewport: true });
            //ctaLayer.setMap(map);
            var isMobile = false;
            var mobile = ['iphone', 'ipad', 'ipod', 'android', 'blackberry', 'nokia', 'opera mini', 'windows mobile', 'windows phone', 'iemobile'];
            for (var i in mobile)
                if (navigator.userAgent.toLowerCase().indexOf(mobile[i].toLowerCase()) > 0) {
                    isMobile = true;
                }
            //if (isMobile) {
            //    document.getElementById("mapHolder").style.width = '90px';
            //}
            //google.maps.event.addDomListener(window, 'load', self.makeGoogleMap());
            google.maps.event.addDomListener(window, "resize", function () {
                //var height = $(window).height();
                //var width = $(window).width();
                mapVerticalResize();
                if (isMobile || !isMobile) {
                    if ($(window).width() > 414) {
                        map.setCenter(mapCenter);
                        map.setZoom(defaultMapZoom);
                    }
                    else {
                        map.setCenter(mapCenter);
                        map.setZoom(defaultMapZoom - 1);
                    }
                }
                    //else if (isMobile) {
                    //    if ($(window).width() > 414) {
                    //        map.setCenter(mapCenter);
                    //        map.setZoom(defaultMapZoom);
                    //    }
                    //    else {
                    //        map.setCenter(mapCenter);
                    //        map.setZoom(defaultMapZoom - 1);
                    //    }

                    //    //$('#map').css('height', '60% !important');
                    //} 
                else {
                    $('#dvMapMarker').find('img').css('margin-left', Math.round($('#mapHolder').width() / 100));
                    //document.getElementById("mapHolder").style.width = '98% !important'; //width + 'px';//width + 'px'; //
                }
                if (map != undefined && map != null) {
                    var center = map.getCenter();
                    google.maps.event.trigger(map, "resize");
                    map.setCenter(center);
                }
            });
            //window.setTimeout(function () {
            map.setZoom(defaultMapZoom);
            if (ctaLayer) {
                ctaLayer.setMap(null);
            }
            ctaLayer = null;
            //if ($('#chkViewLMA').is(':checked'))
            //    self.makeKmlFileName();
            //self.addLocationMarkers();
            //}, 400);
            if ($('#chkViewLMA').is(':checked'))
                self.makeKmlFileName();
        }
    };

    self.reloadTiles = function () {
        var tiles = $("#map-canvas").find("img");
        for (var i = 0; i < tiles.length; i++) {
            var src = $(tiles[i]).attr("src");
            if (/googleapis.com\/vt\?pb=/.test(src)) {
                var new_src = src.split("&ts")[0] + '&ts=' + (new Date()).getTime();
                $(tiles[i]).attr("src", new_src);
            }
        }
    };

    //window.onresize = self.makeGoogleMap();
    self.addStoreMarker = function (marker_point) {
        var marker = null;
        var image = new google.maps.MarkerImage('../images/maps/kubota_map_marker.png',
        // This marker is 59 pixels wide by 24 pixels tall.
        new google.maps.Size(29, 28),
        // The origin for this image is 0,0.
        new google.maps.Point(0, 0),
        // The anchor for this image is the point of the marker at 29,24.
        new google.maps.Point(14, 28));
        var shadow = new google.maps.MarkerImage('',
        // The shadow image is larger in the horizontal dimension
        // while the position and offset are the same as for the main image.
        new google.maps.Size(0, 0),
        new google.maps.Point(0, 0),
        new google.maps.Point(00, 0));
        var shape = {
            coord: [0, 11, 3, 3, 11, 0, 19, 0, 25, 4, 29, 10, 25, 18, 18, 20, 14, 28, 10, 20, 4, 17],
            type: 'poly'
        };

        marker = new google.maps.Marker({
            position: marker_point,
            map: map,
            shadow: shadow,
            icon: image,
            shape: shape,
            draggable: false,
            title: appPrivileges.companyName
        });
        google.maps.event.addListener(marker, 'click', function () {
            infowindow.open(map, marker);
        });
        //return marker;
    };

    self.addLocationMarkers = function (locationsA) {
        //locationsA = eval([['108^Kubota Chicago^', 41.89024, -87.62381579, 1], ['115^Kubota Chicago^', 41.83077287, -87.631802, 2]]);
        var locations_markers_A = [];
        var myLatLng = "";
        var info_window = new google.maps.InfoWindow({
            content: "",
            maxWidth: 100
        });
        for (var i = 0; i < locationsA.length; i++) {
            var location = locationsA[i];
            var marker = null;
            //switch (jobCustomerNumber) {
            //    case KUBOTA:
            //    case GWA:
                    //eval(getURLDecode(pagePrefs.scriptBlockDict.addStoreMarker155711));
                    var image = new google.maps.MarkerImage('../images/maps/kubota_neighboring_map_marker.png',
                    // This marker is 59 pixels wide by 24 pixels tall.
                    new google.maps.Size(26, 25),
                    // The origin for this image is 0,0.
                    new google.maps.Point(0, 0),
                    // The anchor for this image is the point of the marker at 29,24.
                    new google.maps.Point(13, 25));
                    var shadow = new google.maps.MarkerImage('',
                    // The shadow image is larger in the horizontal dimension
                    // while the position and offset are the same as for the main image.
                    new google.maps.Size(0, 0),
                    new google.maps.Point(0, 0),
                    new google.maps.Point(00, 0));
                    var shape = {
                        coord: [0, 9, 3, 3, 10, 0, 15, 0, 23, 4, 26, 9, 22, 15, 16, 18, 15, 24, 9, 18, 3, 15],
                        type: 'poly'
                    };
                    var content_string = "";
                    content_string = '<b>Store Id:</b> ' + ((location[0].indexOf('^') > -1) ? location[0].split('^')[0] : "");
                    content_string += '<br /><b>Store Name:</b> ' + ((location[0].indexOf('^') > -1 && location[0].split('^').length > 1) ? location[0].split('^')[1] : "");
                    //content_string += '<a href="#" onclick="ko.contextFor(this).$root.addRemoveZipsOnMap(\'Cook\',\'60004\',true);">Add</a><br /><a href="#" onclick="">Remove</a>';
                    myLatLng = new google.maps.LatLng(location[1], location[2]);
                    marker = new google.maps.Marker({
                        position: myLatLng,
                        map: map,
                        shadow: shadow,
                        icon: image,
                        shape: shape,
                        draggable: false,
                        //contentString: 'Store Id: ' + location[0],
                        contentString: content_string,
                        title: 'Kubota'
                    });
                //    break;
                //default:
                //    break;
            //}

            locations_markers_A.push(marker);

            google.maps.event.addListener(marker, 'click', function () {
                info_window.setContent(this.contentString);
                info_window.open(map, this);
            });
        }
        for (var k = 0; k < locations_markers_A.length; k++) {
            locations_markers_A[k].setMap(map);
        }

    };

};
