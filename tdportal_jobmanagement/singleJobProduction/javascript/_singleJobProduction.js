﻿jQuery.support.cors = true;

//******************** Global Variables Start **************************
var gData;
var gDataReports;
//var fileUploadURL = serviceURLDomain + 'api/FileUploadMw/' + getSessionData("publisherId");
var singleJobProductionJsonURL = 'JSON/_singleJobProduction.JSON';
var singleJobReportJsonURL = 'JSON/_singleJobReports.JSON';

//******************** Global Variables End **************************

//******************** Page Load Events Start **************************
$('#_singleJobProduction').live('pagebeforecreate', function (event) {
    //displayMessage('_singleJobProduction');
    //fnConfirmSignout('_singleJobProduction');

    $('#popupDialog').attr("data-rel", "dialog");
    $('#popupDialog').attr("data-dismissible", "false");
    //test for mobility...
    var is_mobile = false;
    mobile = ['iphone', 'ipod', 'android', 'blackberry', 'nokia', 'opera mini', 'windows mobile', 'windows phone', 'iemobile'];
    for (var i in mobile) if (navigator.userAgent.toLowerCase().indexOf(mobile[i].toLowerCase()) > 0) {
        is_mobile = true;
    }
    if (localStorage.mobile) { // desktop storage 
        is_mobile = true;
    }
    if (!is_mobile) {
        $('head').append('<link rel="stylesheet" type="text/css" href="../css/screen.css" media="screen" />');
    }

    if (sessionStorage.desktop) // desktop storage 
        $('head').append('<link rel="stylesheet" type="text/css" href="../css/screen.css" media="screen" />');
    else if (localStorage.mobile) // mobile storage
        return true;

    if ($.browser.msie && jQuery.browser.version.substr(0, 1) == '6') {
        alert("This page does not support Internet Explorer 6. Please consider downloading a newer browser. Click 'OK' to close");
        window.open('', '_self', '');
        window.close();
        return false;
    }
    //    if (appPrivileges.roleName != "admin")
    //        $("#divFileMap").hide();
    //load IFRAME for IE to upload files.
    //loadUploadIFrame()
    if (sessionStorage["headerValue"] != undefined && sessionStorage["headerValue"] != null && sessionStorage["headerValue"] != "")
        var headerVal = sessionStorage.headerValue;
    $("#headerH2").text(headerVal);

});

$(window).on('popupbeforeposition', 'div:jqmData(role="popup")', function () {
    var notDismissible = $(this).jqmData('dismissible') === false;
    if (notDismissible) {
        $('.ui-popup-screen').off();
    }
});

$(document).on('pageshow', '#_singleJobProduction', function (event) {
    // $('#divHeader').html("File Upload for " + appPrivileges.publisherName + " Publications <BR/");
    makeGData();
    makeGDataReports();
    //    $.getJSON(dropDownJsonURL, function (data) {
    //        dropDownData = data;
    //        loadDropDowns(dropDownData);
    //    });

});

//******************** Model Creation Start **************************

var singleJobSource = function (singleJobProduction, sourceType) {
    this.targetType = singleJobProduction.dir;
    this.sourceType = sourceType;
    this.fileData = ' Week ' + singleJobProduction.weekNumber + ' (' + singleJobProduction.inHomeDate + ')';
    this.fileSize = singleJobProduction.size;
    this.fileName = singleJobProduction.name;
}

function createViewModel() {
    var singleJob_model = {};
    var singleJobData_model = [];
    var singleJobArt_model = [];
    $.each(gData, function (key, val) {
        var temp_type = "";
        var temp_array = [];
        $.each(val.items, function (a, b) {
            if (b != "" && val.list == "data") {
                if (temp_type != "" && temp_type.toLowerCase() != b.dir.toLowerCase()) {
                    temp_array.push({
                        "listType": temp_type,
                        "items": singleJobData_model
                    });
                    singleJobData_model = [];
                }
                var singleJob = new singleJobSource(b, val.list);
                singleJobData_model.push(singleJob);
                temp_type = b.dir;
                if (val.items.length - 1 == a) {
                    temp_array.push({
                        "listType": temp_type,
                        "items": singleJobData_model
                    });
                    singleJobData_model = [];
                    singleJobData_model = temp_array;
                }
            }
            else if (b != "" && val.list == "artwork") {
                if (temp_type != "" && temp_type.toLowerCase() != b.dir.toLowerCase()) {
                    temp_array.push({
                        "listType": temp_type,
                        "items": singleJobArt_model
                    });
                    singleJobArt_model = [];
                }
                var singleJob = new singleJobSource(b, val.list);
                singleJobArt_model.push(singleJob);
                temp_type = b.dir;
                if (val.items.length - 1 == a) {
                    temp_array.push({
                        "listType": temp_type,
                        "items": singleJobArt_model
                    });
                    singleJobArt_model = [];
                    singleJobArt_model = temp_array;
                }
            }
        });
    });
    singleJob_model["data"] = singleJobData_model;
    singleJob_model["art"] = singleJobArt_model;
    return singleJob_model;
}

var SingleJobReports = function (reportType, items) {
    this.reportType = reportType;
    this.children = ko.observableArray(items);
}

function createViewModelReports() {
    var singleJobReport_model = [];
    $.each(gDataReports, function (key, val) {
        var singleJobReports = new SingleJobReports(val.list, val.weeks);
        singleJobReport_model.push(singleJobReports);

    });
    return singleJobReport_model;
}
//******************** Model Creation End **************************


//******************** Public Functions Start **************************
function makeGData() {
    //    getCORS(fileUploadURL, null, function (data) {
    //        gData = data;
    //        //ko.applyBindings(createViewModel(), $('#divList')[0]);
    //        $('#divList').empty();
    //        ko.cleanNode($('#divList')[0]);
    //        var model = createViewModel();
    //        ko.applyBindings(model, $('#divList')[0]);
    //        fillPublicationDDL();
    //        //displayList();
    //        $.each($('#divList'), function (obj) {
    //            $(this).collapsibleset();
    //        });
    //        $("#divList ul").each(function (i) {/// <reference path="../JSON/_reports.JSON" />

    //            $(this).listview();
    //        });
    //    }, function (error_response) {
    //        showErrorResponseText(error_response);
    //    });
    $.getJSON(singleJobProductionJsonURL, function (data) {
        gData = data;
        //ko.applyBindings(createViewModel(), $('#divList')[0]);
        $('#divData').empty();
        //ko.cleanNode($('#divList')[0]);
        var model = createViewModel();
        //alert('data' + model);
        ko.applyBindings(model.data, $('#ulData')[0]);
        ko.applyBindings(model.art, $('#ulArt')[0]);
        //alert('data2' + model);
        //fillPublicationDDL();
        //displayList();
        $("#ulData").listview('refresh');
        $("#ulArt").listview('refresh');
    });
}

function makeGDataReports() {
    //    getCORS(fileUploadURL, null, function (data) {
    //        gData = data;
    //        //ko.applyBindings(createViewModel(), $('#divList')[0]);
    //        $('#divList').empty();
    //        ko.cleanNode($('#divList')[0]);
    //        var model = createViewModel();
    //        ko.applyBindings(model, $('#divList')[0]);
    //        fillPublicationDDL();
    //        //displayList();
    //        $.each($('#divList'), function (obj) {
    //            $(this).collapsibleset();
    //        });
    //        $("#divList ul").each(function (i) {/// <reference path="../JSON/_reports.JSON" />

    //            $(this).listview();
    //        });
    //    }, function (error_response) {
    //        showErrorResponseText(error_response);
    //    });
    $.getJSON(singleJobReportJsonURL, function (reportsData) {
        gDataReports = reportsData;
        //ko.applyBindings(createViewModel(), $('#divList')[0]);
        $('#divData').empty();
        //ko.cleanNode($('#divList')[0]);
        var model = createViewModelReports();
        //alert('data' + model);
        ko.applyBindings(model, $('#ulProdReports')[0]);
        //displayList();
        $("#ulProdReports").listview('refresh');
    });
}

function postURLData(target_type, week_no, in_home_date, report_type) {
    var div_id = target_type + '_week' + week_no + '_' + in_home_date.replace(/\//g, '');
    window.location.href = ((report_type == "Production") ? '../reportsProduction/reportsProduction.html' : '../reportsMailTracker/reportsMailTracker.html') + '?' + div_id;
}
