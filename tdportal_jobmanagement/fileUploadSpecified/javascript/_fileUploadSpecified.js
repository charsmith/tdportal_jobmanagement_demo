﻿jQuery.support.cors = true;

//******************** Global Variables Start **************************
var gData;
//var fileUploadURL = serviceURLDomain + 'api/FileUploadMw/' + getSessionData("publisherId");

var fileUploadURL = 'JSON/_fileUploadSpecified.JSON';
var dropDownJsonURL = 'JSON/_dropDowns.JSON';
var dropDownData;
var pubDialogType = '';
//******************** Global Variables End **************************

//******************** Page Load Events Start **************************
$('#_plurisFileUpload').live('pagebeforecreate', function (event) {
    //displayMessage('_plurisFileUpload');
    //fnConfirmSignout('_plurisFileUpload');
    $('#popupDialog').attr("data-rel", "dialog");
    $('#popupDialog').attr("data-dismissible", "false");
    //test for mobility...
    var is_mobile = false;
    mobile = ['iphone', 'ipod', 'android', 'blackberry', 'nokia', 'opera mini', 'windows mobile', 'windows phone', 'iemobile'];
    for (var i in mobile) if (navigator.userAgent.toLowerCase().indexOf(mobile[i].toLowerCase()) > 0) {
        is_mobile = true;
    }
    if (localStorage.mobile) { // desktop storage 
        is_mobile = true;
    }
    if (!is_mobile) {
        $('head').append('<link rel="stylesheet" type="text/css" href="../css/screen.css" media="screen" />');
    }

    if (sessionStorage.desktop) // desktop storage 
        $('head').append('<link rel="stylesheet" type="text/css" href="../css/screen.css" media="screen" />');
    else if (localStorage.mobile) // mobile storage
        return true;

    if ($.browser.msie && jQuery.browser.version.substr(0, 1) == '6') {
        alert("This page does not support Internet Explorer 6. Please consider downloading a newer browser. Click 'OK' to close");
        window.open('', '_self', '');
        window.close();
        return false;
    }
    displayNavLinks();
    //load IFRAME for IE to upload files.
    loadUploadIFrame()
    var headerVal;
    if (sessionStorage["headerValue"] != null && sessionStorage["headerValue"] != undefined)
        $("#headerH2").text(sessionStorage["headerValue"]);
});

function loadUploadIFrame() {
    if ($.browser.msie) {
        var iframe_form = '<form enctype="multipart/form-data" method="post" id="formUpload" name="formUpload">';
        iframe_form += '<div id="spnFileInput" style="height: 0px; width: 0px; overflow: hidden;">';
        iframe_form += '<input type="file" id="mailFiles[]" name="mailFiles[]" />';
        iframe_form += '</div><div id="upload"></div></form>';
        var i = 0;
        var target = $(window.frames[0].frames.frameElement).contents()[0];
        target.open();
        target.write('<!doctype html><html><head></head><body></body></html>');
        target.close();
        $(window.frames[0].frames.frameElement).contents().find('body').html(iframe_form);
        $(window.frames[0].frames.frameElement).css('display', 'none');
    }
    else {
        $('#ieUploadframe').css('visibility', 'hidden');
    }
}


$(window).on('popupbeforeposition', 'div:jqmData(role="popup")', function () {
    var notDismissible = $(this).jqmData('dismissible') === false;
    if (notDismissible) {
        $('.ui-popup-screen').off();
    }
});

$(document).on('pageshow', '#_plurisFileUpload', function (event) {
    (($.browser.msie) ? $(window.frames[0].frames.frameElement).contents().find('input[type=file]') : $('input[type=file]')).bind('change', function (e) {
        selectFile($(e.target))
    });
    makeGData();
});

function getFile() {
    if ($.browser.msie)
        window.frames[0].document.getElementById('mailFiles[]').click();
    else
        document.getElementById("mailFiles[]").click();
}

//******************** Model Creation Start **************************

var FileUpload = function (target_type_name, children, are_files_uploaded, file_count) {
    this.targetName = target_type_name;
    this.dataTheme = (are_files_uploaded) ? 'g' : '';
    this.dataCollapsedIcon = (are_files_uploaded) ? 'check' : '';
    this.id = target_type_name.replace(/ /g, '');
    this.children = ko.observableArray(children);
    this.fileCount = file_count + ' of ' + children.length;
}

function createViewModel() {
    var upload_model = [];
    var are_files_missing = true;
    var file_count = 0;
    $.each(gData[0], function (key, val) {
        var missing_file_items = jQuery.grep(val, function (obj) {
            var is_missing_files = false;
            $.each(obj.files, function (a, b) {
                if (b.fileName != '') {
                    is_missing_files = false;
                }
                else {
                    is_missing_files = true;
                    return false;
                }
            });
            if (obj.files.length == 0) is_missing_files = true;
            return (is_missing_files);
        });
        file_count = 0;
        $.each(val, function (a, b) {
            var is_file_counted = false;
            if (b.files.length > 0) {
                for (var i = 0; i < b.files.length; i++) {
                    if (b.files[i].fileName != "") {
                        b["dataTheme"] = 'g';
                        //b["dataIcon"] = 'check';
                        b["dataIcon"] = 'delete';
                        if (!is_file_counted)
                            file_count++;
                        is_file_counted = true;
                    }
                    else {
                        b["dataTheme"] = '';
                        b["dataIcon"] = 'myapp-upload';
                    }
                }
            }
            else {
                b["dataTheme"] = '';
                b["dataIcon"] = 'myapp-upload';
            }

        });
        var file_upload = new FileUpload(key, val, ((missing_file_items.length > 0) ? false : true), file_count);
        upload_model.push(file_upload);
    });
    return upload_model;
}

//******************** Model Creation End **************************


//******************** Public Functions Start **************************
function makeGData() {
    //    getCORS(fileUploadURL, null, function (data) {
    //        gData = data;
    //        //ko.applyBindings(createViewModel(), $('#divList')[0]);
    //        $('#divList').empty();
    //        ko.cleanNode($('#divList')[0]);
    //        var model = createViewModel();
    //        ko.applyBindings(model, $('#divList')[0]);
    //        $.each($('#divList'), function (obj) {
    //            $(this).collapsibleset();
    //        });
    //        $("#divList ul").each(function (i) {
    //            $(this).listview();
    //        });
    //    }, function (error_response) {
    //        showErrorResponseText(error_response);
    //    });
    $.getJSON(fileUploadURL, function (data) {
        gData = data;
        //ko.applyBindings(createViewModel(), $('#divList')[0]);
        $('#divList').empty();
        ko.cleanNode($('#divList')[0]);
        var model = createViewModel();
        ko.applyBindings(model, $('#divList')[0]);
        $.each($('#divList'), function (obj) {
            $(this).collapsibleset();
        });
        $("#divList ul").each(function (i) {
            $(this).listview();
        });
    });


}

function fnpopUp(text, target_type) {
    $("#popupFileUpload").popup("open");
    $("#headerMsg").html(text);
    $('#hidUploadFile').val(text.substring(5, 6) + '|' + target_type);
}

function fnRemoveFilesPopUp(text, files, target_type, data_icon) {
    var files_list = jQuery.parseJSON(files);
    var fileExists = false;
    $('#hidUploadFile').val(text.substring(5, 6) + '|' + target_type);
    $("#ddlFilesList").empty();
    if (data_icon.toLowerCase() == "delete")
        fileExists = true;
    $("#dropDownTemplate").tmpl(files_list).appendTo("#ddlFilesList");
    $("#ddlFilesList").selectmenu('refresh');
    if (fileExists)
        $("#popupRemoveFileUpload").popup("open");
    $("#headerRemoveMsg").html(text);
}

function fnCancel() {
    resetFileUploadForm();
    $('#popupFileUpload').popup('close');
}


function fnRemoveCancel() {
    $('#popupRemoveFileUpload').popup('close');
    return false;
}


function fnUpdatePublicationInfo(file_name, product_key, text) {
    var gdata_publication = gData[product_key];
    var index = 0;
    var gdata_currentbuy = jQuery.grep(gdata_publication, function (obj, key) {
        index = key;
        if (obj.fileType.toLowerCase() === text.toLowerCase()) {
            obj.origFileName = file_name;
            var date = new Date();
            obj.note = 'Successfully Loaded ' + date.getMonth() + "/" + date.getDay() + "/" + date.getFullYear();
        }
    });
    makeGData();
}

function clearFields() {
    $('#txtFileName').val('');
}

var isComplete = false;
var xhr_req;
var is_aborted = false;
var percent_uploaded = 0;
function formSubmit(e) {
    var input_file = $(($.browser.msie) ? $(window.frames[0].document.getElementById('formUpload')) : $('#formUpload')).find('input[type="file"]');
    number_of_files_uploading = input_file.length;
    isComplete = false;
    var start = '';
    var file_name = $(input_file).val();
    file_name = file_name.substring(file_name.lastIndexOf('\\') + 1);
    var publication_info = $('#hidUploadFile').val().split('|');
    e.preventDefault();
    var timer_id = 0;
    var time_stamp = Math.floor(+new Date().getTime() / 1000);
    var increment = 1;
    var timer = (function () {
        var timerElement;
        var timeoutRef;
        var percent_uploaded = 0;
        return {
            start: function (id) {
                if (id) {
                    timerElement = document.getElementById(id);
                }
                timer.run();
            },
            run: function () {
                if (timerElement) {
                    $('#dvProgressBar').popup('open', { positionTo: 'window' });
                    $('#fileName').text(number_of_files_uploading);
                    //Sets the style of progress bar and displays the percent uploaded
                    $('#uploadProgressBar').css('width', percent_uploaded + '%');
                    $('#percentUploaded').empty();
                    $('#percentUploaded').get(0).innerHTML = percent_uploaded + '%';
                }

                if (!isComplete) {
                    timeoutRef = setTimeout(timer.run, 500); //This run function will be called for every 2 secs.

                    //Web service call for progress bar data
                    var pbar_data_url = serviceURLDomain + "api/FileUpload/progress/" + sessionStorage.jobNumber + "^" + sessionStorage.username + "^" + time_stamp + "^1";

                    getCORS(pbar_data_url, null, function (data) {
                        if (typeof (data) != "object" && parseInt(data) != NaN) {
                            if (percent_uploaded < 100)
                                percent_uploaded = JSON.stringify(data); //assigns the values returned from web service.                        
                        }
                        else {
                            showErrorResponseText(data,true);
                        }
                    }, function (error_response) {
                        showErrorResponseText(error_response,true);
                    });
                }
                else {
                    window.clearTimeout(timeoutRef);
                    $('#dvProgressBar').popup('close');
                    if (!is_aborted) {
                        var text = publication_info[0];
                        var product_key = publication_info[1];
                        fnUpdatePublicationInfo(file_name, product_key, text);
                        resetFileUploadForm();
                        var msg = "Your files have been successfully uploaded.";
                        //$('#dialogbox').prepend('<a id="btnClose" href="#" data-role="button" data-theme="a" data-icon="delete" data-iconpos="notext" class="ui-btn-right" onclick="removeCloseButton();">Close</a>');
                        //$('#okBut').hide();
                        $('#alertmsg').text(msg);
                        $('#okBut').attr('onclick', "closeMessagePopup();");
                        $('#popupDialog').popup('open');
                        $('#popupDialog a').each(function (i) {
                            $(this).button();
                        });
                    }
                    is_aborted = false;
                    return true;
                }
            }
        }
    } ());

    //Submitting the form to upload
    var form_obj = ($.browser.msie) ? $(window.frames[0].document.getElementById('formUpload')) : $('#formUpload');
    $(form_obj).attr('action', serviceURLDomain + 'api/FileUpload/' + sessionStorage.jobNumber + '^' + sessionStorage.username + '^' + time_stamp + '^1/' + file_name + '^1');
    var form = $(form_obj).ajaxSubmit(
                                        {
                                            type: 'post',
                                            beforeSend: function (xhr) {
                                                start = new Date().getTime();
                                                form_xhr = xhr;
                                                $('#popupFileUpload').popup('close');
                                                $('#dvProgressBar').popup('open', { positionTo: 'window' });
                                                timer.start('uploadProgressBar'); //Starts the timer just before the upload starts
                                                xhr.setRequestHeader("Authorization", sessionStorage.authString);
                                            },
                                            complete: function (xhr) {
                                                isComplete = true;
                                                if ($.browser.msie) {
                                                    var status_url = serviceURLDomain + "api/FileUpload/error/" + sessionStorage.jobNumber + "^" + sessionStorage.username + "^" + time_stamp + "^1";
                                                    getCORS(status_url, null, function (data) {
                                                        var status = data;
                                                        if (typeof (status) != "object" && status != undefined && status != null && status != "\"\"") {
                                                            is_aborted = true;
                                                            resetFileUploadForm();
                                                            $('#dvProgressBar').popup('close');
                                                            $('#alertmsg').text(status);
                                                            $('#okBut').attr('onclick', "closeMessagePopup();");
                                                            $('#popupDialog').popup('open');
                                                        } else if (typeof (status) != "object" && status == "\"\"" && !is_aborted) {
                                                            uploadingFiles = "";
                                                        }
                                                        else {
                                                            is_aborted = true;
                                                            showErrorResponseText(status,true);
                                                        }
                                                    }, function (error_response) {
                                                        is_aborted = true;
                                                        $('#dvProgressBar').popup('close');
                                                        //$('#alertmsg').text(error_response.responseText);
                                                        showErrorResponseText(error_response, false);
                                                        $('#okBut').attr('onclick', "closeMessagePopup();");
                                                        //$('#popupDialog').popup('open');
                                                    });
                                                }
                                                else {
                                                    if (xhr.statusText.toLowerCase() == "ok" || xhr.statusText.toLowerCase() == "n/a") {
                                                        uploadingFiles = "";
                                                    }
                                                }
                                                $('#dvProgressBar').popup('close');
                                            },
                                            error: function (error_response) {
                                                if (!($.browser.msie)) {
                                                    isComplete = true;
                                                    is_aborted = true;
                                                    resetFileUploadForm();
                                                    $('#dvProgressBar').popup('close');
                                                    // $('#alertmsg').text(error_response.responseText);
                                                    showErrorResponseText(error_response, false);
                                                    $('#okBut').attr('onclick', "closeMessagePopup();");
                                                   // $('#popupDialog').popup('open');
                                                }
                                            }
                                        });
}

function resetFileUploadForm() {
    var form_obj = ($.browser.msie) ? $(window.frames[0].document.getElementById('formUpload')) : $('#formUpload');
    var fileInput = $(form_obj).find('input[type=file]')[0];
    if (fileInput.value != "") {
        // Create a new file input
        var newFileInput = fileInput.cloneNode(true);
        newFileInput.value = null;
        $(form_obj).find('input[type=file]').remove();
        var spn = $(form_obj).find('#spnFileInput');
        $(spn).append(newFileInput);
        $($(form_obj).find('input[type=file]')).bind('change', function (e) {
            selectFile($(e.target));
        });
    }
    $('#txtFileName').val('');
}

function closeMessagePopup() {
    $('#popupDialog').popup('open');
    $('#popupDialog').popup('close');
}

//Cancels the file upload.
function cancelUpload() {
    is_aborted = true;
    form_xhr.abort();
}

//Browse to specific folder
function selectFile(ctrl) {
    var file_name = $(ctrl).val();
    var selected_file_name = $(ctrl).val().substring($(ctrl).val().lastIndexOf('\\') + 1, $(ctrl).val().length);
    var myRegexp = /^([a-zA-Z0-9_\.\-\^\ ])+$/ig;
    if (!myRegexp.test(selected_file_name.substring(0, selected_file_name.lastIndexOf('.')))) {
        $('#popupFileUpload').popup('close');
        $('#txtFileName').val('');
        $('#popupListDefinition').popup('close');
        $('#alertmsg').text('Illegal character in file name. Uploading cannot continue.');
        $('#popupDialog').popup('open');
        return false;
    }
    else {
        var file_name_with_ext = file_name.substring(file_name.lastIndexOf('\\') + 1);
        var extension = file_name_with_ext.substring(file_name_with_ext.lastIndexOf('.') + 1);
        var file_name_without_ext = file_name_with_ext.substring(0, file_name_with_ext.lastIndexOf('.'));
        var allowedExtension = ["CFS", "CSV", "DAT", "DATA", "GZ", "TXT", "ZIP"];
        var isValidFileFormat = false;
        var uploading_file_name = "";
        if (typeof (extension) != 'undefined') {
            for (var i = 0; i < allowedExtension.length; i++) {
                if (extension != undefined && extension.toLowerCase() == allowedExtension[i].toLowerCase()) {
                    isValidFileFormat = true;
                    break;
                }
            }
            if (!isValidFileFormat) {
                resetFileUploadForm();
                $('#txtFileName').val('');
                $('#popupFileUpload').popup('close');
                $('#alertmsg').text('Invalid File Type...Only "CFS", "CSV", "DAT", "DATA", "GZ", "TXT" and "ZIP" types are accepted.');
                $('#popupDialog').popup('open');
                return true;
            }
        }
        $('#txtFileName').val(file_name);
    }
}


function uploadFile(e) {
    if (!validateUploadParameters()) return false;
    //sessionStorage["uploadEMail"] = $("#txtEmail").val();
    var target_type_info = $('#hidUploadFile').val().split('|');
    var gdata_fileUpload = gData[0][target_type_info[1]];
    //var index = 0;
    var file_name = $('#txtFileName').val().substring($('#txtFileName').val().lastIndexOf('\\') + 1, $('#txtFileName').val().length);
    var date = new Date();
    var gdata_upload_to = jQuery.grep(gdata_fileUpload, function (obj) {
        if (obj.weekNumber === target_type_info[0]) {
            if (obj.files[0].fileName == "") {
                obj.files = [];
            }
            obj.files = (obj.files != "") ? obj.files : [];
            obj.files.push({
                fileName: file_name,
                uploaded: "11/26/2013",
                uploadedBy: "admin"
            });
            return obj;
        }
    });
    //gdata_publication.push(gdata_upload_to);
    // var post_url = serviceURLDomain + "api/FileUploadMw/" + getSessionData("publisherId") + "/" + file_name + "/bob";

    //    postCORS(post_url, JSON.stringify(gdata_upload_to[0]), function (response) {
    //        if (response.replace(/"/g, "") == "success") {
    //            formSubmit(e)
    //        }
    //        else {
    //            $('#alertmsg').text(response_error.responseText);
    //            $('#popupDialog').popup('open');
    //            return false;
    //        }
    //    }, function (response_error) {
    //        $('#alertmsg').text(response_error.responseText);
    //        $('#popupDialog').popup('open');
    //    });
    // displayList();

    //temp Solution
    $('#divList').empty();
    ko.cleanNode($('#divList')[0]);
    //gData = gData[0][target_type_info[1]].push(gdata_publication);
    var model = createViewModel();
    ko.applyBindings(model, $('#divList')[0]);
    $.each($('#divList'), function (obj) {
        $(this).collapsibleset();
    });
    $("#divList ul").each(function (i) {
        $(this).listview();
    });
    resetFileUploadForm();
    //makeGData(); Delete above code and open this
}

function removeFile(e) {
    $('#popupRemoveFileUpload').popup('close');
    var target_type = $('#hidUploadFile').val().split('|');
    var file_selected = $('#ddlFilesList').val();
    //var gdata_publication = gData[0][target_type[1]];
    var file_index = 0;
    var gdata_upload_to = jQuery.grep(gData[0][target_type[1]], function (obj) {
        $.each(obj.files, function (a, b) {
            if (b.fileName === file_selected && obj.weekNumber === target_type[0]) {
                file_index = a + 1;
                return false;
            }
        });
    });
    if (file_index > 0)
        gData[0][target_type[1]][target_type[0] - 1].files.splice(file_index - 1, 1);
    //gdata_publication.push(gdata_upload_to);
    // var post_url = serviceURLDomain + "api/FileUploadMw/" + getSessionData("publisherId") + "/" + file_name + "/bob";

    //    postCORS(post_url, JSON.stringify(gdata_upload_to[0]), function (response) {
    //        if (response.replace(/"/g, "") == "success") {
    //            formSubmit(e)
    //        }
    //        else {
    //            $('#alertmsg').text(response_error.responseText);
    //            $('#popupDialog').popup('open');
    //            return false;
    //        }
    //    }, function (response_error) {
    //        $('#alertmsg').text(response_error.responseText);
    //        $('#popupDialog').popup('open');
    //    });
    // displayList();

    //temp Solution
    $('#divList').empty();
    ko.cleanNode($('#divList')[0]);
    //gData = gData[0][target_type_info[1]].push(gdata_publication);
    var model = createViewModel();
    ko.applyBindings(model, $('#divList')[0]);
    $.each($('#divList'), function (obj) {
        $(this).collapsibleset();
    });
    $("#divList ul").each(function (i) {
        $(this).listview();
    });

    //makeGData(); Delete above code and open this
}

function removeCloseButton() {
    $('#btnClose').remove();
    $('#okBut').show();
    $('#popupDialog').popup('close');
}

function validateUploadParameters() {
    var message = "";
    var form_obj = ($.browser.msie) ? $(window.frames[0].document.getElementById('formUpload')) : $('#formUpload');
    if ($(form_obj).find('input[type=file]').val() == "" || $(form_obj).find('input[type=file]').val() == null) {
        message = "Please select a file to upload";
    }
    $('#popupFileUpload').popup('close');
    if (message != "") {
        $('#alertmsg').html(message);
        $("#okBut").attr("onclick", "$('#popupDialog').popup('close'); $('#popupFileUpload').popup('open');");
        $('#popupDialog').popup('open');
        return false;
    }
    return true;
}
//******************** Public Functions End **************************
