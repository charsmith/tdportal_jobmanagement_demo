//------- START GLOBAL -------
var map;
var gCrrtMarkersA = [];
var gCrrtsA;
var ctaLayer;
var defaultMapZoom = 11;
var infowindow;
var service;
var mapCenter;
var isMapMove = false;
var myCircle = null;
var isPolygonEdited = false;
//if (this.wantsCrrtMarkers) {
//    //=this.mapLatLongExporter.makeCrrtMarkerJsArray() 
//}
var selectedDefaultZoom = 0;
//------- END GLOBAL -------

var drawingManager;
var selectedColor;
var colorButtons = {};
var selectedShape;
var colors = ['#1E90FF', '#FF1493', '#32CD32', '#FF8C00', '#4B0082'];
//var colorButtons = {};

function clearSelection() {
    if (selectedShape) {
        selectedShape.setEditable(false);
        selectedShape = null;
    }
}
function setSelection(shape, event) {
    clearSelection();
    selectedShape = shape;
    shape.setEditable(true);
    shape.getPaths().forEach(function (path) {

        google.maps.event.addListener(path, 'insert_at', function () {
            // New point
            //alert('New Point Added');
            var temp_ver = (event.overlay) ? google.maps.geometry.encoding.encodePath(event.overlay.getPath()) : google.maps.geometry.encoding.encodePath(selectedShape.getPath().getArray());
            var peri_targets = $('#spnPheripheralTargets').text();
            if (gSelectedZips.polygonInfo != temp_ver && parseInt(peri_targets) > 0) {
                validateTargetsWithPolygon(shape, event, "edit");
            } else {
                isPolygonEdited = true;
                showArrays(event);
            }
        });

        google.maps.event.addListener(path, 'remove_at', function () {
            //alert('Point Deleted');
            showArrays(event);
        });

        google.maps.event.addListener(path, 'set_at', function () {
            if (!isPolygonEdited) {
                // Point was moved
                //alert('polygon edited');
                //polygon.setMap(map);
                var temp_ver = (event.overlay) ? google.maps.geometry.encoding.encodePath(event.overlay.getPath()) : google.maps.geometry.encoding.encodePath(selectedShape.getPath().getArray());
                var peri_targets = $('#spnPheripheralTargets').text();
                if (gSelectedZips.polygonInfo != temp_ver && parseInt(peri_targets) > 0) {
                    isPolygonEdited = true;
                    validateTargetsWithPolygon(shape, event, "edit");
                } else {
                    saveMapState();
                    isPolygonEdited = true;
                    showArrays(event);
                }
            }
        });

    });
    //selectColor(shape.get('fillColor') || shape.get('strokeColor'));
}
function saveMapState() {
    var mapZoom = map.getZoom();
    var mapCentre = map.getCenter();
    var mapLat = mapCentre.lat();
    var mapLng = mapCentre.lng();
    var cookiestring = mapLat + "_" + mapLng + "_" + mapZoom;
    setCookie("myMapCookie", cookiestring, 30);
}

function loadMapState() {
    var gotCookieString = getCookie("myMapCookie");
    var splitStr = gotCookieString.split("_");
    var savedMapLat = parseFloat(splitStr[0]);
    var savedMapLng = parseFloat(splitStr[1]);
    var savedMapZoom = parseFloat(splitStr[2]);
    if ((!isNaN(savedMapLat)) && (!isNaN(savedMapLng)) && (!isNaN(savedMapZoom))) {
        map.setCenter(new google.maps.LatLng(savedMapLat, savedMapLng));
        map.setZoom(savedMapZoom);
    }
}

function setCookie(c_name, value, exdays) {
    var exdate = new Date();
    exdate.setDate(exdate.getDate() + exdays);
    var c_value = escape(value) + ((exdays == null) ? "" : "; expires=" + exdate.toUTCString());
    document.cookie = c_name + "=" + c_value;
}

function getCookie(c_name) {
    var i, x, y, ARRcookies = document.cookie.split(";");
    for (i = 0; i < ARRcookies.length; i++) {
        x = ARRcookies[i].substr(0, ARRcookies[i].indexOf("="));
        y = ARRcookies[i].substr(ARRcookies[i].indexOf("=") + 1);
        x = x.replace(/^\s+|\s+$/g, "");
        if (x == c_name) {
            return unescape(y);
        }
    }
    return "";
}

function validateTargetsWithPolygon(shape, event, shape_mode) {
    var msg = "Changes to the Polygon will clear all Peripheral Targets selected.";
    $('#popupradiusZipCRRTConfirmDialog').find('a[id=okBut1]').bind('click', function () {
        //$('#spnPheripheralTargets').html(0);
        //$('#spnTotalTargets').html(0);
        $('#popupradiusZipCRRTConfirmDialog').find('a[id=okBut1]').unbind('click');
        $('#popupradiusZipCRRTConfirmDialog').popup('open');
        $('#popupradiusZipCRRTConfirmDialog').popup('close');
        $('#waitPopUp').popup('open', { positionTo: 'window' });
        window.setTimeout(function () {
            if (shape_mode == "delete") {
                selectedShape.setMap(null);
                selectedShape = null;
                $('#btnPolygon').attr('disabled', false);
                $('#btnPolygon').removeClass('ui-disabled');
            }
            else {
                var temp_ver = google.maps.geometry.encoding.encodePath(selectedShape.getPath().getArray());
                var temp_store_list_selection = (sessionStorage.storeListSelection != undefined && sessionStorage.storeListSelection != null && sessionStorage.storeListSelection != null) ? $.parseJSON(sessionStorage.storeListSelection) : [];
                if (Object.keys(temp_store_list_selection).length > 0 || temp_store_list_selection.length > 0) {
                    temp_store_list_selection["polygonInfo"] = temp_ver;
                    sessionStorage.storeListSelection = JSON.stringify(temp_store_list_selection);
                }
                sessionStorage.removeItem('updatedCRRTs');
                getSelectedStoreData();
            }
        }, 1000);
    });
    $('#popupradiusZipCRRTConfirmDialog').find('a[id=cancelBut1]').bind('click', function () {
        $('#popupradiusZipCRRTConfirmDialog').popup('open');
        $('#popupradiusZipCRRTConfirmDialog').popup('close');
        $('#popupradiusZipCRRTConfirmDialog').find('a[id=cancelBut1]').unbind('click');
        if (shape_mode != "delete") {
            selectedShape.setMap(null);
            var polygon = new google.maps.Polygon({
                paths: google.maps.geometry.encoding.decodePath(gSelectedZips.polygonInfo),
                strokeColor: '#FF0000',
                strokeOpacity: 0.8,
                strokeWeight: 2,
                fillColor: '#2f51fd',
                fillOpacity: 0.4
            });
            polygon.setMap(map);
            google.maps.event.addListener(polygon, 'click', function (e) {
                setSelection(polygon, e);
            });
            //polygon.setEditable(true);
        }
        return false;
    });
    $('#popupradiusZipCRRTConfirmDialog').find('div[id=confirmMsg1]').text(msg);
    $('#popupradiusZipCRRTConfirmDialog').popup('open');
    return false;
}

function deleteSelectedShape() {
    if (selectedShape) {
        var temp_ver = google.maps.geometry.encoding.encodePath(selectedShape.getPath().getArray());
        var peri_targets = $('#spnPheripheralTargets').text();
        if (gSelectedZips.polygonInfo != temp_ver && parseInt(peri_targets) > 0) {
            validateTargetsWithPolygon(selectedShape, event);
        } else {
            selectedShape.setMap(null);
            //        drawingManager.setOptions({
            //            drawingControl: true
            //        });
            selectedShape = null;
            $('#btnDeletePolygon')[0].disabled = true;
            $('#btnDeletePolygon').attr('disabled', true);
            $('#btnDeletePolygon').addClass('ui-disabled');
            $('#btnMovePolygon')[0].disabled = true;
            $('#btnMovePolygon').attr('disabled', true);
            $('#btnMovePolygon').addClass('ui-disabled');
            $('#btnPolygon').attr('disabled', false);
            $('#btnPolygon').removeClass('ui-disabled');
            $('#btnPolygon').trigger('click');
            $('#btnSaveSelections').addClass('ui-disabled');
            $('#btnSaveSelections')[0].disabled = true;

            $('#btnSaveTop').addClass('ui-disabled');
            $('#btnSaveTop')[0].disabled = true;
            $('#btnSave').addClass('ui-disabled');
            $('#btnSave')[0].disabled = true;

            $('#btnRevert').removeClass('ui-disabled');
            $('#btnRevert')[0].disabled = false;

            $('#btnModelSave')[0].disabled = true;
            $('#btnModelSave').attr('disabled', true);
            $('#btnModelSave').addClass('ui-disabled');

            $('#saveNavBut').addClass('ui-disabled');
            $('#saveNavBut')[0].disabled = true;

            gSelectedZips.polygonInfo = "";
        }
    }
    else {
        $('#alertmsg').html('Please select a shape to clear');
        $('#popupDialog').popup('open');
    }
}
function selectColor(color) {
    selectedColor = color;
    //for (var i = 0; i < colors.length; ++i) {
    //    var currColor = colors[i];
    //    colorButtons[currColor].style.border = currColor == color ? '2px solid #789' : '2px solid #fff';
    //}

    // Retrieves the current options from the drawing manager and replaces the
    // stroke or fill color as appropriate.
    var polylineOptions = drawingManager.get('polylineOptions');
    polylineOptions.strokeColor = color;
    drawingManager.set('polylineOptions', polylineOptions);

    var rectangleOptions = drawingManager.get('rectangleOptions');
    rectangleOptions.fillColor = color;
    drawingManager.set('rectangleOptions', rectangleOptions);

    var circleOptions = drawingManager.get('circleOptions');
    circleOptions.fillColor = color;
    drawingManager.set('circleOptions', circleOptions);

    var polygonOptions = drawingManager.get('polygonOptions');
    polygonOptions.fillColor = color;
    drawingManager.set('polygonOptions', polygonOptions);
}
//function homeControl(controlDiv, map) {
//    // Set CSS for the control border.
//    var controlUI = document.createElement('div');
//    controlUI.style.backgroundColor = '#ffffff';
//    controlUI.style.borderStyle = 'solid';
//    controlUI.style.borderWidth = '1px';
//    controlUI.style.borderColor = '#ccc';
//    controlUI.style.height = '23px';
//    controlUI.style.marginTop = '5px';
//    controlUI.style.marginLeft = '-6px';
//    controlUI.style.paddingTop = '1px';
//    controlUI.style.cursor = 'pointer';
//    controlUI.style.textAlign = 'center';
//    controlUI.title = 'Click to clear the polygon';
//    controlUI.id = "dvClearShape";
//    controlDiv.appendChild(controlUI);

//    // Set CSS for the control interior.
//    var controlText = document.createElement('div');
//    controlText.style.fontFamily = 'Arial,sans-serif';
//    controlText.style.fontSize = '10px';
//    controlText.style.paddingLeft = '4px';
//    controlText.style.paddingRight = '4px';
//    //controlText.style.marginTop = '-8px';
//    controlText.innerHTML = 'Clear';
//    controlUI.appendChild(controlText);

//    // Setup the click event listeners: simply set the map to Chicago.
//    google.maps.event.addDomListener(controlUI, 'click', function () {
//        deleteSelectedShape();
//    });
//}
/** @this {google.maps.Polygon} */
function showArrays(event) {

    // Since this polygon has only one path, we can call getPath()
    // to return the MVCArray of LatLngs.
    // var vertices = this.getPath();
    var vertices = (event.overlay) ? event.overlay.getPath().getArray() : selectedShape.getPath().getArray();
    var temp_ver = (event.overlay) ? google.maps.geometry.encoding.encodePath(event.overlay.getPath()) : google.maps.geometry.encoding.encodePath(selectedShape.getPath().getArray());
    var polygon_pts = "";
    // Iterate over the vertices.
    for (var i = 0; i < vertices.length ; i++) {
        var xy = vertices[i];
        polygon_pts += '<br>' + 'Coordinate ' + i + ':<br>' + xy.lat() + ',' +
            xy.lng();
    }

    // Replace the info window's content and position.
    //infowindow.setContent(contentString);
    //infowindow.setPosition(mapCenter);

    //infowindow.open(map);
    //$('#alertmsg').html(polygon_pts);
    //$('#popupDialog').popup('open');
    $('#waitPopUp').popup('open', { positionTo: 'window' });
    window.setTimeout(function () {
        var temp_store_list_selection = (sessionStorage.storeListSelection != undefined && sessionStorage.storeListSelection != null && sessionStorage.storeListSelection != null) ? $.parseJSON(sessionStorage.storeListSelection) : [];
        if (Object.keys(temp_store_list_selection).length > 0 || temp_store_list_selection.length > 0) {
            temp_store_list_selection["polygonInfo"] = temp_ver;
            sessionStorage.storeListSelection = JSON.stringify(temp_store_list_selection);
        }

        sessionStorage.removeItem('updatedCRRTs');
        getSelectedStoreData();
    }, 1000);
}

function drawPolygon() {
    drawingManager.setDrawingMode(google.maps.drawing.OverlayType.POLYGON);
};

function movePolygon() {
    drawingManager.setDrawingMode(null);
}

//Initializes the map object when page loads and Updates the map with current values at subsequent changes.
function makeGoogleMap1() { }
function makeGoogleMap() {

    if (typeof selectedStoreId == 'undefined' || selectedStoreId == undefined || Object.keys(gSelectedZips).length == 0) {
        return false;
    }

    var latitude = ($('#ulSelectedCompetitor li').length > 0) ? gSelectedZips.compLatitude : gSelectedZips.latitude;
    var longitude = ($('#ulSelectedCompetitor li').length > 0) ? gSelectedZips.compLongitude : gSelectedZips.longitude;

    mapCenter = (map == undefined || map == null || mapCenter == undefined) ? (new google.maps.LatLng(latitude, longitude)) : mapCenter;

    // if (map == undefined || map == null) {
    map = new google.maps.Map(document.getElementById('map'), {
        'zoom': defaultMapZoom,
        'center': mapCenter,
        'mapTypeId': google.maps.MapTypeId.ROADMAP,
        zoomControlOptions: { position: google.maps.ControlPosition.LEFT_BOTTOM },
        streetViewControlOptions: { position: google.maps.ControlPosition.LEFT_BOTTOM }
    });

    if ($("input[name=rdoCounts]:checked").val() == "p") {
        //if (drawingManager) {
        //    toggleDrawingManager(true);
        //}
        //else {
        createDrawingManager();
        //}
        var temp_store_list_selection = (sessionStorage.storeListSelection != undefined && sessionStorage.storeListSelection != null && sessionStorage.storeListSelection != null) ? $.parseJSON(sessionStorage.storeListSelection) : [];
        //if (Object.keys(temp_store_list_selection).length > 0 || temp_store_list_selection.length > 0) {
        //if (temp_store_list_selection.polygonInfo != undefined && temp_store_list_selection.polygonInfo != null && temp_store_list_selection.polygonInfo != "" && temp_store_list_selection.polygonInfo.length > 0) {
        if (gSelectedZips.polygonInfo != undefined && gSelectedZips.polygonInfo != null && gSelectedZips.polygonInfo != "" && gSelectedZips.polygonInfo.length > 0) {
            //var triangleCoords = [];
            if (gSelectedZips.wantsRadius == 1) {
                var polygon = new google.maps.Polygon({
                    paths: google.maps.geometry.encoding.decodePath(gSelectedZips.polygonInfo),
                    strokeColor: '#FF0000',
                    strokeOpacity: 0.8,
                    strokeWeight: 2,
                    fillColor: '#2f51fd',
                    fillOpacity: 0.4
                });
                //drawingManager.setPolygonOptions
                //paths: google.maps.geometry.encoding.decodePath(temp_store_list_selection.polygonInfo),

                drawingManager.setDrawingMode(null);
                //                google.maps.event.addListener(drawingManager, 'overlaycomplete', function (e) {
                //                    if (e.type != google.maps.drawing.OverlayType.MARKER) {
                //                        var newShape = e.overlay;
                //                        newShape.type = e.type;
                //                        google.maps.event.addListener(newShape, 'click', function () {
                //                            setSelection(newShape, e);
                //                        });
                //                        showArrays(e);
                //                    }
                //                });
                $('#rdoPolygon').attr('checked', true).checkboxradio('refresh').trigger('change');

                $('#btnPolygon').attr('disabled', true);
                $('#btnPolygon').addClass('ui-disabled');
                drawingManager.setDrawingMode(null);
                polygon.setMap(map);
                google.maps.event.addListener(polygon, 'click', function (e) {
                    setSelection(polygon, e);
                });
                temp_store_list_selection = (sessionStorage.storeListSelection != undefined && sessionStorage.storeListSelection != null && sessionStorage.storeListSelection != null) ? $.parseJSON(sessionStorage.storeListSelection) : [];
                if (Object.keys(temp_store_list_selection).length > 0 || temp_store_list_selection.length > 0 && temp_store_list_selection.polygonInfo != undefined && temp_store_list_selection.polygonInfo != null && temp_store_list_selection.polygonInfo != "") {
                    //delete temp_store_list_selection["polygonInfo"];
                }
                sessionStorage.storeListSelection = JSON.stringify(temp_store_list_selection);
                //bindInfoWindow(polygon[1], map, infoWindow, "hello");

                //// Define the LatLng coordinates for the polygon's path.
                //var triangleCoords = [
                //  new google.maps.LatLng(25.774252, -80.190262),
                //  new google.maps.LatLng(18.466465, -66.118292),
                //  new google.maps.LatLng(32.321384, -64.75737),
                //  new google.maps.LatLng(25.774252, -80.190262)
                //];

                //// Construct the polygon.
                //bermudaTriangle = new google.maps.Polygon({
                //    paths: triangleCoords,
                //    strokeColor: '#FF0000',
                //    strokeOpacity: 0.8,
                //    strokeWeight: 2,
                //    fillColor: '#FF0000',
                //    fillOpacity: 0.35
                //});

                //bermudaTriangle.setMap(map);
            }
        }
        //}
    }

    google.maps.event.addListener(map, 'zoom_changed', function () {
        //var crrt_checkbox = document.getElementById('showHideCrrtCheck');
        //only have to worry if crrt is checked.
        //...and if we're crossing defaultMapZoom "zone"
        if (map.getZoom() >= selectedDefaultZoom || map.getZoom() < selectedDefaultZoom) {
            if (map.getZoom() < defaultMapZoom) {

                for (var i = 0; i < gCrrtMarkersA.length; i++) {
                    gCrrtMarkersA[i].setMap(map);
                }
            }
            if (map.getZoom() >= selectedDefaultZoom) {

                for (var j = 0; j < gCrrtMarkersA.length; j++) {
                    gCrrtMarkersA[j].setMap(map);
                }
            }
        }
        //var oldZoom = map.getZoom();
    });

    service = new google.maps.places.PlacesService(map);
    google.maps.event.addListener(map, 'idle', function () {
        // var b = 0;
        if (isMapMove)
            init();
        // if (map.getZoom() != selectedDefaultZoom && $('#chkViewByHouseHold')[0].checked) {
        // window.setTimeout(function updateKML() {
        // zoomLevel = map.getZoom();
        // if (zoomLevel <= defaultMapZoom) {
        // refreshMap();
        // prevZoomLevel = map.getZoom();
        // }
        // else {
        // prevZoomLevel = map.getZoom();
        // }

        // }, 2000);
        // }
    });
    google.maps.event.addListener(map, 'dragend', function () {
        if ($('#chkMailAroundCompetitor').is(":checked")) {
            isMapMove = true;
        }
        if ($('#chkViewByHouseHold')[0].checked) {
            addTiles();
        }
    });
    //var prevZoomLevel = map.getZoom();

    google.maps.event.addListener(map, 'dblclick', function (event) {
        mapCenter = event.latLng;
    });
    google.maps.event.addDomListener(window, "resize", function () {
        //var height = $(window).height();
        //var width = $(window).width();
        //$('#dvMapMarker').find('img').css('margin-left', Math.round($('#mapHolder').width() / 100));
        mapVerticalResize();
        if (isMobile || !isMobile) {
            if ($(window).width() > 414) {
                map.setCenter(mapCenter);
                map.setZoom(defaultMapZoom);
            }
            else {
                map.setCenter(mapCenter);
                map.setZoom(defaultMapZoom - 1);
            }
        }
            //else if (isMobile) {
            //    if ($(window).width() > 414) {
            //        map.setCenter(mapCenter);
            //        map.setZoom(defaultMapZoom);
            //    }
            //    else {
            //        map.setCenter(mapCenter);
            //        map.setZoom(defaultMapZoom - 1);
            //    }

            //    //$('#map').css('height', '60% !important');
            //} 
        else {
            $('#dvMapMarker').find('img').css('margin-left', Math.round($('#mapHolder').width() / 100));
            document.getElementById("mapHolder").style.width = '98% !important'; //width + 'px';//width + 'px'; //
        }
        if (map != undefined && map != null) {
            var center = map.getCenter();
            window.setTimeout(function () {
                google.maps.event.trigger(map, "resize");
                map.setCenter(center);
            }, 1000);
        }
    });
    // }

    $('#map').show();
    if (ctaLayer != null) {
        ctaLayer.setMap(null);
        gCrrtMarkersA = [];
    }

    if (isMapMove) {
        if ($('#ulSelectedCompetitor li').length > 0) {
            mapCenter = new google.maps.LatLng($('#ulSelectedCompetitor li').data('lat'), $('#ulSelectedCompetitor li').data('lng'));
        }
    }

    selectedDefaultZoom = (gSelectedZips.zoomLevel != undefined && gSelectedZips.zoomLevel != null && gSelectedZips.zoomLevel == 0) ? defaultMapZoom : ((gSelectedZips.zoomLevel != undefined && gSelectedZips.zoomLevel != null) ? gSelectedZips.zoomLevel : defaultMapZoom);
    //var map_options = {};
    if (map == undefined || map == null) {
        map = new google.maps.Map(document.getElementById('map'), {
            'zoom': selectedDefaultZoom,
            'center': mapCenter,
            'mapTypeId': google.maps.MapTypeId.ROADMAP
        });
    }
    else {
        //map.setZoom(selectedDefaultZoom);
        if (isPolygonEdited) {
            loadMapState();
            isPolygonEdited = false;
            document.cookie = name + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
        } else {
            mapCenter = (new google.maps.LatLng(latitude, longitude));
            map.setCenter(mapCenter);
            map.setZoom((selectedDefaultZoom > defaultMapZoom) ? defaultMapZoom : selectedDefaultZoom);
        }
    }

    var store_id = (($.parseJSON(sessionStorage.locationConfig).hidden["storeId"] == undefined || $.parseJSON(sessionStorage.locationConfig).hidden["storeId"] == null) && gSelectedZips.storeId != undefined && gSelectedZips.storeId != null && gSelectedZips.storeId != "") ? gSelectedZips.storeId : '';
    var contentString = (store_id != "") ? ("Store Id: " + store_id) : ($('#ddlStoreLocations').find(":selected").text().substring(0, $('#ddlStoreLocations').find(":selected").text().indexOf('-') - 1));

    infowindow = new google.maps.InfoWindow({
        content: contentString,
        maxWidth: 100
    });
    if ($('#ulSelectedCompetitor li').length == 0)
		addStoreMarker(new google.maps.LatLng(latitude, longitude));        
		//var store_marker = addStoreMarker(new google.maps.LatLng(latitude, longitude));
    else {
        drawCompetitorLocMarker();
    }

    if (!$('#dvSelectByRadius').hasClass('ui-collapsible-collapsed')) {
        if (!$('#rdoPolygon').is(':checked')) {
            var radius_value = 14;
            //if (!$('#sldrRange')[0].disabled)
            radius_value = $('#sldrRange').val();
            var units = 'miles';
            if (($("input[name=rdoCounts]:checked").val() == "r" || $("input[name=rdoCounts]:checked").val() == "t") && gSelectedZips.wantsRadius == 1)
                drawCircle(map, (new google.maps.LatLng(latitude, longitude)), radius_value, units);
            else {
                if (myCircle != null) {
                    myCircle.setMap(null);
                }
            }
        }
    }
    generateKMLMap();
}
var theInstructions = "";
//Generates KML Layer on the map
function generateKMLMap() {
    $('#divError').css('display', 'none');

    delete gSelectedZips["wantsCrrtMarkers"];
    if (!($('#chkMailAroundCompetitor').is(':checked') && $('#ulSelectedCompetitor li').length > 0)) {
        delete gSelectedZips.compLatitude;
        delete gSelectedZips.compLongitude;
    }
    var temp_kml_info = $.extend(true, {}, gSelectedZips);
    if ($('#chkViewByHouseHold').is(':checked')) {
        if (temp_kml_info.zoomLevel >= defaultMapZoom) {
            temp_kml_info.zoomLevel = 18;
        }
    }
    temp_kml_info.demos = makeSelectedDemosObject();
    temp_kml_info.storeId = temp_kml_info.storeId.replace(/ /g, '_');
    var kml_url = mapsUrl + 'api/Kml/' + jobCustomerNumber + '/' + facilityId + '/' + jobNumber + '/asdf';
    //var kml_url = mapsUrl + 'api/Kml/' + jobCustomerNumber + '/1/' + jobNumber + '/' + selectedStoreId + '/asdf';
    //if (jobCustomerNumber == ALLIED)
    //   kml_url = mapsUrl + 'api/Kml/' + jobCustomerNumber + '/1/' + jobNumber + '/' + selectedStoreId + '/asdf' + "/" + pageObj.currentInHome().dateValue.replace(/\//g, '_');

    //THE below has to be opend if it is required to set the REGIS auth string to ALLIED customer at the below mentioned commented numbers
    //#1 - START
    var temp_auth_string = sessionStorage.authString;
    if (jobCustomerNumber == ALLIED) {
        sessionStorage.authString = "Basic dXNlcl9yZWcxOnVzZXJfcmVnMQ==";
    }
    //#1
    postCORS(kml_url, JSON.stringify(temp_kml_info), function (data) {

        if (selectedTargetType.toLowerCase().indexOf('usmailing') > -1 && jobCustomerNumber == KUBOTA) {
            data.kmlName = "_61125_20160920015806.kmz";
            if (temp_kml_info.wantsScatter == 1)
                data.kmlScatterName = "_61125_scatter_0920015806.kmz";
        }

        //postCORS(mapsUrl + 'api/Kml/' + appPrivileges.customerNumber + '/1/-13502/' + selectedStoreId + '/asdf', JSON.stringify(gSelectedZips), function (data) {
        //#2 - START
        if (jobCustomerNumber == ALLIED) {
            sessionStorage.authString = temp_auth_string;
        }
        //#2 - END
        if (isMapMove)
            map.setCenter(map.getCenter());

        var kml_file = myProtocol + 'images.tribunedirect.com/mapKml/' + data.kmlName;
        ctaLayer = new google.maps.KmlLayer(String(kml_file), { suppressInfoWindows: false, preserveViewport: true });
        ctaLayer.setMap(map);
        //gCrrtsA = eval(data.crrtA);
        //addCrrts();
        //addLocationMarkers(eval(data.locationsA));
        if ($('#chkViewSurroundingLocations').is(":checked")) {
            theInstructions = "return " + data.locationsA;
            var F = new Function(theInstructions);
            addLocationMarkers(F());
        }

        //         window.setTimeout(function getDelay() {
        //            ctaLayer.zIndex = 5500;
        //        }, 2000);
        window.setTimeout(function () {
            var kml_scatter_file = myProtocol + 'images.tribunedirect.com/mapKml/' + data.kmlScatterName;
            var scatter_kml_Layer = new google.maps.KmlLayer(String(kml_scatter_file), { suppressInfoWindows: false, preserveViewport: true });
            scatter_kml_Layer.setMap(map);
            scatter_kml_Layer.zIndex = 5000;
            
            if (data.kmlScatterAName != undefined && data.kmlScatterAName != null) {
                var kml_scatterA_file = myProtocol + 'images.tribunedirect.com/mapKml/' + data.kmlScatterAName;
                var scatterA_kml_Layer = new google.maps.KmlLayer(String(kml_scatterA_file), { suppressInfoWindows: false, preserveViewport: true });
                scatterA_kml_Layer.setMap(map);
                scatterA_kml_Layer.zIndex = 5000;
            }

            if (data.kmlScatterBName != undefined && data.kmlScatterBName != null) {
                var kml_scatterB_file = myProtocol + 'images.tribunedirect.com/mapKml/' + data.kmlScatterBName;
                var scatterB_kml_Layer = new google.maps.KmlLayer(String(kml_scatterB_file), { suppressInfoWindows: false, preserveViewport: true });
                scatterB_kml_Layer.setMap(map);
                scatterB_kml_Layer.zIndex = 5000;
            }

        }, 1000);
        mapVerticalResize();
    }, function (error_response) {
        //#3 - START
        if (jobCustomerNumber == ALLIED) {
            sessionStorage.authString = temp_auth_string;
        }
        //#3 - END
        showErrorResponseText(error_response, false);
    });
}

//Initiates the search
function init() {
    //var is_map_move = true;
    var NewMapCenter = map.getCenter();
    var search_string = '';

    if ($('#chkMailAroundCompetitor').attr('checked') == 'checked' || $('#ulSelectedCompetitor li').length > 0) {
        var selected_state = document.getElementById("ddlSearchStates").selectedIndex;
        selected_state = $("#ddlSearchStates option:selected").val();

        var business_name = document.getElementById("txtBusinessName").value;

        if (document.getElementById("txtAddress").value != '' && (document.getElementById("txtAddress").value != $('#txtAddress').attr('placeholder')))
            search_string += ' ' + document.getElementById("txtAddress").value;
        if (document.getElementById("txtCity").value != '' && (document.getElementById("txtCity").value != $('#txtCity').attr('placeholder')))
            search_string += ' ' + document.getElementById("txtCity").value;
        if (selected_state != '')
            search_string += ', ' + selected_state;
        if (document.getElementById("txtZip").value != '' && (document.getElementById("txtZip").value != $('#txtZip').attr('placeholder')))
            search_string += ' ' + document.getElementById("txtZip").value;

        //var resultList = [];

        //var request = {
        //    location: NewMapCenter,
        //    radius: '50',
        //    query: business_name
        //};
        //        //if (business_name != '') {
        var geoCoder = new google.maps.Geocoder();

        search_string += ', us';
        if (search_string == '')
            search_string = business_name;
        geoCoder.geocode({ 'address': search_string }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                //                //if (ctrl_id != undefined && ctrl_id != null && ctrl_id != '' && $(ctrl_id).attr('id').toLowerCase() == 'btnrefreshmap') {
                //                //    map.setCenter(NewMapCenter);
                //                //}
                //                //else {
                //                //    var lat_lng = new google.maps.LatLng(results[0].geometry.location.k, results[0].geometry.location.B);
                //                //    map.setCenter(lat_lng);
                //                //}

                if (isMapMove) {
                    map.setCenter(NewMapCenter);
                }
                else {
                    var lat_lng = new google.maps.LatLng(results[0].geometry.location.lat(), results[0].geometry.location.lng());
                    map.setCenter(lat_lng);
                }

                var keyword = business_name;
                //var rankBy = 'prominence';
                var search = {};
                if (keyword) {
                    search.keyword = keyword;
                }
                search.bounds = map.getBounds();
                service.search(search, function (results, status) {
                    if (status == google.maps.places.PlacesServiceStatus.OK) {
                        if ($('#ulSelectedCompetitor li').length > 0 && !$('#chkMailAroundCompetitor').is(':checked')) {
                            $('#dvSearchCompititor').css('display', 'none');
                            $('#lblMailAroundCompetitor').attr('data-theme', '');
                            $('#chkMailAroundCompetitor').attr('checked', false).checkboxradio('refresh');
                            //$('#lblMailAroundCompetitor').trigger('create');
                            drawCompetitorLocMarker();
                        }
                        else {
                            clearSelectedMarkers();
                            plotResultLists(results);
                            isMapMove = false;
                        }
                    }
                });

            }
        });
        clearMarker();
        google.maps.event.clearListeners(map, 'idle');
    }
}

function drawCompetitorLocMarker() {
    if (markersForSelectedLocation.length > 0 && markersForSelectedLocation[0] != undefined) {
        //var selected_marker = markersForSelectedLocation[0].marker;
        var LatLng = new google.maps.LatLng(markersForSelectedLocation[0].marker.position.lat(), markersForSelectedLocation[0].marker.position.lng());
        var marker = new google.maps.Marker({
            map: map,
            position: LatLng
            //icon: (selected_marker != '') ? selected_marker : logoPath + 'mapMarkers/marker_red.png'
        });
        drawNewCircle(markersForSelectedLocation[0].marker.position.lat(), markersForSelectedLocation[0].marker.position.lng(), marker);
    }
}

//Draws the circle with the specified radius
function drawCircle(map, map_center, radius_value, units) {

    if (myCircle != null) {
        myCircle.setMap(null);
    }

    if (units == 'kilometer')
        radius_value = radius_value * 1000;
    else
        radius_value = radius_value * 1609.344;

    var circleOptions = {
        strokeColor: '#FF0000',
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: '#2f51fd',
        fillOpacity: 0.4,
        map: map,
        center: map_center,
        clickable: true,
        radius: radius_value
    };
    // Add the circle for this city to the map.
    myCircle = new google.maps.Circle(circleOptions);

    //my_circle
    google.maps.event.addListener(myCircle, 'dblclick', function (event) {
        mapCenter = event.latLng;
    });

    //circles.push(my_circle);
}

function addStoreMarker(mapCenter) {
    var marker = null;
    var image = null;
    var shadow = null;
    var shape = null;
    switch (jobCustomerNumber) {
        case JETS:
            //eval(getURLDecode(pagePrefs.scriptBlockDict.addStoreMarker186));
            image = new google.maps.MarkerImage('../images/maps/jets_map_marker.png',
           // This marker is 32 pixels wide by 36 pixels tall.
               new google.maps.Size(32, 36),
           // The origin for this image is 0,0.
               new google.maps.Point(0, 0),
           // The anchor for this image is the point of the marker at 16,18.
               new google.maps.Point(16, 18));
            shadow = new google.maps.MarkerImage('',
           // The shadow image is larger in the horizontal dimension
           // while the position and offset are the same as for the main image.
               new google.maps.Size(0, 0),
               new google.maps.Point(0, 0),
               new google.maps.Point(0, 0));
            shape = { coord: [0, 12, 10, 0, 21, 0, 32, 0, 32, 25, 23, 36, 14, 36, 0, 22], type: 'poly' };
            marker = new google.maps.Marker({ position: mapCenter, map: map, shadow: shadow, icon: image, shape: shape, draggable: false, title: "Jet's Pizza" });
            break;
        case CASEYS:
            //eval(getURLDecode(pagePrefs.scriptBlockDict.addStoreMarker155711));
            image = new google.maps.MarkerImage('../images/maps/caseys_map_marker.png',
           // This marker is 59 pixels wide by 24 pixels tall.
           new google.maps.Size(41, 42),
           // The origin for this image is 0,0.
           new google.maps.Point(0, 0),
           // The anchor for this image is the point of the marker at 29,24.
           new google.maps.Point(20, 42));
            shadow = new google.maps.MarkerImage('../images/maps/caseys_map_shadow.png',
           // The shadow image is larger in the horizontal dimension
           // while the position and offset are the same as for the main image.
           new google.maps.Size(50, 42),
           new google.maps.Point(0, 0),
           new google.maps.Point(20, 42));
            shape = {
                coord: [0, 17, 18, 7, 18, 1, 22, 0, 22, 7, 41, 17, 41, 18, 39, 18, 39, 39, 23, 39, 21, 42, 19, 42, 17, 39, 2, 39, 2, 18, 0, 18],
                type: 'poly'
            };

            marker = new google.maps.Marker({
                position: mapCenter,
                map: map,
                shadow: shadow,
                icon: image,
                shape: shape,
                draggable: false,
                title: 'Caseys General Store'
            });
            break;
        case CW:
        case BRIGHTHOUSE: //Demo Customer

            //eval(getURLDecode(pagePrefs.scriptBlockDict.addStoreMarker155370));

            image = new google.maps.MarkerImage('../images/maps/camping_world_map_marker.png',
            new google.maps.Size(30, 41),
            new google.maps.Point(0, 0),
            new google.maps.Point(14, 41));
            shadow = new google.maps.MarkerImage('../images/maps/camping_world_map_shadow.png',
            new google.maps.Size(47, 41),
            new google.maps.Point(0, 0),
            new google.maps.Point(14, 41));
            shape = {
                coord: [14, 41, 10, 29, 0, 16, 0, 9, 9, 0, 20, 0, 30, 9, 30, 16, 21, 29, 15, 41],
                type: 'poly'
            };
            marker = new google.maps.Marker({
                position: mapCenter,
                map: map,
                shadow: shadow,
                icon: image,
                shape: shape,
                draggable: false,
                title: 'Camping World'
            });
            break;
        case REGIS:
            var brand_name = (sessionStorage.brandName != undefined && sessionStorage.brandName != null && sessionStorage.brandName != "select") ? sessionStorage.brandName : "";
            //if (brand_name == "") brand_name = "smartstyle";
            switch (brand_name.toLowerCase().replace(/'/g, '').replace(/\//g, '').replace(/&/g, '')) {
                case "":
                case "beautybar":
                case "beautyunlimited":
                case "carltonhairecotiquedayspa":
                case "carltonhairsalondayspa":
                case "carltonhaircafebeauty":
                case "carltonhairhealthbeauty":
                case "chicagohair":
                case "citylooks":
                case "haircrafters":
                case "hairbystewarts":
                case "hairplus":
                case "hairinc":
                case "holidaybronze":
                case "holidayhairtoo":
                case "imageshairdesign":
                case "jeanlouisdavid":
                case "michaelofthecarlyle":
                case "mitchellshairstyling":
                case "procuts":
                case "progressionssalon":
                case "regissignaturesalon":
                case "saloncielo":
                case "sassoonsalon":
                case "sensations":
                case "twinscissors":
                case "vidalsassoonacademy":
                    brand_name = "default";
                    break;
                default:
                    break;
            }
            //if (brand_name == ""||brand_name.toLowerCase() =="hairbystewarts" ||brand_name.toLowerCase() =="imageshairdesign" ) brand_name = "default";
            //var image = new google.maps.MarkerImage('../images/maps/' + brand_name + '_marker.png',
            //new google.maps.Size(28, 31),
            //new google.maps.Point(0, 0),
            //new google.maps.Point(14, 31));
            //var shape = {
            //    coord: [0, 0, 28, 0, 28, 23, 18, 23, 14, 31, 8, 23, 0, 23],
            //    type: 'poly'
            //};

            //marker = new google.maps.Marker({
            //    position: mapCenter,
            //    map: map,
            //    shadow: shadow,
            //    icon: image,
            //    shape: shape,
            //    draggable: false,
            //    title: 'Regis Corporation',
            //    zIndex: 5000
            //});
            marker = getSelectedLocationMarker(brand_name, mapCenter);
            break;
        case SCOTTS:
            //eval(getURLDecode(pagePrefs.scriptBlockDict.addStoreMarker105));
            image = new google.maps.MarkerImage('../images/maps/scotts_map_marker.png',
            // This marker is 40 pixels wide by 28 pixels tall.
            new google.maps.Size(40, 28),
            // The origin for this image is 0,0.
            new google.maps.Point(0, 0),
            // The anchor for this image is the point of the marker at 20, 27.
            new google.maps.Point(20, 27));
            shadow = new google.maps.MarkerImage('',
            // The shadow image is larger in the horizontal dimension
            // while the position and offset are the same as for the main image.
            new google.maps.Size(0, 0),
            new google.maps.Point(0, 0),
            new google.maps.Point(00, 0));
            shape = {
                coord: [0, 9, 3, 3, 12, 0, 29, 0, 36, 3, 40, 9, 34, 17, 23, 20, 20, 27, 18, 27, 17, 20, 6, 18],
                type: 'poly'
            };

            marker = new google.maps.Marker({
                position: mapCenter,
                map: map,
                shadow: shadow,
                icon: image,
                shape: shape,
                draggable: false,
                title: 'Scotts'
            });
            break;
        case ACE:       
            //eval(getURLDecode(pagePrefs.scriptBlockDict.addStoreMarker105));
            image = new google.maps.MarkerImage('../images/maps/ace_map_marker.png',
            // This marker is 40 pixels wide by 28 pixels tall.
            new google.maps.Size(32,38),
            // The origin for this image is 0,0.
            new google.maps.Point(0, 0),
            // The anchor for this image is the point of the marker at 20, 27.
            new google.maps.Point(16,37));
            shadow = new google.maps.MarkerImage('',
            // The shadow image is larger in the horizontal dimension
            // while the position and offset are the same as for the main image.
            new google.maps.Size(0, 0),
            new google.maps.Point(0, 0),
            new google.maps.Point(00, 0));
            shape = {
                coord: [0, 16, 3, 5, 15, 0, 27, 5, 32, 16, 27, 27, 16, 38, 5, 28],
                type: 'poly'
            };

            marker = new google.maps.Marker({
                position: mapCenter,
                map: map,
                shadow: shadow,
                icon: image,
                shape: shape,
                draggable: false,
                title: 'ACE'
            });
            break;
        case ANDERSEN:
            //eval(getURLDecode(pagePrefs.scriptBlockDict.addStoreMarker364));
            image = new google.maps.MarkerImage('../images/maps/and_map_marker.png',
            // This marker is 30 pixels wide by 30 pixels tall.
            new google.maps.Size(30, 30),
            // The origin for this image is 0,0.
            new google.maps.Point(0, 0),
            // The anchor for this image is the point of the marker at 15,30.
            new google.maps.Point(15, 30));
            shadow = new google.maps.MarkerImage('',
            // The shadow image is larger in the horizontal dimension
            // while the position and offset are the same as for the main image.
            new google.maps.Size(0, 0),
            new google.maps.Point(0, 0),
            new google.maps.Point(00, 0));
            shape = {
                coord: [0, 0, 30, 0, 30, 24, 21, 24, 15, 30, 8, 24, 0, 24],
                type: 'poly'
            };

            marker = new google.maps.Marker({
                position: mapCenter,
                map: map,
                shadow: shadow,
                icon: image,
                shape: shape,
                draggable: false,
                title: 'Andersen'
            });
            break;
        case SHERWINWILLIAMS:                
            //eval(getURLDecode(pagePrefs.scriptBlockDict.addStoreMarker378));
            image = new google.maps.MarkerImage('../images/maps/sw_map_marker.png',
            // This marker is 32 pixels wide by 47 pixels tall.
            new google.maps.Size(32, 47),
            // The origin for this image is 0,0.
            new google.maps.Point(0, 0),
            // The anchor for this image is the point of the marker at 16,44.
            new google.maps.Point(16, 44));
            shadow = new google.maps.MarkerImage('',
            // The shadow image is larger in the horizontal dimension
            // while the position and offset are the same as for the main image.
            new google.maps.Size(0, 0),
            new google.maps.Point(0, 0),
            new google.maps.Point(00, 0));
            shape = {
                coord: [0, 13, 11, 0, 14, 0, 28, 15, 18, 27, 31, 43, 30, 45, 1, 45, 2, 37, 12, 27],
                type: 'poly'
            };

            marker = new google.maps.Marker({
                position: mapCenter,
                map: map,
                shadow: shadow,
                icon: image,
                shape: shape,
                draggable: false,
                title: 'Sherwin Williams'
            });
            break;   
        case TRAEGER:
            //eval(getURLDecode(pagePrefs.scriptBlockDict.addStoreMarker381));
            image = new google.maps.MarkerImage('../images/maps/tr_map_marker.png',
            // This marker is 52 pixels wide by 23 pixels tall.
            new google.maps.Size(52, 23),
            // The origin for this image is 0,0.
            new google.maps.Point(0, 0),
            // The anchor for this image is the point of the marker at 26,12.
            new google.maps.Point(26, 12));
            shadow = new google.maps.MarkerImage('',
            // The shadow image is larger in the horizontal dimension
            // while the position and offset are the same as for the main image.
            new google.maps.Size(0, 0),
            new google.maps.Point(0, 0),
            new google.maps.Point(00, 0));
            shape = {
                coord: [0, 0, 52, 0, 23, 52, 23, 0],
                type: 'poly'
            };

            marker = new google.maps.Marker({
                position: mapCenter,
                map: map,
                shadow: shadow,
                icon: image,
                shape: shape,
                draggable: false,
                title: 'Traeger'
            });
            break;
        case BENJAMINMOORE:
            //eval(getURLDecode(pagePrefs.scriptBlockDict.addStoreMarker384));
            image = new google.maps.MarkerImage('../images/maps/bm_map_marker.png',
            // This marker is 28 pixels wide by 23 pixels tall.
            new google.maps.Size(28, 23),
            // The origin for this image is 0,0.
            new google.maps.Point(0, 0),
            // The anchor for this image is the point of the marker at 13,15.
            new google.maps.Point(13, 15));
            shadow = new google.maps.MarkerImage('',
            // The shadow image is larger in the horizontal dimension
            // while the position and offset are the same as for the main image.
            new google.maps.Size(0, 0),
            new google.maps.Point(0, 0),
            new google.maps.Point(00, 0));
            shape = {
                coord: [13, 0, 27, 22, 1, 22],
                type: 'poly'
            };

            marker = new google.maps.Marker({
                position: mapCenter,
                map: map,
                shadow: shadow,
                icon: image,
                shape: shape,
                draggable: false,
                title: 'Benjamin Moore'
            });
            break;
        case MILWAUKEE:
            //eval(getURLDecode(pagePrefs.scriptBlockDict.addStoreMarker385));
            image = new google.maps.MarkerImage('../images/maps/mi_map_marker.png',
            // This marker is 23 pixels wide by 28 pixels tall.
            new google.maps.Size(23, 28),
            // The origin for this image is 0,0.
            new google.maps.Point(0, 0),
            // The anchor for this image is the point of the marker at 11,27.
            new google.maps.Point(11, 27));
            shadow = new google.maps.MarkerImage('',
            // The shadow image is larger in the horizontal dimension
            // while the position and offset are the same as for the main image.
            new google.maps.Size(0, 0),
            new google.maps.Point(0, 0),
            new google.maps.Point(00, 0));
            shape = {
                coord: [0, 23, 0, 0, 23, 0, 23, 23, 15, 23, 11, 27, 7, 23],
                type: 'poly'
            };

            marker = new google.maps.Marker({
                position: mapCenter,
                map: map,
                shadow: shadow,
                icon: image,
                shape: shape,
                draggable: false,
                title: 'Milwaukee'
            });
            break;
        case ALLIED:
            //eval(getURLDecode(pagePrefs.scriptBlockDict.addStoreMarker186));
            image = new google.maps.MarkerImage('../images/maps/map_marker_allied.png',
           // This marker is 23 pixels wide by 29 pixels tall.
           new google.maps.Size(23, 29),
           // The origin for this image is 0,0.
           new google.maps.Point(0, 0),
           // The anchor for this image is the point of the marker at 12,29.
           new google.maps.Point(12, 29));
            shadow = new google.maps.MarkerImage('',
           // The shadow image is larger in the horizontal dimension
           // while the position and offset are the same as for the main image.
           new google.maps.Size(0, 0),
           new google.maps.Point(0, 0),
           new google.maps.Point(0, 0));
            shape =
           { coord: [0, 0, 23, 0, 23, 23, 15, 23, 12, 29, 8, 23, 0, 23], type: 'poly' }
            ;
            marker = new google.maps.Marker(
            { position: mapCenter, map: map, shadow: shadow, icon: image, shape: shape, draggable: false, title: "Allied Air" }
            );
            break;
        case DCA:
            //eval(getURLDecode(pagePrefs.scriptBlockDict.addStoreMarker155711));
            image = new google.maps.MarkerImage('../images/maps/dca_map_marker.png',
           // This marker is 59 pixels wide by 24 pixels tall.
           new google.maps.Size(29, 37),
           // The origin for this image is 0,0.
           new google.maps.Point(0, 0),
           // The anchor for this image is the point of the marker at 29,24.
           new google.maps.Point(14, 37));
            shadow = new google.maps.MarkerImage('',
           // The shadow image is larger in the horizontal dimension
           // while the position and offset are the same as for the main image.
           new google.maps.Size(0, 0),
           new google.maps.Point(0, 0),
           new google.maps.Point(00, 0));
            shape = {
                coord: [0, 5, 4, 0, 24, 0, 29, 5, 29, 24, 24, 28, 19, 29, 14, 37, 9, 29, 4, 29, 0, 24],
                type: 'poly'
            };
            marker = new google.maps.Marker({ position: mapCenter, map: map, shadow: shadow, icon: image, shape: shape, draggable: false, title: 'Dental Care Alliance' });
            break;
        case SPORTSKING:
            //eval(getURLDecode(pagePrefs.scriptBlockDict.addStoreMarker298));
            image = new google.maps.MarkerImage('../images/maps/sk_map_marker.png',
           // This marker is 29 pixels wide by 36 pixels tall.
           new google.maps.Size(29, 36),
           // The origin for this image is 0,0.
           new google.maps.Point(0, 0),
           // The anchor for this image is the point of the marker at 14,36.
           new google.maps.Point(14, 36));
            shadow = new google.maps.MarkerImage('',
           // The shadow image is larger in the horizontal dimension
           // while the position and offset are the same as for the main image.
           new google.maps.Size(0, 0),
           new google.maps.Point(0, 0),
           new google.maps.Point(00, 0));
            shape = {
                coord: [0, 0, 29, 0, 29, 27, 20, 27, 14, 36, 8, 27, 0, 27],
                type: 'poly'
            };

            marker = new google.maps.Marker({
                position: mapCenter,
                map: map,
                shadow: shadow,
                icon: image,
                shape: shape,
                draggable: false,
                title: 'SportsKing'
            });
            break;
        case KUBOTA:
            //eval(getURLDecode(pagePrefs.scriptBlockDict.addStoreMarker155711));
            image = new google.maps.MarkerImage('../images/maps/kubota_map_marker.png',
           // This marker is 59 pixels wide by 24 pixels tall.
           new google.maps.Size(29, 28),
           // The origin for this image is 0,0.
           new google.maps.Point(0, 0),
           // The anchor for this image is the point of the marker at 29,24.
           new google.maps.Point(14, 28));
            shadow = new google.maps.MarkerImage('',
           // The shadow image is larger in the horizontal dimension
           // while the position and offset are the same as for the main image.
           new google.maps.Size(0, 0),
           new google.maps.Point(0, 0),
           new google.maps.Point(00, 0));
            shape = {
                coord: [0, 11, 3, 3, 11, 0, 19, 0, 25, 4, 29, 10, 25, 18, 18, 20, 14, 28, 10, 20, 4, 17],
                type: 'poly'
            };

            marker = new google.maps.Marker({
                position: mapCenter,
                map: map,
                shadow: shadow,
                icon: image,
                shape: shape,
                draggable: false,
                title: 'Kubota'
            });
            break;
        case GWA:
            //eval(getURLDecode(pagePrefs.scriptBlockDict.addStoreMarker284));
            image = new google.maps.MarkerImage('../images/maps/gwa_map_marker.png',
            // This marker is 19 pixels wide by 23 pixels tall.
            new google.maps.Size(45, 28),
            // The origin for this image is 0,0.
            new google.maps.Point(0, 0),
            // The anchor for this image is the point of the marker at 9,23.
            new google.maps.Point(22, 27));
            shadow = new google.maps.MarkerImage('',
            // The shadow image is larger in the horizontal dimension
            // while the position and offset are the same as for the main image.
            new google.maps.Size(0, 0),
            new google.maps.Point(0, 0),
            new google.maps.Point(00, 0));
            shape = {
                coord: [0, 0, 45, 19, 27, 19, 22, 27, 17, 19, 0, 19],
                type: 'poly'
            };

            marker = new google.maps.Marker({
                position: mapCenter,
                map: map,
                shadow: shadow,
                icon: image,
                shape: shape,
                draggable: false,
                title: 'GWA'
            });
            break;
        case AAG:
            //eval(getURLDecode(pagePrefs.scriptBlockDict.addStoreMarker186));
            image = new google.maps.MarkerImage('../images/maps/aag_map_marker.png',
            // This marker is 59 pixels wide by 24 pixels tall.
            new google.maps.Size(32, 29),
            // The origin for this image is 0,0.
            new google.maps.Point(0, 0),
            // The anchor for this image is the point of the marker at 29,24.
            new google.maps.Point(19, 29));
            shadow = new google.maps.MarkerImage('',
           // The shadow image is larger in the horizontal dimension
           // while the position and offset are the same as for the main image.
           new google.maps.Size(0, 0),
           new google.maps.Point(0, 0),
           new google.maps.Point(00, 0));
            shape = {
                coord: [0, 4, 7, 7, 7, 3, 10, 0, 28, 0, 32, 3, 32, 20, 28, 23, 23, 23, 19, 29, 16, 23, 10, 23, 0, 7],
                type: 'poly'
            };
            marker = new google.maps.Marker({ position: mapCenter, map: map, shadow: shadow, icon: image, shape: shape, draggable: false, title: 'AAG' });
            break;
        case ICYNENE:
            //eval(getURLDecode(pagePrefs.scriptBlockDict.addStoreMarker298));
            image = new google.maps.MarkerImage('../images/maps/icy_map_marker.png',
            // This marker is 21 pixels wide by 29 pixels tall.
            new google.maps.Size(21, 29),
            // The origin for this image is 0,0.
            new google.maps.Point(0, 0),
            // The anchor for this image is the point of the marker at 10,29.
            new google.maps.Point(10, 29));
            shadow = new google.maps.MarkerImage('',
            // The shadow image is larger in the horizontal dimension
            // while the position and offset are the same as for the main image.
            new google.maps.Size(0, 0),
            new google.maps.Point(0, 0),
            new google.maps.Point(00, 0));
            shape = {
                coord: [0, 7, 0, 10, 21, 7, 21, 18, 13, 23, 10, 29, 7, 23, 0, 18],
                type: 'poly'
            };
            marker = new google.maps.Marker({
                position: mapCenter,
                map: map,
                shadow: shadow,
                icon: image,
                shape: shape,
                draggable: false,
                title: 'Icynene'
            });
            break;
        default:
            {
                image = '';
                shadow = '';
                shape = '';
                marker = new google.maps.Marker({
                    position: mapCenter,
                    map: map,
                    shadow: shadow,
                    icon: image,
                    shape: shape,
                    draggable: false,
                    title: ''
                });
            }
            break;
    }


    google.maps.event.addListener(marker, 'click', function () {
        infowindow.open(map, marker);
    });
    return marker;
}

function regisLocationMarker(brand_name, location) {
    var image = "";
    var shape = "";
    var location_info = location[0].split('^');
    var location_brand = location_info[2].replace(/ /g, '').replace('&', 'And');
    switch (location_brand.toLowerCase()) {
        case "bestcuts":
            //image = new google.maps.MarkerImage('../images/maps/bestcuts_marker.png',
            image = new google.maps.MarkerImage(logoPath + 'Regis/bestcuts_marker.png',
                new google.maps.Size(28, 29),
                new google.maps.Point(0, 0),
                new google.maps.Point(14, 29));
            shape = {
                coord: [0, 0, 28, 0, 28, 20, 19, 20, 14, 29, 9, 20, 0, 20],
                type: 'poly'
            };
            break;
        case "famoushair":
        case "chicagohair":
            //image = new google.maps.MarkerImage('../images/maps/famoushair_marker.png',
            image = new google.maps.MarkerImage(logoPath + 'Regis/famoushair_marker.png',
                new google.maps.Size(30, 32),
                new google.maps.Point(0, 0),
                new google.maps.Point(15, 32));
            shape = {
                coord: [0, 0, 30, 0, 30, 24, 19, 24, 15, 32, 9, 24, 0, 24],
                type: 'poly'
            };
            break;
        case "fiestasalons":
            //image = new google.maps.MarkerImage('../images/maps/fiestasalons_marker.png',
            image = new google.maps.MarkerImage(logoPath + 'Regis/fiestasalons_marker.png',
                new google.maps.Size(26, 25),
                new google.maps.Point(0, 0),
                new google.maps.Point(13, 25));
            shape = {
                coord: [0, 0, 26, 0, 26, 17, 17, 17, 13, 25, 7, 17, 0, 17],
                type: 'poly'
            };
            break;
        case "mastercuts":
        case "michaelofthecarlyle":
            //image = new google.maps.MarkerImage('../images/maps/mastercuts_marker.png',
            image = new google.maps.MarkerImage(logoPath + 'Regis/mastercuts_marker.png',
                new google.maps.Size(31, 28),
                new google.maps.Point(0, 0),
                new google.maps.Point(14, 28));
            shape = {
                coord: [0, 0, 31, 0, 31, 20, 19, 20, 14, 28, 9, 20, 0, 20],
                type: 'poly'
            };
            break;
        case "regissalons":
            image = new google.maps.MarkerImage(logoPath + 'Regis/regissalons_marker.png',
                new google.maps.Size(26, 26),
                new google.maps.Point(0, 0),
                new google.maps.Point(13, 26));
            shape = {
                coord: [0, 0, 26, 0, 26, 18, 17, 18, 13, 26, 7, 18, 0, 18],
                type: 'poly'
            };
            break;
        case "regissalon":
        case "regissignaturesalon":
            //image = new google.maps.MarkerImage('../images/maps/regissalons_marker.png',
            image = new google.maps.MarkerImage(logoPath + 'Regis/regissalon_marker.png',
                new google.maps.Size(26, 26),
                new google.maps.Point(0, 0),
                new google.maps.Point(13, 26));
            shape = {
                coord: [0, 0, 26, 0, 26, 18, 17, 18, 13, 26, 7, 18, 0, 18],
                type: 'poly'
            };
            break;
        case "smartstyle":
            //image = new google.maps.MarkerImage('../images/maps/smartstyle_marker.png',
            image = new google.maps.MarkerImage(logoPath + 'Regis/smartstyle_marker.png',
                new google.maps.Size(28, 33),
                new google.maps.Point(0, 0),
                new google.maps.Point(14, 31));
            shape = {
                coord: [0, 0, 28, 0, 28, 23, 18, 23, 14, 31, 8, 23, 0, 23],
                type: 'poly'
            };
            break;
        case "coolcuts4kids":
            //image = new google.maps.MarkerImage('../images/maps/coolcuts4kids_marker.png',
            image = new google.maps.MarkerImage(logoPath + 'Regis/coolcuts4kids_marker.png',
                new google.maps.Size(28, 33),
                new google.maps.Point(0, 0),
                new google.maps.Point(14, 31));
            shape = {
                coord: [0, 0, 28, 0, 28, 23, 18, 23, 14, 31, 8, 23, 0, 23],
                type: 'poly'
            };
            break;
        case "hairmasters":
            //image = new google.maps.MarkerImage('../images/maps/hairmasters_marker.png',
            image = new google.maps.MarkerImage(logoPath + 'Regis/hairmasters_marker.png',
                new google.maps.Size(28, 38),
                new google.maps.Point(0, 0),
                new google.maps.Point(13, 38));
            shape = {
                coord: [0, 0, 28, 0, 28, 30, 18, 30, 13, 38, 9, 30, 0, 30],
                type: 'poly'
            };
            break;
        case "costcutters":
            //image = new google.maps.MarkerImage('../images/maps/costcutters_marker.png',
            image = new google.maps.MarkerImage(logoPath + 'Regis/costcutters_marker.png',
                new google.maps.Size(25, 33),
                new google.maps.Point(0, 0),
                new google.maps.Point(12, 32));
            shape = {
                coord: [0, 0, 25, 0, 24, 24, 17, 24, 12, 32, 7, 24, 0, 24],
                type: 'poly'
            };
            break;
        case "supercuts":
            //image = new google.maps.MarkerImage('../images/maps/supercuts_marker.png',
            image = new google.maps.MarkerImage(logoPath + 'Regis/supercuts_marker.png',
                new google.maps.Size(24, 37),
                new google.maps.Point(0, 0),
                new google.maps.Point(11, 37));
            shape = {
                coord: [0, 0, 24, 0, 24, 30, 15, 30, 11, 37, 8, 30, 0, 30],
                type: 'poly'
            };
            break;
        case "vidalsassoon":
            //image = new google.maps.MarkerImage('../images/maps/vidalsassoon_marker.png',
            image = new google.maps.MarkerImage(logoPath + 'Regis/vidalsassoon_marker.png',
                new google.maps.Size(25, 28),
                new google.maps.Point(0, 0),
                new google.maps.Point(12, 28));
            shape = {
                coord: [0, 0, 25, 0, 25, 20, 16, 20, 12, 28, 6, 20, 0, 20],
                type: 'poly'
            };
            break;
        case "boricshaircare":
            //image = new google.maps.MarkerImage('../images/maps/regissalons_marker.png',
            image = new google.maps.MarkerImage(logoPath + 'Regis/borics_marker.png',
                new google.maps.Size(56, 56),
                new google.maps.Point(0, 0),
                new google.maps.Point(13, 56));
            shape = {
                coord: [0, 0, 26, 0, 26, 18, 17, 18, 13, 26, 7, 18, 0, 18],
                type: 'poly'
            };
            break;
        case "":
        case "beautybar":
        case "beautyunlimited":
        case "carltonhairecotiquedayspa":
        case "carltonhairsalon&dayspa":
        case "carltonhair/cafebeauty":
        case "carltonhair/healthbeauty":
        case "chicagohair":
        case "citylooks":
        case "hairplus":
        case "haircrafters":
        case "hairbystewarts":
        case "hairinc":
        case "holidaybronze":
        case "holidayhairtoo":
        case "imageshairdesign":
        case "jeanlouisdavid":
        case "michaelofthecarlyle":
        case "mitchell'shairstyling":
        case "procuts":
        case "progressionssalon":
        case "regissignaturesalon":
        case "saloncielo":
        case "sassoonsalon":
        case "sensations":
        case "twinscissors":
        case "vidalsassoonacademy":
            location_brand = "default";
            image = createSurroundingMarker(location_brand.toLowerCase().replace(/'/g, '').replace(/\//g, '').replace(/&/g, ''));
            break;
        case "boricshaircare":
        case "holidayhair":
        case "hairexcitement":
        case "firstchoicehaircutters":
        case "hairmasters":
        case "saturdays":
        case "panopoulos":
        case "miaandmaxxhairstudio":
        case "tgf":
        case "styleamerica":
        case "default":
        case "headstart":
        case "mitchellshairstyling":
        case "outlooksforhair":
        case "carltonhair":
            //image = new google.maps.MarkerImage('../images/maps/regissalons_marker.png',
            image = createSurroundingMarker(location_brand.toLowerCase().replace(/'/g, '').replace(/\//g, '').replace(/&/g, ''));
            break;
        default:
            break;
    }
    var myLatLng = new google.maps.LatLng(location[1], location[2]);
    var loc_marker = new google.maps.Marker({
        position: myLatLng,
        map: map,
        //shadow: shadow,
        icon: image,
        shape: shape,
        draggable: false,
        contentString: 'Store Id: ' + location_info[0],
        title: 'Store Id: ' + location_info[0]
    });

    return loc_marker;
}

function createSurroundingMarker(brand_name) {
    var image = new google.maps.MarkerImage(logoPath + 'Regis/' + brand_name + '_marker.png',
    //                new google.maps.Size(56, 56),
    //                new google.maps.Point(0, 0),
    //                new google.maps.Point(13, 56));
    //            shape = {
    //                coord: [0, 0, 26, 0, 26, 18, 17, 18, 13, 26, 7, 18, 0, 18],
    //                type: 'poly'
    new google.maps.Size(31, 28),
                new google.maps.Point(0, 0),
                new google.maps.Point(14, 28));
    //var shape = {
    //    coord: [0, 0, 31, 0, 31, 20, 19, 20, 14, 28, 9, 20, 0, 20],
    //    type: 'poly'

    //};
    return image;
}

function getSelectedLocationMarker(location_brand, mapCenter) {
    var image = "", shape = "";
    switch (location_brand.toLowerCase().replace(/'/g, '')) {
        case "bestcuts":
            image = new google.maps.MarkerImage('../images/maps/bestcuts_marker.png',
                new google.maps.Size(28, 29),
                new google.maps.Point(0, 0),
                new google.maps.Point(14, 29));
            shape = {
                coord: [0, 0, 28, 0, 28, 20, 19, 20, 14, 29, 9, 20, 0, 20],
                type: 'poly'
            };
            break;
        case "famoushair":
        case "chicagohair":
            image = new google.maps.MarkerImage('../images/maps/famoushair_marker.png',
                new google.maps.Size(30, 32),
                new google.maps.Point(0, 0),
                new google.maps.Point(15, 32));
            shape = {
                coord: [0, 0, 30, 0, 30, 24, 19, 24, 15, 32, 9, 24, 0, 24],
                type: 'poly'
            };
            break;
        case "fiestasalons":
            image = new google.maps.MarkerImage('../images/maps/fiestasalons_marker.png',
                new google.maps.Size(26, 25),
                new google.maps.Point(0, 0),
                new google.maps.Point(13, 25));
            shape = {
                coord: [0, 0, 26, 0, 26, 17, 17, 17, 13, 25, 7, 17, 0, 17],
                type: 'poly'
            };
            break;
        case "mastercuts":
        case "michaelofthecarlyle":
            image = new google.maps.MarkerImage('../images/maps/mastercuts_marker.png',
                new google.maps.Size(31, 28),
                new google.maps.Point(0, 0),
                new google.maps.Point(14, 28));
            shape = {
                coord: [0, 0, 31, 0, 31, 20, 19, 20, 14, 28, 9, 20, 0, 20],
                type: 'poly'
            };
            break;
        case "regissalons":
        case "regissalon":
        case "regissignaturesalon":
            image = new google.maps.MarkerImage('../images/maps/regissalons_marker.png',
                new google.maps.Size(26, 26),
                new google.maps.Point(0, 0),
                new google.maps.Point(13, 26));
            shape = {
                coord: [0, 0, 26, 0, 26, 18, 17, 18, 13, 26, 7, 18, 0, 18],
                type: 'poly'
            };
            break;
        case "smartstyle":
            image = new google.maps.MarkerImage('../images/maps/smartstyle_marker.png',
                new google.maps.Size(28, 33),
                new google.maps.Point(0, 0),
                new google.maps.Point(14, 31));
            shape = {
                coord: [0, 0, 28, 0, 28, 23, 18, 23, 14, 31, 8, 23, 0, 23],
                type: 'poly'
            };
            break;
        case "coolcuts4kids":
            image = new google.maps.MarkerImage('../images/maps/coolcuts4kids_marker.png',
                new google.maps.Size(28, 33),
                new google.maps.Point(0, 0),
                new google.maps.Point(14, 31));
            shape = {
                coord: [0, 0, 28, 0, 28, 23, 18, 23, 14, 31, 8, 23, 0, 23],
                type: 'poly'
            };
            break;
        case "hairmasters":
            image = new google.maps.MarkerImage('../images/maps/hairmasters_marker.png',
                new google.maps.Size(28, 38),
                new google.maps.Point(0, 0),
                new google.maps.Point(13, 38));
            shape = {
                coord: [0, 0, 28, 0, 28, 30, 18, 30, 13, 38, 9, 30, 0, 30],
                type: 'poly'
            };
            break;
        case "costcutters":
            image = new google.maps.MarkerImage('../images/maps/costcutters_marker.png',
                new google.maps.Size(25, 33),
                new google.maps.Point(0, 0),
                new google.maps.Point(12, 32));
            shape = {
                coord: [0, 0, 25, 0, 24, 24, 17, 24, 12, 32, 7, 24, 0, 24],
                type: 'poly'
            };
            break;
        case "supercuts":
            image = new google.maps.MarkerImage('../images/maps/supercuts_marker.png',
                new google.maps.Size(24, 37),
                new google.maps.Point(0, 0),
                new google.maps.Point(11, 37));
            shape = {
                coord: [0, 0, 24, 0, 24, 30, 15, 30, 11, 37, 8, 30, 0, 30],
                type: 'poly'
            };
            break;
        case "vidalsassoon":
            image = new google.maps.MarkerImage('../images/maps/vidalsassoon_marker.png',
                new google.maps.Size(25, 28),
                new google.maps.Point(0, 0),
                new google.maps.Point(12, 28));
            shape = {
                coord: [0, 0, 25, 0, 25, 20, 16, 20, 12, 28, 6, 20, 0, 20],
                type: 'poly'
            };
            break;
        case "":
        case "beautybar":
        case "beautyunlimited":
        case "carltonhairecotiquedayspa":
        case "carltonhairsalon&dayspa":
        case "carltonhair/cafebeauty":
        case "carltonhair/healthbeauty":
        case "chicagohair":
        case "citylooks":
        case "haircrafters":
        case "hairbystewarts":
        case "hairplus":
        case "hairinc":
        case "holidaybronze":
        case "holidayhairtoo":
        case "imageshairdesign":
        case "jeanlouisdavid":
        case "michaelofthecarlyle":
        case "mitchell'shairstyling":
        case "procuts":
        case "progressionssalon":
        case "regissignaturesalon":
        case "saloncielo":
        case "sassoonsalon":
        case "sensations":
        case "twinscissors":
        case "vidalsassoonacademy":
            location_brand = "default";
            image = createSurroundingMarker(location_brand.toLowerCase().replace(/'/g, '').replace(/\//g, '').replace(/&/g, ''));
            break;

        case "boricshaircare":
        case "holidayhair":
        case "firstchoicehaircutters":
        case "hairexcitement":
        case "hairmasters":
        case "saturdays":
        case "panopoulos":
        case "miaandmaxxhairstudio":
        case "tgf":
        case "styleamerica":
        case "mitchellshairstyling":
        case "default":
        case "headstart":
        case "outlooksforhair":
        case "carltonhair":
            image = createSurroundingMarker(location_brand.toLowerCase().replace(/'/g, '').replace(/\//g, '').replace(/&/g, ''));
            break;
        default:
            break;
    }
    var selected_loc_marker = new google.maps.Marker({
        position: mapCenter,
        map: map,
        //shadow: shadow,
        icon: image,
        shape: shape,
        draggable: false,
        title: 'Regis Corporation',
        zIndex: 5000
    });
    return selected_loc_marker;
}

function addLocationMarkers(locationsA) {


    var declined_markers_cnt = 0;
    declined_markers_cnt = Math.ceil((locationsA.length * 10) / 100);
    var total_declined_markers = 0;
    var locations_list = [];
    var store_customer = "";
    var is_location_in_order = false;
    //var is_declined = false;
    var locations_markers_A = [];
    var image = null, shadow = null, shape = null, myLatLng = null, content_string = "";
    var info_window = new google.maps.InfoWindow({
        content: "Store Id: " + selectedStoreId,
        maxWidth: 100
    });
    for (var i = 0; i < locationsA.length; i++) {
        var location = locationsA[i];
        var marker = null;
        switch (jobCustomerNumber) {
            case JETS:
                //eval(getURLDecode(pagePrefs.scriptBlockDict.addStoreMarker186));
                image = new google.maps.MarkerImage('../images/maps/jets_map_marker_small.png',
                // This marker is 20 pixels wide by 23 pixels tall.
                new google.maps.Size(20, 23),
                // The origin for this image is 0,0.
                new google.maps.Point(0, 0),
                // The anchor for this image is the point of the marker at 10,12.
                new google.maps.Point(10, 12));
                shadow = new google.maps.MarkerImage('',
                // The shadow image is larger in the horizontal dimension
                // while the position and offset are the same as for the main image.
                new google.maps.Size(0, 0),
                new google.maps.Point(0, 0),
                new google.maps.Point(0, 0));
                shape = { coord: [0, 5, 5, 0, 14, 0, 20, 6, 20, 17, 14, 23, 9, 23, 0, 15], type: 'poly' };
                myLatLng = new google.maps.LatLng(location[1], location[2]);
                var store_id = "";
                if (location[0].split('^').length > 0)
                    store_id = location[0].split('^')[0];
                marker = new google.maps.Marker({
                    position: myLatLng,
                    map: map,
                    shadow: shadow,
                    icon: image,
                    shape: shape,
                    draggable: false,
                    contentString: 'Store Id: ' + store_id,
                    title: "Jet's Pizza"
                });
                break;
            case CASEYS:
                /*The below code and its related variable should be removed after demo / when the attribute is available in service.  -- START*/
                //if ((i % 2) > 0 && total_declined_markers <= declined_markers_cnt) {
                //    if (location[0].split('^')[3] == "False") {
                //        location[0] = location[0].substring(0, location[0].lastIndexOf('^') + 1);
                //        location[0] = location[0] + 'True';
                //    }
                //    if (location[0].split('^')[3] == "True") {
                //        total_declined_markers++;
                //        location[0] = location[0] + '^True';
                //    }
                //}
                /*The above code and its related variable should be removed after demo / when the attribute is available in service.  -- END*/

                store_customer = location[0].split('^');
                //eval(getURLDecode(pagePrefs.scriptBlockDict.addLocationMarkers155711));
                //if (store_customer[0] == "2916") {
                //    location[0] = location[0] + '^False';
                //} else if (store_customer[0] == "6") {
                //    location[0] = location[0] + '^True';
                //}
                locations_list = [];
                if (location[0].indexOf('^') > -1)
                    locations_list = location[0].split('^');
                is_location_in_order = false;
                is_location_in_order = (locations_list.length > 3) ? JSON.parse(locations_list[3].toLowerCase()) : ((locations_list.length <= 3) ? true : false);

                /*The below code and its related variable should be removed after demo / when the attribute is available in service.  -- START*/
                //var is_declined = false;
                //is_declined = (locations_list.length > 4) ? JSON.parse(locations_list[4].toLowerCase()) : ((locations_list.length <= 4) ? false : false);
                //var img_path = "";
                //if (is_location_in_order && is_declined)
                //    img_path = '../images/maps/caseys_map_marker_small_declined.png';
                //else if (is_location_in_order && !is_declined)
                //    img_path = '../images/maps/caseys_map_marker_small.png';
                //else if (!is_location_in_order)
                //    img_path = '../images/maps/caseys_map_marker_small_grey.png';
                /*The above code and its related variable should be removed after demo / when the attribute is available in service.  -- END*/

                image = new google.maps.MarkerImage(((is_location_in_order) ? '../images/maps/caseys_map_marker_small.png' : '../images/maps/caseys_map_marker_small_grey.png'),
                //image = new google.maps.MarkerImage(img_path,
                new google.maps.Size(30, 31),
                new google.maps.Point(0, 0),
                new google.maps.Point(15, 31));
                shadow = new google.maps.MarkerImage('../images/maps/caseys_map_shadow_small.png',
                new google.maps.Size(38, 31),
                new google.maps.Point(0, 0),
                new google.maps.Point(15, 31));
                shape = {
                    coord: [0, 13, 0, 12, 13, 5, 13, 0, 17, 0, 17, 5, 30, 12, 30, 13, 29, 13, 29, 28, 18, 28, 16, 31, 15, 31, 13, 28, 2, 28, 2, 13],
                    type: 'poly'
                };
                myLatLng = new google.maps.LatLng(location[1], location[2]);

                marker = new google.maps.Marker({
                    position: myLatLng,
                    map: map,
                    shadow: shadow,
                    icon: image,
                    shape: shape,
                    draggable: false,
                    contentString: 'Store Id: ' + store_customer[0],
                    title: store_customer[1]
                });
                break;
            case CW:
            case BRIGHTHOUSE:

                //eval(getURLDecode(pagePrefs.scriptBlockDict.addLocationMarkers155370));
                image = new google.maps.MarkerImage('../images/maps/camping_world_map_marker.png',
                 new google.maps.Size(30, 41),
                 new google.maps.Point(0, 0),
                 new google.maps.Point(14, 41));
                shadow = new google.maps.MarkerImage('../images/maps/camping_world_map_shadow.png',
                 new google.maps.Size(47, 41),
                 new google.maps.Point(0, 0),
                 new google.maps.Point(14, 41));
                shape = {
                    coord: [14, 41, 10, 29, 0, 16, 0, 9, 9, 0, 20, 0, 30, 9, 30, 16, 21, 29, 15, 41],
                    type: 'poly'
                };
                myLatLng = new google.maps.LatLng(location[1], location[2]);
                marker = new google.maps.Marker({
                    position: myLatLng,
                    map: map,
                    shadow: shadow,
                    icon: image,
                    shape: shape,
                    draggable: false,
                    title: 'Camping World'
                });
                break;
            case REGIS:
                //var brand_name = (sessionStorage.brandName != undefined && sessionStorage.brandName != null && sessionStorage.brandName != "select") ? sessionStorage.brandName : "";
                //if (brand_name != "")
                marker = regisLocationMarker(sessionStorage.brandName, location);
                break;
            case DCA:
                //eval(getURLDecode(pagePrefs.scriptBlockDict.addStoreMarker155711));
                image = new google.maps.MarkerImage('../images/maps/dca_surrounding_map_marker.png',
                // This marker is 59 pixels wide by 24 pixels tall.
                new google.maps.Size(23, 29),
                // The origin for this image is 0,0.
                new google.maps.Point(0, 0),
                // The anchor for this image is the point of the marker at 29,24.
                new google.maps.Point(9, 23));
                shadow = new google.maps.MarkerImage('',
                // The shadow image is larger in the horizontal dimension
                // while the position and offset are the same as for the main image.
                new google.maps.Size(0, 0),
                new google.maps.Point(0, 0),
                new google.maps.Point(00, 0));
                shape = {
                    coord: [0, 2, 2, 0, 18, 0, 19, 2, 19, 15, 16, 18, 13, 18, 9, 23, 6, 18, 3, 18, 0, 15],
                    type: 'poly'
                };
                content_string = "";
                content_string = '<b>Store Id:</b> ' + ((location[0].indexOf('^') > -1) ? location[0].split('^')[0] : "");
                content_string += '<br /><b>Store Name:</b> ' + ((location[0].indexOf('^') > -1 && location[0].split('^').length > 1) ? location[0].split('^')[1] : "");
                myLatLng = new google.maps.LatLng(location[1], location[2]);
                marker = new google.maps.Marker({
                    position: myLatLng,
                    map: map,
                    shadow: shadow,
                    icon: image,
                    shape: shape,
                    draggable: false,
                    contentString: content_string,
                    title: 'Dental Care Alliance'
                });
                break;
            case SPORTSKING:
                //eval(getURLDecode(pagePrefs.scriptBlockDict.addStoreMarker298));
                image = new google.maps.MarkerImage('../images/maps/sk_surrounding_map_marker.png',
                // This marker is 19 pixels wide by 23 pixels tall.
                new google.maps.Size(19, 23),
                // The origin for this image is 0,0.
                new google.maps.Point(0, 0),
                // The anchor for this image is the point of the marker at 9,23.
                new google.maps.Point(9, 23));
                shadow = new google.maps.MarkerImage('',
                // The shadow image is larger in the horizontal dimension
                // while the position and offset are the same as for the main image.
                new google.maps.Size(0, 0),
                new google.maps.Point(0, 0),
                new google.maps.Point(00, 0));
                shape = {
                    coord: [0, 0, 19, 0, 19, 17, 12, 17, 9, 23, 5, 17, 0, 17],
                    type: 'poly'
                };
                content_string = "";
                content_string = '<b>Store Id:</b> ' + ((location[0].indexOf('^') > -1) ? location[0].split('^')[0] : "");
                content_string += '<br /><b>Store Name:</b> ' + ((location[0].indexOf('^') > -1 && location[0].split('^').length > 1) ? location[0].split('^')[1] : "");
                myLatLng = new google.maps.LatLng(location[1], location[2]);
                marker = new google.maps.Marker({
                    position: myLatLng,
                    map: map,
                    shadow: shadow,
                    icon: image,
                    shape: shape,
                    draggable: false,
                    contentString: content_string,
                    title: 'SportsKing'
                });
                break;
            case KUBOTA:
                //eval(getURLDecode(pagePrefs.scriptBlockDict.addStoreMarker155711));
                image = new google.maps.MarkerImage('../images/maps/kubota_neighboring_map_marker.png',
                // This marker is 59 pixels wide by 24 pixels tall.
                new google.maps.Size(26, 25),
                // The origin for this image is 0,0.
                new google.maps.Point(0, 0),
                // The anchor for this image is the point of the marker at 29,24.
                new google.maps.Point(13, 25));
                shadow = new google.maps.MarkerImage('',
                // The shadow image is larger in the horizontal dimension
                // while the position and offset are the same as for the main image.
                new google.maps.Size(0, 0),
                new google.maps.Point(0, 0),
                new google.maps.Point(00, 0));
                shape = {
                    coord: [0, 9, 3, 3, 10, 0, 15, 0, 23, 4, 26, 9, 22, 15, 16, 18, 15, 24, 9, 18, 3, 15],
                    type: 'poly'
                };
                content_string = "";
                content_string = '<b>Store Id:</b> ' + ((location[0].indexOf('^') > -1) ? location[0].split('^')[0] : "");
                content_string += '<br /><b>Store Name:</b> ' + ((location[0].indexOf('^') > -1 && location[0].split('^').length > 1) ? location[0].split('^')[1] : "");
                myLatLng = new google.maps.LatLng(location[1], location[2]);
                marker = new google.maps.Marker({
                    position: myLatLng,
                    map: map,
                    shadow: shadow,
                    icon: image,
                    shape: shape,
                    draggable: false,
                    //contentString: 'Store Id: ' + location[0],
                    contentString: content_string,
                    title: 'Kubota'
                });
                break;
            case GWA:
                //eval(getURLDecode(pagePrefs.scriptBlockDict.addStoreMarker284));
                image = new google.maps.MarkerImage('../images/maps/gwa_surrounding_map_marker.png',
                // This marker is 19 pixels wide by 23 pixels tall.
                new google.maps.Size(36, 23),
                // The origin for this image is 0,0.
                new google.maps.Point(0, 0),
                // The anchor for this image is the point of the marker at 9,23.
                new google.maps.Point(17, 23));
                shadow = new google.maps.MarkerImage('',
                // The shadow image is larger in the horizontal dimension
                // while the position and offset are the same as for the main image.
                new google.maps.Size(0, 0),
                new google.maps.Point(0, 0),
                new google.maps.Point(00, 0));
                shape = {
                    coord: [0, 0, 36, 0, 36, 17, 21, 17, 17, 23, 14, 17, 0, 17],
                    type: 'poly'
                };
                content_string = "";
                content_string = '<b>Store Id:</b> ' + ((location[0].indexOf('^') > -1) ? location[0].split('^')[0] : "");
                content_string += '<br /><b>Store Name:</b> ' + ((location[0].indexOf('^') > -1 && location[0].split('^').length > 1) ? location[0].split('^')[1] : "");
                myLatLng = new google.maps.LatLng(location[1], location[2]);
                marker = new google.maps.Marker({
                    position: myLatLng,
                    map: map,
                    shadow: shadow,
                    icon: image,
                    shape: shape,
                    draggable: false,
                    contentString: content_string,
                    title: 'GWA'
                });
                break;
            case AAG:
                //eval(getURLDecode(pagePrefs.scriptBlockDict.addStoreMarker155711));
                image = new google.maps.MarkerImage('../images/maps/aag_surrounding_map_marker.png',
                // This marker is 59 pixels wide by 24 pixels tall.
                new google.maps.Size(20, 24),
                // The origin for this image is 0,0.
                new google.maps.Point(0, 0),
                // The anchor for this image is the point of the marker at 29,24.
                new google.maps.Point(9, 24));
                shadow = new google.maps.MarkerImage('',
                // The shadow image is larger in the horizontal dimension
                // while the position and offset are the same as for the main image.
                new google.maps.Size(0, 0),
                new google.maps.Point(0, 0),
                new google.maps.Point(00, 0));
                shape = {
                    coord: [0, 3, 3, 0, 17, 0, 20, 3, 20, 16, 17, 20, 13, 20, 0, 24, 9, 24, 6, 20, 3, 20, 0, 16],
                    type: 'poly'
                };
                content_string = "";
                content_string = '<b>Store Id:</b> ' + ((location[0].indexOf('^') > -1) ? location[0].split('^')[0] : "");
                content_string += '<br /><b>Store Name:</b> ' + ((location[0].indexOf('^') > -1 && location[0].split('^').length > 1) ? location[0].split('^')[1] : "");
                myLatLng = new google.maps.LatLng(location[1], location[2]);
                marker = new google.maps.Marker({
                    position: myLatLng,
                    map: map,
                    shadow: shadow,
                    icon: image,
                    shape: shape,
                    draggable: false,
                    //contentString: 'Store Id: ' + location[0],
                    contentString: content_string,
                    title: 'AAG'
                });
                break;
            case SCOTTS:
                /*The below code and its related variable should be removed after demo / when the attribute is available in service.  -- START*/
                //if ((i % 2) > 0 && total_declined_markers <= declined_markers_cnt) {
                //    if (location[0].split('^')[3] == "False") {
                //        location[0] = location[0].substring(0, location[0].lastIndexOf('^') + 1);
                //        location[0] = location[0] + 'True';
                //    }
                //    if (location[0].split('^')[3] == "True") {
                //        total_declined_markers++;
                //        location[0] = location[0] + '^True';
                //    }
                //}
                /*The above code and its related variable should be removed after demo / when the attribute is available in service.  -- END*/

                store_customer = location[0].split('^');
                //eval(getURLDecode(pagePrefs.scriptBlockDict.addLocationMarkers155711));
                //if (store_customer[0] == "2916") {
                //    location[0] = location[0] + '^False';
                //} else if (store_customer[0] == "6") {
                //    location[0] = location[0] + '^True';
                //}
                locations_list = [];
                if (location[0].indexOf('^') > -1)
                    locations_list = location[0].split('^');
                is_location_in_order = false;
                is_location_in_order = (locations_list.length > 3) ? JSON.parse(locations_list[3].toLowerCase()) : ((locations_list.length <= 3) ? true : false);

                /*The below code and its related variable should be removed after demo / when the attribute is available in service.  -- START*/
                //var is_declined = false;
                //is_declined = (locations_list.length > 4) ? JSON.parse(locations_list[4].toLowerCase()) : ((locations_list.length <= 4) ? false : false);
                //var img_path = "";
                //if (is_location_in_order && is_declined)
                //    img_path = '../images/maps/caseys_map_marker_small_declined.png';
                //else if (is_location_in_order && !is_declined)
                //    img_path = '../images/maps/caseys_map_marker_small.png';
                //else if (!is_location_in_order)
                //    img_path = '../images/maps/caseys_map_marker_small_grey.png';
                /*The above code and its related variable should be removed after demo / when the attribute is available in service.  -- END*/

                //eval(getURLDecode(pagePrefs.scriptBlockDict.addStoreMarker105));
                image = new google.maps.MarkerImage('../images/maps/scotts_map_marker_neighboring.png',
                // This marker is 40 pixels wide by 28 pixels tall.
                new google.maps.Size(40, 28),
                // The origin for this image is 0,0.
                new google.maps.Point(0, 0),
                // The anchor for this image is the point of the marker at 20, 27.
                new google.maps.Point(20, 27));
                shadow = new google.maps.MarkerImage('',
                // The shadow image is larger in the horizontal dimension
                // while the position and offset are the same as for the main image.
                new google.maps.Size(0, 0),
                new google.maps.Point(0, 0),
                new google.maps.Point(00, 0));
                shape = {
                    coord: [0, 9, 3, 3, 12, 0, 29, 0, 36, 3, 40, 9, 34, 17, 23, 20, 20, 27, 18, 27, 17, 20, 6, 18],
                    type: 'poly'
                };

                content_string = "";
                // content_string = '<b>Store Id:</b> ' + ((location[0].indexOf('^') > -1) ? location[0].split('^')[0] : "");
                content_string += '<b>Store Name:</b> ' + ((location[0].indexOf('^') > -1 && location[0].split('^').length > 1) ? location[0].split('^')[1] : "");
                myLatLng = new google.maps.LatLng(location[1], location[2]);
                marker = new google.maps.Marker({
                    position: myLatLng,
                    map: map,
                    shadow: shadow,
                    icon: image,
                    shape: shape,
                    draggable: false,
                    contentString: content_string,
                    title: 'Scotts'
                });
                break;
            case ACE:
                /*The below code and its related variable should be removed after demo / when the attribute is available in service.  -- START*/
                //if ((i % 2) > 0 && total_declined_markers <= declined_markers_cnt) {
                //    if (location[0].split('^')[3] == "False") {
                //        location[0] = location[0].substring(0, location[0].lastIndexOf('^') + 1);
                //        location[0] = location[0] + 'True';
                //    }
                //    if (location[0].split('^')[3] == "True") {
                //        total_declined_markers++;
                //        location[0] = location[0] + '^True';
                //    }
                //}
                /*The above code and its related variable should be removed after demo / when the attribute is available in service.  -- END*/

                store_customer = location[0].split('^');
                //eval(getURLDecode(pagePrefs.scriptBlockDict.addLocationMarkers155711));
                //if (store_customer[0] == "2916") {
                //    location[0] = location[0] + '^False';
                //} else if (store_customer[0] == "6") {
                //    location[0] = location[0] + '^True';
                //}
                locations_list = [];
                if (location[0].indexOf('^') > -1)
                    locations_list = location[0].split('^');
                is_location_in_order = false;
                is_location_in_order = (locations_list.length > 3) ? JSON.parse(locations_list[3].toLowerCase()) : ((locations_list.length <= 3) ? true : false);

                /*The below code and its related variable should be removed after demo / when the attribute is available in service.  -- START*/
                //var is_declined = false;
                //is_declined = (locations_list.length > 4) ? JSON.parse(locations_list[4].toLowerCase()) : ((locations_list.length <= 4) ? false : false);
                //var img_path = "";
                //if (is_location_in_order && is_declined)
                //    img_path = '../images/maps/caseys_map_marker_small_declined.png';
                //else if (is_location_in_order && !is_declined)
                //    img_path = '../images/maps/caseys_map_marker_small.png';
                //else if (!is_location_in_order)
                //    img_path = '../images/maps/caseys_map_marker_small_grey.png';
                /*The above code and its related variable should be removed after demo / when the attribute is available in service.  -- END*/

                image = new google.maps.MarkerImage(((is_location_in_order) ? '../images/maps/ace_map_optedin_marker.png' : '../images/maps/ace_map_optedout_marker.png'),
                //image = new google.maps.MarkerImage(img_path,
                new google.maps.Size(26, 31),
                new google.maps.Point(0, 0),
                new google.maps.Point(13,30));
                shadow = new google.maps.MarkerImage('../images/maps/ace_map_optedin_marker.png',
                new google.maps.Size(0,0),
                new google.maps.Point(0, 0),
                new google.maps.Point(00,0));
                shape = {
                    coord: [0, 13, 3, 4, 13, 0, 22, 4, 26, 13, 21, 22, 13, 31, 4, 22],
                    type: 'poly'
                };
                myLatLng = new google.maps.LatLng(location[1], location[2]);

                marker = new google.maps.Marker({
                    position: myLatLng,
                    map: map,
                    shadow: shadow,
                    icon: image,
                    shape: shape,
                    draggable: false,
                    contentString: 'Store Id: ' + store_customer[0],
                    title: store_customer[1]
                });
                break;
            case ANDERSEN:
                /*The below code and its related variable should be removed after demo / when the attribute is available in service.  -- START*/
                if ((i % 2) > 0 && total_declined_markers <= declined_markers_cnt) {
                    if (location[0].split('^')[3] == "False") {
                        location[0] = location[0].substring(0, location[0].lastIndexOf('^') + 1);
                        location[0] = location[0] + 'True';
                    }
                    if (location[0].split('^')[3] == "True") {
                        total_declined_markers++;
                        location[0] = location[0] + '^True';
                    }
                }
                /*The above code and its related variable should be removed after demo / when the attribute is available in service.  -- END*/
                store_customer = location[0].split('^');
                //eval(getURLDecode(pagePrefs.scriptBlockDict.addStoreMarker364));
                if (store_customer[0] == "2916") {
                    location[0] = location[0] + '^False';
                } else if (store_customer[0] == "6") {
                    location[0] = location[0] + '^True';
                }
                locations_list = [];
                if (location[0].indexOf('^') > -1)
                    locations_list = location[0].split('^');
                is_location_in_order = false;
                is_location_in_order = (locations_list.length > 3) ? JSON.parse(locations_list[3].toLowerCase()) : ((locations_list.length <= 3) ? true : false);

                /*The below code and its related variable should be removed after demo / when the attribute is available in service.  -- START*/
                //is_declined = false;
                //is_declined = (locations_list.length > 4) ? JSON.parse(locations_list[4].toLowerCase()) : ((locations_list.length <= 4) ? false : false);
                //var img_path = "";
                //if (is_location_in_order && is_declined)
                //    img_path = '../images/maps/and_out_map_marker.png';
                //else if (is_location_in_order && !is_declined)
                //    img_path = '../images/maps/and_in_map_marker.png';
                //else if (!is_location_in_order)
                //    img_path = '../images/maps/and_out_map_marker.png';
                /*The above code and its related variable should be removed after demo / when the attribute is available in service.  -- END*/

                image = new google.maps.MarkerImage(((is_location_in_order) ? '../images/maps/and_in_map_marker.png' : '../images/maps/and_out_map_marker.png'),

                // This marker is 24 pixels wide by 25 pixels tall.
                new google.maps.Size(24, 25),
                // The origin for this image is 0,0.
                new google.maps.Point(0, 0),
                // The anchor for this image is the point of the marker at 14,25.
                new google.maps.Point(14, 25));
                shadow = new google.maps.MarkerImage('../images/maps/and_in_map_marker.png',
                // The shadow image is larger in the horizontal dimension
                // while the position and offset are the same as for the main image.
                new google.maps.Size(0, 0),
                new google.maps.Point(0, 0),
                new google.maps.Point(00, 0));
                shape = {
                    coord: [0, 0, 24, 0, 24, 20, 17, 20, 12, 25, 8, 20, 0, 20],
                    type: 'poly'
                };
                myLatLng = new google.maps.LatLng(location[1], location[2]);

                marker = new google.maps.Marker({
                    position: myLatLng,
                    map: map,
                    shadow: shadow,
                    icon: image,
                    shape: shape,
                    draggable: false,
                    contentString: 'Store Id: ' + store_customer[0],
                    title: store_customer[1]
                });
                break;
            case SHERWINWILLIAMS:              
                /*The below code and its related variable should be removed after demo / when the attribute is available in service.  -- START*/
                if ((i % 2) > 0 && total_declined_markers <= declined_markers_cnt) {
                    if (location[0].split('^')[3] == "False") {
                        location[0] = location[0].substring(0, location[0].lastIndexOf('^') + 1);
                        location[0] = location[0] + 'True';
                    }
                    if (location[0].split('^')[3] == "True") {
                        total_declined_markers++;
                        location[0] = location[0] + '^True';
                    }
                }
                /*The above code and its related variable should be removed after demo / when the attribute is available in service.  -- END*/
                store_customer = location[0].split('^');
                //eval(getURLDecode(pagePrefs.scriptBlockDict.addStoreMarker364));
                if (store_customer[0] == "2916") {
                    location[0] = location[0] + '^False';
                } else if (store_customer[0] == "6") {
                    location[0] = location[0] + '^True';
                }
                locations_list = [];
                if (location[0].indexOf('^') > -1)
                    locations_list = location[0].split('^');
                is_location_in_order = false;
                is_location_in_order = (locations_list.length > 3) ? JSON.parse(locations_list[3].toLowerCase()) : ((locations_list.length <= 3) ? true : false);

                /*The below code and its related variable should be removed after demo / when the attribute is available in service.  -- START*/
                //is_declined = false;
                //is_declined = (locations_list.length > 4) ? JSON.parse(locations_list[4].toLowerCase()) : ((locations_list.length <= 4) ? false : false);
                //var img_path = "";
                //if (is_location_in_order && is_declined)
                //    img_path = '../images/maps/sw_out_map_marker.png';
                //else if (is_location_in_order && !is_declined)
                //    img_path = '../images/maps/sw_in_map_marker.png';
                //else if (!is_location_in_order)
                //    img_path = '../images/maps/sw_out_map_marker.png';
                /*The above code and its related variable should be removed after demo / when the attribute is available in service.  -- END*/

                image = new google.maps.MarkerImage(((is_location_in_order) ? '../images/maps/sw_in_map_marker.png' : '../images/maps/sw_out_map_marker.png'),

                // This marker is 24 pixels wide by 25 pixels tall.
                new google.maps.Size(25, 36),
                // The origin for this image is 0,0.
                new google.maps.Point(0, 0),
                // The anchor for this image is the point of the marker at 14,25.
                new google.maps.Point(13, 33));
                shadow = new google.maps.MarkerImage('../images/maps/and_in_map_marker.png',
                // The shadow image is larger in the horizontal dimension
                // while the position and offset are the same as for the main image.
                new google.maps.Size(0, 0),
                new google.maps.Point(0, 0),
                new google.maps.Point(00, 0));
                shape = {
                    coord: [0, 9, 8, 0, 11, 0, 22, 12, 15, 21, 24, 31, 24, 34, 1, 35, 2, 27, 8, 20],
                    type: 'poly'
                };
                myLatLng = new google.maps.LatLng(location[1], location[2]);

                marker = new google.maps.Marker({
                    position: myLatLng,
                    map: map,
                    shadow: shadow,
                    icon: image,
                    shape: shape,
                    draggable: false,
                    contentString: 'Store Id: ' + store_customer[0],
                    title: store_customer[1]
                });
                break;
            case TRAEGER:
                /*The below code and its related variable should be removed after demo / when the attribute is available in service.  -- START*/
                if ((i % 2) > 0 && total_declined_markers <= declined_markers_cnt) {
                    if (location[0].split('^')[3] == "False") {
                        location[0] = location[0].substring(0, location[0].lastIndexOf('^') + 1);
                        location[0] = location[0] + 'True';
                    }
                    if (location[0].split('^')[3] == "True") {
                        total_declined_markers++;
                        location[0] = location[0] + '^True';
                    }
                }
                /*The above code and its related variable should be removed after demo / when the attribute is available in service.  -- END*/
                store_customer = location[0].split('^');
            
                if (store_customer[0] == "2916") {
                    location[0] = location[0] + '^False';
                } else if (store_customer[0] == "6") {
                    location[0] = location[0] + '^True';
                }
                locations_list = [];
                if (location[0].indexOf('^') > -1)
                    locations_list = location[0].split('^');
                is_location_in_order = false;
                is_location_in_order = (locations_list.length > 3) ? JSON.parse(locations_list[3].toLowerCase()) : ((locations_list.length <= 3) ? true : false);

                /*The below code and its related variable should be removed after demo / when the attribute is available in service.  -- START*/
                //is_declined = false;
                //is_declined = (locations_list.length > 4) ? JSON.parse(locations_list[4].toLowerCase()) : ((locations_list.length <= 4) ? false : false);
                //var img_path = "";
                //if (is_location_in_order && is_declined)
                //    img_path = '../images/maps/tr_map_optedout_marker.png';
                //else if (is_location_in_order && !is_declined)
                //    img_path = '../images/maps/tr_map_optedin_marker.png';
                //else if (!is_location_in_order)
                //    img_path = '../images/maps/tr_map_optedout_marker.png';
                /*The above code and its related variable should be removed after demo / when the attribute is available in service.  -- END*/

                //eval(getURLDecode(pagePrefs.scriptBlockDict.addStoreMarker381));
                image = new google.maps.MarkerImage(((is_location_in_order) ? '../images/maps/tr_map_optedin_marker.png' : '../images/maps/tr_map_optedout_marker.png'),
                // This marker is 47 pixels wide by 21 pixels tall.
                new google.maps.Size(47, 21),
                // The origin for this image is 0,0.
                new google.maps.Point(0, 0),
                // The anchor for this image is the point of the marker at 24, 11.
                new google.maps.Point(24, 11));
                shadow = new google.maps.MarkerImage('../images/maps/tr_map_optedin_marker.png',
               // The shadow image is larger in the horizontal dimension
               // while the position and offset are the same as for the main image.
               new google.maps.Size(0, 0),
               new google.maps.Point(0, 0),
               new google.maps.Point(00, 0));
                shape = {
                    coord: [0, 0, 24, 0, 24, 11, 0, 11],
                    type: 'poly'
                };
                myLatLng = new google.maps.LatLng(location[1], location[2]);

                marker = new google.maps.Marker({
                    position: myLatLng,
                    map: map,
                    shadow: shadow,
                    icon: image,
                    shape: shape,
                    draggable: false,
                    contentString: 'Store Id: ' + store_customer[0],
                    title: store_customer[1]
                });
               break;
            case BENJAMINMOORE:
                /*The below code and its related variable should be removed after demo / when the attribute is available in service.  -- START*/
                if ((i % 2) > 0 && total_declined_markers <= declined_markers_cnt) {
                    if (location[0].split('^')[3] == "False") {
                        location[0] = location[0].substring(0, location[0].lastIndexOf('^') + 1);
                        location[0] = location[0] + 'True';
                    }
                    if (location[0].split('^')[3] == "True") {
                        total_declined_markers++;
                        location[0] = location[0] + '^True';
                    }
                }
                /*The above code and its related variable should be removed after demo / when the attribute is available in service.  -- END*/
                store_customer = location[0].split('^');
                //eval(getURLDecode(pagePrefs.scriptBlockDict.addStoreMarker364));
                if (store_customer[0] == "2916") {
                    location[0] = location[0] + '^False';
                } else if (store_customer[0] == "6") {
                    location[0] = location[0] + '^True';
                }
                locations_list = [];
                if (location[0].indexOf('^') > -1)
                    locations_list = location[0].split('^');
                is_location_in_order = false;
                is_location_in_order = (locations_list.length > 3) ? JSON.parse(locations_list[3].toLowerCase()) : ((locations_list.length <= 3) ? true : false);

                /*The below code and its related variable should be removed after demo / when the attribute is available in service.  -- START*/
                //is_declined = false;
                //is_declined = (locations_list.length > 4) ? JSON.parse(locations_list[4].toLowerCase()) : ((locations_list.length <= 4) ? false : false);
                //var img_path = "";
                //if (is_location_in_order && is_declined)
                //    img_path = '../images/maps/bm_map_optedin_marker.png';
                //else if (is_location_in_order && !is_declined)
                //    img_path = '../images/maps/bm_map_optedin_marker.png';
                //else if (!is_location_in_order)
                //    img_path = '../images/maps/bm_map_optedin_marker.png';
                /*The above code and its related variable should be removed after demo / when the attribute is available in service.  -- END*/
                //eval(getURLDecode(pagePrefs.scriptBlockDict.addStoreMarker384));
                image = new google.maps.MarkerImage(((is_location_in_order) ? '../images/maps/bm_map_optedin_marker.png' : '../images/maps/bm_map_optedin_marker.png'),
                // This marker is 20 pixels wide by 16 pixels tall.
                new google.maps.Size(20, 16),
                // The origin for this image is 0,0.
                new google.maps.Point(0, 0),
                // The anchor for this image is the point of the marker at 9, 10.
                new google.maps.Point(9, 10));
                shadow = new google.maps.MarkerImage('../images/maps/bm_map_optedin_marker.png',
                // The shadow image is larger in the horizontal dimension
                // while the position and offset are the same as for the main image.
                new google.maps.Size(0, 0),
                new google.maps.Point(0, 0),
                new google.maps.Point(00, 0));
                shape = {
                    coord: [10, 0, 20, 15, 0, 15],
                    type: 'poly'
                };

                myLatLng = new google.maps.LatLng(location[1], location[2]);
                marker = new google.maps.Marker({
                    position: myLatLng,
                    map: map,
                    shadow: shadow,
                    icon: image,
                    shape: shape,
                    draggable: false,
                    contentString: 'Store Id: ' + store_customer[0],
                    title: store_customer[1]
                });
                break;
            case MILWAUKEE:
                /*The below code and its related variable should be removed after demo / when the attribute is available in service.  -- START*/
                if ((i % 2) > 0 && total_declined_markers <= declined_markers_cnt) {
                    if (location[0].split('^')[3] == "False") {
                        location[0] = location[0].substring(0, location[0].lastIndexOf('^') + 1);
                        location[0] = location[0] + 'True';
                    }
                    if (location[0].split('^')[3] == "True") {
                        total_declined_markers++;
                        location[0] = location[0] + '^True';
                    }
                }
                /*The above code and its related variable should be removed after demo / when the attribute is available in service.  -- END*/
                store_customer = location[0].split('^');
                //eval(getURLDecode(pagePrefs.scriptBlockDict.addStoreMarker364));
                if (store_customer[0] == "2916") {
                    location[0] = location[0] + '^False';
                } else if (store_customer[0] == "6") {
                    location[0] = location[0] + '^True';
                }
                locations_list = [];
                if (location[0].indexOf('^') > -1)
                    locations_list = location[0].split('^');
                is_location_in_order = false;
                is_location_in_order = (locations_list.length > 3) ? JSON.parse(locations_list[3].toLowerCase()) : ((locations_list.length <= 3) ? true : false);

                /*The below code and its related variable should be removed after demo / when the attribute is available in service.  -- START*/
                //is_declined = false;
                //is_declined = (locations_list.length > 4) ? JSON.parse(locations_list[4].toLowerCase()) : ((locations_list.length <= 4) ? false : false);
                //var img_path = "";
                //if (is_location_in_order && is_declined)
                //    img_path = '../images/maps/mi_map_optedout_marker.png';
                //else if (is_location_in_order && !is_declined)
                //    img_path = '../images/maps/mi_map_optedin_marker.png';
                //else if (!is_location_in_order)
                //    img_path = '../images/maps/mi_map_optedout_marker.png';
                /*The above code and its related variable should be removed after demo / when the attribute is available in service.  -- END*/
                //eval(getURLDecode(pagePrefs.scriptBlockDict.addStoreMarker385));
                image = new google.maps.MarkerImage(((is_location_in_order) ? '../images/maps/mi_map_optedin_marker.png' : '../images/maps/mi_map_optedout_marker.png'),
                // This marker is 18 pixels wide by 23 pixels tall.
                new google.maps.Size(18, 23),
                // The origin for this image is 0,0.
                new google.maps.Point(0, 0),
                // The anchor for this image is the point of the marker at 9, 22.
                new google.maps.Point(9, 22));
                shadow = new google.maps.MarkerImage('../images/maps/mi_map_optedin_marker.png',
                // The shadow image is larger in the horizontal dimension
                // while the position and offset are the same as for the main image.
                new google.maps.Size(0, 0),
                new google.maps.Point(0, 0),
                new google.maps.Point(00, 0));
                shape = {
                    coord: [0, 17, 0, 0, 18, 0, 18, 17, 12, 17, 8, 22, 5, 17],
                    type: 'poly'
                };

                myLatLng = new google.maps.LatLng(location[1], location[2]);

                marker = new google.maps.Marker({
                    position: myLatLng,
                    map: map,
                    shadow: shadow,
                    icon: image,
                    shape: shape,
                    draggable: false,
                    contentString: 'Store Id: ' + store_customer[0],
                    title: store_customer[1]
                });
                break;
            case ICYNENE:
                //eval(getURLDecode(pagePrefs.scriptBlockDict.addStoreMarker298));
                image = new google.maps.MarkerImage('../images/maps/icy_neighboring_map_marker.png',
                // This marker is 21 pixels wide by 29 pixels tall.
                new google.maps.Size(21, 29),
                // The origin for this image is 0,0.
                new google.maps.Point(0, 0),
                // The anchor for this image is the point of the marker at 10,29.
                new google.maps.Point(10, 29));
                shadow = new google.maps.MarkerImage('',
                // The shadow image is larger in the horizontal dimension
                // while the position and offset are the same as for the main image.
                new google.maps.Size(0, 0),
                new google.maps.Point(0, 0),
                new google.maps.Point(00, 0));
                shape = {
                    coord: [0, 7, 0, 10, 21, 7, 21, 18, 13, 23, 10, 29, 7, 23, 0, 18],
                    type: 'poly'
                };
                content_string = "";
                // content_string = '<b>Store Id:</b> ' + ((location[0].indexOf('^') > -1) ? location[0].split('^')[0] : "");
                content_string += '<b>Store Name:</b> ' + ((location[0].indexOf('^') > -1 && location[0].split('^').length > 1) ? location[0].split('^')[1] : "");
                myLatLng = new google.maps.LatLng(location[1], location[2]);
                marker = new google.maps.Marker({
                    position: myLatLng,
                    map: map,
                    shadow: shadow,
                    icon: image,
                    shape: shape,
                    draggable: false,
                    contentString: content_string,
                    title: 'Icynene'
                });
                break;
            default:
                break;
        }

        locations_markers_A.push(marker);

        google.maps.event.addListener(marker, 'click', function () {
            info_window.setContent(this.contentString);
            info_window.open(map, this);
        });
    }
    for (var k = 0; k < locations_markers_A.length; k++) {
        locations_markers_A[k].setMap(map);
    }
}
//------- START CRRT -------
function addCrrts() {
    // Add markers to the map
    // Marker sizes are expressed as a Size of X,Y
    // where the origin of the image (0,0) is located
    // in the top left of the image.

    // Origins, anchor positions and coordinates of the marker
    // increase in the X direction to the right and in
    // the Y direction down.
    var shape = {
        coord: [0, 0, 34, 0, 34, 14, 18, 15, 18, 20, 16, 20, 15, 14, 0, 14],
        type: 'poly'
    };
    for (var i = 0; i < gCrrtsA.length; i++) {
        var crrt = gCrrtsA[i];
        if (crrt != undefined) {
            //var the_category = makeCrrtCategory(crrt[4]);
            var the_category = "yellow";
            var image = new google.maps.MarkerImage(logoPath + 'cr_pins/' + the_category + '/' + crrt[0] + '.png',
            // This marker is 20 pixels wide by 32 pixels tall.
                new google.maps.Size(35, 21),
            // The origin for this image is 0,0.
                new google.maps.Point(0, 0),
            // The anchor for this image is the base of the flagpole at 0,32.
                new google.maps.Point(17, 20));
            var shadow = new google.maps.MarkerImage(logoPath + 'cr_pins/cr_shadow.png',
            // The shadow image is larger in the horizontal dimension
            // while the position and offset are the same as for the main image.
                new google.maps.Size(44, 21),
                new google.maps.Point(0, 0),
                new google.maps.Point(17, 20));
            // Shapes define the clickable region of the icon.
            // The type defines an HTML <area> element 'poly' which
            // traces out a polygon as a series of X,Y points. The final
            // coordinate closes the poly by connecting to the first
            // coordinate.
            var myLatLng = new google.maps.LatLng(crrt[1], crrt[2]);
            var marker = new google.maps.Marker({
                position: myLatLng,
                map: map,
                shadow: shadow,
                icon: image,
                shape: shape,
                //title: "Count: " + crrt[4],
                title: crrt[4] + ' of ' + crrt[5] + ' targeted',
                zIndex: crrt[3]
            });
            gCrrtMarkersA.push(marker);
        }
        for (var a = 0; a < gCrrtMarkersA.length; a++) {
            gCrrtMarkersA[a].setMap(map);
        }

    }
}

var geoJSON2JTS = function (boundaries) {
    var coordinates = [];
    for (var i = 0; i < boundaries.length; i++) {
        coordinates.push(new jsts.geom.Coordinate(
            boundaries[i][1], boundaries[i][0]));
    }
    return coordinates;
};

var googleMaps2JTS = function (boundaries) {
    var coordinates = [];
    for (var i = 0; i < boundaries.getLength() ; i++) {
        coordinates.push(new jsts.geom.Coordinate(
            boundaries.getAt(i).lat(), boundaries.getAt(i).lng()));
    }
    return coordinates;
};

/**
 * findSelfIntersects
 *
 * Detect self-intersections in a polygon.
 *
 * @param {object} google.maps.Polygon path co-ordinates.
 * @return {array} array of points of intersections.
 */
var findSelfIntersects = function (googlePolygonPath) {
    //try {
        var coordinates = googleMaps2JTS(googlePolygonPath);
        var geometryFactory = new jsts.geom.GeometryFactory();
        var shell = geometryFactory.createLinearRing(coordinates);
        var jstsPolygon = geometryFactory.createPolygon(shell);

        // if the geometry is aleady a simple linear ring, do not
        // try to find self intersection points.
        var validator = new jsts.operation.IsSimpleOp(jstsPolygon);
        return (validator.isSimpleLinearGeometry(jstsPolygon)) ? true : false;
    //}
    //catch (e) {
    //    console.log(e);
    //}
   /* var res = [];
    var graph = new jsts.geomgraph.GeometryGraph(0, jstsPolygon);
    var cat = new jsts.operation.valid.ConsistentAreaTester(graph);
    var r = cat.isNodeConsistentArea();
    if (!r) {
        var pt = cat.getInvalidPoint();
        res.push([pt.x, pt.y]);
    }
    return res;*/
};

/*
function makeCrrtCategory(the_count){
var the_cat = "yellow";
if (the_count <= 10){
the_cat = "cyan";
}else if (the_count <= 50){
the_cat = "green";
}else if (the_count <= 100){
the_cat = "yellow";
}else if (the_count <= 200){
the_cat = "orange";
}else{
the_cat = "red";
}
return the_cat;
}
*/
/*
function showHideCrrts(){
//var crrt_checkbox = document.getElementById('showHideCrrtMarkers');
addCrrts(map, gCrrtsA);
if (gCrrtsA){
for (i in gCrrtMarkersA){
//if (crrt_checkbox.checked){
gCrrtMarkersA[i].setMap(map);
//}else{
//  gCrrtMarkersA[i].setMap(null);
//}
}
}
}
function showHideZips(){
var zip_checkbox = document.getElementById('showHideZipCheck');
if (ctaLayer){
if (zip_checkbox.checked){
ctaLayer.setMap(map);
}else{
ctaLayer.setMap(null);
}
}
}
*/
//------- END CRRT
// Register an event listener to fire when the page finishes loading.
//    google.maps.event.addDomListener(window, 'load', init);