﻿jQuery.support.cors = true;

//******************** Global Variables Start **************************
var gData;
var selectedStoreId;
var gDataStartIndex = 0;
var gInnerData = [];
var storeData = "";
var urlString = unescape(window.location);
var queryString = urlString.substr(urlString.indexOf("?") + 1, urlString.length)
var params = queryString.split("&");
var encryptedString = (params[0] != undefined && params[0] != "") ? params[0] : "";

var email = "";
var user = "";
var password = "";
var customerNumber = "";
var jobNumber = "";
var jobTypeId = "0";
var facilityId = "";
var approvalData = "";

var approvalEncryptedURL = serviceURLDomain + 'api/Encryption_decrypt/';
var approvalURL = serviceURLDomain + 'api/ManagerApproval_stores/';
var approvalPOSTUrl = serviceURLDomain + 'api/ManagerApproval_stores/';

//******************** Global Variables End **************************

//******************** Page Load Events Start **************************
$('#_districtApproval').live('pagebeforecreate', function (event) {
    displayMessage('_districtApproval');
    createConfirmMessage('_districtApproval');
    loadingImg('_districtApproval');
    $.getJSON("JSON/_approval.JSON", function (data) {
        approvalData = data;
    });
    //test for mobility...
    var is_mobile = false;
    mobile = ['iphone', 'ipod', 'android', 'blackberry', 'nokia', 'opera mini', 'windows mobile', 'windows phone', 'iemobile'];
    for (var i in mobile) if (navigator.userAgent.toLowerCase().indexOf(mobile[i].toLowerCase()) > 0) {
        is_mobile = true;
    }
    if (localStorage.mobile) { // desktop storage 
        is_mobile = true;
    }
    if (!is_mobile) {
        $('head').append('<link rel="stylesheet" type="text/css" href="../css/screen.css" media="screen" />');
    }

    if ($.browser.msie && jQuery.browser.version.substr(0, 1) == '6') {
        alert("This page does not support Internet Explorer 6. Please consider downloading a newer browser. Click 'OK' to close");
        window.open('', '_self', '');
        window.close();
        return false;
    }
    else {
        window.setTimeout(function () {
            getData();
        }, 1500);
    }
});

//******************** Public Functions Start **************************
function getData() {
    makeGData();
}

function makeGData() {
    getCORS(approvalEncryptedURL + encryptedString, null, function (url_with_data) {
        //angie.white@caseys.com|admin_cgs1|admin_cgs1|155711|83965|1
        var data_split = [];
        if (url_with_data != undefined && url_with_data != "") {
            data_split = url_with_data.split('|');
            email = (data_split[0] != undefined && data_split[0] != "") ? data_split[0] : "";
            user = (data_split[1] != undefined && data_split[1] != "") ? data_split[1] : "";
            password = (data_split[2] != undefined && data_split[2] != "") ? data_split[2] : "";
            customerNumber = (data_split[3] != undefined && data_split[3] != "") ? data_split[3] : "";
			//customerNumber = JETS; // TO be removed after Jet's Demo.
            jobNumber = (data_split[4] != undefined && data_split[4] != "") ? data_split[4] : "";
            facilityId = (data_split[5] != undefined && data_split[5] != "") ? data_split[5] : "";
            jobTypeId = (data_split[6] != undefined && data_split[6] != "") ? data_split[6] : "0";
            //jobTypeId = 52;//for Jets Demo
        }
        sessionStorage.authString = makeBasicAuth(user, password);
        var manager_Email = "";
        var store_list = [];
        switch (customerNumber) {
            case JETS:
                manager_Email = "joan.eglseder@caseys.com";
                store_list.push(
                {
                    "storeId": "IL-017",
                    "city": "Champaign",
                    "state": "IL",
                    "isChecked": 0,
                    "targetedPretty": "0",
                    "costPretty": "$0.00",
                    "selectedPostcard": ""
                },
                {
                    "storeId": "MI-056",
                    "city": "Lansing",
                    "state": "MI",
                    "isChecked": 0,
                    "targetedPretty": "0",
                    "costPretty": "$0.00",
                    "selectedPostcard": ""
                }
            );
        }
        if (customerNumber == JETS) {
            var temp_json = {
                "managerEmail": manager_Email,
                "description": (jobTypeId == 71) ? "December 2015 Opt-in Postcard" : "April 2016 Opt-In Postcard",
                "inHomeDate": (jobTypeId == 71) ? "12/31/2016" : "12/31/2016",
                "optInEndDate": (jobTypeId == 71) ? "12/31/2016" : "12/31/2016",
                "approveByEndDate": "12/20/2016",
                "storeList": store_list
            };
            gData = temp_json;
            displayStoresList();
            displayImages(customerNumber);
            setOptInApprovalLogos(customerNumber);
        }
        else {
            getCORS(approvalURL + customerNumber + '/' + facilityId + '/' + jobNumber + '/' + email + '/approve', null, function (data) {
                //$.getJSON("JSON/_districtApproval.JSON", function (data) {
                gData = data;
                displayStoresList();
                displayImages(customerNumber);
                setOptInApprovalLogos(customerNumber);
            }, function (error_response) {
                showErrorResponseText(error_response);
            });
        }
    }, function (error_response) {
        showErrorResponseText(error_response);
    });

}
function displayImages(customer_number) {
    if (customer_number == CASEYS) {
        $("#headerText").text(approvalData[jobTypeId].headerText);
        $("#paragraphOrderApproval").text(approvalData[jobTypeId].paragraphOptIn);
        $("#orderedListInfo").html(approvalData[jobTypeId].orderedListInfo);
        $("#paragraphUserMessage").text(approvalData[jobTypeId].paragraphUserMessage);

        var images_path = optInImagesPath + jobNumber + "_";
        if (jobTypeId == 72) {
            var img_front = images_path + approvalData[jobTypeId].pizzaVersion1Source;
            var img_back = images_path + approvalData[jobTypeId].pizzaVersion2Source;
            $('#trDeliveryImgPizzaVersion').css('display', 'none');
            $('#imagePizzaVersion1').attr('src', img_front);
            $('#imgPizzaVersion1Name').html(approvalData[jobTypeId].pizzaVersion1Name);
            $('#imagePizzaVersion1').attr('alt', approvalData[jobTypeId].pizzaVersion1AlternateText);
            $('#imagePizzaVersion2').attr('src', img_back);
            $('#imgPizzaVersion2Name').html(approvalData[jobTypeId].pizzaVersion2Name);
            $('#imagePizzaVersion2').attr('alt', approvalData[jobTypeId].pizzaVersion2AlternateText);
            $('#aDOO').attr('href', images_path + approvalData[jobTypeId].dooReferenceLink);
            $('#aPostCard').attr('href', images_path + approvalData[jobTypeId].postcardReferenceLink);
            $("#paragraphCriticalNote").html(approvalData[jobTypeId].paragraphNote);
            $('#paragraphDeliveryNote').css('display', 'none');
            $('#trDeliveryOfferLink1').css('display', 'none');
            $('#trDeliveryOfferLink2').css('display', 'none');
            $('#trDeliveryOfferLink3').css('display', 'none');
        }
        else if (jobTypeId == 71) {
            $('#trImgPizzaVersion').css('display', 'none');
            var img_front = images_path + approvalData[jobTypeId].pizzaVersion1Source;
            var img_back = images_path + approvalData[jobTypeId].pizzaVersion2Source;
            $('#imageOffer1').attr('src', img_front);
            $('#imageOffer2').attr('src', img_back);
            $('#lnkOffer1').text(approvalData[jobTypeId].offer1Name);
            $('#lnkOffer1').attr('href', images_path + approvalData[jobTypeId].offer1Link);
            $('#lnkOffer2').text(approvalData[jobTypeId].offer2Name);
            $('#lnkOffer2').attr('href', images_path + approvalData[jobTypeId].offer2Link);
            $('#lnkOffer3').text(approvalData[jobTypeId].offer3Name);
            $('#lnkOffer3').attr('href', images_path + approvalData[jobTypeId].offer3Link);
            $("#paragraphDeliveryNote").html(approvalData[jobTypeId].paragraphNote);
            $('#paragraphCriticalNote').css('display', 'none');
        }
    }
    else if (customer_number == JETS) {
        $("#headerText").text(approvalData[jobTypeId].headerText);
        $("#paragraphOrderApproval").text(approvalData[jobTypeId].paragraphOptIn);
        $("#orderedListInfo").html(approvalData[jobTypeId].orderedListInfo);
        $("#paragraphUserMessage").text(approvalData[jobTypeId].paragraphUserMessage);
        $("#paragraphNote").text(approvalData[jobTypeId].paragraphNote);
        var images_path = logoPath + "JetsPizza/";
        var img_front = images_path + "Grand_Opening_6x11_Postcard.png";
        var img_back = images_path + "Celebrate_Spring_6x11_Postcard.png";
        $('#imagePizzaVersion1').attr('src', img_front);
        $('#imgPizzaVersion1Name').html("Grand Opening 6x11 Postcard*");
        $('#imagePizzaVersion1').attr('alt', 'Grand Opening 6x11 Postcard');
        $('#imagePizzaVersion2').attr('src', img_back);
        $('#imgPizzaVersion2Name').html("Celebrate Spring 6x11 Postcard*");
        $('#imagePizzaVersion2').attr('alt', 'Celebrate Spring 6x11 Postcard');
        $('#aDOO').attr('href', images_path + "Grand_Opening_6x11_Postcard.pdf");
        $('#aPostCard').attr('href', images_path + "Celebrate_Spring_6x11_Postcard.pdf");
    }



}
//load the store locations...
function displayStoresList() {
    storeData = "";
    if ((gData == null || gData == "") || (gData.storeList == undefined) || (gData.storeList.length == 0)) {
        var empty_message = "<br/><div style='font-size: 12px; font-weight: bold; color:red;'>Attention. No stores were found for your user login. " +
                            " Please contact caseysgeneralstore@tribunedirect.com if you have any questions.</font>";
        $('#spText').html(empty_message);
        $('#trInstructions').hide();
        $('#dvQuestion').hide();
        $('#textarea').css('display', 'none');
        $('#btnSubmitApproval').css('display', 'none');
        $('#trImgPizzaVersion').css("display", "none");
        $('#optInContents').css('display', 'none');
    } else {
        var the_text = "";
        var approvals_end_date = new Date(gData.approveByEndDate); //new Date("06/03/2016");
        var current_date = new Date();
        var approvals_end_date_diff = dateDifference(approvals_end_date, current_date);
        if (parseInt(approvals_end_date_diff) >= 1) {
            the_text = '<div style="font-size: 12px; font-weight: bold; color:red;text-align:center;"><br />The Approval period has Closed.</div>';
            $('#btnSubmitApproval').hide();
            $('#trInstructions').hide();
            $('#dvQuestion').hide();
            $('#textarea').hide();
            $('#spText').html(the_text);
            $('#trImgPizzaVersion').css("display", "none");
            $('#optInContents').css('display', 'none');
        } else {

            the_text = '<div style="font-size: 12px; font-weight: normal">Please review the cost and counts for each of the stores below. ' +
                       'To provide your approval check the approved stores and click Submit Approvals.</div>';
            $.each(gData, function (key, val) {
                switch (key.toLowerCase()) {
                    case "description":
                        if (jobTypeId != 71) {
                            storeData += '<li><b>Campaign: </b>' + val + '</li>';
                            $('#navHeader').html(val);
                        }
                        break;
                    case "inhomedate":
                        storeData += '<li><b>In-Home ' + ((jobTypeId == 71) ? 'Date' : 'Start') + ':</b> ' + val + '</li>';
                        break;
                    case "approvebyenddate":               //approvaldate                        
                        storeData += '<li style="color:#C00"><b>Approval Period Ends: </b>' + val + '</li>';
                        $("#hdnApprovalPeriod").val(val);
                        break;
                    case "emailnotes":
                        $('#textarea').text(val);
                        break;
                }
            });
            $('#ulPostCardInfo').html(storeData);
            buildStores();
        }
    }
}
//to display the "All Stores"...
function buildStores() {
    var li_chk_routes = "";
    li_chk_routes = '<fieldset id="optInStoresSet" data-role="controlgroup" data-mini="true" data-theme="c" data-inset="false">';

    li_chk_routes += '<input type="checkbox" name="checkbox-100a" id="checkbox-100a" class="custom" data-theme="a" onclick="checkCheckboxes(this.id);"/>';
    li_chk_routes += '<label for="checkbox-100a">Approve All</label><div id="dvChk" class="ui-corner-all">';
    var checked_cnt = 0;
    $.each(gData.storeList, function (store_key, store_val) {
        var is_checked = "";
        if (store_val.isChecked == 1) {
            is_checked = "checked";
            checked_cnt = parseInt(checked_cnt) + 1;
        }
        var qty = (store_val.targetedPretty != undefined && store_val.targetedPretty != null && store_val.targetedPretty != "") ? store_val.targetedPretty : 0;
        var cost = (store_val.costPretty != undefined && store_val.costPretty != null && store_val.costPretty != "") ? store_val.costPretty : 0;
        li_chk_routes += '<input type="checkbox" id=' + store_val.storeId + ' ' + is_checked + ' onclick="fnSelect(this, \'' + store_val.storeId + '\')" />';
        li_chk_routes += '<label for="' + store_val.storeId + '">' + store_val.storeId + ' - ' + store_val.city.toUpperCase() + ', ' + store_val.state.toUpperCase();
        li_chk_routes += '</br>Qty: ' + qty + ' | Cost: ' + cost + '</label>';
        var ddl_postcard = "";
        ddl_postcard += createPostCardSelectionList(customerNumber, store_val.storeId, store_val.selectedPostcard);
        li_chk_routes += ddl_postcard;
    });
    li_chk_routes += '</div></fieldset>';
    $('#dvLocations').append(li_chk_routes);
    $("#dvLocations").trigger("create");
    if (checked_cnt == gData.storeList.length) $('#checkbox-100a').attr('checked', true).checkboxradio('refresh');
    if (customerNumber == CASEYS) {
        $('select[id^=ddlSelectPostcard]').parent().parent().css('display', 'none');
        $('#dvLocations div.ui-checkbox label').last().addClass('ui-last-child');
    }
}

function createPostCardSelectionList(customer_number, store_id, selected_post_card) {
    var ddl_postcard = "";
    var selected_postcard = "";
    if (customer_number == CASEYS) {
        ddl_postcard += '<select name="ddlSelectPostcard' + store_id + '" id="ddlSelectPostcard' + store_id + '" data-mini="true" data-theme="f" onchange="fnPostcardSelect(this, \'' + store_id + '\')">';
        if (selected_post_card == "") selected_postcard = "selected"; else selected_postcard = "";
        if (jobTypeId == 71) {
            ddl_postcard += '<option value="-1" ' + selected_postcard + '>Select Postcard for Store ' + store_id + '</option>';
            if (selected_post_card == "Seven Day Delivery") selected_postcard = "selected"; else selected_postcard = "";
            ddl_postcard += '<option value="Seven Day Delivery" ' + selected_postcard + '>Seven Day Delivery</option>';
            if (selected_post_card == "Thursday-Sunday Delivery") selected_postcard = "selected"; else selected_postcard = "";
            ddl_postcard += '<option value="Thursday-Sunday Delivery" ' + selected_postcard + '>Four Day Delivery: Thu - Sun</option>';
            if (selected_post_card == "Two Day Delivery: Friday-Saturday") selected_postcard = "selected"; else selected_postcard = "";
            ddl_postcard += '<option value="Two Day Delivery: Friday-Saturday" ' + selected_postcard + '>Two Day Delivery: Fri - Sat</option>';
            //if (selected_post_card == "Delivery Only") selected_postcard = "selected"; else selected_postcard = "";
            //ddl_postcard += '<option value="Delivery Only"' + selected_postcard + '>Delivery Only</option>';
            //if (selected_post_card == "Carry-Out and/or Delivery") selected_postcard = "selected"; else selected_postcard = "";
            //ddl_postcard += '<option value="Carry-Out and/or Delivery" ' + selected_postcard + '>Carry-Out and/or Delivery</option>';
        }
        else {
            if (selected_post_card == "Critical Store Postcard") selected_postcard = "selected"; else selected_postcard = "";
            //ddl_postcard += '<option value="-1" ' + selected_postcard + '>Select Postcard for Store ' + store_id + '</option>';
            ddl_postcard += '<option value="Critical Store Postcard"' + selected_postcard + '>Critical Store Postcard</option>';
        }
        ddl_postcard += '</select>';
    }
    else if (customer_number == JETS) {
        ddl_postcard += '<select name="ddlSelectPostcard' + store_id + '" id="ddlSelectPostcard' + store_id + '" data-mini="true" data-theme="f" onchange="fnPostcardSelect(this, \'' + store_id + '\')">';
        if (selected_post_card == "") selected_postcard = "selected"; else selected_postcard = "";
        ddl_postcard += '<option value="-1" ' + selected_postcard + '>Select Postcard for Store ' + store_id + '</option>';
        if (selected_post_card == "Grand Opening 6x11 Postcard") selected_postcard = "selected"; else selected_postcard = "";
        ddl_postcard += '<option value="Grand Opening 6x11 Postcard"' + selected_postcard + '>Grand Opening 6x11 Postcard</option>';
        if (selected_post_card == "Celebrate Spring 6x11 Postcard") selected_postcard = "selected"; else selected_postcard = "";
        ddl_postcard += '<option value="Celebrate Spring 6x11 Postcard" ' + selected_postcard + '>Celebrate Spring 6x11 Postcard</option>';
        ddl_postcard += '</select>';
    }
    return ddl_postcard;
}

function fnSelect(ctrl, checked_store_id) {
    var selected_chk = jQuery.grep(gData.storeList, function (a, b) {
        return a.storeId === checked_store_id.toString();
    });
    if (ctrl.checked) {
        selected_chk[0].isChecked = 1;
        $('#' + ctrl.id).attr('checked', true).checkboxradio('refresh');
        if (jobTypeId == 72)
            $('#ddlSelectPostcard' + ctrl.id).val('Critical Store Postcard').trigger('change');
        //else if (jobTypeId == 71)
        //    $('#ddlSelectPostcard' + ctrl.id).val('Carry-Out and/or Delivery').trigger('change');
    }
    else {
        selected_chk[0].isChecked = 0;
        $('#' + ctrl.id).attr('checked', false).checkboxradio('refresh');
    }

    var unchecked_stores = $('#dvChk input:checkbox:not(:checked)');
    if (unchecked_stores.length > 0) $('#checkbox-100a').attr('checked', false).checkboxradio('refresh');
    else
        $('#checkbox-100a').attr('checked', true).checkboxradio('refresh');

    if (customerNumber == CASEYS) {
        $('#ddlSelectPostcard' + ctrl.id).parent().parent().css('display', 'none');
    }
}

function fnPostcardSelect(ctrl, checked_store_id) {
    var selected_chk = jQuery.grep(gData.storeList, function (a, b) {
        return a.storeId === checked_store_id.toString();
    });
    if ($('#ddlSelectPostcard' + checked_store_id).val() != "-1")
        selected_chk[0].selectedPostcard = $('#ddlSelectPostcard' + checked_store_id).val();
}

function checkCheckboxes(id) {
    if ($('#checkbox-100a')[0].checked) {
        $('#dvChk input[type=checkbox]').each(function () {
            $(this).attr('checked', true);
            if (jobTypeId == 72)
                $('#ddlSelectPostcard' + $(this)[0].id).val('Critical Store Postcard').trigger('change');
            //else if (jobTypeId == 71)
            //    $('#ddlSelectPostcard' + $(this)[0].id).val('Carry-Out and/or Delivery').trigger('change');

            $.each(gData.storeList, function (key, val) {
                val.isChecked = 1;
            });
        });
    }
    else {
        $('#dvChk input[type=checkbox]').each(function () {
            $(this).attr('checked', false);
            $.each(gData.storeList, function (key, val) {
                val.isChecked = 0;
            });
        });
    }

    $("#dvChk input[type=checkbox]").each(function (i) {
        $(this).checkboxradio("refresh");
    });

    if (customerNumber == CASEYS) {
        $('select[id^=ddlSelectPostcard]').parent().parent().css('display', 'none');
    }
}

function getZipInfo(zip_code) {
    $('#hdnSelectedZip').val(zip_code);
}

function fnSubmit() {
    var checked_stores_count = $('#dvChk input:checkbox:checked');
    if (checked_stores_count.length == 0) {
        $('#confirmMsg').html("Submitting without selecting any of your locations will result in <b>Opting Out</b> all of your locations for this mailing. Do you want to continue with this Opt-Out?");
        $('#okButConfirm').text("Yes, Opt-Out");
        $('#okButConfirm').bind('click', function () {
            $('#okButConfirm').unbind('click');
            window.setTimeout(function () { submitApprovals(); }, 500);
            $('#popupConfirmDialog').popup('close');
        });
        $('#popupConfirmDialog').popup('open');
    } else {
        if (validateApprovals()) {
            gData.emailNotes = $('#textarea').val();            
            submitApprovals();
        }
    }
}
function submitApprovals() {
	var msg = "Saving failed.  Contact Administrator!";
    postCORS(approvalPOSTUrl + customerNumber + '/' + facilityId + '/' + jobNumber + '/' + email + '/post/approve/' + jobTypeId, JSON.stringify(gData), function (response) {
        if (response.indexOf("success") != "-1") {
            msg = "Thank You!</br></br>Your Approvals have been submitted successfully." +
                          "You may continue to make changes to your approvals up to the end of the Approval period on " + $("#hdnApprovalPeriod").val() + ". " +
                          "Any stores left deselected at that time will automatically be removed from the mailing." +
                          "You may continue to edit and submit your approvals or close this screen.";
            $('#alertmsg').html(msg);
            $('#popupDialog').popup('open');
        }
        else {
            $('#alertmsg').html(msg);
            $('#popupDialog').popup('open');
        }
    }, function (response_error) {
        $('#alertmsg').text(msg);
        $('#popupDialog').popup('open');
    });

}
function validateApprovals() {
    var store_ids = "";
    $.each($('#dvChk input:checkbox:checked'), function (a, b) {
        if ($('#ddlSelectPostcard' + $(this)[0].id).val() == "-1")
            store_ids += (store_ids != "") ? "," + $(this)[0].id : $(this)[0].id;
    });
    if (store_ids != "") {
        msg = "You must select a postcard for the following stores before approving cost and counts: " + store_ids + "."
        $('#alertmsg').html(msg);
        $('#popupDialog').popup('open');
        return false;
    }
    else {
        return true;
    }
}
//******************** Public Functions End **************************
