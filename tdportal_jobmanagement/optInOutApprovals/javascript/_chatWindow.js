﻿function loadOptInChatWidget() {
    var chat_widget_ref = "59ee3812c28eca75e4627aed";
    var s1 = document.createElement("script"), s0 = document.getElementsByTagName("script")[0];
    s1.async = true;
    s1.src = 'https://embed.tawk.to/' + chat_widget_ref + '/default';
    s1.charset = 'UTF-8';
    s1.setAttribute('crossorigin', '*');
    s0.parentNode.insertBefore(s1, s0);
}

$(document).ready(function () {
    loadOptInChatWidget();
});