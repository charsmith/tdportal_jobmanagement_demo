﻿/***  CREATES MODULE WISE HINT BOXES 
    
    page_name = Unique page id where the hint boxes are created.

***/
var commonCreateDemoHintBoxes = function (page_name) {
    switch (page_name) {
        case "_jobSelectTemplate":
            createHintBox(page_name, 'walkthroughStepsPop', 'walkthroughSteps', 300, '', '', '', 'false', 'true');
            createHintBox(page_name, 'welcomePop', 'dvWelcomeDemoPopup', 300, '', 'navHints', 'window', 'true', 'true');
            createHintBox(page_name, 'navHintsPop', 'navHints', 300, '', 'walkthroughSteps', 'namingHint', 'false', 'true');
            createHintBox(page_name, 'welcomeOptInPop', 'dvwelcomeOptInPop', 512, 'txtJobName', 'navHints', 'window', 'false', 'false');
            break;
        case "_jobPieceAttributes":
            createHintBox(page_name, 'pieceAttributesPop', 'pieceAttributesHints', 300, '', '', '', 'true', 'true');
            break;
        case "_jobMilestones":
            createHintBox(page_name, 'milestoneDatePop', 'mileStoneHints', 300, '', '', '', 'true', 'true');
            break;
        default:
            break;
    }
}
/***  CREATES GIZMO DEMO HINT POPUP

    page_name                             = Page Id in which this demo hint is created
    id_hint_message                       = div id to which the message is appended.
    id_popup                              = popup unique id.
    popup_max_width                       = maximum width the popup can occupies on the screen.
    ctrl_id_to_focus                      = control id to which focus to set.
    id_next_popup                         = nextpopup unique id.
    next_popup_position                   = position to which the next continuous popup should open.
    need_to_display_dont_show_checkbox    = Flag to verify whether the Don't show me Again checkbox should display or not.
    need_to_display_close_icon            = Flag to verify whether close icon should display or not.

***/
var createHintBox = function (page_name, id_hint_message, id_popup, popup_max_width, ctrl_id_to_focus, id_next_popup, next_popup_position, need_to_display_dont_show_checkbox, need_to_display_close_icon) {
    var hint_popup = '<div data-role="popup" id="' + id_popup + '" class="ui-content" data-theme="p" style="max-width:' + popup_max_width + 'px;" data-history="false" data-dismissible="false" data-overlay-theme="d">';
    if (JSON.parse(need_to_display_close_icon)) {
        if (getValueExists(id_next_popup)) {
            hint_popup += '<a href="#" onclick="toggleContinuousPopups(\'' + id_popup + '\',\'' + id_next_popup + '\',\'' + next_popup_position + '\',\'' + ctrl_id_to_focus + '\');" data-role="button" data-theme="a" data-icon="delete" data-iconpos="notext" class="ui-btn-right">Close</a>';
        }
        else {
            hint_popup += '<a href="#"  onclick="$(\'#' + id_popup + '\').popup(\'close\');" data-role="button" data-theme="a" data-icon="delete" data-iconpos="notext" class="ui-btn-right">Close</a>';
        }
        hint_popup += '<div id="' + id_hint_message + '"></div>';
        hint_popup += '<br /><p align="right"><a href="#" onclick="turnDemoHintsOffClick(\'' + id_popup + '\')" data-theme="p" style="font-size:14px;text-decoration:none;">Turn Demo Hints Off</a></p>';
    }
    else {
        hint_popup += '<div id="' + id_hint_message + '"></div>';
        hint_popup += '<br /><div style="float:right"><a data-role="button" href="#" onclick="toggleContinuousPopups(\'' + id_popup + '\',\'' + id_next_popup + '\',\'' + next_popup_position + '\',\'' + ctrl_id_to_focus + '\');" data-theme="p" style="font-size:14px;text-decoration:none;">Let\'s Get Started</a></div>';
        hint_popup += '<div style="height:40px">&nbsp;</div>';
    }
    if (getBooleanValue(need_to_display_dont_show_checkbox)) {
        hint_popup += '<fieldset data-role="controlgroup" style="width:76%;padding-left:86px">';
        hint_popup += '<label><input type="checkbox" data-iconpos="right" id="chkDontShowMeThisAgain" name="chkDontShowMeThisAgain" data-theme="p" data-mini="true" onchange="doNotShowMeThisAgainClick(event, \'' + id_popup + '\')" />Do Not Show Me This Again</label>';
        hint_popup += '</fieldset>';
    }
    hint_popup += '</div>';

    $("#" + page_name).append(hint_popup);
}

/*** DISPLAYS THE DEMO HINT BOX 
    
    id_popup    = popup unique id.
    position   = position in which the popup should display

***/
var commonOpenHintBox = function (id_popup, position) {
    if (position != undefined) {
        if (position.toLowerCase() == "window") {
            $('#' + id_popup).popup('open', { positionTo: 'window' });
        }
        else {
            $('#' + id_popup).popup('open', { positionTo: '#' + position });
        }
    }
    else {
        $('#' + id_popup).popup('open');
    }
}

/*** DISPLAYS THE DEMO HINT BOX 
    
    id_popup    = popup unique id.
    position   = position in which the popup should display
    timeout    = time to wait before displaying popup

***/
var openHintBoxWithTimeLimit = function (id_popup, position, timeout) {
    window.setTimeout(function () {
        commonOpenHintBox(id_popup, position);
    }, timeout);
}

/*** CLOSES DEMO HINT BOX
    
    id_popup = popup unique id

***/
var closeHintBox = function (id_popup) {
    $('#' + id_popup).popup('close');
}

/*** TOGGLES CONTINUOUS POPUPS
    
    id_popup_to_close = popup unique id to close.
    id_popup_to_open  = popup unique id to open.
    popup_position  = position where the popup should display.
    ctrl_id_to_focus        = control id to which focus to set.

***/
var toggleContinuousPopups = function (id_popup_to_close, id_popup_to_open, popup_position, ctrl_id_to_focus) {
    closeHintBox(id_popup_to_close);
    if (getValueExists(ctrl_id_to_focus)) {
        $('#' + ctrl_id_to_focus).focus();
    }
    window.setTimeout(function () {
        commonOpenHintBox(id_popup_to_open, popup_position);
    }, 300);
}

/*** DISPLAYS DEFAULT DEMO HINT BOX AFTER PAGE LOAD COMPLETES 
    
    page_name  = Page Id in which this demo hint is created

***/
var dontShowHintsAgain = getBooleanValue(localStorage.getItem('doNotShowHintsAgain'));

var commonShowDemoHintAfterPageLoad = function (page_name) {
    var customer = getCustomerNumber();
    if (!dontShowHintsAgain) {
        switch (page_name) {
            case "_jobPieceAttributes":
                if (showPieceAttributesDemoHint() && checkShowDemoHintsFlag() && !getBooleanValue(sessionStorage.isPieceAttributesDemoHintsDisplayed)) {
                    sessionStorage.isPieceAttributesDemoHintsDisplayed = true;
                    openHintBoxWithTimeLimit('pieceAttributesHints', 'dvPieceAttributes', 500);
                }
                break;
            case "_jobMilestones":
                if (showMilestonesDemoHInt() && checkShowDemoHintsFlag() && !getBooleanValue(sessionStorage.isMileStonesDemoHintsDisplayed)) {
                    sessionStorage.isMileStonesDemoHintsDisplayed = true;
                    openHintBoxWithTimeLimit('mileStoneHints', 'ulMileStones', 500);
                }
                break;
            default:
                break;
        }
    }
}