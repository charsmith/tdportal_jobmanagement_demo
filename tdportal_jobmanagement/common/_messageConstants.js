﻿var MESSAGE = {};

// Messages with key starts with  --------------    'A'
MESSAGE['ALERT_NO_DATA'] = 'No data available for selected combination.';
MESSAGE['APPROVAL_START_NOT_LESS_THAN_4WEEKS_PRIOR_TO_INHOME'] = 'The approval start date can be no less than 4 weeks prior to the In-Home.';
MESSAGE['APPROVAL_END_ATLEAST_1WEEK_FROM_APPROVAL_START'] = 'Approvals end date should be at least be 1 week from Approvals start date.';
MESSAGE['ART_ARRIVAL_BTWN_CURRENT_DATE_AND_INHOME'] = 'Art arrival date should be set between the current date and at least {0} days less than the In home date.';
MESSAGE['ART_ARRIVAL_BTWN_CURRENT_DATE_AND_N_DAYS_PRIOR_TO_INHOME'] = 'The Art Arrival date should be set between the current date and {0} days prior to the in-home date.';

// Messages with key starts with  --------------    'B'
// Messages with key starts with  --------------    'C'
MESSAGE['COMMENT_REQUIRED'] = 'Please enter the comment';
MESSAGE['COMMENT_DATA_SAVED'] = 'Comments data was saved.';
MESSAGE['COMMENT_DATA_NOT_SAVED'] = 'Your comments data was not saved.';
MESSAGE['CONFIRM_REMOVE_LIST'] = 'Are you sure you want remove this list?';
MESSAGE['CUSTOMER_REQUIRED'] = 'Please enter Customer.';
MESSAGE['CONTACT_MANAGER_FOR_TEMPLATE_CHANGE_FOR_SUBMITTED_ORDER'] = 'This order has already been submitted for processing. If your order requires a template change, please contact an account manager.';

// Messages with key starts with  --------------    'D'
MESSAGE['DATE_MANDATORY'] = '{0} date is mandatory!';

// Messages with key starts with  --------------    'E'
// Messages with key starts with  --------------    'F'
MESSAGE['FLAG_REQUIRED'] = 'Please select at least one flag';

// Messages with key starts with  --------------    'G'
// Messages with key starts with  --------------    'H'

// Messages with key starts with  --------------    'I'
MESSAGE['INHOMEDATE_GREATERTHAN_CURRENT_DATE'] = 'In-Home date should be always greater than current date.';
MESSAGE['INHOMEDATE_GREATERTHAN_N_DAYS_CURRENT_DATE'] = 'In-Home date should always be greater than {0} days from current date.';
MESSAGE['INHOMEDATE_NOT_CLOSER_THAN_N_DAYS_FOR_DATE_PROVIDED'] = 'In-Home date should be always no closer than {0} days from the date provided for when Art Arrival date & Data/List arrival date.';
MESSAGE['INHOME_DATE_REQUIRED'] = 'Please enter Inhome Date.';

// Messages with key starts with  --------------    'J'
MESSAGE['JOB_CLOSED'] = 'This job is closed. Any changes made in the edit screens will not be saved. These screens are provided only to review previous selections and proofs. If you have any questions, please contact a site administrator.';
MESSAGE['JOB_NAME_ALREADY_EXISTS'] = 'The entered Job name <b>{0}</b> already exists. Please enter unique job name.';
MESSAGE['JOB_DESC_MIN_LENGTH'] = 'Job Description should be more than 5 characters.';
MESSAGE['JOB_DESC_REQUIRED'] = 'Please enter Job Description.';
MESSAGE['JOB_DESC_INVALID'] = 'Please enter a valid job description. The following characters are not allowed "\/@#$%^&*(){}[]|?<>~`+\'".';

// Messages with key starts with  --------------    'L'
MESSAGE['LIST_ARRIVAL_BTWN_CURRENT_DATE_AND_INHOME'] = 'Data/List arrival date should be set between the current date and at least {0} days less than the In home date.';
MESSAGE['LIST_ARRIVAL_BTWN_CURRENT_DATE_AND_N_DAYS_PRIOR_TO_INHOME'] = 'The Data/List Arrival date should be set between the current date and {0} days prior to the in-home date.';

// Messages with key starts with  --------------    'N'
MESSAGE['NEED_FILTER_OPTION'] = 'Please select at least one filter option';

// Messages with key starts with  --------------    'O'
MESSAGE['OFFER_EXPIRATION_DATE_REQUIRED'] = 'Please enter Offer Expiration Date.';

// Messages with key starts with  --------------    'S'
MESSAGE['SALE_END_GREATER_THAN_SALE_START'] = 'Sale end date should be greater than or equal to Sale start date.';
MESSAGE['SIGN_UP_START_6WEEKS_PRIOR_TO_INHOME'] = 'The sign up start date can be no less than 6 weeks prior to the In-Home.';
MESSAGE['SIGN_UP_START_ATLEAST_2WEEKS_FROM_APPROVAL_START'] = 'The sign up start date should be at least 2 weeks from the approvals start date.';
MESSAGE['SIGN_UP_END_ATLEAST_1WEEK_FROM_SIGN_UP_START'] = 'Sign up end date should be at least be 1 week from Sign up start date.';
MESSAGE['SELECTING_NEW_TEMPLATE'] = 'Selecting a new template will discard any other changes you have made to this job. Do you want to continue?';

//Messages with key starts with  --------------     'T'
MESSAGE['TEMPLATE_SELECTION_REQUIRED'] = 'Please select a Template.';
MESSAGE['TEMPLATE_CANNOT_CHANGED'] = 'The template cannot be changed once the job has been saved.';
