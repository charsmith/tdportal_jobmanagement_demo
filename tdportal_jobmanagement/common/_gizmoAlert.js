﻿//Creating Gizmo Alert Box
var commonCreateGizmoAlert = function (page_name) {
    var alert_box = '<div data-role="popup" data-position-to="window" data-transition="pop" id="gizmoAlert" data-overlay-theme="d" data-theme="c" style="text-align:left;max-width:500px;" class="ui-corner-all" data-history="false">'
                  + '   <div data-role="header" data-theme="d" class="ui-corner-top" id="gizmoAlertHeader">'
                  + '       <h1 id="gizmoAlertTitle">Alert</h1>'
                  + '   </div>'
                  + '   <div data-role="content" data-theme="c" class="ui-corner-bottom ui-content">'
                  + '       <div id="gizmoAlertMsg"></div>'
                  + '       <a href="#" data-role="button" data-inline="true" data-theme="a" id="gizmoAlertOK" onclick="$(\'#gizmoAlert\').popup(\'close\');">Ok</a>'
                  + '   </div>'
                  + '</div>';

    $("#" + page_name).append(alert_box);
}

//Display Gizmo Alert
var commonShowGizmoAlert = function (message, is_html, navigation_id, navigation_popup_position) {
    if (is_html) {
        $('#gizmoAlertMsg').html(message);
    }
    else {
        $('#gizmoAlertMsg').text(message);
    }
    if (navigation_id != undefined && navigation_id != null && navigation_id != "") {
        $('#gizmoAlertOK').bind('click', function () {
            $('#gizmoAlertOK').unbind('click');
            $('#gizmoAlertOK').attr('onclick', '$(\'#gizmoAlert\').popup(\'close\');');
            $('#gizmoAlertOK').popup('close');
            window.setTimeout(function () {
                if (navigation_popup_position != undefined && navigation_popup_position != null && navigation_popup_position != "") {
                    $('#' + navigation_id).popup('open', { positionTo: '#' + navigation_popup_position });
                }
                else {
                    $('#' + navigation_id).popup('open', { positionTo: 'window' });
                }                
            }, 400);
        });
    }      
    window.setTimeout(function () {
        $('#gizmoAlert').popup('open', { positionTo: 'window' });
    }, 300);
    return false;
}