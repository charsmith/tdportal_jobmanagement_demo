﻿$.mobile.popup.prototype.options.history = false;
jQuery.support.cors = true;

//******************** Data URL Variables Start **************************
var pageObj;
var gEmailRecipientsServiceUrl = "../adminEmailLists/JSON/_emailRecipients.JSON";
var gServiceEmailRecipientsData = [];
var gServiceJobData;
var pageObj;
var companyData;
var primaryFacilityData;
//******************** Data URL Variables End **************************

//******************** Page Load Events Start **************************
$('#_emailRecipients').live('pagebeforecreate', function (event) {
    ko.setTemplateEngine(new ko.nativeTemplateEngine);
    displayMessage('_emailRecipients');
    createConfirmMessage("_emailRecipients");
    getEmailRecipientsInfo();
    $("#navLinksPanel").on("panelopen", function (event, ui) {
        sessionStorage.isNavPanelOpen = true;
    });

});

$(document).on('pageshow', '#_emailRecipients', function (event) {
    window.setTimeout(function () {
        pageObj = new loadRecipientsInfo();
        ko.applyBindings(pageObj);
        $('div').trigger('create');
    }, 500);
});

var getEmailRecipientsInfo = function () {
    $.getJSON(gEmailRecipientsServiceUrl, function (data) {
        gServiceEmailRecipientsData = data;
    });
};

var emailRecipient = function (email_id) {
    var self = this;
    self.emailId = ko.observable({});
    self.emailId(email_id);
};
var emailRecipientsList = function (recipients_for, recipients_list) {
    var self = this;
    self.recipientsFor = ko.observable({});
    self.recipientsFor(recipients_for);
    self.recipientsList = ko.observableArray([]);
    self.recipientsList(recipients_list);
    self.totalCount = ko.observable({});
    self.totalCount(recipients_list.length);
};

var loadRecipientsInfo = function () {
    var self = this;
    self.description = ko.observable({});
    self.description(sessionStorage.selectedEmailGroup);
    self.emailRecipients = [];
    var temp_email_recipients = [];
    var temp_list = [];
    var type = sessionStorage.selectedEmailGroup;
    type = type.replace(/ /g, '').initSmall() + 'Recipients';
    if (gServiceEmailRecipientsData[type] != undefined && gServiceEmailRecipientsData[type] != null) {
        $.each(gServiceEmailRecipientsData[type], function (key, val) {
            temp_list = [];
            $.each(val, function (key1, val1) {
                temp_list.push(new emailRecipient(val1));
            });
            self.emailRecipients.push(new emailRecipientsList(key, temp_list));
        });
    }
    self.selectedEmailRecipientFor = ko.observable({});

    self.addRecipientClick = function (data, event) {
        var ctrl = event.target || event.currentTarget;
        var parent_context = ko.contextFor(ctrl).$parent;
        self.selectedEmailRecipientFor(parent_context);
        $('#popupAddRecipient').popup('open');
    };
    self.addRecipient = function (data, event) {
        var temp_recipient = {};
        self.selectedEmailRecipientFor().recipientsList.push(new emailRecipient($('#txtEmailId').val()));
        $('#popupAddRecipient').popup('close');
        $('ul').listview('refresh');
    };
    self.removeEmailId = function (data, event) {
        var ctrl = event.target || event.currentTarget;
        var parent_context = ko.contextFor(ctrl).$parent;
        $('#okButConfirm').text('Delete');
        $('#confirmMsg').html('Are you sure you want remove this item?');
        $('#okButConfirm').unbind('click');
        $('#okButConfirm').bind('click', function () {
            parent_context.recipientsList.remove(data);
            $('#popupConfirmDialog').popup('close');
        });
        $('#popupConfirmDialog').popup('open');
    };
    self.saveAsNow = function () {
        $('#popupSaveAsNow').popup('open');
    };

    self.save = function () {
        $("#popupConfirmDialog div[align=right] a").eq(0).after($('<a data-role="button" data-theme="a" data-inline="true" data-mini="true" id="btnSaveAsNowPopUp">Save As Now</a>'));
        $("#popupConfirmDialog div[align=right]").trigger('create');
        //$('#okButConfirm').text('Save');
        //$('#confirmMsg').html('Are you sure you want remove this item?');
        //$('#okButConfirm').unbind('click');
        //$('#okButConfirm').bind('click', function () {
        //    parent_context.recipientsList.remove(data);
        //    $('#popupConfirmDialog').popup('close');
        //});
        //$("#popupConfirmDialog div[align=right] a").eq(1).remove();
        $('#popupConfirmDialog').popup('open');
    };

    self.deleteList = function (data,event) {
        var ctrl = event.target || event.currentTarget;
        var parent_context = ko.contextFor(ctrl).$parent;
        $('#okButConfirm').text('Delete');
        $('#confirmMsg').html('Are you sure you want remove this list?');
        $('#okButConfirm').unbind('click');
        $('#okButConfirm').bind('click', function () {
            // Service Call goes here.
            $('#popupConfirmDialog').popup('close');
            self.cancel();
        });
        $('#popupConfirmDialog').popup('open');
    };

    self.cancel = function () {
        window.location.href = " ../adminEmailLists/emailLists.html";
    };
};

String.prototype.initSmall = function () {
    return this.replace(/(?:^|\s)[A-Z]/g, function (m) {
        return m.toLowerCase();
    });
};

function showHomePage(type) {
    if (type == "home") {
        window.location.href = "../index.htm";
    }
    else if ((type == "emailrecipients")) {
        window.location.href = " ../adminEmailLists/emailRecipients.html";
    }
};

