﻿$.mobile.popup.prototype.options.history = false;
jQuery.support.cors = true;
var mapping = { 'ignore': ["__ko_mapping__"] }
//******************** Data URL Variables Start **************************
var pageObj;
var jobInfo = {};
if (sessionStorage.selectedJobInfo != undefined && sessionStorage.selectedJobInfo != null && sessionStorage.selectedJobInfo != "")
    jobInfo = JSON.parse(sessionStorage.selectedJobInfo);
//jobInfo.jobNumber = "415656"
var gAppFinanceServiceUrl = (sessionStorage.isAggregated) ? "../adminFinance/JSON/_jobFinanceAggregateAR.JSON" : "../adminFinance/JSON/_jobFinanceInfo.JSON";
var gFinanceInfoServiceGetUrl = serviceURLDomain + "api/JobFinance_get/" + sessionStorage.facilityId + "/" + jobCustomerNumber + "/" + jobInfo.jobNumber;
var gFinanceInfoServicePostUrl = serviceURLDomain + "api/JobFinance_post/" + sessionStorage.facilityId + "/" + jobCustomerNumber + "/" + jobInfo.jobNumber;
var gServiceJobData;
var formattedJobInfo = {};
var gServiceFormattedJobData = {};
var pageObj;
var companyData;
var primaryFacilityData;
//******************** Data URL Variables End **************************
getCORS(gFinanceInfoServiceGetUrl, null, function (job_finance_data) {
    $.each(job_finance_data.listJobAccountsReceivable, function (key, val) {
        //var recd_date =new Date(val.ReceivedDate);
        //var temp_date = (recd_date.getMonth()+"/"+recd_date.getDate()+"/" +recd_date.getFullYear());
        var recd_date = val.ReceivedDate.substring(0, val.ReceivedDate.indexOf('T'));
        var date = new Date(recd_date).getDate();
        if (date.toString().length == 1)
            date = '0' + date;
        var month = new Date(recd_date).getMonth() + 1;
        if (month.toString().length == 1)
            month = '0' + month;
        var year = new Date(recd_date).getFullYear();
        val.ReceivedDate = month + "/" + date + "/" + year;
    });
    gServiceJobData = $.extend(true, {}, job_finance_data);
    formattedJobInfo["storeIdToPkDict"] = [];
    $.each(job_finance_data.storeIdToPkDict, function (key, val) {
        formattedJobInfo["storeIdToPkDict"].push({
            "storePk": val,
            "storeId": key
        });
    });

    formattedJobInfo["listJobAccountsReceivable"] = gServiceJobData.listJobAccountsReceivable;
    delete job_finance_data.storeIdToPkDict;
    delete job_finance_data.listJobAccountsReceivable;
    $.each(job_finance_data, function (key, val) {
        formattedJobInfo[key] = val;
    });
}, function (error_response) {
    showErrorResponseText(error_response, true);
});
//******************** Page Load Events Start **************************
$('#_jobFinanceInfo').live('pagebeforecreate', function (event) {
    ko.setTemplateEngine(new ko.nativeTemplateEngine);
    displayMessage('_jobFinanceInfo');
    createConfirmMessage("_jobFinanceInfo");
    //getJobInfo();
    $("#navLinksPanel").on("panelopen", function (event, ui) {
        sessionStorage.isNavPanelOpen = true;
    });
});

$(document).on('pageshow', '#_jobFinanceInfo', function (event) {
    window.setTimeout(function () {
        pageObj = new loadFinanceInfo();
        ko.applyBindings(pageObj);
        $('div').trigger('create');
        $('select[data-role=slider]').slider('refresh');
        $('input[data-fieldType="dateRec"]').datepicker({
            minDate: new Date(2010, 0, 1),
            dateFormat: 'mm/dd/yy',
            constrainInput: true,
            beforeShowDay: nationalDays
        });
    }, 500);
});

var getJobInfo = function () {
    //$.getJSON(gAppFinanceServiceUrl, function (data) {
    //    gServiceJobData = data;
    //});


};

var paymentInfoVm = function (data) {
    var self = this;
    if (data.postageReceivedPretty != undefined && data.postageReceivedPretty != null)
        self.postageReceivedPretty = ko.observable(data.postageReceivedPretty);
    if (data.billingReceivedPretty != undefined && data.billingReceivedPretty != null)
        self.billingReceivedPretty = ko.observable(data.billingReceivedPretty);
    if (data.billingCheck != undefined && data.billingCheck != null)
        self.billingCheck = ko.observable(data.billingCheck);
    if (data.postageCheck != undefined && data.postageCheck != null)
        self.postageCheck = ko.observable(data.postageCheck);
    if (data.postageDate != undefined && data.postageDate != null)
        self.postageDate = ko.observable(data.postageDate);
    if (data.billingDate != undefined && data.billingDate != null)
        self.billingDate = ko.observable(data.billingDate);
    if (data.postageNotes != undefined && data.postageNotes != null)
        self.postageNotes = ko.observable(data.postageNotes);
    if (data.billingNotes != undefined && data.billingNotes != null)
        self.billingNotes = ko.observable(data.billingNotes);
};

var nonAggregateJobsInfoVm = function (data) {
    var self = this;
    self.isCombinedInv = ko.observable('off');
    self.combinedInvInfoList = ko.observableArray([]);
    self.paymentInfoList = ko.observableArray([]);
    $.each(data.paymentInfoList, function (pmt_key, pmt_val) {
        self.paymentInfoList.push(new paymentInfoVm(pmt_val));
    });
};

var aggregatedJobsInfoVm = function (data) {
    var self = this;
    self.jobNumber = ko.observable(data.jobNumber);
    self.jobName = ko.observable(data.jobName);
    self.isCombinedInv = ko.observable('off');
    self.combinedInvInfoList = ko.observableArray([]);
    self.paymentInfoList = ko.observableArray([]);
    $.each(data.paymentInfoList, function (pmt_key, pmt_val) {
        self.paymentInfoList.push(new paymentInfoVm(pmt_val));
    });
};

var loadFinanceInfo = function () {
    var self = this;

    self.jobs = ko.observableArray([]).extend({ notify: 'always' });
    self.isAggregatedJob = ko.observable({});
    self.isAggregatedJob((sessionStorage.isAggregated != undefined && sessionStorage.isAggregated != null && sessionStorage.isAggregated == "true") ? true : false);
    self.combinedInvOptions = [{ 'invOptionText': 'On', 'invOptionValue': "on" }, { 'invOptionText': 'Off', 'invOptionValue': "off" }];
    self.jobInfo = ko.observable({});
    self.aggregatedPaymentInfoList = ko.observableArray([]);
    self.isPayInAggregate = ko.observable('false');
    self.previousPaymentObject = {};
    ko.mapping.fromJS(formattedJobInfo, {}, self);

    self.customerName = ko.computed(function () {
        return appPrivileges.customerName;
    });
    self.size = ko.computed(function () {
        return self.height() + 'X' + self.width();
    });
    //$.each(gServiceJobData, function (key1, val1) {
    //    key1 = key1.toLowerCase();
    //    switch (key1) {
    //        case "jobinfo":
    //            self.jobInfo(val1);
    //            break;
    //        case "aggregatedjobs":
    //            $.each(val1, function (agg_jobs_key, agg_jobs_val) {
    //                self.jobs.push(new aggregatedJobsInfoVm(agg_jobs_val));
    //            });
    //            break;
    //        case "aggregateinfo":
    //            self.aggregatedPaymentInfoList.push(new nonAggregateJobsInfoVm(val1));
    //            break;
    //        case "paymentinfolist":
    //            self.jobs.push(new nonAggregateJobsInfoVm(gServiceJobData));
    //            break;
    //        default:
    //            break;
    //    }
    //});

   self.removeReceivableLog = function (data, event) {
        var ele = event.target || event.currentTarget;
        var type = $(ele).attr('data-addType');
        $('#confirmMsg').html("Are you sure you want to remove this receivable log?");
        $('#okButConfirm').text('Remove')
        $('#okButConfirm').unbind('click');
        $('#okButConfirm').bind('click', function () {
            $('#okButConfirm').unbind('click');
            $('#popupConfirmDialog').popup('close');
            if (type === "c")
                self.removePayments(data, event);
            else
                self.removePostageOrInvoice(data, event);
        });
        $('#cancelButConfirm').unbind('click');
        $('#cancelButConfirm').bind('click', function () {
            $('#cancelButConfirm').unbind('click');
            $('#popupConfirmDialog').popup('close');
        });
        window.setTimeout(function () {
            $('#popupConfirmDialog').popup('open');
        }, 500);
    };

    self.removePostageOrInvoice = function (data, event) {
        self.listJobAccountsReceivable.remove(data);
        //var ele = event.target || event.currentTarget;
        //var root_context = ko.contextFor(ele).$root;
        //var target_index = ko.contextFor(ele).$index();
        //var type = $(ele).attr('data-addType');
        //var pmt_info_lst = root_context.jobs()[0].paymentInfoList();
        //var new_target_obj = {};

        //if (type == 'p') {
        //    $.each(pmt_info_lst, function (key, val) {
        //        if (target_index === 0 && key === 0) {
        //            val.postageReceivedPretty('');
        //            val.postageCheck('');
        //            val.postageDate('');
        //            val.postageNotes('');
        //        }
        //        else if (key == target_index && target_index != 0) {
        //            if (target_index + 1 <= pmt_info_lst.length - 1 && pmt_info_lst[target_index + 1].hasOwnProperty('postageReceivedPretty')) {
        //                val.postageReceivedPretty(pmt_info_lst[target_index + 1].postageReceivedPretty());
        //                val.postageCheck(pmt_info_lst[target_index + 1].postageCheck());
        //                val.postageDate(pmt_info_lst[target_index + 1].postageDate());
        //                val.postageNotes(pmt_info_lst[target_index + 1].postageNotes());
        //            }
        //            else {
        //                root_context.jobs()[0].paymentInfoList.splice(target_index, 1);
        //                if (val.hasOwnProperty('billingReceivedPretty')) {
        //                    new_target_obj["billingReceivedPretty"] = ko.observable(val.billingReceivedPretty());
        //                    new_target_obj["billingCheck"] = ko.observable(val.billingCheck());
        //                    new_target_obj["billingDate"] = ko.observable(val.billingDate());
        //                    new_target_obj["billingNotes"] = ko.observable(val.billingNotes());
        //                    root_context.jobs()[0].paymentInfoList.splice(target_index, 0, new_target_obj);
        //                }
        //            }
        //            target_index++;
        //        }
        //    });
        //}
        //else {
        //    $.each(pmt_info_lst, function (key, val) {
        //        if (target_index === 0 && key === 0) {
        //            val.billingReceivedPretty('');
        //            val.billingCheck('');
        //            val.billingDate('');
        //            val.billingNotes('');
        //        }
        //        else if (key == target_index && target_index != 0) {
        //            if (target_index + 1 <= pmt_info_lst.length - 1 && pmt_info_lst[target_index + 1].hasOwnProperty('billingReceivedPretty')) {
        //                val.billingReceivedPretty(pmt_info_lst[target_index + 1].billingReceivedPretty());
        //                val.billingCheck(pmt_info_lst[target_index + 1].billingCheck());
        //                val.billingDate(pmt_info_lst[target_index + 1].billingDate());
        //                val.billingNotes(pmt_info_lst[target_index + 1].billingNotes());
        //            }
        //            else {
        //                root_context.jobs()[0].paymentInfoList.splice(target_index, 1);
        //                if (val.hasOwnProperty('postageReceivedPretty')) {
        //                    new_target_obj["postageReceivedPretty"] = ko.observable(val.postageReceivedPretty());
        //                    new_target_obj["postageCheck"] = ko.observable(val.postageCheck());
        //                    new_target_obj["postageDate"] = ko.observable(val.postageDate());
        //                    new_target_obj["postageNotes"] = ko.observable(val.postageNotes());
        //                    root_context.jobs()[0].paymentInfoList.splice(target_index, 0, new_target_obj);
        //                }
        //            }
        //            target_index++;
        //        }

        //    });
        //}
        if (root_context.isPayInAggregate() == "true") {
            $('#dvAggrPmt div').trigger('create');
        } else {
            $('div').trigger('create');
        }
        $('input[data-fieldType="dateRec"]').datepicker({
            minDate: new Date(2010, 0, 1),
            dateFormat: 'mm/dd/yy',
            constrainInput: true,
            beforeShowDay: nationalDays
        });
    };

    self.checkValueUpdated = function (data, event) {
        var ele = event.target || event.currentTarget;
        var check_duplicate_exists = false;
        self.setUpdate(data, event);
        //var root_context = ko.contextFor(ele).$root;
        //var target_index = ko.contextFor(ele).$index();
        //var type = $(ele).attr('data-fieldType');
        //var current_check_value = $(ele).val();
        //var pmt_info_lst = (type === "c") ? root_context.jobs()[0].combinedInvInfoList() :  root_context.jobs()[0].paymentInfoList();

        //$.each(pmt_info_lst, function (key, val) {
        //    if (target_index === key && type != "c") {
        //        if (type === "pc" && val.billingCheck().toString() === current_check_value) {
        //            check_duplicate_exists = true;
        //            return false;
        //        }
        //        else if (type === "ic" && val.postageCheck().toString() === current_check_value){
        //            check_duplicate_exists = true;
        //            return false;
        //        }
        //    }
        //    else if (target_index != key) {
        //        if (type === "c") {
        //            if (val.receivedCheck().toString() === current_check_value) {
        //                check_duplicate_exists = true;
        //                return false;
        //            }                    
        //        }
        //        else {
        //            if (val.postageCheck().toString() === current_check_value || val.billingCheck().toString() === current_check_value) {
        //                check_duplicate_exists = true;
        //                return false;
        //            }                    
        //        }                
        //    }
        //});
        if (check_duplicate_exists) {
            $('#alertmsg').html("This check # was previously entered for Job <b>" + self.jobInfo().jobNumber + "</b>.");
            $('#alertmsg').css('text-align', 'left');
            $("#popupDialog").popup('open');
            $("#okBut").attr("onclick", "$('#popupDialog').popup('close')");
            return false;
        }
    };

    self.getPreviousValue = function (data, el) {
        //self.previousPaymentObject
        var temp = ko.mapping.toJS(data, mapping);
        self.previousPaymentObject = temp;
    }

    self.setUpdate = function (data, el) {
        var temp = ko.mapping.toJS(data, mapping);
        if (JSON.stringify(temp) != JSON.stringify(self.previousPaymentObject)) {
            data["update"] = ko.observable('');
            data.update('updated');
            date.updatedBy(sessionStorage.username);
        }
        //else {
        //    if (data["update"])
        //        delete data["update"];
        //}
    };

    self.addPostageOrInvoice = function (data, event) {
        var ele = event.target || event.currentTarget;
        var root_context = ko.contextFor(ele).$root;
        var type = $(ele).attr('data-addType');
        //var pmt_info_lst = data.paymentInfoList;
        ele = $('#dvPayments div div div.ui-grid-solo.ui-responsive')[0];
        var pmt_info_list = ko.contextFor(ele).$parent;

        var temp = { "pk": '', "customerId": "155370", "orderPk": 26840, "storePk": data.storePk(), "paymentType": type, "CheckNumber": '', "payPalTransactionId": null, "ReceivedAmount": '', "ReceivedDate": "", "createdBy": sessionStorage.username, "created": "", "updatedBy": sessionStorage.username, "updated": "", "notes": "", "update": "new" };
        var temp1 = {};
        ko.mapping.fromJS(temp, {}, temp1);
        self.listJobAccountsReceivable.push(temp1);
        if (root_context.isPayInAggregate() == "true") {
            $('#dvAggrPmt div').trigger('create');
        } else {
            $('div').trigger('create');
        }
        $('input[data-fieldType="dateRec"]').datepicker({
            minDate: new Date(2010, 0, 1),
            dateFormat: 'mm/dd/yy',
            constrainInput: true,
            beforeShowDay: nationalDays
        });
    };
    self.combineUncombineInv = function (data, event) {
        var ele = event.target || event.currentTarget;
        var root_context = ko.contextFor(ele).$root;
        if (ele.value == "off") {

        }
        else {
            if (data.combinedInvInfoList().length == 0) {
                var temp_info = {};
                temp_info["receivedPretty"] = ko.observable("");
                temp_info["receivedCheck"] = ko.observable("");
                temp_info["receivedDate"] = ko.observable("");
                temp_info["receivedNotes"] = ko.observable("");
                data.combinedInvInfoList.push(temp_info);
            }
        }
        if (root_context.isPayInAggregate() == "true") {
            $('#dvAggrPmt div').trigger('create');
        } else {
            $('div').trigger('create');
        }
        $('input[data-fieldType="dateRec"]').datepicker({
            minDate: new Date(2010, 0, 1),
            dateFormat: 'mm/dd/yy',
            constrainInput: true,
            beforeShowDay: nationalDays
        });
        // $('div').trigger('create');
    };
    self.addPayments = function (data, event) {
        var temp_info = {};
        var ele = event.target || event.currentTarget;
        var root_context = ko.contextFor(ele).$root;
        temp_info["receivedPretty"] = ko.observable("");
        temp_info["receivedCheck"] = ko.observable("");
        temp_info["receivedDate"] = ko.observable("");
        temp_info["receivedNotes"] = ko.observable("");
        data.combinedInvInfoList.push(temp_info);
        if (root_context.isPayInAggregate() == "true") {
            $('#dvAggrPmt div').trigger('create');
        } else {
            $('div').trigger('create');
        }
    };

    self.removePayments = function (data, event) {
        var temp_info = {};
        var ele = event.target || event.currentTarget;
        var root_context = ko.contextFor(ele).$root;
        var target_index = ko.contextFor(ele).$index();
        if (target_index === 0) {
            $.each(root_context.jobs()[0].combinedInvInfoList(), function (key, val) {
                if (key === 0) {
                    val.receivedPretty('');
                    val.receivedCheck('');
                    val.receivedDate('');
                    val.receivedNotes('');
                }
                return false;
            });
        }
        else {
            root_context.jobs()[0].combinedInvInfoList.splice(target_index, 1);
        }


        if (root_context.isPayInAggregate() == "true") {
            $('#dvAggrPmt div').trigger('create');
        } else {
            $('div').trigger('create');
        }
    };

    self.makePayInAggregate = function (data, event) {
        var ele = event.target || event.currentTarget;
        if ($(ele).is(':checked'))
            data.isPayInAggregate('true');
        else
            data.isPayInAggregate('false');
        if (data.isPayInAggregate() == "true") {
            $('#dvAggrPmt div').trigger('create');
        } else {
            $('div').trigger('create');
        }
        $('input[data-fieldType="dateRec"]').datepicker({
            minDate: new Date(2010, 0, 1),
            dateFormat: 'mm/dd/yy',
            constrainInput: true,
            beforeShowDay: nationalDays
        });
    };

    self.saveAggregateInvoiceData = function () {
        if (!self.validateInvoice()) return false;

        var accReceivableSaveJSON = $.extend(true, {}, gServiceJobData);

        accReceivableSaveJSON.listJobAccountsReceivable = [];

        $.each(self.listJobAccountsReceivable(), function (key, val) {
            if (val["update"]) {
                delete val["update"];
                accReceivableSaveJSON.listJobAccountsReceivable.push(ko.mapping.toJS(val, mapping));
            }
        });
        var k = accReceivableSaveJSON;
        postCORS(gFinanceInfoServicePostUrl, JSON.stringify(accReceivableSaveJSON), function (response) {
            $('#alertmsg').html('Accounts Receivables have been saved successfully.');
            $("#popupDialog").popup('open');
        }, function (response_error) {
            showErrorResponseText(response_error, true);
        });
    };
    self.validateInvoice = function () {
        var msg = "";
        var invoice_data = $('input[type="text"][data-fieldType=ic],[data-fieldType=ia],[data-fieldType=a],[data-fieldType=c],[data-fieldType=dateRec]');
        var regNumbers = /^[0-9]*$/;
        var regCurrency = /^\$?[0-9]+(\.[0-9][0-9])?$/;// /^\$?(\d{1,3}(\,\d{3})*|(\d+))(\.\d{2})?$/; //
        var job_desc_regex = /^[ A-Za-z0-9,._-]*$/;
        var type = "";
        var count_a = 0;
        var count_va = 0;
        var count_iav = 0
        var count_vc = 0
        var count_icv = 0
        var count_ia = 0;
        var count_c = 0;
        var count_ic = 0;
        var count_dt = 0;
        $.each(invoice_data, function (key, val) {
            type = $(val).attr('data-fieldType');
            if (type == "a" && count_a < 1) {

                if ($(val).val() == "") {
                    if (msg != undefined && msg != "" && msg != null)
                        msg += "<br />Please enter Postage Amount";
                    else
                        msg = "Please enter Postage Amount";
                    if (msg != "" && count_a < 1) {
                        count_a++;
                    }
                }
            }
            if (type == "a" && count_va < 1) {
                if ($(val).val() != "" && !regCurrency.test($(val).val())) {
                    if (msg != undefined && msg != "" && msg != null)
                        msg += "<br />Please enter valid Postage Amount";
                    else
                        msg = "Please enter valid Postage Amount";
                    if (msg != "" && count_va < 1) {
                        count_va++;
                    }
                }
            }
            if ((type == "c" || type == "pc") && count_c < 1) {
                if ($(val).val() == "") {
                    if (msg != undefined && msg != "" && msg != null)
                        msg += "<br />Please enter Postage Check Number";
                    else
                        msg = "Please enter Postage Check Number";
                    if (msg != "" && count_c < 1) {
                        count_c++;
                    }
                }
            }
            if ((type == "c" || type == "pc") && count_vc < 1) {
                if ($(val).val() != "" && !regNumbers.test($(val).val())) {
                    if (msg != undefined && msg != "" && msg != null)
                        msg += "<br />Please enter valid Postage Check Number";
                    else
                        msg = "Please enter valid Postage Check Number";
                    if (msg != "" && count_vc < 1) {
                        count_vc++;
                    }
                }
            }
            if (type == "ia" && count_ia < 1) {
                if ($(val).val() == "") {
                    if (msg != undefined && msg != "" && msg != null)
                        msg += "<br />Please enter Invoice Amount";
                    else
                        msg = "Please enter Invoice Amount";
                    if (msg != "" && count_ia < 1) {
                        count_ia++;
                    }
                }
            }
            if (type == "ia" && count_iav < 1) {
                if ($(val).val() != "" && !regCurrency.test($(val).val())) {
                    if (msg != undefined && msg != "" && msg != null)
                        msg += "<br />Please enter valid Invoice Amount";
                    else
                        msg = "Please enter valid Invoice Amount";
                    if (msg != "" && count_iav < 1) {
                        count_iav++;
                    }
                }
            }
            if (type == "ic" && count_ic < 1) {
                if ($(val).val() == "") {
                    if (msg != undefined && msg != "" && msg != null)
                        msg += "<br />Please enter Invoice Check Number";
                    else
                        msg = "Please enter Invoice Check Number";
                    if (msg != "" && count_ic < 1) {
                        count_ic++;
                    }
                }
            }
            if (type == "ic" && count_icv < 1) {
                if ($(val).val() != "" && !regNumbers.test($(val).val())) {
                    if (msg != undefined && msg != "" && msg != null)
                        msg += "<br />Please enter valid Invoice Check Number";
                    else
                        msg = "Please enter valid Postag Invoice Number";
                    if (msg != "" && count_icv < 1) {
                        count_icv++;
                    }
                }
            }
            if (type == "dateRec" && count_dt < 1) {
                if ($(val).val() == "") {
                    if (msg != undefined && msg != "" && msg != null)
                        msg += "<br />Please enter Date";
                    else
                        msg = "Please enter Date";
                    if (msg != "" && count_dt < 1) {
                        count_dt++;
                    }
                }
            }
        });
        if ($('#txtNotes').val() != "" && !job_desc_regex.test($('#txtNotes').val())) {
            if (msg != undefined && msg != "" && msg != null)
                msg += '<br />Please enter valid Notes.The following characters are not allowed "\/@#$%^&*(){}[]|?<>~`+\'".';
            else
                msg = 'Please enter valid Notes.The following characters are not allowed "\/@#$%^&*(){}[]|?<>~`+\'".';
        }
        if (msg != "") {
            $('#alertmsg').html(msg);
            $('#alertmsg').css('text-align', 'left');
            $("#popupDialog").popup('open');
            $("#okBut").attr("onclick", "$('#popupDialog').popup('close')");
            return false;
        }
        return true;

    };
};

function showHomePage(type) {
    if (type == "home") {
        window.location.href = "../index.htm";
    }
    else if (type === "jobmanagement") {
        window.location.href = " ../jobManagement/jobManagement.html";
    }
    else if ((type == "joblookup")) {
        sessionStorage.removeItem('isAggregated');
        window.location.href = " ../adminFinance/financeJobLookUp.html";
    }
};

