﻿
var jobInfo = {};

$("#_postToProduction").live('pagebeforecreate', function (event) {
    //createConfirmMessage('_postToProduction');
    if (sessionStorage.approveProductionInfo != undefined && sessionStorage.approveProductionInfo != null && sessionStorage.approveProductionInfo != "") {
        jobInfo = $.parseJSON(sessionStorage.approveProductionInfo);
        //sessionStorage.authString = jobInfo.s;
    }
    createConfirmMessage("_postToProduction");
    displayMessage("_postToProduction");
});
$(document).on('pageshow', '#_postToProduction', function (event) {
    $('#spnJobName').html(jobInfo.n);
    $('#spnJobNumber').html(jobInfo.j);
});

function approveClick() {
    $('#confirmMsg').html('Are you sure you want to move the ' + jobInfo.n + ' for job number ' + jobInfo.j + ' to production?');
    $('#okButConfirm').text('Move to Production');
    $('#okButConfirm').bind('click', function () {
        $('#popupConfirmDialog').popup('close');
        fnApprove();
    });
    $('#popupConfirmDialog').popup('open', { positionTo: 'window' });
}

function fnApprove() {
    var post_approvers_info = {};
    post_approvers_info["customerNumber"] = jobInfo.c;
    post_approvers_info["jobNumber"] = jobInfo.j;
    var post_approval_url = serviceURLDomain + "api/Job_postToProduction";
    postCORS(post_approval_url, JSON.stringify(post_approvers_info), function (response) {
        var approve_job = response;
        if (response.status == "success!") {
            $('#alertmsg').html('Job has been submitted for production');
            $('#okBut').bind('click', function () {
                window.location.href = "../index.htm"
            });
        }
        else {
            $('#alertmsg').html('Failed to submit the job for production. Please try after sometime or contact administrator');
        }
        window.setTimeout(function displayStatus() {
            $('#popupDialog').popup('open', { positionTo: 'window' });
        }, 500);
    }, function (response_error) {
        //var msg = 'Job creation failed.';
        var msg = response_error.responseText;
        $('#alertmsg').text(msg);
        $('#popupDialog').popup('open', { positionTo: 'window' });
        var error = response_error;
    });
    $('#okButConfirm').unbind('click');
};
