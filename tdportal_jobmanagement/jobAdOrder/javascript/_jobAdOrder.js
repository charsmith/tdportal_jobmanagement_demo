﻿$.mobile.popup.prototype.options.history = false;
jQuery.support.cors = true;

//******************** Data URL Variables Start **************************
var pageObj;
appPrivileges = jQuery.parseJSON(getSessionData("appPrivileges"));
//var gServiceAddOrderUrl = 'JSON/adOrder.JSON';
var gServiceAddOrderUrl = serviceURLDomainIPInternal + 'api/AdOrderRollups';
var getAdvtInfoUrl = serviceURLDomainIPInternal + 'api/AdOrder';
var getIssueDateUrl = serviceURLDomainIPInternal + "api/AdOrderIssueDates"
var reportsServiceUrl = "../JSON/_stngReportNames.JSON";
var reportsServiceData = {};
var gServiceAdOrderData = [];
var gIssueDatesList = [];
//******************** Data URL Variables End **************************

//******************** Page Load Events Start **************************
$('#_jobAdOrder').live('pagebeforecreate', function (event) {
    ko.setTemplateEngine(new ko.nativeTemplateEngine);
    displayNavLinks();
    displayMessage('_jobAdOrder');
    createConfirmMessage("_jobAdOrder");
    createOrderConfirmMessage("_jobAdOrder");
    $("#navLinksPanel").on("panelopen", function (event, ui) {
        sessionStorage.isNavPanelOpen = true;
    });
    loadIssueDates();
    //makeAddata();
    displayReportsList();
});

$(document).keydown(function (e) {
    if (e.keyCode === 8) {
        var d = e.srcElement || e.target;
        if ((e.target.nodeName != "INPUT"))
            return false;
    }
});

$(document).on('pageshow', '#_jobAdOrder', function (event) {
    persistNavPanelState();
    if (sessionStorage.adOrderIssueDate != undefined && sessionStorage.adOrderIssueDate != null && sessionStorage.adOrderIssueDate != "") {
        $('#ddlIssueDate').val(sessionStorage.adOrderIssueDate).selectmenu('refresh').trigger('change');
    }
    else {
        $('#btnReports').addClass('ui-disabled');
        $('#btnReports')[0].disabled = true;
        $('#btnPostToSAM').addClass('ui-disabled');
        $('#btnPostToSAM')[0].disabled = true;
        $('#btnProcessAditOrder').addClass('ui-disabled');
        $('#btnProcessAditOrder')[0].disabled = true;
    }

});

//******************** Page Load Events End **************************

//******************** Public Functions Start **************************

var mapping = { 'ignore': ["__ko_mapping__"] }

var adOrderViewModel = function (ad_order) {
    var that = this;
    ko.mapping.fromJS(ad_order, {}, that);
    that.orderedPubsData = ad_order.orderedPubs;
    that.tot = ko.computed(function () {
        var tot = 0;
        $.each(that.orderedPubs(), function (key, val) {
            if (val.count != undefined && val.count != null)
                tot += parseInt(val.count());
        });
        return tot;
    });
    that.advtName = ko.computed(function () {
        return that.versionOrderName() + " - " + that.version();
    });
    that.publishName = ad_order.versionOrderName; //.versionOrderName;
    that.versionAttributes = ad_order.attributes;
}

var adOrderVM = function () {
    var self = this;
    self.chkOptions = ko.observableArray();
    self.adOrderDataList = ko.observableArray();
    self.selectedVersion = ko.observable();
    self.versionDescription = ko.observable();
    self.versionAditID = ko.observable();
    self.selectedPubVersion = ko.observable();
    self.selectedAdvertiserPubs = ko.observableArray();
    self.selectedPreviousAdvtPubs = ko.observableArray();
    $.each(gServiceAdOrderData, function (a, b) {
        var temp_advt_info = {};

        temp_advt_info["status"] = b.status;
        temp_advt_info["version"] = b.version;
        temp_advt_info["title"] = "";
        temp_advt_info["orderedPubs"] = [];
        temp_advt_info["attributes"] = [];
        temp_advt_info["versionOrderName"] = b.customerName;
        temp_advt_info["quantity"] = b.quantity;
        temp_advt_info["adOrderId"] = b.adOrderId;
        temp_advt_info["totalWeightVerfiedAmount"] = b.totalWeightVerfiedAmount
        temp_advt_info["statusIcon"] = (b.status == "") ? 'carat-r' : 'myapp-' + b.status.replace(/_/g, '-');
        temp_advt_info["statusColor"] = (b.status == "not_received") ? '#900' : 'none';
        temp_advt_info["statusTextStyle"] = (b.status == "killed") ? 'line-through' : 'none';
        var temp_attrs = {};
        $.each(b, function (key, val) {
            if (key != "customerName" && key != "quantity") {
                temp_attrs = {};
                temp_attrs["name"] = key;
                temp_attrs["displayName"] = getFieldDisplayName(key);
                temp_attrs["value"] = ko.observable(val);
                temp_advt_info.attributes.push(temp_attrs);
            }
        });

        self.adOrderDataList.push(new adOrderViewModel(temp_advt_info));
    });

    self.editPubValues = function () {
        self.selectedPubVersion(this);
        $('#popUpEditPub').popup().popup('open');
        if (this.values.length > 0) {
            if (typeof this.values()[0].value() == "object")
                self.chkOptions(this.values()[0].value());
            else
                self.chkOptions(this.values()[0].value().split(','));
            self.selectedVersion(this.values()[1].value());
            self.versionDescription(this.values()[2].value());
            self.versionAditID(this.values()[3].value());
            $("input[type='checkbox']").checkboxradio('refresh')
            $('#ddlVersions').selectmenu('refresh');
        }
    }

    self.updateAdvtOrderDtls = function () {

    };

    //    self.postToSAMConfirm = function (data) {
    //
    //    };

    self.postToSAM = function (data) {
        var selected_issue_date = $('#ddlIssueDate').val();
    };

    self.editOrder = function (data) {
        var order_id = data.adOrderId();
        sessionStorage.adOrderId = order_id;
        sessionStorage.adOrderIssueDate = $('#ddlIssueDate').val();
        window.location.href = "../jobMaterialsReceiving/jobMaterialsReceiving.html"
    };

    self.advtOrder = function (data) {
        self.selectedAdvertiserPubs(data.orderedPubs);
        self.selectedPreviousAdvtPubs(ko.mapping.toJS(data.orderedPubs, mapping));
        $('#editOrder').popup('open');
        window.setTimeout(function applyThemes() {
            $('#editOrder').find('input').trigger('create');
        }, 1000);
    }
    self.cancelEdit = function (data) {
        self.selectedAdvertiserPubs(ko.mapping.fromJS(self.selectedPreviousAdvtPubs, []));
        $('#editOrder').popup('close');
    };
    self.killAdOrder = function () {
        self.selectedPubVersion().status('killed');
        self.closePopUp();
    };

    self.canKillEntireOrder = function (data, el, event) {
        $('#hdrAdOrderConfirm').html('Confirm');
        $('#adOrderConfirmMsg').html('Do you really want to kill the ' + data.versionOrderName() + ' ad order?');
        $('#dvEmails').empty();
        $('#okAdOrderConfirm').text('Kill');
        $('#okAdOrderConfirm').unbind('click');
        $('#okAdOrderConfirm').bind('click', function () {
            //self.killEntireOrder(data);
            self.killEntireOrder(data, el, event);
            $('#popupAdOrderConfirmDialog').popup('close');
        });
        $('#popupAdOrderConfirmDialog').popup('open');
    };
    self.canAddEntireOrder = function (data, el, event) {
        $('#hdrAdOrderConfirm').html('Confirm');
        $('#adOrderConfirmMsg').html('Do you really want to add the ' + data.versionOrderName() + ' ad order?');
        $('#dvEmails').empty();
        $('#okAdOrderConfirm').text('Add');
        $('#okAdOrderConfirm').unbind('click');
        $('#okAdOrderConfirm').bind('click', function () {
            //pageObj.postToSAM(data);
            $('#popupAdOrderConfirmDialog').popup('close');
        });
        $('#popupAdOrderConfirmDialog').popup('open');
    };
    self.killOrderConfirm = function (data) {
        $('#confirmMsg').html('Are you sure you want to Kill this order?');
        $('#okButConfirm').text('Kill Order');
        $('#okButConfirm').bind('click', function () {
            self.killEntireOrder(data);
        });
        $('#popupConfirmDialog').popup('open');
    };

    self.killEntireOrder = function (data, el, event) {
        data.status('killed');
        data.statusIcon('myapp-killed');
        data.statusColor('none');
        data.statusTextStyle('line-through');
        $.each(data.attributes(), function (key, val) {
            if (val.name() == "status") {
                val.value('killed');
                return false;
            }
        });
        $.each(data.orderedPubs(), function (key, val) {
            val.status('killed');
        });
        $('#popupConfirmDialog').popup('close');
    };

    self.closePopUp = function () {
        $('#popUpEditPub').popup().popup('close');
    }

    self.updateVersionValues = function () {
        //self.selectedPubVersion();
        self.selectedPubVersion().values()[0].value(self.chkOptions());
        self.selectedPubVersion().values()[1].value(self.selectedVersion());
        self.selectedPubVersion().values()[2].value(self.versionDescription());
        self.selectedPubVersion().values()[3].value(self.versionAditID());
        $('#popUpEditPub').popup().popup('close');
    }
    self.getAdvertiserData = function (event, data) {
        var adv_data = ko.contextFor(event.target).$data;
        var post_json = {};
        var attrs = adv_data.attributes();
        $.each(attrs, function (key, val) {
            var b = key;
            post_json["issueDate"] = $('#ddlIssueDate').val();
            //if (val["name"]() == "pageType" || val["name"]() == "pageCount" || val["name"]() == "weight")
            if (val["name"]() == "version" || val["name"]() == "materialNumber" || val["name"]() == "customerNumber" || val["name"]() == "version")
                post_json[val["name"]()] = val["value"]();
        });
        var c = post_json;
        postCORS(getAdvtInfoUrl, JSON.stringify(post_json), function (data) {
            var temp_data = data;
            var temp_ordered_pubs = []; // ko.observableArray([]);
            var temp_ordered_pub = ko.observable({});
            $.each(data, function (key, val) {
                temp_ordered_pub = {};
                temp_ordered_pub["name"] = ko.observable(val.product);
                temp_ordered_pub["status"] = ko.observable(adv_data.status());
                temp_ordered_pub["count"] = ko.observable(parseInt(val.quantity.replace(/,/g, '')));
                temp_ordered_pub["values"] = ko.observableArray([]);
                temp_ordered_pubs.push(temp_ordered_pub);
                adv_data.orderedPubs(temp_ordered_pubs);
            });

            $(event.target).find('ul').listview('refresh');
            //ko.applyBindings(new adOrderVM());
        }, function (error_response) {
            showErrorResponseText(error_response,true);
        });
    }
}
var canPostToSAM = function () {
    var selected_issue_date = $('#ddlIssueDate').val();
    var selected_issue_date_text = $('#ddlIssueDate').find('option:selected').text();
    if (selected_issue_date == undefined || selected_issue_date == null || selected_issue_date == "") {
        return false;
    }
    $('#confirmMsg').html('Do you really want to post the ' + selected_issue_date_text + ' issue to SAM?');
    $('#okButConfirm').text('Post to SAM');
    $('#okButConfirm').bind('click', function () {
        //pageObj.postToSAM(data);
        $('#popupConfirmDialog').popup('close');
    });
    $('#popupConfirmDialog').popup('open');
};

var canProcessAditOrder = function (type) {
    var selected_issue_date = $('#ddlIssueDate').val();
    if (selected_issue_date == undefined || selected_issue_date == null || selected_issue_date == "") {
        return false;
    }
    $('#hdrAdOrderConfirm').html('Run ' + type + ' Process');
    $('#adOrderConfirmMsg').html('Enter an email address to receive an alert message containing the number of records pulled when processing completes.');
    $('#dvEmails').empty();
    $('#dvEmails').append('<input type="text" name="txtEmails" id="txtEmails" data-theme="c" style="width:100%" placeholder="Email Address">').trigger('create');
    $('#okAdOrderConfirm').text('Run Process');
    $('#okAdOrderConfirm').bind('click', function () {
        if (!(emailValidation($("#txtEmails").val()))) {
            return false;
        }
        //pageObj.postToSAM(data);
        $('#popupAdOrderConfirmDialog').popup('close');
    });
    $('#popupAdOrderConfirmDialog').popup('open');
};

var loadIssueDates = function () {
    getCORS(getIssueDateUrl, null, function (data) {
        gIssueDatesList = data;
        //var temp_date = new Date(data[0].date);
        //var date = new Date(Date.UTC(temp_date.getYear(), temp_date.getMonth(), temp_date.getDate()));
        var date;
        //var new_date = date.substring(date.indexOf(' ') + 1, date.length);
        var temp_issue_dates_json = [];
        temp_issue_dates_json.push({
            "dateName": "-- Select --",
            "dateValue": ""
        });
        $.each(gIssueDatesList, function (key, val) {
            date = new Date(val.date).toDateString();
            temp_issue_dates_json.push({
                "dateName": val.dow + ', ' + date.substring(date.indexOf(' ') + 1, date.length),
                "dateValue": val.date.replace(/\//g, '_')
            });
        });
        $("#ddlIssueDates").empty();
        $("#issueDatesTemplate").tmpl(temp_issue_dates_json).appendTo("#ddlIssueDate");
    }, function (error_response) {
        showErrorResponseText(error_response,true);
    });
};

function makeAddata() {
    var selected_issue_date = $('#ddlIssueDate').val();
    if (selected_issue_date != "") {
        $('#btnReports').removeClass('ui-disabled');
        $('#btnReports')[0].disabled = false;
        $('#btnPostToSAM').removeClass('ui-disabled');
        $('#btnPostToSAM')[0].disabled = false;
        $('#btnProcessAditOrder').removeClass('ui-disabled');
        $('#btnProcessAditOrder')[0].disabled = false;
        sessionStorage.adOrderIssueDate = $('#ddlIssueDate').val();
        getCORS(gServiceAddOrderUrl + '/' + selected_issue_date, null, function (data) {
            gServiceAdOrderData = data;
            var is_bound = (pageObj != undefined && pageObj != null);
            if (!is_bound) {
                pageObj = new adOrderVM();
                ko.applyBindings(pageObj);
                //window.setTimeout(function setDelay() {
                //$('#dvAdOrder ul').listview();
                $('#dvAdOrder').collapsible();
                $('div[data-role="collapsible-set"]').trigger('create');
                $('div[data-role="collapsible-set"]').collapsibleset().collapsibleset("refresh");
                //        $('#dvAdOrderInfo div[data-role="collapsible"]').bind('expand', function (event) {
                //            alert('welcome');
                //        });

                $('#dvAdOrderInfo div[data-role="collapsible"]').bind('collapsibleexpand', function (event, data) {
                    pageObj.getAdvertiserData(event, data);
                });
                //}, 20);

            }
            else {
                //pageObj.adOrderDataList([]);
                var temp_arr = [];
                $.each(gServiceAdOrderData, function (a, b) {
                    var temp_advt_info = {};
                    temp_advt_info["status"] = b.status;
                    temp_advt_info["version"] = b.version;
                    temp_advt_info["title"] = "";
                    temp_advt_info["orderedPubs"] = [];
                    temp_advt_info["attributes"] = [];
                    temp_advt_info["versionOrderName"] = b.customerName;
                    temp_advt_info["quantity"] = b.quantity;
                    temp_advt_info["adOrderId"] = b.adOrderId;
                    temp_advt_info["totalWeightVerfiedAmount"] = b.totalWeightVerfiedAmount;
                    temp_advt_info["statusIcon"] = (b.status == "") ? 'carat-r' : 'myapp-' + b.status.replace(/_/g, '-');
                    temp_advt_info["statusColor"] = (b.status == "not_received") ? '#900' : 'none';
                    temp_advt_info["statusTextStyle"] = (b.status == "killed") ? 'line-through' : 'none';
                    var temp_attrs = {};
                    $.each(b, function (key, val) {
                        if (key != "customerName" && key != "quantity") {
                            temp_attrs = {};
                            temp_attrs["name"] = key;
                            temp_attrs["displayName"] = getFieldDisplayName(key);
                            temp_attrs["value"] = val;
                            temp_advt_info.attributes.push(temp_attrs);
                        }
                    });

                    temp_arr.push(new adOrderViewModel(temp_advt_info));

                });
                pageObj.adOrderDataList(temp_arr);
                $('#dvAdOrder').collapsible();
                $('div[data-role="collapsible-set"]').trigger('create');
                $('div[data-role="collapsible-set"]').collapsibleset().collapsibleset("refresh");
                $('#dvAdOrderInfo div[data-role="collapsible"]').bind('collapsibleexpand', function (event, data) {
                    pageObj.getAdvertiserData(event, data);
                });
            }
        }, function (error_response) {
            showErrorResponseText(error_response,true);
        });
    }
    else {
        var temp_arr = [];
        sessionStorage.removeItem('adOrderIssueDate');
        $('#btnReports').addClass('ui-disabled');
        $('#btnReports')[0].disabled = true;
        $('#btnPostToSAM').addClass('ui-disabled');
        $('#btnPostToSAM')[0].disabled = true;
        $('#btnProcessAditOrder').addClass('ui-disabled');
        $('#btnProcessAditOrder')[0].disabled = true;
        $('#pnlReports').panel('close');
        pageObj.adOrderDataList(temp_arr);
    }



    //    $.getJSON(gServiceAddOrderUrl, function (data) {
    //        gServiceAdOrderData = data;
    //        ko.applyBindings(new adOrderVM());
    //    });
}

ko.bindingHandlers.insertText = {
    init: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
        var innerBindingContext = bindingContext.extend(valueAccessor);
        ko.applyBindingsToDescendants(innerBindingContext, element);
        return { controlsDescendantBindings: true };
    }
};

function displayReportsList() {
    $.getJSON(reportsServiceUrl, function (data) {
        reportsServiceData = data;
        var accounting_reports = '<li data-theme="d" data-icon="delete"><a href="" onclick="$(\'#pnlReports\').panel(\'close\');" data-rel="close" >Reports</a></li>';
        $.each(reportsServiceData, function (key, val) {
            var show_report = true;
            $.each(val, function (key1, val1) {
                accounting_reports += '<li><a onclick="fnDisplayReports(\'' + val1.value + '\',\'\')" href="#popupReportsParam" data-rel="popup" data-position-to="window" data-inline="true">' + val1.name + '</a></li>';
            });
        });
        $("#ulReports").append(accounting_reports);
        $('#ulReports').listview('refresh');
    });
}

//calls the webservice to show the SSRS reports based on the user selection report
function fnDisplayReports(report_name) {
    var issue_date = "";
    switch (report_name) {
        case "Materials_at_2.5%":
            window.open("http://s-reportserver/ReportServer/Pages/ReportViewer.aspx?%2fShared+Mail+-+Materials%2fMaterials+at+2.5%25&rs:Command=Render&rs:ClearSession=true&issueDate=" + sessionStorage.adOrderIssueDate.replace(/_/g, '/'));
            break;
        case "Materials_Not_Received":
            window.open("http://s-reportserver/ReportServer/Pages/ReportViewer.aspx?%2fShared+Mail+-+Materials%2fMaterials+Not+Received&rs:Command=Render&rs:ClearSession=true&issueDate=" + sessionStorage.adOrderIssueDate.replace(/_/g, '/'));
            break;
        case "Materials_with_Weights":
            window.open("http://s-reportserver/ReportServer/Pages/ReportViewer.aspx?%2fShared+Mail+-+Materials%2fMaterials+With+Weights&rs:Command=Render&rs:ClearSession=true&issueDate=" + sessionStorage.adOrderIssueDate.replace(/_/g, '/'));
            break;
        case "Late_Delivery":
            window.open("http://s-reportserver/ReportServer/Pages/ReportViewer.aspx?%2fShared+Mail+-+Materials%2fLate+Stock+Delivery+Report&rs:Command=Render&rs:ClearSession=true&issueDate=" + sessionStorage.adOrderIssueDate.replace(/_/g, '/'));
            break;
    }

}

function createOrderConfirmMessage(page_name) {
    var msg_box = '';
    msg_box = '<div data-role="popup" id="popupAdOrderConfirmDialog" data-overlay-theme="d" data-theme="c" style="text-align:left;max-width:500px;" class="ui-corner-all" data-history="false">';
    msg_box += '<div data-role="header" data-theme="d" class="ui-corner-top" id="adOrderDialogbox">';
    msg_box += '<h1 id="hdrAdOrderConfirm"></h1>';
    msg_box += '</div>';
    msg_box += '<div data-role="content" data-theme="c" class="ui-corner-bottom ui-content">';
    msg_box += '<div id="adOrderConfirmMsg"></div>';
    msg_box += '<div id="dvEmails"></div>';
    msg_box += '<div align="right">';
    msg_box += '<a href="#" data-role="button" data-inline="true" data-theme="a" id="cancelAdOrderConfirm" data-mini="true" onclick="$(\'#popupAdOrderConfirmDialog\').popup(\'open\');$(\'#popupAdOrderConfirmDialog\').popup(\'close\');">Cancel</a>';
    msg_box += '<a href="#" data-role="button" data-inline="true" data-theme="a" id="okAdOrderConfirm" data-mini="true">Ok</a>';
    msg_box += '</div>';
    msg_box += '</div></div>';
    $("#" + page_name).append(msg_box);
}

function emailValidation(email_id) {
    var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    var msg = "";
    if (email_id == "")
        msg = 'Email address is mandatory to download Proofs.';
    else {
        if (!expr.test($.trim(email_id)))
            msg = 'Invalid email address.';
    }
    $('#alertmsg').text("");
    $('#okBut').attr('onclick', '$("#popupDialog").popup("close");');
    if (msg != "") {
        $('#popupAdOrderConfirmDialog').popup('close');
        $('#alertmsg').text(msg);
        $('#popupDialog').popup('open');
        $('#okBut').attr('onclick', '$(\'#alertmsg\').text("");$("#popupDialog").popup("close");$("#popupAdOrderConfirmDialog").popup("open");');
        return false;
    }
    return true;
}

//******************** Public Functions End **************************