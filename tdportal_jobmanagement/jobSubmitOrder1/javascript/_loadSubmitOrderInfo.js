﻿//var loadSubmitOrderInfo = function (self) {
//******************** View Model Start ******************************
var jobSubmitOrder = function () {
    var self = this;
    //******************** Global Variables Start **************************
    self.gData;
    self.jobNumber = getSessionData("jobNumber");
    self.facilityId = sessionStorage.facilityId;
    self.approversList = [];
    self.gOutputData;
    self.postJobURL;
    self.PAGE_NAME = 'jobSubmitOrder.html';
    self.customerDetailsData = "";
    var gEditJobService = serviceURLDomain + "api/JobTicket/" + sessionStorage.facilityId + "/" + sessionStorage.jobNumber + '/asdf' /*+ sessionStorage.username;*/ ;
    //******************** Global Variables End **************************
    new makeSubmitOrder(self);
    var Order = function (order_data) {
        var self = this;
        ko.mapping.fromJS(order_data, [], self);
    }

    self.ordersData = ko.observableArray();
    self.loadSubmitOrder = function () {
        var displayEmail = "";
        var customerDetails = ko.utils.arrayFilter(self.customerDetailsData, function (customer) {
            return customer.jobCustomerNumber == jobCustomerNumber;
        });
        var tempList = {};
        self.jobCustomerNumber = jobCustomerNumber;
        $.each(self.gData.checkOutGrid.checkOutGridStoreList, function (a, b) {
            var names = Object.getOwnPropertyNames(b);
            $.each(Object.getOwnPropertyNames(b), function (c, d) {
                if (customerDetails[0].gridColIds.indexOf(d) > -1)
                    tempList[d] = b[d];
            });
            tempList.optionalChargesPretty = tempList.optionalChargesPretty.substring(1);
            self.ordersData.push(new Order(tempList));
        });
        self.finalInvoiceAmount = self.gData.checkOutGrid.finalInvoiceAmountPretty;
        self.emails = ko.observable();
        self.approversViewersList = ko.observable({});
        self.approversViewersList(self.approversList);
        self.isTextField = ko.observable((sessionStorage.userRole == 'user') ? 0 : 1);
        self.otherComments = ko.observable('test');
        //to filter customer details based on logged in customer job number
        var customerDetails = ko.utils.arrayFilter(self.customerDetailsData, function (customer) {
            return customer.jobCustomerNumber == jobCustomerNumber;
        });

        self.displayEmail = customerDetails[0].email;
        if (customerDetails != null && customerDetails[0].gridCols != undefined && customerDetails[0].gridCols != null)
            self.headers = customerDetails[0].gridCols;
    };

    self.submitOrder = function () {
        if (jobCustomerNumber == BBB || jobCustomerNumber == AAG || jobCustomerNumber == SPORTSAUTHORITY || jobCustomerNumber == WALGREENS || jobCustomerNumber == "6000140") {
            if (!self.artListUploaded()) return false;
        }
        if ($("#chkNoArtApprove").is(":checked") || $("#chkNoListApprove").is(":checked"))
            $("#btnContinuePopup").text("Continue");
        else
            $("#btnContinuePopup").text("Submit Order");

        var is_list_uploaded = false;
        var is_art_uploaded = false;
        var is_needs_count = false;

        if (self.gOutputData.standardizeAction.standardizeFileList != undefined && self.gOutputData.standardizeAction.standardizeFileList.length > 0 && self.gOutputData.standardizeAction.standardizeFileList[0].fileName != "")
            is_list_uploaded = true;

        if (self.gOutputData.artworkAction.artworkFileList != undefined && Object.keys(self.gOutputData.artworkAction.artworkFileList).length > 0)
            is_art_uploaded = true;

        if (self.gOutputData != undefined && self.gOutputData.selectedLocationsAction != undefined && self.gOutputData.selectedLocationsAction.selectedLocationsList != undefined && self.gOutputData.selectedLocationsAction.selectedLocationsList.length > 0) {
            $.each(self.gOutputData.selectedLocationsAction.selectedLocationsList, function (key, val) {
                if (val.needsCount) {
                    is_needs_count = true;
                    return false;
                }
            });
        }
        var msg = "";
        if ($("#ulJobLinks li[id*=liListSelection]").length > 0) {
            if (!is_art_uploaded || is_needs_count)
                msg = "All artwork files should be upload and list selections made prior to submitting your order. Submitting your order will send Confirmation and Approval emails to the users listed on this page. Do you want to Continue to Submit this Order?";
        }
        else if (!is_list_uploaded || (jobCustomerNumber != AAG && jobCustomerNumber != SPORTSAUTHORITY && jobCustomerNumber != WALGREENS && jobCustomerNumber != "6000140" && !is_art_uploaded)) {
            var art_work_msg = ((jobCustomerNumber == "6000140" || jobCustomerNumber == AAG) ? '' : 'and artwork files ');
            var approval_email_msg = ((jobCustomerNumber == "6000140" || jobCustomerNumber == AAG || jobCustomerNumber == SPORTSAUTHORITY || jobCustomerNumber == WALGREENS) ? '' : 'and Approval ');
            msg = "All lists " + art_work_msg + "should be upload prior to submitting your order. Submitting your order will send Confirmation " + approval_email_msg + "emails to the users listed on this page. Do you want to Continue to Submit this Order?";
        }
        if (msg != "") {
            $('#dvMsg').html(msg);
            $('#confirmAdmin').popup('open');
        }
        else {
            var submit_log_info = serviceURLDomain + "api/Readiness_submit/" + self.facilityId + '/' + jobCustomerNumber + "/" + self.jobNumber + '/';
            //var submit_log_info = serviceURLDomain + "api/Readiness_submit/" + self.facilityId + '/6000420/602098/';
            getCORS(submit_log_info, null, function (data) {
                if (data.status.toLowerCase() != "ok") {
                    self.loadLogsInfo(data);
                    $('#postReminder').popup('open', { positionTo: 'window' });
                }
                else {
                    self.fnJobFinal("submit");
                }
            }, function (response_error) {
				showErrorResponseText(response_error, false);
            });
        }
    };
    self.listJobDetails = ko.observableArray([]);
    self.lastSubmittedDate = ko.observable({});
    self.loadLogsInfo = function (data) {
        //This function will display details info for selected logging
        self.listJobDetails([]);
        self.lastSubmittedDate(data.last_submitted_time);
        $.each(data.logList, function (a, b) {
            //for (i = 0; i < b.length; i++) {
            self.listJobDetails.push(b);
            //}
        });
    };

    self.fnJobFinal = function (action_type) {
        $('#confirmAdmin').popup('close');
        // post approvers info
        var approvers_info = {};
        $('#postReminder').popup('close');
        $.each(self.approversViewersList(), function (key, val) {
            if (val != null && val != "") {
                approvers_info["approvalType"] = key;
                //            if ((key.toLowerCase().indexOf('viewer') > -1))
                //                approvers_info["viewers"] = val;
                //            else
                approvers_info["approvers"] = val;
                (function (post_approvers_info) {
                    var my_timer = setInterval(function () {
                        var post_approval_url = serviceURLDomain + "api/Tagging_post_approvers/" + sessionStorage.facilityId + "/" + jobCustomerNumber + "/" + self.jobNumber
                        postCORS(post_approval_url, JSON.stringify(post_approvers_info), function (response) {
                            approvers_info = {};
                        }, function (response_error) {
                            showErrorResponseText(response_error, false);
                            ////var msg = 'Job creation failed.';
                            //var msg = response_error.responseText;
                            //$('#alertmsg').text(msg);
                            //$('#popupDialog').popup('open', { positionTo: 'window' });
                            //var error = response_error;
                        });
                        clearInterval(my_timer);
                    }, 1000);
                })(approvers_info);
                approvers_info = {};
            }
        });

        //submit job
        if (appPrivileges.customerNumber === CW && self.jobNumber != "-1") //Biz rules for needsBillingSave flag
            self.gOutputData = fnNeedsBillingSave(self.gOutputData);

        postJobURL = serviceURLDomain + 'api/JobTicket/' + self.facilityId + '/' + self.jobNumber + '/' + sessionStorage.username + '/' + action_type + '/ticket';
        postCORS(postJobURL, JSON.stringify(self.gOutputData), function (response) {
            var msg = (sessionStorage.jobNumber == "-1") ? 'Your job was saved.' : 'Your job was updated.';
            msg = (action_type == "submit") ? ((jobCustomerNumber == BBB || jobCustomerNumber == AAG || jobCustomerNumber == SPORTSAUTHORITY || jobCustomerNumber == WALGREENS || jobCustomerNumber == "6000140") ? "Thanks for submitting your job to Gizmo. Once your files have processed, you’ll receive an automated notification from the system – usually within half an hour or less." : "Thank you! Your order has been submitted for execution.") : msg;
            //
            if (response != "") {
                if (!isNaN(parseInt(response.replace(/"/g, "")))) {
                    self.jobNumber = parseInt(response.replace(/"/g, ""));
                    sessionStorage.jobNumber = self.jobNumber;
                    self.gOutputData.jobNumber = self.jobNumber;
                    sessionStorage.jobDesc = (self.gOutputData.jobName != undefined) ? self.gOutputData.jobName : "";
                    $('#liJobSummary').css('display', "block");
                    displayJobInfo();
                    sessionStorage.jobSetupOutput = JSON.stringify(self.gOutputData);
                    sessionStorage.jobSetupOutputCompare = JSON.stringify(self.gOutputData);
                }
                else
                    msg = response;
                $('#alertmsg').text(msg);
                //self.makeGData();
                window.setTimeout(function getDelay() {
                    if (msg == 'Your job was saved.' || msg == 'Your job was updated.')
                        $("#okBut").attr("onclick", "$('#popupDialog').popup('close');");

                    if (action_type == "submit" && (jobCustomerNumber == BBB || jobCustomerNumber == AAG || jobCustomerNumber == SPORTSAUTHORITY || jobCustomerNumber == WALGREENS))
                        $("#okBut").bind("click", function endDemoJob() {
                            $('#popupDialog').popup('close');
                            window.location.href = "../index.htm";
                        });

                    $('#popupDialog').popup('open', { positionTo: 'window' });
                }, 500);
            }
            else {
                var msg = 'Job creation failed.';
                $('#alertmsg').text(msg);
                $('#popupDialog').popup('open', { positionTo: 'window' });
            }
        }, function (response_error) {
            var msg = 'Job creation failed.';
            var msg = response_error.responseText;
            if (response_error.status == 409) {
                msg = "Your job <b>" + self.gOutputData.jobName + "</b> could not be saved as there is already a job in the system with that name. Please rename your job and try again.";
                $('#alertmsg').html(msg);
            }
            else
showErrorResponseText(response_error, false);
                //$('#alertmsg').text("Unknown Error: Contact a Site Administrator");
            //$('#popupDialog').popup('open', { positionTo: 'window' });
            //var error = response_error;
        });

    };
    self.displayOrderSummary = function () {
        var session_data = $.parseJSON(sessionStorage.jobSetupOutput)
        var order_summary = '';
        if (session_data.jobNumber == "-1") {
            order_summary = ' <h3>Order Summary</h3>';
            order_summary += '<div style="padding-bottom:12px;"><b>Job Name:</b> ' + session_data.jobName + ' <br /></div>';
            order_summary += '<div style="padding-bottom:12px;"><b>In-Home Date:</b>' + session_data.inHomeDate + '<br /></div>';
            order_summary += '<div style="padding-bottom:12px;"><b>Uploaded List Files:</b><br />NA<br /></div>';
            if (jobCustomerNumber != AAG && jobCustomerNumber != SPORTSAUTHORITY && jobCustomerNumber != WALGREENS && jobCustomerNumber != "6000140") {
                order_summary += '<div style="padding-bottom:12px;"><b>Uploaded Art Files:</b><br />NA<br /></div>';
            }
        }
        else {
            //getCORS(gEditJobService, null, function (data) {
            var list_files_data = (session_data.standardizeAction.standardizeFileList == undefined || session_data.standardizeAction.standardizeFileList == null || session_data.standardizeAction.standardizeFileList == "" || Object.keys(self.gOutputData.standardizeAction.standardizeFileList).length == 0 || self.gOutputData.standardizeAction.standardizeFileList[0].fileName == "") ? [] : session_data.standardizeAction.standardizeFileList;
            var art_files_data = (session_data.artworkAction.artworkFileList == undefined || session_data.artworkAction.artworkFileList == null || session_data.artworkAction.artworkFileList == "" || Object.keys(session_data.artworkAction.artworkFileList).length == 0) ? [] : session_data.artworkAction.artworkFileList;
            order_summary = ' <h3>Order Summary</h3>';
            order_summary += '<div style="padding-bottom:12px;"><b>Job Name:</b> ' + session_data.jobName + ' <br /></div>';
            order_summary += '<div style="padding-bottom:12px;"><b>In-Home Date:</b>' + session_data.inHomeDate + '<br /></div>';
            order_summary += '<div style="padding-bottom:12px;"><b>Uploaded List Files:</b><br />';
            if (list_files_data.length > 0) {
                $.each(list_files_data, function (key, val) {
                    order_summary += '<span>' + val.fileName + '</span><br />';
                });
            }
            else {
                order_summary += '<span>NA</span><br />';
            }
            order_summary += '</div>';
            if (jobCustomerNumber != AAG && jobCustomerNumber != SPORTSAUTHORITY && jobCustomerNumber != WALGREENS && jobCustomerNumber != "6000140") {
                order_summary += '<div style="padding-bottom:12px;"><b>Uploaded Art Files:</b><br />';
                if (Object.keys(art_files_data).length > 0) {
                    $.each(art_files_data, function (key, val) {
                        order_summary += '<span>' + val.fileName + '</span><br />';
                    });
                }
                else {
                    order_summary += '<span>NA</span><br />';
                }
                order_summary += '</div>';
            }
            //}, function (error_response) {
            //   showErrorResponseText(error_response);
            //})
        }
        $('#dvOrderSummary').append(order_summary);
    };
    self.artListUploaded = function () {
        var msg = '';
        if (self.gOutputData.standardizeAction.standardizeFileList == undefined || self.gOutputData.standardizeAction.standardizeFileList == null || self.gOutputData.standardizeAction.standardizeFileList == "" || Object.keys(self.gOutputData.standardizeAction.standardizeFileList).length == 0) {
            if (msg != undefined && msg != "" && msg != null)
                msg += "<br />Please select at least one List file.";
            else
                msg = "Please select at least one List file.";
        }
        else {
            var list_file_cnt = 0;
            $.each(self.gOutputData.standardizeAction.standardizeFileList, function (key, val) {
                if (val.fileName != "") {
                    var file_source_type = getFileType(val.sourceType);
                    if (file_source_type.toLowerCase() == "source") {
                        list_file_cnt++;
                    }
                }
            });
            if (list_file_cnt == 0) {
                if (msg != undefined && msg != "" && msg != null)
                    msg += "<br />Please select at least one List (Source) file.";
                else
                    msg = "Please select at least one List file.";
            }
        }
        if ((jobCustomerNumber != AAG && jobCustomerNumber != SPORTSAUTHORITY && jobCustomerNumber != WALGREENS && jobCustomerNumber != "6000140") && (self.gOutputData.artworkAction.artworkFileList == undefined || self.gOutputData.artworkAction.artworkFileList == null || self.gOutputData.artworkAction.artworkFileList == "" || Object.keys(self.gOutputData.artworkAction.artworkFileList).length == 0)) {
            if (msg != undefined && msg != "" && msg != null)
                msg += "<br />Please select at least one Art file";
            else
                msg = "Please select at least one Art file";
        }
        if (msg != "") {
            $('#alertmsg').html(msg);
            $('#alertmsg').css('text-align', 'left');
            $("#popupDialog").popup('open');
            $("#okBut").attr("onclick", "$('#popupDialog').popup('close');$('#popupComments').popup('open');");
            return false;
        }
        return true;

    };

}
