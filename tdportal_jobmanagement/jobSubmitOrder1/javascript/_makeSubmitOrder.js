﻿var makeSubmitOrder = function (self) {
    self.makeGData = function () {
        if (jobCustomerNumber == CASEYS) {
            var temp_json = {
                "checkOutAction": {
                    "checkOutApproval": null,
                    "storePaymentInfoList": [],
                    "name": "checkout1",
                    "type": "checkout",
                    "isEnabled": true
                },
                "checkOutGrid": {
                    "finalInvoiceAmountPretty": "$0.00",
                    "totalPostageReceivedPretty": "$0.00",
                    "totalBillingReceivedPretty": "$0.00",
                    "remainingBalancePretty": "$00.00",
                    "checkOutGridStoreList": []
                }
            }
        }
        $.getJSON(serviceURL, function (data) {
            self.gData = data;
            if (sessionStorage.jobSetupOutput != undefined && sessionStorage.jobSetupOutput != "" && sessionStorage.jobSetupOutput != null) {
                var output_json = jQuery.parseJSON(sessionStorage.jobSetupOutput);
                var selected_stores = output_json.selectedLocationsAction.selectedLocationsList;
                var inhome_date = output_json.inHomeDate;
                if (selected_stores != undefined && selected_stores.length > 0) {
                    $.each(selected_stores, function (a, b) {
                        self.gData.selectedLocationsList.push({
                            "city": b.city,
                            "state": b.state,
                            "originalIndex": b.originalIndex,
                            "quantity": b.quantity,
                            "address1": b.address1,
                            "zip": b.zip,
                            "storeName": b.storeName,
                            "storeId": b.storeId,
                            "storePk": b.pk
                        });
                        //TO BE REMOVED LATER 
                        if (jobCustomerNumber == CASEYS) {
                            temp_json.checkOutAction.storePaymentInfoList.push({
                                "postageReceivedPretty": "$0.00",
                                "billingReceivedPretty": "$0.00",
                                "billingCheck": 0,
                                "postageCheck": 0,
                                "city": b.city,
                                "state": b.state,
                                "storeName": null,
                                "storeId": b.storeId
                            });
                            temp_json.checkOutGrid.checkOutGridStoreList.push({
                                "quantityPretty": "1000",
                                "productionCPPPretty": "$0.24",
                                "postageCPPPretty": "$0.00",
                                "additionalStoreChargePretty": "$0.00",
                                "optionalChargesPretty": "$0.00",
                                "dataChargesPretty": "$0.00",
                                "totalPretty": "$240",
                                "storeName": "",
                                "storeId": b.storeId
                            });
                        }
                        //END
                    });
                    if (jobCustomerNumber == CASEYS) {
                        temp_json.checkOutGrid.finalInvoiceAmountPretty = '$' + (selected_stores.length * 240) + '.00';
                    }
                }
                //gData.pricingId = output_json.pricingId;
                self.gData["jobTemplate"] = (output_json.templateName != undefined) ? output_json.templateName : ((output_json.template != undefined) ? output_json.template : '');
                self.gData["mailstreamAction"] = output_json.mailstreamAction;
                if (output_json.mailstreamAction != undefined) {
                    self.gData.isFirstClassMail = (output_json.mailstreamAction.mailClass != undefined && output_json.mailstreamAction.mailClass == "1") ? 1 : 0;
                }
                if (jobCustomerNumber == CASEYS) {
                    self.gData = temp_json;
                    self.loadSubmitOrder();
                    $("#tblOrderGrid").css("display", "block");
                    $("#tblOrderGrid").css("display", "");
                    ko.applyBindings(self);
                    $("#tblOrderGrid").table("refresh");
                    self.makeGOutputData();
                }
                else {
                    postCORS(gCheckoutServiceURL + self.facilityId + '/' + ((appPrivileges.customerNumber == "203") ? CW : jobCustomerNumber) + '/' + self.jobNumber + '/asdf', JSON.stringify(self.gData), function (response) {
                        self.gData = response;
                        self.loadSubmitOrder();
                        $("#tblOrderGrid").css("display", "block");
                        $("#tblOrderGrid").css("display", "");
                        ko.applyBindings(self);
                        $("#tblOrderGrid").trigger('create');
                        $("#tblOrderGrid").table("refresh");
                        self.makeGOutputData();
                    }, function (error_response) {
                        //showErrorResponseText(error_response);
                        $("#tblOrderGrid").css("display", "none");
                        var customerDetails = ko.utils.arrayFilter(self.customerDetailsData, function (customer) {
                            return customer.jobCustomerNumber == jobCustomerNumber;
                        });
                        var checkout_grid_stores = [];
                        var temp_checkout_grid_store_list = {};
                        $.each(customerDetails[0].gridColIds, function (key, val) {
                            if (val != "storeId")
                                temp_checkout_grid_store_list[val] = (val != "quantityPretty") ? "$0.00" : "0";
                            else
                                temp_checkout_grid_store_list[val] = "";
                        });
                        checkout_grid_stores.push(temp_checkout_grid_store_list);
                        var temp_checkout_info = {
                            "checkOutAction": {
                                "checkOutApproval": null,
                                "storePaymentInfoList": [],
                                "name": "checkout1",
                                "type": "checkout",
                                "isEnabled": true
                            },
                            "checkOutGrid": {
                                "finalInvoiceAmountPretty": "$0.00",
                                "totalPostageReceivedPretty": "$0.00",
                                "totalBillingReceivedPretty": "$0.00",
                                "remainingBalancePretty": "$0.00",
                                "checkOutGridStoreList": checkout_grid_stores
                            }
                        }
                        //temp_checkout_info = { "checkOutAction": { "checkOutApproval": null, "storePaymentInfoList": [{ "postageReceivedPretty": "$0.00", "billingReceivedPretty": "$0.00", "billingCheck": 0, "postageCheck": 0, "city": "Auburn", "state": "WA", "storeName": null, "storeId": ""}], "name": "checkout1", "type": "checkout", "isEnabled": true }, "checkOutGrid": { "finalInvoiceAmountPretty": "$684.00", "totalPostageReceivedPretty": "$0.00", "totalBillingReceivedPretty": "$0.00", "remainingBalancePretty": "$684.00", "checkOutGridStoreList": [{ "quantityPretty": "1,500", "productionCPPPretty": "$0.235", "postageCPPPretty": "$0.217", "additionalStoreChargePretty": "$0.00", "optionalChargesPretty": "$0.00", "dataChargesPretty": "$4.00", "totalPretty": "$684.00", "storeName": "Poulsbo RV", "storeId": ""}]} };
                        self.gData = temp_checkout_info;
                        self.loadSubmitOrder();
                        ko.applyBindings(self);
                        self.makeGOutputData();
                    });
                }
            }
        });
    };

    self.makeGOutputData = function () {
        if (sessionStorage.jobSetupOutput == undefined && sessionStorage.jobSetupOutput == undefined)
            $.getJSON(gOutputServiceUrl, function (dataOutput) {
                self.gOutputData = dataOutput;
                self.gOutputData.checkOutAction = self.gData.checkOutAction;
            });
        else {
            self.gOutputData = jQuery.parseJSON(sessionStorage.jobSetupOutput);
            var is_firsttime = false;
            if (self.gOutputData.checkOutAction == undefined) { self.gOutputData["checkOutAction"] = {}; is_firsttime = true; }
            self.gOutputData.checkOutAction = self.gData.checkOutAction;

            if (!is_firsttime) {
                updateStorePaymentInfoInSession();
                self.gOutputData = jQuery.parseJSON(sessionStorage.jobSetupOutput);
                self.gData.checkOutAction = self.gOutputData.checkOutAction;
                if (self.gData.checkOutAction.storePaymentInfoList != undefined && self.gData.checkOutAction.storePaymentInfoList != null && self.gData.checkOutAction.storePaymentInfoList != "") {
                    $.each(self.gData.checkOutAction.storePaymentInfoList, function (key, val) {
                        $.each(val, function (key1, val1) {
                            if (key1.indexOf('Pretty') > -1) {
                                if (val1.indexOf('$') == -1)
                                    self.gData.checkOutAction.storePaymentInfoList[key][key1] = '$' + val1;
                                if (val1.indexOf('.00') == -1)
                                    self.gData.checkOutAction.storePaymentInfoList[key][key1] = self.gData.checkOutAction.storePaymentInfoList[key][key1] + '.00';

                            }
                        });
                    });
                }
            }

            if (sessionStorage.jobSetupOutputCompare != undefined) {
                var goutput_compare_data = jQuery.parseJSON(sessionStorage.jobSetupOutputCompare);
                goutput_compare_data.checkOutAction = self.gOutputData.checkOutAction;
                sessionStorage.jobSetupOutputCompare = JSON.stringify(goutput_compare_data);
            }
        }
        delete self.gOutputData.checkoutAction; //TODO: to be discussed with Jim. We are getting this attribute in templateOnchange web service

        //if (self.gOutputData.modulesAction != undefined && self.gOutputData.modulesAction != null && self.gOutputData.modulesAction != "" && self.gOutputData.modulesAction.modulesDict != null && self.gOutputData.modulesAction.modulesDict != null && self.gOutputData.modulesAction.modulesDict != "") {
        //    //getCustNavLinks();
        //    //createNavLinks();
        //    displayNavLinks();
        //}
        //else {
        //    displayNavLinks();
        //}
        if (self.gOutputData != null) {
            getBubbleCounts(self.gOutputData);
        }
        self.displayOrderSummary();
    };

    //to get Customer email JSON
    self.makeEmails = function () {
        $.getJSON(customerEmailsURL, function (data) {
            self.customerDetailsData = data;
        });
    };

    self.getApproversList = function () {
        var approvers_list_url = serviceURLDomain + "api/Tagging_approvers/" + sessionStorage.facilityId + "/" + jobCustomerNumber + "/" + self.jobNumber + "/null";
        getCORS(approvers_list_url, null, function (response_data) {
            self.approversList = (response_data != undefined && response_data != null && response_data != "" && response_data.Approvers != undefined && response_data.Approvers != null && response_data.Approvers != "" && Object.keys(response_data.Approvers).length > 0) ? response_data.Approvers : [];
            if (self.approversList.length == 0) {
                self.approversList = { "artApprover": "", "listViewer": "", "artViewer": "", "listApprover": "" };
            }
            if (self.approversList["artViewer"] == undefined) self.approversList["artViewer"] = "";
            if (self.approversList["listViewer"] == undefined) self.approversList["listViewer"] = "";
            if (self.approversList["artApprover"] == undefined) self.approversList["artApprover"] = "";
            if (self.approversList["listApprover"] == undefined) self.approversList["listApprover"] = "";
        }, function (error_response) {
            showErrorResponseText(error_response, true);
        });
    };

    self.showExecuteConfirmationToAdmin = function () {
        var msg_box = '';
        msg_box = '<div data-role="popup" data-position-to="window" data-transition="pop"  id="confirmAdmin" data-overlay-theme="d" data-history="false" data-theme="c" data-dismissible="false" style="max-width:520px;" class="ui-corner-all">' +
                    '<div data-role="header" data-theme="d" class="ui-corner-top" id="dialogHeader">' +
                    '	<h1>Warning!</h1>' +
                    '</div>' +
                    '<div data-role="content" data-theme="d" class="ui-corner-bottom ui-content">' +
                    '<div id="dvMsg"></div>' +
                    '	<div align="right"><a href="#" data-role="button" data-inline="true" data-mini="true" id="btnCancelPopup"  data-theme="a"  onclick="$(\'#confirmAdmin\').popup(\'close\') ">Cancel</a>' +
                    '	<a href="#" data-role="button" data-inline="true" data-mini="true"  id="btnContinuePopup" data-theme="a" data-transition="flow" onclick="ko.contextFor(this).$data.fnJobFinal(\'submit\');" >Submit Order</a></div>' +
                    '</div>' +
                '</div>';
        $("#_jobSubmitOrder").append(msg_box);
    };
};