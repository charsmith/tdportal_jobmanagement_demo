﻿jQuery.support.cors = true;

//******************** Global Variables Start **************************
var gData;
var gDataAll;
var searchData;
var selectedStoreId;
//var serviceURL = '../listSelection/JSON/_jobListSelection.JSON';
//var serviceURL = myProtocol + 'maps.tribunedirect.com/api/ZipSelectList/';
var serviceURL = mapsUrl + 'api/ZipSelectList/';
var gOutputServiceUrl = '../JSON/_jobSetupOutput.JSON';
var gOutputData;
var gDataStartIndex = 0;
var gInnerData = [];
var gSelectedZips = [];
var jobNumber = "";
var facilityId = "";
var totalAvailable = 0;
var jsonStoreLocations = "";
var selectedCRRTS = [];
var gDataStartIndex = 0;
var gPageSize = 7;
var pagePrefs;
jobNumber = getSessionData("jobNumber");
facilityId = getSessionData("facilityId");
caseysStaicJobNumber = sessionStorage.jobNumber;
//******************** Global Variables End **************************

//******************** Page Load Events Start **************************
$(window).on('popupbeforeposition', 'div:jqmData(role="popup")', function () {
    var notDismissible = $(this).jqmData('dismissible') === false;
    if (notDismissible) {
        $('.ui-popup-screen').off();
    }
});

$('#_jobListSelection').live('pagebeforecreate', function (event) {

    displayMessage('_jobListSelection');
    saveConfirmMessage('_jobListSelection');
    showStoreChangeMessage('_jobListSelection');
    $('#btnContinue').attr('onclick', 'continueToOrder()');
    //displayNavLinks();
    //test for mobility...
    loadMobility();
    //var over = '<div id="overlay" style="z-index:100;"><img id="loading" alt="Loading...." src="../images/kaleidoscope_wait.gif"></img><span id="loadingText">Loading Map....</span></div>';
    //$(over).appendTo('body');
    loadingImg("_jobListSelection");
});
$(document).bind('pageshow', '#_jobListSelection', function (event) {    
    //$('#overlay').css('display', 'none');
    if (sessionStorage.ListSelectionPrefs == undefined || sessionStorage.ListSelectionPrefs == null || sessionStorage.ListSelectionPrefs == "")
        getPagePreferences('ListSelection.html', sessionStorage.facilityId, appPrivileges.customerNumber);
    else
        managePagePrefs(jQuery.parseJSON(sessionStorage.ListSelectionPrefs));
    pagePrefs = jQuery.parseJSON(sessionStorage.ListSelectionPrefs);

    if (!fnVerifyScriptBlockDict(pagePrefs))
        return false;

    if (pagePrefs != null) {
        displayListFileInfo();
        //$('#ddlStoreLocations').change(function () {

        //});
        //eval(getURLDecode(pagePrefs.scriptBlockDict.pageshow));
    }
    if (appPrivileges.customerNumber == CW) {
        $('#btnRevert').css('display', "none");
        $('#btnSaveSelections').css('display', "none");
        $('#btnRefresh').css('display', "none");
    }
    else {
        $('#btnRevert').addClass('ui-disabled');
        $('#btnSaveSelections').addClass('ui-disabled');
        $('#btnRefresh').addClass('ui-disabled');
        //click events.
        $('#btnRefresh').click(function () {
            //validateTargetQty();
            calculateTargetedCnt();
        });
        $('#btnSaveSelections').click(function () {
            saveSelections();
        });
    }
    $('#loadingText1').text("Loading Map.....")
});


//******************** Public Functions Start **************************

function makeGData() {
    var zip_select_url = serviceURL + appPrivileges.customerNumber + "/1/" + ((appPrivileges.customerNumber == CW) ? jobNumber : caseysStaicJobNumber + '/' + selectedStoreId);
    getCORS(zip_select_url, null, function (data) {
        gDataAll = data;
        gData = data;
        sessionStorage.storeListSelection = JSON.stringify(gData);
    }, function (error_response) {
        showErrorResponseText(error_response);
    });
}
function displayListFileInfo() {
    if (sessionStorage.jobSetupOutput != undefined) {
        gOutputData = jQuery.parseJSON(sessionStorage.jobSetupOutput);
        loadStoreLocations();
    }
}
//load the store locations that are selected in the "select location" job page.
function loadStoreLocations() {
    var loaded_stores_data = [];
    if (gOutputData != undefined && gOutputData.selectedLocationsAction != undefined && gOutputData.selectedLocationsAction.selectedLocationsList != undefined && gOutputData.selectedLocationsAction.selectedLocationsList.length > 0) {
        loaded_stores_data = gOutputData.selectedLocationsAction.selectedLocationsList;
        loadStores(loaded_stores_data);
    }
    else {
        getCORS(serviceURLDomain + "api/StoreStatus/" + appPrivileges.customerNumber + "/1/" + ((appPrivileges.customerNumber == CW) ? jobNumber : caseysStaicJobNumber) + "/0/all", null, function (data) {
            if (data["opted-in"] != undefined && data["opted-in"] != null) {
                if (gOutputData != null && gOutputData.selectedLocationsAction != null && gOutputData.selectedLocationsAction != undefined && gOutputData.selectedLocationsAction.selectedLocationsList != undefined && gOutputData.selectedLocationsAction.selectedLocationsList != null) {
                    gOutputData.selectedLocationsAction.selectedLocationsList = data["opted-in"];
                    gOutputData.selectedLocationsAction.selectedLocationsList = gOutputData.selectedLocationsAction.selectedLocationsList.sort(function (a, b) {
                        return parseInt(a.storeId) > parseInt(b.storeId) ? 1 : -1;
                    });
                    sessionStorage.jobSetupOutput = JSON.stringify(gOutputData);
                    loadStores(gOutputData.selectedLocationsAction.selectedLocationsList);
                }
            }
        }, function (error_response) {
            showErrorResponseText(error_response);
        });
    }
}
function loadStores(loaded_stores_data) {
    var stores_data = '<select name="ddlStoreLocations" id="ddlStoreLocations" data-theme="b" data-mini="true" onchange="selectedStoreChanged();">';
    stores_data += '<option value="">Select Store</option>';
    var store_display_name = "";
    $.each(loaded_stores_data, function (key, val) {
        store_display_name = (val.pendingApproval != undefined && val.pendingApproval != null && val.pendingApproval == 1) ? val.storeId + ' - ' + val.city + ', ' + val.state + ' *' : val.storeId + ' - ' + val.city + ', ' + val.state;
        stores_data += '<option value="' + val.storeId + '">' + store_display_name + '</option>';
    });
    stores_data += '</select>';
    stores_data += '<br />* <span style="font-size:12px">Pending Approval</span>';
    $('#dvSelectedLocations').empty();
    $('#dvSelectedLocations').html(stores_data);
    $('#dvSelectedLocations select').each(function (i) {
        $(this).selectmenu();
    });
}

function selectedStoreChanged() {
    //var selected_crrts = $('div input:checked:not("id^=checkAll"):not("id^=chkEntireZip")');
    var selected_crrts = $('div input[type="checkbox"]:not([id^=checkAll]):not([id^=chkEntireZip]):checked');
    var diff = [];
    if ((selectedCRRTS.length > 0 && selected_crrts.length > 0) || (selectedCRRTS.length > 0 && selected_crrts.length == 0) || (selectedCRRTS.length == 0 && selected_crrts.length == 0))
        //diff = getDifferences(selectedCRRTS, selected_crrts);
        diff = getDifferences(selected_crrts, selectedCRRTS);
    else if (selectedCRRTS.length == 0 && selected_crrts.length > 0)
        //diff = getDifferences(selected_crrts, selectedCRRTS);
        diff = getDifferences(selectedCRRTS, selected_crrts);

    if (diff.length == undefined || diff.length == 0) {
        if (sessionStorage.storeListSelection != undefined) {
            var selected_text = $('#hdnSelectedStore').val();
            if (selected_text != "" && selectedCRRTS.length > 0) {
                var message = "The Targeted Selections have been changed for Store '" + selected_text + "' " +
                              "To select another location you must either Save Selections or Revert to the previously saved selections.";
                $('#storeMsg').text(message);
                $('#popupStoreDialog').popup('open');
            }
            else {
                $('#waitPopUp').popup('open', { positionTo: 'window' });
                getSelectedStoreData();
                selectedCRRTS = [];
            }
        }
        else {
            $('#waitPopUp').popup('open', { positionTo: 'window' });
            getSelectedStoreData();
            selectedCRRTS = [];
        }
        $('#hdnSelectedStore').val($('#ddlStoreLocations option:selected').text());
    }
    else {

        if (selected_crrts.length > 0 || selectedCRRTS.length > 0) {
            var selected_text = $('#hdnSelectedStore').val();
            var msg = "The Targeted Selections have been changed for Store '" + selected_text + "'. " +
                      "To select another location you must refresh the map. Click 'Ok' to Refresh Map";
            $('#confirmMsg').text(msg);
            $('#popupConfirmDialog').popup('open');
        }
    }
    $('#btnRefresh').removeClass('ui-disabled');
    makeReadOnlyZips();
}


function getSelectedStoreData() {
    selectedStoreId = $('#ddlStoreLocations').attr('value');
    //selectedStoreId = "2218"; // TO BE Removed and used above commented line when the service url becomes dynamic.
    makeGData();
    if (gData == undefined || gData == null || gData == "" || gData.zips == undefined || gData.zips == null || gData.zips == "" || gData.zips.length == 0) {
        $('#map').empty();
        $('#map').hide();
        $('#dvLocations').empty();
        $('#divAvailable').empty();
        $('#txtTargeted').empty();
        return;
    }
    {
        $('#map').show();
    }


    eval(getURLDecode(pagePrefs.scriptBlockDict.getSelectedStoreData));

    //if (appPrivileges.customerNumber == CW) {
    //    makePageSelect(false)
    //    $('#pageSelect').trigger('onchange');
    //}
    //else
    //    buildZips(selectedStoreId);
    //getDataFromSession();
    ////$('#waitPopUp').popup('open', { positionTo: 'window' });
    ////$('#overlay').css('display', 'block');
    //calculateTargetedCnt();
    ////$('#overlay').css('display', 'none');
    //$('#waitPopUp').popup('close');
}

function getDataFromSession() {
    if (sessionStorage.jobSetupOutput != undefined && sessionStorage.jobSetupOutput != null) {
        var job_info = jQuery.parseJSON(sessionStorage.jobSetupOutput);
        if (job_info.listSelection != undefined && job_info.listSelection != null) {
            var store_info = jQuery.grep(job_info.listSelection, function (obj) {
                return obj.storeId === selectedStoreId;
            });
            if (store_info.length > 0) {
                gSelectedZips = store_info[0];
                buildSelectedZips();
                //calculateTargetedCnt();
            }
        }
    }
}

//Prevents the collapsible set item expansion when clicked on checkbox.
function chkClick(e) {
    e.stopPropagation();
}

function chkSingleCRRT() {
    var selected_crrts = $('div input[type="checkbox"]:not([id^=checkAll]):not([id^=chkEntireZip]):checked');
    selectedCRRTS = $.extend(true, {}, selected_crrts);
}

function checkEntireZip(selected_zip, ctrl) {

    if (ctrl.checked) {
        $('#checkAll' + selected_zip)[0].checked = true;
        $('#checkAll' + selected_zip).attr("checked", true).checkboxradio("refresh");
        $('#hdnSelectedZip').val(selected_zip)
        $('#checkAll' + selected_zip).trigger('onchange');
        $('div.ui-collapsible-content', "#" + $('#hdnSelectedZip').val()).trigger('collapse');
        //selectAllZips(true, $('#checkAll' + selected_zip), 0);
        $('#chkEntireZip' + selected_zip).attr("checked", true).checkboxradio("refresh");
    }
    else {
        $('#checkAll' + selected_zip)[0].checked = false;
        $('#checkAll' + selected_zip).attr("checked", false).checkboxradio("refresh");
        $('#hdnSelectedZip').val(selected_zip)
        $('#checkAll' + selected_zip).trigger('onchange');
        $('div.ui-collapsible-content', "#" + $('#hdnSelectedZip').val()).trigger('collapse');
        //selectAllZips(false, $('#checkAll' + selected_zip), 0);
        $('#chkEntireZip' + selected_zip).attr("checked", false).checkboxradio("refresh");
    }
}

if (typeof String.prototype.startsWith != 'function') {
    // see below for better implementation!
    String.prototype.startsWith = function (str) {
        return this.indexOf(str) == 0;
    };
}

//to display the "All zips" for the selected store location.
function buildZips(selectedStoreId) {
    var li_zips = "";
    var li_all_routes = "";
    var li_chk_routes = "";
    var index = 0;
    var allzip = 0;
    var i = 0;
    var is_all_checkbox_selected = true;
    totalAvailable = 0;
    var target_selected = 0;
    if (selectedStoreId != undefined) {
        li_zips = '<div data-role="collapsible-set"  data-inset="false" data-theme="b" style="padding-left: 10px" data-collapsed-icon="arrow-r" data-expanded-icon="arrow-d">';
        $('#dvLocations').empty();
        //        gInnerData = jQuery.grep(gData, function (obj) {
        //            return obj.storeId === selectedStoreId;
        //        });
        if (gData != null) {
            gInnerData = gData;
            $.each(gInnerData.zips, function (zip_key, zip_val) {
                if (gInnerData.zips[index] != undefined) {
                    var city_name = zip_val.zipName;
                    var zipCode = zip_val.zipCode;
                    if (zip_val.available != null && zip_val.available > 0) {
                        var avail = zip_val.available;
                        totalAvailable = eval(parseInt(totalAvailable) + parseInt(avail));
                    }
                    if (zip_val.alsoInStore == null)
                        li_zips = '<div data-theme="b" data-role="collapsible" data-collapsed-icon="arrow-r" data-expanded-icon="arrow-d" onclick="getZipInfo(' + zipCode + ');" id="' + zipCode + '"><h3 style="width:105%">' + zipCode + ' - ' + city_name + '';
                    else
                        li_zips = '<div data-theme="e" data-role="collapsible" data-collapsed-icon="arrow-r" data-expanded-icon="arrow-d" onclick="getZipInfo(' + zipCode + ');" id="' + zipCode + '"><h3 style="width:105%">' + zipCode + ' - ' + city_name + '';
                    li_all_routes = '<div data-role="fieldcontain" data-inset="false"><fieldset id="fieldset" data-role="controlgroup" data-mini="true" data-inset="false">';
                    var selected_routes_count = 0;
                    var len = 0;
                    var is_Crrt_Starts_With_B = false;
                    if (zip_val.crrts != null || zip_val.crrts.length > 0) {
                        li_chk_routes = "";
                        i = 0;
                        len = zip_val.crrts.length;
                        var zip_targeted_cnt = 0;
                        $.each(zip_val.crrts, function (key, val) {
                            allzip += eval(parseInt(val.available));

                            var is_checked = '';
                            if (val.isChecked == 1) {
                                selected_routes_count++;
                                zip_targeted_cnt += parseInt(val.available);
                                target_selected += parseInt(val.available);
                                is_checked = ' checked="checked" ';
                            } else {
                                is_checked = '';
                            }
                            if (is_checked == '')
                                is_all_checkbox_selected = false;
                            if (!is_Crrt_Starts_With_B)
                                is_Crrt_Starts_With_B = (val.crrtName.toUpperCase().startsWith('B'));
                            li_chk_routes += '<input type="checkbox" id="check_' + zipCode + '_' + i + '" onmouseup="chkSingleCRRT();" onchange="selectAllZips(false,this,' + val.available + ', \'' + zip_val.alsoInStore + '\')" value="' + val.available + '" "' + is_checked + '"/>';
                            li_chk_routes += '<label for="check_' + zipCode + '_' + i + '" onmouseup="chkSingleCRRT();">' + val.crrtName + '<span style="font-weight:normal;font-size:12px;color:#369"> | Avlb: ' + val.available + '</span></label>';
                            i = i + 1;
                        });
                    }
                    if (selected_routes_count == len)
                        li_all_routes += '<input type="checkbox" name="checkAll" onchange="selectAllZips(true,this,0, \'' + zip_val.alsoInStore + '\')" id="checkAll' + zipCode + '" checked/>';
                    else
                        li_all_routes += '<input type="checkbox" name="checkAll" onchange="selectAllZips(true,this,0, \'' + zip_val.alsoInStore + '\')" id="checkAll' + zipCode + '"/>';
                    li_all_routes += '<label for="checkAll' + zipCode + '">Select Entire Zip<span style="font-weight:normal;font-size:12px;color:#369;"> | Avlb: ' + zip_val.available + '</span></label>';
                    $('#divAvailable').html(zip_val.available);

                    if (is_Crrt_Starts_With_B && appPrivileges.customerNumber===CASEYS) {
                        li_chk_routes += '<div style="font-size:12px;padding-top:8px">Carrier Routes starting with "B" are P.O. Boxes and will not display on the map.</div>';
                        is_Crrt_Starts_With_B = false;
                    }

                    li_all_routes += li_chk_routes + '</fieldset></div>';
                    if (selected_routes_count == len) {
                        if (zip_val.alsoInStore == null)
                            li_zips += '<div data-role="fieldcontain" style="width:40px;" data-inset="true" data-mini="true"  class="ui-li-aside" ><fieldset data-role="controlgroup" data-inset="false" data-mini="true" style="width:40px"><legend></legend><input class="my-list" type="checkbox" name="chkEntireZip' + zipCode + '" id="chkEntireZip' + zipCode + '" class="custom" onchange="checkEntireZip(\'' + zipCode + '\',this)" onclick="chkClick(event);" data-theme="b" data-mini="true" checked/><label for="chkEntireZip' + zipCode + '" onclick="chkClick(event);" style="width:40px">&nbsp;</label></fieldset></div>';
                        else
                            li_zips += '<div data-theme="e" data-role="fieldcontain" style="width:40px;" data-inset="true" data-mini="true"  class="ui-li-aside" ><fieldset data-role="controlgroup" data-inset="false" data-mini="true" style="width:40px" data-theme="e"><legend></legend><input class="my-list" type="checkbox" name="chkEntireZip' + zipCode + '" id="chkEntireZip' + zipCode + '" class="custom" onchange="checkEntireZip(\'' + zipCode + '\',this)" onclick="chkClick(event);" data-theme="e" data-mini="true" checked/><label data-theme="e" for="chkEntireZip' + zipCode + '" onclick="chkClick(event);" style="width:40px">&nbsp;</label></fieldset></div>';
                    }
                    else {
                        if (zip_val.alsoInStore == null)
                            li_zips += '<div data-role="fieldcontain" style="width:40px;" data-inset="true" data-mini="true"  class="ui-li-aside"><fieldset  data-role="controlgroup" data-inset="false" data-mini="true" style="width:40px"><legend></legend><input class="my-list" type="checkbox" name="chkEntireZip' + zipCode + '" id="chkEntireZip' + zipCode + '" class="custom" onchange="checkEntireZip(\'' + zipCode + '\',this)" onclick="chkClick(event);" data-theme="b" data-mini="true"/><label for="chkEntireZip' + zipCode + '" onclick="chkClick(event);" style="width:40px">&nbsp;</label></fieldset></div>';
                        else
                            li_zips += '<div data-theme="e" data-role="fieldcontain" style="width:40px;" data-inset="true" data-mini="true"  class="ui-li-aside"><fieldset data-theme="e" data-role="controlgroup" data-inset="false" data-mini="true" style="width:40px"><legend></legend><input class="my-list" type="checkbox" name="chkEntireZip' + zipCode + '" id="chkEntireZip' + zipCode + '" class="custom" onchange="checkEntireZip(\'' + zipCode + '\',this)" onclick="chkClick(event);" data-theme="e" data-mini="true"/><label for="chkEntireZip' + zipCode + '" onclick="chkClick(event);" style="width:40px" data-theme="e">&nbsp;</label></fieldset></div>';
                    }

                    li_zips += '<br/><span style="font-weight:normal;font-size:12px;">Available: ' + zip_val.available + ' | Targeted: <span style="font-weight:normal;font-size:12px;" id="spnTargetedZipCnt' + zipCode + '">' + zip_targeted_cnt + '</span></span><br/>';
                    if (selected_routes_count == len) {
                        if (zip_val.alsoInStore == null)
                            li_zips += '<span id="spn' + zipCode + '" style="font-weight:normal;font-size:12px;">Entire Zip Selected</span></h3>';
                        else
                            li_zips += '<span id="spn' + zipCode + '" style="font-weight:normal;font-size:12px;">Entire Zip Selected <br />Also in Store: ' + zip_val.alsoInStore + '</span></h3>';
                    }
                    else {
                        if (zip_val.alsoInStore == null)
                            li_zips += '<span id="spn' + zipCode + '" style="font-weight:normal;font-size:12px;">' + selected_routes_count + ' of ' + len + ' Carrier Routes Selected</span></h3>';
                        else
                            li_zips += '<span id="spn' + zipCode + '" style="font-weight:normal;font-size:12px;">' + selected_routes_count + ' of ' + len + ' Carrier Routes Selected <br />Also in Store: ' + zip_val.alsoInStore + '</span></h3>';
                    }
                    li_zips += li_all_routes;
                    li_zips += '</div>';
                    if (is_all_checkbox_selected)
                        $('#checkAll').attr("checked", true);
                    index++;
                    $('#dvLocations').append(li_zips);
                }
            });
            $("#dvLocations").trigger("create");

            $($('div[class=ui-checkbox]').find('label[for^=chkEntireZip]')).each(function (i) {
                $(this).find('span').css('padding-left', '1px');
            });
        }
        $('#divAvailable').html(totalAvailable);
        $('#txtTargeted').html(target_selected);
    }
    else {
        $('#hdnSelectedStore').val('');
    }
}

function getZipInfo(zip_code) {
    $('#hdnSelectedZip').val(zip_code);
}

function getDifferences(oldObj, newObj) {
    var diff = {};

    for (var k in oldObj) {
        if (!(k in newObj))
            diff[k] = undefined;  // property gone so explicitly set it undefined
        else if (oldObj[k] !== newObj[k])
            //else if (!isEqual(oldObj[k], newObj[k]))
            diff[k] = newObj[k];  // property in both but has changed
    }

    for (k in newObj) {
        if (!(k in oldObj))
            diff[k] = newObj[k]; // property is new
    }

    return diff;
}

//when clicked on "Save Selections" button..
function saveSelections() {
    var selected_crrts = $('div input[type="checkbox"]:not([id^=checkAll]):not([id^=chkEntireZip]):checked');
    var diff = [];
    if ((selectedCRRTS.length > 0 && selected_crrts.length > 0) || (selectedCRRTS.length > 0 && selected_crrts.length == 0) || (selectedCRRTS.length == 0 && selected_crrts.length == 0))
        diff = getDifferences(selected_crrts, selectedCRRTS);
    else if (selectedCRRTS.length == 0 && selected_crrts.length > 0)
        diff = getDifferences(selectedCRRTS, selected_crrts);

    if (diff.length == undefined || diff.length == 0) {
        //Get the complete jon info.
        if (sessionStorage.jobSetupOutput != undefined) {
            gOutputData = jQuery.parseJSON(sessionStorage.jobSetupOutput);
        }

        //get the selected zips info for the selected store.
        if (sessionStorage.storeListSelection != undefined && sessionStorage.storeListSelection != null) {
            gSelectedZips = jQuery.parseJSON(sessionStorage.storeListSelection);
        }

        if (gSelectedZips != undefined && gSelectedZips != null) {
            if (gOutputData.listSelection != undefined && gOutputData.listSelection != "") {
                var zip_index = 0;
                var selected_zip = jQuery.grep(gOutputData.listSelection, function (key, obj) {
                    zip_index = obj;
                    return key.storeId === selectedStoreId;
                });
                if (selected_zip != undefined && selected_zip.length == 0) {
                    selected_zip = gSelectedZips;
                    gOutputData.listSelection.push(gSelectedZips);
                }
                else {
                    selected_zip = gSelectedZips;
                    gOutputData.listSelection[zip_index] = selected_zip;
                }
            } else {
                gOutputData["listSelection"] = [];
                gOutputData.listSelection.push(gSelectedZips);
            }
        }
        sessionStorage.jobSetupOutput = JSON.stringify(gOutputData);
        sessionStorage.removeItem('storeListSelection');
        $('#hdnSelectedStore').val('');
        var post_json_zips = (selected_zip != undefined) ? selected_zip : gSelectedZips;
        //postCORS(myProtocol + "maps.tribunedirect.com/api/ZipSelectList/1/" + (appPrivileges.customerNumber == CW) ? jobNumber : caseysStaicJobNumber + "/" + selectedStoreId + "/store_select", JSON.stringify(post_json_zips), function (response) {
        //postCORS(myProtocol + "maps.tribunedirect.com/api/ZipSelectList/" + appPrivileges.customerNumber + "/1/" + ((appPrivileges.customerNumber == CW) ? jobNumber : caseysStaicJobNumber + "/" + selectedStoreId), JSON.stringify(post_json_zips), function (response) {
        postCORS(mapsUrl + "api/ZipSelectList_save/" + appPrivileges.customerNumber + "/1/" + ((appPrivileges.customerNumber == CW) ? jobNumber : caseysStaicJobNumber + "/" + selectedStoreId), JSON.stringify(post_json_zips), function (response) {
            var msg = (response.toLowerCase() == "\"success\"") ? 'Your selections have been saved.' : 'Your selections have not been saved.';
            $('#alertmsg').text(msg);
            $('#popupDialog').popup('open');
        }, function (response_error) {
            //var msg = 'Your selections have not been saved.';
            var msg = response_error.responseText;
            $('#alertmsg').text(msg);
            $('#popupDialog').popup('open');
        });
        $('#btnRevert').addClass('ui-disabled');
        selectedCRRTS = [];
    }
    else {
        var msg = "The Targeted Selections have been changed for Store '" + $('#hdnSelectedStore').val() + "'. " +
                  "To save selections you must refresh the map. Click 'Ok' to Refresh Map.";
        $('#confirmMsg').text(msg);
        $('#popupConfirmDialog').popup('open');
    }
}

//when clicked on "Refresh Map" button to validate the targeted quantity. If the entered quantity is greater than total targeted quanity, show message.
function validateTargetQty() {
    //    var entered_targeted_qty = parseInt($("#txtTargeted").val());
    //    alert("entered_targeted_qty:" + entered_targeted_qty);
    //    var total_targeted = parseInt(getTargetedCounts());
    //    alert("total_targeted:" + total_targeted);
    //    $("#txtTargeted").val(total_targeted);
    //    var radius = parseInt($("#txtRadius").val());
    //    var error_text = "The targeted quantity of " + entered_targeted_qty + " cannot be found within a " + radius + " mile radius of this location. ";
    //    error_text += "Either increase the radius selection or decrease the targeted quantity.";
    //    if (entered_targeted_qty > total_targeted) {
    //        $('#alertmsg').text(error_text);
    //        $('#popupDialog').popup('open');
    //        $("#txtTargeted").val(entered_targeted_qty);
    //        return false;
    //    }
    //    else {
    //        refreshMap();
    //    }
    var targeted_qty = parseInt($("#txtTargeted").html());
    if (targeted_qty <= 0) {
        $('#alertmsg').text("Please select at least One Zip route.");
        $('#popupDialog').popup('open');
        $("#txtTargeted").val(targeted_qty);
        return false;
    }
    else {
        return true;
    }
}


//when user checks "Select Entire Zip" / "Individual Zip codes"..
function selectAllZips(is_check_all, ctrl, avlb, also_in_store) {
    //var selected_crrts = $('div input:checked:not("id^=checkAll"):not("id^=chkEntireZip")');
    var selected_crrts = $('div input[type="checkbox"]:not([id^=checkAll]):not([id^=chkEntireZip]):checked');
    if (is_check_all)
        selectedCRRTS = $.extend(true, {}, selected_crrts);

    var selected_zip = $('#hdnSelectedZip').val();
    var is_select_all = $('#checkAll' + selected_zip)[0].checked;
    var targeted_value = 0;
    $('#hdnSelectedStore').val($('#ddlStoreLocations option:selected').text());
    //    var store_info = jQuery.grep(gData, function (obj) {
    //        return obj.storeId === selectedStoreId;
    //    });
    store_info = gData;
    var zip_routes = jQuery.grep(store_info.zips, function (key, obj) {
        return key.zipCode === parseInt(selected_zip);
    });
    if (!is_select_all) {
        targeted_value = zip_routes[0].targeted;
    }

    //getting all crrt checkboxes in that selected zip.
    var selected_zip_crrts = $('#' + selected_zip + ' input:checkbox:not("#checkAll' + selected_zip + '"):not("#chkEntireZip' + selected_zip + '")');
    if (is_check_all) {
        $.each(selected_zip_crrts, function (i, j) {
            if (is_select_all)
                $(this).attr('checked', true).checkboxradio('refresh');
            else
                $(this).attr('checked', false).checkboxradio('refresh');
        });
        if (is_select_all)
            $('#chkEntireZip' + selected_zip).attr('checked', true).checkboxradio('refresh');
        else
            //
            $('#chkEntireZip' + selected_zip).attr('checked', false).checkboxradio('refresh');
    }

    //getting previous selections in the selected zip.
    var selected_crrts = $('#' + selected_zip + ' input:checkbox:not("#checkAll' + selected_zip + '):not("#chkEntireZip' + selected_zip + '")');
    var prev_targetted_value = 0;
    var index = 0;
    $.each(selected_crrts, function (a, b) {
        if ($(this)[0].checked) {
            prev_targetted_value += parseInt(b.value);
            index++;
        }
    });
    var the_spn = '';
    if (index < selected_crrts.length) {
        $('#chkEntireZip' + selected_zip).attr('checked', false).checkboxradio('refresh');
        $('#checkAll' + selected_zip).attr('checked', false).checkboxradio('refresh');
        the_spn = index + ' of ' + selected_crrts.length + ' Carrier Routes Selected';
    }
    else {
        $('#chkEntireZip' + selected_zip).attr('checked', true).checkboxradio('refresh');
        $('#checkAll' + selected_zip).attr('checked', true).checkboxradio('refresh');
        the_spn = 'Entire Zip Selected';
    }
    if (also_in_store != 'null' && also_in_store != null) {
        the_spn += '<br/>Also in Store: ' + also_in_store;
    }
    $('#spn' + selected_zip).html(the_spn);

    $('#spnTargetedZipCnt' + selected_zip).html(prev_targetted_value);
    $('div.ui-collapsible-content', "#" + $('#hdnSelectedZip').val()).trigger('expand');

    //var selected_crrts = $('#' + selected_zip + ' input:checkbox:not("#checkAll' + selected_zip + '):not("#chkEntireZip' + selected_zip + '")');
    var selected_crrts = $('div input[type="checkbox"]:not([id^=checkAll]):not([id^=chkEntireZip]):checked');
    var diff = [];
    if ((selectedCRRTS.length > 0 && selected_crrts.length > 0) || (selectedCRRTS.length > 0 && selected_crrts.length == 0) || (selectedCRRTS.length == 0 && selected_crrts.length == 0))
        //diff = getDifferences(selectedCRRTS, selected_crrts);
        diff = getDifferences(selected_crrts, selectedCRRTS);
    else if (selectedCRRTS.length == 0 && selected_crrts.length > 0)
        //diff = getDifferences(selected_crrts, selectedCRRTS);
        diff = getDifferences(selectedCRRTS, selected_crrts);
    if (diff.length != undefined && diff.length > 0)
        $('#btnSaveSelections').removeClass('ui-disabled');
    $('#btnRefresh').removeClass('ui-disabled');
}


//when user checks "Select Entire Zip" / "Individual Zip codes"..
function calculateTargetedCnt() {

    eval(getURLDecode(pagePrefs.scriptBlockDict.calculateTargetedCnt));
    //if (appPrivileges.customerNumber == CW) {
    //    var selected_crrts = $('div input[type="checkbox"]:not([id^=checkAll]):[id^=chkEntireZip]');
    //    $.each(selected_crrts, function (key1, val1) {
    //        $(val1).attr('checked', true).checkboxradio('refresh');
    //    });
    //    var selected_crrts = $('div input[type="checkbox"]:not([id^=checkAll]):[id^=chkEntireZip]:checked');
    //    $.each(gDataAll.zips, function (key, val) {
    //        $('#spnTargetedZipCnt' + val.zipCode).html(val.available);
    //    });
    //    gSelectedZips = $.extend(true, {}, gDataAll);
    //    $.each(gSelectedZips.zips, function (key, val) {
    //        val.crrts = [];
    //    });
    //    sessionStorage.storeListSelection = JSON.stringify(gSelectedZips);
    //    refreshMap();
    // }
    //else {
    //    var selected_crrts = $('div input[type="checkbox"]:not([id^=checkAll]):not([id^=chkEntireZip]):checked');
    //    //var selected_crrts = $('div input:checked:not("id^=checkAll"):not("id^=chkEntireZip")');
    //    var diff = [];
    //    if ((selectedCRRTS.length > 0 && selected_crrts.length > 0) || (selectedCRRTS.length > 0 && selected_crrts.length == 0) || (selectedCRRTS.length == 0 && selected_crrts.length == 0))
    //        //diff = getDifferences(selectedCRRTS, selected_crrts);
    //        diff = getDifferences(selected_crrts, selectedCRRTS);
    //    else if (selectedCRRTS.length == 0 && selected_crrts.length > 0)
    //        //diff = getDifferences(selected_crrts, selectedCRRTS);
    //        diff = getDifferences(selectedCRRTS, selected_crrts);

    //    var targed_cnt = 0;
    //    $.each(selected_crrts, function (i, j) {
    //        if ($(this)[0].id.toLowerCase().indexOf('checkall') == -1 && $(this)[0].id.toLowerCase().indexOf('chkentirezip') == -1 && $(this)[0].checked) {
    //            targed_cnt = parseInt(targed_cnt) + parseInt($(this)[0].value);
    //        }
    //    });
    //    $('#txtTargeted').html(targed_cnt);
    //    //if (validateTargetQty()) {
    //    var temp_g_data = $.extend(true, {}, gData);
    //    var targeted_qty = 0;
    //    if (temp_g_data != null && temp_g_data.zips != undefined) {
    //        var zip_routes = jQuery.grep(temp_g_data.zips, function (key, obj) {
    //            var zip = key.zipCode;
    //            var is_selected = false;
    //            var index = 0;
    //            var selected_crrts_cnt = 0;
    //            $.each(key.crrts, function (i, j) {
    //                if ($('#check_' + zip + '_' + index)[0] != undefined && $('#check_' + zip + '_' + index)[0].checked) {
    //                    is_selected = true;
    //                    j.isChecked = 1;
    //                    selected_crrts_cnt = parseInt(j.available) + parseInt(selected_crrts_cnt);
    //                }
    //                else {
    //                    j.isChecked = 0;
    //                }
    //                index++;
    //            });
    //            if (is_selected) {
    //                key.targeted = selected_crrts_cnt;
    //                key.isChecked = 1;
    //                targeted_qty = parseInt(targeted_qty) + parseInt(selected_crrts_cnt);
    //                return key;
    //            }
    //        });
    //        temp_g_data.targetedQty = targeted_qty;
    //        temp_g_data.zips = zip_routes;
    //        gSelectedZips = temp_g_data;
    //        sessionStorage.storeListSelection = JSON.stringify(gSelectedZips);
    //        refreshMap();
    //        //selectedCRRTS = $('div input:checked:not("id^=checkAll"):not("id^=chkEntireZip")');
    //        selectedCRRTS = $('div input[type="checkbox"]:not([id^=checkAll]):not([id^=chkEntireZip]):checked');
    //        if (diff.length != undefined && diff.length > 0)
    //            $('#btnRevert').removeClass('ui-disabled');
    //    }
    //}
}
function buildSelectedZips() {
    //    var store_info = jQuery.grep(gSelectedZips, function (obj) {
    //        return obj.storeId === selectedStoreId;
    //    });
    $.each(gSelectedZips.zips, function (a, b) {
        var e = 0;
        var zip_code = b.zipCode;
        var index = 0;
        var selected_crrts_cnt = 0;
        $.each(b.crrts, function (c, d) {
            if (d.isChecked) {
                $('#check_' + zip_code + '_' + index).attr('checked', true).checkboxradio('refresh');
                selected_crrts_cnt++;
            }
            index++;
        });
        var the_spn = '';
        if (index == selected_crrts_cnt) {
            $('#checkAll' + zip_code).attr('checked', true).checkboxradio('refresh');
            $('#chkEntireZip' + zip_code).attr('checked', true).checkboxradio('refresh');
            the_spn = 'Entire Zip Selected';
        }
        else {
            the_spn = selected_crrts_cnt + ' of ' + index + ' Carrier Routes Selected';
        }
        //
        if (b.alsoInStore != 'null' && b.alsoInStore != null) {
            the_spn += '<br/>Also in Store: ' + b.alsoInStore;
        }
        $('#spn' + zip_code).html(the_spn);

    });
}
function refreshMap() {    
    makeGoogleMap();
    $("#dvMapMarker").css('display', 'block');
}

function showStoreChangeMessage(page_name) {
    var msg_box = '';
    msg_box = '<div data-role="popup" id="popupStoreDialog" data-overlay-theme="a" data-history="false" data-theme="c" data-dismissible="false" style="max-width:500px;" class="ui-corner-all">' +
					'<div data-role="header" data-theme="a" class="ui-corner-top">' +
					'	<h1>Confirm Changes?</h1>' +
					'</div>' +
					'<div data-role="content" data-theme="d" class="ui-corner-bottom ui-content">' +
					'<div id="storeMsg"></div>' +
					'	<a href="#" data-role="button" data-inline="true" data-mini="true" id="btnModelSave" data-theme="b" onclick="savaSelectedInfo();">Save Selections</a>' +
					'	<a href="#" data-role="button" data-inline="true" data-mini="true" id="btnModelRevert" data-transition="flow" data-theme="c" onclick="revertSelections();">Revert</a>' +
                    '	<a href="#" data-role="button" data-inline="true" data-mini="true" id="btnCancel" data-transition="flow" data-theme="c" onclick="cancelSelections()">Cancel</a>' +
					'</div>' +
				'</div>';
    $("#" + page_name).append(msg_box);
}

function savaSelectedInfo() {
    saveSelections();
    getSelectedStoreData();
    $('#popupStoreDialog').popup('close');
}

function revertSelections() {
    gSelectedZips = [];
    //$('#ddlStoreLocations').val(selectedStoreId).selectmenu('refresh');
    $('#popupStoreDialog').popup('close');
    $('#hdnSelectedStore').val('');
    //    var checked_items = $('input:checked');
    //    $.each(checked_items, function (a, b) {
    //        $(this).attr('checked', false).checkboxradio('refresh');
    //    });
    //getDataFromSession();
    selectedCRRTS = [];
    sessionStorage.removeItem('storeListSelection');
    //buildSelectedZips();
    //refreshMap();
    $('#ddlStoreLocations').trigger('onchange');
    $('#btnRevert').addClass('ui-disabled');
    $('#btnSaveSelections').addClass('ui-disabled');
}
function cancelSelections() {
    $('#ddlStoreLocations').val(selectedStoreId).selectmenu('refresh');
    $('#popupStoreDialog').popup('close');
}

function refreshCurrentSelections() {
    calculateTargetedCnt();
    $('#popupConfirmDialog').popup('close');
    selectedCRRTS = $('div input[type="checkbox"]:not([id^=checkAll]):not([id^=chkEntireZip]):checked');
    //selectedCRRTS = [];
    $('#ddlStoreLocations').val(selectedStoreId).selectmenu('refresh');
    //$('#ddlStoreLocations').trigger('onchange');
    //saveSelections();
}

function revertCurrentSelections() {
    $('#popupConfirmDialog').popup('close');
    $('#ddlStoreLocations').val(selectedStoreId).selectmenu('refresh');
}

function saveConfirmMessage(page_name) {
    var msg_box = '';
    msg_box = '<div data-role="popup" id="popupConfirmDialog" data-history="false" data-overlay-theme="a" data-theme="c" style="max-width:400px;" class="ui-corner-all">';
    msg_box += '<div data-role="header" data-theme="a" class="ui-corner-top" id="dialogbox">';
    msg_box += '<h1>Confirm</h1>';
    msg_box += '</div>';
    msg_box += '<div data-role="content" data-theme="d" class="ui-corner-bottom ui-content">';
    msg_box += '<div id="confirmMsg"></div>';
    msg_box += '<div id="colId"></div>';
    msg_box += '<div id="valList"></div>';
    msg_box += '<a href="#" data-role="button" data-inline="true" data-theme="b" id="cancelBut" onclick="revertCurrentSelections();">Cancel</a>';
    msg_box += '<a href="#" data-role="button" data-inline="true" data-theme="b"  id="okBut" onclick="refreshCurrentSelections();">Ok</a>';
    msg_box += '</div></div>';
    $("#" + page_name).append(msg_box);
}

function makePageSelect(has_search) {
    var zip_data = ((has_search) ? searchData : gDataAll);
    var page_size = Math.round(zip_data.zips.length / gPageSize);
    //    if (gDataAll.zips.length % gPageSize > 0) {
    //        page_size++;
    //    }
    var str = '';
    //var str = '<li>';
    //str += '<select name="pageSelect" id="pageSelect" data-theme="b" data-overlay-theme="d" data-native-menu="false" data-mini="true" onchange="updateStartIndex()">';
    for (var i = 0; i < page_size; i++) {
        str += '<option value="' + i + '">Page ' + (1 + i) + '</option>';
    }
    //str += '</select>';
    //str += '</li>';
    if ($('#search').length == 0) {
        var search_ctrl = '<br/><input type="search" width="20%" maxlength="25" name="search" id="search" value="" onkeyup="searchZips();" onchange="searchZips();"/>';
        $(search_ctrl).insertAfter('#dvPageNumbers');
    }
    if (page_size == 0)
        $('#dvPageNumbers').css('display', 'none');
    else {
        $('#pageSelect option').remove();
        $(str).appendTo('#pageSelect');
        $('#pageSelect').selectmenu('refresh');
        $('#dvPageNumbers').css('display', 'block');
    }
    $('.ui-page').trigger('create');
}

function updateStartIndex(has_search) {
    var multiplier = parseInt($('#pageSelect').prop("selectedIndex"));
    gDataStartIndex = multiplier * gPageSize;
    var upper_value = (gDataStartIndex + gPageSize <= gDataAll.zips.length) ? gDataStartIndex + gPageSize : gDataAll.zips.length;
    var temp_gdata = [];
    temp_gdata = $.extend(true, {}, ((has_search || (searchData != undefined && searchData != null && searchData.zips != undefined && searchData.zips.length > 0)) ? searchData : gDataAll));
    var zips_in_page = temp_gdata.zips.splice(gDataStartIndex, upper_value - gDataStartIndex);
    temp_gdata.zips = zips_in_page;
    gData = temp_gdata;
    $('#dvLocations').empty();
    buildZips(selectedStoreId);
    $.each(gData.zips, function (key, val) {
        $('#spnTargetedZipCnt' + val.zipCode).html(val.available);
    });
    var avbl_cnt = 0;
    $.each(gDataAll.zips, function (key, val) {
        avbl_cnt = parseInt(avbl_cnt) + parseInt(val.available);
    });
    $('#divAvailable').html(avbl_cnt);
    $('#txtTargeted').html($('#divAvailable').text());
    makeReadOnlyZips();
}

function makeReadOnlyZips() {

    eval(getURLDecode(pagePrefs.scriptBlockDict.makeReadOnlyZips));
    //if (appPrivileges.customerNumber == CW) {
    //    $($('div[class=ui-checkbox]')).each(function (i) {
    //        $(this).addClass('ui-disabled')
    //        $(this).css('opacity', '0.8');
    //    });
    //    $.each($('#dvLocations div'), function (key, val) {
    //        if (val.id != undefined && val.id != null && val.id != "") {
    //            $('#' + val.id + ' h3 a').click(function (event) {
    //                return false;
    //            });
    //            $('#' + val.id + ' h3 a').css('cursor', 'default');
    //            //$('#' + val.id + ' h3 a').removeClass('ui-btn-icon-left');
    //        }
    //    });
    //}
}

function searchZips() {
    var filterd_list = [];
    var search_value = $('#search').val();

    if (search_value != "" && search_value != undefined) {
        var searched_zips = jQuery.grep(gDataAll.zips, function (a, b) {
            return (a.zipCode.toString().indexOf(search_value) > -1 || a.zipName.toLowerCase().indexOf(search_value.toLowerCase()) > -1 || a.available.toString().indexOf(search_value) > -1 || (a.zipCode.toString() + ' - ' + a.zipName.toLowerCase()).indexOf(search_value.toLowerCase()) > -1);
        });
        searchData = $.extend(true, {}, gDataAll);
        searchData.zips = searched_zips;
        makePageSelect(true);
        updateStartIndex(true);
    }
    else {
        searchData = [];
        makePageSelect(false);
        updateStartIndex(false);
    }


}

//******************** Public Functions End **************************
