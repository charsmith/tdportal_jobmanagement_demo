﻿$.mobile.popup.prototype.options.history = false;
jQuery.support.cors = true;

//******************** Data URL Variables Start **************************
var pageObj;
var gVarArtworkInfoServiceUrl = "../jobBaseSelection/JSON/_jobBaseSelection.JSON";
var gServiceData;
var pageObj;
var companyData;
var primaryFacilityData;
var gOutputData;
//******************** Data URL Variables End **************************

//******************** Page Load Events Start **************************
$('#_jobBaseSelection').live('pagebeforecreate', function (event) {
    ko.setTemplateEngine(new ko.nativeTemplateEngine);
    displayMessage('_jobBaseSelection');
    createConfirmMessage("_jobBaseSelection");
    displayNavLinks();
    getBaseInfo();
    $("#navLinksPanel").on("panelopen", function (event, ui) {
        sessionStorage.isNavPanelOpen = true;
    });
    setDemoHintsSliderValue();
    //$('#sldrShowHints').val(sessionStorage.showDemoHints).slider('refresh');
});

$(document).on('pageshow', '#_jobBaseSelection', function (event) {
    window.setTimeout(function () {
        //if (pageObj == undefined || pageObj == null) {
        pageObj = new loadCompanyInfo();
        ko.applyBindings(pageObj);
        //}
        //window.setTimeout(function () {
        //$('input[type=radio]').trigger('create');//.checkboxradio();
        $('#dvNavBar').trigger('create');
        $('#dvNavBar div ul li a').first().addClass('ui-btn-active');
        //$('div').trigger('create');
        $('#ulBaseList li').removeClass('ui-body-j').addClass('ui-body-c');
        $(':radio:checked').parent().addClass('ui-body-j').removeClass('ui-body-c');
        $('#ulBaseList').listview();
        $('#ddlLocations').selectmenu('refresh');
        pageObj.getSelectedInfo();
        $('#chkReplicate').checkboxradio('refresh');
        //}, 500);
    }, 500);
    persistNavPanelState();
});

var locationVm = function (store_id, store_name, store_pk) {
    var self = this;
    self.locationId = store_pk;
    if (store_id != undefined && store_id != null && store_id != "")
        self.locationName = store_id + ' - ' + store_name;
    else
        self.locationName = store_name;
};

var getBaseInfo = function () {
    if (sessionStorage.jobSetupOutput != undefined && sessionStorage.jobSetupOutput != null && sessionStorage.jobSetupOutput != "") {
        gOutputData = $.parseJSON(sessionStorage.jobSetupOutput);
        $.getJSON(gVarArtworkInfoServiceUrl, function (data) {
            gServiceData = data;
            $.each(data, function (key, val) {
                var is_found = false;
                //if (key == gOutputData.templateName.substring(1, 4).replace('_', 'X')) {
                if (gOutputData.templateName.indexOf(key) > -1)
                    $.each(val, function (key1, val1) {
                        if (key1.toLowerCase() == "GW Berkheimer".toLowerCase()) {
                            gServiceData = val1;
                            is_found = true;
                            return false;
                        }
                    });
                if (is_found)
                    return false;
                //}
            });
        });
    }
};

var mapping = { 'ignore': ["__ko_mapping__"] }

var artworkInfo = function (artwork_type, artwork_name, base_image1, base_image2, is_checked) {
    var self = this;
    self.baseType = ko.observable();
    self.baseType(artwork_type);
    self.base = ko.observable('');
    self.base(artwork_name);
    self.baseImage1 = ko.observable();
    self.baseImage1(base_image1);
    self.baseImage2 = ko.observable();
    self.baseImage2(base_image2);
};

var artworkTypeInfo = function (type, selected_base, offers_list) {
    var self = this;
    self.selectedBase = ko.observable();
    self.selectedBase(selected_base);
    self.type = ko.observable();
    self.type(type);
    self.baseList = ko.observableArray([]);
    self.baseList(offers_list);
};

var selectedBaseListInfo = function (base_list, is_replicated) {
    var self = this;
    self.isReplicated = ko.observable('');
    self.isReplicated(is_replicated);
    $.each(base_list, function (key, val) {
        self[key] = ko.observable({});
        self[key](val);
    });
};

var selectedBaseInfo = function (base_info, is_enabled) {
    var self = this;
    self.baseInfo = ko.observableArray([]);
    self.baseInfo(base_info);
    self.isEnabled = ko.observable('');
    self.isEnabled(is_enabled);
    self.name = "baseSelection";
};

var loadCompanyInfo = function () {
    var self = this;
    //self.varArtworkInfo = ko.observableArray([]);
    self.varArtworkInfo = ko.observable({});
    self.varBaseListInfo = ko.observable({});
    self.locationsList = ko.observableArray([]);
    self.selectedLocation = ko.observable({});

    self.prevVarBaseListInfo = ko.observable({});
    self.selectedInHomeStoreBaseInfo = ko.observable({});
    self.selectedInhomeInfo = ko.observable({});
    self.inHomeDatesList = ko.observableArray([]);
    self.currentInHome = ko.observable();
    self.selectedStore = ko.observable('');
    self.isReplicated = ko.observable();
    self.previousSelectedStore = ko.observable({});

    //Loading all the available bases
    $.each(gServiceData, function (key, val) {
        temp_artwork_info = [];
        selected_base = "";
        $.each(val.baseList, function (key1, val1) {
            var optional_logo_path = logoPath;
            optional_logo_path += appPrivileges.companyName;
            var temp_name = gOutputData.templateName.substring(1);
            optional_logo_path += "/" + temp_name.substring(0, temp_name.indexOf('_'));
            optional_logo_path += "/" + "GWBerkheimer" + "/";
            temp_artwork_info.push(new artworkInfo(val1.baseType, val1.base, (optional_logo_path + val1.baseImage1), (optional_logo_path + val1.baseImage2), ''));
        });
        self.varArtworkInfo(new artworkTypeInfo("", selected_base, temp_artwork_info));
    });

    //Loading all the selected stores into drop down.
    if (sessionStorage.jobSetupOutput != undefined && sessionStorage.jobSetupOutput != null && sessionStorage.jobSetupOutput != "") {
        gOutputData = $.parseJSON(sessionStorage.jobSetupOutput);
        if (sessionStorage.jobSetupOutput != undefined && sessionStorage.jobSetupOutput != null && sessionStorage.jobSetupOutput != "") {
            gOutputData = $.parseJSON(sessionStorage.jobSetupOutput);
            if ((gOutputData.selectedLocationsAction != undefined && gOutputData.selectedLocationsAction != null && gOutputData.selectedLocationsAction != "") && (gOutputData.selectedLocationsAction.selectedLocationsList.length > 0)) {
                $.each(gOutputData.selectedLocationsAction.selectedLocationsList, function (key, val) {
                    if (key == 0) {
                        self.selectedLocation(val.pk);
                        self.selectedStore(val.pk);
                        self.previousSelectedStore(self.selectedStore());
                    }
                    self.locationsList.push(new locationVm(val.storeId, ((val.storeName) ? val.storeName : ((val.location) ? val.location : "")), val.pk));
                });
            }
        }
        if ((gOutputData.baseSelectionAction != undefined && gOutputData.baseSelectionAction != null && gOutputData.baseSelectionAction != "")) {
            var temp_inhomewise_list = {};
            var is_replicated = '';
            var index = 0;
            var temp_bases_list = {};
            $.each(gOutputData.baseSelectionAction, function (key, val) {
                if (is_replicated == '' && key == 'isReplicated')
                    is_replicated = val;
                else {
                    temp_inhomewise_list[key] = {};
                    $.each(val, function (key1, val1) {
                        var temp_base_info = [];
                        $.each(val1.baseInfo, function (key2, val2) {
                            temp_base_info.push(new artworkInfo(val2.baseType, val2.base, val2.baseImage1, val2.baseImage2, ''));
                        });
                        temp_inhomewise_list[key][key1] = new selectedBaseInfo(temp_base_info, val1.isEnabled);
                    });
                }
            });
            if (is_replicated == true)
                self.isReplicated(true);
            else
                self.isReplicated(false);

            self.varBaseListInfo(new selectedBaseListInfo(temp_inhomewise_list, is_replicated));
            self.prevVarBaseListInfo(new selectedBaseListInfo(temp_inhomewise_list, is_replicated));
            //var curr_info = ko.toJS(self.varBaseListInfo());
            //var prev_info = ko.toJS(self.prevVarBaseListInfo());
            //var diff_between_json_objects = DiffObjects(jQuery.parseJSON(JSON.stringify(curr_info)), jQuery.parseJSON(JSON.stringify(prev_info)));
            //if (diff_between_json_objects.length > 0)
            //    self.isReplicated(false);
            //else
            //    self.isReplicated(true);
        }
    }

    //Loading Inhome dates nav bar.
    if (gOutputData != undefined && gOutputData != null) {
        var nav_bar_items = '<div data-role="navbar" class="ui-body-o" data-iconpos="right"><ul>';
        var cnt = -1;
        $.each(gOutputData, function (key, val) {
            if (key.toLowerCase().indexOf('inhome') > -1) {
                var temp_list = {};
                temp_list[key] = val;
                self.inHomeDatesList.push({
                    "showCheckIcon": false,
                    'dateValue': val
                });
                cnt++;
                if (cnt == 0)
                    nav_bar_items += '<li><a href="#" data-icon="check">' + val + ' In Home</a></li>';
                else
                    nav_bar_items += '<li><a href="#" data-icon="edit">' + val + ' In Home</a></li>';
            }
        });
        nav_bar_items += '</ul></div>';
        self.currentInHome(self.inHomeDatesList()[0]);
    }

    self.persistSelectedBaseInfo = function () {
        var selected_inhome = self.currentInHome().dateValue;

        if (selected_inhome != "" && self.selectedStore() != "") {
            var selected_base = self.varArtworkInfo().selectedBase();
            var temp_base_list = self.varArtworkInfo().baseList();
            if (selected_base != "") {
                var temp_base_val = ko.observable({});
                $.each(temp_base_list, function (base_key, base_val) {
                    if (base_val.base() == selected_base) {
                        temp_base_val = ko.toJS(base_val);
                        temp_base_val = new artworkInfo(temp_base_val.baseType, temp_base_val.base, temp_base_val.baseImage1, temp_base_val.baseImage2, "");
                        temp_base_val.baseImage1(temp_base_val.baseImage1().substring(temp_base_val.baseImage1().lastIndexOf('/') + 1));
                        temp_base_val.baseImage2(temp_base_val.baseImage2().substring(temp_base_val.baseImage2().lastIndexOf('/') + 1));
                        return false;
                    }
                });
                if (Object.keys(ko.toJS(temp_base_val)).length > 0) {
                    self.varBaseListInfo()[selected_inhome]()[self.selectedStore()].baseInfo([]);
                    self.varBaseListInfo()[selected_inhome]()[self.selectedStore()].baseInfo.push(temp_base_val);
                }
            }
        }
    };

    self.navBarItemClick = function (data, event) {
        $('div.ui-grid-a').find('label,input,textarea').remove();
        var ele = event.target || event.targetElement;
        $('#dvNavBar div ul li a').removeClass('ui-btn-active');
        $(ele).addClass('ui-btn-active');
        self.persistSelectedBaseInfo();
        self.currentInHome(ko.dataFor(ele));
        self.getSelectedInfo();
    }

    //Loading the data based on the selected store in the current inhome.
    self.locationChanged = function (data, event) {
        //self.validateForDataExistency();
        var ele;
        var selected_store;
        var ele = event.target || event.targetElement;
        var selected_store = $(ele).val();
        var selected_inhome = self.currentInHome().dateValue;

        self.persistSelectedBaseInfo();

        self.selectedStore(self.selectedLocation());
        self.getSelectedInfo();

        var curr_info = ko.toJS(self.varBaseListInfo());
        var prev_info = ko.toJS(self.prevVarBaseListInfo());
        var diff_between_json_objects = [];
        var is_diff_found = false;
        $.each(curr_info, function (key1, val1) {
            if (key1.toLowerCase() != 'isreplicated') {
                $.each(prev_info, function (key2, val2) {
                    if (key2.toLowerCase() != 'isreplicated') {
                        $.each(val1, function (key3, val3) {
                            var curr_temp1 = val3;
                            var prev_temp2 = val2[key3];
                            diff_between_json_objects = DiffObjects(jQuery.parseJSON(JSON.stringify(curr_temp1)), jQuery.parseJSON(JSON.stringify(prev_temp2)));
                            if (diff_between_json_objects.length != 0) {
                                is_diff_found = true;
                                return false;
                            }
                        });
                    }
                    if (is_diff_found) {
                        return false;
                    }
                });
                if (is_diff_found) {
                    return false;
                }
            }
        });

        if (is_diff_found) {
            self.isReplicated(false);
            $('#chkReplicate').checkboxradio('refresh');
        }
        else {
            self.isReplicated(true);
            $('#chkReplicate').checkboxradio('refresh');
        }
    };

    //Load the base info for the selected location in the current inhome.
    self.getSelectedInfo = function () {
        $('div.ui-grid-a').find('label,input,textarea').remove();
        var selected_inhome = self.currentInHome().dateValue;
        var is_found = false;
        if (self.varBaseListInfo()[selected_inhome]() != undefined && self.varBaseListInfo()[selected_inhome]() != null)
            self.selectedInhomeInfo(self.varBaseListInfo()[selected_inhome]());

        if (self.varBaseListInfo()[selected_inhome]()[self.selectedStore()] != undefined && self.varBaseListInfo()[selected_inhome]()[self.selectedStore()] != null && self.varBaseListInfo()[selected_inhome]()[self.selectedStore()].baseInfo().length > 0) {
            $.each(self.varBaseListInfo()[selected_inhome]()[self.selectedStore()].baseInfo(), function (key, val) {
                $.each(self.varArtworkInfo().baseList(), function (key1, val1) {
                    if (val.base() == val1.base()) {
                        self.varArtworkInfo().selectedBase(val.base());
                        self.selectedInHomeStoreBaseInfo(val);
                    }
                });
            });
        }
        else
            self.varArtworkInfo().selectedBase("");
        $('#dvNavBar').trigger('create');
        $('#ulBaseList').listview('refresh');
        $('#ulBaseList li').removeClass('ui-body-j').addClass('ui-body-c');
        $(':radio:checked').parent().addClass('ui-body-j').removeClass('ui-body-c');
        var temp = ko.toJS(self.varBaseListInfo());
        self.updateInHomeNavBarUI(temp);
    };


    self.baseChanged = function () {
        $('#ulBaseList li').removeClass('ui-body-j').addClass('ui-body-c');
        $(':radio:checked').parent().addClass('ui-body-j').removeClass('ui-body-c');
    };

    self.replicateChanged = function (data, event) {
        self.persistSelectedBaseInfo();
        var ele = event.target || event.targetElement;
        $('#confirmMsg').html('Replicating Setup Entries will overwrite any previous information entered.');
        $('#okButConfirm').text('Continue');

        var curr_info = ko.toJS(self.varBaseListInfo());
        var prev_info = ko.toJS(self.prevVarBaseListInfo());
        var is_diff_found = false;
        $.each(curr_info, function (key1, val1) {
            if (key1.toLowerCase() != 'isreplicated') {
                $.each(prev_info, function (key2, val2) {
                    if (key2.toLowerCase() != 'isreplicated') {
                        //if (key1 == key2) {
                        $.each(val1, function (key3, val3) {
                            var curr_temp1 = val3;
                            var prev_temp2 = val2[key3];
                            diff_between_json_objects = DiffObjects(jQuery.parseJSON(JSON.stringify(curr_temp1)), jQuery.parseJSON(JSON.stringify(prev_temp2)));
                            if (diff_between_json_objects.length != 0) {
                                is_diff_found = true;
                                return false;
                            }
                        });
                        //}
                        if (is_diff_found) {
                            return false;
                        }
                    }
                });
                if (is_diff_found) {
                    return false;
                }
            }
        });
        $('#cancelButConfirm').bind('click', function () {
            $('#cancelButConfirm').unbind('click');
            $('#okButConfirm').unbind('click');
            $('#cancelButConfirm').attr('onclick', '$(\'#popupConfirmDialog\').popup(\'close\');');
            self.isReplicated(false);
            $('#chkReplicate').checkboxradio('refresh');
            $('#popupConfirmDialog').popup('close');
        });
        $('#okButConfirm').bind('click', function () {
            $('#cancelButConfirm').unbind('click');
            $('#cancelButConfirm').attr('onclick', '$(\'#popupConfirmDialog\').popup(\'close\');');
            $('#okButConfirm').unbind('click');
            self.replicateData();
            self.isReplicated(true);
            $('#chkReplicate').checkboxradio('refresh');
            $('#popupConfirmDialog').popup('close');
        });
        if ($(ele).is(':checked') && is_diff_found) {
            $('#popupConfirmDialog').popup('open');
        }
    };
    self.replicateData = function () {
        var selected_inhome = self.currentInHome().dateValue;
        var temp_info = {};
        if (self.varBaseListInfo()[selected_inhome]() != undefined && self.varBaseListInfo()[selected_inhome]() != null && self.varBaseListInfo()[selected_inhome]() != "")
            temp_info = self.varBaseListInfo()[selected_inhome]();
        $.each(self.inHomeDatesList(), function (key2, val2) {
            if (selected_inhome != val2.dateValue) {
                self.varBaseListInfo()[val2.dateValue](temp_info);
            }
        });

        self.varBaseListInfo().isReplicated(true);

        var temp = ko.toJS(self.varBaseListInfo());
        if ((temp != undefined && temp != null && temp != "")) {
            var temp_inhomewise_list = {};
            var is_replicated = '';
            var index = 0;
            var temp_bases_list = {};
            $.each(temp, function (key, val) {
                if (is_replicated == '' && key == 'isReplicated')
                    is_replicated = val;
                else {
                    temp_inhomewise_list[key] = {};
                    $.each(val, function (key1, val1) {
                        var temp_base_info = [];
                        $.each(val1.baseInfo, function (key2, val2) {
                            temp_base_info.push(new artworkInfo(val2.baseType, val2.base, val2.baseImage1, val2.baseImage2, ''));
                        });
                        temp_inhomewise_list[key][key1] = new selectedBaseInfo(temp_base_info, val1.isEnabled);
                    });
                }

            });
            self.prevVarBaseListInfo(new selectedBaseListInfo(temp_inhomewise_list, is_replicated));
        }


        $.each(gOutputData.baseSelectionAction, function (key, val) {
            if (is_replicated == '' && key == 'isReplicated')
                is_replicated = val;
            else {
                temp_inhomewise_list[key] = {};
                $.each(val, function (key1, val1) {
                    var temp_base_info = [];
                    $.each(val1.baseInfo, function (key2, val2) {
                        temp_base_info.push(new artworkInfo(val2.baseType, val2.base, val2.baseImage1, val2.baseImage2, ''));
                    });
                    temp_inhomewise_list[key][key1] = new selectedBaseInfo(temp_base_info, val1.isEnabled);
                });
            }
        });
        self.updateInHomeNavBarUI(temp);
    };

    self.verifyData = function (temp_info) {
        var is_data_found = false;
        $.each(temp_info, function (key1, val1) {
            if (val1.baseInfo.length > 0)
                is_data_found = true;
            else
                is_data_found = false;
            if (!is_data_found) {
                return false;
            }
        });
        return is_data_found;
    };

    self.updateInHomeNavBarUI = function (temp) {
        $.each(temp, function (key, val) {
            if (key != "isReplicated") {
                var is_valid = self.verifyData(val);
                if (is_valid) {
                    $('#dvNavBar div ul li a[data-inHome="' + key + '"]').attr('data-icon', 'check').addClass('ui-icon-check').removeClass('ui-icon-edit');
                }
                else {
                    $('#dvNavBar div ul li a[data-inHome="' + key + '"]').attr('data-icon', 'edit').addClass('ui-icon-edit').removeClass('ui-icon-check');
                }
            }
        });
    };

    self.confirmContinue = function (current_page, page_name, i_count, click_type) {
        self.persistSelectedBaseInfo();
        var temp = ko.toJS(self.varBaseListInfo());
        self.updateInHomeNavBarUI(temp);
        var ctrls = $('#dvNavBar div ul li a.ui-icon-edit');
        if (ctrls.length > 0 && click_type == "onlycontinue") {
            var tmp_list = "";
            $.each(ctrls, function (key, val) {
                tmp_list += (tmp_list != "") ? (key < (ctrls.length - 1)) ? ', ' : ' and ' + $(this).attr('data-inHome') : $(this).attr('data-inHome');
            });

            $('#confirmMsg').html('You have not completed setup for ' + tmp_list + '. Do you want to stay on this page to continue setup or go to the next page?');
            $('#okButConfirm').text('Go To Next Page');
            $('#cancelButConfirm').text('Continue Setup');

            $('#cancelButConfirm').bind('click', function () {
                $('#cancelButConfirm').unbind('click');
                $('#okButConfirm').unbind('click');
                $('#cancelButConfirm').attr('onclick', '$(\'#popupConfirmDialog\').popup(\'close\');');
                $('#popupConfirmDialog').popup('close');
            });
            $('#okButConfirm').bind('click', function () {
                $('#okButConfirm').unbind('click');
                $('#cancelButConfirm').unbind('click');
                $('#cancelButConfirm').attr('onclick', '$(\'#popupConfirmDialog\').popup(\'close\');');
                $('#popupConfirmDialog').popup('close');
                goToNextPage("../jobBaseSelection/jobBaseSelection.html", "../jobBaseSelection/jobBaseSelection.html", 5, 'onlycontinue');
                self.updateBaseSelectionInfo(current_page, page_name, i_count, click_type);
            });
            $('#popupConfirmDialog').popup('open');
            return false;
        }
        else {
            //goToNextPage("../jobcompanyInfo/jobCompanyInfo.html", "../jobbaseSelection/jobBaseSelection.html", 5, 'onlycontinue');
            return self.updateBaseSelectionInfo(current_page, page_name, i_count, click_type);
        }
    };
    self.updateBaseSelectionInfo = function (current_page, page_name, i_count, click_type) {
        // Call validation function to validate the data in the fields.  If it returns true proceed further else stop further execution.
        var selected_inhome = self.currentInHome().dateValue;

        var temp_json = ko.mapping.toJS(self.varBaseListInfo);
        gOutputData.baseSelectionAction = temp_json;
        //var temp_selected_base = {};
        //$.each(temp_json, function (key, val) {
        //    $.each(temp_json.baseList, function (base_key, base_val) {
        //        if (base_val.base == val.selectedBase) {
        //            var temp_base_val = base_val;
        //            temp_base_val["type"] = val.type;
        //            temp_base_val.baseImage1 = temp_base_val.baseImage1.substring(temp_base_val.baseImage1.lastIndexOf('/') + 1);
        //            temp_base_val.baseImage2 = temp_base_val.baseImage2.substring(temp_base_val.baseImage2.lastIndexOf('/') + 1);
        //            gOutputData.baseSelectionAction.baseInfo.push(temp_base_val);
        //        }
        //    });
        //});
        sessionStorage.jobSetupOutput = JSON.stringify(gOutputData);
        if (click_type == "onlycontinue")
        window.location.href = (page_name != undefined && page_name != null && page_name != "") ? page_name : current_page;
        return true;
        //return false;
    };
    self.showOrderedLocations = function () {
        $('#popUpLocations').popup('open');
    };

    self.manageLocationsPopup = function (data, event) {
        var ele = event.target || event.targetElement;
        if ($(ele).attr('data-btnType') == "submit") {
            self.previousSelectedStore(self.selectedStore());
        }
        else {
            self.selectedStore(self.previousSelectedStore());
            self.selectedLocation(self.previousSelectedStore())
        }
        $('#ddlLocations').selectmenu('refresh');
        displayJobInfo();
        var header_text = $('#navHeader').text();
        var sel_location = $('#ddlLocations option:selected').text();
        header_text = header_text + ' (' + sel_location + ')';
        $('#navHeader').text(header_text);
        $('#popUpLocations').popup('close');
    };
    self.previewMailing = function () {
        getNextPageInOrder();
        var preview_proof = "../jobProofing/jobProofing.html?fileName=58683_GWBerkheimerOLSONCOMFORT.pdf&jobNumber=" + sessionStorage.jobNumber;
        window.location.href = preview_proof;
    }
};

function showHomePage(type) {
    if (type == "home") {
        window.location.href = "../index.htm";
    }
    else if ((type == "template")) {
        window.location.href = " ../adminTemplateSetup/templateSetup.html";
    }
    else if ((type == "vcc")) {
        window.location.href = " ../adminTemplateSetup/variableCompanyInfo.html";
    }
};

