﻿var manageGroupedLocations = function (self) {

    self.groupedLocations = ko.observableArray([]);
    self.loadGroupedLocations = function () {
        $('#popupGroupedLocations ul').empty();
        $('#popupGroupedLocations div[data-role="collapsible"]').remove();
        $.getJSON("../jobSelectLocations/JSON/_groupedLocations.JSON", function (data) {
            self.gDataFilteredGroup.removeAll();
            var p = data;
            //self.gDataFilteredGroup = data;
            if (Object.keys(data).length > 0) {
                self.clearMarkers('g');
                self.groupedLocations.removeAll();
                var search_string = "";
                if (data != undefined && data != null && Object.keys(data).length > 0) {
                    var index = 0;
                    var latLngPts = [];
                    var current_markers = $.grep(self.markers_array, function (obj) {
                        return obj.type === 'g';
                    });
                    var grpLoc_lst = [];
                    $.each(data, function (key, val) {
                        var loc_cnt = val.length;
                        grpLoc_lst = [];
                        $.each(val, function (a, b) {
                            b["needsCount"] = (b["needsCount"] != undefined && b["needsCount"] != null && b["needsCount"] != "") ? b["needsCount"] : true;
                            b["isNewStore"] = false;
                            grpLoc_lst.push(new locationModel1(b, a));
                            search_string += (search_string != '') ? '|' + ((grpLoc_lst[a].storeCompleteAddress() != "") ? grpLoc_lst[a].storeCompleteAddress() : '') : (grpLoc_lst[a].storeCompleteAddress() != "") ? grpLoc_lst[a].storeCompleteAddress() : '';
                            search_string += (grpLoc_lst[a].computedStoreName() != "") ? '^' + grpLoc_lst[a].computedStoreName() : '^';
                            search_string += ((grpLoc_lst[a].phone != undefined && grpLoc_lst[a].phone != null && grpLoc_lst[a].phone() != "") ? '^' + grpLoc_lst[a].phone() : '^');
                            search_string += '^' + grpLoc_lst[a].locationIndex; // +'^';
                            latLngPts.push({ 'lat': grpLoc_lst[a].latitude(), 'lng': grpLoc_lst[a].longitude() });
                            current_markers[0].addresses.push(search_string);
                            search_string = "";
                            if (loc_cnt == grpLoc_lst.length) {
                                self.gDataFilteredGroup.push(new locationsGroupModel(key, grpLoc_lst));
                                //self.gDataFilteredGroup(grpLoc_lst);
                                //$('#dvSearchLocationsMap').css('display', 'block');
                                //$('#map-canvas').css('display', 'block');
                                if (self.uIViewConfig.m())
                                    self.displayMarkers(latLngPts, 'g');
                                //$('#mapBlock').css('visibility', 'visible');
                                //self.isFirstTimeExisting(false);
                            }

                        });
                        index++;
                    });
                }
                else {

                }
                $('#popupGroupedLocations ul').listview().trigger('create');
                $('#popupGroupedLocations div[data-role="collapsible"]').collapsible().trigger('create');
                $('#popupGroupedLocations div[data-role="collapsible-set"]').collapsibleset('refresh');
                $('#popupGroupedLocations').popup('open', { positionTo: 'window' });
            }
        });
    };
    self.addGroupToOrder = function (data, event) {
        var context = ko.contextFor(event.target).$parent;
        var loc_no_delivery = "";
        $.each(data.locations(), function (a, b) {
            if ((jobCustomerNumber == CASEYS || jobCustomerNumber == JETS) && self.gOutputData.templateName.indexOf('cgs_postcard') > -1 && b.hasDelivery().toLowerCase() == "false") {
                loc_no_delivery += (loc_no_delivery != "") ? ", " + b.storeId() : b.storeId();
            }
            else {
                self.addSelectedStore(b, event);
            }
        });
        $('#dvSearchLocationsMap').css('display', 'block');
        $('#map-canvas').css('display', 'block');
        $('#mapBlock').css('visibility', 'visible');
        $('#popupGroupedLocations').popup('close');
        if (loc_no_delivery != "") {
            $('#alertmsg').html("Locations (" + loc_no_delivery + ") do not offer delivery and may not be included in an Opt-In order.");
            $('#okBut').attr('onclick', '$(\'#popupDialog\').popup().popup(\'close\');');
            $('#popupDialog').popup().popup('open');
        }
        var t = data;

    };
    self.clearOrderedLocations = function (data, event) {
        $('#confirmMsg').html("Clearing the locations removes all the locations from the order. Click \"Clear\" to remove or \"Cancel\" if you do not want to remove.");
        $('#okButConfirm').text('Clear');
        $('#okButConfirm').bind('click', function () {
            self.selectedLocationsAdded.removeAll();
            self.selectedLocationsPaged.removeAll();
            self.gOutputData.selectedLocationsAction.selectedLocationsList = [];
            sessionStorage.jobSetupOutput = JSON.stringify(self.gOutputData);
            self.clearMarkers('o');
            if (self.selectedLocationsAdded().length == 0 && self.searchedLocations().length == 0 && self.locations().length == 0) {
                $('#dvSearchLocationsMap').css('display', 'none');
                $('#map-canvas').css('display', 'none');
                $('#mapBlock').css('visibility', 'hidden');
            }
            $('#popupConfirmDialog').popup().popup('close');
            $('#okButConfirm').unbind('click');
            $('#dvOrderedLocationsContainer').css('height', 'auto');
            $('#dvOrderedLocationsContainer').css('overflow-y', 'auto');
            if (self.selectedLocationsAdded().length == 0) {
                $('#fldGrpButPanel').css('display', 'none');
            }
            else {
                if (self.gOutputData.jobTypeId == 71 || self.gOutputData.jobTypeId == 72)
                    $('#fldGrpButPanel').css('display', 'none');
            }
        });
        $('#okButConfirm').find('.ui-btn-text').text('Clear');
        $('#popupConfirmDialog').popup().popup('open');
        var p = data;
    };

    self.removeGrpLocationsConfirm = function (data, event) {
        $('#confirmMsg').html("Are you sure you want to delete this Group?");
        $('#okButConfirm').bind('click', function () {
            $('#okButConfirm').unbind('click');
            $('#cancelButConfirm').unbind('click');
            $('#cancelButConfirm').attr('onclick', '$(\'#popupConfirmDialog\').popup(\'open\');$(\'#popupConfirmDialog\').popup(\'close\');');
            self.removeGroupedLocations(data, event);
        });
        $('#cancelButConfirm').bind('click', function () {
            $('#popupConfirmDialog').popup('close');
            $('#cancelButConfirm').unbind('click');
            $('#cancelButConfirm').attr('onclick', '$(\'#popupConfirmDialog\').popup(\'open\');$(\'#popupConfirmDialog\').popup(\'close\');');
            $('#okButConfirm').unbind('click');
            window.setTimeout(function restoreGrpList() {
                $('#popupGroupedLocations').popup('open');
            }, 500);
        });
        $('#popupGroupedLocations').popup('close');
        $('#popupConfirmDialog').popup().popup('open');
    };

    self.removeGroupedLocations = function (data, event) {
        self.gDataFilteredGroup.remove(data);
        $('#popupGroupedLocations div[data-role="collapsible-set"]').collapsibleset('refresh');
        $('#popupConfirmDialog').popup('close');
        $('#alertmsg').html('Group has been deleted successfully.');
        $('#okBut').bind('click', function () {
            $('#popupDialog').popup('close');
            $('#okBut').unbind('click');
            $('#okBut').attr('onclick', '$(\'#popupDialog\').popup(\'close\');');
            if (self.gDataFilteredGroup().length > 0) {
                window.setTimeout(function restoreGrpList() {
                    $('#popupGroupedLocations').popup('open');
                }, 500);
            }
        });
        window.setTimeout(function closeGrpList() {
            $('#popupDialog').popup().popup('open');
        }, 500);
    };

    self.removeIndGrpLocation = function (index, parent, data, event) {
        //parent.locations.remove(data);
        //self.gDataFilteredGroup()[ko.contextFor(event.target).$parentContext.$index()].locations.splice(index, 1);
        parent.locations.remove(data);
        //delete parent.locations[index];
        if (parent.locations().length == 0) {
            self.gDataFilteredGroup.remove(parent);
            $('#popupGroupedLocations div[data-role="collapsible-set"]').collapsibleset('refresh');
        }
        $('#popupGroupedLocations').popup('close');
        $('#alertmsg').html('Location has been deleted from group successfully.');
        $('#okBut').bind('click', function () {
            $('#popupDialog').popup('close');
            $('#okBut').unbind('click');
            $('#okBut').attr('onclick', '$(\'#popupDialog\').popup(\'close\');');
            if (self.gDataFilteredGroup().length > 0) {
                window.setTimeout(function restoreGrpList() {
                    $('#popupGroupedLocations').popup('open');
                }, 500);
            }
        });
        $('#popupDialog').popup().popup('open');
    };
}