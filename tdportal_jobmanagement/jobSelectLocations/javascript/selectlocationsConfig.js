﻿hideFindAllAvailableLocationDiv = function () {
    var customer = getCustomerNumber();
    return ([DKI, ICYNENE, SPORTSKING, CW, DCA, KUBOTA, GWA, BUFFALONEWS, PIONEERPRESS, REDPLUM, SCOTTS, EQUINOX, ALLIED, ACE, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);
}

showPostSearchLocationsPopup = function () {
    var customer = getCustomerNumber();
    return ([DKI, ICYNENE, JETS, ALLIED, CASEYS, CW, DCA, AAG, KUBOTA, SPORTSKING, GWA, SCOTTS, BUFFALONEWS, PIONEERPRESS, REDPLUM, EQUINOX, ACE, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);
}

isCreateLocationsDemoHints = function () {
    var customer = getCustomerNumber();
    return ([DKI, ICYNENE, JETS, KUBOTA, ALLIED, CASEYS, CW, DCA, AAG, SPORTSKING, GWA, SCOTTS, BUFFALONEWS, PIONEERPRESS, REDPLUM, EQUINOX, ACE, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);
}

hideLocationGroups = function () {
    var customer = getCustomerNumber();       
    return ([AAG, DKI, ICYNENE, ACE, CASEYS, DCA, KUBOTA, CW, SPORTSKING, GWA, SCOTTS, BUFFALONEWS, PIONEERPRESS, REDPLUM, EQUINOX, ALLIED, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);
}

isChangeTextToAllPopups = function () {
    var customer = getCustomerNumber();
    return ([DKI, ICYNENE].indexOf(customer) > -1);
}

showSearchLocationsPopup = function () {
    var customer = getCustomerNumber();
    return ([DKI, ICYNENE, JETS, ALLIED, CASEYS, CW, DCA, KUBOTA, AAG, SPORTSKING, GWA, SCOTTS, BUFFALONEWS, PIONEERPRESS, REDPLUM, EQUINOX, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);
}

showLocationsMapPopup = function () {
    var customer = getCustomerNumber();    
    return ([DKI, ICYNENE, JETS, ALLIED, CASEYS, CW, DCA, AAG, KUBOTA, SPORTSKING, GWA, SCOTTS, BUFFALONEWS, PIONEERPRESS, REDPLUM, EQUINOX, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);
}

showQuantityRequired = function () {
    var customer = getCustomerNumber();  
    return ([CASEYS, REGIS, SCOTTS, JETS, ALLIED, AAG, DCA, KUBOTA, SPORTSKING, GWA, BUFFALONEWS, PIONEERPRESS, REDPLUM, EQUINOX, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER, DKI, ICYNENE].indexOf(jobCustomerNumber) == -1);
}

isUpdateInHomeDate = function () {
    var customer = getCustomerNumber();
    return ([DKI, ICYNENE].indexOf(customer) == -1);
}

showSearchedLocationsPopup = function () {
    var customer = getCustomerNumber();    
    return ([DKI, ICYNENE,JETS, CASEYS, CW, AAG, SCOTTS, BUFFALONEWS, PIONEERPRESS, REDPLUM, EQUINOX, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);
}

showNewLocationsPopup = function () {
    var customer = getCustomerNumber();    
    return ([DKI, ICYNENE, JETS, CASEYS, DCA, KUBOTA, SPORTSKING, GWA, SCOTTS, BUFFALONEWS, PIONEERPRESS, REDPLUM, EQUINOX, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);
}

hideStatusList = function () {
    var customer = getCustomerNumber();
    return ([KUBOTA, GWA, SCOTTS, ALLIED, ACE, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER,CW].indexOf(customer) > -1);
}

isChangePartialHeaderText = function () {
    var customer = getCustomerNumber();    
    return ([DCA, KUBOTA, SPORTSKING, GWA, BUFFALONEWS, PIONEERPRESS, REDPLUM].indexOf(customer) > -1);
}

isRVSales = function () {
    var customer = getCustomerNumber();
    return ([CW, BRIGHTHOUSE, BBB].indexOf(customer) > -1);
}

isHardwareStore = function () {
    var customer = getCustomerNumber();
    return ([SCOTTS, ACE, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);
}

isHaircut = function () {
    var customer = getCustomerNumber();
    return ([REGIS].indexOf(customer) > -1);
}

showOrderLocationsLimit = function () {
    var customer = getCustomerNumber();
    return ([MILWAUKEE, ACE].indexOf(customer) > -1);
}