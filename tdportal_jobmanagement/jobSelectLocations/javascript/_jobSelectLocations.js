﻿jQuery.support.cors = true;

//******************** Data URL & Page Global Variables Start ************************
var gServiceUrl = "";
var gOutputServiceUrl = '../JSON/_jobSetupOutput.JSON';
var gStateServiceUrl = '../JSON/_listStates.JSON';
var gSixMangoLocationsUrl = 'JSON/_sixMangoLocations.JSON';
var gLocationSearchUrl = serviceURLDomain + 'api/Locations/';
var gData;
var gMapBoundsUSA;
var gMapCenter;
var map;
var statesJSON;
var pagePrefs;
var jobNumber = '';
var jobDesc = '';
var geocoder;
var pageObj;
var sixMangoLocations;
var dontShowHintsAgain = (localStorage.getItem('doNotShowHintsAgain') != undefined && localStorage.getItem('doNotShowHintsAgain') != null && localStorage.getItem('doNotShowHintsAgain') != "" && localStorage.getItem('doNotShowHintsAgain') != "false") ? true : false;
//******************** Data URL & Page Global Variables End **************************

//******************** Page Load Events Start **************************

$(window).on('popupbeforeposition', 'div:jqmData(role="popup")', function () {
    var notDismissible = $(this).jqmData('dismissible') === false;
    if (notDismissible) {
        $('.ui-popup-screen').off();
    }
});
$(document).keydown(function (e) {
    if (e.keyCode === 8) {
        var d = e.srcElement || e.target;
        if (($(d).find('div[data-role=popup]').attr('id') == "popUpLocationProfile" || e.target.nodeName == "FIELDSET" || e.target.nodeName == "SELECT"))
            return false;
    }
});
$('#_jobSelectLocations').live('pagebeforecreate', function (event) {
    ko.setTemplateEngine(new ko.nativeTemplateEngine);
    loadingImg("_jobSelectLocations");
    displayMessage('_jobSelectLocations');
    createConfirmMessage("_jobSelectLocations");

    $('#loadingText1').text('Loading...');
    displayNavLinks();
    jobNumber = sessionStorage.jobNumber;
    jobDesc = sessionStorage.jobDesc;

    window.setTimeout(function loadpopups() {
        $('div[data-role="popup"]').popup();
        $('div[data-role="panel"]').panel();
        $('input[type="radio"]').checkboxradio();
    }, 1500);
    $("#navLinksPanel").on("panelopen", function (event, ui) {
        sessionStorage.isNavPanelOpen = true;
    });
    $('#dvHeaderButtons div').prepend('<a href="#" data-rel="popup" data-position-to="window" data-transition="pop" data-role="button" data-icon="myapp-map-marker" data-iconpos="notext" data-inline="true" data-mini="true" href="#" title="Locations Preferences" data-bind="click:$root.viewClick">Locations Preferences</a>');

    if (isCreateLocationsDemoHints()) {
        setDemoHintsSliderValue();
        //window.setTimeout(function loadHints() {
        createDemoHints("jobSelectLocations");
        if (jobCustomerNumber == AAG && appPrivileges.roleName == "power") {
            $('#dvHeaderButtons div').prepend('<a href="#" data-rel="popup" data-position-to="window" data-transition="pop" data-role="button" data-icon="user" data-iconpos="notext" data-inline="true" data-mini="true" title="Loan Officers" data-bind="click:$root.showLoanOfficers">Loan Officers</a>');
        }
        // }, 100);
    }
    $("#searchLocations").bind({
        popupafterclose: function (event, ui) {
            if (!dontShowHintsAgain && (sessionStorage.showDemoHints == undefined || sessionStorage.showDemoHints == null || sessionStorage.showDemoHints == "" || sessionStorage.showDemoHints == "on") && (sessionStorage.isPostSearchLocationsHints == undefined || sessionStorage.isPostSearchLocationsHints == null || sessionStorage.isPostSearchLocationsHints == "" || sessionStorage.isPostSearchLocationsHints == "false") && featureDefaultLoadExistingLocations() && (showPostSearchLocationsPopup())) {
                  sessionStorage.isPostSearchLocationsHints = true;
                  $('#postSearchLocations').popup('open', { positionTo: '#mapBlock' });
            }            
        }
    });
});

$(document).on('pageshow', '#_jobSelectLocations', function (event) {
    if (jobCustomerNumber == DCA || jobCustomerNumber == KUBOTA || jobCustomerNumber == SPORTSKING || jobCustomerNumber == GWA || jobCustomerNumber == BUFFALONEWS || jobCustomerNumber == PIONEERPRESS || jobCustomerNumber == REDPLUM || jobCustomerNumber == REDPLUM) {
        $('div[data-role="content"] h2').html("Locations");
    }
    $('#loading1').parent().find('p').css('margin-bottom', '0px');
    $('#loading1').parent().find('p').css('margin-top', '0px');
    $('#waitPopUp').popup('open', { positionTo: '#dvWaitPopupRef' });
    window.onload = loadScript(true);

    createPlaceHolderforIE();
    persistNavPanelState();

    if (jobCustomerNumber == AAG) {
        $('a[mysearch=groups]').hide();
        $('a[mysearch=new]').css('visibility', 'visible');
        $('#fldGrpButPanel').css('display', 'none');
        $('#fldGrpButPanel').hide();
    }
    if (hideLocationGroups()) {
        $('a[mysearch=groups]').hide();
        $('#fldGrpButPanel').css('display', 'none');
    }
    //$('#dvLocationsPrefs input[type="checkbox"]').attr('checked', true).checkboxradio('refresh');
    $('#dvLocationsPrefs input[type="checkbox"]').checkboxradio('refresh');
    //$('#popUpLocationProfile').on('popupbeforeposition', function () {
    //    $('#body').keydown(function (e) {
    //        if ($('#popUpLocationProfile').is(':visible')) {
    //            if (e.target.nodeName == "DIV" || e.target.nodeName == "FIELDSET") {
    //                if (e.keyCode == 8) { // 8 is backspace
    //                    e.preventDefault();
    //                }
    //            }
    //        }
    //    });
    //});
    //$('#popUpLocationProfile').on('popupafterclose', function () {
    //    $('#body').unbind('keydown');
    //});
    if (featureNewLocations()) {
        $('#btnAddNew').css('visibility', 'visible');
        $('label[for="chkViewNewLocations"]').css('display', 'block');
        $('chkViewNewLocations').css('display', 'block');
    }
    if (jobCustomerNumber == CASEYS || jobCustomerNumber == CW || jobCustomerNumber == AAG || jobCustomerNumber == BUFFALONEWS || jobCustomerNumber == PIONEERPRESS || jobCustomerNumber == REDPLUM || jobCustomerNumber == EQUINOX || isChangeTextToAllPopups()) {
        $('#searchLocations p a').text('Turn Hints Off');
        $('#groupedLocations p a').text('Turn Hints Off');
        $('#selectedLocations p a').text('Turn Hints Off');
        $('#newLocations p a').text('Turn Hints Off');
        $('#locationMap p a').text('Turn Hints Off');
    }
    if (!dontShowHintsAgain && (showSearchLocationsPopup()) && (sessionStorage.showDemoHints == undefined || sessionStorage.showDemoHints == null || sessionStorage.showDemoHints == "" || sessionStorage.showDemoHints == "on") && (sessionStorage.isLocationsDemoHintsDisplayed == undefined || sessionStorage.isLocationsDemoHintsDisplayed == null || sessionStorage.isLocationsDemoHintsDisplayed == "false")) {
        if (!featureDefaultLoadExistingLocations()) {
            sessionStorage.isLocationsDemoHintsDisplayed = true;
            $('#searchLocations').popup('close');
            window.setTimeout(function loadHints() {
                sessionStorage.isSearchLocationsPopupDisplayed = true;
                $('#searchLocations').popup('open', { positionTo: '#dvExistingLocationsContainer' });
            }, 3800);
        }
        //$('#btnNavHints').trigger('click');
    }
    $("[data-role=footer]").toolbar({ tapToggleBlacklist: "#map-canvas" });
    $('#sldrShowHints').slider("refresh");
});
function loadPage() {
    //var latlng = new google.maps.LatLng(43.1462421, -104.9929722);
    //geocoder = new google.maps.Geocoder();
    //geocoder.geocode({ 'address': 'US' }, function (results, status) {
    //    if (status == "OK") {
    //        var mapOptions = {
    //            //zoom: 12,
    //            zoom: 7,
    //            //center: latlng,
    //            center: results[0].geometry.location,
    //            //mapTypeId: google.maps.MapTypeId.TERRAIN
    //            mapTypeId: google.maps.MapTypeId.ROADMAP
    //        }
    //        map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
    //        gMapBoundsUSA = new google.maps.LatLngBounds(new google.maps.LatLng(28.70, -127.50), new google.maps.LatLng(48.85, -55.90));
    //        gMapCenter = map.getCenter();
    //    }
    loadStates();
    //});
}
//******************** Page Load Events End **************************

//******************** Public Functions Start **************************

//Loads the states from static JSON into object and after that initializes the view model and binds to UI.
function loadStates() {
    $.getJSON(gStateServiceUrl, function (result) {
        statesJSON = result;
        //loadData();
        //page_obj = new buildNewStores();
        pageObj = new locationFiltersAndPaging();
        //page_obj.makeGOutputData();
        //window.setTimeout(function loadInfo() {
        pageObj.loadLocationConfig();
        //pageObj.initializeMap();
        //if (jobCustomerNumber == AAG) {
        //    var search_json = {};
        //    search_json["state"] = "IL";
        //    pageObj.getFilterdLocations(JSON.stringify(search_json));
        //}
        //ko.cleanNode(document.getElementById('_jobStores'));
        //$("div[data-temp='true']").remove();
        ko.applyBindings(pageObj);
        $('#ulAvailableLocations').listview().listview('refresh');
        $('#ulSelectedLocations').listview().listview('refresh').trigger("create");
        $('#fldStOptInUsers input[type="checkbox"]').checkboxradio('refresh');
        $('#fldGrpButPanel').parent().trigger('create');
        $('#dvE').trigger('create');
        $('#dvO').trigger('create');
        $('#dvM').trigger('create');
        $('#dvN').trigger('create');
        $('#mapBlock').css('display', 'block');
        $('#mapBlock').css('visibility', 'hidden');
        $('#fldStOptInUsers').trigger('create');
        $('#dvLocationsPrefs input[type="checkbox"]').checkboxradio('refresh');
        if (hideLocationGroups()) {
            $('#fldGrpButPanel').css('display', 'none');
        }
		if (map == undefined) {
            sessionStorage.removeItem("firstTimeInLocations");
            pageObj.initMapCanvas();
            var timer_id = window.setInterval(function () {
                if (map != undefined) {
                    ///***** START Temporary demo purpose, remove later *****///
                    if (jobCustomerNumber == KUBOTA && (sessionStorage.username.toLowerCase().indexOf('six_mango') > -1)) {
                        $('#btnSearchExisting').css('display', 'none');
                        sessionStorage.firstTimeInLocations = true;
                        pageObj.uIViewConfig.e(true);
                        pageObj.viewPrefChanged();
                    }
                    ///***** END Temporary demo purpose, remove later *****///

                    if (featureDefaultLoadExistingLocations()) {
                         if ((sessionStorage.firstTimeInLocations == undefined || sessionStorage.firstTimeInLocations == null || sessionStorage.firstTimeInLocations == "")) {                           
                             $('#btnSearchExisting').trigger('click');
                             if (!dontShowHintsAgain && (sessionStorage.showDemoHints == undefined || sessionStorage.showDemoHints == null || sessionStorage.showDemoHints == "" || sessionStorage.showDemoHints == "on") && (sessionStorage.isLocationsDemoHintsDisplayed == undefined || sessionStorage.isLocationsDemoHintsDisplayed == null || sessionStorage.isLocationsDemoHintsDisplayed == "false")) {

                                 $('#searchLocations').popup('close');
                                 window.setTimeout(function () {
                                     sessionStorage.isSearchLocationsPopupDisplayed = true;
                                     $('#searchLocations').popup('open', { positionTo: '#dvExistingLocationsContainer' });
                                 }, 5000);
                             }                             
                        }
                        else {
                            pageObj.locationsSearch();
                        }

                    }
                    else {
                        pageObj.locationsSearch();
                    }
                }
                window.clearInterval(timer_id);
            }, 1000);

        }
        //else
        //    pageObj.locationsSearch();
        window.setTimeout(function () {
            $('#waitPopUp').popup('close');
        }, 2000);
        if (((sessionStorage.isDemoSelected != undefined && sessionStorage.isDemoSelected != null && sessionStorage.isDemoSelected == "1") || (sessionStorage.username.toLowerCase().indexOf('demo') > -1)) &&
                (sessionStorage.isDemoInsDisplayed == undefined || sessionStorage.isDemoInsDisplayed == null || sessionStorage.isDemoInsDisplayed == "" || sessionStorage.isDemoInsDisplayed == "false")) {
            sessionStorage.isDemoInsDisplayed = true;
            //if (jobCustomerNumber != AAG)
            //    $('#btnDemoInstructions').trigger('click');
        }
        //}, 2000);
    });
    ///***** START Temporary demo purpose, remove later *****///
    $.getJSON(gSixMangoLocationsUrl, function (location_result) {
        sixMangoLocations = location_result;
    });
    ///***** END Temporary demo purpose, remove later *****///
}

function ddlFocusSet(id) {
    var element = document.getElementById(id);
    if ($.browser.msie) {
        element.focus();
        setTimeout(function () {
            element.focus();
        }, 1);
    }
    else {
        element.focus();
        //var select = $(this).val();
    }
}

//Capitalizes the first letter in the given string.
String.prototype.initCap = function () {
    return this.replace(/(?:^|\s)[a-z]/g, function (m) {
        return m.toUpperCase();
    });
};

//Checks the given string ends with a specific (given character).
String.prototype.endsWith = function (suffix) {
    return this.indexOf(suffix, this.length - suffix.length) !== -1;
};

function manageDemoHintsPopups() {
    if (!dontShowHintsAgain && (sessionStorage.showDemoHints == undefined || sessionStorage.showDemoHints == null || sessionStorage.showDemoHints == "" || sessionStorage.showDemoHints == "on") && (sessionStorage.isMapDisplayed == undefined || sessionStorage.isMapDisplayed == null || sessionStorage.isMapDisplayed == "" || sessionStorage.isMapDisplayed == "false") && (showLocationsMapPopup())) {
        sessionStorage.isMapDisplayed = true;
        $('#locationMap').popup('open', { positionTo: '#mapBlock' });
    }
    //($('#locationsCount').text() != "" && sessionStorage.isMapDisplayed =="false"))
}

function closeInitialPopup(popup_ctrl) {
    $('#' + popup_ctrl).popup('close');
    //if (jobCustomerNumber == CASEYS || jobCustomerNumber == CW) {
    //    $('#groupedLocations').popup();
    //    $('#groupedLocations').popup('open', { positionTo: "#dvButtonPadRight" });
    //}
}