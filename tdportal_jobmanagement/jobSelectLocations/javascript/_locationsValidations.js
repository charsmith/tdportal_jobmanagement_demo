﻿var validateLocations = function (self) {
    //Validates the seleced locations whether the quantity is entered by user or not and also for the Maximum alowable quantity.
    self.validateLocations = function () {
        var message = "";
        //eval(getURLDecode(pagePrefs.scriptBlockDict.validateLocations));
        // if (gOutputData.selectedLocationsAction == undefined || gOutputData.selectedLocationsAction.selectedLocationsList == undefined || gOutputData.selectedLocationsAction.selectedLocationsList == null || gOutputData.selectedLocationsAction.selectedLocationsList.length == undefined || gOutputData.selectedLocationsAction.selectedLocationsList.length == 0) {
        //    if (jobCustomerNumber == "1" && sessionStorage.facilityId == undefined || sessionStorage.facilityId == null || sessionStorage.facilityId == "")
        //        message = "Please select at least one facility location.";
        //    else if (jobCustomerNumber == CW || jobCustomerNumber == CASEYS)
        //        message = "Please select at least one location.";
        // }
        //sessionStorage.setUpTwoDate = true;
        if (jobCustomerNumber == AAG || jobCustomerNumber == ALLIED) {
            if (self.gOutputData.selectedLocationsAction.selectedLocationsList.length == 0) {
                self.gOutputData.selectedLocationsAction.selectedLocationsList.push({
                    "storeName": "Test Store1",
                    "address1": "1132 PLEASANT ST STE C",
                    "address2": "1132 PLEASANT ST STE C",
                    "city": "DEKALB",
                    "state": "IL",
                    "zip": "60115",
                    "latitude": "41.932802870000",
                    "longitude": "-88.738168640000",
                    "pk": "15430",
                    "needsCount": "true",
                    "firstName": "Mike",
                    "lastName": "Stanley",
                    "storeId": ""
                });
                if (jobCustomerNumber == ALLIED) {
                    self.gOutputData.selectedLocationsAction.selectedLocationsList.push({
                        "storeName": "Test Store2",
                        "address1": "1132 PLEASANT ST STE C",
                        "address2": "1132 PLEASANT ST STE C",
                        "city": "DEKALB",
                        "state": "IL",
                        "zip": "60115",
                        "latitude": "41.932802870000",
                        "longitude": "-88.738168640000",
                        "pk": "15428",
                        "needsCount": "true",
                        "firstName": "Mike",
                        "lastName": "Stanley",
                        "storeId": ""
                    });
                }
            }
        }
        if (jobCustomerNumber == ALLIED || jobCustomerNumber == AAG || jobCustomerNumber == SPORTSKING) {//jobNumber == "-1" &&  || jobCustomerNumber == DCA
            var inhome_list = [];
            var temp_cmp_info = {};
            if (jobCustomerNumber == AAG) {
                //sessionStorage.setUpTwoDate = true;
                temp_cmp_info = [
                    {
                        "companyInfo": [

                                 { "label": "First Name", "value": "Mike", "isRequired": "true", "showCheckBox": true },
                                               { "label": "Last Name", "value": "Stanley", "isRequired": "true", "showCheckBox": true },
                                               { "label": "Email", "value": "mstanley@aag.com", "isRequired": "true", "showCheckBox": true },
                                               { "label": "Event Date", "value": "mm/dd/yyyy", "isRequired": "true", "showCheckBox": true },
                                               { "label": "Event Time", "value": "00:00 am", "isRequired": "true", "showCheckBox": true },
                                               { "label": "RSVP by", "value": "mm/dd/yyyy", "isRequired": "true", "showCheckBox": true }
                        ],
                        "customInfo": [

                                 { "label": "NMSL#", "value": "", "isRequired": "true", "showCheckBox": "true" },
                                          { "label": "Biography", "value": "", "isRequired": "true", "showCheckBox": "true" },
                                          { "label": "Toll-Free Number", "value": "", "isRequired": "true", "showCheckBox": "true" }
                        ],
                        "isEnabled": true,
                        "name": "companyInfo"
                    },
                     {
                         "companyInfo": [

                                  { "label": "First Name", "value": "Mike", "isRequired": "true", "showCheckBox": true },
                                                { "label": "Last Name", "value": "Stanley", "isRequired": "true", "showCheckBox": true },
                                                { "label": "Email", "value": "mstanley@aag.com", "isRequired": "true", "showCheckBox": true }

                         ],
                         "customInfo": [

                                  { "label": "NMSL#", "value": "", "isRequired": "true", "showCheckBox": "true" },
                                           { "label": "Biography", "value": "", "isRequired": "true", "showCheckBox": "true" },
                                           { "label": "Toll-Free Number", "value": "", "isRequired": "true", "showCheckBox": "true" }
                         ]
                                        ,
                         "isEnabled": true,
                         "name": "companyInfo"
                     }
                ];
            }
            else {
                //if (jobCustomerNumber == KUBOTA || jobCustomerNumber == GWA) {//jobCustomerNumber == DCA || 
                //    temp_cmp_info = [{
                //        "companyInfo": [
                //                            { "label": "Prefix", "value": "", "isRequired": "true", "showCheckBox": true },
                //                            { "label": "First Name", "value": "Anny", "isRequired": "true", "showCheckBox": true },
                //                            { "label": "Middle Name or Initial", "value": "B.", "isRequired": "true", "showCheckBox": true },
                //                            { "label": "Last Name", "value": "Vera", "isRequired": "true", "showCheckBox": true },
                //                            { "label": "Suffix", "value": "", "isRequired": "true", "showCheckBox": true }//,
                //                            //{ "label": "Business Phone", "value": "", "isRequired": "true", "showCheckBox": true },
                //                            //{ "label": "Website Address", "value": "TOWNCAREDENTAL.COM", "isRequired": "true", "showCheckBox": true }
                //        ],
                //        "customInfo": [
                //            { "label": "Doctor's Quote", "value": "My goal is to help improve your appearance, comfort, confidence, self-esteem and to offer you a better quality of life.", "isRequired": "true", "showCheckBox": "true" },
                //            { "label": "Biography", "value": "As a prosthodontist, Dr. Vera practices state-of-the-art dentistry. She is trained to integrate dental medicine with the treatment of the most challenging dental problems to deliver the highest level of dentistry to her patients. Dr. Vera’s commitment to excellence can improve your smile and overall health in a number of ways, from cosmetic dentistry to replacement and restoration. She received her postgraduate training in Prosthodontics and her Master of Science at the University of Maryland School of Dentistry. Dr. Vera is a former clinical assistant professor at the Department of Restorative Dentistry and Advanced General Dentistry at the University of Maryland School of Dentistry. She is a member of the honor society Phi Kappa Phi, the American College of Prosthodontics, American Academy of Facial Esthetics, Volusia Dental Association, and Florida Prosthodontic Association.", "isRequired": "true", "showCheckBox": "true" },
                //            { "label": "Description", "value": "Comprehensive, affordable dental care in a single, convenient, state-of-the-art office:", "isRequired": "true", "showCheckBox": "true" },
                //            { "label": "Description Bullets(Select up to six)", "value": "", "isRequired": "true", "showCheckBox": "true" },
                //            { "label": "Description Disclaimer", "value": "Every effort will be made to complete treatment at your initial appointment; however, two appoinments may be necessary.", "isRequired": "true", "showCheckBox": "true" },
                //            { "label": "Patient Quote", "value": "My experience here was great. Everyone was friendly and very helpful with the questions I had. Everything went smoothly, too.", "isRequired": "true", "showCheckBox": "true" },
                //            { "label": "Patient Name", "value": "Rachel P. (Current Towncare Dental Patient)", "isRequired": "true", "showCheckBox": "true" },
                //            { "label": "Disclaimer", "value": "* Must mention this offer to receive special pricing and begin treatment within 30 days of initial visit to receive discount. New, adult patients only. Includes comprehensive exam, all necessary x-rays as determined by provider, oral cancer screening, and basic routine cleaning. In the event gum disease is diagnosed, patients will receive a full-mouth debridement in lieu of cleaning. Further gum treatment may be indicated. This offer may not be combined with any other offer, discount, insurance, or reduced-fee program. Offer expires 60 days after receipt. ADA 0150, 0210, 0272, 0274, 0330, 1110, 4355. THE PATIENT AND ANY OTHER PERSON RESPONSIBLE FOR PAYMENT HAS A RIGHT TO REFUSE TO PAY, CANCEL PAYMENT, OR BE REIMBURSED FOR PAYMENT FOR ANY OTHER SERVICE, EXAM OR TREATMENT WHICH IS PERFORMED AS A RESULT OF AND WITHIN 72 HOURS OF RESPONDING TO THE ADVERTISEMENT FOR THE FREE, DISCOUNTED, REDUCED-FEE SERVICE, EXAMINATION OR TREATMENT. LIC#DN8580, LIC#DN13483, LIC#DN18083, LIC#DN17541, LIC#DN20050", "isRequired": "true", "showCheckBox": "true" }
                //        ],
                //        "isEnabled": true,
                //        "name": "companyInfo"
                //    }];
                //}
                //else {
                temp_cmp_info = [{

                   
                    "companyInfo": [{"label": "Company Name","value": "Test Company","isRequired": "true"}, 
                                    {"label": "Street Address","value": "North Genoa St","isRequired": "true"}, 
                                    {"label": "City","value": "Genoa","isRequired": "true"}, 
                                    {"label": "State","value": "Illinois","isRequired": "true"}, 
                                    {"label": "Zip","value": "58466","isRequired": "true"}, 
                                    {"label": "Business Phone","value": "(854) 45896214","isRequired": "true"}
                    ],
                    "customInfo": [{"label": "Website Address","value": "www.testcompany.com","isRequired": "true"}, 
                                   {"label": "License Number","value": "APJ56-8563-56-4","isRequired": "true"}, 
                                   {"label": "Dealer Tracking No. (Dealer Use Only)","value": "2546-865-54","isRequired": "true"}, 
                                   { "label": "Disclaimer or Additional Text", "value": "Test Disclaimer", "isRequired": "true" }],
                    "isEnabled": true,
                    "name": "companyInfo"


                    //"companyInfo": [
                    //                    { "label": "Company Name", "value": "Test Company", "isRequired": "true", "showCheckBox": true },
                    //                    { "label": "Street Address", "value": "North Genoa St", "isRequired": "true", "showCheckBox": true },
                    //                    { "label": "City", "value": "Genoa", "isRequired": "true", "showCheckBox": true },
                    //                    { "label": "State", "value": "Illinois", "isRequired": "true", "showCheckBox": true },
                    //                    { "label": "Zip", "value": "58466", "isRequired": "true", "showCheckBox": true },
                    //                    { "label": "Business Phone", "value": "(854) 45896214", "isRequired": "true", "showCheckBox": true }
                    //],
                    //"customInfo": [
                    //    { "label": "Website Address", "value": "", "isRequired": "true", "showCheckBox": "true" },
                    //    { "label": "License Number", "value": "", "isRequired": "true", "showCheckBox": "true" },
                    //    { "label": "Dealer Tracking No. (Dealer Use Only)", "value": "", "isRequired": "true", "showCheckBox": "true" },
                    //    { "label": "Disclaimer or Additional Text", "value": "", "isRequired": "true", "showCheckBox": "true" }
                    //],
                    //"isEnabled": true,
                    //"name": "companyInfo"
                }];
                //}
                //var temp_cmp_info = {
                //    "companyInfo": [
                //                        { "label": "Company Name", "value": "Test Company", "isRequired": "true", "showCheckBox": true },
                //                        { "label": "Street Address", "value": "North Genoa St", "isRequired": "true", "showCheckBox": true },
                //                        { "label": "City", "value": "Genoa", "isRequired": "true", "showCheckBox": true },
                //                        { "label": "State", "value": "Illinois", "isRequired": "true", "showCheckBox": true },
                //                        { "label": "Zip", "value": "58466", "isRequired": "true", "showCheckBox": true },
                //                        { "label": "Business Phone", "value": "(854) 45896214", "isRequired": "true", "showCheckBox": true }
                //    ],
                //    "customInfo": [
                //        { "label": "Website Address", "value": "", "isRequired": "true", "showCheckBox": "true" },
                //        { "label": "License Number", "value": "", "isRequired": "true", "showCheckBox": "true" },
                //        { "label": "Dealer Tracking No. (Dealer Use Only)", "value": "", "isRequired": "true", "showCheckBox": "true" },
                //        { "label": "Disclaimer or Additional Text", "value": "", "isRequired": "true", "showCheckBox": "true" }
                //    ],
                //    "isEnabled": true,
                //    "name": "companyInfo"
                //};
                var temp_base_info = {
                    "baseInfo": [],
                    "isEnabled": true,
                    "name": "baseSelection"
                };

                var temp_logo_info = {
                    "logoFile": {},
                    "optionalLogosSelected": [],
                    "isEnabled": true,
                    "name": "logoSelection"
                };
                var temp_offer_info = {
                    "offersList": [],
                    "isEnabled": true,
                    "name": "offerSetup"
                };
                var temp_select_images_info = {
                    "selectImagesList": [],
                    "isEnabled": true,
                    "name": "selectImages"
                };
            }
            if (self.gOutputData.companyInfoAction != undefined && self.gOutputData.companyInfoAction["inHomeDate"] == undefined && self.gOutputData.companyInfoAction["inHomeDate"] == null) {
                $.each(self.gOutputData, function (key, val) {
                    if (key.toLowerCase().indexOf('inhome') > -1) {
                        //custom text/company info
                        if (jobCustomerNumber == AAG) {
                            if (self.gOutputData.companyInfoAction["Event 1"] == undefined && self.gOutputData.companyInfoAction["Event 1"] == null)
                                self.gOutputData.companyInfoAction["Event 1"] = {};
                        }
                        if (jobCustomerNumber == AAG && (appPrivileges.roleName != "admin" && (sessionStorage.setUpTwoDate != undefined && sessionStorage.setUpTwoDate != null && sessionStorage.setUpTwoDate != "" && JSON.parse(sessionStorage.setUpTwoDate) == true))) {
                            if (self.gOutputData.companyInfoAction["Event 2"] == undefined && self.gOutputData.companyInfoAction["Event 2"] == null)
                                self.gOutputData.companyInfoAction["Event 2"] = {};
                        }
                        if (jobCustomerNumber != AAG && jobCustomerNumber != SPORTSKING && jobCustomerNumber != GWA && jobCustomerNumber != KUBOTA && jobCustomerNumber != BUFFALONEWS && jobCustomerNumber != PIONEERPRESS && jobCustomerNumber != REDPLUM)
                            if (self.gOutputData.companyInfoAction[val] == undefined && self.gOutputData.companyInfoAction[val] == null)
                                self.gOutputData.companyInfoAction[val] = {};

                        //select images
                        if (jobCustomerNumber == AAG) {
                            if (self.gOutputData.selectImagesAction["Event 1"] == undefined && self.gOutputData.selectImagesAction["Event 1"] == null)
                                self.gOutputData.selectImagesAction["Event 1"] = {};
                            if (jobCustomerNumber == AAG && (appPrivileges.roleName != "admin" && (sessionStorage.setUpTwoDate != undefined && sessionStorage.setUpTwoDate != null && sessionStorage.setUpTwoDate != "" && JSON.parse(sessionStorage.setUpTwoDate) == true))) {
                                if (self.gOutputData.selectImagesAction["Event 2"] == undefined && self.gOutputData.selectImagesAction["Event 2"] == null)
                                    self.gOutputData.selectImagesAction["Event 2"] = {};
                            }
                        }
                        //else if (jobCustomerNumber == KUBOTA || jobCustomerNumber == GWA) {//jobCustomerNumber == SPORTSKING || 
                        //    if (self.gOutputData.selectImagesAction[val] == undefined && self.gOutputData.selectImagesAction[val] == null)
                        //        self.gOutputData.selectImagesAction[val] = {};
                        //}
                        //base selection
                        if (jobCustomerNumber == ALLIED) {
                            if (self.gOutputData.baseSelectionAction[val] == undefined && self.gOutputData.baseSelectionAction[val] == null)
                                self.gOutputData.baseSelectionAction[val] = {};
                            if (self.gOutputData.logoSelectionAction[val] == undefined && self.gOutputData.logoSelectionAction[val] == null)
                                self.gOutputData.logoSelectionAction[val] = {};
                        }
                        //offer setup
                        if (jobCustomerNumber == ALLIED) {// || jobCustomerNumber == SPORTSKING || jobCustomerNumber == DCA
                            if (self.gOutputData.offerSetupAction[val] == undefined && self.gOutputData.offerSetupAction[val] == null)
                                self.gOutputData.offerSetupAction[val] = {};
                        }
                        $.each(self.gOutputData.selectedLocationsAction.selectedLocationsList, function (loc_key, loc_val) {
                            //company info/custom text setup
                            if (jobCustomerNumber == AAG && (appPrivileges.roleName != "admin" && (sessionStorage.setUpTwoDate != undefined && sessionStorage.setUpTwoDate != null && sessionStorage.setUpTwoDate != "" && JSON.parse(sessionStorage.setUpTwoDate) == true))) {
                                self.gOutputData.companyInfoAction["Event 1"][loc_val.pk] = temp_cmp_info;
                                self.gOutputData.companyInfoAction["Event 2"][loc_val.pk] = temp_cmp_info;
                            }
                            else if (jobCustomerNumber == AAG && (appPrivileges.roleName != "admin" && (sessionStorage.setUpTwoDate == undefined || sessionStorage.setUpTwoDate == null || sessionStorage.setUpTwoDate == "" || JSON.parse(sessionStorage.setUpTwoDate) == false))) {
                                self.gOutputData.companyInfoAction["Event 1"][loc_val.pk] = temp_cmp_info;
                            }
                            else {
                                if (jobCustomerNumber != SPORTSKING && self.gOutputData.companyInfoAction[val][loc_val.pk] == undefined && self.gOutputData.companyInfoAction[val][loc_val.pk] == null)
                                    self.gOutputData.companyInfoAction[val][loc_val.pk] = temp_cmp_info;
                            }
                            //select images setup
                            if (jobCustomerNumber == AAG && (appPrivileges.roleName != "admin" && (sessionStorage.setUpTwoDate != undefined && sessionStorage.setUpTwoDate != null && sessionStorage.setUpTwoDate != "" && JSON.parse(sessionStorage.setUpTwoDate) == true))) {
                                self.gOutputData.selectImagesAction["Event 1"][loc_val.pk] = temp_select_images_info;
                                self.gOutputData.selectImagesAction["Event 2"][loc_val.pk] = temp_select_images_info;
                            }
                            else if (jobCustomerNumber == AAG && (appPrivileges.roleName != "admin" && (sessionStorage.setUpTwoDate == undefined || sessionStorage.setUpTwoDate == null || sessionStorage.setUpTwoDate == "" || JSON.parse(sessionStorage.setUpTwoDate) == false))) {
                                self.gOutputData.selectImagesAction["Event 1"][loc_val.pk] = temp_select_images_info;
                            }
                            //else if (jobCustomerNumber == KUBOTA || jobCustomerNumber == GWA) {//|| jobCustomerNumber == SPORTSKING || jobCustomerNumber == DCA || 
                            //    if (self.gOutputData.selectImagesAction[val][loc_val.pk] == undefined && self.gOutputData.selectImagesAction[val][loc_val.pk] == null)
                            //        self.gOutputData.selectImagesAction[val][loc_val.pk] = temp_select_images_info;
                            //}
                            //base selection setup
                            if (jobCustomerNumber == ALLIED) {
                                if (self.gOutputData.baseSelectionAction[val][loc_val.pk] == undefined && self.gOutputData.baseSelectionAction[val][loc_val.pk] == null)
                                    self.gOutputData.baseSelectionAction[val][loc_val.pk] = temp_base_info;
                                if (self.gOutputData.logoSelectionAction[val][loc_val.pk] == undefined && self.gOutputData.logoSelectionAction[val][loc_val.pk] == null)
                                    self.gOutputData.logoSelectionAction[val][loc_val.pk] = temp_logo_info;
                            }
                            //offer setup
                            if (jobCustomerNumber == ALLIED) {// || jobCustomerNumber == DCA || jobCustomerNumber == SPORTSKING
                                if (self.gOutputData.offerSetupAction[val][loc_val.pk] == undefined && self.gOutputData.offerSetupAction[val][loc_val.pk] == null)
                                    self.gOutputData.offerSetupAction[val][loc_val.pk] = temp_offer_info;
                            }
                        });
                    }
                });
            }
        }
        if (sessionStorage.updateLocationsList != undefined && sessionStorage.updateLocationsList != null && sessionStorage.updateLocationsList !== "")
            self.updateLocations = $.parseJSON(sessionStorage.updateLocationsList);
        else
            self.updateLocations = [];
        if (self.updateLocations.length > 0 /*&& self.gOutputData.selectedLocationsAction.selectedLocationsList.length > 0*/) {
            message = "locations not saved";
        }

        if ((jobCustomerNumber == DCA || jobCustomerNumber == KUBOTA || jobCustomerNumber == SPORTSKING || jobCustomerNumber == GWA || jobCustomerNumber == BUFFALONEWS || jobCustomerNumber == PIONEERPRESS || jobCustomerNumber == REDPLUM) && (self.gOutputData.selectedLocationsAction.selectedLocationsList == undefined || self.gOutputData.selectedLocationsAction.selectedLocationsList == null || self.gOutputData.selectedLocationsAction.selectedLocationsList.length == 0)) {
            //message = "No locations have been selected for this order. You will not be able to create a mailing list without selecting a location. Do you want to Continue?";
            message = "no locations";
        }
        //if (jobCustomerNumber == CW || jobCustomerNumber == BRIGHTHOUSE || jobCustomerNumber == BBB) {
        //    $.each(self.gOutputData.selectedLocationsAction.selectedLocationsList, function (key, obj) {
        //        obj.quantity = ($('#' + obj.pk + '_quantity').val() != null && jQuery.trim($('#' + obj.pk + '_quantity').val()) != "") ? parseInt($('#' + obj.pk + '_quantity').val().replace(/,/g, '')) : "";
        //        if (obj.quantity == null || obj.quantity == "") {
        //            //message = "Please enter estimated quantity for added locations.";
        //            message = "quantity";
        //        }
        //        else if (obj.quantity > 10000000) // maximum allowed quantity  is 10,000,000. Charlie mail dated 22nd May'14
        //            //message = "Maximum allowable quantity is '10,000,000'.";
        //            message = "qty limit";
        //    });
        //}
        return message;
    };


    self.checkLocationsSelected = function (navigate_page, source, current_page, page_name, i_count, click_type) {
        var message = self.validateLocations();
        if (message == "no locations" && click_type != "save") {
            return getNoLocationsConfirmation(navigate_page, source, current_page, page_name, i_count, click_type);
        }
        else if (message == "locations not saved" && click_type != "save") {
            var message = "Changes to the Ordered Locations have not been saved. Please save or discard your changes now.";
            $('#confirmMsg').html(message);
            $('#okButConfirm').text('Save');
            disableSaveForClosedJobs();
            $('#cancelButConfirm').text('Discard');
            $('#okButConfirm').bind('click', function () {
                $('#cancelButConfirm').unbind('click');
                $('#okButConfirm').unbind('click');
                $('#cancelButConfirm').attr('onclick', '$(\'#popupConfirmDialog\').popup(\'close\');');
                $('#okButConfirm').text('Ok');
                $('#cancelButConfirm').text('Cancel');
                self.updateLocations = [];
                postJobSetUpData('save');
                $('#popupConfirmDialog').popup('close');
                setTimeout(function () {
                    self.updateStoreAndInHomeDate(navigate_page, "");
                }, 500);
            });
            $('#cancelButConfirm').bind('click', function () {
                $('#cancelButConfirm').unbind('click');
                $('#okButConfirm').unbind('click');
                $('#cancelButConfirm').attr('onclick', '$(\'#popupConfirmDialog\').popup(\'close\');');
                $('#okButConfirm').unbind('click');
                $('#okButConfirm').text('Ok');
                $('#cancelButConfirm').text('Cancel');
                sessionStorage.removeItem("updateLocationsList");
                sessionStorage.removeItem('tempSelectedStores');
                self.updateLocations = [];
                getCORS(serviceURLDomain + "api/Locations_dropDown/" + appPrivileges.facility_id + "/" + appPrivileges.customerNumber + "/" + self.gOutputData.jobNumber + "/0/2/" + self.gOutputData.jobTypeId, null, function (data) {
                    self.gOutputData.selectedLocationsAction.selectedLocationsList = [];
                    if (data.locationsSelected != undefined && data.locationsSelected != null && data.locationsSelected != "")
                        self.gOutputData.selectedLocationsAction.selectedLocationsList = data.locationsSelected;
                }, function (error_response) {
                    showErrorResponseText(error_response, true);
                });
                $('#popupConfirmDialog').popup('close');
                setTimeout(function () {
                    self.updateStoreAndInHomeDate(navigate_page, "");
                }, 500);
            });
            $('#popupConfirmDialog').popup('open');
            return false;
        }
        else {
            var msg = '';
            if (message == "quantity")
                msg = "Please enter estimated quantity for added locations.";
            else if (message == "qty limit")
                msg = "Maximum allowable quantity is '10,000,000'.";
            if (msg != '') {
                $('#alertHeader').text("Alert");
                $('#alertmsg').text(msg);
                $('#popupDialog').popup().popup('open');
                return false;
            }
            else
                return self.updateStoreAndInHomeDate(navigate_page, source);
        }
    }

    var isPopedUp = false;
    var userAction = "";
    var getNoLocationsConfirmation = function (navigate_page, source, current_page, page_name, i_count, click_type) {
        if (!isPopedUp) {
            var message = "No locations have been selected for this order. You will not be able to create a mailing list without selecting a location. Do you want to Continue?";
            $('#confirmMsg').html(message);
            $('#okButConfirm').text('Continue');
            $('#cancelButConfirm').text('Cancel');
            $('#cancelButConfirm').bind('click', function () {
                $('#cancelButConfirm').unbind('click');
                $('#okButConfirm').unbind('click');
                $('#cancelButConfirm').attr('onclick', '$(\'#popupConfirmDialog\').popup(\'close\');');
                $('#popupConfirmDialog').popup('close');
                isPopedUp = false;
                userAction = "";                //return false;
            });
            $('#okButConfirm').bind('click', function () {
                $('#popupConfirmDialog').popup('close');
                $('#cancelButConfirm').unbind('click');
                $('#okButConfirm').unbind('click');
                $('#cancelButConfirm').attr('onclick', '$(\'#popupConfirmDialog\').popup(\'close\');');
                $('#okButConfirm').unbind('click');
                userAction = "";
                //if (jobCustomerNumber == SPORTSKING) {
                //    self.gOutputData.selectedLocationsAction.selectedLocationsList.push({
                //        "storeName": "Test Store1",
                //        "address1": "1132 PLEASANT ST STE C",
                //        "address2": "1132 PLEASANT ST STE C",
                //        "city": "DEKALB",
                //        "state": "IL",
                //        "zip": "60115",
                //        "latitude": "41.932802870000",
                //        "longitude": "-88.738168640000",
                //        "pk": "15430",
                //        "needsCount": "true",
                //        "firstName": "Mike",
                //        "lastName": "Stanley",
                //        "storeId": ""
                //    });
                //    self.validateLocations();
                //}
                if (sessionStorage.updateLocationsList != undefined && sessionStorage.updateLocationsList != null && sessionStorage.updateLocationsList !== "") {
                    postJobSetUpData('save');
                    self.updateLocations = [];
                }
                setTimeout(function () {
                    //navigateTo(true, current_page, page_name, i_count, click_type);
                    self.updateStoreAndInHomeDate(navigate_page, "");
                }, 500);
                //return true;
                isPopedUp = false;
            });
            isPopedUp = true;
            $('#popupConfirmDialog').popup('open');
            return false;
        }
        if (isPopedUp && userAction == "") {
            setTimeout(getNoLocationsConfirmation(callback, navigate_page, source), 0);
        }
    };
    ////Verifies the selected locations.
    //self.checkLocationsSelected = function (navigate_page, source) {
    //    //if (pagePrefs.scriptBlockDict != null) {
    //    var message = self.validateLocations();
    //    if (message != "") {
    //        if ((jobCustomerNumber == DCA || jobCustomerNumber == KUBOTA) && self.gOutputData.selectedLocationsAction.selectedLocationsList.length == 0) {
    //            //setTimeout(function () {

    //            $('#confirmMsg').html(message);
    //            $('#okButConfirm').text('Continue');
    //            $('#cancelButConfirm').text('Cancel');
    //            $('#cancelButConfirm').bind('click', function () {
    //                $('#cancelButConfirm').unbind('click');
    //                $('#okButConfirm').unbind('click');
    //                $('#cancelButConfirm').attr('onclick', '$(\'#popupConfirmDialog\').popup(\'close\');');
    //                $('#popupConfirmDialog').popup('close');
    //                return false;
    //            });
    //            $('#okButConfirm').bind('click', function () {
    //                $('#cancelButConfirm').unbind('click');
    //                $('#okButConfirm').unbind('click');
    //                //$('#cancelButConfirm').attr('onclick', '$(\'#popupConfirmDialog\').popup(\'close\');');
    //                $('#okButConfirm').unbind('click');
    //                self.gOutputData.selectedLocationsAction.selectedLocationsList.push({
    //                    "storeName": "Test Store1",
    //                    "address1": "1132 PLEASANT ST STE C",
    //                    "address2": "1132 PLEASANT ST STE C",
    //                    "city": "DEKALB",
    //                    "state": "IL",
    //                    "zip": "60115",
    //                    "latitude": "41.932802870000",
    //                    "longitude": "-88.738168640000",
    //                    "pk": "15430",
    //                    "needsCount": "true",
    //                    "firstName": "Mike",
    //                    "lastName": "Stanley",
    //                    "storeId": ""
    //                });
    //                $('#popupConfirmDialog').popup('close');
    //                clearInterval(my_timer);
    //                return self.updateStoreAndInHomeDate(navigate_page, source);
    //            });
    //            $('#popupConfirmDialog').popup('open');
    //            //return false;
    //        }
    //        else {
    //            $('#alertHeader').text("Alert");
    //            $('#alertmsg').text(message);
    //            $('#popupDialog').popup().popup('open');
    //            return false;
    //        }
    //    }
    //    else {
    //        return self.updateStoreAndInHomeDate(navigate_page, source);
    //    }
    //};

    //Updates InhomeDate and jobSetupOutput
    self.updateStoreAndInHomeDate = function (navigate_page, source) {

        if ((jobCustomerNumber == CASEYS || jobCustomerNumber == JETS || jobCustomerNumber == BUFFALONEWS || jobCustomerNumber == PIONEERPRESS || jobCustomerNumber == REDPLUM || jobCustomerNumber == EQUINOX || isUpdateInHomeDate()) && self.gOutputData.selectedLocationsAction.selectedLocationsList != undefined) {
            $.each(self.gOutputData.selectedLocationsAction.selectedLocationsList, function (key, obj) {
                obj.inHomeDate = $('#' + obj.pk + '_inHomeDate').val();
            });
        }

        if (self.gOutputData.selectedLocationsAction != undefined && self.gOutputData.selectedLocationsAction != null && self.gOutputData.selectedLocationsAction.selectedLocationsList != undefined && self.gOutputData.selectedLocationsAction.selectedLocationsList != null && self.gOutputData.selectedLocationsAction.selectedLocationsList.length > 0) {
            $.each(self.gOutputData.selectedLocationsAction.selectedLocationsList, function (key, obj) {
                if (obj.isLatLngExists) delete obj.isLatLngExists;
                if (obj.isNewStore) delete obj.isNewStore;
            });
        }

        sessionStorage.jobSetupOutput = JSON.stringify(self.gOutputData);
        self.updateLocations = [];
        //sessionStorage.removeItem("firstTimeInLocations");
        if (source == "") {
            if ((jobCustomerNumber == REDPLUM || jobCustomerNumber == PIONEERPRESS || jobCustomerNumber == BUFFALONEWS) && sessionStorage.navGroupCount == undefined) {
                sessionStorage.navGroupCount = 5;
            }                  
            if (navigate_page.toLowerCase().indexOf('jobsummary') == -1) {
                window.location.href = navigate_page;
            }
            else
                return true;
        }
        else
            return true; //while clicking on Save & continue button need to increase session in jobNavLinks page.
    }

    //Validates the location profile fields based on its type and needToValidate attributes.
    self.controlToValidate = function () {
        var isValid = true;
        var alert_message = "";
        var field_value = "";
        var spanish_french_regex = /^[ A-Za-z0-9A-Za-z\u00C0-\u017F_@'.\/#&+,-\][()]*$/;
        var validation_regrx = {
            'email': /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/,
            'alpha': /^[a-zA-Z ]*$/,
            'city': /^[a-zA-Z.\-\/\ ]*$/,
            'alphaNum': /^[ A-Za-z0-9_@'.\/#&+,-\][()]*$/,
            'zip': /(^\d{5}$)|(^\(?(\d{5})[- ]?(\d{4})$)/,
            //'phone': /^\(?(\d{3})[- ]?(\d{4})$/,
            //'phone': /^((\d{3}-\d{4})|((\d{3}\s){0,1}\d{3}\s\d{4})|((\d{3}\.){0,1}\d{3}\.\d{4})|((\d{3}\-){0,1}\d{3}\-\d{4})|((\(\d{3}\) ){0,1}\d{3}-\d{4}))$/,
            'latitude': /^-?([1-8]?[1-9]|[1-9]0)\.{1}\d{1,12}/,
            //'longitude': /^-?([1]?[1-7][1-9]|[1]?[1-8][0]|[1-9]?[0-9])\.{1}\d{1,12}/,
            'longitude': /^-{1,3}\d*\.{0,1}\d+$/,
            'areaCode': /^[0-9]{3}$/
        };
        for (cnt = 0; cnt < self.validationControls.length; cnt++) {
            var control = self.validationControls[cnt];
            var is_required = (control.name.search("State") != -1) ? $('#ddlProfileState').parent().parent().parent().attr('data-isRequired') : $('#' + control.ctrlid).attr('data-isRequired');
            var valide = $('#' + control.ctrlid).attr('validate');
            if (is_required == "true") {
                if (control.name.search("State") != -1) {
                    field_value = $('#ddlProfileState').val();
                    if (field_value == "") {
                        alert_message += (alert_message != '') ? "<br/>Please select " + control.name + "." : "Please select " + control.name + ".";
                    }
                }
                else if ($('#' + control.ctrlid)[0].tagName.toLowerCase() == "select") {
                    if ($('#' + control.ctrlid).val() == "-1")
                        alert_message += (alert_message != "") ? "<br/>Please select " + control.name : "Please select " + control.name;
                }
                else if ($('#' + control.ctrlid).val() == "" || $('#' + control.ctrlid).val() == $('#' + control.ctrlid).attr('placeholder'))
                    alert_message += (alert_message != "") ? "<br/>Please enter " + control.name : "Please enter " + control.name;
            }

            if (valide != '') {
                field_value = $('#' + control.ctrlid).val();
                if (field_value == $('#' + control.ctrlid).attr('placeholder'))
                    field_value = '';

                var reg_ex = '';
                for (var key in validation_regrx) {
                    if (key == valide) {
                        reg_ex = validation_regrx[key];
                        break;
                    }
                }
                if (field_value != '' && reg_ex != "") {
                    if ($('#' + control.ctrlid).attr('needSpanishFrenchCheck') ? !spanish_french_regex.test(field_value) : !reg_ex.test(field_value))
                        alert_message += (alert_message != "") ? "<br/>Please enter valid " + control.name + "." : "Please enter valid " + control.name + ".";
                }
            }

        }
        return alert_message;
    };
};