﻿var locationConfig = function (self) {
    //Loads the location profile configuration into page scoped object and creates the profile page.
    self.loadLocationConfig = function () {
        //if (sessionStorage.locationConfig == undefined || sessionStorage.locationConfig == null || sessionStorage.locationConfig == "") {
        if ((sessionStorage.locationConfig == undefined || sessionStorage.locationConfig == null || sessionStorage.locationConfig == "") || (appPrivileges.customerNumber != jobCustomerNumber)) {
            var location_config_url = serviceURLDomain + 'api/Locations_config/' + jobCustomerNumber;
            getCORS(location_config_url, null, function (data) {
                sessionStorage.locationConfig = JSON.stringify(data);
                self.createLocationInfoPopUp(data);
            }, function (error_response) {
                showErrorResponseText(error_response, true);
            });
        }
        else {
            var loc_config = jQuery.parseJSON(sessionStorage.locationConfig);
            self.createLocationInfoPopUp(loc_config);
        }
    };

    //Creates the fields to be displayed based on the loaded locaton profile configuration into location profile popup.
    self.createLocationInfoPopUp = function (data) {
        var loc_popup_fields = "";
        var index = 0;
        var total_fields_count = 0;
        var hidden_field_count = (data.hidden != undefined && data.hidden != null && data.hidden != "" && (Object.keys(data.hidden)).length != undefined && (Object.keys(data.hidden)).length != null) ? (Object.keys(data.hidden)).length : 0;
        var div_count = (data.type != undefined && data.type != null && data.type != "" && (Object.keys(data.type)).length > 0) ? Math.round(((Object.keys(data.type)).length - hidden_field_count) / 2) : 0;
        var required_fields = data.needsValidation;
        var col_left = "";
        var col_right = "";
        $.each(data.type, function (key, val) {
            if (data.hidden[key] == undefined || data.hidden[key] == null) {
                var name = self.getFieldDisplayName(key);
                var attribute = "";
                var needSpanishFrenchCheck = false;
                switch (val) {
                    case "string":
                    case "int":
                    case "double":
                        loc_popup_fields += '<fieldset data-role="controlgroup">';
                        self.getAllControlType('lbl' + key, val, name);
                        if ((name.search("Zip") != -1) || (name.search("Phone") != -1) || (name.search("Latitude") != -1) || (name.search("Longitude") != -1) || (name.search("Area") != -1)) {
                            attribute = key;
                        }
                        else if (name.search("City") != -1) {
                            if (name == "City")
                                attribute = 'city';
                            else
                                attribute = 'alpha';
                        }
                        else {
                            attribute = 'alphaNum';
                        }
                        //loc_popup_fields += '<label for="txt' + key + '"><span style="font-size: 14px; font-weight: normal;">' + data.name[key] + '</span></label>';
                        if (!featureLocationProfileUpdate()) {
                            loc_popup_fields += '<span style="font-size: 14px; font-weight: bold;">' + data.name[key] + ': </span></br>';
                            loc_popup_fields += '<span name="lbl' + key + '" id="lbl' + key + '"  maxlength = "' + data.length[key] + '" class="word_wrap" data-mini="true" data-bind="text:$root.editLocationInfo().' + key + '"></span>';
                        }
                        else {
                            if (key == "hasSubs" || key == "hasDelivery" || key == "newConstruction") {
                                loc_popup_fields += '<span style="font-size: 14px; font-weight: normal; padding-top: 55px">' + data.name[key] + '</span>'
                                loc_popup_fields += '<select name="ddl' + key + '" id="ddl' + key + '" data-mini="true" data-bind="value:$root.editLocationInfo().' + key + '" data-name ="' + name + '" data-isRequired="' + required_fields[key] + '">';
                                loc_popup_fields += '<option value="-1">-- Select -- </option>';
                                loc_popup_fields += '<option value="True">True</option>';
                                loc_popup_fields += '<option value="False">False</option>';
                                loc_popup_fields += '</select>';
                                self.getAllControlType('ddl' + key, val, name);
                            }
                            else if (key != 'state') {
                                self.getAllControlType('txt' + key, val, name);
                                if ((name.search("Zip") != -1) || (name.search("Phone") != -1) || (name.search("Latitude") != -1) || (name.search("Longitude") != -1) || (name.search("Area") != -1)) {
                                    attribute = key;
                                }
                                else if (name.search("City") != -1) {
                                    if (name == "City")
                                        attribute = 'city';
                                    else
                                        attribute = 'alpha';
                                }
                                else {
                                    attribute = 'alphaNum';
                                }
                                if ((name.search('Location') > -1) || (name.search('City') > -1) || (name.search('Address1') > -1) || (name.search('Address2') > -1)) {
                                    needSpanishFrenchCheck = true;
                                }

                                //loc_popup_fields += '<label for="txt' + key + '"><span style="font-size: 14px; font-weight: normal;">' + data.name[key] + '</span></label>';
                                loc_popup_fields += '<span style="font-size: 14px; font-weight: normal;">' + data.name[key] + '</span>';
                                loc_popup_fields += '<input type="text" needSpanishFrenchCheck="' + needSpanishFrenchCheck + '" validate="' + attribute + '" name="txt' + key + '" id="txt' + key + '"  data-isRequired="' + required_fields[key] + '"  value="" maxlength = "' + data.length[key] + '" placeholder="' + name + '" data-mini="true" data-bind="value:$root.editLocationInfo().' + key + '"/>';

                            }
                            else {
                                self.getAllControlType('txt' + key, val, name);
                                loc_popup_fields += '<span style="font-size: 14px; font-weight: normal; padding-top: 55px">' + data.name[key] + '</span>'
                                loc_popup_fields += '<div style="padding-top: 8px" data-isRequired="' + required_fields[key] + '">'
                                loc_popup_fields += '<!-- ko template: { name: \'stateTemplate\'} --><!-- /ko -->';
                                //loc_popup_fields += '<select name="ddlProfileState" id="ddlProfileState" data-mini="true" data-bind="options:$root.states, optionsText:\'stateName\', optionsValue:\'stateValue\',value:$root.editLocationInfo().state">';
                                //loc_popup_fields += '<select name="ddlProfileState" id="ddlProfileState" data-mini="true" class="samplecss" ><option value="1">One</option>';

                                //loc_popup_fields += '</select>';
                                loc_popup_fields += '</div>';
                            }
                        }
                        
                        loc_popup_fields += '</fieldset>';
                        break;
                }
                if (div_count == 1 && index > 0 && index == div_count) {
                    col_left = loc_popup_fields;
                    $('#dvLocationLeft').html(col_left);
                    loc_popup_fields = "";
                    index = 0;
                }
                else if (index > 0 && (index == (div_count - 1)) || (total_fields_count == ((Object.keys(data.type).length - hidden_field_count) - 1))) {
                    if (col_left != "") {
                        col_right = loc_popup_fields;
                        $('#dvLocationRight').html(col_right);
                    }
                    else {
                        col_left = loc_popup_fields;
                        $('#dvLocationLeft').html(col_left);
                    }
                    loc_popup_fields = "";
                    total_fields_count++;
                    index = 0;
                }
                else {
                    index++;
                    total_fields_count++;
                }
            }
        });
        var fields_grid = '';
        var col_items = '';
        var has_two_cols = false;
        if (col_left != "" && col_right != "") {
            fields_grid = '<div class="ui-grid-c ui-responsive" id="dvLocationsFields" style="max-height: 500px; overflow-y:scroll;">';
            col_items = '<div class="ui-block-a" style="width: 45%"><div style="padding: 0px 4px 0px 4px" id="dvLocationLeft">' + col_left + '</div></div>';
            col_items += '<div class="ui-block-b" style="width: 10%"><div></div></div>';
            col_items += '<div class="ui-block-c" style="width: 45%"><div style="padding: 0px 4px 0px 4px" id="dvLocationRight">' + col_right + '</div></div>';
            has_two_cols = true;
        }
        else {
            fields_grid = '<div class="ui-grid-a ui-responsive" id="dvLocationsFields">';
            col_items = '<div class="ui-block-a" style="width: 100%"><div style="padding: 0px 4px 0px 4px" id="dvLocationLeft">' + col_left + '</div></div>';
        }
        fields_grid = fields_grid + col_items + '</div>';
        if (has_two_cols)
            $('#popUpLocationProfile').css('style', 'width: 150%');
        else
            $('#popUpLocationProfile').css('style', 'width: 75%');
        $(fields_grid).insertAfter($('#popUpLocationProfile').find('div[id="dvStoreProfile"] h3'));
        //var user_text = '<div class="ui-grid-solo"><div class="ui-block-a"><span style="font-size:12px;" class="word_wrap">Profile information for Ordered Locations cannot be edited unless<br />it is removed from the order first.</span></div></div>';
        ////<span style="font-size:12px;" class="word_wrap">Profile information for Ordered Locations cannot be edited unless it is removed from the order first.</span>
        //$(user_text).insertBefore($('#popUpLocationProfile').find('#dvLocationsFields'));
        $('#dvStoreProfile').trigger('create');
        $('#dvStoreProfile').find('select').selectmenu('refresh');

    };

    //Converts the text of camel case into normal text with starting character capital in each word and includes the space between words.
    self.getFieldDisplayName = function (page_name) {
        var PATTERN = /^[ABCDEFGHIJKLMNOPQRSTUVWXYZ]/;
        var page_display_name = '';
        $.each(new String(page_name), function (key, value) {
            if (key > 0 && value.match(PATTERN)) {
                page_display_name += ' ' + value;
            }
            else {
                page_display_name += value;
            }
        });
        return page_display_name.initCap();
    };

    //Gets all the controls to be validated into validationControl object.
    self.getAllControlType = function (ctrl_id, val_type, name) {
        var controls = { 'ctrlid': ctrl_id, 'type': val_type, 'name': name };
        self.validationControls.push(controls);
    };
};
