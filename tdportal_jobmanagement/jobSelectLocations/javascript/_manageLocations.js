﻿var manageLocations = function (self) {
    //Addes the location to ordered locations when user clicks a 'Add to Order' link in the marker bubble infowindow  / "Add to Order" button in location profile.
    self.addToOrder = function (data, event) {
        var error_msg = self.controlToValidate();
        if (error_msg != '') {
            $('#popUpLocationProfile').popup().popup('close');
            if (error_msg != "") {
                $('#alertmsg').html(error_msg);
                $('#popUpConfirmChange').popup().popup('close');
                $("#popupDialog").popup().popup('open');
                $('#okBut').unbind('click');
                $("#okBut").bind('click', function () {
                    $('#popupDialog').popup().popup('close');
                    window.setTimeout(function openPopSearch() {
                        $('#popUpLocationProfile').popup().popup('open');
                    }, 100);
                });
                //$("#okBut").attr("onclick", "$('#popupDialog').popup('close');$('#popUpLocationProfile').popup().popup('open');");
            }
            return false;
        }
        var temp_editInfo = $.extend(true, {}, self.editLocationInfo());
        var is_with_in_50_miles = self.validateWithIn50MilesOrNot(temp_editInfo);
        temp_editInfo = ko.mapping.toJS(temp_editInfo, mapping);
        $('#btnNo').attr('selectedIndex', $('#btnProfilePopupCancel').attr('selectedIndex'));
        $('#popUpLocationProfile').popup().popup('close');

        if (jobCustomerNumber == CW || jobCustomerNumber == BRIGHTHOUSE || jobCustomerNumber == BBB || is_with_in_50_miles) {
            if (JSON.stringify(temp_editInfo) !== JSON.stringify(self.locationPreviousInfo())) {
                $('#popUpConfirmChange').popup().popup('open');
            }
            else
                self.addSelectedStore(self.editLocationInfo(), event);
        }
        else
            $('#popupClearListSelectionsConfirm').popup().popup('open', { positionTo: 'window' });
    };

    //update selected location information.
    self.updateLocation = function (data, event) {
        var click_type = $(event.currentTarget).attr('data-clickType');
        if ($('#btnPopUpAddUpdate').text().indexOf('Remove') > -1) {
            $('#popUpLocationProfile').popup().popup('close');
            self.addLocationBack(self.editLocationInfo());
            return false;
        }
        var error_msg = self.controlToValidate();
        if (error_msg != '') {
            $('#popUpLocationProfile').popup().popup('close');
           if (error_msg != "") {
                $('#alertmsg').html(error_msg);
                $('#popUpConfirmChange').popup().popup('close');
                $("#popupDialog").popup().popup('open');
                $('#okBut').unbind('click');
                $("#okBut").bind('click', function () {
                    $('#popupDialog').popup().popup('close');
                    window.setTimeout(function openPopSearch() {
                        $('#popUpLocationProfile').popup().popup('open');
                    }, 200);
                });
                //$("#okBut").attr("onclick", "$('#popupDialog').popup('close');$('#popUpLocationProfile').popup().popup('open');");
            }
            return false;
        }
        var temp_location = ko.mapping.toJS(self.editLocationInfo(), mapping);
        delete temp_location.isQuantityRequired;
        delete temp_location.computedStoreName;
        delete temp_location.storeCompleteAddress;
        var location_save_url = serviceURLDomain + 'api/Locations_post/' + jobCustomerNumber

        if (click_type == "new") {
            if ((jobCustomerNumber != CW && jobCustomerNumber != BRIGHTHOUSE && jobCustomerNumber != BBB ) && self.locationsNeedToReview.length == 0 && !self.validateWithIn50MilesOrNot(self.editLocationInfo())) {
                $('#popUpLocationProfile').popup().popup('close');
                $('#btnClearListSelections').attr('data-clickType', "new");
                $('#popupClearListSelectionsConfirm').popup().popup('open', { positionTo: 'window' });
                return false;
            }
        }
        switch (click_type) {
            case "existing-no":
            case "no-countsClear":
                $.each(self.locationPreviousInfo(), function (key, val) {
                    if (key.indexOf('mapping') == -1 && val.getDependenciesCount == undefined && val.getDependenciesCount == null)
                        self.editLocationInfo()[key](val)
                });
                var selected_index = $('#btnNo').attr('selectedIndex');
                if (selected_index != undefined && selected_index != null && selected_index != "") {
                    $.each(self.editLocationInfo(), function (key, val) {
                        if (key.indexOf('mapping') == -1 && val.getDependenciesCount == undefined && val.getDependenciesCount == null)
                            if (key != "locationIndex")
                                self.locations()[selected_index][key](val())
                            else
                                self.locations()[selected_index][key] = val;
                    });
                    $('#btnNo').removeAttr('selectedIndex');
                    if (click_type == "no-countsClear") {
                        $('#popupClearListSelectionsConfirm').popup().popup('close');
                        self.locationsNeedToReview = []
                    }
                    else {
                        $('#popUpConfirmChange').popup().popup('close');
                        $('#dvStoreProfile').find('select').selectmenu().selectmenu('refresh');
                        setTimeout(function createStateDDL() {
                            $('#ddlProfileState').trigger('create');
                            var config_json = $.parseJSON(sessionStorage.locationConfig).type;
                            if (config_json["hasSubs"] != undefined)
                                $('#ddlhasSubs').trigger('create');
                            if (config_json["hasDelivery"] != undefined)
                                $('#ddlhasDelivery').trigger('create');
                            if (config_json["newConstruction"] != undefined)
                                $('#ddlnewConstruction').trigger('create');
                        }, 2000);
                        $('#popUpLocationProfile').popup().popup('open');
                    }
                }
                else {
                    if (click_type == "no-countsClear" || click_type == "existing-no") {
                        $('#popupClearListSelectionsConfirm').popup().popup('close');
                        $('#popUpConfirmChange').popup().popup('close');
                    }
                }
                break;
            case "countsClear":
                $('#popupClearListSelectionsConfirm').popup().popup('open');
                $('#popupClearListSelectionsConfirm').popup().popup('close');
                var temp_locatin = ko.mapping.toJS(self.locationPreviousInfo, mapping);
                if (!temp_locatin.isNewStore && Object.keys(ko.mapping.toJS(self.locationPreviousInfo, mapping)).length > 0 && JSON.stringify(temp_location) !== JSON.stringify(self.locationPreviousInfo()))
                    $('#popUpConfirmChange').popup().popup('open');
                else {
                    if (!temp_locatin.isNewStore)
                        $(event.currentTarget).attr('data-clickType', "");
                    self.addSelectedStore(self.editLocationInfo(), event);
                }
                break;
            case "existing":
            case "newClearCounts":
            case "new":
                var temp_selected_locations = ko.mapping.toJS(self.selectedLocationsAdded, mapping);
                if (self.isStoreExistsInList(temp_selected_locations, temp_location)) {
                    $('#popUpLocationProfile').popup().popup('close');
                    $('#popUpConfirmChange').popup().popup('close');
                    if (click_type == "existing")
                        $('#alertmsg').text('This location profile cannot be edited as it has already been ordered');
                    else if (click_type == "new")
                        $('#alertmsg').text('This location profile cannot be added as it has already been ordered');
                    $("#okBut").attr("onclick", "ko.contextFor(this).$root.cancelPopUp();$('#popupDialog').popup('open');$('#popupDialog').popup('close');");
                    $('#popupDialog').popup().popup('open');
                    return false;
                }
                delete temp_location.isNewStore;
                delete temp_location.quantity;
                delete temp_location.needsCount;
                delete temp_location.status;
                if (click_type != "existing" && ($('#btnPopUpAddUpdate').text().indexOf('Update') > -1))
                    temp_location.pk = null;
                //else if (click_type != "existing" && ($('#btnPopUpAddUpdate').find('.ui-btn-text').text().indexOf('Add') > -1))
                $('#alertmsg').text('');
                postCORS(location_save_url, JSON.stringify(temp_location), function (data) {
                    if (typeof (parseInt(data)) == "number") {
                        var msg = "Location updated successfully."
                        if (self.editLocationInfo().pk() == null || self.editLocationInfo().pk() == "") {
                            msg = "Location added successfully.";
                            self.editLocationInfo().pk(data);
                        }
                        if (click_type == "new" || (click_type = "existing" && (($(event.currentTarget).text().indexOf('Add') > -1) || ($(event.currentTarget).text().indexOf('Yes') > -1))))
                            self.addSelectedStore(self.editLocationInfo(), event);

                        if (click_type = "existing") {
                            var search_string = "";
                            search_string += (search_string != '') ? '|' + ((self.editLocationInfo().storeCompleteAddress() != "") ? self.editLocationInfo().storeCompleteAddress() : '') : (self.editLocationInfo().storeCompleteAddress() != "") ? self.editLocationInfo().storeCompleteAddress() : '';
                            search_string += (self.editLocationInfo().computedStoreName() != "") ? '^' + self.editLocationInfo().computedStoreName() : '^';
                            search_string += ((self.editLocationInfo().phone != undefined && self.editLocationInfo().phone != null && self.editLocationInfo().phone() != "") ? '^' + self.editLocationInfo().phone() : '^');
                            search_string += '^' + self.editLocationInfo().locationIndex; // +'^';
                            var source_markers = $.grep(self.markers_array, function (obj) {
                                return obj.type === 'e';
                            });
                            if (source_markers.length > 0) {
                                var marker_idx = "";
                                var temp_marker;
                                $.each(source_markers[0].markers, function (key, val) {
                                    if (val.markerNo == self.editLocationInfo().locationIndex) {
                                        temp_marker = val;
                                        marker_idx = key;
                                        return false;
                                    }
                                });
                                source_markers[0].addresses[marker_idx] = search_string;
                                //google.maps.event.clearListeners(source_markers[0].markers[marker_idx].marker, 'click');
                                //source_markers[0].markers[marker_idx].marker.setMap(null);
                            }
                        }
                        $('#ulSelectedLocations').listview().listview('refresh').trigger("create");
                        $('#popUpConfirmChange').popup().popup('open');
                        $('#popUpConfirmChange').popup().popup('close');
                        $('#btnPopUpAddUpdate').removeAttr('data-clickType');
                        $('#popUpLocationProfile').popup().popup('open');
                        $('#popUpLocationProfile').popup().popup('close');
                        $('#popupClearListSelectionsConfirm').popup().popup('close');
                        $('#alertmsg').text(msg);
                        $('#okBut').unbind('click');
                        $('#okBut').bind('click', function () {
                            $('#popupDialog').popup().popup('close');
                        });
                        //$('#okBut').attr('onclick', '$(\'#popupDialog\').popup().popup(\'close\');');
                        $('#popupDialog').popup().popup('open');
                    }
                }, function (error_response) {
                    //if (!(jQuery.browser.msie)) {
                    //    $('#alertmsg').text((error_response.responseText != undefined) ? error_response.responseText : error_response.statusText);
                    //}
                    //else {
                    //    $('#alertmsg').text((error_response.errorMessage != undefined) ? error_response.errorMessage : ((error_response.responseText != undefined) ? error_response.responseText : error_response.Message));
                    //}
                    //if ($('#alertmsg').text() == "") {
                    //$('#alertmsg').text("An error occurred. Please contact administrator.")
                    showErrorResponseText(error_response, false);
                    //}

                    $('#popUpConfirmChange').popup().popup('close');
                    $('#popUpLocationProfile').popup().popup('close');
                    $('#popupClearListSelectionsConfirm').popup().popup('close');
                    $('#okBut').attr('onclick', '$(\'#popupDialog\').popup().popup(\'close\');');
                    //$('#popupDialog').popup().popup('open');
                    //showErrorResponseText(error_response, false);
                });
                break;
        }
    };

    //Adds selected location to ordered locations when user clicks on add (+) button
    self.addSelectedStore = function (selected_store, event) {
        var error_msg = '';
        var locations_add_limit = 1;
        if (showOrderLocationsLimit()) {
            locations_add_limit = getCustomTextLocationsLimit();
        }
        if (self.selectedLocationsAdded().length >= locations_add_limit && showOrderLocationsLimit()) {
            error_msg = 'The mail piece you are ordering only allows <b>' + locations_add_limit + '</b> locations to be added. To change the Ordered Location, click the Remove button to the right of the location name. Then add the new location.';
        }
        else {
            self.editLocationInfo(selected_store);
            self.locationPreviousInfo(ko.mapping.toJS(selected_store, mapping));
            error_msg = self.controlToValidate();     
        }

        if (error_msg != '') {
            $('#popUpLocationProfile').popup().popup('close');
            $('#alertmsg').html(error_msg);
            $('#popUpConfirmChange').popup().popup('close');
            $("#popupDialog").popup().popup('open');
            $("#okBut").unbind('click');
            $("#okBut").bind('click', function () {
                $('#popupDialog').popup('close');
            });
            return false;
        }

        var temp_selected_location = ko.mapping.toJS(selected_store, mapping);
        var temp_selected_locations = ko.mapping.toJS(self.selectedLocationsAdded, mapping);
        var temp_source_locations = ko.mapping.toJS(self.locations, mapping);
        var click_type = $(event.currentTarget).attr('data-clickType');
        var type = "";
        if (click_type == '' || click_type == 'existing' || click_type == 'clear')
            type = "e";
        else if (click_type == "new")
            type = "n"
        else if (click_type == "grouped")
            type = 'g';

        var source_markers = $.grep(self.markers_array, function (obj) {
            return obj.type === type;
        });

        var target_markers = $.grep(self.markers_array, function (obj) {
            return obj.type === "o";
        });

        if (self.locationsNeedToReview.length == 0 && !self.validateWithIn50MilesOrNot(selected_store) && (jobCustomerNumber != CW && jobCustomerNumber != BRIGHTHOUSE && jobCustomerNumber != BBB )) {
            if (Object.keys(ko.mapping.toJS(self.editLocationInfo(), mapping)).length <= 1)
                self.editLocationInfo(selected_store);
            $('#popupClearListSelectionsConfirm').popup().popup('open');
        }
        else if ((jobCustomerNumber == CASEYS || jobCustomerNumber == JETS) && self.gOutputData.templateName.indexOf('cgs_postcard') > -1 && selected_store.hasDelivery().toLowerCase() == "false") {
            $('#alertmsg').html("Location (" + selected_store.storeId() + ") does not offer delivery and may not be included in an Opt-In order.");
            $('#okBut').attr('onclick', '$(\'#popupDialog\').popup().popup(\'close\');');
            $('#popupDialog').popup().popup('open');
            //
            return false;
        }
        else {
            if (!self.isStoreExistsInList(temp_selected_locations, temp_selected_location)) {
                var index = self.getObjectIndex(temp_source_locations, temp_selected_location);

                var selected_store_icon = selected_store.locationMarker();
                var temp_icon_index = selected_store_icon.substring(selected_store_icon.lastIndexOf('.') - 2, selected_store_icon.lastIndexOf('.'));
                if (parseInt(temp_icon_index).toString() == "NaN") {
                    temp_icon_index = selected_store_icon.substring(selected_store_icon.lastIndexOf('.') - 1, selected_store_icon.lastIndexOf('.'));
                    selected_store_icon = selected_store_icon.substring(0, selected_store_icon.lastIndexOf('.') - 1);
                }
                else {
                    selected_store_icon = selected_store_icon.substring(0, selected_store_icon.lastIndexOf('.') - 2);
                }
                var icon = selected_store_icon + (self.selectedLocationsAdded().length + 1) + '.png';
                //if (jobCustomerNumber != CASEYS) {
                icon = icon.replace('E', 'O').replace('N', 'O').replace('blue', 'yellow').replace('green', 'yellow').replace('grey', 'yellow').replace('/e/', '/o/').replace('/n/', '/o/');
                    selected_store.status('Opted-In');
                //}
                selected_store.locationMarker(icon);

                self.selectedLocationsAdded.push(selected_store);

                var temp_selected_stores = (sessionStorage.tempSelectedStores != undefined && sessionStorage.tempSelectedStores != null && sessionStorage.tempSelectedStores != "") ? $.parseJSON(sessionStorage.tempSelectedStores) : [];

                var is_added = false;
                var added_idx;
                $.each(temp_selected_stores, function (key, val) {
                    //if (val.storeId == selected_store.storeId()) {
                    if (val.pk == selected_store.pk()) {
                        added_idx = key;
                        is_added = true;
                        return false;
                    }
                });
                if (!is_added) {
                    temp_selected_stores.push(ko.mapping.toJS(selected_store, mapping));
                }
                sessionStorage.tempSelectedStores = JSON.stringify(temp_selected_stores);

                var search_string = ""
                // var icon = logoPath + 'mapMarkers/o/markerO_red' + (parseInt(selected_store.locationIndex.substring(0, selected_store.locationIndex.indexOf('-'))) + 1) + '.png';
                //var icon = logoPath + 'mapMarkers/o/markerO_red' + ((self.selectedLocationsAdded().length > 50) ? (self.selectedLocationsAdded().length + (50 - self.selectedLocationsAdded().length)) : self.selectedLocationsAdded().length) + '.png';

                //var icon = logoPath + 'mapMarkers/o/markerO_red' + ((self.selectedLocationsAdded().length > 50) ? (self.selectedLocationsAdded().length + (50 - self.selectedLocationsAdded().length)) : self.selectedLocationsAdded().length) + '.png';
                if (self.uIViewConfig.m()) {
                    var marker_idx = "";
                    var temp_marker;
                    $.each(source_markers[0].markers, function (key, val) {
                        if (val.markerNo == selected_store.locationIndex) {
                            temp_marker = val;
                            marker_idx = key;
                            return false;
                        }
                    });
                    if (source_markers[0].markers[marker_idx] != undefined) {
                        google.maps.event.clearListeners(source_markers[0].markers[marker_idx].marker, 'click');
                        source_markers[0].markers[marker_idx].marker.setMap(null);
                    }
                    var temp_marker = source_markers[0].markers.splice(marker_idx, 1);
                    var temp_address = source_markers[0].addresses.splice(marker_idx, 1);
                    target_markers[0].addresses.push(temp_address[0]);
                    target_markers[0].markers.push(temp_marker[0]);
                    target_markers[0].markers[target_markers[0].markers.length - 1].marker.setMap(self.map());
                    target_markers[0].markers[target_markers[0].markers.length - 1].marker.setIcon(icon)
                    google.maps.event.addListener(target_markers[0].markers[target_markers[0].markers.length - 1].marker, 'click', self.showInfoWindowExisting(target_markers[0].markers.length - 1, 'o', target_markers[0].addresses[target_markers[0].addresses.length - 1]));
                    $.each(source_markers[0].markers, function (key, val) {
                        if (parseInt(val.markerNo) != key) {
                            //val.markerNo = key;
                            google.maps.event.clearListeners(source_markers[0].markers[key].marker, 'click');
                            google.maps.event.addListener(source_markers[0].markers[key].marker, 'click', self.showInfoWindowExisting(key, type));

                        }
                    });
                    google.maps.event.trigger(self.map(), 'resize');

                    self.showLocationDetails('o', target_markers[0].markers.length - 1);
                }
                if ((jobCustomerNumber != CW && jobCustomerNumber != BRIGHTHOUSE && jobCustomerNumber != BBB) && self.locationsNeedToReview.length > 0) {
                    $.each(self.selectedLocationsAdded(), function (a, b) {
                        if (self.locationsNeedToReview.indexOf(b.storeId()) > -1)
                            b.needsCount(true);
                    });
                    self.locationsNeedToReview = [];
                    $(event.currentTarget).attr('data-clickType', "countsClear");
                }

                var selected_location = ko.mapping.toJS(self.selectedLocationsAdded, mapping);
                $.each(selected_location, function (a, b) {
                    delete b.isQuantityRequired;
                    delete b.computedStoreName;
                    delete b.storeCompleteAddress;
                });

                self.gOutputData.selectedLocationsAction.selectedLocationsList = self.reconstructJobLocationObject(selected_location);
                sessionStorage.jobSetupOutput = JSON.stringify(self.gOutputData);
                if (type == "e") {
                    self.locations.remove(selected_store);

                    var page_range = $('#spnLocationsRange').text();
                    var page_y = page_range.substring(page_range.indexOf('- ') + 1);
                    var page_x = page_range.substring(0, page_range.indexOf(' -'));
                    page_y = parseInt(page_y) - 1;
                    page_range = page_x + ' - ' + page_y;
                    $('#spnLocationsRange').text(page_range);

                    if (self.locations().length == 0 && (self.gDataFiltered != undefined && self.gDataFiltered != null && self.gDataFiltered.availableTotalCount > 50) && (click_type == '' || click_type == 'existing' || click_type == 'clear'))
                        if (self.gDataFiltered.availableFinalIndex < self.gDataFiltered.availableTotalCount)
                            $('#btnNext').trigger('click');
                        else
                            $('#btnPrevious').trigger('click');
                    var search_string_existing = "";
                }
                //var page_id = Math.floor(self.gOutputData.selectedLocationsAction.selectedLocationsList.length / 50);
                //if ((self.gOutputData.selectedLocationsAction.selectedLocationsList.length % 50) == 0)
                //    page_id = page_id - 1;
                //self.ordPageId(page_id);

                var page_range = $('#spnOrdLocRange').text();
                var page_y = page_range.substring(page_range.indexOf('- ') + 1);
                var page_x = page_range.substring(0, page_range.indexOf(' -'));
                page_y = parseInt((page_y != '') ? page_y : 0) + 1;
                page_range = parseInt((page_x != "") ? page_x : 1) + ' - ' + page_y;
                $('#spnOrdLocRange').text(page_range);
                var page_count = "";
                if (self.gDataFiltered != undefined && self.gDataFiltered != null && self.gDataFiltered.seletedTotalCount != undefined && self.gDataFiltered.selectedTotalCount != null && self.gDataFiltered.selectedTotalCount != "") {
                    var page_count = (self.gDataFiltered.selectedTotalCount == 0) ? 1 : Math.ceil((self.gDataFiltered.selectedTotalCount / 50));
                }
                else {
                    page_count = $('#spnOrdTotalPages').text();
                    if (page_count = "") {
                        page_count = 1
                    }
                    else {
                        page_count++;
                    }
                }
                //var page_count = (self.gDataFiltered.selectedTotalCount == 0) ? 1 : Math.ceil((self.gDataFiltered.selectedTotalCount / 50));
                $('#spnOrdTotalPages').text(page_count);
                $('#spnOrdPagesCnt').text(page_count);
                //self.getOrderedLocationsPaged();

                $('#ulAvailableLocations').listview().listview('refresh');
                $('#ulSelectedLocations').listview().listview('refresh').trigger("create");
                if (self.selectedLocationsAdded().length > 0) {
                    if (self.gOutputData.jobTypeId == 71 || self.gOutputData.jobTypeId == 72 || jobCustomerNumber == DCA || jobCustomerNumber == KUBOTA || jobCustomerNumber == SPORTSKING || jobCustomerNumber == GWA || jobCustomerNumber == BUFFALONEWS || jobCustomerNumber == PIONEERPRESS || jobCustomerNumber == REDPLUM) {
                        $('#fldGrpButPanel').css('display', 'none');
                    }
                    else
                        $('#fldGrpButPanel').css('display', 'block');
                }
                if (self.locations().length < 6) {
                    $('#dvExistingLocationsContainer').css("height", "auto");
                    $('#dvExistingLocationsContainer').css("overflow-y", "auto");
                }
                else {
                    var total_locations = $('#spnTotalLocations').text();
                    $('#dvExistingLocationsContainer').css("height", ((total_locations > 50) ? '470px' : '523px'));
                    $('#dvExistingLocationsContainer').css('overflow-y', 'scroll');
                }

                if (self.selectedLocationsAdded().length >= 6) {
                    var total_locations = $('#spnTotalOrdLocations').text();
                    $('#dvOrderedLocationsContainer').css("height", ((total_locations > 50) ? '470px' : '523px'));
                    $('#dvOrderedLocationsContainer').css("overflow-y", "scroll");
                }
                else {
                    $('#dvOrderedLocationsContainer').css('height', 'auto');
                    $('#dvOrderedLocationsContainer').css('overflow-y', 'auto');
                }
                if ((self.isPagerDisplayed == false) && (self.gDataFiltered != undefined && self.gDataFiltered != null && self.gDataFiltered.availableTotalCount > 50)) {// || self.pageId() > 0
                    $('#dvPaging').css('display', 'block');
                    self.isPagerDisplayed = true;
                }
                getBubbleCounts(self.gOutputData);
                if (sessionStorage.updateLocationsList != undefined && sessionStorage.updateLocationsList != null && sessionStorage.updateLocationsList !== "")
                    self.updateLocations = $.parseJSON(sessionStorage.updateLocationsList);
                var is_update = true;
                $.each(self.updateLocations, function (key, val) {
                    //if (selected_store.storeId() == val.storeId && selected_store.pk() == val.pk && val.update == "remove") {
                    if (selected_store.pk() == val.pk && val.update == "remove") {
                        self.updateLocations.splice(key, 1);
                        is_update = false;
                        return false;
                    }
                });
                if (is_update) {
                    var is_added = false;
                    $.each(self.updateLocations, function (key, val) {
                        //if (val.storeId == selected_store.storeId()) {
                        if (val.pk == selected_store.pk()) {
                            is_added = true;
                            return false;
                        }
                    });
                    if (!is_added)
                        self.updateLocations.push(new locationUpdateModel(selected_store.storeId(), selected_store.pk(), "add"));
                }
                sessionStorage.updateLocationsList = JSON.stringify(self.updateLocations);
                self.updateLocations = [];
            }
            else {
                $('#alertmsg').html("This location has already been ordered.");
                $('#okBut').attr('onclick', '$(\'#popupDialog\').popup().popup(\'close\');');
                $('#popupDialog').popup().popup('open');
            }
            if (self.gOutputData.jobTypeId == 71 || self.gOutputData.jobTypeId == 72) {
                if (!dontShowHintsAgain && (sessionStorage.showDemoHints == undefined || sessionStorage.showDemoHints == null || sessionStorage.showDemoHints == "" || sessionStorage.showDemoHints == "on") && (sessionStorage.isLocationOrderedFirstTime == undefined || sessionStorage.isLocationOrderedFirstTime == null || sessionStorage.isLocationOrderedFirstTime == "" || sessionStorage.isLocationOrderedFirstTime == "false") && (showSearchedLocationsPopup())) {
                    sessionStorage.isLocationOrderedFirstTime = true;
                    $('#selectedLocations').popup('open', { positionTo: '#dvOrderedLocationsContainer' });
                }
            }
        }
    };
    //Removes the location from "Ordered Locations" list and adds into "Existing Locations" list.
    self.addLocationBack = function (removed_store) {
        var temp_removed_location = ko.mapping.toJS(removed_store, mapping);
        var temp_locations = ko.mapping.toJS(self.locations, mapping);
        var is_exists = false;
        is_exists = self.isStoreExistsInList(temp_locations, temp_removed_location);

        var selected_store_icon = removed_store.locationMarker();
        var temp_icon_index = selected_store_icon.substring(selected_store_icon.lastIndexOf('.') - 2, selected_store_icon.lastIndexOf('.'));

        if (parseInt(temp_icon_index).toString() == "NaN") {
            temp_icon_index = selected_store_icon.substring(selected_store_icon.lastIndexOf('.') - 1, selected_store_icon.lastIndexOf('.'));
            selected_store_icon = selected_store_icon.substring(0, selected_store_icon.lastIndexOf('.') - 1);
        }
        else {
            selected_store_icon = selected_store_icon.substring(0, selected_store_icon.lastIndexOf('.') - 2);
        }

        var icon = selected_store_icon + (self.locations().length + 1) + '.png';
        var icon = selected_store_icon + (self.selectedLocationsAdded().length + 1) + '.png';
        //if (jobCustomerNumber != CASEYS) {
            icon = icon.replace('O', 'E').replace('red', 'grey').replace('orange', 'grey').replace('yellow', 'grey').replace('/o/', '/e/');
        //removed_store.status('Available');
            removed_store.status('Opted-Out');
        //}
        removed_store.locationMarker(icon);

        var temp_removed_store = ko.mapping.toJS(removed_store, mapping);
        if (!is_exists) {
            removed_store.quantity('');
            self.locations.push(removed_store);
            if (self.locations().length > 0)
                self.isFirstTimeExisting(false);
        }
        var search_string_existing = "";
        self.selectedLocationsAdded.remove(removed_store);
        var source_markers = $.grep(self.markers_array, function (obj) {
            return obj.type === 'o';
        });

        var target_markers = $.grep(self.markers_array, function (obj) {
            return obj.type === "e";
        });
        //var icon = logoPath + 'mapMarkers/e/markerE_blue' + (parseInt(removed_store.locationIndex.substring(0, removed_store.locationIndex.indexOf('-'))) + 1) + '.png';
        if (self.uIViewConfig.m()) {
            var marker_idx = "";
            var temp_marker;
            $.each(source_markers[0].markers, function (key, val) {
                if (val.markerNo == removed_store.locationIndex) {
                    temp_marker = val;
                    marker_idx = key;
                    return false;
                }
            });

            google.maps.event.clearListeners(source_markers[0].markers[marker_idx].marker, 'click');
            source_markers[0].markers[marker_idx].marker.setMap(null);
            var temp_marker = source_markers[0].markers.splice(marker_idx, 1);
            var temp_address = source_markers[0].addresses.splice(marker_idx, 1);
            if (!is_exists) {
                target_markers[0].addresses.push(temp_address[0]);
                target_markers[0].markers.push(temp_marker[0]);
                target_markers[0].markers[target_markers[0].markers.length - 1].marker.setMap(self.map());
                target_markers[0].markers[target_markers[0].markers.length - 1].marker.setIcon(icon)
                google.maps.event.addListener(target_markers[0].markers[target_markers[0].markers.length - 1].marker, 'click', self.showInfoWindowExisting(target_markers[0].markers.length - 1, 'e', target_markers[0].addresses[target_markers[0].addresses.length - 1]));
                $.each(source_markers[0].markers, function (key, val) {
                    //if (parseInt(val.markerNo) != key) {
                    //val.markerNo = key;
                    google.maps.event.clearListeners(source_markers[0].markers[key].marker, 'click');
                    google.maps.event.addListener(source_markers[0].markers[key].marker, 'click', self.showInfoWindowExisting(key, 'o'));

                    //}
                });
            }
            google.maps.event.trigger(self.map(), 'resize');
        }
        var selected_location = ko.mapping.toJS(self.selectedLocationsAdded, mapping);
        $.each(selected_location, function (a, b) {
            delete b.isQuantityRequired;
            delete b.computedStoreName;
            delete b.storeCompleteAddress;
            selected_location.needsCount = true;
        });

        var is_found = false;
        var temp_removed_index = -1;
        $.each(self.gOutputData.selectedLocationsAction.selectedLocationsList, function (loc_key1, loc_val1) {
            if (loc_val1.pk == temp_removed_store.pk) {
                temp_removed_index = loc_key1;
                return false;
            }
        });
        if (temp_removed_index > -1) {
            self.gOutputData.selectedLocationsAction.selectedLocationsList.splice(temp_removed_index, 1);
            //    delete self.gOutputData.selectedLocationsAction.selectedLocationsList[temp_removed_index];
        }

        //self.gOutputData.selectedLocationsAction.selectedLocationsList = self.reconstructJobLocationObject(selected_location);

        sessionStorage.jobSetupOutput = JSON.stringify(self.gOutputData);
        //var page_id = Math.floor(self.gOutputData.selectedLocationsAction.selectedLocationsList.length / 50);
        //if ((self.gOutputData.selectedLocationsAction.selectedLocationsList.length % 50) == 0)
        //    page_id = page_id - 1;
        //self.ordPageId(page_id);
        //self.getOrderedLocationsPaged();

        var page_range = $('#spnOrdLocRange').text();
        var page_y = page_range.substring(page_range.indexOf('- ') + 1);
        var page_x = page_range.substring(0, page_range.indexOf(' -'));
        page_y = parseInt(((page_y == "") ? "0" : page_y)) - 1;
        page_range = ((page_x == "") ? 1 : page_x) + ' - ' + page_y;
        $('#spnOrdLocRange').text(page_range);

        $('#ulAvailableLocations').listview().listview('refresh');
        $('#ulSelectedLocations').listview().listview('refresh');
        if (self.selectedLocationsAdded().length == 0) {
            $('#fldGrpButPanel').css('display', 'none');
        }
        else {
            if (self.gOutputData.jobTypeId == 71 || self.gOutputData.jobTypeId == 72)
                $('#fldGrpButPanel').css('display', 'none');
        }
        if (self.selectedLocationsAdded().length < 6) {
            $('#dvOrderedLocationsContainer').css('height', 'auto');
            $('#dvOrderedLocationsContainer').css('overflow-y', 'auto');
        }
        else {
            var total_locations = $('#spnTotalOrdLocations').text();
            $('#dvOrderedLocationsContainer').css("height", ((total_locations > 50) ? '470px' : '523px'));
            $('#dvOrderedLocationsContainer').css("overflow-y", "scroll");

        }
        if (self.locations().length >= 6) {
            var total_locations = $('#spnTotalLocations').text();
            $('#dvExistingLocationsContainer').css("height", ((total_locations > 50) ? '470px' : '523px'));
            $('#dvExistingLocationsContainer').css("overflow-y", "scroll");
        }
        else {
            $('#dvExistingLocationsContainer').css('height', 'auto');
            $('#dvExistingLocationsContainer').css('overflow-y', 'auto');
        }

        var page_range = $('#spnLocationsRange').text();
        var page_y = page_range.substring(page_range.indexOf('- ') + 1);
        var page_x = page_range.substring(0, page_range.indexOf(' -'));
        page_y = parseInt(((page_y == "") ? "0" : page_y)) + 1;
        page_range = ((page_x == "") ? 1 : page_x) + ' - ' + page_y;
        $('#spnLocationsRange').text(page_range);
        if ((self.isPagerDisplayed == false && (self.gDataFiltered != undefined && self.gDataFiltered != null && self.gDataFiltered.availableTotalCount > 50))) {// self.locations().length > 50   || self.pageId() > 0
            $('#dvPaging').css('display', 'block');
            self.isPagerDisplayed = true;
        }
        getBubbleCounts(self.gOutputData);
        $('#ulSelectedLocations').listview().listview('refresh').trigger("create");

        var temp_selected_stores = (sessionStorage.tempSelectedStores != undefined && sessionStorage.tempSelectedStores != null && sessionStorage.tempSelectedStores != "") ? $.parseJSON(sessionStorage.tempSelectedStores) : [];
        var is_added = false;
        var removing_idx;
        $.each(temp_selected_stores, function (key, val) {
            //if (val.storeId == removed_store.storeId()) {
            if (val.pk == removed_store.pk()) {
                removing_idx = key;
                is_added = true;
                return false;
            }
        });
        if (is_added)
            temp_selected_stores.splice(removing_idx, 1);
        sessionStorage.tempSelectedStores = JSON.stringify(temp_selected_stores);

        if (sessionStorage.updateLocationsList != undefined && sessionStorage.updateLocationsList != null && sessionStorage.updateLocationsList !== "")
            self.updateLocations = $.parseJSON(sessionStorage.updateLocationsList);
        var is_update = true;
        $.each(self.updateLocations, function (key, val) {
            //if (removed_store.storeId() == val.storeId && removed_store.pk() == val.pk && val.update == "add") {
            if (removed_store.pk() == val.pk && val.update == "add") {
                self.updateLocations.splice(key, 1);
                is_update = false;
                return false;
            }
        });
        if (is_update) {
            var is_found = false;
            $.each(self.updateLocations, function (key, val) {
                if (val.pk == removed_store.pk() && val.update == "remove") {
                    is_found = true;
                    return false;
                }
            });
            if (!is_found)
                self.updateLocations.push(new locationUpdateModel(removed_store.storeId(), removed_store.pk(), "remove"));
        }
        sessionStorage.updateLocationsList = JSON.stringify(self.updateLocations);
        self.updateLocations = [];
    };
    //Open the location profile window with the selected location information.
    self.editLocation = function (current_location, idx, temp_data, event) {
        //        if ($(event.currentTarget).attr('data-clickType') == "new") {
        //            var temp_job_number = (sessionStorage.jobNumber != undefined && sessionStorage.jobNumber != null && sessionStorage.jobNumber != "") ? sessionStorage.jobNumber : jobNumber;
        //            if (temp_job_number == "-1") {
        //                $('#confirmMsg').html("You must save your job first before adding a new location");
        //                $('#okButConfirm').attr('onclick', '$(\'#popupConfirmDialog\').popup(\'close\');postJobSetUpData("save");');
        //                $('#okButConfirm').find('.ui-btn-text').text('Save');
        //                $('#popUpLocationProfile').popup().popup('close');
        //                $('#popupConfirmDialog').popup().popup('open');
        //                return false;
        //            }
        //        }
        $('#btnProfilePopupCancel').attr('selectedIndex', idx);
        $("#popUpLocationProfile :input").attr("disabled", false);
        $('#ddlProfileState').selectmenu().selectmenu('enable');
        $('#btnAddToOrder').css('display', 'none');
        $('#btnPopUpAddUpdate').css('display', 'block');

        var temp_location = ko.mapping.toJS(current_location, mapping);
        var temp_selected_locations = ko.mapping.toJS(self.selectedLocationsAdded, mapping);
        if ($(event.currentTarget).attr('data-clickType') == "new") {
            $('#popUpLocationProfile').find('.ui-grid-solo').find('.ui-block-a').html('<span style="font-size:12px;" class="word_wrap">Please verify and complete the Location Profile information below <br />as fully as possible before adding this location to your order.</span>');
            $('#btnPopUpAddUpdate').text('Add Location');
            $('#btnPopUpAddUpdate').attr('data-clickType', 'new');
        }
        else if ($(event.currentTarget).attr('data-clickType') == "edit") {
            $('#btnAddToOrder').css('display', 'block');
            $('#btnAddToOrder').css("display", "");
            $('#btnPopUpAddUpdate').attr('data-clickType', 'existing');
            $('#btnPopUpAddUpdate').text('Update Location');
            if (!featureLocationProfileUpdate())
                $('#btnPopUpAddUpdate').css('display', 'none');
        }
        else if ($(event.currentTarget).attr('data-clickType') == "ordered") {
            $('#btnPopUpAddUpdate').text('Remove from Order');
            $("#popUpLocationProfile :input").attr("disabled", true);
        }
        if ($(event.currentTarget).attr('data-clickType') == "ordered") {
            $('#popUpLocationProfile').find('.ui-grid-solo').find('.ui-block-a').html('<span style="font-size:12px;" class="word_wrap">Profile information for Ordered Locations cannot be edited unless<br />it is removed from the order first.</span>');
            $('#popUpLocationProfile').find('.ui-grid-solo').css('display', 'block');
        }
        else {
            if ($(event.currentTarget).attr('data-clickType') == "new") {
                $('#popUpLocationProfile').find('.ui-grid-solo').find('.ui-block-a').html('<span style="font-size:12px;" class="word_wrap">Please verify and complete the Location Profile information below <br />as fully as possible before adding this location to your order.</span>');
                $('#popUpLocationProfile').find('.ui-grid-solo').css('display', 'block');
            }
            else
                $('#popUpLocationProfile').find('.ui-grid-solo').css('display', 'none');
        }
        self.editLocationInfo(current_location);
        self.locationPreviousInfo(ko.mapping.toJS(current_location, mapping));

        $("#popUpLocationProfile :input[id*=latitude]").attr("disabled", true);
        $("#popUpLocationProfile :input[id*=longitude]").attr("disabled", true);
        $('#ddlProfileState').selectmenu().selectmenu('refresh');
        $('#ddlProfileState').parent().removeClass('ui-btn-corner-all').addClass('ui-corner-all');
        var config_json = $.parseJSON(sessionStorage.locationConfig).type;
        if (config_json["hasSubs"] != undefined) {
            $('#ddlhasSubs').parent().removeClass('ui-btn-corner-all').addClass('ui-corner-all');
            $('#ddlhasSubs').selectmenu().selectmenu('refresh');
        }
        if (config_json["hasDelivery"] != undefined) {
            $('#ddlhasDelivery').parent().removeClass('ui-btn-corner-all').addClass('ui-corner-all');
            $('#ddlhasDelivery').selectmenu().selectmenu('refresh');
        }
        if (config_json["newConstruction"] != undefined) {
            $('#ddlnewConstruction').parent().removeClass('ui-btn-corner-all').addClass('ui-corner-all');
            $('#ddlnewConstruction').selectmenu().selectmenu('refresh');
        }

        //$('.ui-select').find('div').removeClass('ui-btn-corner-all').addClass('ui-corner-all');
        $('#popUpLocationProfile').popup().popup('open');
    };
    //Initiates the Search for new locations on Google.
    self.search = function (type) {
        $('#dvNewSearchResults').css('display', 'none');
        if (type == undefined) type = 'n';

        // $('#btnRefresh')[0].disabled = true;

        if (self.isSwapping()) {
            if (self.newSearchText() != "" && Object.keys(self.newSearchText()).length > 0) {
                var temp_json = ko.toJS(self.newSearchText);
                if (temp_json.locationName != "")
                    $('#txtLocationName').val(temp_json.locationName);
                if (temp_json.city != "")
                    $('#txtCity').val(temp_json.city);
                if (temp_json.address != "")
                    $('#txtAddress').val(temp_json.address);
                if (temp_json.state != "")
                    $('#ddlState').val(temp_json.state);
                if (temp_json.city != "")
                    $('#txtZipCode').val(temp_json.zip);
            }
        }
        self.isFirstTime(false);
        self.clearMarkers(type);
        self.searchedLocations([]);
        self.reallyDoSearch();
        $('#dvSearchLocations').css('display', 'block');
        $('#dvSearchLocationsMap').css('display', 'block');
        $("#dvNewLocations").css('display', 'block');
        $('#map-canvas').css('display', 'block');
        google.maps.event.trigger(self.map(), 'resize');
    };
    //Searches the locations on Google based on the given criteria
    self.reallyDoSearch = function () {
        if (self.map().getZoom() != 7) {
            self.previousZoomLevel = self.map().getZoom();
            self.map().setZoom(7);
        }
        var search_string = ""
        var current_markers = $.grep(self.markers_array, function (obj) {
            return obj.type === 'n';
        });
        if ($('#headerSearch').text().toLowerCase().indexOf('new') > -1 || (self.newSearchText() != "" && Object.keys(self.newSearchText()).length > 0)) {
            var temp_json = {};
            var search_store_name = $('#txtLocationName').val();
            search_store_name = (search_store_name != $('#txtLocationName').attr('placeholder')) ? search_store_name : "";
            if (search_store_name != "")
                temp_json["locationName"] = search_store_name;
            var search_city = $('#txtCity').val();
            search_city = (search_city != $('#txtCity').attr('placeholder')) ? search_city : "";
            if (search_city != "")
                temp_json["city"] = search_city;
            var store_address = $('#txtAddress').val();
            store_address = (store_address != $('#txtAddress').attr('placeholder')) ? store_address : "";
            if (store_address != "")
                temp_json["address"] = store_address;
            var search_state = ($('#ddlState').val() != "") ? $('#ddlState').val() : "";
            search_state = (search_state != $('#ddlState').attr('placeholder')) ? search_state : "";
            if (search_state != "")
                temp_json["state"] = search_state;
            var search_zip = ($('#txtZipCode').val() != "") ? $('#txtZipCode').val() : "";
            search_zip = (search_zip != $('#txtZipCode').attr('placeholder')) ? search_zip : "";
            if (search_zip != "")
                temp_json["zip"] = search_zip;
            if (Object.keys(temp_json).length > 0)
                self.newSearchText(temp_json);

            if (store_address != "")
                search_string += store_address + ' ';
            if (search_city != "")
                search_string += search_city + ' ';
            if (search_state != "")
                search_string += search_state + ' ';
            if (search_zip != "")
                search_string += search_zip + ' ';
        }
        if (search_string != "") {
            search_string += ', United States of America'
            var sw = new google.maps.LatLng(25.5, -126);
            var ne = new google.maps.LatLng(49.5, -67);
            var bounds = new google.maps.LatLngBounds(sw, ne);
            self.isFirstTime(true); // componentRestrictions: { country: 'US'}
            var result_list = [];
            var latLngPts = [];
            var current_markers = $.grep(self.markers_array, function (obj) {
                return obj.type === 'n';
            });
            self.geoCoder().geocode({ 'address': search_string, 'componentRestrictions': { country: 'USA'} }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    window.setTimeout(function () { $('#btnRefresh').prop('disabled', false); if ($('#btnRefresh').length > 0) $('#btnRefresh')[0].disabled = false; }, 3000);
                    if (self.newPosition() == undefined || self.newPosition() == null || self.newPosition() == "")
                        self.map().setCenter(results[0].geometry.location);
                    else
                        self.map().setCenter(self.newPosition());

                    if ($('#rbtStreetAddr').attr('checked') == "checked") {
                        for (var i = 0; i < results.length; i++) {
                            self.addResult(results[i], i, latLngPts, current_markers);
                            if (i == results.length - 1) {
                                $('#ulSearchLocations').listview().listview('refresh').trigger('create');
                                var new_seach_results = result_list.join('|');
                                $('#dvSearchLocations').css('display', 'block');
                                $('#dvSearchLocationsMap').css('display', 'block');
                                $("#dvNewLocations").css('display', 'block');
                                $('#map-canvas').css('display', 'block');
                                if (self.uIViewConfig.m())
                                    self.displayMarkers(latLngPts, 'n');
                            }
                        }
                        if (results.length > 0) {
                            $('#mapBlock').css('visibility', 'visible');
                            $('#dvNewSearchResults').css('display', 'block');
                        }
                        else {
                            self.searchedLocations.removeAll();
                            $('#dvSearchLocationsContainer').css('height', 'auto');
                            $('#dvSearchLocationsContainer').css('overflow-y', 'auto');
                            $('#dvNewSearchResults').css('display', 'block');
                        }
                        //$('#btnRefresh')[0].disabled = false;
                        $('#dvNewSearchResults').css('display', 'block');

                    }
                    else {

                        var keyword = "";
                        if ($('#rbtBusinessName').attr('checked') == "checked")
                            keyword = search_store_name;
                        else if ($('#rbtStreetAddr').attr('checked') == "checked")
                            keyword = search_store_name;

                        //var rankBy = document.getElementById('rankBy').value;
                        var rankBy = 'PROMINENCE';
                        var search = {};
                        // search.rankBy = google.maps.places.RankBy.PROMINENCE;
                        if (keyword) {
                            search.keyword = keyword;
                        }
                        //                    else {
                        //                        search.types = ['hair_care']; //'pharmacy'
                        //                    }

                        if (rankBy == 'DISTANCE' && (search.types || search.keyword)) {
                            search.rankBy = google.maps.places.RankBy.DISTANCE;
                            search.location = self.map().getCenter();
                        } else {
                            search.rankBy = google.maps.places.RankBy.PROMINENCE;
                            search.bounds = self.map().getBounds();
                        }
                        newLocSearchString = "";
                        self.service().search(search, function (results, status) {
                            if (status == google.maps.places.PlacesServiceStatus.OK) {
                                var filtered_results = [];
                                for (var i = 0; i < results.length; i++) {
                                    self.addResult(results[i], i, latLngPts, current_markers);
                                    if (i == results.length - 1) {
                                        $('#ulSearchLocations').listview().listview('refresh').trigger('create');
                                        var new_seach_results = result_list.join('|');
                                        $('#dvSearchLocations').css('display', 'block');
                                        $('#dvSearchLocationsMap').css('display', 'block');
                                        $("#dvNewLocations").css('display', 'block');
                                        $('#map-canvas').css('display', 'block');
                                        if (self.uIViewConfig.m())
                                            self.displayMarkers(latLngPts, 'n');
                                        if ((sessionStorage.showDemoHints == undefined || sessionStorage.showDemoHints == null || sessionStorage.showDemoHints == "" || sessionStorage.showDemoHints == "on") && (sessionStorage.isNewLocationSearchFirstTime == undefined || sessionStorage.isNewLocationSearchFirstTime == null || sessionStorage.isNewLocationSearchFirstTime == "" || sessionStorage.isNewLocationSearchFirstTime == "false") && (showNewLocationsPopup())) {
                                            sessionStorage.isNewLocationSearchFirstTime = true;
                                            $('#newLocations').popup('open', { positionTo: '#dvSearchLocationsContainer' });
                                        }
                                        self.updateNewLocationDetails(results);
                                    }
                                }
                                if (results.length > 0) {
                                    $('#mapBlock').css('visibility', 'visible');
                                    $('#dvSearchLocationsContainer').css("height", ((results.length > 50) ? '470px' : '523px'));
                                    $('#dvSearchLocationsContainer').css("overflow-y", "scroll");
                                    $('#dvNewSearchResults').css('display', 'block');
                                }
                                else {
                                    self.searchedLocations.removeAll();
                                    $('#dvSearchLocationsContainer').css('height', 'auto');
                                    $('#dvSearchLocationsContainer').css('overflow-y', 'auto');
                                    $('#ulSearchLocations').listview().listview('refresh').trigger('create');
                                    $('#dvNewSearchResults').css('display', 'block');
                                }
                            }
                            else {
                                self.searchedLocations.removeAll();
                                $('#dvSearchLocationsContainer').css('height', 'auto');
                                $('#dvSearchLocationsContainer').css('overflow-y', 'auto');
                                $('#ulSearchLocations').listview().listview('refresh').trigger('create');
                                $('#dvNewSearchResults').css('display', 'block');
                            }
                            //$('#btnRefresh')[0].disabled = false;
                        });
                    }
                    window.setTimeout(function () { $('#btnRefresh').prop('disabled', false); if ($('#btnRefresh').length > 0) $('#btnRefresh')[0].disabled = false; }, 3000);
                }
            });
        }
        else window.setTimeout(function () {
            if ($('#btnRefresh').length > 0) {
                $('#btnRefresh').prop('disabled', false);
                $('#btnRefresh')[0].disabled = false;
            }
        }, 3000);
    };

    //Updates the selected location profile with the Country
    self.updateNewLocationDetails = function (results) {
        $.each(results, function (key, value) {
            var my_timer = setInterval(function () {
                var idx = key;
                self.service().getDetails({
                    reference: value.reference
                }, function (detailsResult, status) {
                    if (detailsResult != null) {
                        var address_parts = detailsResult.formatted_address.split(',');
                        var is_match = (detailsResult.name == address_parts[0])
                        var state_zip = ((address_parts.length > 4) ? ((address_parts[3] != undefined && address_parts[3] != null) ? address_parts[3] : "").trim() : ((address_parts[2] != undefined && address_parts[2] != null) ? address_parts[2] : "")).trim();
                        var zip = "";
                        if (state_zip != "") {
                            zip = state_zip.substring((state_zip.indexOf(' ') + 1), state_zip.length);
                        }
                        var state = $.trim(state_zip.replace(/\d/g, ''));
                        if (self.searchedLocations()[idx] != undefined && self.searchedLocations()[idx] != null) {
                            self.searchedLocations()[idx].state(state);
                            self.searchedLocations()[idx].zip(zip);
                        }
                        clearInterval(my_timer);
                    }
                })
            }, 1000);
        });
    };

    //Adds the searched locations to "New Locations" list.
    self.addResult = function (result, i, lat_lng_pts, current_markers) {
        var place_name = (result.name) ? result.name : "";
        var address_parts = [];
        if (result.vicinity != undefined && result.vicinity != null && result.vicinity != "") {
            address_parts = result.vicinity.split(',');
        }
        else if (result.formatted_address != undefined && result.formatted_address != null && result.formatted_address != "") {
            address_parts = result.formatted_address.split(',');
        }

        var address = "";
        var city = "";
        var state_zip = "";
        var zip = "";
        var is_match = (result.name == address_parts[0])

        address = ((is_match) ? ((address_parts[1] != undefined && address_parts[1] != null) ? address_parts[1] : "") : ((address_parts[0] != undefined && address_parts[0] != null) ? address_parts[0] : ""));
        city = ((is_match) ? ((address_parts[2] != undefined && address_parts[2] != null) ? address_parts[2] : "") : ((address_parts[1] != undefined && address_parts[1] != null) ? address_parts[1] : ""));
        state_zip = ((is_match) ? ((address_parts[3] != undefined && address_parts[3] != null) ? address_parts[3] : "") : ((address_parts[2] != undefined && address_parts[2] != null) ? address_parts[2] : ""));

        zip = state_zip.match(/\b\d{5}\b/g);
        var state = state_zip.replace(/\d/g, '');


        var temp_loc_json = $.extend(true, {}, $.parseJSON(sessionStorage.locationConfig).type);
        $.each(temp_loc_json, function (a, b) {
            temp_loc_json[a] = "";
        });
        temp_loc_json["location"] = $.trim(result.name);
        temp_loc_json["address"] = $.trim(address);
        temp_loc_json["address2"] = "";
        temp_loc_json["storeId"] = "";
        temp_loc_json["marketManager"] = "";
        temp_loc_json["city"] = $.trim(city);
        temp_loc_json["state"] = $.trim(state);
        temp_loc_json["zip"] = $.trim(zip);
        temp_loc_json["latitude"] = $.trim(result.geometry.location.lat());
        temp_loc_json["longitude"] = $.trim(result.geometry.location.lng());
        temp_loc_json["isNewStore"] = true;
        temp_loc_json["pk"] = null;
        temp_loc_json["quantity"] = "";
        temp_loc_json["needsCount"] = true;
        temp_loc_json["status"] = "";
        var temp_search_results = "";
        temp_search_results = address_parts.join(',');
        temp_search_results += (temp_search_results != "") ? '^' + $.trim(temp_loc_json["location"]) : $.trim(temp_loc_json["location"]);
        temp_search_results += ((temp_loc_json["phone"] != undefined && temp_loc_json["phone"] != null && temp_loc_json["phone"] != "") ? '^' + $.trim(temp_loc_json["phone"]) : '^');
        temp_search_results += '^' + i + '-' + temp_loc_json['storeId'];
        temp_search_results += '^' + "";
        lat_lng_pts.push({ 'lat': temp_loc_json["latitude"], 'lng': temp_loc_json["longitude"] });
        current_markers[0].addresses.push(temp_search_results);
        temp_search_results = "";
        self.searchedLocations.push(new locationModel1(temp_loc_json, i));
    };
    //Shows the location details over the map when user clicks on the marker on the map/or on the marker image in the respectie locations list.
    self.showLocationDetails = function (type, index) {
        if (type == 'o') {
            var page_id = self.ordPageId();
        }
        var current_markers = $.grep(self.markers_array, function (obj) {
            return obj.type === type;
        });
        if (current_markers[0].markers[index] != undefined) {
            google.maps.event.trigger(current_markers[0].markers[index].marker, 'click');
            //    self.getIWContentExisting(index, type, "");
        }
        else {
            if (self.uIViewConfig.m()) {
                var address = current_markers[0].addresses[index]
                var store_id = address.split('^')[1];
                $('#alertmsg').text('Location ' + store_id + ' does not have a latitude or longitude and will not be displayed on the map.');
                $('#popupDialog').popup('open');
            }
        }
    };
    //Checks the location is with in the 50 Miles or not before adding a location into ordered locations.
    self.validateWithIn50MilesOrNot = function (selected_store) {
        var is_with_in_50_miles = false;
        $.each(self.selectedLocationsAdded(), function (a, b) {
            if (b.needsCount() != undefined && b.needsCount() != null && !JSON.parse(b.needsCount().toString().toLowerCase())) {
                var LatLng_ordered_store = new google.maps.LatLng(b.latitude(), b.longitude());
                var LatLng_selected_store = new google.maps.LatLng(selected_store.latitude(), selected_store.longitude());

                var distance_between_two = google.maps.geometry.spherical.computeDistanceBetween(LatLng_ordered_store, LatLng_selected_store);  //In meters
                var distance_in_miles = distance_between_two / 1609.344;
                if (distance_in_miles < 50)
                    self.locationsNeedToReview.push(b.storeId());
            }
        });
        if (self.locationsNeedToReview.length > 0)
            return false;
        else return true;
    };
    self.disableClick = function (data, event) {
        event.preventDefault();

    };
    //Persists the user entered quantity into the session
    self.saveQuantity = function (current_store, event) {
        var ctrl = (event.target || event.currentTarget).id;
        setDigits(ctrl);
        var filtered_store = jQuery.grep(self.gOutputData.selectedLocationsAction.selectedLocationsList, function (key1, obj1) {
            return (key1.pk == current_store.pk())
        });
        if (filtered_store[0].quantity != parseInt($("#" + ctrl).val().replace(/,/g, "")))
            self.gOutputData["needsPostageReportUpdate"] = true; //if quantity, template and/or mail class have changed, we need make this attribue to true

        filtered_store[0].quantity = ($("#" + ctrl).val() != "") ? parseInt($("#" + ctrl).val().replace(/,/g, "")) : "";
        sessionStorage.jobSetupOutput = JSON.stringify(self.gOutputData);
    };
    //Constructs the location model for existing locations based on the loaded configuration.
    self.constructLocationModelForExistingJobs = function (location_obj) {
        var temp_loc_json = {};
        temp_loc_json["storeId"] = $.trim(((location_obj["storeId"] != undefined && location_obj["storeId"] != null && location_obj["storeId"] != "") ? location_obj["storeId"] : ""));
        temp_loc_json["storeType"] = $.trim(((location_obj["storeType"] != undefined && location_obj["storeType"] != null && location_obj["storeType"] != "") ? location_obj["storeType"] : ""));
        temp_loc_json["location"] = $.trim(((location_obj["storeName"] != undefined && location_obj["storeName"] != null && location_obj["storeName"] != "") ? location_obj["storeName"] : ""));
        if (location_obj["hasSubs"] != undefined || location_obj["hasSubs"] != null)
            temp_loc_json["hasSubs"] = $.trim(location_obj["hasSubs"]);
        if (location_obj["hasDelivery"] != undefined || location_obj["hasDelivery"] != null)
            temp_loc_json["hasDelivery"] = $.trim(location_obj["hasDelivery"]);
        if (location_obj["newConstruction"] != undefined || location_obj["newConstruction"] != null)
            temp_loc_json["newConstruction"] = $.trim(location_obj["newConstruction"]);
        if (location_obj["fax"] != undefined || location_obj["fax"] != null)
            temp_loc_json["fax"] = $.trim(location_obj["fax"]);
        if (location_obj["market"] != undefined && location_obj["market"] != null)
            temp_loc_json["market"] = $.trim(location_obj["market"]);
        temp_loc_json["address"] = $.trim(((location_obj["address1"] != undefined && location_obj["address1"] != null && location_obj["address1"] != "") ? location_obj["address1"] : ""));
        temp_loc_json["address2"] = $.trim(((location_obj["address2"] != undefined && location_obj["address2"] != null && location_obj["address2"] != "") ? location_obj["address2"] : ""));
        temp_loc_json["city"] = $.trim(((location_obj["city"] != undefined && location_obj["city"] != null && location_obj["city"] != "") ? location_obj["city"] : ""));
        temp_loc_json["state"] = $.trim(((location_obj["state"] != undefined && location_obj["state"] != null && location_obj["state"] != "") ? location_obj["state"] : ""));
        temp_loc_json["zip"] = $.trim(((location_obj["zip"] != undefined && location_obj["zip"] != null && location_obj["zip"] != "") ? location_obj["zip"] : ""));
        temp_loc_json["latitude"] = $.trim(((location_obj["latitude"] != undefined && location_obj["latitude"] != null && location_obj["latitude"] != "") ? location_obj["latitude"] : ""));
        temp_loc_json["longitude"] = $.trim(((location_obj["longitude"] != undefined && location_obj["longitude"] != null && location_obj["longitude"] != "") ? location_obj["longitude"] : ""));
        temp_loc_json["isNewStore"] = false;
        temp_loc_json["phone"] = $.trim(((location_obj["phoneNumber"] != undefined && location_obj["phoneNumber"] != null && location_obj["phoneNumber"] != "") ? $.trim(location_obj["phoneNumber"]) : ''));
        temp_loc_json["areaCode"] = $.trim(((location_obj["areaCode"] != undefined && location_obj["areaCode"] != null && location_obj["areaCode"] != "") ? $.trim(location_obj["areaCode"]) : ''));
        temp_loc_json["pk"] = $.trim(((location_obj["pk"] != undefined && location_obj["pk"] != null && location_obj["pk"] != "") ? $.trim(location_obj["pk"]) : ''));
        temp_loc_json["quantity"] = $.trim(((location_obj["quantity"] != undefined && location_obj["quantity"] != null && location_obj["quantity"] != "") ? $.trim(location_obj["quantity"]) : ''));
        temp_loc_json["marketManager"] = $.trim(((location_obj["marketManager"] != undefined && location_obj["marketManager"] != null && location_obj["marketManager"] != "") ? $.trim(location_obj["marketManager"]) : ''));
        temp_loc_json["needsCount"] = (location_obj["needsCount"] != undefined && location_obj["needsCount"] != null && location_obj["needsCount"] != "") ? location_obj["needsCount"] : false;
        if (jobCustomerNumber == AAG) {
            location_obj["firstName"] = "Mike";
            location_obj["lastName"] = "Stanely";
            temp_loc_json["firstName"] = $.trim(((location_obj["firstName"] != undefined && location_obj["firstName"] != null && location_obj["firstName"] != "") ? location_obj["firstName"] : ""));
            temp_loc_json["lastName"] = $.trim(((location_obj["lastName"] != undefined && location_obj["lastName"] != null && location_obj["lastName"] != "") ? location_obj["lastName"] : ""));
        }
        return temp_loc_json;
    };
    //Constructs the location model of the selected locations to save them in job ticket session. 
    self.reconstructJobLocationObject = function (location_obj) {
        var temp_locations_list = [];
        $.each(location_obj, function (key, val) {
            var temp_loc_json = {};
            temp_loc_json["storeId"] = $.trim(((val["storeId"] != undefined && val["storeId"] != null && val["storeId"] != "") ? val["storeId"] : ""));
            var store_name = "";
            //            if ($.parseJSON(sessionStorage.locationConfig).hidden["location"] == undefined || $.parseJSON(sessionStorage.locationConfig).hidden["location"] == null) {
            //                if (val["location"] != undefined && val["location"] != null && val["location"] != "")
            //                    store_name = $.trim(val["location"]);
            //                else
            //                    store_name = $.trim("");
            //            }
            //            else if ($.parseJSON(sessionStorage.locationConfig).hidden["storeType"] == undefined || $.parseJSON(sessionStorage.locationConfig).hidden["storeType"] == null) {
            //                if (val["storeType"] != undefined && val["storeType"] != null && val["storeType"] != "")
            //                    store_name = $.trim(val["storeType"]);
            //                else
            //                    store_name = $.trim("");
            //            }
            temp_loc_json["storeName"] = $.trim(((val["location"] != undefined && val["location"] != null && val["location"] != "") ? val["location"] : ""));
            //temp_loc_json["storeName"] = store_name;
            // (?val["storeType"]:"")));


            temp_loc_json["address1"] = $.trim(((val["address"] != undefined && val["address"] != null && val["address"] != "") ? val["address"] : ""));
            temp_loc_json["address2"] = $.trim(((val["address2"] != undefined && val["address2"] != null && val["address2"] != "") ? val["address2"] : ""));
            temp_loc_json["city"] = $.trim(((val["city"] != undefined && val["city"] != null && val["city"] != "") ? val["city"] : ""));
            temp_loc_json["state"] = $.trim(((val["state"] != undefined && val["state"] != null && val["state"] != "") ? val["state"] : ""));
            temp_loc_json["zip"] = $.trim(((val["zip"] != undefined && val["zip"] != null && val["zip"] != "") ? val["zip"] : ""));
            temp_loc_json["latitude"] = $.trim(((val["latitude"] != undefined && val["latitude"] != null && val["latitude"] != "") ? val["latitude"] : ""));
            temp_loc_json["longitude"] = $.trim(((val["longitude"] != undefined && val["longitude"] != null && val["longitude"] != "") ? val["longitude"] : ""));
            temp_loc_json["isNewStore"] = val["isNewStore"];
            temp_loc_json["phoneNumber"] = $.trim(((val["phone"] != undefined && val["phone"] != null && val["phone"] != "") ? $.trim(val["phone"]) : ''));
            temp_loc_json["areaCode"] = $.trim(((val["areaCode"] != undefined && val["areaCode"] != null && val["areaCode"] != "") ? $.trim(val["areaCode"]) : ''));
            temp_loc_json["pk"] = $.trim(((val["pk"] != undefined && val["pk"] != null && val["pk"] != "") ? $.trim(val["pk"]) : ''));
            temp_loc_json["quantity"] = $.trim(((val["quantity"] != undefined && val["quantity"] != null && val["quantity"] != "") ? $.trim(val["quantity"]) : ''));
            temp_loc_json["marketManager"] = $.trim(((val["marketManager"] != undefined && val["marketManager"] != null && val["marketManager"] != "") ? $.trim(val["marketManager"]) : ''));
            temp_loc_json["storeType"] = $.trim(((val["storeType"] != undefined && val["storeType"] != null && val["storeType"] != "") ? $.trim(val["storeType"]) : ''));

            if (val["fax"] != undefined && val["fax"] != null)
                temp_loc_json["fax"] = $.trim(((val["fax"] != "") ? $.trim(val["fax"]) : ''));

            if (val["market"] != undefined && val["market"] != null)
                temp_loc_json["market"] = $.trim(((val["market"] != "") ? $.trim(val["market"]) : ''));

            if (val["hasSubs"] != undefined && val["hasSubs"] != null)
                temp_loc_json["hasSubs"] = $.trim(((val["hasSubs"] != "") ? $.trim(val["hasSubs"]) : ''));

            if (val["hasDelivery"] != undefined && val["hasDelivery"] != null)
                temp_loc_json["hasDelivery"] = $.trim(((val["hasDelivery"] != "") ? $.trim(val["hasDelivery"]) : ''));

            if (val["newConstruction"] != undefined && val["newConstruction"] != null)
                temp_loc_json["newConstruction"] = $.trim(((val["newConstruction"] != "") ? $.trim(val["newConstruction"]) : ''));

            temp_loc_json["needsCount"] = (val["needsCount"] != undefined && val["needsCount"] != null && val["needsCount"] != "") ? val["needsCount"] : true;
            temp_locations_list.push(temp_loc_json);
        });
        return temp_locations_list;
    };

    self.showLoanOfficers = function () {
        $('#popUpLoanOfficers').popup('open');
    };
};
