﻿var requirementsData;
var mapping = { 'ignore': ["__ko_mapping__"] }

var gJobRequirementsInfoUrl = 'JSON/_jobRequirementsList.JSON';
var jobRequirementsListData;

$.getJSON('JSON/_jobRequirements.JSON', function (result) {
    requirementsData = result;
});

$.getJSON(gJobRequirementsInfoUrl, function (result) {
    jobRequirementsListData = result;
});

$('#_jobRequirements').live('pagebeforecreate', function (event) {
    displayMessage('_jobRequirements');
    ko.setTemplateEngine(new ko.nativeTemplateEngine);
});

$(document).on('pageshow', '#_jobRequirements', function (event) {

    try {
        window.setTimeout(function () {
            ko.applyBindings(new jobRequirementsViewModel());

            //$('ul[data-server]').listview();

            //$('div[class=ui-block-c] ul').listview();
            //$('#dvPrintstreamConnections div[data-role=collapsible-set] div[data-role=collasible] ul').listview();
            //$('#dvPrintstreamConnections div[data-role=collapsible]').find('.ui-collapsible-content').css('padding-bottom', '0px');
            //$('a[data-role=button]').markup('button');
            $('div[data-role=collapsible]:first-child').addClass('ui-first-child');
            $('div[data-role=collapsible]:last-child').addClass('ui-last-child');
            $('div[data-role=collapsible]').collapsible().trigger('create');

            //$('#dvPrintstreamConnections div[data-role=collapsible] div[data-role=collapsible] h2').find('a.ui-collapsible-heading-toggle').css('padding-left', '8px');
            //$('#dvPrintstreamConnections').collapsibleset('refresh');
            //$('#dvPrintstreamConnections div[data-role=collapsible]').eq(0).collapsible("disable");

        }, 2000);
    } catch (e) {
        alert(e);
    }

});

var FilesListModel = function (requirement_info) {
    var self = this;
    ko.mapping.fromJS(requirement_info, {}, self);
    self.displayName = ko.computed(function () {
        return self.general.requirementName() + ' - ' + self.status() + ' ' + ((self.date != undefined && self.date != null) ? self.date() : "");
    });
};

var itemModel = function (elements) {
    var self = this;
    ko.mapping.fromJS(elements, [], self);
};
var jobRequirementsViewModel = function () {
    var self = this;

    self.general = ko.observableArray();
    self.scheduling = ko.observableArray();
    self.type = ko.observableArray();

    self.fileslist = ko.observableArray([]);
    self.selectedRequirement = ko.observable({});
    self.newRequirementData = ko.observable({});


    var temp = ko.observableArray([]);
    temp = ko.mapping.fromJS(requirementsData);
    self.newRequirementData(temp);
    self.selectedRequirement(temp);

    //$.each(requirementsData, function (key, val) {
    //    if (key == "general") {
    //        self.general(new itemModel(val));
    //    }
    //    else if (key == "scheduling") {
    //        self.scheduling(new itemModel(val));
    //    }
    //    else {
    //        self.type(new itemModel(val));
    //    }
    //});

    $.each(jobRequirementsListData, function (key, val) {
        self.fileslist.push(new FilesListModel(val));
    });


    //Rediret to System Dashboard.
    self.redirectToJobRequirements = function (data, e) {
        window.setTimeout(function () {
            window.location.href = '../jobRequirements/jobRequirements.html';
        }, 500);
    };
    self.showHomePage = function () {
        //Need to call the function
        redirectToIndexPage();
    }
    self.showJobSummary = function () {
        //Need to call the function
        redirectToSingleJobPage();
    }

    self.requirementEditClick = function (data, el) {
        self.selectedRequirement(data);
        $('#btnAddRequirement').text('Update Requirement');
        window.setTimeout(function () {
            $('select').selectmenu("refresh");
            $('#addRequirement').popup('open');
        }, 100);
    };
    self.addNewRequirementClick = function () {
        self.selectedRequirement(self.newRequirementData());
        $('#btnAddRequirement').text('Add Requirement');
        window.setTimeout(function () {
            $('select').selectmenu("refresh");
            $('#addRequirement').popup('open');
        }, 100);
    };
    self.addNewRequirement = function (data, el) {
        var ctrl = el.targetElement || el.target;
        if ($(ctrl).text().indexOf('Add') > -1) {
            $('div[data-role=collapsible]:last-child').removeClass('ui-last-child');
            var temp = ko.mapping.toJS(self.selectedRequirement, mapping);
            self.fileslist.push(new FilesListModel(temp));
            $('div[data-role=collapsible]:last-child').addClass('ui-last-child');
            $('div[data-role=collapsible]').collapsible().trigger('create');
        }
        $('#addRequirement').popup('close');
    }

    self.deleteRequirement = function (data,el) {

        self.fileslist.remove(data);
        $('div[data-role=collapsible]:first-child').removeClass('ui-first-child');
        $('div[data-role=collapsible]:last-child').removeClass('ui-last-child');

        $('div[data-role=collapsible]:first-child').addClass('ui-first-child');
        $('div[data-role=collapsible]:last-child').addClass('ui-last-child');
    }
};
//Capitalizes the first letter in the given string.
String.prototype.initSmall = function () {
    return this.replace(/(?:^|\s)[A-Z]/g, function (m) {
        return m.toLowerCase();
    });
};
