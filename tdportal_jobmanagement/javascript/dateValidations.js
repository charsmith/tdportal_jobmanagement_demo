﻿/*
FileName: dateValidations.js
path: /javascript/dateValidations.js

The purpose of this page is to validate all types of date inputs
This page is included in .html pages wherever requires.
*/

var dtCh = "/";
var minYear = 1900;
var maxYear = 2100;

function fnDateFormatMMDDYYYY(date, ctl_yes) { //ctl_yes 0-milestone page, 2-singlejob page, 1- reports and milestone page popups
    var dateString = "";
    if (ctl_yes == 1)
        dateString = $("#" + date).val();
    else
        dateString = $("#" + date.id).val();

    if (dateString != "" && isDate(dateString) == false) {
        if (ctl_yes == 2) 
            $("#" + date.id).val('');            
        
        return false;
    }
    return true;
}

function fnStartEndDateValidation(start_date, end_date){
    if (Date.parse(end_date) < Date.parse(start_date)) {
        $('#popupLogin').popup('close');
        $('#popupReportsParam').popup('close');
        $('#alertmsg').text("Please ensure that the end date is greater than or equal to the start date.");
        $('#popupDialog').popup('open');
        $('#okBut').attr("onclick", "$('#popupDialog').popup('open'); $('#popupDialog').popup('close');");
        $('#okBut').focus();
        return false;
    }
    return true;
}

function isInteger(s) {
    var i;
    for (i = 0; i < s.length; i++) {
        // Check that current character is number.
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) return false;
    }
    // All characters are numbers.
    return true;
}

function stripCharsInBag(s, bag) {
    var i;
    var returnString = "";
    // Search through string's characters one by one.
    // If character is not in bag, append to returnString.
    for (i = 0; i < s.length; i++) {
        var c = s.charAt(i);
        if (bag.indexOf(c) == -1) returnString += c;
    }
    return returnString;
}

function daysInFebruary(year) {
    // February has 29 days in any year evenly divisible by four,
    // EXCEPT for centurial years which are not also divisible by 400.
    return (((year % 4 == 0) && ((!(year % 100 == 0)) || (year % 400 == 0))) ? 29 : 28);
}
function DaysArray(n) {
    for (var i = 1; i <= n; i++) {
        this[i] = 31;
        if (i == 4 || i == 6 || i == 9 || i == 11) { this[i] = 30 }
        if (i == 2) { this[i] = 29 }
    }
    return this
}

function isDate(dtStr) {
    var daysInMonth = DaysArray(12);
    var pos1 = dtStr.indexOf(dtCh);
    var pos2 = dtStr.indexOf(dtCh, pos1 + 1);
    var strMonth = dtStr.substring(0, pos1);
    var strDay = dtStr.substring(pos1 + 1, pos2);
    var strYear = dtStr.substring(pos2 + 1);
    strYr = strYear;
    if (strDay.charAt(0) == "0" && strDay.length > 1) strDay = strDay.substring(1);
    if (strMonth.charAt(0) == "0" && strMonth.length > 1) strMonth = strMonth.substring(1);
    for (var i = 1; i <= 3; i++) {
        if (strYr.charAt(0) == "0" && strYr.length > 1) strYr = strYr.substring(1);
    }
    month = parseInt(strMonth);
    day = parseInt(strDay);
    year = parseInt(strYr);
    if (pos1 == -1 || pos2 == -1) {
        $('#popupLogin').popup('close');
        $('#popupReportsParam').popup('close');
        $('#alertmsg').text("The date format should be : mm/dd/yyyy");
        $('#popupDialog').popup('open');
        $('#okBut').attr("onclick", "$('#popupDialog').popup('open'); $('#popupDialog').popup('close');");
        $('#okBut').focus();
        return false;
    }
    else if (strMonth.length < 1 || month < 1 || month > 12) {
        $('#popupLogin').popup('close');
        $('#popupReportsParam').popup('close');
        $('#alertmsg').text("Please enter a valid month");
        $('#popupDialog').popup('open');
        $('#okBut').attr("onclick", "$('#popupDialog').popup('open'); $('#popupDialog').popup('close');");
        $('#okBut').focus();
        return false;
    }
    else if (strDay.length < 1 || day < 1 || day > 31 || (month == 2 && day > daysInFebruary(year)) || day > daysInMonth[month]) {
        $('#popupLogin').popup('close');
        $('#popupReportsParam').popup('close');
        $('#alertmsg').text("Please enter a valid day");
        $('#popupDialog').popup('open');
        $('#okBut').attr("onclick", "$('#popupDialog').popup('open'); $('#popupDialog').popup('close');");
        $('#okBut').focus();
        return false;
    }
    else if (strYear.length != 4 || year == 0 || year < minYear || year > maxYear) {
        $('#popupLogin').popup('close');
        $('#popupReportsParam').popup('close');
        $('#alertmsg').text("Please enter a valid 4 digit year between " + minYear + " and " + maxYear);
        $('#popupDialog').popup('open');
        $('#okBut').attr("onclick", "$('#popupDialog').popup('open'); $('#popupDialog').popup('close');");
        $('#okBut').focus();
        return false;
    }
    else if (dtStr.indexOf(dtCh, pos2 + 1) != -1 || isInteger(stripCharsInBag(dtStr, dtCh)) == false) {
        $('#popupLogin').popup('close');
        $('#popupReportsParam').popup('close');
        $('#alertmsg').text("Please enter a valid date");
        $('#popupDialog').popup('open');
        $('#okBut').attr("onclick", "$('#popupDialog').popup('open'); $('#popupDialog').popup('close');");
        $('#okBut').focus();
        return false;
    }
    return true;
}