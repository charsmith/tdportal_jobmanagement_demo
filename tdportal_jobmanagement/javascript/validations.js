﻿//Allow Alpha Numerics only
function allowAlphaNumerics(event) {
    event.value = event.value.replace(/[^a-zA-Z0-9]+/g, '');
}

//Allow Paragraph Text Content.
function allowTextContent(event) {
    event.value = event.value.replace(/[^a-zA-Z0-9 .,]+/g, '');
}

//Validate Multiple Phone Numbers (U.S. Format) (comma separated)
function validateMulitpleUSPhoneNumbers(phoneNumbers) {
    var msg = "";
    var phone_numbers = phoneNumbers;
    if (phone_numbers.indexOf(',') > -1) {
        phone_numbers = phone_numbers.split(',');
        $.each(phone_numbers, function (a, b) {
            if (!validateUSPhone(b))
                msg = 'Invalid';
        });
    }
    else {
        if (!validateUSPhone(phoneNumbers))
            msg = 'Invalid';
    }

    return msg;
}

//Validate Text Content
function validateJunkCharacters(value) {
    var reg_ex = /^[^<>]+$/;
    var msg = "";
    if (!reg_ex.test(value)) {
        msg = '> or < symbols not allowed in ';
    }
    return msg;
} 

//Validate Phone Number (U.S. Format)
function validateUSPhone(phone) { 
    var expr = /^\(?(\d{3})\)?[- ]?(\d{3})[- ]?(\d{4})$/;
    var phone_number = $.trim(phone);
    phone_number = phone_number.replace(/[^0-9]+/g, '').toString().length == 11 ? phone_number.replace("1-", "").replace("1/", "").replace("1 ", "").replace(/\(/g,"").replace(/\)/g,"") : phone_number;
    if (!expr.test(phone_number))
        return false;
    return true;
}

function validateGmcPhone(phone) {
    var expr = /^\(?(\d{3})\)?[- ]?(\d{3})[- ]?(\d{4})$/;
    var phone_number = $.trim(phone);   
    if (!expr.test(phone_number))
        return false;
    return true;
}

//Validate Email Address
function validateMail(mail_id) {
    var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    if (!expr.test($.trim(mail_id)))
        return false;
    return true;
}

//Validate Multiple Email Addresses
function validateMultipleEmails(emails) {
    var msg = "";
    var email_ids = emails;
    if (email_ids.indexOf(',') > -1) {
        email_ids = email_ids.split(',');
        $.each(email_ids, function (a, b) {
            if (!validateMail(b))
                msg = 'Invalid';
        });
    }
    else {
        if (!validateMail(emails))
            msg = 'Invalid';
    }

    return msg;
}

// a and b are javascript Date objects
function getNumberOfDaysBetweenDates(a, b) {
    var _MS_PER_DAY = 1000 * 60 * 60 * 24;
    // Discard the time and time-zone information.
    var utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
    var utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());

    return Math.floor((utc2 - utc1) / _MS_PER_DAY);
}

//Utility Functions
function DeepTrim(obj) {
    for (var prop in obj) {
        var value = obj[prop], type = typeof value;
        if (value != null && (type == "string" || type == "object") && obj.hasOwnProperty(prop)) {
            if (type == "object") {
                DeepTrim(obj[prop]);
            } else {
                obj[prop] = obj[prop].trim();
            }
        }
    }
    return obj;
}

//Validating Price format
var priceValidation = function (control) {
    var regexpr = ($(control)[0].id).indexOf("discount") > -1 ? /^\$[0-9]{1,2}?$/ : /^\$[0-9]{1,3}(\.[0-9]{2,2})$/;
    var error_message = ($(control)[0].id).indexOf("discount") > -1 ? 'Enter price in $XX format' : 'Enter price in $XXX.XX format';
    var entered_value = $(control).val();
    var result = true;

    ////removing all characters ,spaces and special characters 
    //if ($(control).val().length > 0) {
    //    $(control).val($(control).val().replace(/[^0-9.]/g, ''));
    //}

    //formating $ symbol in the enetered value 
    if ($(control).val().length > 0 && ($(control).val() != 'n/a' && $(control).val()!='NA')) {
        $(control).val('$' + $(control).val().replace(/\$/g, ''));
    }
    if (!regexpr.test($(control).val())) {
        $(control)[0].title = error_message;
        $(control).css('border', 'solid 2px yellow');
        result = false;
    }
    else {
        $(control)[0].title = "";
        $(control).css('border', 'none');
    }
    //adding decimals  
    if (($(control)[0].id).indexOf("discount") == -1 && ($(control).val() != 'n/a' && $(control).val() != 'NA')) {
        if ($(control).val().replace(/\$/g, '').trim().length >= 1 && $(control).val().indexOf('.') == -1) {
            $(control).val($(control).val() + '.00');
        }
        else if ($(control).val().replace(/\$/g, '').trim().length >= 1 && $(control).val().indexOf('.') > -1 && $(control).val().split('.')[1].length == 0) {
            $(control).val($(control).val() + '00');
        }
        else if ($(control).val().replace(/\$/g, '').trim().length >= 1 && $(control).val().indexOf('.') > -1 && $(control).val().split('.')[1].length == 1) {
            $(control).val($(control).val() + '0');
        }
    }
    return result;
}



