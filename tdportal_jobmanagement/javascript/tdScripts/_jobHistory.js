jQuery.support.cors = true;

//******************** Global Variables Start **************************
var gData;
var gPageSize = 50;
var gDataStartIndex = 0;
var gDataList = [];
var gServiceUrl = "";
var cgsCustomerNumber = CASEYS;
if (appPrivileges.customerNumber == cgsCustomerNumber) { // for cgs
    gServiceUrl = '../JSON/_jobHistoryCGS.JSON';
} else {
    gServiceUrl = '../JSON/_jobHistory.JSON'; // for cw
}

//******************** Global Variables Start **************************

//******************** Page Load Events Start **************************
$(document).on('pageshow', '#_jobHistory', function (event) {
    //test for mobility...
    loadMobility();
    makeGData();
    $("#_jobHistory").trigger("create");
});
//******************** Page Load Events End ****************************

//******************** Public Functions Start **************************
function updateStartIndex(selector_id, list_div, caption, column) {
    var page_array = [];
    var multiplier = parseInt($('#' + selector_id).prop("selectedIndex"));
    gDataStartIndex = multiplier * gPageSize;

    //if ($('#txtJobnumber').val() != "" && $('#txtJobnumber').val() != undefined) {
    searchHistory();
    //}
    //else {
    //$.each(gDataList, function (facility_key, facitlity_val) {
    //    if (facitlity_val.list != null && (facitlity_val.list.toLowerCase() == caption.toLowerCase()))
    //        page_array = facitlity_val.items;
    //});
    //makeList(page_array, selector_id, list_div, caption, parseInt(column));
    //}
    $("div ul").each(function () {
        $(this).listview();
    });
    if (!jQuery.browser.msie) {
        $("div select").each(function () {
            $(this).selectmenu();
        });
    }
    $("#containDiv").css({ "position": "absolute", "top": "0", "right": "0", "margin": "0", "padding": "0", "width": "120px" });
    $("#containDiv").attr({ "data-role": "controlgroup", "data-type": "horizontal", "data-mini": "true" });
}

function createFacility(facility_name, job_list) {
    $('#ulJobHistory').append(job_list);
    $('#ulJobHistory').listview('refresh');
}

function makeList(array_name, selector_id, list_div, caption, column) {
    var header;
    var selector;
    var array = eval(array_name);
    var selector = "";
    var page_size = Math.round(array.length / gPageSize);
    if (page_size * gPageSize < array.length) {
        page_size++;
    }
    if (gDataStartIndex > array.length) gDataStartIndex = 0;
    var selected_index = $('#' + selector_id).prop("selectedIndex");
    var more_list_id = (selected_index == undefined) ? 0 : selected_index;
    if ((more_list_id + 1) > page_size) {
        more_list_id = selected_index = 0;
    }
    var selector = '<div id="containDiv" style="position: absolute; top: 0; right: 0; margin: 0; padding: 0; width:120px;" data-role="fieldgroup" data-type="horizontal" data-mini="true" ><select name="' + selector_id + '" id="' + selector_id + '" data-theme="b" data-overlay-theme="d" data-native-menu="true" data-mini="true" onchange="updateStartIndex(\'' + selector_id + '\',\'' + list_div + '\',\'' + caption + '\',\'' + column + '\')">';
    for (var i = 0; i < page_size; i++) {
        selector = (i == selected_index) ? selector + '<option selected value="' + i + '">Page ' + (1 + i) + '</option>' : selector + '<option value="' + i + '">Page ' + (1 + i) + '</option>';
    }
    selector += '</select></div>';

    var temp_array = (array.length > gDataStartIndex) ? array.slice(gDataStartIndex, gDataStartIndex + gPageSize) : array;
    //var search_field = '<li data-theme="c" ><input type="search" name="search" id="search" value="" onkeyup="searchHistory(\'' + selector_id + '\',\'' + list_div + '\',\'' + caption + '\',\'' + column + '\');" onchange="searchHistory(\'' + selector_id + '\',\'' + list_div + '\',\'' + caption + '\',\'' + column + '\')"/></li>';
    //temp_array.splice(0, 0, search_field)
    header = '<li data-role="list-divider" id="li' + selector_id + '" ><a style="text-decoration:none; color:#fff;">JOB HISTORY</a>' + selector + '</li>';
    temp_array.splice(0, 0, header)
    if ((more_list_id + 1) < page_size) {
        var more_list = '<li data-theme="c" ><a style="text-decoration:none;" href="#" onclick="getMoreData(\'' + selector_id + '\',\'' + (more_list_id + 1) + '\');">More...</a></li>';
        temp_array.splice(temp_array.length, 0, more_list);
    }
    var search_value = $('#txtJobnumber').val();
    $('#ulJobHistory').empty();
    $('#ulJobHistory').append(temp_array.join(' '));
    $('#ulJobHistory').listview('refresh');
    $('.ui-page').trigger('create');
    $('#txtJobnumber')[0].focus();
    $('#txtJobnumber').val(search_value);
    $('#lnkLess').css("color", "#2489ce");
    $('#lnkMore').css("color", "#2489ce");
}

function dateDifference() {
    var first = $('#inHomeFromDate').val();
    var second = $('#inHomeToDate').val();
    var days = parseInt(second.substring(0, 2)) - parseInt(first.substring(0, 2));


}

function clearFilter() {

    $('#txtJobnumber').attr("value", "");
    $('#txtCustomerId').attr("value", "");
    $('#inHomeFromDate').attr("value", "");
    $('#inHomeToDate').attr("value", "");
    searchHistory();
    // window.location.reload();
}
function searchHistory() {
    var filterd_list = [];
    //var search_value = $('#txtJobnumber').val();
    //if (search_value != "" && search_value != undefined) {
    gDataStartIndex = 0;

    var inhome_from_date = new Date($("#inHomeFromDate").val());
    var inhome_to_date = new Date($("#inHomeToDate").val());

    if ($('#txtJobnumber').val() == "" && $('#txtCustomerId').val() == "" && inhome_from_date == "" && inhome_to_date == "") {
        gDataStartIndex = 0;
        //$('#' + selector_id).val(gDataStartIndex);
        makeList(gDataList[0].items, '', '', '', '');
    }
    else {
        var searched_li = jQuery.grep(gDataList[0].items, function (a, b) {
            var inhome_date = ($("#spn" + b) != undefined && $("#spn" + b) != "") ? new Date(($("#spn" + b)[0].innerHTML !=undefined) ? $("#spn" + b)[0].innerHTML : "") : "";

            var filter_text = a.substring(a.indexOf('data-filtertext'), a.indexOf('><a') - 1);
            filter_text = filter_text.substring(filter_text.indexOf('=') + 2);
            if ($('#txtJobnumber').val() != "" && filter_text.toLowerCase().indexOf($('#txtJobnumber').val()) > -1) {
                filterd_list.push(a);
            }
            else if ($('#txtCustomerId').val() != "" && filter_text.toLowerCase().indexOf($('#txtCustomerId').val()) > -1) {
                filterd_list.push(a);
            }
            else if (inhome_date != "" && inhome_from_date < inhome_date && inhome_to_date > inhome_date) {
                filterd_list.push(a);
            }
            //return (filter_text.toLowerCase().indexOf($('#txtJobnumber').val().toLowerCase()) > -1);
        });
        makeList(filterd_list, '', '', '', '');
    }
}

function getSelectedNav(nav_text) {
    $('#hidNavText').val(nav_text);
}
function makeGData() {
    $.getJSON(gServiceUrl, function (data) {
        gData = data;
        buildLists();
    });
}
//function makeGData() {
//    $.ajax({
//        url: gServiceUrl,
//        dataType: 'json',
//        timeout: 10000,
//        type: 'GET',
//        async: false,
//        success: function (data) {
//            gData = jQuery.grep(data, function (obj) {
//                return obj.facilityName.toUpperCase() == "OPEN JOBS";
//            });
//            buildLists();
//        },
//        error: function (xml_http_request, text_status, error_thrown) {
//            $('#alertmsg').text("Error status :" + text_status + ". Error type: " + error_thrown + ". Error Message: " + xml_http_request.responseXML);
//            $('#popupDialog').popup('open');
//        }
//    });
//}
function buildLists() {
    var counter = 0;
    if (appPrivileges.customerNumber == cgsCustomerNumber) { // for cgs        
        $.each(gData, function (job_key, job_val) {
            job_counter++;
            if (job_val.jobHistoryDic != null && job_val.jobHistoryDic != "") {
                var job_history = job_val.jobDict;
                var temp_history_data = [];
                var selector_id = "", list_div = "", caption = "";
                selector_id = job_val.jobHistoryDic.toLowerCase() + 'Select';
                list_div = job_val.jobHistoryId;
                caption = job_val.jobHistoryDic;
                qtyName = "";
                dateName = "";
                var job_counter = 0;
                var navlink_counter = '';
                $.each(job_history, function (job_key, job_val) {
                    for_link_caption = caption.replace(' ', '');
                    job_counter = job_counter + 1;
                    navlink_counter = job_counter + "_" + for_link_caption + "_" + job_val.pk;
                    var job_number = (real_job_number > 0) ? real_job_number : neg_job_number;
                    //var filter_text = job_val.jobName + ' ' + job_val.date + ' ' + job_val.qty + ' ' + job_val.qty.replace(',', '');
                    var filter_text = job_number + ' ' + job_val.customerNumber;
                    var li = '<li id="' + job_val.jobName + '" data-filtertext="' + filter_text + '">';
                    li += '<a href="#" id="navLink' + navlink_counter + '" onclick="storeJobInfo(\'' + navlink_counter + '\',\'' + job_val.pk + '\',\'' + "" + '\',\'' + job_val.jobName + '\',\'' + list_div + '\')" title="Targeted Qty:' + job_val.qty + '">';
                    //li += '<img src="' + job_val.orderImage + '"/>
                    li += '<h3>' + job_val.jobName + '</h3>';
                    dateName = "In Home : ";
                    if (job_val.qty != "") {
                        qtyName = "Targeted Qty : ";
                        li += '<p>' + qtyName + job_val.qty;
                    }
                    li += '<p class="ui-li-aside">' + dateName + ' <b><span id="spn' + counter + '">' + job_val.date + '</span></b></p></a></li>';
                    temp_history_data.push(li);
                });
                gDataList.push({
                    "list": job_val.facilityName,
                    "items": temp_history_data
                });
                makeList(temp_history_data, selector_id.replace(" ", "_"), list_div, caption, "2");
            }
        });
    } else {
        var pager = ""
        var facility_counter = 0;
        $.each(gData, function (facility_key, facitlity_val) {
            facility_counter++;
            if (facitlity_val.facilityName != null && facitlity_val.facilityName != "") {
                var facility_jobs = facitlity_val.jobDict;
                var temp_facility_data = [];
                var selector_id = "", list_div = "", caption = "";
                selector_id = facitlity_val.facilityName.toLowerCase() + 'Select';
                list_div = facitlity_val.facilityId;
                caption = facitlity_val.facilityName;
                qtyName = "";
                dateName = "";
                var job_counter = 0;
                var navlink_counter = '';
                var neg_job_number = "";
                var real_job_number = "";
                $.each(facility_jobs, function (job_key, job_val) {
                    neg_job_number = "";
                    real_job_number = "";
                    if (job_val.jobNumber.indexOf('(-') > -1) {
                        real_job_number = eval(job_val.jobNumber.substring(0, (job_val.jobNumber.indexOf('(') - 1)));
                        neg_job_number = eval(job_val.jobNumber.substring((job_val.jobNumber.indexOf('(') + 1), job_val.jobNumber.indexOf(')')));
                    }
                    else {
                        real_job_number = 0;
                        neg_job_number = eval(job_val.JobNumberPretty);
                    }
                    var job_number = (real_job_number > 0) ? real_job_number : neg_job_number;
                    for_link_caption = caption.replace(' ', '');
                    job_counter = job_counter + 1;
                    navlink_counter = job_counter + "_" + for_link_caption + "_" + job_val.pk;
                    //var filter_text = job_val.jobName + ' ' + job_val.inHome + ' ' + job_val.actualQty + ' ' + job_val.actualQty.replace(',', '') + ' ' + job_val.qty + ' ' + job_val.qty.replace(',', '') + ' ' + job_val.customerNumber + ' ' + job_number;
                    var filter_text = job_number + ' ' + job_val.customerNumber;
                    var li = '<li id="' + job_val.jobName + '" data-filtertext="' + filter_text + '">';
                    li += '<a href="#" id="navLink' + navlink_counter + '" onclick="storeJobInfo(\'' + navlink_counter + '\',\'' + job_val.pk + '\',\'' + "" + '\',\'' + job_val.jobName + '\',\'' + list_div + '\')" title="quantity:' + job_val.qty + '">';
                    //li += '<img src="' + job_val.orderImage + '"/>
                    li += '<h3>' + job_val.jobName + '</h3>';
                    dateName = "In Home : ";
                    if (job_val.qty != "") {
                        qtyName = "Est Qty : ";
                        li += '<p>' + qtyName + job_val.qty + '| Size: ' + ((job_val.size != null) ? job_val.size : "") + ' | Class: ' + ((job_val.class != null) ? job_val.class : "") + ' | Versions: ' + ((job_val.versions != null) ? job_val.versions : "") + '<br/>TD#: ' + job_val.jobNumber + ' | Data: ' + ((job_val.data != null) ? job_val.data : "") + ' | Art: ' + ((job_val.art != null) ? job_val.art : "") + ' | Mail: ' + ((job_val.mail != null) ? job_val.mail : "") + '</p>';
                    }
                    //if (job_val.actualQty != "") {
                    //    qtyName = "Actual Qty : ";
                    //    li += ' | ' + qtyName + job_val.actualQty + '</p>'
                    //}

                    li += '<p class="ui-li-aside">' + dateName + ' <b><span id="spn' + counter + '">' + job_val.inHome + '</span></b></p></a></li>';
                    counter++;
                    temp_facility_data.push(li);
                });
                gDataList.push({
                    "list": facitlity_val.facilityName,
                    "items": temp_facility_data
                });
                makeList(temp_facility_data, selector_id.replace(" ", "_"), list_div, caption, "2");
            }
        });
    }
}
//******************** Public Functions End **************************