jQuery.support.cors = true;

//******************** Global Variables Start **************************
var gServiceUrl = "api/NexusIndex_v2/";
//if (appPrivileges.customerNumber == CASEYS) // for casey's
//    gServiceUrl = serviceURLDomain + "api/JobStatus/" + appPrivileges.customerNumber + "/1/caseysOnline";

var gServicePreference = serviceURLDomain + "api/Preferences/155711/1/index.htm";

var customerConfigUrl = "JSON/_customerConfiguration.JSON";
var customconfigData = {};
var advanceSearchUrl = "JSON/_advSearch.JSON";
var index_json_url = "JSON/_indexTd.JSON";
var index_jobs_error_url = "JSON/_indexJobProcessError.JSON";
var gUserServiceUrl = 'JSON/_personnelUsers.JSON';
var dailyTaskServiceUrl = "JSON/_myTaskSummary.JSON";
var optInUrl = "https://nexus.tribunedirect.com/api/DistrictEmail/";
var changePasswordUrl = serviceURLDomain + "api/Password/";
var dailyTaskUrlData = {};
var advanceSearchData = {};
var reportsServiceUrl = (jobCustomerNumber == SUNTIMES) ? "JSON/_stngReportNames.JSON" : "JSON/_reportsName.JSON";
var reportsServiceData = {};
var gData;
var gPageSize = 20;
var gDataStartIndex = 0;
var gDataList = [];
var gPrefData;
var pagePrefs = "";
var facilityId = "";
var vm;
var selectedUsers = [];
var tagJobInfo = {};
var demoEmailsList = [];
var processErrorData;
var usersJson = [];
var jobSchedulingInfo = {};
var isSearched = false;
//facilityId = "1";
facilityId = (sessionStorage.facilityId != undefined && sessionStorage.facilityId != null && sessionStorage.facilityId != "") ? sessionStorage.facilityId : "1";
//else 

if (appPrivileges != undefined && appPrivileges != null && appPrivileges != "" && appPrivileges.customerNumber == ALLIED) {
    facilityId = '3';
    sessionStorage.facilityId = "3";
}
var job_mile_stones = ["Scheduled", "Sign-Up Start", "Sign-Up End", "Approval Start", "Approval End", "Sale Start", "Sale End", "Data", "Art Arrival", "Data Complete", "Whitepaper / SO Due", "Printed Materials", "Production", "Postage", "Shipped", "Mail Drop", "In-Home", "Invoice", "Job Complete"];

var facilityJSON = [{ "facilityName": "Chicago", "facilityValue": "1" }, { "facilityName": "Los Angeles", "facilityValue": "2" }];

var dontShowHintsAgain = (localStorage.getItem('doNotShowHintsAgain') != undefined && localStorage.getItem('doNotShowHintsAgain') != null && localStorage.getItem('doNotShowHintsAgain') != "" && localStorage.getItem('doNotShowHintsAgain') != "false") ? true : false;
//******************** Global Variables Start **************************

//******************** Page Load Events Start **************************

$(window).on('popupbeforeposition', 'div:jqmData(role="popup")', function () {
    var notDismissible = $(this).jqmData('dismissible') === false;
    if (notDismissible) {
        $('.ui-popup-screen').off();
    }
});

$(document).on('pageshow', '#_indexGeneral', function () {

    if (jobCustomerNumber == BBB || jobCustomerNumber == AAG || jobCustomerNumber == TRIBUNEPUBLISHING || jobCustomerNumber == SANDIEGO || jobCustomerNumber == AH || jobCustomerNumber == SPORTSAUTHORITY || jobCustomerNumber == WALGREENS || jobCustomerNumber == JETS || jobCustomerNumber == CASEYS || jobCustomerNumber == CW || jobCustomerNumber == MOTIV8 || jobCustomerNumber == ALLIED || jobCustomerNumber == REDPLUM || jobCustomerNumber == SAFEWAY || jobCustomerNumber == LOWES || jobCustomerNumber == DCA || jobCustomerNumber == KUBOTA || jobCustomerNumber == GWA || jobCustomerNumber == OH || jobCustomerNumber == SPORTSKING || jobCustomerNumber == BUFFALONEWS || jobCustomerNumber == PIONEERPRESS || jobCustomerNumber == EQUINOX || isCreateDemoHintsInIndexPage()) {
        //window.setTimeout(function loadHints() {

        setDemoHintsSliderValue();
        createDemoHints("index");
        //if (jobCustomerNumber != REDPLUM)
        if (jobCustomerNumber != ACE) {
            $('#dvWelcomeMsg').css('display', 'block');
            $('#dvJobScheduling').css('display', 'none');
            $('#sldrShowHints').parent().css('display', 'none');
            //$('#popupWelcomeMsg').popup('open');
        }
        //}, 500);
    }
    //if (jobCustomerNumber == SPORTSAUTHORITY || jobCustomerNumber == WALGREENS || jobCustomerNumber == MOTIV8 || jobCustomerNumber == SAFEWAY || jobCustomerNumber == LOWES || jobCustomerNumber == CW || jobCustomerNumber == CASEYS || (jobCustomerNumber == AAG && appPrivileges.roleName != "admin") || jobCustomerNumber == DCA || jobCustomerNumber == KUBOTA || jobCustomerNumber == OH) {
    //    window.setTimeout(function () {
    //        var source = ((jobCustomerNumber == AAG && appPrivileges.roleName != "admin") || jobCustomerNumber == DCA || jobCustomerNumber == KUBOTA) ? "demoListJobs" : "dvJetsDemoJobs";
    //        //$('#' + source + ' div[data-role=collapsible]').trigger('click')
    //        $('#' + source + ' div[data-role=collapsible]').collapsible('expand');
    //    }, (jobCustomerNumber == CW || jobCustomerNumber == CASEYS || jobCustomerNumber == DCA || jobCustomerNumber == KUBOTA || jobCustomerNumber == OH) ? 3500 : 3500);
    //}
    //if ((jobCustomerNumber == BBB || jobCustomerNumber == ALLIED || jobCustomerNumber == JETS || jobCustomerNumber == CASEYS || jobCustomerNumber == CW || jobCustomerNumber == REDPLUM || jobCustomerNumber == OH /*|| jobCustomerNumber == DCA */ || (jobCustomerNumber == AAG && appPrivileges.roleName != "admin")) && (sessionStorage.isDemoIntroDisplayed == undefined || sessionStorage.isDemoIntroDisplayed == null || sessionStorage.isDemoIntroDisplayed == "" || sessionStorage.isDemoIntroDisplayed == "false")) {

    if ((showDemoIntroductionPopup() || (jobCustomerNumber == AAG && !is_valid_domain && appPrivileges.roleName != "admin")) && (sessionStorage.isDemoIntroDisplayed == undefined || sessionStorage.isDemoIntroDisplayed == null || sessionStorage.isDemoIntroDisplayed == "" || sessionStorage.isDemoIntroDisplayed == "false")) {
        sessionStorage.isDemoIntroDisplayed = true;
        window.setTimeout(function () {
            $('#popupGizmoIntro').popup('open');
            window.setTimeout(function () {
                $('#popupGizmoIntro').popup('close');
                if (jobCustomerNumber == ACE) {
                    if (!isWelcomeMsgRequired() && !dontShowHintsAgain && (sessionStorage.showDemoHints == undefined || sessionStorage.showDemoHints == null || sessionStorage.showDemoHints == "" || sessionStorage.showDemoHints == "on") && (sessionStorage.isWelcomeMsgDisplayed == undefined || sessionStorage.isWelcomeMsgDisplayed == null || sessionStorage.isWelcomeMsgDisplayed == "" || sessionStorage.isWelcomeMsgDisplayed == "false")) {
                        window.setTimeout(function () {
                            sessionStorage.isWelcomeMsgDisplayed = true;
                            $('#popupWelcomeMsg').popup('open');
                        }, 500);
                    }
                }
            }, 10000);
        }, 500);
    }
    else {
        if (jobCustomerNumber == ACE) {
            if (isWelcomeMsgRequired() && !dontShowHintsAgain && (sessionStorage.showDemoHints == undefined || sessionStorage.showDemoHints == null || sessionStorage.showDemoHints == "" || sessionStorage.showDemoHints == "on") && (sessionStorage.isWelcomeMsgDisplayed == undefined || sessionStorage.isWelcomeMsgDisplayed == null || sessionStorage.isWelcomeMsgDisplayed == "" || sessionStorage.isWelcomeMsgDisplayed == "false")) {
                window.setTimeout(function () {
                    sessionStorage.isWelcomeMsgDisplayed = true;
                    $('#popupWelcomeMsg').popup('open');
                }, 1000);
            }
        }
    }

    //if (appPrivileges.roleName == "admin" && appPrivileges.customerNumber == "1") {
    if ((appPrivileges.roleName == "admin" && (appPrivileges.customerNumber == "1" /*|| appPrivileges.customerNumber == REDPLUM*/)) && (sessionStorage.isDemoSelected == undefined || sessionStorage.isDemoSelected == null || sessionStorage.isDemoSelected == "" || sessionStorage.isDemoSelected == 0)) {
        //$('#dvPreferences').empty();
        $('#btnFilter').remove();
        showFilters();
        showFilterTasks();
        window.setTimeout(function () {
            $('#dvPreferences').collapsibleset('refresh');
        }, 50);
    }
    //if (appPrivileges.customerNumber == BRIGHTHOUSE || appPrivileges.customerNumber == "99998" || appPrivileges.customerNumber == "99997" || appPrivileges.customerNumber == "99996" || appPrivileges.customerNumber == "1") {
    //if (appPrivileges.customerNumber == "99998" || appPrivileges.customerNumber == "99997" || appPrivileges.customerNumber == "99996" || appPrivileges.customerNumber == "1") {
    //    showDemos();
    //    window.setTimeout(function applyThemes() {
    //        $('#dvPreferences').collapsibleset('refresh');
    //    }, 50);
    //}
    window.setTimeout(function () {
        if (sessionStorage.indexPrefs != undefined && sessionStorage.indexPrefs != null && sessionStorage.indexPrefs != "null" && sessionStorage.indexPrefs != "")
            loadData();
        $("#_indexGeneral").trigger("create");
    }, ((appPrivileges.customerNumber == "1" /*|| appPrivileges.customerNumber == REDPLUM*/) ? 1000 : 0));
    //if (appPrivileges.customerNumber== CW && (getSessionData("userRole") == "user" || getSessionData("userRole") == "power"))
    //$("#dvJobs").css("display", "block");
    //if (appPrivileges.customerNumber == CW) {

    //$('#viewJobs').html(viewJobs()); //TO BE Discussed

    $('.ui-page').trigger('create');
    //if(getSessionData("userRole") == "admin")
    //    $("#btnJobHistory").css("display", "block");
    $("#aSort").css("display", "block");
    $("#btnJobHistory").css("display", "none");
    //}

    //if (appPrivileges.customerNumber == "1") {
    if ((appPrivileges.customerNumber == "1" /*|| appPrivileges.customerNumber == REDPLUM*/) && (sessionStorage.isDemoSelected == undefined || sessionStorage.isDemoSelected == null || sessionStorage.isDemoSelected == "" || sessionStorage.isDemoSelected == 0)) {
        //$('#lnkNewJob').css('display', 'none');
        //$("#btnJobHistory").css("display", "none");
        //$("#dvDailyTaskListData").css("display", "block");
        //$('#btnSignOut').addClass('ui-corner-all');
        loadRegionsDDL();
        $('#ddlRegions').val(sessionStorage.facilityId).selectmenu('refresh');
    }

    if (appPrivileges.customerNumber != "1" /*&& appPrivileges.customerNumber != REDPLUM*/) {
        $('#dvRegionsDDL').css('display', 'none');
        //$("#dvDailyTaskListData").css("display", "none");
        //$('#dvReports').addClass('ui-corner-all');
    }
    //if (jobCustomerNumber != JETS && jobCustomerNumber != OH && jobCustomerNumber != AAG && jobCustomerNumber != TRIBUNEPUBLISHING && jobCustomerNumber != SANDIEGO)
    //if (jobCustomerNumber != JETS && jobCustomerNumber != OH && jobCustomerNumber != AAG && jobCustomerNumber != TRIBUNEPUBLISHING && jobCustomerNumber != SANDIEGO || (jobCustomerNumber == AAG && appPrivileges.roleName != "admin"))
    if (jobCustomerNumber != JETS && jobCustomerNumber != OH && jobCustomerNumber != AAG && jobCustomerNumber != TRIBUNEPUBLISHING && jobCustomerNumber != SANDIEGO /* || (jobCustomerNumber == AAG && !is_valid_domain && appPrivileges.roleName != "admin")*/)
        displayReportsList();
    if (appPrivileges.roleName == "admin") {
        $('#dvAdministration').css('display', 'block');
        $('#btnFilter').css('display', 'block');
        //        window.setTimeout(function loadDropdowns() {
        //            loadTagRegionsDDL(facilityJSON, 'filter');
        //            loadDepartmentsDDL(departmentsJson, 'filter')
        //            loadTagUsersDDL(usersJson, 'filter');
        //            // $('#fldSetFilterOptions').controlgroup('refresh');
        //        }, 1000);
        ////        window.setTimeout(function applyThemes() {
        ////            $('#fldSetFilterOptions').controlgroup('refresh');
        ////        }, 2000);
        if (jobCustomerNumber != BBB && (jobCustomerNumber == AAG && appPrivileges.roleName != "admin") && jobCustomerNumber != TRIBUNEPUBLISHING && jobCustomerNumber != SANDIEGO && jobCustomerNumber != AH && jobCustomerNumber != SPORTSAUTHORITY && jobCustomerNumber != WALGREENS && jobCustomerNumber != JETS && jobCustomerNumber != CASEYS && jobCustomerNumber != CW && jobCustomerNumber != MOTIV8 && jobCustomerNumber != ALLIED && jobCustomerNumber != SAFEWAY && jobCustomerNumber != LOWES && jobCustomerNumber != DCA && jobCustomerNumber != OH && jobCustomerNumber != SPORTSKING && jobCustomerNumber != KUBOTA && jobCustomerNumber != GWA && jobCustomerNumber != REDPLUM && jobCustomerNumber != BUFFALONEWS && jobCustomerNumber != PIONEERPRESS && jobCustomerNumber != EQUINOX && showDemoListJobs()) {
            $('#dvJetsDemoJobs').css('display', 'none');
            $('#dvBobsList').css('display', 'none');
            $('#demoListJobs').css('display', 'none');
            //$('#dvJetsDemoJobs').css('display', 'none');
            $('#dvWelcomeMsg').css('display', 'none');
            $('#dvAboutDemo').css('display', 'none');
            $('#btnHint').css('display', 'none');
        }
        else {
            loadAboutThisDemoFaq();
            if (jobCustomerNumber != CASEYS && jobCustomerNumber != CW && jobCustomerNumber != ALLIED && jobCustomerNumber != DCA && jobCustomerNumber != KUBOTA && jobCustomerNumber != GWA && jobCustomerNumber != OH && jobCustomerNumber != SPORTSKING && jobCustomerNumber != REDPLUM && jobCustomerNumber != BUFFALONEWS && jobCustomerNumber != PIONEERPRESS && jobCustomerNumber != EQUINOX && showDemoReports()) {
                loadDemoReportsJson();
                //$('#dvPreferences').css('display', 'none');
            }
            else {
                if (jobCustomerNumber != AAG)
                    displayReportsList();
            }
            //$('#dvWelcomeMsg').css('display', 'block');
            $('#btnHint').css('display', 'block');
            $('#btnHint').css('display', '');
            if (jobCustomerNumber == BBB) {
                $('#dvJetsDemoJobs').css('display', 'none');
                $('#dvBobsList').css('display', 'block');
                $('#dvAboutDemo').css('display', 'block');
            }
            else if (jobCustomerNumber == JETS || jobCustomerNumber == CASEYS || jobCustomerNumber == CW || jobCustomerNumber == SPORTSAUTHORITY || jobCustomerNumber == WALGREENS || jobCustomerNumber == MOTIV8 || jobCustomerNumber == SAFEWAY || jobCustomerNumber == LOWES || jobCustomerNumber == OH || jobCustomerNumber == REDPLUM || jobCustomerNumber == BUFFALONEWS || jobCustomerNumber == PIONEERPRESS || jobCustomerNumber == EQUINOX || showJetsDemoJobsDiv()) {
                $('#demoListJobs').css('display', 'none');
                $('#dvJetsDemoJobs').css('display', 'block');
                $('#dvBobsList').css('display', 'none');
                if (jobCustomerNumber != SPORTSAUTHORITY && jobCustomerNumber != WALGREENS && jobCustomerNumber != MOTIV8 && jobCustomerNumber != SAFEWAY && jobCustomerNumber != LOWES && jobCustomerNumber != CW && jobCustomerNumber != CASEYS && jobCustomerNumber != OH && jobCustomerNumber != REDPLUM && jobCustomerNumber != BUFFALONEWS && jobCustomerNumber != PIONEERPRESS && jobCustomerNumber != EQUINOX && showAboutDemoDiv())
                    $('#dvAboutDemo').css('display', 'block');
                else $('#dvAboutDemo').css('display', 'none');
                $('#dvAdministration').css('display', 'block');
            }
            else {
                $('#dvJetsDemoJobs').css('display', 'none');
                if (jobCustomerNumber != ALLIED && jobCustomerNumber != DCA && jobCustomerNumber != KUBOTA && jobCustomerNumber != SPORTSKING && jobCustomerNumber != GWA && (jobCustomerNumber == AAG && appPrivileges.roleName != "admin")) {
                    $('#demoListJobs').css('display', 'none');
                }
                $('#dvBobsList').css('display', 'none');
                $('#dvAboutDemo').css('display', 'none');
            }
        }
        $('#btnDeleteJob').css('display', 'block');
        $('#btnDeleteJob').css('display', '');
        $('#okButConfirm').text('Delete Job');
    }
    else {
        $('#dvAdministration').css('display', 'none');
        if (jobCustomerNumber != BBB && jobCustomerNumber != AAG && jobCustomerNumber != TRIBUNEPUBLISHING && jobCustomerNumber != SANDIEGO && jobCustomerNumber != AH && jobCustomerNumber != SPORTSAUTHORITY && jobCustomerNumber != WALGREENS && jobCustomerNumber != JETS && jobCustomerNumber != CASEYS && jobCustomerNumber != CW && jobCustomerNumber != MOTIV8 && jobCustomerNumber != ALLIED && jobCustomerNumber != SAFEWAY && jobCustomerNumber != LOWES && jobCustomerNumber != DCA && jobCustomerNumber != KUBOTA && jobCustomerNumber != OH && jobCustomerNumber != SPORTSKING && jobCustomerNumber != GWA && jobCustomerNumber != REDPLUM && jobCustomerNumber != BUFFALONEWS && jobCustomerNumber != PIONEERPRESS && jobCustomerNumber != EQUINOX && showBobsListDiv()) {
            $('#dvBobsList').css('display', 'none');
            $('#demoListJobs').css('display', 'none');
            $('#dvJetsDemoJobs').css('display', 'none');
            $('#dvWelcomeMsg').css('display', 'none');
            $('#dvAboutDemo').css('display', 'none');
        }
        else {
            loadAboutThisDemoFaq();
            //if ([CW].indexOf(jobCustomerNumber) == -1) {
            if ([AAG].indexOf(jobCustomerNumber) > -1 && appPrivileges.roleName != "admin") {
                $('#dvPreferences').css('display', 'none');
            }
            //$('#dvWelcomeMsg').css('display', 'block');
            if (jobCustomerNumber == BBB) {
                $('#dvJetsDemoJobs').css('display', 'none');
                $('#dvBobsList').css('display', 'block');
                $('#dvAboutDemo').css('display', 'block');
            }
            else if (jobCustomerNumber == JETS || jobCustomerNumber == CASEYS || jobCustomerNumber == CW || jobCustomerNumber == SPORTSAUTHORITY || jobCustomerNumber == WALGREENS || jobCustomerNumber == MOTIV8 || jobCustomerNumber == SAFEWAY || jobCustomerNumber == LOWES || jobCustomerNumber == OH || jobCustomerNumber == REDPLUM || jobCustomerNumber == BUFFALONEWS || jobCustomerNumber == PIONEERPRESS || jobCustomerNumber == EQUINOX || showJetsDemoJobsDiv()) {
                $('#dvJetsDemoJobs').css('display', 'block');
                $('#demoListJobs').css('display', 'none');
                $('#dvBobsList').css('display', 'none');
                if (jobCustomerNumber != SPORTSAUTHORITY && jobCustomerNumber != WALGREENS && jobCustomerNumber != MOTIV8 && jobCustomerNumber != SAFEWAY && jobCustomerNumber != LOWES && jobCustomerNumber != CW && jobCustomerNumber != CASEYS && jobCustomerNumber != OH && jobCustomerNumber != REDPLUM && jobCustomerNumber != BUFFALONEWS && jobCustomerNumber != PIONEERPRESS && jobCustomerNumber != EQUINOX && showAboutDemoDiv())
                    $('#dvAboutDemo').css('display', 'block');
                else $('#dvAboutDemo').css('display', 'none');
                if (jobCustomerNumber == JETS) {
                    $('#ulPickDemo').parent().parent().remove();
                    $('#dvAboutDemo div[data-role=collapsible]').addClass('ui-last-child');
                    $('#dvAboutDemo').collapsibleset('refresh');
                }
                $('#dvAdministration').css('display', 'none');
            }
            else {
                $('#dvJetsDemoJobs').css('display', 'none');
                if (jobCustomerNumber != ALLIED && jobCustomerNumber != DCA && /*jobCustomerNumber != AAG*/ (jobCustomerNumber == AAG && appPrivileges.roleName != "admin") && jobCustomerNumber != KUBOTA && jobCustomerNumber != SPORTSKING && jobCustomerNumber != GWA) {
                    $('#demoListJobs').css('display', 'none');
                }
                $('#dvBobsList').css('display', 'none');
                $('#dvAboutDemo').css('display', 'none');
            }
        }
    }

    if (appPrivileges.roleName != "admin")
        $('#aSort').addClass('ui-corner-all');
    /*$.each($('#col1 div[data-role=collapsible-set]'), function (key, val) {
    $(this).collapsibleset().trigger('create'); ;

    });*/


    $('#btnSort').addClass('ui-btn-corner-all');
    if (jobCustomerNumber == SUNTIMES) {
        //$('#lnkMaterialsAdmin').addClass('ui-corner-bl ui-corner-br');
        $('#lnkNewJob').hide();
        $('#lnkMaterialsAdmin').show();
        $('#lnkMaterialsAdmin').addClass('ui-first-child ui-last-child');
        $('#lnkMaterialsAdmin').addClass('ui-corner-all');
    }
    else {
        $('#lnkMaterialsAdmin').hide();
        if (jobCustomerNumber == BBB || jobCustomerNumber == JETS || jobCustomerNumber == CASEYS || jobCustomerNumber == MOTIV8 || jobCustomerNumber == EQUINOX && changeLinkNewJobText()) {
            $('#lnkNewJob').text('Start A New Job');
            if (jobCustomerNumber == CASEYS && appPrivileges.roleName == "power")
                $('#lnkNewJob').css('display', 'block');
        }
        else if ((jobCustomerNumber == AAG && /*(is_valid_domain || appPrivileges.roleName == "admin")*/ appPrivileges.roleName != "admin") || jobCustomerNumber == TRIBUNEPUBLISHING || jobCustomerNumber == SANDIEGO || jobCustomerNumber == AH || jobCustomerNumber == SPORTSAUTHORITY || jobCustomerNumber == WALGREENS || jobCustomerNumber == SAFEWAY || jobCustomerNumber == LOWES) {
            //else if ((jobCustomerNumber == AAG && appPrivileges.userRole == "admin") || jobCustomerNumber == TRIBUNEPUBLISHING || jobCustomerNumber == SANDIEGO || jobCustomerNumber == AH || jobCustomerNumber == SPORTSAUTHORITY || jobCustomerNumber == WALGREENS || jobCustomerNumber == SAFEWAY || jobCustomerNumber == LOWES) {
            $('#lnkNewJob').text('Start Upload Process');
        }
            //else if ((jobCustomerNumber == AAG && appPrivileges.roleName == "power")) {
        else if (/*(jobCustomerNumber == AAG && !is_valid_domain && appPrivileges.roleName == "power")*/jobCustomerNumber == AAG && appPrivileges.roleName == "admin") {
            //$('#lnkEditJob').text('Start Job');
            $('#lnkEditJob').css('display', 'none');
            $('#lnkNewJob').css('display', 'block');
        }

        $('#lnkNewJob').addClass('ui-corner-all');

        if (jobCustomerNumber != EQUINOX) {
            $('#lnkNewJob').css('border-bottom-right-radius', '0.7em');
            $('#lnkNewJob').css('border-bottom-left-radius', '0.7em');
        }
    }
    $('#lnAdmin').hide();
    if (getSessionData("userRole") == "admin" && (jobCustomerNumber != BBB && jobCustomerNumber != AAG && jobCustomerNumber != TRIBUNEPUBLISHING && jobCustomerNumber != SANDIEGO && jobCustomerNumber != AH && jobCustomerNumber != SPORTSAUTHORITY && jobCustomerNumber != WALGREENS && jobCustomerNumber != JETS && jobCustomerNumber != MOTIV8 && jobCustomerNumber != SAFEWAY && jobCustomerNumber != LOWES)) {
        //To show customer config button for admin only
        $("#aCustomerConfig").css("display", "block");
        //$("#dvReports").css("display", "block");
        $('#dvDemoEmailsList').css('display', 'none');
    }
    else {
        if (jobCustomerNumber != AAG && jobCustomerNumber != TRIBUNEPUBLISHING && jobCustomerNumber != SANDIEGO && jobCustomerNumber != AH && jobCustomerNumber != SPORTSAUTHORITY && jobCustomerNumber != WALGREENS && jobCustomerNumber != JETS && jobCustomerNumber != CASEYS && jobCustomerNumber != CW && jobCustomerNumber != MOTIV8 && jobCustomerNumber != ALLIED && jobCustomerNumber != SAFEWAY && jobCustomerNumber != LOWES && jobCustomerNumber != DCA && jobCustomerNumber != KUBOTA && jobCustomerNumber != OH && jobCustomerNumber != SPORTSKING && jobCustomerNumber != GWA && jobCustomerNumber != REDPLUM && jobCustomerNumber != BUFFALONEWS && jobCustomerNumber != PIONEERPRESS && jobCustomerNumber != EQUINOX && showDemoEmailListDiv())
            $('#dvDemoEmailsList').css('display', 'block');
        $("#aCustomerConfig").css("display", "none");
        if (jobCustomerNumber == AAG)
            $("#dvReports").css("display", "none");
        //$("#dvReports").css("display", "none");
        //$('#dvHearder').removeClass('ui-bar');
        //if ($('#btnSignOut').hasClass('ui-corner-all'))
        //  $('#btnSignOut').removeClass('ui-corner-all').addClass('ui-corner-right');
        //$('#btnSignOut').addClass('ui-corner-left');
        $('#btnSignOut').addClass('ui-corner-all');
        $('#btnSignOut').addClass('ui-first-child');
        //$('#dvPreferences').css('display', 'none');
        $('#dvAdministration').css('display', 'none');
        //if (jobCustomerNumber == JETS) {
        //    $('#dvAdministration').css('display', 'block');
        //}
    }

    if (appPrivileges.customerNumber == REGIS || appPrivileges.customerNumber == CW || appPrivileges.customerNumber == SUNTIMES || appPrivileges.customerNumber == CASEYS || appPrivileges.customerNumber == "1" || appPrivileges.customerNumber == BRIGHTHOUSE || appPrivileges.customerNumber == WALGREENS || appPrivileges.customerNumber == ALLIED || appPrivileges.customerNumber == REDPLUM || appPrivileges.customerNumber == DCA || appPrivileges.customerNumber == KUBOTA || appPrivileges.customerNumber == OH || appPrivileges.customerNumber == SPORTSKING || appPrivileges.customerNumber == GWA || appPrivileges.customerNumber == REDPLUM || appPrivileges.customerNumber == BUFFALONEWS || appPrivileges.customerNumber == PIONEERPRESS || appPrivileges.customerNumber == EQUINOX || isAppendCustomerToLogin()) {//appPrivileges.customerNumber == "186"
        //$('#btnSignOut').attr('href', 'login_new.htm');
        var url1 = 'login.htm?q=' + appPrivileges.customerNumber;
        $('#btnSignOut').attr('href', url1);
    }
    else
        $('#btnSignOut').attr('href', 'login.htm');

    //if (appPrivileges.customerNumber == CASEYS) {
    //    $('#lnkNewJob').css('display', 'none');
    //    $('#btnSignOut').addClass('ui-corner-all');
    //}
    if ($('#ddlFilterMilestones').val() == "-1") {
        if (($('#dtpStart') || $('#dtpEnd')).hasClass('ui-input-text')) {
            $('#dtpStart')[0].disabled = true;
            //$('#dtpStart').addClass('ui-disabled');
            $('#dtpStart').css('opacity', '0.7');
            //$('#dtpEnd').addClass('ui-disabled');
            $('#dtpEnd')[0].disabled = true;
            $('#dtpEnd').css('opacity', '0.7');
        }
    }
    if ((jobCustomerNumber == SPORTSAUTHORITY || jobCustomerNumber == WALGREENS || jobCustomerNumber == SAFEWAY || jobCustomerNumber == LOWES) && (appPrivileges.roleName == "power" || appPrivileges.roleName == "user")) {
        $('#lnkNewJob').css('display', 'none');
    }
    if (jobCustomerNumber == KUBOTA && getSessionData("userRole") != "power")
        $('#dvAdministration').css('display', 'block');
    /* Caseys Op-in and Appoval email links for Demo

    if (appPrivileges.customerNumber == CASEYS && appPrivileges.roleName == "admin") {
    if ($('#lnkNewJob').hasClass('ui-corner-top')) {
    $('#lnkNewJob').removeClass('ui-corner-all')
    }
    $('#lnkSendOptInEmail').css('display', 'block');
    $('#lnkSendApprovalEmail').css('display', 'block');
    $('#lnkSendApprovalEmail').addClass('ui-corner-bl ui-corner-br')
    }
    else {
    $('#lnkSendOptInEmail').css('display', 'none');
    $('#lnkSendApprovalEmail').css('display', 'none');
    } */
    if ([SCOTTS, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(jobCustomerNumber) == -1 && (sessionStorage.progressJob != undefined && sessionStorage.progressJob != null && (sessionStorage.progressJobMsgDisplayed == undefined || sessionStorage.progressJobMsgDisplayed == "" || sessionStorage.progressJobMsgDisplayed == "false"))) {
        window.setTimeout(function () {
            $("#okBut").text('close');
            sessionStorage.progressJobMsgDisplayed = "true";
            var submit_msg = 'This order has been submitted and is being converted to a live job.';
            if (sessionStorage.progressJob > 0) {
                $("#jobConversionPopup p").css('display', 'none');
                submit_msg = 'Your order has been submitted and is being processed. When processing has completed, you will receive a confirmation email.';
            }
            $("#jobConversionPopup h3").text(submit_msg);
            $('#jobConversionPopup').popup('open');
            $("#okBut").bind("click", function () {
                $('#jobConversionPopup').popup('close');
            });
        }, 1000);
    }
    $('#sldrShowHints').slider("refresh");
    jQuery("#txtInHomeDate").datepicker({
        minDate: new Date(2010, 0, 1),
        dateFormat: 'mm/dd/yy',
        constrainInput: true,
        beforeShowDay: function (date) {
            if (jobCustomerNumber == MOTIV8)
                return displayBlockDays(date, 0);
            else if (sessionStorage.asmFixedInHomeDate != undefined && sessionStorage.asmFixedInHomeDate != null && sessionStorage.asmFixedInHomeDate != "")
                return displayBlockDaysOtherthanSelected(date);
            else if (showFixedInHomeDatesForSelection())
                return displaySelectedDays(date);
            else if (sessionStorage.userRole != "admin")
                return displayBlockDays(date, ((jobCustomerNumber == DCA || jobCustomerNumber == SPORTSKING) ? 15 : ((jobCustomerNumber == OH) ? 7 : 21)));
            else
                return displayBlockDays(date, 1);
        }
    });
});

$('#_indexGeneral').live('pagebeforecreate', function () {
    //    if (jobCustomerNumber == BBB || jobCustomerNumber == JETS || jobCustomerNumber == AAG) {
    //        loadDemoHintsInfo();
    //    }
    if (appPrivileges.roleName != "user") {
        $('<li><a href="" onclick="deleteGizmoUsers();">Archive or Delete Jobs</a></li><!--<li><a href="" onclick="confirmFlushInMemoryDB();">Flush In-Memory DB</a></li>-->').appendTo('#ul2');
        if (featureFinanceSection())
            $('<li><a href="" onclick="manageFin();">Finance</a></li>').appendTo('#ul2');
        if (appPrivileges.roleName == "admin") {
            $('<li style="display:none"><a href="" onclick="navEmailLists();">Email Lists</a></li>').appendTo('#ul2');
            //$('<li><a href="" id="printStreamConnection" onclick="displayFacilityList();">Printstream Connections</a></li>').appendTo('#ul2');
            if (jobCustomerNumber == KUBOTA || jobCustomerNumber == GWA) {
                $('<li><a href="" id="lmaSetup" onclick="loadLMASetup();">LMA Setup</a></li>').appendTo('#ul2');
                if (jobCustomerNumber == KUBOTA)
                    $('<li><a href="" id="loadStoreData" onclick="loadStoreData();">Load Store Data</a></li>').appendTo('#ul2');
            }
            if (featureJobManagement()) {
                //$('<li><a href="" id="jobDiagnostics" onclick="loadJobDiagnostics(this)">Job Diagnostics</a></li>').appendTo('#ul2');
                $('<li><a href="" id="jobManagement" onclick="loadJobDiagnostics(this)">Job Management</a></li>').appendTo('#ul2');
            }
            $('<li><a href="" id="liSystemDashboard" onclick="loadSystemDashboard()">Systems Dashboard</a></li>').appendTo('#ul2');
        }
        if (featureModuleConfiguration()) {
            $('<li><a href="" id="liModuleConfig" onclick="loadModuleConfiguration()">Module Configuration</a></li>').appendTo('#ul2');
        }
        if (featureShortageModulesConfiguration()) {
            $('<li><a href="" id="liModuleConfig" onclick="loadShortageAndCorrections()">Shortages & Corrections</a></li>').appendTo('#ul2');
        }
        if (featureSharedMailDashboard()) {
            $('<li><a href="" id="liSharedMailDashboard" onclick="loadSharedMailDashboard()">Shared Mail Dashboard</a></li>').appendTo('#ul2');
        }
        //$('<li><a href="" onclick="navTemplateSetup();">Template Setup</a></li>').appendTo('#ul2');
    }
    else {
        $('<li><a href="" id="lmaSetup" onclick="loadLMASetup();">LMA Setup</a></li>').appendTo('#ul2');
        $("li").first().css("display", "none");
    }

    if ((appPrivileges.customerNumber == "1" /*|| appPrivileges.customerNumber == REDPLUM*/) && (sessionStorage.isDemoSelected == undefined || sessionStorage.isDemoSelected == null || sessionStorage.isDemoSelected == "" || sessionStorage.isDemoSelected == 0)) {
        getDailyTaskData();
    }
    if (sessionStorage.indexPageSortOrder != undefined && sessionStorage.indexPageSortOrder != null && sessionStorage.indexPageSortOrder != "") {
        $("#hdnSortOpenJobs").val(sessionStorage.indexPageSortOrder);
        if ($("#hdnSortOpenJobs").val() == 1)
            $('#chkInHome').attr('checked', true);
        else if ($("#hdnSortOpenJobs").val() == 2)
            $('#chkTemplate').attr('checked', true);
        else if ($("#hdnSortOpenJobs").val() == 3) {
            $('#chkInHome').attr('checked', true);
            $('#chkTemplate').attr('checked', true);
        }
    }
    getProcessErrorJobs();
    if (jobCustomerNumber == BBB || jobCustomerNumber == JETS || jobCustomerNumber == CASEYS || jobCustomerNumber == CW || jobCustomerNumber == AAG || jobCustomerNumber == TRIBUNEPUBLISHING || jobCustomerNumber == SANDIEGO || jobCustomerNumber == AH || jobCustomerNumber == SPORTSAUTHORITY || jobCustomerNumber == WALGREENS || jobCustomerNumber == SAFEWAY || jobCustomerNumber == LOWES || jobCustomerNumber == REDPLUM || jobCustomerNumber == BUFFALONEWS || jobCustomerNumber == PIONEERPRESS || jobCustomerNumber == EQUINOX || isLoadDemoEmailList())
        loadDemoEmailsList();
    //loadPersonalGroups(sessionStorage.facilityId);
    //loadUsersToJson();
    displayMessage('_indexGeneral');
    createConfirmMessage("_indexGeneral");
    getData();
    var region = jobCustomerNumber; // appPrivileges.customerNumber;
    if (region != null && region != "1") {
        setLogo(region);
    }
    else if (appPrivileges.customerNumber == "1" /*|| appPrivileges.customerNumber == REDPLUM*/)
        setLogo(sessionStorage.facilityId);
    //test for mobility...
    //loadMobility();
    if (appPrivileges.customerNumber == KUBOTA)
        setCustomGizmoLogo(appPrivileges.customerNumber);
    else
        setGizmoLogo();

    loadingImg("_indexGeneral");
    //if (appPrivileges.customerNumber == "1")

    getNextPageInOrder();
    window.setTimeout(function () {
        $('#sldrShowHints').trigger('create');
    }, 3000);
});
function getProcessErrorJobs() {
    $.getJSON(index_jobs_error_url, function (data) {
        processErrorData = data;
    });
}
function getData() {
    if (sessionStorage.indexPrefs == undefined || sessionStorage.indexPrefs == null || sessionStorage.indexPrefs == "null" || sessionStorage.indexPrefs == "") {
        if (appPrivileges.customerNumber == REGIS || appPrivileges.customerNumber == "203" || appPrivileges.customerNumber == SUNTIMES || appPrivileges.customerNumber == "1" || appPrivileges.customerNumber == BRIGHTHOUSE || appPrivileges.customerNumber == BBB || appPrivileges.customerNumber == AAG || appPrivileges.customerNumber == TRIBUNEPUBLISHING || appPrivileges.customerNumber == SANDIEGO || appPrivileges.customerNumber == AH || appPrivileges.customerNumber == SPORTSAUTHORITY || appPrivileges.customerNumber == WALGREENS || appPrivileges.customerNumber == MOTIV8 || appPrivileges.customerNumber == ALLIED || appPrivileges.customerNumber == REDPLUM || appPrivileges.customerNumber == SAFEWAY || appPrivileges.customerNumber == LOWES || appPrivileges.customerNumber == DCA || appPrivileges.customerNumber == KUBOTA || appPrivileges.customerNumber == OH || appPrivileges.customerNumber == SPORTSKING || appPrivileges.customerNumber == GWA || appPrivileges.customerNumber == BUFFALONEWS || appPrivileges.customerNumber == PIONEERPRESS || appPrivileges.customerNumber == EQUINOX || isLoadCampingWorldPagePreferences())
            getPagePreferences('index.htm', "1",/* (appPrivileges.customerNumber == REDPLUM) ? "1" :*/ CW);
        else
            getPagePreferences('index.htm', "1", (appPrivileges.customerNumber == JETS) ? CASEYS : appPrivileges.customerNumber);
    }
    else {
        managePagePrefs(jQuery.parseJSON(sessionStorage.indexPrefs));
    }
}

function loadData() {
    pagePrefs = jQuery.parseJSON(sessionStorage.indexPrefs);
    if (!fnVerifyScriptBlockDict(pagePrefs))
        return false;

    if (pagePrefs != null) {
        //gServiceUrl = eval(getURLDecode(pagePrefs.scriptBlockDict.gServiceUrl));
        //gServiceUrl = serviceURLDomain + "api/CWIndexV2/" + $("#hdnSortOpenJobs").val();
        var user_role = getSessionData("userRole");
        if ((jobCustomerNumber == CASEYS && user_role == "user") || (jobCustomerNumber != CASEYS && user_role != "admin")) {
            //eval(getURLDecode(pagePrefs.scriptBlockDict.pagebeforecreate));
            var theInstructions = getURLDecode(pagePrefs.scriptBlockDict.pagebeforecreate);
            var F = new Function(theInstructions);
            if (theInstructions != "")
                return (F());
        }
    }
    makeGData();

}

//******************** Page Load Events End ****************************

//******************** Public Functions Start **************************
var DailyTaskModel = function (process_name, process_number, process_attrs, process_theme, facility_id, process_status, icon_alt_text, is_editable) {
    var self = this;
    self.processName = process_name;
    self.processNumber = process_number;
    self.processAttrs = process_attrs;
    self.processTheme = process_theme;
    self.facilityId = facility_id;
    self.processStatusIcon = process_status;
    self.isEditable = (is_editable == 0) ? false : true;
};

var DailyTaskListModel = function (title, process_list, incomplete_cnt) {
    var self = this;
    self.groupTitle = title;
    self.incompleteCount = ko.observable(incomplete_cnt);
    self.processListData = ko.observableArray([]);
    self.processListData(process_list);
    //self.taskFacilityId(facility_id);
};

var JobModel = function (job_name, job_indentifier, job_attrs, filter_text, job_theme, job_number, job_customer_number, facility_id, milestone_icon, icon_alt_text, is_editable) {
    var self = this;
    self.jobName = job_name;
    self.jobIdentifier = job_indentifier;
    self.jobAttrs = job_attrs;
    self.filterText = filter_text;
    self.jobTheme = job_theme;
    self.jobNumber = job_number;
    self.jobCustomerNumber = job_customer_number;
    self.facilityId = facility_id;
    self.milestoneIcon = milestone_icon;
    self.milestoneIconAltText = icon_alt_text;
    self.isEditable = (is_editable == 0) ? false : true;
    //if (jobCustomerNumber == CASEYS && job_number == "414943") {
    //    self.pendingApprovalPercent = "100";
    //    self.pendingApprovalStatusPercent = '100'
    //    self.pendingApprovalStatus = getPendingApprovalStatus("100");
    //}
    //else if (jobCustomerNumber == CASEYS && job_number == "414987") {
    //    self.pendingApprovalPercent = '3';
    //    self.pendingApprovalStatusPercent = '45'
    //    self.pendingApprovalStatus = getPendingApprovalStatus('45');
    //}
    //else {
    self.pendingApprovalPercent = getPendingApprovalPercent(job_name, job_number);
    self.pendingApprovalStatusPercent = self.pendingApprovalPercent;
    self.pendingApprovalStatus = getPendingApprovalStatus(self.pendingApprovalPercent);
    self.isProgressJobConversion = ([SCOTTS, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(jobCustomerNumber) == -1 && (sessionStorage.progressJob != undefined && sessionStorage.progressJob != null && sessionStorage.progressJob != "")) ? (job_number == sessionStorage.progressJob) : false;
    //}
};


var jobGroupModel = function (title, show_region, job_items, paging_info, selected_page, search_value, facility_id, show_tagging) {
    var self = this;
    self.groupTitle = title;
    self.regionTitle = getTribuneFacilityName(facility_id);
    self.showRegion = show_region;
    self.listViewData = ko.observableArray([]); //.extend({ rateLimit: { method: "notifyWhenChangesStop", timeout: 0} });
    self.pagingData = ko.observableArray(paging_info).extend({ rateLimit: { method: "notifyWhenChangesStop", timeout: 200 } });
    self.selectedPageValue = ko.observable();
    var selected_value = (selected_value != undefined && selected_value != null && selected_value != "") ? selected_page : "0";
    self.selectedPageValue(selected_value);
    self.listViewData(job_items);
    self.searchValue = ko.observable();
    self.prevSearchValue = ko.observable('');
    self.searchValue(((search_value != undefined && search_value != null) ? search_value : ""));
    self.searchedRows = ko.observableArray([]);

    self.showMoreOption = ko.observable();
    var is_more_visible = (paging_info.length > 0 && parseInt(selected_value) < paging_info.length) ? true : false;
    self.showMoreOption(is_more_visible);
    self.showTagging = show_tagging;
    self.showSearchField = ko.observable();

    //    self.thvalue = ko.computed(function () {
    //        return self.searchValue();
    //    }, self).extend({ rateLimit: { method: "notifyWhenChangesStop", timeout: 600 }, throttle: 1 });
    //    self.thvalue.subscribe(function (val) {
    //        //if (val !== '') {
    //        var selected_target = $('#hdnSelectedGroup').val();
    //        var foundKeys = [];
    //        var page_index = 0;
    //        var context_data = "";
    //        var ctrl_id = 'dv' + selected_target.replace(/ /g, '_');
    //        if (selected_target != "") {
    //            foundKeys = Object.keys(gData.indexLists).filter(function (key) {
    //                return gData.indexLists[key] === self.groupTitle;
    //            });
    //            var root_context = ko.contextFor($('#' + ctrl_id)[0]).$root;
    //            context_data = ko.dataFor($('#dv' + selected_target.replace(/ /g, '_'))[0]);
    //            var sort_order = (sessionStorage.indexPageSortOrder != undefined && sessionStorage.indexPageSortOrder != null && sessionStorage.indexPageSortOrder != "") ? sessionStorage.indexPageSortOrder : null;
    //            var gServiceUrl_data = serviceURLDomain + gServiceUrl + appPrivileges.customerNumber + "/" + sessionStorage.facilityId;
    //            page_index = context_data.selectedPageValue();
    //            page_index = (page_index != undefined && page_index != null && page_index != "") ? page_index : 0;
    //            getSelectedData(ctrl_id, self, self.groupTitle, gServiceUrl_data, foundKeys[0], page_index, sort_order, '');
    //        }
    //        //}
    //    });
    //{ throttle: 500 }
    self.dataIcon = 'user'; //((title.indexOf('Open') > -1) ? 'myapp-tag' : ((title.indexOf('Tagged') > -1) ? 'myapp-tag' : ''));
};

var demoEmailListModel = function (data) {
    var self = this;
    self.name = ko.observable(data.name);
    self.type = ko.observable(data.type);
    self.startTime = ko.observable((data.startTime != undefined && data.startTime != null) ? data.startTime : "");
    self.endTime = ko.observable((data.endTime != undefined && data.endTime != null) ? data.endTime : "");
    self.id = ko.observable(data.id);
    self.showStartEndTime = ko.computed(function () {
        return (self.startTime() != "" && self.endTime() != "");
    });
};

var viewModel = function () {
    var self = this;
    taggingInfo(self);
    if (jobCustomerNumber == 1)
        self.loadPersonalGroups(sessionStorage.facilityId);
    self.person = ko.observable();
    self.editDefaultJob = function () {
        var temp_default_json = (sessionStorage.defaultJobDetails != undefined && sessionStorage.defaultJobDetails != null && sessionStorage.defaultJobDetails != "") ? $.parseJSON(sessionStorage.defaultJobDetails) : {};
        if (Object.keys(temp_default_json).length > 0) {
            sessionStorage.jobCustomerNumber = jobCustomerNumber;
            sessionStorage.jobNumber = temp_default_json.defaultJobNumber;
            sessionStorage.isGizmoJob = true;
            sessionStorage.jobDesc = temp_default_json.defaultJobName;
            if (temp_default_json.defaultJobNumber < 0)
                sessionStorage.negJobNumber = temp_default_json.defaultJobNumber;
            //Gets the selected job data from service.
            var g_edit_job_service = serviceURLDomain + "api/JobTicket/" + sessionStorage.facilityId + "/" + temp_default_json.defaultJobNumber + "/asdf";// + sessionStorage.username;
            getCORS(g_edit_job_service, null, function (data) {
                if (data == null) {
                    $('#alertmsg').text("No data exists for the selected Job. Please contact Administrator.");
                    $('#popupDialog').popup('open');
                    return false;
                }
                else {
                    var params = [];
                    params.push(data);
                    params.push('edit');
                    makeEditJobData(params);
                    window.location.href = "../jobSelectTemplate/jobSelectTemplate.html";
                }
            }, function (error_response) {
                showErrorResponseText(error_response);
            });

            //Creates required local session objects for the selected job when edit is clicked.
            self.assignGetData = function () {

            };
            //storeJobInfo(navlink_counter, current_context.jobNumber, current_context.jobNumber, current_context.jobName, current_context.facilityId, current_context.jobCustomerNumber, current_context.isEditable);
        }
    };
    self.jobActionsMenuClick = function (current_context, index, title, e) {
        if (jobCustomerNumber == ACE) {
            var ctrl_id = (e.targetElement || e.currentTarget || e.target).id;
            var ctrl = $((e.targetElement || e.currentTarget || e.target)).parent().parent();
            $('a[data-actiontype="edit"]').bind('click', function () {
                var context_info;
                if ($(ctrl).length > 0 && $(ctrl)[0].id.indexOf('Drive') > -1)
                    context_info = ko.toJS(current_context, mapping);
                else
                    context_info = current_context;
                self.jobClick(context_info, index, title, false);
            });
            $('a[data-actiontype="copy"]').bind('click', function () {
                $('#popupEditJob').popup('close');
                $('#btnCopyJobCancel').bind('click', function () {
                    $('#popupCopyJob').popup('close');
                });
                window.setTimeout(function () {
                    $('#popupCopyJob').popup('open');
                }, 100);
                //var context_info;
                //if ($(ctrl).length > 0 && $(ctrl)[0].id.indexOf('Drive') > -1)
                //    context_info = ko.toJS(current_context, mapping);
                //else
                //    context_info = current_context;
                //self.jobClick(context_info, index, title, true);
            });
            $('a[data-actiontype="delete"]').bind('click', function () {
                var context_info;
                if ($(ctrl).length > 0 && $(ctrl)[0].id.indexOf('Drive') > -1)
                    context_info = ko.toJS(current_context, mapping);
                else
                    context_info = current_context;
                self.getUserConfirmationForDelete(context_info);
            });
            $('#popupEditJob').popup('open', { positionTo: $('#' + ctrl_id) });
        }
        else {
            self.jobClick(current_context, index, title, false);
        }
    };
    self.jobActionsClose = function () {
        $('a[data-actiontype="edit"]').unbind('click');
    };
    self.jobClick = function (current_context, index, title, is_copy_job) {
        if (sessionStorage.progressJob != undefined && sessionStorage.progressJob != null && current_context.jobNumber == sessionStorage.progressJob && current_context.jobNumber < -1 && [SCOTTS, ANDERSEN, SHERWINWILLIAMS].indexOf(current_context.jobCustomerNumber) == -1) {
            $("#okBut").text('close');
            $("#jobConversionPopup h1").css('display', 'none');
            $("#jobConversionPopup h3").text('This order has been submitted and is being converted to a live job.');
            $('#jobConversionPopup').popup('open');
            $("#okBut").bind("click", function () {
                $('#jobConversionPopup').popup('close');
            });
            return false;
        }
        var navlink_counter = (index + 1) + '_' + title.replace(/ /g, '') + '_' + current_context.jobNumber;
        if (current_context.jobCustomerNumber == BBB || current_context.jobCustomerNumber == ALLIED || current_context.jobCustomerNumber == AAG || current_context.jobCustomerNumber == TRIBUNEPUBLISHING || current_context.jobCustomerNumber == SANDIEGO || current_context.jobCustomerNumber == AH || current_context.jobCustomerNumber == SPORTSAUTHORITY || current_context.jobCustomerNumber == WALGREENS || current_context.jobCustomerNumber == JETS || current_context.jobCustomerNumber == CASEYS || current_context.jobCustomerNumber == CW || current_context.jobCustomerNumber == MOTIV8 || current_context.jobCustomerNumber == SAFEWAY || current_context.jobCustomerNumber == LOWES || current_context.jobCustomerNumber == DCA || current_context.jobCustomerNumber == KUBOTA || current_context.jobCustomerNumber == SPORTSKING || current_context.jobCustomerNumber == GWA || current_context.jobCustomerNumber == REDPLUM || current_context.jobCustomerNumber == BUFFALONEWS || current_context.jobCustomerNumber == PIONEERPRESS || current_context.jobCustomerNumber == EQUINOX || isSetShowDemoHintsON(current_context.jobCustomerNumber))
            sessionStorage.showDemoHints = "on";
        storeJobInfo(navlink_counter, current_context.jobNumber, current_context.jobNumber, current_context.jobName, current_context.facilityId, current_context.jobCustomerNumber, current_context.isEditable, is_copy_job);
    };

    self.pageChanged = function (el, e) {//, parent
        if (e.originalEvent) {
            var ctrl_id = (e.targetElement || e.target || e.currentTarget).id;
            var ctrl = $('#' + e.target.id);
            var context_data = ko.contextFor(ctrl[0]).$parent;

            var foundKeys = Object.keys(gData.indexLists).filter(function (key) {
                return gData.indexLists[key] === context_data.groupTitle;
            });
            var sort_order = (sessionStorage.indexPageSortOrder != undefined && sessionStorage.indexPageSortOrder != null && sessionStorage.indexPageSortOrder != "") ? sessionStorage.indexPageSortOrder : null;
            //var gServiceUrl_data = serviceURLDomain + gServiceUrl + appPrivileges.customerNumber + "/" + sessionStorage.facilityId;
            var gServiceUrl_data = serviceURLDomain + gServiceUrl + jobCustomerNumber + "/" + sessionStorage.facilityId;
            getSelectedData(ctrl_id, context_data, context_data.groupTitle, gServiceUrl_data, foundKeys[0], ctrl.val(), sort_order);

            //window.setTimeout(function setUI() {
            $("div[id='dv" + ctrl_id.replace('ddl', '') + "']").find('ul').listview('refresh').trigger('create');
            $("div[id='dv" + ctrl_id.replace('ddl', '') + "']").find('ul').find('select').selectmenu('refresh').trigger('create');
            $("div[id='dv" + ctrl_id.replace('ddl', '') + "']").find('ul').find("input[data-type=search]").trigger('create');
            //}, 40);
        }
    };
    self.pageWithEnterKey = function (el, e) {
        if (e.which == 13) {
            self.moreClick(el, e);
            return false;
        }
        return true;
    };
    self.moreClick = function (el, e) {
        if (e.originalEvent) {
            $('#loadingText1').text('Loading...');
            $('#waitPopUp').popup('open');
            window.setTimeout(function () {
                $('#pageSelectPopup').popup('close');
                var ctrl = (e.targetElement || e.currentTarget || e.target);
                var ctrl_id = ctrl.id;
                if (ctrl_id == "")
                    ctrl_id = $(ctrl).attr('data-selectedPageOption');
                var id_suffix = ctrl_id.substring(ctrl_id.lastIndexOf('_') + 1);

                ctrl = $('#' + ctrl_id);
                var context_data = ko.contextFor(ctrl[0]).$parent;

                var foundKeys = Object.keys(gData.indexLists).filter(function (key) {
                    return gData.indexLists[key] === context_data.groupTitle;
                });

                var page_no = "";
                if (id_suffix == 'Prev')
                    page_no = parseInt(context_data.selectedPageValue()) - 1;
                else if (id_suffix == "Next")
                    page_no = parseInt(context_data.selectedPageValue()) + 1;
                else if (id_suffix == 'txt') {
                    if (parseInt(e.target.value) > context_data.pagingData().length)
                        page_no = (context_data.pagingData().length - 1);
                    else if (parseInt(e.target.value) < 1)
                        page_no = 0;
                    else
                        page_no = parseInt(e.target.value) - 1;
                }
                else if (id_suffix.indexOf("Pop") > -1)
                    page_no = parseInt($('#txtPageNumber').val()) - 1;
                else
                    page_no = id_suffix - 1;

                context_data.selectedPageValue(page_no);
                var sort_order = (sessionStorage.indexPageSortOrder != undefined && sessionStorage.indexPageSortOrder != null && sessionStorage.indexPageSortOrder != "") ? sessionStorage.indexPageSortOrder : null;
                // var gServiceUrl_data = serviceURLDomain + gServiceUrl + appPrivileges.customerNumber + "/" + sessionStorage.facilityId;
                var gServiceUrl_data = serviceURLDomain + gServiceUrl + jobCustomerNumber + "/" + sessionStorage.facilityId;

                getSelectedData(ctrl_id, context_data, context_data.groupTitle, gServiceUrl_data, foundKeys[0], (context_data.selectedPageValue()), sort_order);
                //window.setTimeout(function setUI() {
                $("#" + +ctrl_id.replace('btn', '')).find('ul').trigger("create").listview().listview('refresh');
                $("div[id='dv" + ctrl_id.replace('ddl', '') + "']").find('ul').listview('refresh').trigger('create');
                $("div[id='dv" + ctrl_id.replace('ddl', '') + "']").find('ul').find('select').selectmenu('refresh').trigger('create');
                $("div[id='dv" + ctrl_id.replace('ddl', '') + "']").find('ul').find("input[data-type=search]").trigger('create');
                //}, 40);
            }, 500);
        }
        //if (e.originalEvent) {
        //    $('#loadingText1').text('Loading...');
        //    $('#waitPopUp').popup('open');
        //    window.setTimeout(function () {
        //        $('#pageSelectPopup').popup('close');
        //        var ctrl = (e.targetElement || e.currentTarget || e.target);
        //        var ctrl_id = ctrl.id;
        //        if (ctrl_id == "")
        //            ctrl_id = $(ctrl).attr('data-selectedPageOption');
        //        var id_suffix = ctrl_id.substring(ctrl_id.lastIndexOf('_') + 1);

        //        var ctrl = $('#' + ctrl_id);
        //        var context_data = ko.contextFor(ctrl[0]).$parent;

        //        var foundKeys = Object.keys(gData.indexLists).filter(function (key) {
        //            return gData.indexLists[key] === context_data.groupTitle;
        //        });

        //        var page_no = "";
        //        if (id_suffix == 'Prev')
        //            page_no = parseInt(context_data.selectedPageValue()) - 1;
        //        else if (id_suffix == "Next")
        //            page_no = parseInt(context_data.selectedPageValue()) + 1;
        //        else if (id_suffix.indexOf("Pop") > -1)
        //            page_no = parseInt($('#txtPageNumber').val()) - 1;
        //        else
        //            page_no = id_suffix - 1;

        //        context_data.selectedPageValue(page_no);
        //        var sort_order = (sessionStorage.indexPageSortOrder != undefined && sessionStorage.indexPageSortOrder != null && sessionStorage.indexPageSortOrder != "") ? sessionStorage.indexPageSortOrder : null;
        //        // var gServiceUrl_data = serviceURLDomain + gServiceUrl + appPrivileges.customerNumber + "/" + sessionStorage.facilityId;
        //        var gServiceUrl_data = serviceURLDomain + gServiceUrl + jobCustomerNumber + "/" + sessionStorage.facilityId;

        //        getSelectedData(ctrl_id, context_data, context_data.groupTitle, gServiceUrl_data, foundKeys[0], (context_data.selectedPageValue()), sort_order);
        //        //window.setTimeout(function setUI() {
        //        $("#" + +ctrl_id.replace('btn', '')).find('ul').trigger("create").listview().listview('refresh');
        //        $("div[id='dv" + ctrl_id.replace('ddl', '') + "']").find('ul').listview('refresh').trigger('create');
        //        $("div[id='dv" + ctrl_id.replace('ddl', '') + "']").find('ul').find('select').selectmenu('refresh').trigger('create');
        //        $("div[id='dv" + ctrl_id.replace('ddl', '') + "']").find('ul').find("input[data-type=search]").trigger('create');
        //        //}, 40);
        //    }, 500);
        //}
    };

    self.selectedJobName = ko.observable();

    self.selectedJob = ko.observable({});

    self.getData = function (id) {
        var context_data = ko.dataFor($('#' + id)[0]);
        var foundKeys = Object.keys(gData.indexLists).filter(function (key) {
            return gData.indexLists[key] === context_data.groupTitle;
        });
        $('#hdnSelectedGroup').val(context_data.groupTitle);
        //var page_index = $('#ddl' + context_data.groupTitle.replace(/ /g, '_')).val();
        var page_index = context_data.selectedPageValue();
        page_index = (page_index != undefined && page_index != null && page_index != "") ? page_index : 0;
        var sort_order = (sessionStorage.indexPageSortOrder != undefined && sessionStorage.indexPageSortOrder != null && sessionStorage.indexPageSortOrder != "") ? sessionStorage.indexPageSortOrder : null;
        //var gServiceUrl_data = serviceURLDomain + gServiceUrl + appPrivileges.customerNumber + "/" + sessionStorage.facilityId;
        var gServiceUrl_data = serviceURLDomain + gServiceUrl + jobCustomerNumber + "/" + sessionStorage.facilityId;
        getSelectedData(id, context_data, context_data.groupTitle, gServiceUrl_data, foundKeys[0], page_index, sort_order);
    };

    self.fnSortJobs = function () {
        var sort_order = null;
        //    if ($("#chkInHome").is(':checked') && $("#chkTemplate").is(':checked'))
        //        sort_open_jobs = 3; //both checked
        //    else if ($("#chkTemplate").is(':checked'))
        //        sort_open_jobs = 2; //only template checked
        //    else if ($("#chkInHome").is(':checked'))
        //        sort_open_jobs = 1; //only inhome date checked
        if ($("#chkTemplate").is(':checked'))
            sort_order = 'template'; //only template checked
        else if ($("#chkInHome").is(':checked'))
            sort_order = 'inHomeDate'; //only inhome date checked
        else if ($("#chkJobNumber").is(':checked'))
            sort_order = 'jobNumber'; //only inhome date checked
        else if ($("#chkJobName").is(':checked'))
            sort_order = 'jobName'; //only inhome date checked
        else if ($("#chkApprovalStatus").is(':checked'))
            sort_order = 'approvalStatus'; //only Approval Status checked
        sessionStorage.indexPageSortOrder = sort_order;

        $("#hdnSortOpenJobs").val(sort_order);
        //$('#List2').hasClass('ui-collapsible-expanded')
        var selected_target = $('#hdnSelectedGroup').val();
        var foundKeys = [];
        var page_index = 0;
        var context_data = "";
        var ctrl_id = 'dv' + selected_target.replace(/ /g, '_');
        if (selected_target != "") {
            foundKeys = Object.keys(gData.indexLists).filter(function (key) {
                return gData.indexLists[key] === selected_target;
            });
            context_data = ko.dataFor($('#dv' + selected_target.replace(/ /g, '_'))[0]);
            page_index = context_data.selectedPageValue();
            page_index = 0;//(page_index != undefined && page_index != null && page_index != "") ? page_index : 0;
            context_data.selectedPageValue(page_index);
            var gServiceUrl_data = "";
            var index_list_name = (foundKeys.length > 0) ? foundKeys[0] : "";
            //gServiceUrl_data = serviceURLDomain + gServiceUrl + appPrivileges.customerNumber + "/" + sessionStorage.facilityId;
            gServiceUrl_data = serviceURLDomain + gServiceUrl + jobCustomerNumber + "/" + sessionStorage.facilityId;
            getSelectedData(ctrl_id, context_data, context_data.groupTitle, gServiceUrl_data, index_list_name, page_index, sort_order);
        }


        $('#popupSort').popup('close');
        //$("#col2 div[id^=List]").empty();
        //$("#col3 div[id^=List]").empty();
        //makeGData($("input[type=radio]:checked").val());
    };
    self.getSelectedFacilityJobs = function () {
        clearFilter(false);
        sessionStorage.facilityId = $('#ddlRegions').val();
        var selected_target = $('#hdnSelectedGroup').val();
        var foundKeys = [];
        var page_index = 0;
        var context_data = "";
        var ctrl_id = 'dv' + selected_target.replace(/ /g, '_');
        if (selected_target != "") {
            foundKeys = Object.keys(gData.indexLists).filter(function (key) {
                return gData.indexLists[key] === selected_target;
            });
            context_data = ko.dataFor($('#dv' + selected_target.replace(/ /g, '_'))[0]);
            page_index = context_data.selectedPageValue();
            page_index = (page_index != undefined && page_index != null && page_index != "") ? page_index : 0;
            var gServiceUrl_data = "";
            var sort_order = (sessionStorage.indexPageSortOrder != undefined && sessionStorage.indexPageSortOrder != null && sessionStorage.indexPageSortOrder != "") ? sessionStorage.indexPageSortOrder : null;
            var index_list_name = (foundKeys.length > 0) ? foundKeys[0] : "";
            //gServiceUrl_data = serviceURLDomain + gServiceUrl + appPrivileges.customerNumber + "/" + sessionStorage.facilityId;
            gServiceUrl_data = serviceURLDomain + gServiceUrl + jobCustomerNumber + "/" + sessionStorage.facilityId;
            getSelectedData(ctrl_id, context_data, context_data.groupTitle, gServiceUrl_data, index_list_name, page_index, sort_order);
        }
    };

    self.filterJobs = function (data, el, event) {
        if ((event.srcElement || event.originalTarget) != undefined && (event.srcElement || event.originalTarget).textContent == "Filter") {
            if (!(validateFilters()))
                return false;
        }
        var selected_target = $('#hdnSelectedGroup').val();
        var foundKeys = [];
        var page_index = 0;
        var context_data = "";
        var ctrl_id = 'dv' + selected_target.replace(/ /g, '_');
        if (selected_target != "") {
            foundKeys = Object.keys(gData.indexLists).filter(function (key) {
                return gData.indexLists[key] === selected_target;
            });
            context_data = ko.dataFor($('#dv' + selected_target.replace(/ /g, '_'))[0]);
            page_index = context_data.selectedPageValue();
            page_index = (page_index != undefined && page_index != null && page_index != "") ? page_index : 0;
            var gServiceUrl_data = "";
            var sort_order = (sessionStorage.indexPageSortOrder != undefined && sessionStorage.indexPageSortOrder != null && sessionStorage.indexPageSortOrder != "") ? sessionStorage.indexPageSortOrder : null;
            var index_list_name = (foundKeys.length > 0) ? foundKeys[0] : "";
            //gServiceUrl_data = serviceURLDomain + gServiceUrl + appPrivileges.customerNumber + "/" + sessionStorage.facilityId;
            gServiceUrl_data = serviceURLDomain + gServiceUrl + jobCustomerNumber + "/" + sessionStorage.facilityId;
            getSelectedData(ctrl_id, context_data, context_data.groupTitle, gServiceUrl_data, index_list_name, page_index, sort_order);
        }
        // $('#popupFilters').popup('close');
    };
    self.search = function (data, event) {
        var selected_target = $('#hdnSelectedGroup').val();
        var foundKeys = [];
        var page_index = 0;
        var context_data = "";
        var ctrl_id = 'dv' + selected_target.replace(/ /g, '_');
        if (selected_target != "" && !isSearched) {
            foundKeys = Object.keys(gData.indexLists).filter(function (key) {
                return gData.indexLists[key] === selected_target;
            });
            //var root_context = ko.contextFor($('#' + ctrl_id)[0]).$root;
            context_data = ko.dataFor($('#dv' + selected_target.replace(/ /g, '_'))[0]);
            var sort_order = (sessionStorage.indexPageSortOrder != undefined && sessionStorage.indexPageSortOrder != null && sessionStorage.indexPageSortOrder != "") ? sessionStorage.indexPageSortOrder : null;
            //var gServiceUrl_data = serviceURLDomain + gServiceUrl + appPrivileges.customerNumber + "/" + sessionStorage.facilityId;
            var gServiceUrl_data = serviceURLDomain + gServiceUrl + jobCustomerNumber + "/" + sessionStorage.facilityId;
            page_index = context_data.selectedPageValue();
            page_index = (page_index != undefined && page_index != null && page_index != "") ? page_index : 0;
            isSearched = true;
            if (context_data.prevSearchValue() != "")
                page_index = ((context_data.prevSearchValue().toLowerCase().indexOf(context_data.searchValue().toLowerCase()) == -1) || context_data.prevSearchValue() != context_data.searchValue()) ? 0 : parseInt(page_index);
            context_data.selectedPageValue(page_index);
            if (event != undefined) {
                window.setTimeout(function () {
                    getSelectedData(ctrl_id, context_data, selected_target, gServiceUrl_data, foundKeys[0], page_index, sort_order);
                }, 1000);
                //context_data.selectedPageValue(0);
            }
            else {
                context_data.selectedPageValue(0);
            }
            window.setTimeout(function () {
                $('#dv' + selected_target.replace(/ /g, '_')).find('input[data-type=search]').focus();
            }, 1200);
            //(ctrl, context_data, title, service_url, index_list_name, page_index, sort_order, search_term)
        }
    };
    self.searchWithEnterKey = function (data, event) {
        if (event.which == 13) {
            ko.contextFor($('#dv' + $('#hdnSelectedGroup').val().replace(/ /g, '_'))[0]).$parent.searchValue(event.target.value);
            //self.search(data, event);
            $('input[data-type=search]').eq(0).blur();
            return false;
        }
        return true;
    };

    self.refreshAdminEmailListConfirm = function () {
        $('#confirmMsg').html("This will update the current email configuration actions for this customer. Do you want to continue?");
        $('#okButConfirm').attr('onclick', 'ko.contextFor(this).$root.refreshAdminEmailList();');
        $('#okButConfirm').text('Ok');
        $('#popupConfirmDialog').popup('open');
        $('#popupConfirmDialog').popup('open');
    };

    self.refreshAdminEmailList = function () {
        getCORS(serviceURLDomain + "api/GizmoEmail_refresh/" + facilityId + "/" + jobCustomerNumber, null, function (data) {
            if (data == "success") {
                $('#alertmsg').text("Admin email actions config has been refreshed successfully.");
                $('#popupConfirmDialog').popup('close');
                window.setTimeout(function () {
                    $('#popupDialog').popup('open');
                }, 500);
            }
        }, function (response_error) {
            $('#alertmsg').text(response_error);
            $('#popupDialog').popup('open');
        });
    };

    self.getUserConfirmationForEmailAction = function (data) {
        var is_send_mail = true;
        if (data.startTime() == "" || data.endTime() == "") {
            is_send_mail = false;
        }
        if (is_send_mail) {
            var st_dt_parts = data.startTime().split('/');
            var st_date = new Date(st_dt_parts[2], st_dt_parts[0] - 1, st_dt_parts[1]).setHours(0, 0, 0, 0);

            var end_dt_parts = data.endTime().split('/');
            var end_date = new Date(end_dt_parts[2], end_dt_parts[0] - 1, end_dt_parts[1]).setHours(0, 0, 0, 0);

            var today = new Date().setHours(0, 0, 0, 0);
            if (today >= st_date && today <= end_date) {
                $('#confirmMsg').html("Are you sure you want to fire the " + data.name() + " email action using the options set up in the email config file?");
                $('#okButConfirm').attr('onclick', 'ko.contextFor(this).$root.sendEmailAction(\'' + data.type() + '\',\'' + data.id() + '\');');
                $('#okButConfirm').text('Ok');
                $('#popupConfirmDialog').popup('open');
                $('#popupConfirmDialog').popup('open');
            }
            else {
                is_send_mail = false;
            }
        }

        if (!is_send_mail) {
            $('#alertmsg').html("This email cannot be sent. It is outside of the configured start and end time range.");
            $('#popupDialog').popup('open');
        }
    };
    self.sendEmailAction = function (type, id) {
        getCORS(serviceURLDomain + "api/GizmoEmail_basic/" + facilityId + "/" + jobCustomerNumber + "/" + type + "/" + id, null, function (response) {
            if (response == "success") {
                $('#popupConfirmDialog').popup('close');
                var msg = "Your email action was successfully sent.";
                $('#alertmsg').html(msg);
                //$('#popupDialog').popup('open');
                window.setTimeout(function () {
                    $('#popupDialog').popup('open');
                }, 1000);
            }
        }, function (response_error) {
            $('#popupConfirmDialog').popup('close');
            showErrorResponseText(response_error, false);
            //$('#alertmsg').text(response_error.responseText || response_error.statusText);
            //window.setTimeout(function dispMsg() {
            //    $('#popupDialog').popup('open');
            //}, 500);
        });
    };
    self.getUserConfirmationForDelete = function (context_data) {
        $('#popupEditJob').popup('close');
        $('#confirmMsg').html("Are you sure you want to Delete this job? <br />This will delete all job instructions, associated databases and all site entries.");
        //$('#okButConfirm').attr('onclick', 'ko.contextFor(this).$root.deleteJob(ko.contextFor(this).$data,event);');
        $('#okButConfirm').bind('click', function (event) {
            self.deleteJob(context_data, event);
        });
        $('#popupJobStatus').popup('close');
        $('#popupConfirmDialog').popup('open');
        $('#popupConfirmDialog').popup('open');
    };

    self.openCalendarOptions = function () {
        $('#sldrShowPastDates').slider('refresh');
        $('#sldrShowCompletedJobs').slider('refresh');
        $('#popupCalendarOptions').popup('open', { positionTo: "#dvJobScheduling" });
    };
    self.deleteJob = function () {
        //var delete_job_json = { "customerNumber": data.selectedJob().jobCustomerNumber, "facilityId": data.selectedJob().facilityId, "jobNumber": data.selectedJob().jobNumber };
        //var delete_job_json = { "customerNumber": data.jobCustomerNumber, "facilityId": data.facilityId, "jobNumber": data.jobNumber };
        ///
        //var gdelete_service_url = serviceURLDomain + "api/Job_delete";
        //postCORS(gdelete_service_url, JSON.stringify(delete_job_json), function (response_data) {
        //    $('#popupConfirmDialog').popup('close');
        //    var msg = "The following resources have been deleted for Job Number<b>" + response_data.jobNumber + '</b>';
        //    msg += '<ul>';
        //    $.each(response_data, function (key, val) {
        //        if (key.toLowerCase() != "jobnumber") {
        //            msg += '<li>' + val + '</li>';
        //        }
        //    });
        //    msg += '</ul>';
        //    $('#alertmsg').html(msg);
        //    window.setTimeout(function () {
        //        $('#popupDialog').popup('open');
        //    }, 1000);
        //    var selected_target = $('#hdnSelectedGroup').val();
        //    var foundKeys = [];
        //    var page_index = 0;
        //    var context_data = "";
        //    var ctrl_id = 'dv' + selected_target.replace(/ /g, '_');
        //    if (selected_target != "") {
        //        foundKeys = Object.keys(gData.indexLists).filter(function (key) {
        //            return gData.indexLists[key] === selected_target;
        //        });
        //        context_data = ko.dataFor($('#dv' + selected_target.replace(/ /g, '_'))[0]);
        //        var sort_order = (sessionStorage.indexPageSortOrder != undefined && sessionStorage.indexPageSortOrder != null && sessionStorage.indexPageSortOrder != "") ? sessionStorage.indexPageSortOrder : null;
        //        //var gServiceUrl_data = serviceURLDomain + gServiceUrl + appPrivileges.customerNumber + "/" + sessionStorage.facilityId;
        //        var gServiceUrl_data = serviceURLDomain + gServiceUrl + jobCustomerNumber + "/" + sessionStorage.facilityId;
        //        page_index = context_data.selectedPageValue();
        //        page_index = (page_index != undefined && page_index != null && page_index != "") ? page_index : 0;
        //        page_index = (context_data.prevSearchValue().toLowerCase().indexOf(context_data.searchValue().toLowerCase()) == -1) ? 0 : parseInt(page_index);
        //        getSelectedData(ctrl_id, context_data, selected_target, gServiceUrl_data, foundKeys[0], page_index, sort_order);
        //    }
        //    //{"jobNumber":-14706,"database":"No database dropped for negative job numbers.","scheduler":"Deleted scheduler and other administrative entries.","dirProject":"Deleted project directories for this job."}
        //}, function (error_response) {
        //    $('#popupConfirmDialog').popup('close');
        //    showErrorResponseText(error_response, true);

        //});
        $('#popupConfirmDialog').popup('close');
    };
};

function openPopups(ctrl) {
    $('#' + ctrl).find('select').val('-1').selectmenu('refresh');
    $('#popupFilters').popup('open');
}

function getSelectedData(ctrl, context_data, title, service_url, index_list_name, page_index, sort_order) {
    $('#' + ctrl).data('collapsed', false);
    var post_json = {};
    //post_json["pageIndex"] = (context_data.prevSearchValue().toLowerCase().indexOf(context_data.searchValue().toLowerCase()) == -1 || context_data.searchValue() == "") ? 0 : parseInt(page_index); //(context_data.searchValue() == '') ? parseInt(page_index) : 0;
    post_json["pageIndex"] = page_index;
    post_json["indexListName"] = index_list_name;
    post_json["sortBy"] = sort_order;
    if (context_data.searchValue() != "")
        post_json["searchTerm"] = context_data.searchValue();

    //if (appPrivileges.roleName == "user")
    //    post_json["assignedList"] = [{ "userId": sessionStorage.username}];

    var filter_users = $('#ulSelectedUsers li a[value!=""][title!=Remove]').attr('value');
    if (filter_users != undefined && filter_users != null && filter_users != "")
        post_json["assignedList"] = [{ "userId": filter_users }];

    if (index_list_name == "myTaggedJobs") {
        post_json["assignedList"] = [{ "userId": sessionStorage.username }];
        //post_json["indexListName"] = 'openJobs';
    }
    if ($('#ddlFilterMilestones').length > 0 && $('#ddlFilterMilestones').val() != -1) {
        var ms_search = {};
        ms_search["milestone"] = $('#ddlFilterMilestones').val();
        ms_search["fromDate"] = $('#dtpStart').val();
        ms_search["toDate"] = $('#dtpEnd').val();
        post_json["milestoneSearch"] = ms_search;
    }
    //    "milestoneSearch":
    //{ "milestone": "inhome", "fromDate": "12/29/2014", "toDate": "01/01/2015" }

    var facility_id = "";
    facility_id = sessionStorage.facilityId;
    if ((appPrivileges.customerNumber == "1" /*|| appPrivileges.customerNumber == REDPLUM*/) && $('#ddlRegions').val() != "select")
        facility_id = $('#ddlRegions').val();
    post_json["facilityList"] = [{ "facilityId": facility_id }];
    //post_json["searchTerm"] = sort_order; //TO BE REMOVED
    var jobs_list = [];
    postCORS(service_url, JSON.stringify(post_json), function (response_data) {
        //$.getJSON(index_static_url, function (response_data) {
        jobSchedulingInfo = response_data.jobs;
        //jobs_list.push(new jobGroupModel(key, false, val));
        if (appPrivileges.roleName == "admin" && index_list_name == "jobsWithProcessingErrors") {// && response_data.jobs.length>0
            response_data.indexLists["jobsWithProcessingErrors"] = "Jobs With Processing Errors";
            response_data["jobs"] = processErrorData.jobs;
            response_data["target"] = processErrorData.target;
            response_data["totalCount"] = processErrorData.totalCount;
            response_data["startIndex"] = processErrorData.startIndex;
            response_data["finalIndex"] = processErrorData.finalIndex;
        }
        //if (appPrivileges.customerNumber == "1") {
        if (appPrivileges.roleName == "admin") {
            //My Tagged Jobs, Assigned Jobs 
            //response_data.indexLists["myTaggedJobs"] = "My Tagged Jobs";
            //response_data.indexLists["assignedJobs"] = "Assigned Jobs";
        }
        var job_name = "", in_home = "";
        if (response_data.jobs != undefined && response_data.jobs != null && response_data.jobs != "") {
            $.each(response_data.jobs, function (key, val) {
                job_name = (val["Job Name"] != undefined && val["Job Name"] != null && val["Job Name"] != "") ? val["Job Name"] : '';
                in_home = (val["In-Home"] != undefined && val["In-Home"] != null && val["In-Home"] != "") ? 'In-Home: <b>' + val["In-Home"] + '</b>' : '';
                /*if (val["Status"] == null && [SCOTTS, ANDERSEN, SHERWINWILLIAMS, BENJAMINMOORE, TRAEGER].indexOf(appPrivileges.customerNumber) > -1) {
                    val["Status"] = ((key == 0) ? '<span style="color:green;font-weight:bold;">APPROVED</span>' : ((key == 1) ? '<span style="color:crimson;font-weight:bold;">DECLINED</span>' : '<span style="font-weight:bold;">OPEN</span>'));
                }
                val["Status"] = ((val["Status"] == 1 || val["Status"] == "1") ? '<span style="color:green;font-weight:bold;">' + getApprovalStatus(val["Status"]).toUpperCase() + '</span>' :
                    ((val["Status"] == 0 || val["Status"] == "0") ? '<span style="color:crimson;font-weight:bold;">' + getApprovalStatus(val["Status"]).toUpperCase() + '</span>' : '<span style="font-weight:bold;">OPEN</span>'));*/

                var status = "";
                status = getApprovalStatus(val["Status"]);
                if (status.toLowerCase() == "approved" || status.toLowerCase() == "declined") {
                    val["Status"] = '<span style="color:' + ((status.toLowerCase() == "declined") ? 'crimson' : 'green') + ';font-weight:bold;">' + status.toUpperCase() + '</span>';
                }
                else {
                    val["Status"] = '<span style="font-weight:bold;">NONE</span>';
                }

                var in_home_theme = '';
                if (val["In-Home"] != undefined && val["In-Home"] != null && val["In-Home"] != "") {
                    var due_date = new Date(val["In-Home"]);
                    var curr_date = new Date();
                    var dt_diff = daysBetween(curr_date, due_date);
                    if (response_data.target.toLowerCase().indexOf('open') == -1)
                        in_home_theme = (dt_diff < 0) ? 'red' : '';
                }
                in_home = (in_home == "") ? ((val["Job Number"] != undefined && val["Job Number"] != null && val["Job Number"] != "") ? '<font color=' + in_home_theme + '>Job Number:<b>' + val["Job Number"] + '</b></font>' : '') : '<font color=' + in_home_theme + '>' + in_home + '</font>';
                in_home = (appPrivileges.customerNumber == DCA || appPrivileges.customerNumber == KUBOTA || appPrivileges.customerNumber == CW || appPrivileges.customerNumber == OH || appPrivileges.customerNumber == SPORTSKING || appPrivileges.customerNumber == GWA || appPrivileges.customerNumber == CASEYS || appPrivileges.customerNumber == REDPLUM || appPrivileges.customerNumber == BUFFALONEWS || appPrivileges.customerNumber == PIONEERPRESS || appPrivileges.customerNumber == EQUINOX || appPrivileges.customerNumber == GWA || isHideInHomeinJobsList()) ? "" : in_home;
                var index = 0;
                var previous_split_index = 0;
                var temp_attr = "";
                var filter_text = "";
                var break_index = ((appPrivileges.customerNumber == DCA || appPrivileges.customerNumber == KUBOTA || appPrivileges.customerNumber == CW || appPrivileges.customerNumber == OH || appPrivileges.customerNumber == SPORTSKING || appPrivileges.customerNumber == GWA || appPrivileges.customerNumber == CASEYS || appPrivileges.customerNumber == REDPLUM || appPrivileges.customerNumber == BUFFALONEWS || appPrivileges.customerNumber == PIONEERPRESS || appPrivileges.customerNumber == EQUINOX || appPrivileges.customerNumber == GWA || appPrivileges.customerNumber == ALLIED || isBreakIndexCustomized()) ? 2 : 3);
                var elements_toSkip = ["isGizmo", "Facility ID", "Customer Number", "Customer Name", "Job Name", "Created By User"];
                var attr_list = "";
                var temp_job_attr_key = "";
                $.each(val, function (job_attr_key, job_attr_val) {
                    if (job_attr_val != "" || job_attr_val == 0) {
                        if (((appPrivileges.customerNumber == CW || appPrivileges.customerNumber == OH || appPrivileges.customerNumber == CASEYS || appPrivileges.customerNumber == REDPLUM || appPrivileges.customerNumber == BUFFALONEWS || appPrivileges.customerNumber == PIONEERPRESS || appPrivileges.customerNumber == EQUINOX || isFilterAttributesRequired()) && (job_attr_key != "isGizmo")) || (appPrivileges.customerNumber != DCA && appPrivileges.customerNumber != KUBOTA && appPrivileges.customerNumber != CW && appPrivileges.customerNumber != OH && appPrivileges.customerNumber != SPORTSKING && appPrivileges.customerNumber != GWA && appPrivileges.customerNumber != CASEYS && appPrivileges.customerNumber != REDPLUM && appPrivileges.customerNumber != BUFFALONEWS && appPrivileges.customerNumber != PIONEERPRESS && appPrivileges.customerNumber != EQUINOX && appPrivileges.customerNumber != ALLIED && !isFilterAttributesRequired()) || ((appPrivileges.customerNumber == DCA || appPrivileges.customerNumber == KUBOTA || appPrivileges.customerNumber == SPORTSKING || appPrivileges.customerNumber == GWA || appPrivileges.customerNumber == ALLIED) && (jQuery.inArray(job_attr_key, elements_toSkip) == -1))) {
                            filter_text += (filter_text != "") ? ' | ' + job_attr_val : job_attr_val;
                            //if (job_attr_key == "In-Home") date_name = job_attr_key;
                            //if (job_attr_key != "Job Name" && job_attr_key != "In-Home" && job_attr_key != "Customer Number" && job_attr_key != "Customer Name" && job_attr_key != "Facility ID") {
                            temp_attr = ((val["In-Home"] == undefined || val["In-Home"] == null) && job_attr_key == "Job Number") ? "" : job_attr_key;
                            if (temp_attr != "") {
                                temp_job_attr_key = ((appPrivileges.customerNumber == DCA || appPrivileges.customerNumber == KUBOTA || appPrivileges.customerNumber == SPORTSKING || appPrivileges.customerNumber == GWA) && (job_attr_key == "Entry Point")) ? "Entry" : job_attr_key;
                                if ((index - previous_split_index) == break_index) {
                                    attr_list += "<br/>";
                                }
                                if (temp_job_attr_key == "Size" && job_attr_val == "x") {
                                    var size_val = '';
                                    attr_list += (attr_list != "") ? ((((index - previous_split_index) < break_index) ? " | " : "") + temp_job_attr_key + ": " + size_val) : "<p style='margin-top:-2px;'>" + (temp_job_attr_key + ": " + size_val);
                                }
                                else
                                    attr_list += (attr_list != "") ? ((((index - previous_split_index) < break_index) ? " | " : "") + temp_job_attr_key + ": " + ((job_attr_val != null) ? job_attr_val : "")) : "<p style='margin-top:-2px;'>" + (temp_job_attr_key + ": " + ((job_attr_val != null) ? job_attr_val : ""));
                            }
                            if ((index - previous_split_index) == break_index)
                                previous_split_index = index;
                            index++;
                        }
                    }
                });
                if (attr_list != "")
                    attr_list += '</p>';
                var mile_stone_icon = getMilestoneIcon(val);
                var mile_stone_alt_text = mile_stone_icon.substring(mile_stone_icon.lastIndexOf('/') + 1, mile_stone_icon.lastIndexOf('.')).replace('icon', '').replace('warning', '').replace(/_/g, ' ');
                var job_number = (val["Job Number"] != undefined && val["Job Number"] != null && val["Job Number"] != "") ? parseInt(val["Job Number"]) : '';
                //var facility_id = (val["Facility ID"] != undefined && val["Facility ID"] != null) ? eval(val["Facility ID"]) : '1';
                //sessionStorage.facilityId = facility_id;
                var job_customer_number = (val["Customer Number"] != undefined && val["Customer Number"] != null && val["Customer Number"] != "") ? parseInt(val["Customer Number"]) : appPrivileges.customerNumber;
                var data_theme = '';
                if (job_number > -1)
                    if (val["isGizmo"] != undefined && val["isGizmo"] != null && val["isGizmo"] == 0) {
                        data_theme = 'j';
                    }
                    else
                        data_theme = 'c';
                else
                    if (val["isGizmo"] != undefined && val["isGizmo"] != null && val["isGizmo"] == 0) {
                        data_theme = 'j';
                    }
                    else
                        data_theme = 'g';

                var job_item = new JobModel(job_name, in_home, attr_list, filter_text, data_theme, job_number, job_customer_number, sessionStorage.facilityId, mile_stone_icon, mile_stone_alt_text, val["isGizmo"]);

                //var job_item = new JobModel(job_name, in_home, attr_list, filter_text, ((job_number > -1) ? '' : 'e'), job_number, job_customer_number, facility_id, mile_stone_icon, mile_stone_alt_text, val["isGizmo"]);
                jobs_list.push(job_item);
            });
        }
        //var is_show_region = (appPrivileges.customerNumber == "1" || appPrivileges.customerNumber == REDPLUM) ? true : false;
        //var paging_info = createPagerObject(val);
        //context_data.listViewData(jobs_list);
        //col1_data.push(new jobGroupModel(context_data.groupTitle, is_show_region, jobs_list, '', '1', '', sessionStorage.facilityId));
        //$('#' + ctrl).trigger('expand');
        context_data.prevSearchValue(context_data.searchValue());
        context_data.regionTitle = getTribuneFacilityName(sessionStorage.facilityId);
        context_data.pagingData.removeAll();
        context_data.pagingData(createPagerObject1(response_data.totalCount));
        //var selected_page_value = (context_data.selectedPageValue() != undefined && context_data.selectedPageValue() != null && context_data.selectedPageValue() != "") ? context_data.selectedPageValue() : 0;
        var is_more_visible = context_data.pagingData().length > 0 ? true : false;//(context_data.pagingData().length > 0 && parseInt(selected_page_value + 1) < context_data.pagingData().length) ? true : false;
        context_data.showMoreOption(is_more_visible);
        context_data.listViewData.removeAll();
        context_data.listViewData(jobs_list);

        isSearched = false;
        //        if (jobs_list.length == 0)
        //            context_data.searchValue('');
        //$("#col2").trigger("create");
        //$("#" + +ctrl_id.replace('btn', '')).find('ul').trigger("create").listview().listview('refresh');
        var ctrl_id_target = ctrl;
        if (ctrl.indexOf('btn') > -1 || ctrl.indexOf('ddl') > -1)
            ctrl_id_target = 'dv' + ctrl.replace('ddl', '').replace('btn', '').replace('_Pop1', '').replace('_Pop2', '');
        // || (jobCustomerNumber == AAG && appPrivileges.roleName != "admin" && !is_valid_domain)
        if (jobCustomerNumber == BBB || jobCustomerNumber == ALLIED || jobCustomerNumber == DCA || jobCustomerNumber == KUBOTA || jobCustomerNumber == SPORTSKING || jobCustomerNumber == GWA || (jobCustomerNumber == AAG && appPrivileges.roleName == "admin" /* && !is_valid_domain*/)) {
            //$("#demoListJobs").find('ul').trigger("create").listview().listview('refresh');
            //$('#demoListJobs').collapsibleset('refresh');
            $("#demoListJobs").find('ul').listview().listview('refresh');
            $("#demoListJobs").find('ul li div[data-role=controlgroup]').controlgroup();
            $("#demoListJobs").find('ul li div[data-role=controlgroup]').find('input[type=text]').parent().trigger('create');
            $("div[id='demoListJobs']").find('ul').find('select').selectmenu().selectmenu('refresh');
            $("div[id='demoListJobs']").find('ul').find("input[data-type=search]").parent().trigger('create');
            $('#demoListJobs').collapsibleset('refresh');
            $('.thermometer-noconfig').thermometer({
                speed: 'slow'
            });
            $('#dvHeader' + ctrl_id_target.replace('dv', '').replace('_txt', '')).css('display', 'none');
            //$('#demoListJobs').trigger('create');
            $('#waitPopUp').popup('close');
            if (jobCustomerNumber == AAG && appPrivileges.roleName == "admin")
                $('#demoListJobs').addClass('ui-corner-all');
        }
        else if (jobCustomerNumber == JETS || jobCustomerNumber == CASEYS || jobCustomerNumber == CW || jobCustomerNumber == SPORTSAUTHORITY || jobCustomerNumber == WALGREENS || jobCustomerNumber == SAFEWAY || jobCustomerNumber == LOWES || jobCustomerNumber == REDPLUM || jobCustomerNumber == BUFFALONEWS || jobCustomerNumber == PIONEERPRESS || jobCustomerNumber == EQUINOX || isRefreshJetsDemoJobsDiv()) {
            //$("#dvJetsDemoJobs").find('ul').trigger("create").listview().listview('refresh');


            //$("#btnSettings").button();
            //$('#btnSettings').parent().css('margin-top', '2px');
            //$('#btnSettings').parent().css('margin-bottom', '2px');
            if (jobCustomerNumber == ACE) {
                $("#List2").find('ul').listview().listview('refresh');
                $("#List2").find('ul li div[data-role=controlgroup]').controlgroup();
                $("#List2").find('ul li div[data-role=controlgroup]').find('input[type=text]').parent().trigger('create');
                $("#List2").find('ul').find('select').selectmenu().selectmenu('refresh');
                $("#List2").find('ul').find("input[data-type=search]").parent().trigger('create');
                $('#List2').collapsibleset('refresh');
                $("#ulDriveCalendar").listview().listview('refresh');
                $('#dvJobScheduling').collapsibleset('refresh');
            }
            else {
                $("#dvJetsDemoJobs").find('ul').listview().listview('refresh');
                $("#dvJetsDemoJobs").find('ul li div[data-role=controlgroup]').controlgroup();
                $("#dvJetsDemoJobs").find('ul li div[data-role=controlgroup]').find('input[type=text]').parent().trigger('create');
                $("div[id='dvJetsDemoJobs']").find('ul').find('select').selectmenu().selectmenu('refresh');
                $("div[id='dvJetsDemoJobs']").find('ul').find("input[data-type=search]").parent().trigger('create');
                $('#dvJetsDemoJobs').collapsibleset('refresh');
            }
            $('.thermometer-noconfig').thermometer({
                speed: 'slow'
            });
            $('#dvHeader' + ctrl_id_target.replace('dv', '').replace('_txt', '')).css('display', 'none');
            //$('#dvJetsDemoJobs').trigger('create');
            $('#waitPopUp').popup('close');
        }
        else {
            $("#" + ctrl_id_target).find('ul').listview().listview('refresh');
            if (ctrl_id_target.indexOf('dv') != 0)
                ctrl_id_target = "dv" + ctrl_id_target;

            $("div[id='" + ctrl_id_target + "']").find('ul').listview().listview('refresh');
            $("div[id='" + ctrl_id_target + "']").find('ul li div[data-role=controlgroup]').controlgroup();
            $("div[id='" + ctrl_id_target + "']").find('ul li div[data-role=controlgroup]').find('input[type=text]').parent().trigger('create');
            $("div[id='" + ctrl_id_target + "']").find('ul').find('select').selectmenu().selectmenu('refresh');
            $("div[id='" + ctrl_id_target + "']").find('ul').find("input[data-type=search]").parent().trigger('create');
            $("div[id='" + ctrl_id_target + "']").collapsibleset().collapsibleset('refresh');
            $('#dvHeader' + ctrl_id_target.replace('dv', '').replace('_txt', '')).css('display', 'none');
            //$("#" + ctrl_id_target).find('ul').trigger("create").listview().listview('refresh');
            //window.setTimeout(function setUI() {
            //    $("div[id='dv" + ctrl_id_target + "']").find('ul').listview('refresh').trigger('create');
            //    $("div[id='dv" + ctrl_id_target + "']").find('ul').find('select').selectmenu('refresh').trigger('create');
            //    $("div[id='dv" + ctrl_id_target + "']").find('ul').find("input[data-type=search]").trigger('create');
            //}, 5);
            $('.thermometer-noconfig').thermometer({
                speed: 'slow'
            });
            $('#waitPopUp').popup('close');
        }
        if ((jobCustomerNumber == KUBOTA /* || jobCustomerNumber == SCOTTS*/) && (sessionStorage.isCoopDisplayed == undefined || sessionStorage.isCoopDisplayed == null || sessionStorage.isCoopDisplayed == "" || !JSON.parse(sessionStorage.isCoopDisplayed))) {
            sessionStorage.isCoopDisplayed = true;
            loadPopup();
        }
        if (!featureStatusThermometer()) {
            $("#tblStatusThermometer").css("display", "none");
            $("#tblStatusThermometer tbody").css("display", "none");
        }

    }, function (error_response) {
        showErrorResponseText(error_response, true);
        //var is_show_region = (appPrivileges.customerNumber == "1" || appPrivileges.customerNumber == REDPLUM) ? true : false;
        //var paging_info = createPagerObject(val);
        //context_data.listViewData(jobs_list);
        //col1_data.push(new jobGroupModel(context_data.groupTitle, is_show_region, jobs_list, '', '1', '', sessionStorage.facilityId));
        //$('#' + ctrl).trigger('expand');
        context_data.prevSearchValue(context_data.searchValue());
        context_data.regionTitle = getTribuneFacilityName(sessionStorage.facilityId);
        context_data.pagingData(createPagerObject1(0));
        var selected_page_value = (context_data.selectedPageValue() != undefined && context_data.selectedPageValue() != null && context_data.selectedPageValue() != "") ? context_data.selectedPageValue() : 0;
        var is_more_visible = (context_data.pagingData().length > 0 && parseInt(selected_page_value + 1) < context_data.pagingData().length) ? true : false;
        context_data.showMoreOption(is_more_visible);
        context_data.listViewData.removeAll();
        context_data.listViewData(jobs_list);

        isSearched = false;
        //        if (jobs_list.length == 0)
        //            context_data.searchValue('');
        //$("#col2").trigger("create");
        //$("#" + +ctrl_id.replace('btn', '')).find('ul').trigger("create").listview().listview('refresh');
        var ctrl_id_target = ctrl;
        if (ctrl.indexOf('btn') > -1 || ctrl.indexOf('ddl') > -1)
            ctrl_id_target = 'dv' + ctrl.replace('ddl', '').replace('btn', '');

        $("#" + ctrl_id_target).find('ul').trigger("create").listview().listview('refresh');
        window.setTimeout(function () {
            $("div[id='dv" + ctrl_id_target + "']").find('ul').listview('refresh').trigger('create');
            $("div[id='dv" + ctrl_id_target + "']").find('ul').find('select').selectmenu('refresh').trigger('create');
            $("div[id='dv" + ctrl_id_target + "']").find('ul').find("input[data-type=search]").trigger('create');
            if (jobCustomerNumber == KUBOTA /* || jobCustomerNumber == SCOTTS*/) {
                sessionStorage.isCoopDisplayed = true;
                loadPopup();
            }
            $('#waitPopUp').popup('close');
        }, 5);
    });
}


function makeGData() {
    var sort_order = (sessionStorage.indexPageSortOrder != undefined && sessionStorage.indexPageSortOrder != null && sessionStorage.indexPageSortOrder != "") ? sessionStorage.indexPageSortOrder : null;
    var page_url = serviceURLDomain + gServiceUrl + jobCustomerNumber + "/" + sessionStorage.facilityId;
    $.each($('input[id^=search]'), function () {
        $(this).val('');
    });
    gDataStartIndex = 0;
    gDataList = [];
    var post_json = {};
    post_json["pageIndex"] = 0;
    post_json["indexListName"] = "";
    post_json["sortBy"] = sort_order;
    // post_json["searchTerm"] = ""; // TO BE Removed
    //if (appPrivileges.roleName == "user")
    //    post_json["assignedList"] = [{ "userId": sessionStorage.username}];

    var facility_id = "";
    facility_id = "1";
    if ($('#ddlRegions').length > 0 && $('#ddlRegions').val() != "select")
        facility_id = $('#ddlRegions').val();
    post_json["facilityList"] = [{ "facilityId": facility_id }];
    if (vm != undefined && vm != null && vm.person() != undefined) {
        var selected_target = $('#hdnSelectedGroup').val();
        var foundKeys = [];
        var page_index = 0;
        var context_data = "";

        if (selected_target != "") {
            var ctrl_id = 'dv' + selected_target.replace(/ /g, '_');
            foundKeys = Object.keys(gData.indexLists).filter(function (key) {
                return gData.indexLists[key] === selected_target;
            });
            context_data = ko.dataFor($('#dv' + selected_target.replace(/ /g, '_'))[0]);
            page_index = context_data.selectedPageValue();
            page_index = (page_index != undefined && page_index != null && page_index != "") ? page_index : 0;
            var gServiceUrl_data = "";
            sort_order = (sessionStorage.indexPageSortOrder != undefined && sessionStorage.indexPageSortOrder != null && sessionStorage.indexPageSortOrder != "") ? sessionStorage.indexPageSortOrder : null;
            var index_list_name = (foundKeys.length > 0) ? foundKeys[0] : "";
            gServiceUrl_data = serviceURLDomain + gServiceUrl + jobCustomerNumber + "/" + sessionStorage.facilityId;
            getSelectedData(ctrl_id, context_data, context_data.groupTitle, gServiceUrl_data, index_list_name, page_index, sort_order);
        }
    } else {
        postCORS(page_url, JSON.stringify(post_json), function (data) {
            //$.getJSON(index_static_url, function (data) {
            jobSchedulingInfo = data.jobs;
            gData = data;
            if (jobCustomerNumber == AAG /*&& (is_valid_domain || appPrivileges.roleName == "admin")*/) {
                if (data.defaultJobName != undefined && data.defaultJobName != null && data.defaultJobNumber != undefined && data.defaultJobNumber != null) {
                    var temp_default_job_info = {};
                    temp_default_job_info["defaultJobName"] = data.defaultJobName;
                    temp_default_job_info["defaultJobNumber"] = data.defaultJobNumber;
                    sessionStorage.defaultJobDetails = JSON.stringify(temp_default_job_info);

                    //if (data.defaultJobNumber != "-1" && appPrivileges.roleName != "admin") {
                    if (appPrivileges.roleName != "admin" && data.defaultJobName != null && data.defaultJobName != "" && data.defaultJobNumber != null && data.defaultJobNumber != "" && data.defaultJobNumber != 0) {
                        $('#lnkNewJob').css('display', 'none');
                        $('#lnkEditJob').css('display', 'block');
                    }
                    else if (appPrivileges.roleName != "admin") {
                        $('#lnkNewJob').css('display', 'none');

                    }
                }
                else if (appPrivileges.roleName != "admin") {
                    $('#lnkNewJob').css('display', 'none');
                    $('#lnkEditJob').css('display', 'none');
                }
                /* if (data.defaultJobNumber == "-1" && (is_valid_domain || appPrivileges.roleName == "admin"))
                     data.defaultJobNumber = "-16875";*/
            }
            if (appPrivileges.roleName == "admin") {
                //My Tagged Jobs, Assigned Jobs 
                //gData.indexLists["myTaggedJobs"] = "My Tagged Jobs";
                //gData.indexLists["assignedJobs"] = "Assigned Jobs";
            }
            if (vm == undefined) {
                vm = new viewModel();
                vm.person(new makeViewModel());
                ko.applyBindings(vm);
                $("#col2 div[data-role=collapsible-set]").collapsibleset('refresh');
                $("#col3 div[data-role=collapsible-set]").collapsibleset('refresh');

                if (appPrivileges.customerNumber != "1"/* && appPrivileges.customerNumber != REDPLUM*/) {
                    //$('div.ui-collapsible-content', '#dvReports').addClass('ui-corner-all');
                    //$('div.ui-collapsible-content', "#dvReports").trigger('expand');
                }
                var source = (!isJetsDemoJobsDivCustomer()) ? "div[id=col2]" : (jobCustomerNumber == BBB || (jobCustomerNumber == AAG && /*!is_valid_domain*/ appPrivileges.roleName == "admin") || jobCustomerNumber == TRIBUNEPUBLISHING || jobCustomerNumber == AH || jobCustomerNumber == ALLIED || jobCustomerNumber == DCA || jobCustomerNumber == SPORTSKING || jobCustomerNumber == KUBOTA || jobCustomerNumber == GWA) ? ((jobCustomerNumber == ACE) ? "div[id=List2]" : "div[id=demoListJobs]") : ((jobCustomerNumber == ACE) ? "div[id=List2]" : "div[id = dvJetsDemoJobs]");
                $.each($(source + ' div[data-role=collapsible]'), function () {
                    $(this).on("collapsibleexpand", function () {
                        $(this).data("previous-state", "expanded");
                        $('#' + this.id + 'a.ui-input-clear').bind('onclick', function () { });
                        $('.ui-content').on('click', '.ui-input-clear', function () {
                            vm.search();
                        });
                        vm.getData(this.id);
                    });

                    $(this).on("collapsiblecollapse", function () {
                        $(this).data("previous-state", "collapsed");
                    });
                });

                if ((jobCustomerNumber == AAG && /*!is_valid_domain && appPrivileges.roleName != "admin"*/ appPrivileges.roleName == "admin") || isExpandSourceRequired()) {
                    source = ((jobCustomerNumber == AAG && /*!is_valid_domain && appPrivileges.roleName != "admin"*/ appPrivileges.roleName == "admin") || jobCustomerNumber == DCA || jobCustomerNumber == KUBOTA || jobCustomerNumber == SPORTSKING || jobCustomerNumber == GWA || jobCustomerNumber == ALLIED) ? ((jobCustomerNumber == ACE) ? "List2" : "demoListJobs") : ((jobCustomerNumber == ACE) ? "List2" : "dvJetsDemoJobs");
                    window.setTimeout(function () {
                        $('#' + source + ' div[data-role=collapsible]').eq(0).collapsible('expand');
                    }, 100);
                }

            }

            vm.loadTagRegionsDDL(facilityJSON, 'filter');
            vm.loadDepartmentsDDL(departmentsJson, 'filter');
            vm.loadTagUsersDDL(usersJson, 'filter');
            $('#fldSetFilterOptions').controlgroup('refresh');
            window.setTimeout(function () {
                $('#fldSetFilterOptions').controlgroup('refresh');
                $('#ulEmailList').listview('refresh');
            }, 2000);

        }, function (error_response) {
            showErrorResponseText(error_response, true);
        });
    }
}

function getPendingApprovalPercent(job_name, job_number) {
    var env_id = getEnvironmentId();
    if (sessionStorage.caseysThermometersStatus != undefined && sessionStorage.caseysThermometersStatus != null && sessionStorage.caseysThermometersStatus != "" && env_id == 3) {
        var job_schedule = $.parseJSON(sessionStorage.caseysThermometersStatus);
        var curr_date = new Date();
        var percent = "";
        var prev_end_date = "";
        var index = 0;
        if (job_schedule[job_number] != undefined && job_schedule[job_number] != null && job_schedule[job_number] != "") {
            $.each(job_schedule[job_number], function (key, val) {
                if (key.toLowerCase() == "optin" && curr_date < (new Date(val.startDate))) {
                    percent = 0;
                }
                else if ((curr_date <= (new Date(val.endDate))) && (index == 0 || (prev_end_date != "" && curr_date > prev_end_date))) {
                    percent = val.percent;
                    return false;
                }
                //else if (prev_end_date != "" && curr_date > prev_end_date && curr_date <= (new Date(val.endDate))) {
                //    percent = val.percent;
                //    return false;
                //}
                prev_end_date = new Date(val.endDate);
                index++;
            });
        }
        var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        var job_month = job_name.substring(0, job_name.indexOf(' '));
        var job_year = job_name.substring(job_name.indexOf(' ') + 1, ((job_name.indexOf(' ') + 1) + 4));
        job_month = (months.indexOf(job_month) > -1) ? months.indexOf(job_month) : -1;
        var curr_month = (new Date()).getMonth();
        var curr_year = (new Date()).getFullYear();
        if (percent == 0 && job_month > -1 && (job_month < curr_month && job_year >= curr_year))
            percent == "0";
        else if (percent == "")
            percent = "100";
        return percent;
    } else {
        var count = 0;
        for (var x = 0, c = ''; c = job_name.charAt(x) ; x++) {
            count += c.charCodeAt(0);
        }
        count = count % 100;
        count = count < 15 ? 100 : count;
        return count;
    }
};

function getPendingApprovalStatus(pending_approval_percent) {
    if (pending_approval_percent <= 15)
        return 'Open';
    else if (pending_approval_percent <= 30)
        return 'In Processing';
    else if (pending_approval_percent <= 45)
        return 'In Approval';
    else if (pending_approval_percent <= 60)
        return 'In Production';
    else if (pending_approval_percent <= 75)
        return 'In Shipping';
    else if (pending_approval_percent <= 90)
        return 'Mailed';
    else if (pending_approval_percent <= 100)
        return 'In Home';
    else
        return '';
}

function makeViewModel() {
    var self = this;
    var job_name = "";
    var jobs_list = [];
    var attr_list = "";
    var grouped_jobs_col1 = [];
    var grouped_jobs_col2 = [];
    var is_show_region = false, show_tagging = false;
    var paging_info = [];
    ko.utils.extend(self, new jobScheduling(self, jobSchedulingInfo)); // Extends the view model to integrate the locations configuration api.
    self.makeJobSchedulingData();
    self.col1GroupWisePagedJobs = []; // ko.observableArray([]);
    self.col2GroupWisePagedJobs = []; // ko.observableArray([]);
    self.myTaskSummary = []; // ko.observableArray([]);
    self.demoEmailsList = [];
    $.each(gData.indexLists, function (key, val) {
        if (jobCustomerNumber != TRIBUNEPUBLISHING && jobCustomerNumber != SANDIEGO && jobCustomerNumber != AH) {
            if (!isGroupedJobsColumn2()) {
                jobs_list = [];
                is_show_region = (appPrivileges.customerNumber == "1" /*|| appPrivileges.customerNumber == REDPLUM*/) ? true : false;
                paging_info = createPagerObject1(jobs_list.length);
                //var show_tagging = (appPrivileges.roleName == "admin" && appPrivileges.customerNumber == "1") ? true : false;
                show_tagging = false;
                grouped_jobs_col1.push(new jobGroupModel(val, is_show_region, jobs_list, paging_info, '0', '', sessionStorage.facilityId, show_tagging, true));
            }
                //else if (((((jobCustomerNumber != SPORTSAUTHORITY && jobCustomerNumber != WALGREENS && jobCustomerNumber != SAFEWAY && jobCustomerNumber != LOWES) && appPrivileges.roleName == "admin") || jobCustomerNumber == BBB || jobCustomerNumber == SPORTSAUTHORITY || jobCustomerNumber == WALGREENS || jobCustomerNumber == MOTIV8 || jobCustomerNumber == ALLIED || jobCustomerNumber == SAFEWAY || jobCustomerNumber == LOWES || jobCustomerNumber == CW || jobCustomerNumber == DCA || jobCustomerNumber == KUBOTA || jobCustomerNumber == OH || jobCustomerNumber == SPORTSKING || jobCustomerNumber == GWA) && val.toLowerCase() == "open jobs") || (jobCustomerNumber == CASEYS && appPrivileges.roleName != "user" && (val.toLowerCase() == "open jobs")) || (jobCustomerNumber == CASEYS && appPrivileges.roleName != "admin" && val.toLowerCase() == "open jobs")) {
            else if ((((([SPORTSAUTHORITY, WALGREENS, SAFEWAY, LOWES].indexOf(jobCustomerNumber) == -1) && appPrivileges.roleName == "admin") || (jobCustomerNumber == AAG && /*!is_valid_domain*/ appPrivileges.roleName == "admin") || (([SPORTSAUTHORITY, WALGREENS, SAFEWAY, LOWES].indexOf(jobCustomerNumber) == -1) && isGroupedJobsColumn2())) && val.toLowerCase() == "open jobs") || (jobCustomerNumber == CASEYS && appPrivileges.roleName != "user" && (val.toLowerCase() == "open jobs")) || (jobCustomerNumber == CASEYS && appPrivileges.roleName != "admin" && val.toLowerCase() == "open jobs")) {
                jobs_list = [];
                is_show_region = (appPrivileges.customerNumber == "1" /*|| appPrivileges.customerNumber == REDPLUM*/) ? true : false;
                paging_info = createPagerObject1(jobs_list.length);
                //var show_tagging = (appPrivileges.roleName == "admin" && jobCustomerNumber != BBB && jobCustomerNumber != AAG && jobCustomerNumber != TRIBUNEPUBLISHING && jobCustomerNumber != SANDIEGO && jobCustomerNumber != AH && jobCustomerNumber != SPORTSAUTHORITY && jobCustomerNumber != WALGREENS && jobCustomerNumber != MOTIV8 && jobCustomerNumber != SAFEWAY && jobCustomerNumber != LOWES && jobCustomerNumber != CW && jobCustomerNumber != CASEYS) ? true : false;
                show_tagging = false;
                if (jobCustomerNumber == ACE)
                    grouped_jobs_col1.push(new jobGroupModel(val, is_show_region, jobs_list, paging_info, '0', '', sessionStorage.facilityId, show_tagging, true));
                else
                    grouped_jobs_col2.push(new jobGroupModel(val, is_show_region, jobs_list, paging_info, '0', '', sessionStorage.facilityId, show_tagging, true));
            }
        }
    });
    if ((jobCustomerNumber == BBB || jobCustomerNumber == JETS || jobCustomerNumber == CASEYS || jobCustomerNumber == AAG || jobCustomerNumber == TRIBUNEPUBLISHING || jobCustomerNumber == SANDIEGO || jobCustomerNumber == AH || jobCustomerNumber == EQUINOX || isUpdateDemoEmailList()) && appPrivileges.roleName == "admin") {
        $.each(demoEmailsList, function (key, val) {
            self.demoEmailsList.push(new demoEmailListModel(val));
        });
    }
    if (appPrivileges.customerNumber == "1" /*|| appPrivileges.customerNumber == REDPLUM*/) {
        var my_tasks_summary = [];
        var tasks_cnt = "";
        var temp_task_list = [];
        is_show_region = (appPrivileges.customerNumber == "1" /*|| appPrivileges.customerNumber == REDPLUM*/) ? true : false;
        var total_tasks_cnt = 0;
        $.each(dailyTaskUrlData, function (key, val) {
            paging_info = createPagerObject1(val.length);
            $.each(val, function (key1, val1) {
                tasks_cnt = '<b>Tasks: ' + val1["Tasks"] + '</b>';
                job_name = (val1["Job Name"] != undefined && val1["Job Name"] != null && val1["Job Name"] != "") ? val1["Job Name"] : '';
                var index = 0;
                var previous_split_index = 0;
                var temp_attr = "";
                var filter_text = "";
                attr_list = "";
                $.each(val1, function (job_attr_key, job_attr_val) {
                    if (job_attr_key != "Tasks") {
                        filter_text += (filter_text != "") ? ' | ' + job_attr_val : job_attr_val;
                        temp_attr = (job_attr_key == "Job Name") ? "" : job_attr_key;
                        //if (job_attr_key == "Job Name") temp_attr = ""; else temp_attr = job_attr_key;
                        if (job_attr_val != "" || job_attr_val == 0) {
                            if (temp_attr != "") {
                                if ((index - previous_split_index) == 3) {
                                    attr_list += "<br/>";
                                }
                                attr_list += (attr_list != "") ? ((((index - previous_split_index) < 3) ? " | " : "") + job_attr_key + ": " + ((job_attr_val != null) ? job_attr_val : "")) : "<p>" + (job_attr_key + ": " + ((job_attr_val != null) ? job_attr_val : ""));
                                if ((index - previous_split_index) == 3)
                                    previous_split_index = index;
                                index++;
                            }
                        }
                    }
                });
                if (attr_list != "")
                    attr_list += '</p>';
                //temp_task_list.push(new DailyTaskModel(job_name, "", attr_list, 'c', '', val1.Status, '', ((appPrivileges.roleName == "admin") ? 1 : 0)));
                temp_task_list.push(new JobModel(job_name, tasks_cnt, attr_list, filter_text, 'c', val1["Job Number"], appPrivileges.customerNumber, sessionStorage.facilityId, '', '', 0));
                total_tasks_cnt = parseInt(total_tasks_cnt) + parseInt(val1["Tasks"]);
            });
            my_tasks_summary.push(new jobGroupModel("My Task Summary", is_show_region, temp_task_list, paging_info, 0, '', sessionStorage.facilityId, show_tagging));
        });
        self.totalTasksCount = ko.observable();
        self.totalTasksCount(total_tasks_cnt);
        self.myTaskSummary = my_tasks_summary;
    }
    self.col1GroupWisePagedJobs = grouped_jobs_col1;
    self.col2GroupWisePagedJobs = grouped_jobs_col2;
    self.searchedJobs = ko.observableArray([]);
}

function manageFocusAndCursor(ctrl) {
    //var strLength = $(ctrl).val().length;
    //ctrl[0].setSelectionRange(strLength, strLength);
    //ctrl.focus(); //sets focus to element
    var val = ctrl.value; //store the value of the element
    ctrl.value = ''; //clear the value of the element
    ctrl.value = val; //set that value back.  
}

function createPagerObject1(total_jobs) {
    var pager_length = Math.ceil(total_jobs / gPageSize);
    var pager_object = [];
    if (pager_length > 1) {
        for (var j = 0; j < pager_length; j++) {
            pager_object.push({ 'name': 'Page ' + (j + 1), 'value': (j) });
        }
    }
    return pager_object;
}


function createPagerObject(val) {
    var pager_length = Math.ceil(val.length / gPageSize);
    var pager_object = [];
    if (pager_length > 1) {
        for (var j = 0; j < pager_length; j++) {
            pager_object.push({ 'name': 'Page ' + (j + 1), 'value': (j + 1) });
        }
    }
    return pager_object;
}


function getMilestoneIcon(job_attrs) {
    var one_day = 1000 * 60 * 60 * 24;
    var dt_diff = "";
    var due_date = "";
    var icon_type = '';
    var curr_date = new Date();
    var job_mile_stones = job_attrs["Status"];
    var mile_stone_key = '';
    //var attr_val = "";
    //var val_scheduled_date = "";
    $.each(job_attrs, function (key, val) {
        //attr_val = "";
        if (job_mile_stones != undefined && job_mile_stones != null && job_mile_stones != '' && job_mile_stones.indexOf(key.toLowerCase().replace(/ /g, '')) > -1) {
            mile_stone_key = key.toLowerCase().replace(/ /g, '_');
            //if (val == "1/1/1900 12:00:00 AM")
            //    attr_val = "";
            if (val != undefined && val != null && val != "") { //  
                due_date = new Date(val);
                //val_scheduled_date = (val.indexOf(' ') == -1) ? val : val.substring(0, val.indexOf(' '));
                dt_diff = Math.floor((due_date.getTime() - curr_date.getTime()) / one_day);
                if (dt_diff < 0) {
                    icon_type = '_warning';
                    return false;
                }
            }
        }
    });
    var mile_stone_icon = "";


    if (mile_stone_key.indexOf('in-home') > -1)
        mile_stone_icon = 'icon_in_home';
    else {
        //if (job_mile_stones.indexOf('start') > -1 || job_mile_stones.indexOf('end') > -1)
        //    mile_stone_icon = 'icon' + job_mile_stones.substring(0, (job_mile_stones.indexOf('start') || job_mile_stones.indexOf('end'))).replace(/ /g, '').replace(/-/g, '').replace(/_/g, '');
        //else
        mile_stone_icon = 'icon' + job_mile_stones;
    }

    if (mile_stone_icon.endsWith('_'))
        mile_stone_icon = mile_stone_icon.substring(0, mile_stone_icon.length - 1);
    if (icon_type != '')
        mile_stone_icon = mile_stone_icon.toLowerCase() + 'warning.png';
    else if (mile_stone_icon.length > 8)
        mile_stone_icon = mile_stone_icon.toLowerCase() + '.png';

    return (mile_stone_icon.length > 8) ? mile_stone_icon : "";
}
String.prototype.endsWith = function (suffix) {
    return this.indexOf(suffix, this.length - suffix.length) !== -1;
};

//function fnSortJobs() {
//    var sort_open_jobs = 0;
////    if ($("#chkInHome").is(':checked') && $("#chkTemplate").is(':checked'))
////        sort_open_jobs = 3; //both checked
////    else if ($("#chkTemplate").is(':checked'))
////        sort_open_jobs = 2; //only template checked
////    else if ($("#chkInHome").is(':checked'))
////        sort_open_jobs = 1; //only inhome date checked
//    if ($("#chkTemplate").is(':checked'))
//        sort_open_jobs = 'template'; //only template checked
//    else if ($("#chkInHome").is(':checked'))
//        sort_open_jobs = 'inHomeDate'; //only inhome date checked
//    else if ($("#chkJobNumber").is(':checked'))
//        sort_open_jobs = 'jobNumber'; //only inhome date checked
//    else if ($("#chkJobName").is(':checked'))
//        sort_open_jobs = 'jobName'; //only inhome date checked
//    sessionStorage.indexPageSortOrder = sort_open_jobs;

//    $("#hdnSortOpenJobs").val(sort_open_jobs);
//    $('#popupSort').popup('close');
//    //$("#col2 div[id^=List]").empty();
//    //$("#col3 div[id^=List]").empty();

//    
//    makeGData($("input[type=radio]:checked").val());
//}

//function getSelectedFacilityJobs() {
//    sessionStorage.facilityId = $('#ddlRegions').val();
//    makeGData("alljobs");
//}

function loadCustomerConfigData() {
    //if (Object.keys(customconfigData).length == 0) {
    var cust_config_data_url = serviceURLDomain + "api/NexusIndex_config/" + appPrivileges.customerNumber;
    getCORS(cust_config_data_url, null, function (data) {
        customconfigData = data;
        bindConfigData();
    }, function (error_response) {
        showErrorResponseText(error_response, true);
    });
    //    }
    //    else {
    //        bindConfigData()
    //    }

}

String.prototype.initCap = function () {
    return this.replace(/(?:^|\s)[a-z]/g, function (m) {
        return m.toUpperCase();
    });
};

function bindConfigData() {
    var config_data = [];

    $.each(customconfigData, function (key1, val1) {
        var div_count = (val1 != undefined && val1 != null && val1 != "" && val1.length > 0) ? Math.round(val1.length / 2) : 0;
        var group_data = [];
        var col_id = 1;
        $.each(val1, function (key2, val2) {
            if (key2 + 1 <= div_count)
                group_data.push(val2);
            else {
                if (key2 + 1 == div_count + 1) {
                    var temp = {};
                    temp[key1 + col_id] = group_data;
                    config_data.push(temp);
                    col_id++;
                    group_data = [];
                }
                group_data.push(val2);
            }

            //$('#chk' + val2.value.initCap()).attr('checked', JSON.parse((val2.isChecked != undefined && val2.isChecked != null && val2.isChecked != "") ? val2.isChecked : "false")); //.checkboxradio('refresh');
        });
        if (group_data.length > 0) {
            var temp = {};
            temp[key1 + col_id] = group_data;
            config_data.push(temp);
            group_data = [];
        }
    });
    $('div[id^=dvConfigCol]').empty();
    $.each(config_data, function (key, val) {
        var index = 0;
        var config_fields = "";
        $.each(val, function (key1, val1) {
            $.each(val1, function (key2, val2) {
                if (index == 0)
                    config_fields += '<fieldset data-role="controlgroup"><legend>' + key1.substring(0, key1.length - 1) + ':</legend>';
                config_fields += '<input type="checkbox" name="chk' + val2.value + '" id="chk' + val2.value + '" ' + ((val2.isChecked != undefined && val2.isChecked != null && val2.isChecked != "" && val2.isChecked == "true") ? "checked" : '') + '>';
                config_fields += '<label for="chk' + val2.value + '"><span style="font-size:14px; font-weight:normal;">' + val2.name + '</span></label>';
                index++;
            });
        });
        config_fields += '</fieldset>';
        $('#dvConfigCol' + (key + 1)).append(config_fields);
    });
    $('#dvConfigGrid').trigger('create');
    $('#popupCustomerConfig').popup('open');
}

function saveCustomerConfigData() {
    var saveConfigInfo = [];
    $.each(customconfigData, function (key1, val1) {
        $.each(val1, function (key2, val2) {
            if ($('#chk' + val2.value).attr('checked')) {
                var temp_val2 = $.extend(true, {}, val2);
                temp_val2.isChecked = "true";
                saveConfigInfo.push(temp_val2);
            }
        });
    });
    //if (Object.keys(saveConfigInfo).length > 0) {
    postCORS(serviceURLDomain + "api/NexusIndex_postConfig/" + appPrivileges.customerNumber, JSON.stringify(saveConfigInfo), function () {
        $('#popupCustomerConfig').popup('close');
        $('#alertmsg').text("Configuration saved successfully.");
        $('#popupDialog a[id=okBut]').attr('onclick', '$(\'#popupDialog\').popup(\'open\');$(\'#popupDialog\').popup(\'close\');');
        $('#popupDialog').popup('open');
        //fnSortJobs();

        makeGData();

    }, function (error_response) {
        $('#popupCustomerConfig').popup('close');
        showErrorResponseText(error_response, false);
        // if (!(jQuery.browser.msie)) {
        //     $('#alertmsg').text((error_response.responseText != undefined) ? error_response.responseText : error_response.statusText);
        // }
        // else {
        //     $('#alertmsg').text((error_response.errorMessage != undefined) ? error_response.errorMessage : ((error_response.responseText != undefined) ? error_response.responseText : error_response.Message));
        // }
        // $('#popupDialog').popup('open');
    });
    //}
    // $('#chk' + val1').attr('checked', false).checkbox('refresh');
}

// fuctionality for the Advanced search
function loadAdvanceSearchData() {
    $('#dvSearchFields').empty();
    $.getJSON(advanceSearchUrl, function (data) {
        advanceSearchData = data;
        fillDeafaultSearchDropdown();
    });
}

function fillDeafaultSearchDropdown() {
    var options_text = "";
    var filter_ddl = "";
    $.each(advanceSearchData, function (key, val) {
        if (key == 0)
            options_text += '<option value="select">Select</option>';
        options_text += '<option value="' + val.item + '" data-options=\'' + JSON.stringify(val.values).replace(/'/g, "\\'") + '\'>' + val.text + '</option>';
    });
    if (options_text != "") {
        var filter_ddl_count = $('#popupAdvSearch select[id^=ddlGroupSelect]').length;
        filter_ddl = '<fieldset data-role="controlgroup" data-type="vertical"><div id="dvSearchFieldsRow' + (filter_ddl_count + 1) + '" width="100%" style="position:relative"><div class="ui-block-a" style="width: 160px" ><fieldset data-role="controlgroup" style="padding:10px"><select id="ddlGroupSelect' + (filter_ddl_count + 1) + '" name="ddlGroupSelect' + (filter_ddl_count + 1) + '" data-theme="b" style="width:80px" data-mini="true" onchange="loadSearchDropdownValues(this);">' + options_text + '</select></fieldset></div></div></fieldset>';
        $('#dvSearchFields').append(filter_ddl);
        $('#popupAdvSearch').popup('open');
        $('#popupAdvSearch div[id=dvSearchFieldsRow' + (filter_ddl_count + 1) + '] div[class=ui-block-a ]').find('fieldset').controlgroup().trigger('create');
    }
}
function loadSearchDropdownValues(ctrl) {
    var load_text = "";
    var filtered_val = "";
    var options = $(ctrl).find(':selected').data('options');
    var ctrl_suffix = ctrl.id.substring(ctrl.id.indexOf('ddlGroupSelect') + 14);
    $.each(options, function (key1, val1) {
        if (key1 == 0)
            load_text += '<option value="select">Select</option>';
        load_text += "<option value='" + val1.value + "'>" + val1.name + "</option>";
    });
    if (load_text != "") {
        if ($('#ddlGroupItems' + ctrl_suffix).length == 0) {
            filtered_val = '<div class="ui-block-b" style="width: 250px" class="ui-bar"><fieldset data-role="controlgroup" style="padding:10px"><select data-theme="b" name="ddlGroupItems' + ctrl_suffix + '" id="ddlGroupItems' + ctrl_suffix + '" onchange="loadDropdownValueDetails(\'dvSearchFieldsRow' + ctrl_suffix + '\',this);" data-mini="true" style="width:100%" >' + load_text + '</select></fieldset></div>';
            $('#dvSearchFieldsRow' + ctrl_suffix).append(filtered_val);
            $('#popupAdvSearch div[id=dvSearchFieldsRow' + (ctrl_suffix) + '] div[class=ui-block-b]').find('fieldset').controlgroup().trigger('create');
        }
        else {

            $("#ddlGroupItems" + ctrl_suffix + " option").remove();
            $("#ddlGroupItems" + ctrl_suffix).append($(load_text));
            $("#ddlGroupItems" + ctrl_suffix).val('select').selectmenu('refresh');
        }
    }

    //$('#dvLoadSearchFieldValues').css('display', 'block');
    //    $.each(advanceSearchData, function (key, val) {
    //        if ($(ctrl).val() == val.item) {
    //           var selected_element = val.values;
    //           $.each(selected_element, function (key1, val1) {
    //               load_text += "<option value='" + val1.value + "'>" + val1.name + "</option>";
    //           });
    //       }
    //   });
    //   if (load_text != "") {
    //       filtered_val = "<select  data-theme='b' >" + load_text + "</select>";
    //       $('#dvLoadSearchFieldValues').append(filtered_val);

    //   }
}

function loadDropdownValueDetails(ctrl_to_append, ctrl) {
    var options = '';
    var txtData = '';
    var ctrl_suffix = ctrl.id.substring(ctrl.id.indexOf('ddlGroupItems') + 13);
    if ($('#ddlMethod' + ctrl_suffix).length == 0) {
        options += '<div class="ui-block-c" style="width: 150px" ><fieldset data-role="controlgroup" style="padding:10px"><select data-theme="b" name="ddlMethod' + ctrl_suffix + '" id="ddlMethod' + ctrl_suffix + '" data-mini="true" data-mini="true" style="width:100%">';
        options += '<option value="equal">Equal</option>';
        options += '<option value="notequal">Not Equal</option></select></fieldset></div>';
        $("#" + ctrl_to_append).append(options);
        txtData += '<div class="ui-block-d" style="width: 150px" ><fieldset data-role="controlgroup" style="padding:10px"><input type="text" name="txtData' + ctrl_suffix + '" id="txtData' + ctrl_suffix + '" value="" maxlength="10" data-mini="true" data-mini="true"/></fieldset></div><div class="ui-block-e"><fieldset data-role="controlgroup" style="padding:10px"><a href="#" onclick="fillDeafaultSearchDropdown();" data-role="button" data-theme="a" data-inline="true" data-mini="true" id="" data-mini="true">Add</a></fieldset></div>';
        $("#" + ctrl_to_append).append(txtData);
        $('#' + ctrl_to_append).css('display', 'block');
        $('#popupAdvSearch div[id=' + ctrl_to_append + '] div[class=ui-block-c]').find('fieldset').controlgroup().trigger('create');
        $('#popupAdvSearch div[id=' + ctrl_to_append + '] div[class=ui-block-d]').find('fieldset').controlgroup().trigger('create');
        $('#popupAdvSearch div[id=' + ctrl_to_append + '] div[class=ui-block-e]').find('fieldset').controlgroup().trigger('create');
    }
    else {
        $("#ddlMethod" + ctrl_suffix).val('equal').selectmenu('refresh');
        $("#txtData" + ctrl_suffix).val('');

    }
}

function saveAdvanceSearchData() {
    if (!validateAdavanceSearchFields())
        return false;
    var selected_rows = $('div[id^=dvSearchFieldsRow]');
    var selected_search_info = {};
    var row_data = {};
    $.each(selected_rows, function (key, val) {
        var field1 = $(val).find('select[id^=ddlGroupSelect' + (key + 1) + ']').val();
        var field_data1 = $(val).find('select[id^=ddlGroupItems' + (key + 1) + ']').val();
        var field_data2 = $(val).find('select[id^=ddlMethod' + (key + 1) + ']').val();
        var field_data3 = $(val).find('input[id^=txtData' + (key + 1) + ']').val();

        row_data[field1] = {
            "field1": field_data1,
            "field2": field_data2,
            "field3": field_data3
        };
        selected_search_info['row' + (key + 1)] = row_data;
        row_data = {};
    });
}

function validateAdavanceSearchFields() {
    var ddl_not_selected = $('#popupAdvSearch select option[value=select]:selected');
    var text_field_not_selected = $('#popupAdvSearch input[type=text][value=""]');
    var msg = "";
    if (ddl_not_selected.length > 0 || text_field_not_selected.length > 0)
        msg = 'Please select required values and click submit';
    if (msg == "")
        return true;
    else {
        $('#alertmsg').html(msg);
        $('#popupAdvSearch').popup('close');
        $('#popupDialog a[id=okBut]').attr('onclick', '$("#popupDialog").popup("close");$("#popupAdvSearch").popup("open");');
        $('#popupDialog').popup('open');
        return false;
    }
}

function loadUsersToJson() {
    $.getJSON(gUserServiceUrl, function () {
        usersJson = dataUsers;
        //fillUsers('ulUserName', 'usersTemplate', usersJson, true);
    });
}

function loadRegionsDDL() {
    $('#dvRegionsDDL').empty();
    var regions = '<select id="ddlRegions" data-theme="e" data-mini="true" data-iconpos="left" onchange = "ko.contextFor(this).$root.getSelectedFacilityJobs();">';
    regions += '<option value="1">Chicago</option>';
    regions += '<option value="2">Los Angeles</option>';
    regions += '</select>';
    $('#dvRegionsDDL').append(regions).trigger('create');
}
function displayReportsList() {
    if (showReportsSection() || displayReports()) {
        if ((appPrivileges.customerNumber == CASEYS && appPrivileges.roleName != "power") || (appPrivileges.customerNumber != CASEYS)) {
            var accounting_reports = "";
            accounting_reports = '<div id="dvReports" data-role="collapsible"  data-theme="f" data-content-theme="c"  data-collapsed-icon="carat-r" data-expanded-icon="carat-d"><h3>Reports</h3><ul id="ulAccountingReports" data-role="listview">';
            $.getJSON(reportsServiceUrl, function (data) {
                reportsServiceData = data;
                $.each(reportsServiceData, function (key, val) {
                    var show_report = true;
                    $.each(val, function (key1, val1) {
                        if (val1.userRole != undefined && val1.userRole != null && val1.userRole.toLowerCase().indexOf(appPrivileges.roleName.toLowerCase()) > -1) {
                            show_report = true;
                            if ((val1.value == "Production_Release_Report" || val1.value == "Lettershop_Production_Report") && (appPrivileges.customerNumber == "1" /*|| appPrivileges.customerNumber == REDPLUM*/))
                                show_report = true;
                            else if ((val1.value == "Open_Accounts_Receivable" || val1.value == "Daily_Milestones") && (appPrivileges.customerNumber == CW)) {
                                show_report = false;
                            }
                            else if (((val1.value == "Production_Release_Report" || val1.value == "Lettershop_Production_Report") && appPrivileges.customerNumber != "1" /*&& appPrivileges.customerNumber != REDPLUM*/) ||
                                     ((val1.value == "Open_Accounts_Receivable" || val1.value == "Job_Invoice_Summary" || val1.value == "Daily_Milestones") && (appPrivileges.customerNumber == DCA || appPrivileges.customerNumber == KUBOTA || appPrivileges.customerNumber == SPORTSKING || appPrivileges.customerNumber == GWA || appPrivileges.customerNumber == CASEYS || appPrivileges.customerNumber == EQUINOX || isFilterJSONReports())) ||
                                     ((val1.value == "Open_Jobs" || val1.value == "Geolocation_Summary" || val1.value == "Postage_Summary" || val1.value == "Job_Invoice_Summary" || val1.value == "Artwork_Summary") && (appPrivileges.customerNumber == CW)) ||
                                     (val1.value == "Postage_Summary" && (appPrivileges.customerNumber == CASEYS || appPrivileges.customerNumber == EQUINOX || isFilterJSONReports()))) {
                                show_report = false;
                            }
                            if (show_report)
                                accounting_reports += '<li data-theme="c"><a onclick="populateAccountingReportsOutputForm(\'' + val1.value + '\',\'\')" href="#popupReportsParam" data-rel="popup" data-position-to="window" data-inline="true">' + val1.name + '</a></li>';
                        }
                    });
                });
                accounting_reports += '</ul></div>';
                if ($(accounting_reports).find('li[data-theme="c"]').length == 0) {
                    accounting_reports = "";
                }
                $("#colList1").find('#dvReports').remove();
                $("#colList1").append(accounting_reports);
                $.each($('#ulAccountingReports'), function () {
                    $(this).listview();
                });
                $('#colList1').collapsibleset('refresh');
            });
        }
    }
}

function getDailyTaskData() {
    $.getJSON(dailyTaskServiceUrl, function (data) {
        dailyTaskUrlData = data;
    });
}

function fnChangePassword() {
    var error_msg = "";
    var is_validate = true;
    if ($("#cnfpwd").val() != $('#newpwd').val()) {
        error_msg = 'Password and Confirm password should be same.';
        is_validate = validateChangePassword(error_msg);
        return is_validate;
    }
    if ($("#cnfpwd").val() == '' || $('#newpwd').val() == '') {
        error_msg = 'Please enter password and confirm password.';
        is_validate = validateChangePassword(error_msg);
        return is_validate;
    }
    var change_pass_url = changePasswordUrl + appPrivileges.facility_id + '/' + appPrivileges.customerNumber + '/' + sessionStorage.username + '/' + jQuery.trim($("#cnfpwd").val());
    getCORS(change_pass_url, null, function () {
        $('#popupChangePassword').popup('close');
        $('#alertmsg').text('Password has been change successfully.');
        $('#popupDialog').popup('open');
        $('#popupChangePassword').popup('close');
    }, function (error_response) {
        if (error_response.responseText.toLowerCase() == "error: profile. forgot my password..") {
            $('#alertmsg').text('We are unable to change the password for this account. Please contact a system administrator.');
            $('#popupChangePassword').popup('close');
            $('#popupDialog').popup('open');
        }
        else {
            showErrorResponseText(error_response, true);
        }
    });

}
function validateChangePassword(error_msg) {
    if (error_msg != '') {
        $('#popupChangePassword').popup().popup('close');
        $('#alertmsg').html(error_msg);
        $("#popupDialog").popup().popup('open');
        $("#okBut").attr("onclick", "$('#popupDialog').popup('close');$('#popupChangePassword').popup().popup('open');");
        return false;
    }
}

function fnCancel() {
    $('#divError').css('display', 'none');
    $('#popupChangePassword').popup('close');
}

function deleteChkElement() {
    $.each(dailyTaskUrlData, function (key, val) {
        $.each(val.values, function (key1, val1) {
            //$('#chk' + val1.jobNumber), this).remove();
            if ($('#chk' + val1.jobNumber).is(':checked')) {
                //$('#chk' + val1.jobNumber).find('option:selected').remove();
                $('#chk' + val1.jobNumber).removeAttr('checked').checkboxradio('refresh');
            }
        });

    });
}

function showFilters() {
    var filter_show = '<div id="btnFilter" data-role="collapsible" data-theme="e" data-content-theme="c" data-collapsed="true" data-collapsed-icon="myapp-filter" data-expanded-icon="carat-d">';
    filter_show += '<h3>Filter Jobs</h3><div data-role="controlgroup" data-mini="true" align="left">';
    filter_show += '<label for="ddlFilterMilestones">Date Type:</label><select name="ddlFilterMilestones" id="ddlFilterMilestones" data-mini="true" data-theme="e" onchange="enableStartEndDates();">';
    filter_show += '<option value="-1">Select</option><option value="is">Data Processing/IS</option><option value="postage">Postage</option><option value="mailDrop">Mail Drop</option>';
    filter_show += '<option value="verified">Ship Verification</option><option value="inhome">In-Home</option><option value="invoice">Invoice</option></select>';
    filter_show += '</div><div data-mini="true" align="left"><label for="dtpStart">From:</label><input type="text" onfocus="showDatePicker();" placeholder="mm/dd/yyyy" data-clear-btn="false" name="dtpStart" id="dtpStart" data-mini="true">';
    filter_show += '<label for="dtpEnd">To:</label><input type="text" onfocus="showDatePicker();" placeholder="mm/dd/yyyy" data-clear-btn="false" name="dtpEnd" id="dtpEnd" data-mini="true"></div>';
    filter_show += '<div data-role="collapsible-set" id="dvDropAllUsers"><div data-role="collapsible" data-collapsed="false" data-content-theme="c" data-theme="e" data-mini="true" data-collapsed-icon="carat-r" data-expanded-icon="carat-d">';
    filter_show += '<h3>Personnel</h3><div id="dvSelectedFilterUsers" style="display:none"><ul data-role="listview" id="ulSelectedUsers" data-inset="true" data-divider-theme="e" data-theme="c" data-mini="true"></ul></div>';
    filter_show += '<fieldset data-role="controlgroup" data-mini="true" id="fldSetFilterOptions"><div id="dvFilterOptions"></div></fieldset></div>';
    filter_show += '<div align="center"><a href="#" data-role="button" data-theme="f" data-inline="true" data-mini="true" name="btnFiltersCancel" id="btnFiltersCancel" onclick="clearFilter(true);">Clear</a>';
    filter_show += '<a href="#" data-role="button" data-theme="f" data-inline="true" data-mini="true" name="btnFiltersSubmit" id="btnFiltersSubmit" onclick="ko.contextFor(this).$root.filterJobs(ko.contextFor(this).$data,ko.contextFor(this).$parent,event);">Filter</a></div></div></div>';
    $('#dvPreferences').append(filter_show);
    $('#btnFilter').trigger('create');
}

//No longer useful - remove later
function showDemos() {
    $('#btnDemo').remove();
    var demo_show = '<div id="btnDemo" data-role="collapsible" data-theme="e" data-content-theme="c" data-collapsed="true" data-collapsed-icon="myapp-demo" data-expanded-icon="carat-d">';
    demo_show += ' <h3>Gizmo Demos</h3><ul data-role="listview" >';
    //demo_show += ' <li id="liCaseys"><a href="#" onclick="loadDemoCustData(this);" data-custno="99998" data-program="oip" style="font-size:14px;">Opt-In Program</a></li>';
    demo_show += '<li><a href="#" onclick="loadDemoCustData(this);" data-custno="99996" data-program="smp" style="font-size:14px;">Shared Mail Program</a></li>';
    //demo_show += '<li><a href="#" onclick="loadDemoCustData(this);" data-custno=BRIGHTHOUSE data-program="up" style="font-size:14px;">Upload Program</a></li>';
    demo_show += '<li><a href="#" data-program="up" style="font-size:14px;">Upload Program</a></li>';
    demo_show += '<li><a href="#" onclick="loadDemoCustData(this);" data-custno="99997" data-program="lmm" style="font-size:14px;">Location Mgmt Module</a></li>';
    demo_show += '<li><a href="#" onclick="loadDemoCustData(this);" data-custno="99997" data-program="lsm" style="font-size:14px;">List Selection Module</a></li>';
    demo_show += '<li><a href="#" onclick="loadDemoCustData(this);" data-custno="99997" data-program="pam" style="font-size:14px;">Proofing & Approval Module</a></li>';
    demo_show += '</ul></div>';
    $('#dvPreferences').append(demo_show);
    $('#btnDemo').trigger('create');
}

function showFilterTasks() {
    var filter_tasks = '';
    filter_tasks += '<div id="dvFilterTasks" data-role="collapsible" data-theme="e" data-content-theme="c" data-collapsed="true" data-collapsed-icon="myapp-filter" data-expanded-icon="carat-d">';
    filter_tasks += '<h3>Filter Tasks</h3>';
    filter_tasks += '<fieldset data-role="controlgroup" class="ui-controlgroup">';
    filter_tasks += '<input type="radio" name="rdoFilterTasks" id="rdoAllTasks" data-mini="true" class="custom" data-icon="check" title="All Tasks" value="allTasks" checked style="left:0.2em;top:40%" />';
    filter_tasks += '<label for="rdoAllTasks" data-mini="true" title="All Tasks">All Tasks</label>';
    filter_tasks += '<input type="radio" name="rdoFilterTasks" id="rdoMyTasks" data-mini="true" class="custom" data-icon="check" title="My Tasks" value="myTasks" style="left:0.2em;top:40%" />';
    filter_tasks += '<label for="rdoMyTasks" data-mini="true" title="My Tasks">My Tasks</label>';
    filter_tasks += '</fieldset>';
    filter_tasks += '<div align="right"><a data-role="button" data-theme="f" data-inline="true" data-mini="true" name="btnFilterTasks" id="btnFilterTasks">Filter Tasks</a></div></div>';
    $('#dvPreferences').append(filter_tasks);
    $('#dvFilterTasks').trigger('create');
}

function loadDemoCustData(ctrl) {
    var cust_id = $(ctrl).attr('data-custno');
    var demo_program = $(ctrl).attr('data-program');
    if (cust_id != undefined && cust_id != null && cust_id != "") {
        sessionStorage.isDemoSelected = 1;
        if (sessionStorage.demoProgram != demo_program)
            sessionStorage.isDemoInsDisplayed = false;
        sessionStorage.demoProgram = demo_program;
        //if (cust_id == "99998" && (appPrivileges.roleName == "power" || appPrivileges.roleName == "user"))
        //    $('#lnkNewJob').css('display', 'none');
        //else
        $('#lnkNewJob').css('display', 'block');
        demoNavlinks(cust_id);
        setLogo(cust_id);
        $('#dvRegionsDDL').css('display', 'none');
        $('#dvPreferences').find('#btnFilter').css('display', 'none');
        $("#dvDailyTaskListData").css("display", "none");
        $('#dvReports').addClass('ui-corner-all');
    }
}

function validateFilters() {
    var msg = '';
    if ($('#ddlFilterMilestones').val() == -1 && $('#dvDropAllUsers li').length == 0) {
        if (msg != undefined && msg != "" && msg != null)
            msg += "<br />Please select at least one option from  Milestones or Personnel(Region,Department and User)";
        else
            msg = "Please select at least one option from  Milestones or Personnel(Region,Department and User)";
    }
    if ($('#ddlFilterMilestones').val() != -1 && $('#dtpStart').val() == "") {
        if (msg != undefined && msg != "" && msg != null)
            msg += "<br />Please select From Date";
        else
            msg = "Please select From Date";

    }
    if ($('#ddlFilterMilestones').val() != -1 && $('#dtpEnd').val() == "") {
        if (msg != undefined && msg != "" && msg != null)
            msg += "<br />Please select To Date";
        else
            msg = "Please select To Date";
    }
    if (msg != "") {
        $('#alertmsg').html(msg);
        $('#alertmsg').css('text-align', 'left');
        $("#popupDialog").popup('open');
        $("#okBut").attr("onclick", "$('#popupDialog').popup('close');$('#popupComments').popup('open');");
        return false;
    }
    return true;
}

function clearFilter(flag) {
    $('#ddlFilterMilestones').val('-1').selectmenu('refresh');
    $('#ddlFilterRegions').val('-1').selectmenu('refresh');
    $('#ddlFilterDepartments').val('-1').selectmenu('refresh');
    $('#ddlFilterUsers').val('-1').selectmenu('refresh');
    $('#ulSelectedUsers').empty();
    $('#dtpStart').val('');
    $('#dtpEnd').val('');
    $('#ddlFilterRegions').val(sessionStorage.facilityId).selectmenu('refresh').trigger('change');
    if (($('#dtpStart') || $('#dtpEnd')).hasClass('ui-input-text')) {
        $('#dtpStart')[0].disabled = true;
        $('#dtpEnd')[0].disabled = true;
        $('#dtpStart').css('opacity', '0.7');
        $('#dtpEnd').css('opacity', '0.7');
    }
    $("input[id$='dtpStart']").datepicker("option", "maxDate", null);
    $("input[id$='dtpEnd']").datepicker("option", "minDate", null);
    if (flag)
        $('#btnFiltersSubmit').trigger('click');
}

function enableStartEndDates() {
    if ($('#ddlFilterMilestones').val() != "-1") {
        //$('#dtpStart').removeClass('ui-disabled');
        $('#dtpStart')[0].disabled = false;
        $('#dtpStart').css('opacity', '1');
        //$('#dtpEnd').removeClass('ui-disabled');
        $('#dtpEnd')[0].disabled = false;
        $('#dtpEnd').css('opacity', '1');
    }
    else if ($('#ddlFilterMilestones').val() == "-1") {
        $('#dtpStart').val('');
        $('#dtpEnd').val('');
        if (($('#dtpStart') || $('#dtpEnd')).hasClass('ui-input-text')) {
            $('#dtpStart')[0].disabled = true;
            $('#dtpEnd')[0].disabled = true;
            $('#dtpStart').css('opacity', '0.7');
            $('#dtpEnd').css('opacity', '0.7');
        }
    }
}

function loadDemoEmailsList() {
    getCORS(serviceURLDomain + "api/GizmoEmail_actions/" + facilityId + "/" + jobCustomerNumber, null, function (data) {
        demoEmailsList = data;
    }, function (response_error) {
        //$('#alertmsg').text(response_error);
        //$('#popupDialog').popup('open');
        showErrorResponseText(response_error, false);
    });
}

function fnSendOptInEmail(type) {
    getCORS(optInUrl + jobCustomerNumber + '/' + facilityId + '/408313/' + type, null, function (response) {
        //if (response.indexOf("success") != "-1") {
        $('#alertmsg').html(response);
        $('#popupDialog').popup('open');
        //}
    }, function (response_error) {
        //$('#alertmsg').text(response_error);
        //$('#popupDialog').popup('open');
        showErrorResponseText(response_error, false);
    });
}

function aboutDemoChange(selected_demo_item) {
    //var selected_demo_item = $('#ddlAboutDemo').val();
    if (selected_demo_item != "" && selected_demo_item > 0) {
        $('div[id^=demoText]').css('display', 'none');
        $('#demoText' + selected_demo_item).css('display', 'block');
        $('#popupAboutDemo').popup('open');
        $('#popupAboutDemo').popup('open');
    }
}

function pickDemo(type) {
    switch (type) {
        case "1":
            $('#headerText').text('Automated Job Processing');
            $('#pText').css('display', 'none');
            $('#ulInnderText').css('display', 'none');
            break;
        case "2":
            $('#headerText').text('Small Job Processing');
            $('#pText').css('display', 'block');
            $('#ulInnderText').css('display', 'block');
            $('#pTextArea').css('display', 'none');
            $('#pTextArea1').css('display', 'none');
            break;
        case "3":
            $('#headerText').text('Franchise opt-in email');
            $('#pText').css('display', 'none');
            $('#ulInnderText').css('display', 'block');
            $('#pTextArea').css('display', 'block');
            $('#pTextArea1').css('display', 'none');
            break;
        case "4":
            $('#headerText').text('Mail Tracker');
            $('#pText').css('display', 'none');
            $('#ulInnderText').css('display', 'none');
            $('#pTextArea').css('display', 'block');
            $('#pTextArea1').css('display', 'none');
            break;
        case "5":
            $('#headerText').text('List Selection');
            $('#pText').css('display', 'none');
            $('#ulInnderText').css('display', 'block');
            $('#pTextArea').css('display', 'block');
            $('#pTextArea1').css('display', 'none');
            break;
        default:
            break;
    }
}

function showGizmoUsers() {
    window.location.href = "userProfile/adminUsers.html";
}
function deleteGizmoUsers() {
    window.location.href = "archiveDeleteJobs.html";
}

function loadDemoReportsJson() {
    var demo_reports_json = {};
    $.getJSON("../JSON/_demoReports.JSON", function (data) {
        $.each(data, function (key, val) {
            if (key.indexOf(sessionStorage.customerNumber) > -1) {
                $.each(val, function (key1, val1) {
                    demo_reports_json[key1] = val1;
                });
            }
        });
        if (Object.keys(demo_reports_json).length > 0)
            sessionStorage.demoReportsJson = JSON.stringify(demo_reports_json);
        createDemoReportsList();
    });
}

function loadAboutThisDemoFaq() {
    var demo_faq_json = {};
    $.getJSON("../JSON/_demoAboutThisDemoFaq.JSON", function (data) {
        $.each(data, function (key, val) {
            if (key.indexOf(sessionStorage.customerNumber) > -1) {
                $.each(val, function (key1, val1) {
                    demo_faq_json[key1] = val1;
                });
            }
        });
        if (Object.keys(demo_faq_json).length > 0)
            sessionStorage.demoFaqJson = JSON.stringify(demo_faq_json);
        createDemoFaqList();
    });
}

function createDemoFaqList() {
    if (sessionStorage.demoFaqJson != undefined && sessionStorage.demoFaqJson != null && sessionStorage.demoFaqJson != "") {
        var demo_faqs = {};
        demo_faqs = $.parseJSON(sessionStorage.demoFaqJson);
        $('#dvFaqs').empty();
        var temp_list = "";
        $.each(demo_faqs, function (key, val) {
            temp_list += val;
        });
        if (temp_list != "") {
            //$('#dvFaqs').html(temp_list).trigger('create');
            $('#dvFaqs').html(temp_list).trigger('create').collapsibleset('refresh');
        }
    }

}

function createDemoReportsList() {
    if (sessionStorage.demoReportsJson != undefined && sessionStorage.demoReportsJson != null && sessionStorage.demoReportsJson != "") {
        var demo_reports = {};
        demo_reports = $.parseJSON(sessionStorage.demoReportsJson);
        $('#ulPickDemo').empty();
        var temp_list = "";
        $.each(demo_reports, function (key, val) {
            temp_list += val;
        });
        if (temp_list != "") {
            $('#ulPickDemo').html(temp_list).listview('refresh');
        }
    }

}

function loadStoreData() {
    window.location.href = "../loadStoreData/loadStoreData.html";
}

function loadLMASetup() {
    window.location.href = "../lmaSetup/lmaSetup.html";
}

function loadJobDiagnostics(ctrl) {
    if (ctrl.id == "jobDiagnostics")
        window.location.href = "../jobDiagnostics/jobHealth.html";
    else if (ctrl.id == "jobManagement")
        window.location.href = "../jobManagement/jobManagement.html";
}

function loadSystemDashboard() {
    window.location.href = "../systemsDashboard/systemsDashboard.html";
}

function loadSharedMailDashboard() {
    window.location.href = "../sharedMailDashboard/sharedMailDashboard.html";
}

function loadModuleConfiguration() {
    window.location.href = "../moduleConfiguration/moduleConfiguration.html";
}

function loadShortageAndCorrections() {
    window.location.href = "../shortagesAndCorrections/floorShortages.html";
}

function displayFacilityList() {
    var demo_faq_json = {};
    $('#dvFacilityNames').empty();
    $.getJSON("../JSON/_facilityList.JSON", function (data) {
        $.each(data, function (key, val) {
            $.each(val, function (key1, val1) {
                demo_faq_json[key1] = val1;
            });
        });
        var facilty_list = '<ul data-role="listview" id="ulFacilityList" data-inset="true">';
        $.each(demo_faq_json, function (key, val) {
            $.each(val, function (key1, val1) {
                if (key1 == "facilityName")
                    facilty_list += '<li class="ui-grid-d ui-responsive"><label class="ui-block-a" style="width:65%;valign:middle;">' + val1 + '</label>';// <label data-bind="attr:{'for':'chk'+ $data.userName.replace(' ','')}, text:$data.computedUserNameWithCount"></label>
                if (key1 == "facilityValue") {
                    facilty_list += '<select class="ui-block-b" data-theme="e" style="width:30%;valign:middle;" data-role="flipswitch" data-mini="true">' + (val1 == sessionStorage.facilityId ? '<option value="off">Off</option><option value="on" selected="">On</option>' : '<option value="off" selected="">Off</option><option value="on">On</option>') + '</select></li>';
                }
            });
        });
        facilty_list += '</ul>';
        $('#dvFacilityNames').append(facilty_list);
        $('#ulFacilityList').trigger('create');
        $('#dvFacilityNames').trigger('create');
        $("#popupFaciltyList").popup('open');
    });
}

function confirmFlushInMemoryDB() {
    //$('#okButConfirm').text('Delete Job');
    $('#confirmMsg').html("Do you really want to delete all of the in-memory job tickets? This will cause them to be recreated from scratch on the next customer request. In general, this will NOT cause data loss, but may take a few minutes to perform. Note that in-memory job tickets are flushed overnight automatically, so it's sometimes a good idea to wait.");
    $('#okButConfirm').text('Yes');
    $('#cancelButConfirm').text('No');
    $('#cancelButConfirm').bind('click', function () {
        $('#cancelButConfirm').unbind('click');
        $('#okButConfirm').unbind('click');
        $('#cancelButConfirm').attr('onclick', '$(\'#popupConfirmDialog\').popup(\'open\');$(\'#popupConfirmDialog\').popup(\'close\');');
        $('#popupConfirmDialog').popup('close');
    });
    $('#okButConfirm').bind('click', function () {
        $('#okButConfirm').unbind('click');
        var flush_in_mem_db_url = serviceURLDomain + "api/InMemory_flush";
        var post_json = {};
        post_json["userId"] = sessionStorage.username;
        $('#popupConfirmDialog').popup('close');
        postCORS(flush_in_mem_db_url, JSON.stringify(post_json), function (response_data) {
            if (response_data.toLowerCase() == "success") {
                $('#alertmsg').html('In-Memory DB flushed successfully.');
                window.setTimeout(function () {
                    $('#popupDialog').popup('open');
                }, 500);
            }
        }, function (error_response) {
            showErrorResponseText(error_response, true);
        });
    });
    $('#popupConfirmDialog').popup('open');
}

function manageFin() {
    window.location.href = "adminFinance/financeJobLookUp.html";
}

function navEmailLists() {
    window.location.href = "adminEmailLists/emailLists.html";
}
function navTemplateSetup() {
    window.location.href = "adminTemplateSetup/templateSetup.html";
}

function fnDisplayBbbReports() {
    var report_url = "http://s-reportserver/ReportServer/Pages/ReportViewer.aspx?%2fMail+Tracker%2fTracking_Email_Dev&rs:Command=Render&rs:ClearSession=true&customerNumber=" + jobCustomerNumber;  //&effortName=seneca_dev_20150415035959
    window.open(report_url);
    //var customer_number = $('#txtCustNumber').val();
    //var email_effort = $('#ddlEmailEffort').val();
    //window.open("http://s-reportserver/Reports/Pages/Report.aspx?ItemPath=%2fMail+Tracker%2fTracking_Email_Dev" + "/" +customer_number + "/" + email_effort);
}
//******************** Public Functions End *************************