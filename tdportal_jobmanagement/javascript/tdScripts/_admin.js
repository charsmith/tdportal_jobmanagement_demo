﻿jQuery.support.cors = true;

//******************** Global Variables Start **************************
//var gServiceUrl = '../JSON/_admin.JSON';
var gServiceUrl = serviceURLDomain + "api/UserProfile/1/" + appPrivileges.customerNumber + "/all";
var gData;
//******************** Global Variables End **************************

//******************** Page Load Events Start **************************
$(document).on('pageshow', '#_admin', function (event) {
    makeGData();
});

$('#_admin').live('pagebeforecreate', function (event) {
    confirmMessage();
    //test for mobility...
    loadMobility();
});
//******************** Page Load Events End **************************


//******************** Public Functions Start **************************
function makeGData() {
    //$.ajax({
    //    url: gServiceUrl,
    //    dataType: 'json',
    //    timeout: 10000,
    //    type: 'GET',
    //    async: false,
    //    success: function (data) {
    //        gData = data;
    //        buildLists();
    //    },
    //    error: function (XMLHttpRequest, textStatus, errorThrown) {
    //        $('#alertmsg').text("Error status :" + textStatus + ". Error type: " + errorThrown + ". Error Message: " + XMLHttpRequest.responseXML);
    //        $('#popupDialog').popup('open');
    //    }
    //});

    getCORS(gServiceUrl, null, function (data) {
        gData = data;
        buildLists();
    }, function (error_response) {
        showErrorResponseText(error_response,true);
    });
}

function buildLists() {
    $("#ulContent").empty();
    var counter = 0;
    $.each(gData.Users, function (key_admin, val_admin) {
        //var li = '<li><a href="#" onclick="fnProfile(\'' + val_admin.userName + '\')"><h3>' + val_admin.userName + '</h3>'
        //li += '<p>' + val_admin.details["firstName"] + ' ' + val_admin.details["lastName"] + ' | ' + val_admin.details["company"] + ' | ' + val_admin.details["active"] + '</p>'
        //li += '<p class="ui-li-aside">' + val_admin.details["userRole"] + '</p></a>';
        //li += '<a href="#" onclick="fnRemoveProfile(\'' + counter + '\');">Remove User</a></li>';
        var li = '<li><a href="#" onclick="fnProfile(\'' + val_admin.Username + '\')"><h3>' + val_admin.Username + '</h3>';
        li += '<p>' + val_admin.FirstName + ' ' + val_admin.LastName;
        if (val_admin.Phone != undefined && val_admin.Phone != "")
            li += ' | ' + val_admin.Phone;

        li += '</p>';
        li += '<a href="#" onclick="fnRemoveProfile(\'' + counter + '\');">Remove User</a></li>';

        $("#ulContent").append(li);
        $("#ulContent").listview("refresh");
        counter++;
    });
}

function fnRemoveProfile(pk) {
    $("#hdnPk").val(pk);
    var returnVal = $('#confirmMsg').text("Are you sure you want to Remove this user?");
    $('#popupConfirmDialog').popup('open');
    return false;
}

function fnProfile(usrName) {
    window.location.href = "userProfile.html?userName=" + usrName;
}

function confirmMessage() {
    var msg_box = '';
    msg_box = '<div data-role="popup" id="popupConfirmDialog" data-overlay-theme="a" data-theme="c" style="max-width:400px;" class="ui-corner-all">';
    msg_box += '<div data-role="header" data-theme="a" class="ui-corner-top" id="dialogbox">';
    msg_box += '<h1>Confirm</h1>';
    msg_box += '</div>';
    msg_box += '<div data-role="content" data-theme="d" class="ui-corner-bottom ui-content">';
    msg_box += '<div id="confirmMsg"></div>';
    msg_box += '<a href="#" data-role="button" data-inline="true" data-rel="back" data-theme="b" id="cancelBut">Cancel</a>';
    msg_box += '<a href="#" data-role="button" data-inline="true" data-rel="back" data-theme="b" onclick="fnConfirmContinueClick()" id="continueBut">Continue</a>';
    msg_box += '</div></div>';
    $("#_admin").append(msg_box);
}

function fnConfirmContinueClick() {
    gData.splice($("#hdnPk").val(), 1);
    buildLists();
}


//******************** Public Functions End **************************