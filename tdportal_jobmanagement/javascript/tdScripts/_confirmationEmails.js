﻿var confirmationEmails = function (self) {

    self.approversList = [];
    self.approversViewersList = ko.observable({});

    self.loadApproversAndViewersKOUI = function (target_ele) {
        var ui_ele = "";
        ui_ele = '<div class="ui-grid-a ui-responsive"><div class="ui-block-a">';
        ui_ele += '<div class="ui-body ui-body-d" data-theme="d" id="dvConfirmationEmailData">';
        ui_ele += '<label for="txtEmails"><b>Send Confirmation and Approval Emails To:</b></label><br />';
        ui_ele += '<span data-bind="text:$data.displayEmail"></span>';
        ui_ele += '<div id="dvApproversViewersList" data-bind="with:$data.approversViewersList" class="ui-responsive">';
        ui_ele += '<div data-role="fieldcontain">';
        ui_ele += '<label for="txtArtApprover"><b>Art Approver:</b></label>';
        if (appPrivileges.roleName != 'user') {
            ui_ele += '<input type="text" id="txtArtApprover" data-mini="true" name="txtArtApprover" data-bind="value:artApprover" placeholder="Add recipients here, separated by commas." />';
        }
        else {
            ui_ele += '<input type="text" id="txtArtApprover" data-mini="true" disabled name="txtArtApprover" data-bind="value:artApprover" placeholder="Add recipients here, separated by commas." />';
        }
        ui_ele += '</div>';
        ui_ele += '<div data-role="fieldcontain">';
        ui_ele += '<label for="txtListApprover"><b>List Approver:</b></label>';
        if (appPrivileges.roleName != 'user') {
            ui_ele += '<input type="text" id="txtListApprover" data-mini="true" name="txtListApprover" data-bind="value:listApprover" placeholder="Add recipients here, separated by commas." />';
        }
        else {
            ui_ele += '<input type="text" id="txtListApprover" data-mini="true" disabled name="txtListApprover" data-bind="value:listApprover" placeholder="Add recipients here, separated by commas." />';
        }
        ui_ele += '</div>';
        ui_ele += '<div data-bind="visible:artViewer.length > 0" data-role="fieldcontain">';
        ui_ele += '<label for="txtArtViewer"><b>Art Viewer:</b></label>';
        if (appPrivileges.roleName != 'user') {
            ui_ele += '<input type="text" id="txtArtViewer" data-mini="true" name="txtArtViewer" data-bind="value:artViewer" placeholder="Add recipients here, separated by commas." />';
        }
        else {
            ui_ele += '<input type="text" id="txtArtViewer" data-mini="true" disabled name="txtArtViewer" data-bind="value:artViewer" placeholder="Add recipients here, separated by commas." />';
        }
        ui_ele += '</div>';
        ui_ele += '<div data-bind="visible:listViewer.length > 0" data-role="fieldcontain">';
        ui_ele += '<label for="txtListViewer"><b>List Viewer:</b></label>';
        if (appPrivileges.roleName != 'user') {
            ui_ele += '<input type="text" id="txtListViewer" data-mini="true" name="txtListViewer" data-bind="value:listViewer" placeholder="Add recipients here, separated by commas." />';
        }
        else {
            ui_ele += '<input type="text" id="txtListViewer" data-mini="true" disabled name="txtListViewer" data-bind="value:listViewer" placeholder="Add recipients here, separated by commas." />';
        }
        ui_ele += '</div>';
        ui_ele += '<div>';
        ui_ele += '<fieldset data-role="controlgroup" data-mini="true" id="fsApprovalTypes" style="display: none">';
        ui_ele += '<legend></legend>';
        ui_ele += '<input type="checkbox" id="chkNoArtApprove" name="chkNoArtApprover" data-theme="e" />';
        ui_ele += '<label for="chkNoArtApprove"><b>No Art To Approve</b></label>';
        ui_ele += '<input type="checkbox" id="chkNoListApprove" name="chkNoListApprove" data-theme="e" />';
        ui_ele += '<label for="chkNoListApprove"><b>No List To Approve</b></label>';
        ui_ele += '</fieldset>';
        ui_ele += '</div>';
        if (appPrivileges.roleName != 'user')
            ui_ele += '<div style="float:right"><a data-role="button" data-theme="f" data-mini="true" data-bind="click:$root.submitApproversList">Send</a></div>';
        ui_ele += '</div></div></div></div>';

        $(target_ele).empty();
        $(target_ele).html(ui_ele);
    }

    self.getApproversList = function () {
        var job_number;
        job_number = (self.jobNumber) ? self.jobNumber : sessionStorage.jobNumber;
        var approvers_list_url = serviceURLDomain + "api/Tagging_approvers/" + sessionStorage.facilityId + "/" + jobCustomerNumber + "/" + job_number + "/null";
        getCORS(approvers_list_url, null, function (response_data) {
            self.approversList = (response_data != undefined && response_data != null && response_data != "" && response_data.Approvers != undefined && response_data.Approvers != null && response_data.Approvers != "" && Object.keys(response_data.Approvers).length > 0) ? response_data.Approvers : [];
            if (self.approversList.length == 0) {
                self.approversList = { "artApprover": "", "listViewer": "", "artViewer": "", "listApprover": "" };
            }
            if (jobCustomerNumber == ALLIED) {
                response_data.Approvers.artApprover = "admin_al";
                response_data.Approvers.artViewer = "admin_al";
            }
            if (self.approversList["artViewer"] == undefined) self.approversList["artViewer"] = "";
            if (self.approversList["listViewer"] == undefined) self.approversList["listViewer"] = "";
            if (self.approversList["artApprover"] == undefined) self.approversList["artApprover"] = "";
            if (self.approversList["listApprover"] == undefined) self.approversList["listApprover"] = "";

            self.approversViewersList(self.approversList);
        }, function (error_response) {
            showErrorResponseText(error_response, true);
        });
    };

    //Submit approvers and viewers
    self.submitApproversList = function () {
        var job_number;
        job_number = (self.jobNumber) ? self.jobNumber : sessionStorage.jobNumber;
        $.each(self.approversViewersList(), function (key, val) {
            if (val != null && val != "") {
                approvers_info["approvalType"] = key;
                //            if ((key.toLowerCase().indexOf('viewer') > -1))
                //                approvers_info["viewers"] = val;
                //            else
                approvers_info["approvers"] = val;
                (function (post_approvers_info) {
                    var my_timer = setInterval(function () {
                        var post_approval_url = serviceURLDomain + "api/Tagging_post_approvers/" + sessionStorage.facilityId + "/" + jobCustomerNumber + "/" + job_number
                        postCORS(post_approval_url, JSON.stringify(post_approvers_info), function (response) {
                            approvers_info = {};
                        }, function (response_error) {
                            showErrorResponseText(response_error, false);
                            ////var msg = 'Job creation failed.';
                            //var msg = response_error.responseText;
                            //$('#alertmsg').text(msg);
                            //$('#popupDialog').popup('open', { positionTo: 'window' });
                            //var error = response_error;
                        });
                        clearInterval(my_timer);
                    }, 1000);
                })(approvers_info);
                approvers_info = {};
            }
        });
    };
}