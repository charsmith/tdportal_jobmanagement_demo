jQuery.support.cors = true;

//******************** Global Variables Start **************************
var gServiceUrl = '';
var gData;
var gPageSize = 5;
var gDataStartIndex = 0;
//******************** Global Variables End **************************

//******************** Page Load Events Start **************************
$(document).on('pageshow', '#_resetPasswordDialog', function () {
    $("#_resetPasswordDialog").trigger("create");
});

$('#_resetPasswordDialog').live('pagebeforecreate', function () {
    if(sessionStorage.temppassword == "true"){
    $("#_resetPasswordDialog").dialog({
        modal: 'true',
        width: '100%'
    });
    }
});
//******************** Page Load Events End **************************

//******************** Public Functions Start **************************
function submitData() {
    $.ajax({
        url: gServiceUrl,
        data: "{'currentpassword':'','newpassword':'','confirmpassword':''}",
        dataType: 'json',
        timeout: 10000,
        type: 'POST',
        async: false,
        success: function (data) {
            gData = data;
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            $('#alertmsg').text("Error status :" + textStatus + ". Error type: " + errorThrown + ". Error Message: " + XMLHttpRequest.responseXML);
            $('#popupDialog').popup('open');
        }
    });
}
//******************** Public Functions End **************************