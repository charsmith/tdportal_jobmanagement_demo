﻿jQuery.support.cors = true;

//******************** Global Variables Start **************************
var gServiceUrl = serviceURLDomain + 'api/GizmoEmail_unsubscribe/';//1/155370/jim.wright@tribunedirect.com/client_string
urlString = unescape(window.location);
queryString = (urlString.indexOf("?") > -1) ? urlString.substr(urlString.indexOf("?") + 1, urlString.length) : "";
var params = queryString.split("&");
var facility_id = 0;
var customer_number = 0;
var unsubscribe_email = "";
var unsubscribe_msg = "";

String.prototype.hexEncode = function () {
    var hex, i;

    var result = "";
    for (i = 0; i < this.length; i++) {
        hex = this.charCodeAt(i).toString(16);
        result += (hex).slice(-4);
    }

    return result
}

String.prototype.hexDecode = function () {
    var j;
    var hexes = this.match(/.{1,4}/g) || [];
    var back = "";
    for (j = 0; j < hexes.length; j++) {
        back += String.fromCharCode(parseInt(hexes[j], 16));
    }

    return back;
}

if (params.length >= 3) {
    facility_id = params[0].substring(params[0].indexOf('=') + 1);
    customer_number = params[1].substring(params[1].indexOf('=') + 1);
    unsubscribe_email = params[2].substring(params[2].indexOf('=') + 1);

    client_string = facility_id + customer_number + unsubscribe_email.substring(unsubscribe_email.indexOf('@')).hexEncode();
    //client_string = client_string.hexEncode();

    gServiceUrl += facility_id + '/' + customer_number + '/' + unsubscribe_email + '/' + client_string;
    getCORS(gServiceUrl, null, function (data) {
        if (data == "success")
            unsubscribe_msg = "We're sorry to see you go. <i>" + unsubscribe_email + "</i> has been unsubscribed from future Gizmo emails.";
    }, function (error_response) {
        showErrorResponseText(error_response);
    });
}
//******************** Global Variables End **************************
//******************** Page Load Events Start **************************
$(document).on('pageshow', '#_unSubscribe', function () {
    $('.ui-grid-solo div.ui-block-a h3').html(unsubscribe_msg);
    $('.ui-grid-solo div.ui-block-a h3').css({ 'color': 'grey', 'font-weight': 'normal', 'padding-top': '30px' });
});

$('#_unSubscribe').live('pagebeforecreate', function () {
});

//******************** Page Load Events End **************************

