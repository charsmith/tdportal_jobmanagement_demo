jQuery.support.cors = true;

//******************** Global Variables Start **************************
var gServiceUrl = '../JSON/_indexCamping.JSON';
var gData;
var gPageSize = 50;
var gDataStartIndex = 0;
var gDataList = [];
//******************** Global Variables Start **************************

//******************** Page Load Events Start **************************
$(document).on('pageshow', '#_cwJobHistory', function () {
    makeGData();
    $("#_cwJobHistory").trigger("create");
});

$('#_cwJobHistory').live('pagebeforecreate', function () {
    $('#lnkNewJob').attr('href', 'pages/jobSelectTemplate.html');
});
//******************** Page Load Events End ****************************

//******************** Public Functions Start **************************
function updateStartIndex(selector_id, list_div, caption, column) {
    var page_array = [];
    var multiplier = parseInt($('#' + selector_id).prop("selectedIndex"));
    gDataStartIndex = multiplier * gPageSize;

    if ($('#search' + list_div).val() != "" && $('#search' + list_div).val() != undefined) {
        searchHistory(selector_id, list_div, caption, column)
    }
    else {

        $.each(gDataList, function (facility_key, facitlity_val) {
            if (facitlity_val.list != null && (facitlity_val.list.toLowerCase() == caption.toLowerCase()))
                page_array = facitlity_val.items;
        });
        makeList(page_array, selector_id, list_div, caption, parseInt(column));
    }
    $("div ul").each(function () {
        $(this).listview();
    });
    if (!jQuery.browser.msie) {
        $("div select").each(function (i) {
            $(this).selectmenu();
        });
    }
    $("#containDiv").css({ "position": "absolute",
        "top": "0",
        "right": "0",
        "margin": "0",
        "padding": "0",
        "width": "120px"
    });
    $("#containDiv").attr({ "data-role": "controlgroup", "data-type": "horizontal", "data-mini": "true" });
}

function createFacility(facility_name, job_list, add_to_column) {
    $('#ulJobHistory').append(job_list);
    $('#ulJobHistory').listview('refresh');
}

function makeList(array_name, selector_id, list_div, caption, column) {
    var header;
    var selector;

    var array = eval(array_name);

    var selector = "";

    var page_size = Math.round(array.length / gPageSize);
    if (page_size * gPageSize < array.length) {
        page_size++;
    }
    if (gDataStartIndex > array.length) gDataStartIndex = 0;
    var selected_index = $('#' + selector_id).prop("selectedIndex");
    var more_list_id = (selected_index == undefined) ? 0 : selected_index;
    if ((more_list_id + 1) > page_size) {
        more_list_id = selected_index = 0;
    }
    var selector = '<div id="containDiv" style="position: absolute; top: 0; right: 0; margin: 0; padding: 0; width:120px;" data-role="fieldgroup" data-type="horizontal" data-mini="true" ><select name="' + selector_id + '" id="' + selector_id + '" data-theme="b" data-overlay-theme="d" data-native-menu="true" data-mini="true" onchange="updateStartIndex(\'' + selector_id + '\',\'' + list_div + '\',\'' + caption + '\',\'' + column + '\')">';
    for (var i = 0; i < page_size; i++) {
        selector = (i == selected_index) ? selector + '<option selected value="' + i + '">Page ' + (1 + i) + '</option>' : selector + '<option value="' + i + '">Page ' + (1 + i) + '</option>';
    }
    selector += '</select></div>';

    var temp_array = (array.length > gDataStartIndex) ? array.slice(gDataStartIndex, gDataStartIndex + gPageSize) : array;

    var search_field = '<li data-theme="c" ><input type="search" name="search" id="search" value="" onkeyup="searchHistory(\'' + selector_id + '\',\'' + list_div + '\',\'' + caption + '\',\'' + column + '\');" onchange="searchHistory(\'' + selector_id + '\',\'' + list_div + '\',\'' + caption + '\',\'' + column + '\')"/></li>';
    temp_array.splice(0, 0, search_field)

    header = '<li data-role="list-divider" id="li' + selector_id + '" ><a style="text-decoration:none; color:#fff;">JOB HISTORY</a>' + selector + '</li>';
    temp_array.splice(0, 0, header)

    if ((more_list_id + 1) < page_size) {
        var more_list = '<li data-theme="c" ><a style="text-decoration:none;" href="#" onclick="getMoreData(\'' + selector_id + '\',\'' + (more_list_id + 1) + '\');">More...</a></li>';
        temp_array.splice(temp_array.length, 0, more_list);
    }
    var search_value = $('#search').val();
    $('#ulJobHistory').empty();
    $('#ulJobHistory').append(temp_array.join(' '));
    $('#ulJobHistory').listview('refresh');
    $('.ui-page').trigger('create');
    $('#search')[0].focus();
    $('#search').val(search_value);
    $('#lnkLess').css("color", "#2489ce");
    $('#lnkMore').css("color", "#2489ce");
}

function searchHistory(selector_id, list_div, caption, column) {
    var filterd_list = [];    
    var search_value = $('#search').val();
    if (search_value != "" && search_value != undefined) {
//        gDataStartIndex = 0;
//        $('#' + selector_id).val(gDataStartIndex);

        var searched_li = jQuery.grep(gDataList[0].items, function (a, b) {
            var a1 = $(a);
            var filter_text = a.substring(a.indexOf('data-filtertext'), a.indexOf('><a') - 1);
            filter_text = filter_text.substring(filter_text.indexOf('=') + 2);
            if (filter_text.toLowerCase().indexOf($('#search').val().toLowerCase()) > -1) {
                filterd_list.push(a);
            }
            return (filter_text.toLowerCase().indexOf($('#search').val().toLowerCase()) > -1);
        });
        makeList(filterd_list, selector_id, list_div, caption, column);
    }
    else {
        gDataStartIndex = 0;
        $('#' + selector_id).val(gDataStartIndex);
        makeList(gDataList[0].items, selector_id, list_div, caption, column);
    }
}

function getSelectedNav(nav_text) {
    $('#hidNavText').val(nav_text);
}

function makeGData() {
    $.ajax({
        url: gServiceUrl,
        dataType: 'json',
        timeout: 10000,
        type: 'GET',
        async: false,
        success: function (data) {
            gData = jQuery.grep(data, function (obj) {
                return obj.facilityName.toUpperCase() == "OPEN JOBS";
            });
            buildLists();
        },
        error: function (xml_http_request, text_status, error_thrown) {
            $('#alertmsg').text("Error status :" + text_status + ". Error type: " + error_thrown + ". Error Message: " + xml_http_request.responseXML);
            $('#popupDialog').popup('open');
        }
    });
}
function buildLists() {
    var pager = ""
    var facility_counter = 0;
    var column_counter = 0;
    $.each(gData, function (facility_key, facitlity_val) {

        facility_counter++;
        if (facitlity_val.facilityName != null && facitlity_val.facilityName != "") {
            var facility_jobs = facitlity_val.jobDict;
            var temp_facility_data = [];
            var selector_id = "", list_div = "", caption = "";
            selector_id = facitlity_val.facilityName.toLowerCase() + 'Select';
            list_div = facitlity_val.facilityId;
            caption = facitlity_val.facilityName;
            qtyName = "";
            dateName = "";
            var job_counter = 0;
            var navlink_counter = '';
            $.each(facility_jobs, function (job_key, job_val) {
                for_link_caption = caption.replace(' ', '');
                job_counter = job_counter + 1;
                navlink_counter = job_counter + "_" + for_link_caption + "_" + job_val.pk;

                var filter_text = job_val.jobName + ' ' + job_val.date + ' ' + job_val.actualQty + ' ' + job_val.actualQty.replace(',', '') + ' ' + job_val.qty + ' ' + job_val.qty.replace(',', '');

                var li = '<li id="' + job_val.jobName + '" data-filtertext="' + filter_text + '"><a href="#" id="navLink' + navlink_counter + '" onclick="storeJobInfo(\'' + navlink_counter + '\',\'' + job_val.pk + '\',\'' + "" + '\',\'' + job_val.jobName + '\',\'' + list_div + '\')" title="quantity:' + job_val.qty + '"><img src="' + job_val.orderImage + '"/><h3>' + job_val.jobName + '</h3>'
                dateName = "In Home : ";
                if (job_val.qty != "") {
                    qtyName = "Estimated Qty : ";
                    li += '<p>' + qtyName + job_val.qty;
                }

                if (job_val.actualQty != "") {
                    qtyName = "Actual Qty : ";
                    li += ' | ' + qtyName + job_val.actualQty + '</p>'
                }
                li += '<p class="ui-li-aside">' + dateName + ' <b>' + job_val.date + '</b></p></a></li>';
                temp_facility_data.push(li);
            });
            if (facility_counter % 2 == 1)
                column_counter = 2;
            else if (facility_counter % 2 == 0)
                column_counter = 3;
            gDataList.push({
                "list": facitlity_val.facilityName,
                "items": temp_facility_data
            });
            makeList(temp_facility_data, selector_id.replace(" ", "_"), list_div, caption, column_counter);
        }

    });
}
//******************** Public Functions End **************************