﻿//******************** Model Creation Start **************************
var mapping = { 'ignore': ["__ko_mapping__"] }
var jobScheduling = function (self, jobSchedulingInfo) {
    var gJobSchDataAll;
    var gJobSchData;
    self.driveCalendarInfo = ko.observableArray([]);
    self.showPastDates = ko.observable("off");
    self.showCompletedJobs = ko.observable("on");
    self.orderBy = ko.observable("oldestToNewest");
    self.showJobsBy = ko.observable("inhomeDate");
    self.viewMode = ko.observable("futureJobs");
    //self.dateInfo = ko.observable("Date Info");
    self.pageCount = ko.observable(1);
    var navType = "next";
    var tempTotalFutureJobsCount = 20;
    var maxFutureJobs = 5;
    self.currentPageJobsCount = ko.observable("-1");
    self.currentPageStartIndex = ko.observable(-1);
    self.currentWeekDate = ko.observable('00/00/0000');
    self.areNoJobs = ko.observable(false);
    var currentDate = new Date();
    currentDate = currentDate.getMonth() + "/" + currentDate.getDate() + "/" + currentDate.getFullYear();
    var selectedDate = "";
    //var pageCount = 1;
    $("#popupMonthView").bind({
        popupafterclose: function (event, ui) {
            if (self.viewMode() != 'day') {
                self.viewMode('week');
            }
            $('#calendar').fullCalendar('destroy');
            getListView();
            $('input[type=radio]').checkboxradio('refresh');
            $("#ulDriveCalendar").listview().listview('refresh');
            $('#dvJobScheduling').collapsibleset('refresh');
        }
    });
    self.filterTerm = ko.computed(function () {
        var term = "";
        if (self.showJobsBy() == "inhomeDate")
            term = "In-Home";
        else if (self.showJobsBy() == "productionDate")
            term = "Production";
        else if (self.showJobsBy() == "mailDate")
            term = "Mail Drop";
        return term;
    });


    self.DriveCalendar = function (key, event_date, event) {
        this.children = ko.observableArray(event);
        this.type = key;
        this.eventDate = event_date;
        this.eventDateTitle = self.makeTitle(event_date);
        if (event.image != undefined)
            this.imagePath = event.image;
    }

    self.jobEvent = function (event) {
        var self = this;
        var job_customer_number = (event["Customer Number"] != undefined && event["Customer Number"] != null && event["Customer Number"] != "") ? parseInt(event["Customer Number"]) : appPrivileges.customerNumber;
        var job_number = (event["Job Number"] != undefined && event["Job Number"] != null && event["Job Number"] != "") ? parseInt(event["Job Number"]) : '';
        ko.mapping.fromJS(new JobModel(event["Job Name"], event["In Home"], "", "", 'c', job_number, job_customer_number, sessionStorage.facilityId, "", "", event["isGizmo"]), {}, self);
        ko.mapping.fromJS(event, {}, self);
        self.title = ko.observable(event["Job Name"]);
        self.orderedBy = ko.observable(event["Last Updated By"]);
        self.status = ko.observable(event.status);
        self.image = ko.observable(bindImage(event.status.toString()));
        self.theme = ko.observable('c');
        self.dataIcon = ko.observable('');
    }

    self.makeTitle = function (event_date) {
        var date_string;
        var d = new Date(event_date);
        var dayNames = new Array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
        var monthNames = ["January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"];
        date_string = dayNames[d.getDay()] + ', ' + monthNames[d.getMonth()] + ' ' + d.getDate() + ', ' + d.getFullYear();
        return date_string;
    }

    self.makeFutureDateRangeTitle = function (start_date, end_date) {
        var date_range;
        var d1 = new Date(start_date);
        var d2 = new Date(end_date);
        var dayNames = new Array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
        var monthNames = ["January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"];
        if (d1.getFullYear() == d2.getFullYear() && d1.getMonth() == d2.getMonth())
            date_range = monthNames[d1.getMonth()].substring(0, 3) + ' ' + d1.getDate() + ' - ' + d2.getDate() + ', ' + d1.getFullYear();
        else if (d1.getFullYear() == d2.getFullYear() && d1.getMonth() != d2.getMonth())
            date_range = monthNames[d1.getMonth()].substring(0, 3) + ' ' + d1.getDate() + ' - ' + monthNames[d2.getMonth()].substring(0, 3) + ' ' + d2.getDate() + ', ' + d1.getFullYear();
        else
            date_range = monthNames[d1.getMonth()].substring(0, 3) + ' ' + d1.getDate() + ' ' + d1.getFullYear() + ' - ' + monthNames[d1.getMonth()].substring(0, 3) + ' ' + d2.getDate() + ', ' + d1.getFullYear();
        return date_range;
    };

    function calendarStyles() {
        var isMobile = false;
        var mobile = ['iphone', 'ipad', 'ipod', 'android', 'blackberry', 'nokia', 'opera mini', 'windows mobile', 'windows phone', 'iemobile'];
        for (var i in mobile)
            if (navigator.userAgent.toLowerCase().indexOf(mobile[i].toLowerCase()) > 0) {
                isMobile = true;
            }
        $('div.fc-row,.fc-week,.fc-widget-content').css('height', '');
        $('div.fc-row,.fc-week,.fc-widget-content').css('height', '20px !important');
        $('#calendar div.fc-scroller,.fc-day-grid-container').css('height', '');
        $('#calendar div.fc-scroller,.fc-day-grid-container').css('height', '100% !important');
        if (!isMobile) {
            $('#popupMonthView-popup').removeClass('mobile');
            $('#popupMonthView-popup').addClass('notMobile');
        }
        else {
            $('#popupMonthView-popup').removeClass('notMobile');
            $('#popupMonthView-popup').addClass('mobile');
        }
    }

    function createViewModel() {
        var drive_calendar_model = [];

        var all_events_data = [];
        var term = self.filterTerm();
        var source_data = sortBy(gJobSchDataAll, term);
        //Filter the jobs based on Mode(week).
        if (self.viewMode() == "week") {
            all_events_data = getWeeklyJobs(source_data, term);
        }
        else if (self.viewMode() == "month") {
            var month_jobs = makeMonthView(source_data, term);
            getMonthView(month_jobs);
        }
        else if (self.viewMode() == "day") {
            all_events_data = getDayViewJobs(source_data, term);
        }
        else if (self.viewMode() == "futureJobs") {
            all_events_data = getFutureJobs(source_data, term);
        }

        // all_events_data = sortBy(all_events_data, "eventDate");
        $.each(all_events_data, function (p, q) {
            var evt_date = new Date(new Date(q.eventDate).getFullYear(), new Date(q.eventDate).getMonth(), new Date(q.eventDate).getDate());
            var drive_calendar;
            if (q.jobs.length > 0) {
                drive_calendar = new self.DriveCalendar("", q.eventDate, q.jobs);
                drive_calendar_model.push(drive_calendar);
            }
        });
        return drive_calendar_model;
    }

    self.modeChange = function () {
        if (self.viewMode() == "futureJobs") {
            self.currentPageJobsCount = ko.observable("-1");
            self.currentPageStartIndex = ko.observable(-1);
        }
        else if (self.viewMode() == "week") {
            navType = "next";
            self.currentWeekDate = ko.observable('00/00/0000');
        }

        getListView();
        $("#ulDriveCalendar").listview().listview('refresh');
        $('#dvJobScheduling').collapsibleset('refresh');
    };

    self.settingsChange = function () {
        if (self.viewMode() == "week") {
            if (self.pageCount() > 1)
                self.currentWeekDate((new Date(self.currentWeekDate()).getWeekStartDate()));
            else
                self.currentWeekDate('00/00/0000');
            //self.pageCount(1);
        }
        getListView();
        $("#ulDriveCalendar").listview().listview('refresh');
        $('#dvJobScheduling').collapsibleset('refresh');
    }

    function bindEventsData(events, evt_date, curr_date, main_date, showCompleted, type) {
        var events_array = [];
        $.each(events, function (c, d) {
            //if (type != "")
            d["image"] = bindImage(d.status);
            /*if ((showCompleted == "on" && d.status.toLowerCase() != "completed") || showCompleted == "off") {
                if (((Math.abs(evt_date - curr_date) / (1000 * 60 * 60 * 24)) == 1) && evt_date > curr_date) {
                    d["dataTheme"] = 'e';
                }
                else if (((Math.abs(curr_date - evt_date) / (1000 * 60 * 60 * 24)) >= 1) && curr_date > evt_date && d.status == '') {
                    d["dataTheme"] = 'h';
                    d["dataIcon"] = 'myapp-attention';
                }
                else if (d.status.toLowerCase() == 'completed') {
                    d["dataTheme"] = 'i';
                    d["dataIcon"] = 'check';
                }
                else {
                    d["dataTheme"] = 'c';
                    d["dataIcon"] = '';
                }
                events_array.push(d);
            }*/
            d["dataTheme"] = 'c';
            d["dataIcon"] = '';
            events_array.push(d);

        });
        var drive_calendar;
        if (events_array.length > 0)
            drive_calendar = new self.DriveCalendar(type, main_date, events_array);
        return drive_calendar;
    }

    self.loadEvents = function () {
        if ($('#ddlPageCustomerType').val() != "All")
            gJobSchData = gJobSchDataAll[$('#ddlPageCustomerType').val()];
        //ko.applyBindings(createViewModel(), $('#divList')[0]);
        bindgJobSchData();
    }
    Date.prototype.getWeekEndDate = function () {
        diff = 6 - this.getDay();
        if (diff < 0) {
            diff += 6;
        }
        this.setDate(this.getDate() + (1 * diff));
        return this;
    }
    Date.prototype.getWeekStartDate = function () {
        diff = this.getDay() - 7;
        if (diff < 0) {
            diff += 7;
        }
        return this.setDate(this.getDate() + (-1 * diff));
    }
    self.getWeekRange = function () {


    };
    self.canDisablePrevNav = ko.computed(function () {
        if (self.viewMode() == "futureJobs")
            return (self.currentPageStartIndex() <= 0);
        else if (self.viewMode() == "week")
            return (self.pageCount() == 1);
        else if (self.viewMode() == "day" && navType == "next" && self.currentWeekDate() == "00/00/0000")
            return (self.areNoJobs());

    });
    self.canDisableNextNav = ko.computed(function () {
        if (self.viewMode() == "futureJobs")
            return (self.currentPageStartIndex() + self.currentPageJobsCount() >= (tempTotalFutureJobsCount - 1));
        else if (self.viewMode() == "week" || self.viewMode() == "day")
            return (self.pageCount() > 1 && self.areNoJobs());
        else if (self.viewMode() == "day" && navType == "prev" && self.currentWeekDate() != "00/00/0000")
            return (self.pageCount() > 0 && self.areNoJobs());
    });
    //******************** Model Creation End **************************
    self.dateInfo = ko.computed(function () {
        //return self.currentPageStartIndex() + ":" + self.currentPageJobsCount() + ":" + self.pageCount();
        if (self.driveCalendarInfo().length > 0) {
            var a = new Date(self.driveCalendarInfo()[0].eventDate);
            var b = new Date(self.driveCalendarInfo()[self.driveCalendarInfo().length - 1].eventDate);
            if (a > b)
                return self.makeFutureDateRangeTitle(self.driveCalendarInfo()[self.driveCalendarInfo().length - 1].eventDate, self.driveCalendarInfo()[0].eventDate);
            else
                return self.makeFutureDateRangeTitle(self.driveCalendarInfo()[0].eventDate, self.driveCalendarInfo()[self.driveCalendarInfo().length - 1].eventDate);
        }
        else
            return "";
    });
    self.clickNav = function (data, el) {
        var ctrl = el.target || el.currentTarget || el.targetElement;
        if ($(ctrl).attr("data-NavType") == "prev") {
            if (self.viewMode() == "futureJobs")
                self.currentPageStartIndex((self.pageCount() <= 0) ? -1 : self.currentPageStartIndex() - maxFutureJobs);
            //if (self.viewMode() == "week")
            //    self.currentWeekDate(new Date((new Date(self.currentWeekDate())).setDate((new Date(self.currentWeekDate())).getDate() - 7)));
            //if (self.viewMode() == 'day') {
            //    self.currentWeekDate(new Date((new Date(self.currentWeekDate())).setDate((new Date(self.currentWeekDate())).getDate() - 1)));
            //}
            if (self.pageCount() > 0)
                self.pageCount(self.pageCount() - 1);

            navType = "prev";
            getListView();
        }
        else {
            navType = "next";
            self.currentPageStartIndex(self.currentPageStartIndex() + maxFutureJobs);
            if (self.viewMode() == "week" && self.orderBy() == "newestToOldest") {
                self.currentWeekDate(new Date((new Date(self.currentWeekDate())).setDate((new Date(self.currentWeekDate())).getDate() - 7)));
            }
            //if (self.viewMode() == 'day') {
            //    self.currentWeekDate(new Date((new Date(self.currentWeekDate())).setDate((new Date(self.currentWeekDate())).getDate() + 1)));
            //}
            getListView();
            if (self.driveCalendarInfo().length > 0)
                self.pageCount(self.pageCount() + 1);
        }
        $("#ulDriveCalendar").listview().listview('refresh');
        $('#dvJobScheduling').collapsibleset('refresh');
    };

    //******************** Public Functions Start **************************
    self.makeJobSchedulingData = function () {
        //    getCORS(fileUploadURL, null, function (data) {
        //        gJobSchData = data;
        //        //ko.applyBindings(createViewModel(), $('#divList')[0]);
        //        $('#divList').empty();
        //        ko.cleanNode($('#divList')[0]);
        //        var model = createViewModel();
        //        ko.applyBindings(model, $('#divList')[0]);
        //        $.each($('#divList'), function (obj) {
        //            $(this).collapsibleset();
        //        });
        //        $("#divList ul").each(function (i) {
        //            $(this).listview();
        //        });
        //    }, function (error_response) {
        //        showErrorResponseText(error_response);
        //    });
        //$.getJSON(driveCalendarURL, function (data) {
        gJobSchDataAll = jobSchedulingInfo;

        $.each(gJobSchDataAll, function (key, val) {
            var temp_date = new Date(val["In-Home"]);
            temp_date.setDate(temp_date.getDate() - 2);
            val["Production"] = temp_date;
            temp_date.setDate(temp_date.getDate() - 1);
            val["Mail Date"] = temp_date;
            if (key % 2 == 1)
                val["status"] = 1;
            else if (key % 2 == 0)
                val["status"] = 0;
            else if (key % 5 == 0)
                val["status"] = 4;
        });

        //if ($('#ddlPageCustomerType').val() != "All") {
        // gJobSchData = gJobSchDataAll[$('#ddlPageCustomerType').val()];


        //gJobSchData = sortBy(gJobSchData, "eventDate");
        gJobSchData = gJobSchDataAll;

        //}
        //else {
        //    gJobSchData = gJobSchDataAll;
        //}


        //if ($('input[id=popMonth].ui-btn-up-b').length > 0)
        //    $('#popMonth').trigger('change');
        //else if ($('input[id=popList].ui-btn-up-b').length > 0) {
        //    ko.cleanNode($('#ulDriveCalendar')[0]);
        //    $('#popList').trigger('change');
        //}
        //else
        //    $('#popMonth').trigger('change');
        getListView();
        //ko.applyBindings(createViewModel(), $('#divList')[0]);
        //$('#ulDriveCalendar').empty();
        //ko.cleanNode($('#ulDriveCalendar')[0]);

        //});
    }

    self.fnEditPopUp = function (event, event_date, key, type, event_index, event_date_index) {
        var eventData = jQuery.parseJSON(event);
        var date = event_date;
        $("#hidEventData").val(JSON.stringify(eventData) + '|' + date + '|' + type + '|' + event_index + '|' + event_date_index);
        fnResetPopup();
        switch (key) {
            case "add":
                {
                    $('#tdAddBtn').css('visibility', 'visible');
                    $('#tdSubmitBtn').css('visibility', 'hidden');
                    $('#tdDeleteBtn').css('visibility', 'hidden');
                    //$('#dvStatus').css('visibility', 'hidden');
                    $('#editHeader').html('Add Event');
                    $("#editPopupEvent").popup("open");
                    break;
                }
            case "edit":
                {
                    $('#tdAddBtn').css('visibility', 'hidden');
                    $('#tdSubmitBtn').css('visibility', 'visible');
                    $('#tdDeleteBtn').css('visibility', 'visible');
                    //$('#dvStatus').css('visibility', 'visible');
                    $('#editHeader').html('Edit Event');
                    $("#editPopupEvent").popup("open");
                    $("#txtDate").val(date);
                    $("#txtTitle").val(eventData.title);
                    $("#txtNotes").val(eventData.notes);
                    $('#ddlCustomertype').val(type).selectmenu('refresh');
                    if (eventData.status.toLowerCase() == 'completed')
                        $('#chkTask').attr('checked', true).checkboxradio('refresh');
                    else
                        $('#chkTask').removeAttr('checked', false).checkboxradio('refresh');
                    break;
                }
        }
    }

    self.fnResetPopup = function () {
        $("#txtDate").val('');
        $("#txtTitle").val('');
        $("#txtNotes").val('');
        if ($("#chkTask").is(':checked'))
            $('#chkTask').removeAttr('checked', false).checkboxradio('refresh');
    }

    self.fnCancel = function () {
        $('#editPopupEvent').popup('close');
    }

    function getCurrentDate() {
        var curr_date = new Date(new Date(currentDate).getFullYear(), new Date(currentDate).getMonth() + 1, new Date(currentDate).getDate());
        return curr_date;
    }

    function getFilteredJobs(jsonArray, curr_date, key) {
        var filtered_array = $(jsonArray).filter(function (idx) {
            var temp_date = (jsonArray[idx][key]) ? new Date(jsonArray[idx][key]) : new Date(jsonArray[idx]["eventDate"]);
            var date_term = new Date(temp_date.getFullYear(), temp_date.getMonth(), temp_date.getDate());
            if (self.showPastDates() == "off")
                return (self.showCompletedJobs() == "off" && jsonArray[idx][key]) ? (date_term > curr_date && jsonArray[idx].status != 1) : (date_term > curr_date);
            else if (self.viewMode() != "futureJobs" && self.showPastDates() == "on")
                return (self.showCompletedJobs() == "off" && jsonArray[idx][key]) ? (jsonArray[idx].status != 1) : true;
            else
                return true;

        });
        return filtered_array;
    }

    function areJobsAvailableInNextWeeek(sorted_array, start_date, end_date, key_term) {
        var jobs_found = false;
        //if (navType == "prev")
        //    var temp_array = (new Array(sorted_arrray))[0].reverse();
        //else 
        //    var temp_array = sorted_array;
        $.each(sorted_array, function (key, val) {
            if (((navType == "next" && self.orderBy() == "oldestToNewest") || (navType == "prev" && self.orderBy() == "newestToOldest")) && (new Date(val[key_term]) > end_date)) {
                jobs_found = true;
                self.currentWeekDate(new Date(val[key_term]));
                return false;
            }
            if (((navType == "prev" && self.orderBy() == "oldestToNewest") || (navType == "next" && self.orderBy() == "newestToOldest")) && (new Date(val[key_term]) < start_date)) {
                jobs_found = true;
                self.currentWeekDate(new Date(val[key_term]));
                return false;
            }
            if (!jobs_found && sorted_array.length == (key + 1) && self.viewMode() == 'day')
                self.currentWeekDate(val[key_term]);
        });

        self.areNoJobs(!jobs_found);
    }

    function getWeeklyJobs(sorted_array, key) {
        var all_events_data = [], temp_array = [];
        var start_date, end_date;
        if (self.currentWeekDate() != "00/00/0000") {
            //var t_array = (new Array(sorted_array)).reverse();
            var t_array = new Array(sorted_array);
            temp_array = $.grep(t_array[0], function (obj) {
                if (navType == 'prev')
                    if (self.orderBy() == "newestToOldest")
                        return (new Date(obj[key])) > self.currentWeekDate();
                    else
                        return (new Date(obj[key])) < self.currentWeekDate();
                else {
                    if (self.orderBy() == "newestToOldest")
                        return (new Date(obj[key])) < self.currentWeekDate();
                    else
                        return (new Date(obj[key])) > self.currentWeekDate();
                }
            });
        }
        if (temp_array.length == 0)
            temp_array = sorted_array;
        /* if (navType == "prev" && temp_array.length > 0)
            temp_array = temp_array.reverse();
        else
            temp_array = sorted_array;*/
        var filterd_list = $(temp_array).filter(function (idx) {
            var temp_date = (temp_array[idx][key]) ? new Date(temp_array[idx][key]) : new Date(temp_array[idx]["eventDate"]);
            if (all_events_data.length == 0) {
                start_date = new Date(temp_date.getWeekStartDate());
                end_date = new Date(temp_date.getWeekEndDate());
            }
            //self.currentWeekDate(temp_array[idx][key]);
            //if (self.currentWeekDate() == "00/00/0000") {
            //    start_date = new Date(temp_date.getWeekStartDate());
            //    end_date = new Date(temp_date.getWeekEndDate());
            //    self.currentWeekDate(temp_array[idx][key]);
            //}
            //else if (navType == "next" && (new Date(temp_array[idx][key])) > (new Date(self.currentWeekDate())) && all_events_data.length == 0/*(new Date(sorted_array[idx][key])) != (new Date(self.currentWeekDate()))*/) {
            //    start_date = new Date(temp_date.getWeekStartDate());
            //    end_date = new Date(temp_date.getWeekEndDate());
            //    self.currentWeekDate(temp_array[idx][key]);
            //}
            //else if (navType == "prev" && (new Date(temp_array[idx][key])) < (new Date(self.currentWeekDate())) && all_events_data.length == 0/*(new Date(sorted_array[idx][key])) != (new Date(self.currentWeekDate()))*/) {
            //    start_date = new Date(temp_date.getWeekStartDate());
            //    end_date = new Date(temp_date.getWeekEndDate());
            //    self.currentWeekDate(temp_array[idx][key]);
            //}

            if (temp_date >= start_date && temp_date <= end_date) {
                var found_index = -1;
                $.each(all_events_data, function (c, d) {
                    if (d.eventDate == temp_array[idx][key]) {
                        found_index = c;
                        return false;
                    }
                });
                if (found_index > -1) {
                    all_events_data[found_index].jobs.push(new self.jobEvent(temp_array[idx]));
                }
                else {
                    var tmp = [];
                    tmp.push(new self.jobEvent(temp_array[idx]));
                    all_events_data.push({
                        "eventDate": temp_array[idx][key],
                        "jobs": tmp
                    });
                }
                return true;
            }
            else return false;
            //return (temp_date >= start_date && temp_date <= end_date);
        });
        areJobsAvailableInNextWeeek(sorted_array, start_date, end_date, key);

        if (navType == "next" && self.areNoJobs() && start_date) {
            navType = "prev";
            //areJobsAvailableInNextWeeek(sorted_array, start_date, end_date, key);
            self.currentWeekDate(start_date);
        }
        else if (navType == "prev" && self.areNoJobs() && end_date) {
            navType = "next";
            //areJobsAvailableInNextWeeek(sorted_array, start_date, end_date, key);
            self.currentWeekDate(end_date);
        }
        else if (!self.areNoJobs() && navType == "next" && end_date)
            self.currentWeekDate(end_date);
        else if (!self.areNoJobs() && navType == "prev" && start_date)
            self.currentWeekDate(start_date);

        return (navType == "prev") ? all_events_data/*.reverse()*/ : all_events_data;
    }

    function getFutureJobs(source_data, term) {
        var all_events_data = [];
        var term = self.filterTerm();
        var job_cnt = 0;
        //var source_data = sortBy(gJobSchDataAll, term);
        tempTotalFutureJobsCount = source_data.length;
        var date_range = "";
        $.each(source_data, function (a, b) {
            var found_index = -1;
            var tmp_date = new Date(b[term]);
            tmp_date = tmp_date.getMonth() + "/" + tmp_date.getDate() + "/" + tmp_date.getFullYear();
            if (b[term] && (a >= self.currentPageStartIndex())) {
                $.each(all_events_data, function (c, d) {
                    if (d.eventDate == b[term]) {
                        found_index = c;
                        return false;
                    }
                });
                if (found_index > -1) {
                    all_events_data[found_index].jobs.push(new self.jobEvent(b));
                }
                else {
                    var tmp = [];
                    tmp.push(new self.jobEvent(b));
                    all_events_data.push({
                        "eventDate": b[term],
                        "jobs": tmp
                    });
                }
                job_cnt++;
                if (job_cnt == 1)
                    self.currentPageStartIndex(a);
                if ((self.viewMode() == "futureJobs" && job_cnt == maxFutureJobs) || a == (source_data.length - 1)) {
                    return false;
                }
                self.currentPageJobsCount(job_cnt);
            }
        });
        return all_events_data;
    }

    function getDayViewJobs(source_data, term) {
        var filtered_jobs = [];/* $(source_data).filter(function (idx) {
            return ((new Date(source_data[idx][term])) == (new Date(self.currentWeekDate())));
        });*/
        var job_found = false;
        var new_arr = [];
        $.each(source_data, function (key, val) {
            if ((new Date(val[term])).toString() == (new Date(self.currentWeekDate())).toString()) {
                filtered_jobs.push(val);
                job_found = true;
            }
            new_arr.push(val);
        });

        if (navType == "prev")
            new_arr = new_arr.reverse();
        var start_date = (new Date(self.currentWeekDate()));
        var end_date = (new Date(self.currentWeekDate()));
        areJobsAvailableInNextWeeek(new_arr, (new Date(self.currentWeekDate())), end_date, term);


        var all_events_data = [];
        var tmp = [];
        var evt_date = "";
        $.each(filtered_jobs, function (key, val) {
            evt_date = val[term];
            tmp.push(new self.jobEvent(val));
        });

        all_events_data.push({
            "eventDate": evt_date,
            "jobs": tmp
        });
        if (navType == "next" && self.areNoJobs()) {
            navType = "prev";
            //areJobsAvailableInNextWeeek(new_arr.reverse(), end_date, end_date, term);
        }
        else if (navType == "prev" && self.areNoJobs()) {
            navType = "next";
        }

        return all_events_data;
    }

    function makeMonthView(sorted_array, term) {
        var eventData = [];

        var current_month = (new Date(self.currentWeekDate())).getMonth();

        $.each(sorted_array, function (p, q) {
            //if ((new Date(q[term])).getMonth() == current_month) {
            eventData.push({
                title: (q["Job Name"].length > 10) ? q["Job Name"].substring(0, 10) + '...' : q["Job Name"],
                jobName: q["Job Name"],
                start: new Date(q[term]),
                backgroundColor: (q["status"] == 1) ? 'green' : ((q["status"] == 0) ? 'red' : 'yellow'),
                borderColor: "lightgrey",
                textColor: (q["status"] != 1 && q["status" != 0]) ? black : "#ffffff",
                notes: "",
                status: q.status,
                type: p
            });
            //if (p == 3) {
            //    eventData.push({
            //        title: 'Nth More...',
            //        jobName: 'Nth More...',
            //        start: new Date(q[term]),
            //        backgroundColor: 'lightBlue',
            //        borderColor: "lightgrey",
            //        textColor: "#ffffff",
            //        notes: "",
            //        status: '',
            //        type: ''
            //    });
            //}
            //}
        });
        return eventData;
    }

    function getMonthView(month_jobs) {
        $('#calendar').show();
        $('#popupMonthView').popup('close');
        $('#calendar').fullCalendar({
            defaultView: 'month',
            editable: true,

            events: month_jobs,
            eventLimit: 4, // If you set a number it will hide the itens
            eventLimitText: "More",
            eventRender: function (event, element) {
                //element.attr('onclick', 'openEventPopup("' + event.title + '","' + event.start + '","' + event.notes + '","' + event.type + '","' + event.status + '");');
                element.attr('title', event.jobName);
                element.innerText = (event.title.length > 10) ? event.title.substring(0, 10) + '...' : event.title;
                //switch (event.type) {
                //    case "Suddenlink":
                //        element.find("div.fc-event-inner").prepend("<img src='customerLogos/suddenlink_cal_icon.png' class= 'ui-li-icon' title ='Suddenlink' >");
                //        break;
                //    case "Conversion Alliance":
                //        element.find("div.fc-event-inner").prepend("<img src='customerLogos/tribune_cal_icon.png' class= 'ui-li-icon' title ='Conversion Alliance' >");
                //        break;
                //    case "Pluris":
                //        element.find("div.fc-event-inner").prepend("<img src='customerLogos/pluris_cal_icon.png' class= 'ui-li-icon' title ='Pluris'>");
                //        break;
                //}
            }, eventClick: function (calEvent, jsEvent, view) {
                self.viewMode('day');
                var temp = new Date(calEvent.start);
                var tmp_date = temp.getMonth() + 1 + "/" + temp.getDate() + "/" + temp.getFullYear();
                navType = "next";
                self.currentWeekDate(new Date(tmp_date));
                $('#popupMonthView').popup('close');
            }, windowResize: function (view) {
                calendarStyles();
            },
            viewRender: function (view, element) {
                window.setTimeout(function () {
                    //$('#calendar div.fc-scroller,.fc-day-grid-container').css('height', '');
                    calendarStyles();
                }, 0);
            },
            width: ($(window).width() * .60)
        });
        $('#divMonthView').trigger('create');
        window.setTimeout(function () {
            $('#popupMonthView').popup('open', { positionTo: "#dvJobScheduling" });
            calendarStyles();
            $('#divMonthView').fullCalendar('refresh');
            window.setTimeout(function () {
                $('#divMonthView').fullCalendar('refresh');
            }, 1000);
        }, 10);
        return false;
    }

    function sortBy(jsonArray, key) {
        if (jsonArray) {
            var curr_date = getCurrentDate();
            //Filter the jobs based on the Calendar preferences.
            var filtered_array = getFilteredJobs(jsonArray, curr_date, key);

            //Sort the job based on Calendar preferences.
            var sortedArray = filtered_array.sort(function (left, right) {
                var a = left[key];
                var b = right[key];
                if (self.orderBy() == "newestToOldest") {
                    a = new Date(new Date(a).getFullYear(), new Date(a).getMonth(), new Date(a).getDate());
                    b = new Date(new Date(b).getFullYear(), new Date(b).getMonth(), new Date(b).getDate());
                    if (a !== b) {
                        if (a < b || a === void 0) return 1;
                        if (a > b || b === void 0) return -1;
                    }
                    //else if (a > currentDate || b > currentDate)
                    //    return 0;
                    return 0;
                }
                else {
                    a = new Date(new Date(a).getFullYear(), new Date(a).getMonth(), new Date(a).getDate());
                    b = new Date(new Date(b).getFullYear(), new Date(b).getMonth(), new Date(b).getDate());
                    if (a !== b) {
                        if (a > b || a === void 0) return 1;
                        if (a < b || b === void 0) return -1;
                    }
                    //else if (a > currentDate || b > currentDate)
                    //    return 0;
                    return 0;
                }
            });
            return sortedArray;
        }
    }

    self.nthMoreClick = function (data, e) {
        self.viewMode("day");
        navType = "next";
        self.currentWeekDate((new Date(data[self.filterTerm()]())));
        getListView();
        $('input[type=radio]').checkboxradio('refresh');
        $("#ulDriveCalendar").listview().listview('refresh');
        $('#dvJobScheduling').collapsibleset('refresh');
    };
    //function addEvent() {
    //    $("#editPopupEvent").popup('close');
    //    var event = [];
    //    var isOld = false;
    //    if ($('#ddlCustomertype').val() != $('#ddlPageCustomerType').val()) {
    //        gJobSchData = gJobSchDataAll[$('#ddlCustomertype').val()];
    //        $('#ddlPageCustomerType').val($('#ddlCustomertype').val()).selectmenu('refresh');
    //    }
    //    $.each(gJobSchData, function (a, b) {
    //        var evt_date = new Date(new Date(b.eventDate).getFullYear(), new Date(b.eventDate).getMonth(), new Date(b.eventDate).getDate());
    //        var evt_txt_date = new Date(new Date($('#txtDate').val()).getFullYear(), new Date($('#txtDate').val()).getMonth(), new Date($('#txtDate').val()).getDate());
    //        if (evt_date.toString() == evt_txt_date.toString()) {
    //            b.events.push({
    //                notes: $("#txtNotes").val().replace(/'/g, "`"),
    //                title: $("#txtTitle").val().replace(/'/g, "`"),
    //                status: $("#chkTask").is(':checked') ? 'Completed' : '',
    //                date: '',
    //                image: bindImage($('#ddlCustomertype').val())
    //            })
    //            isOld = true;
    //            return false;
    //        }
    //    });
    //    if (!isOld) {
    //        event.push({
    //            notes: $("#txtNotes").val().replace(/'/g, "`"),
    //            title: $("#txtTitle").val().replace(/'/g, "`"),
    //            status: $("#chkTask").is(':checked') ? 'Completed' : '',
    //            date: '',
    //            image: bindImage($('#ddlCustomertype').val())
    //        });
    //        gJobSchData.push({
    //            eventDate: $("#txtDate").val(),
    //            events: event
    //        });
    //    }
    //    bindgJobSchData();
    //}

    function bindImage(status) {
        var imagePath;
        switch (status) {
            case "1":
                imagePath = "../images/statusIcons/approved.png";
                break;
            case "4":
                imagePath = "../images/statusIcons/pending.png";
                break;
            case "0":
                imagePath = "../images/statusIcons/rejected.png";
                break;
        }
        return imagePath;
    }

    //function editEvent() {
    //    $('#editPopupEvent').popup('close');
    //    var event_data = $("#hidEventData").val().split('|');
    //    //var gJobSchData_event = gJobSchData[event_data[2]][event_data[4]];
    //    if ($('#ddlPageCustomerType').val() != "All") {
    //        var isNewDate = false;
    //        gJobSchData[event_data[4]].events[event_data[3]].title = $('#txtTitle').val();
    //        gJobSchData[event_data[4]].events[event_data[3]].notes = $('#txtNotes').val();
    //        ($("#chkTask").is(':checked')) ? gJobSchData[event_data[4]].events[event_data[3]].status = 'Completed' : gJobSchData[event_data[4]].events[event_data[3]].status = '';
    //        if ($('#ddlCustomertype').val() == event_data[2]) {
    //            $.each(gJobSchData, function (a, b) {
    //                var evt_date = new Date(new Date(b.eventDate).getFullYear(), new Date(b.eventDate).getMonth(), new Date(b.eventDate).getDate());
    //                var evt_txt_date = new Date(new Date($('#txtDate').val()).getFullYear(), new Date($('#txtDate').val()).getMonth(), new Date($('#txtDate').val()).getDate());
    //                if (evt_date.toString() == evt_txt_date.toString()) {
    //                    isNewDate = false;
    //                    //push the editing event if the dates are same.
    //                    b.events.push(gJobSchData[event_data[4]].events[event_data[3]]);
    //                    gJobSchData[event_data[4]].events.splice(event_data[3], 1);
    //                    if (gJobSchData[event_data[4]].events.length == 0)
    //                        gJobSchData.splice(event_data[4], 1);
    //                    return false;
    //                }
    //                else
    //                    isNewDate = true;
    //            });
    //            if (isNewDate) {
    //                var new_event = [];
    //                new_event.push(gJobSchData[event_data[4]].events[event_data[3]]);
    //                gJobSchData.push({
    //                    'eventDate': $("#txtDate").val(),
    //                    'events': new_event
    //                });
    //                gJobSchData[event_data[4]].events.splice(event_data[3], 1);
    //                if (gJobSchData[event_data[4]].events.length == 0)
    //                    gJobSchData.splice(event_data[4], 1);
    //            }
    //        }
    //        else {
    //            var required_event_data = $.grep(gJobSchDataAll[$('#ddlCustomertype').val()], function (obj) {
    //                if (obj.eventDate === $("#txtDate").val())
    //                    return obj.events;
    //            });
    //            if (required_event_data.length > 0)
    //                required_event_data[0].events.push(gJobSchData[event_data[4]].events[event_data[3]]);
    //            else {
    //                var events = [];
    //                events.push(gJobSchData[event_data[4]].events[event_data[3]]);
    //                gJobSchDataAll[$('#ddlCustomertype').val()].push({
    //                    'eventDate': $("#txtDate").val(),
    //                    'events': events
    //                });
    //            }
    //            gJobSchData[event_data[4]].events.splice(event_data[3], 1);
    //            if (gJobSchData[event_data[4]].events.length == 0)
    //                gJobSchData.splice(event_data[4], 1);

    //        }
    //        gJobSchDataAll[$('#ddlPageCustomerType').val()] = gJobSchData;
    //    }
    //    else {
    //        var isNewDate = false;
    //        gJobSchData = gJobSchDataAll[event_data[2]];
    //        gJobSchData[event_data[4]].events[event_data[3]].title = $('#txtTitle').val();
    //        gJobSchData[event_data[4]].events[event_data[3]].notes = $('#txtNotes').val();
    //        ($("#chkTask").is(':checked')) ? gJobSchData[event_data[4]].events[event_data[3]].status = 'Completed' : gJobSchData[event_data[4]].events[event_data[3]].status = '';
    //        if ($('#ddlCustomertype').val() == event_data[2]) {
    //            $.each(gJobSchData, function (a, b) {
    //                var evt_date = new Date(new Date(b.eventDate).getFullYear(), new Date(b.eventDate).getMonth(), new Date(b.eventDate).getDate());
    //                var evt_txt_date = new Date(new Date($('#txtDate').val()).getFullYear(), new Date($('#txtDate').val()).getMonth(), new Date($('#txtDate').val()).getDate());
    //                if (evt_date.toString() == evt_txt_date.toString()) {
    //                    isNewDate = false;
    //                    //push the editing event if the dates are same.
    //                    b.events.push(gJobSchData[event_data[4]].events[event_data[3]]);
    //                    gJobSchData[event_data[4]].events.splice(event_data[3], 1);
    //                    if (gJobSchData[event_data[4]].events.length == 0)
    //                        gJobSchData.splice(event_data[4], 1);
    //                    return false;
    //                }
    //                else
    //                    isNewDate = true;
    //            });
    //            if (isNewDate) {
    //                var new_event = [];
    //                new_event.push(gJobSchData[event_data[4]].events[event_data[3]]);
    //                gJobSchData.push({
    //                    'eventDate': $("#txtDate").val(),
    //                    'events': new_event
    //                });
    //                gJobSchData[event_data[4]].events.splice(event_data[3], 1);
    //                if (gJobSchData[event_data[4]].events.length == 0)
    //                    gJobSchData.splice(event_data[4], 1);
    //            }
    //        }
    //        else {
    //            var required_event_data = $.grep(gJobSchDataAll[$('#ddlCustomertype').val()], function (obj) {
    //                if (obj.eventDate === $("#txtDate").val())
    //                    return obj.events;
    //            });
    //            if (required_event_data.length > 0)
    //                required_event_data[0].events.push(gJobSchData[event_data[4]].events[event_data[3]]);
    //            else {
    //                var events = [];
    //                events.push(gJobSchData[event_data[4]].events[event_data[3]]);
    //                gJobSchDataAll[$('#ddlCustomertype').val()].push({
    //                    'eventDate': $("#txtDate").val(),
    //                    'events': events
    //                });
    //            }
    //            gJobSchData[event_data[4]].events.splice(event_data[3], 1);
    //            if (gJobSchData[event_data[4]].events.length == 0)
    //                gJobSchData.splice(event_data[4], 1);
    //        }

    //        gJobSchDataAll[event_data[2]] = gJobSchData;
    //    }
    //    bindgJobSchData();
    //}

    //function deleteEvent() {
    //    $('#editPopupEvent').popup('close');
    //    $('#confirmMsg').text("Do you want to delete the Event?");
    //    $('#popupConfirmDialog').popup('open');
    //}

    //function showEventsOnSlider() {
    //    bindgJobSchData();
    //}

    //function fnConfirmRemove() {
    //    var event_data = $("#hidEventData").val().split('|');
    //    if ($('#ddlPageCustomerType').val() != "All") {
    //        $.each(gJobSchData, function (a, b) {
    //            var evt_date = new Date(new Date(b.eventDate).getFullYear(), new Date(b.eventDate).getMonth(), new Date(b.eventDate).getDate());
    //            var evt_txt_date = new Date(new Date($('#txtDate').val()).getFullYear(), new Date($('#txtDate').val()).getMonth(), new Date($('#txtDate').val()).getDate());
    //            if (evt_date.toString() == evt_txt_date.toString()) {
    //                gJobSchData[event_data[4]].events.splice(event_data[3], 1);
    //                if (gJobSchData[event_data[4]].events.length == 0)
    //                    gJobSchData.splice(event_data[4], 1);
    //                return false;
    //            }
    //        });
    //        gJobSchDataAll[$('#ddlPageCustomerType').val()] = gJobSchData;
    //    }
    //    else {
    //        gJobSchData = gJobSchDataAll[event_data[2]];
    //        $.each(gJobSchData, function (a, b) {
    //            var evt_date = new Date(new Date(b.eventDate).getFullYear(), new Date(b.eventDate).getMonth(), new Date(b.eventDate).getDate());
    //            var evt_txt_date = new Date(new Date($('#txtDate').val()).getFullYear(), new Date($('#txtDate').val()).getMonth(), new Date($('#txtDate').val()).getDate());
    //            if (evt_date.toString() == evt_txt_date.toString()) {
    //                gJobSchData[event_data[4]].events.splice(event_data[3], 1);
    //                if (gJobSchData[event_data[4]].events.length == 0)
    //                    gJobSchData.splice(event_data[4], 1);
    //                return false;
    //            }
    //        });
    //        gJobSchDataAll[event_data[2]] = gJobSchData;
    //    }
    //    $('#popupConfirmDialog').popup('close');
    //    bindgJobSchData();
    //}

    self.bindgJobSchData = function () {
        //if ($('input[id=popMonth].ui-btn-up-b').length > 0) {
        //    bindFullMonthCalendar();
        //    //        if ($("#txtDate").val() != "") {
        //    //            redirectCalendar($("#txtDate").val());
        //    //            $('#ddlPageCustomerType').val($('#ddlCustomertype').val()).selectmenu('refresh');
        //    //        }
        //}
        //else if ($('input[id=popList].ui-btn-up-b').length > 0) {
        //    if ($('#ddlPageCustomerType').val() != "All") {
        //        gJobSchData = sortBy(gJobSchData, "eventDate");
        //    }
        //    $('#ulDriveCalendar').empty();
        //    ko.cleanNode($('#ulDriveCalendar')[0]);
        //    var model = createViewModel();
        //    ko.applyBindings(model, $('#ulDriveCalendar')[0]);
        //    $("#ulDriveCalendar").each(function (i) {
        //        $(this).listview();
        //    });
        //}
    }

    self.getQueryVariable = function (variable) {
        var query = window.location.search.substring(1);
        var vars = query.split("&");
        for (var i = 0; i <= vars.length; i++) {
            var pair = vars[i].split("=");
            if (pair[0] == variable) {
                return pair[1];
            }
        }
    }

    function getListView() {
        $('#ulDriveCalendar').empty();
        //ko.cleanNode($('#ulDriveCalendar')[0]);
        var model = createViewModel();
        self.driveCalendarInfo(model);
        // ko.applyBindings(model, $('#ulDriveCalendar')[0]);
        //$("#ulDriveCalendar").each(function (i) {
        //    $(this).listview();
        //});
        //$('#popMonth').attr("data-theme", "c").removeClass("ui-btn-up-b").addClass("ui-btn-up-c");
        $('#popMonth').attr("data-theme", "c").removeClass("ui-btn-up-b").removeClass("ui-btn-hover-b").addClass("ui-btn-up-c").addClass("ui-btn-hover-c");
        $('#popList').attr("data-theme", "b").removeClass("ui-btn-up-c").removeClass("ui-btn-hover-c").addClass("ui-btn-up-b").addClass("ui-btn-hover-b");
        //$('#popList').buttonMarkup({ theme: "b" });
        //$('#divMonthView').hide();
        $('#divListView').show();
        $('#divListOnOff').show();
    }

    //function bindFullMonthCalendar() {
    //    $('#divMonthView').show();
    //    $('#divMonthView').empty();
    //    $('#divListOnOff').hide();
    //    $('#divListView').hide();
    //    $('#popMonth').attr("data-theme", "b").removeClass("ui-btn-up-c").removeClass("ui-btn-hover-c").addClass("ui-btn-up-b").addClass("ui-btn-hover-b");
    //    $('#popList').attr("data-theme", "c").removeClass("ui-btn-up-b").removeClass("ui-btn-hover-b").addClass("ui-btn-up-c").addClass("ui-btn-hover-c");
    //    $('#divMonthView').fullCalendar({
    //        events: makeEventJson(),
    //        editable: true,
    //        eventRender: function (event, element) {
    //            element.attr('onclick', 'openEventPopup("' + event.title + '","' + event.start + '","' + event.notes + '","' + event.type + '","' + event.status + '");');
    //            element.attr('title', event.title);
    //            switch (event.type) {
    //                case "Suddenlink":
    //                    element.find("div.fc-event-inner").prepend("<img src='customerLogos/suddenlink_cal_icon.png' class= 'ui-li-icon' title ='Suddenlink' >");
    //                    break;
    //                case "Conversion Alliance":
    //                    element.find("div.fc-event-inner").prepend("<img src='customerLogos/tribune_cal_icon.png' class= 'ui-li-icon' title ='Conversion Alliance' >");
    //                    break;
    //                case "Pluris":
    //                    element.find("div.fc-event-inner").prepend("<img src='customerLogos/pluris_cal_icon.png' class= 'ui-li-icon' title ='Pluris'>");
    //                    break;
    //            }
    //        }
    //    });
    //}
    //function redirectCalendar(event_date) {
    //    $('#divMonthView').fullCalendar('gotoDate', new Date(event_date).getFullYear(), new Date(event_date).getMonth());
    //}


    //function getView(ctrl) {

    //    if (ctrl.value == "month") {
    //        bindFullMonthCalendar();
    //        $(ctrl).attr('checked', true).checkboxradio('refresh');
    //    }
    //    else if (ctrl.value == "list") {
    //        $(ctrl).attr('checked', true).checkboxradio('refresh');
    //        getListView();
    //    }
    //}

    //function getEvent(event_title, event_st_date) {
    //    var selected_event;
    //    var parent_index = 0;
    //    var event_index = 0;
    //    var start_date = new Date(new Date(event_st_date).getFullYear(), new Date(event_st_date).getMonth(), new Date(event_st_date).getDate()).toString("MM/dd/yyyy").toLowerCase();
    //    if ($('#ddlPageCustomerType').val() != "All") {
    //        $.each(gJobSchData, function (index, obj) {
    //            var event_date = new Date(new Date(obj.eventDate).getFullYear(), new Date(obj.eventDate).getMonth(), new Date(obj.eventDate).getDate()).toString("MM/dd/yyyy").toLowerCase();
    //            if (event_date === start_date) {
    //                $.each(obj.events, function (childIndex, eventObj) {
    //                    if (eventObj.title === event_title) {
    //                        parent_index = index;
    //                        event_index = childIndex;
    //                        selected_event = eventObj;
    //                        return false;
    //                    }
    //                });
    //                if (selected_event != undefined && selected_event != null && JSON.stringify(selected_event).length > 0)
    //                    return false;
    //            }
    //        });
    //    }
    //    else {
    //        $.each(gJobSchDataAll, function (p, q) {
    //            $.each(q, function (index, obj) {
    //                var event_date = new Date(new Date(obj.eventDate).getFullYear(), new Date(obj.eventDate).getMonth(), new Date(obj.eventDate).getDate()).toString("MM/dd/yyyy").toLowerCase();
    //                if (event_date === start_date) {
    //                    $.each(obj.events, function (childIndex, eventObj) {
    //                        if (eventObj.title === event_title) {
    //                            parent_index = index;
    //                            event_index = childIndex;
    //                            selected_event = eventObj;
    //                            return false;
    //                        }
    //                    });
    //                    if (selected_event != undefined && selected_event != null && JSON.stringify(selected_event).length > 0)
    //                        return false;
    //                }
    //            });
    //        });
    //    }
    //    return parent_index + '|' + event_index;
    //}


    //function openEventPopup(title, date, notes, type, status) {
    //    fnResetPopup();
    //    $('#tdAddBtn').css('visibility', 'hidden');
    //    $('#tdSubmitBtn').css('visibility', 'visible');
    //    $('#tdDeleteBtn').css('visibility', 'visible');
    //    //$('#dvStatus').css('visibility', 'visible');
    //    $('#editHeader').html('Edit Event');
    //    var day = new Date(date).getDate();
    //    var month = new Date(date).getMonth();
    //    var year = new Date(date).getFullYear();
    //    var indexes = getEvent(title, date);
    //    indexes = indexes.split('|');
    //    $("#hidEventData").val('' + '|' + date + '|' + type + '|' + indexes[1] + '|' + indexes[0]);
    //    $("#editPopupEvent").popup("open");
    //    $("#txtDate").val(month + 1 + '/' + day + '/' + year);
    //    $("#txtTitle").val(title);
    //    $("#txtNotes").val(notes);
    //    $('#ddlCustomertype').val(type).selectmenu('refresh');
    //    if (status.toLowerCase() == 'completed')
    //        $('#chkTask').attr('checked', true).checkboxradio('refresh');
    //    else
    //        $('#chkTask').removeAttr('checked', false).checkboxradio('refresh');
    //}

    //function makeEventJson() {
    //    var eventData = [];
    //    if ($('#ddlPageCustomerType').val() != "All") {
    //        $.each(gJobSchData, function (a, b) {
    //            $.each(b.events, function (key, evtData) {
    //                eventData.push({
    //                    title: evtData.title,
    //                    start: new Date(b.eventDate),
    //                    backgroundColor: setEventColors(b.eventDate, evtData.status, 'bc'),
    //                    borderColor: setEventColors(b.eventDate, evtData.status, 'bc'),
    //                    textColor: setEventColors(b.eventDate, evtData.status, 'tc'),
    //                    notes: evtData.notes,
    //                    status: evtData.status,
    //                    type: $('#ddlPageCustomerType').val()
    //                });
    //            });

    //        });
    //    }
    //    else {
    //        $.each(gJobSchDataAll, function (p, q) {
    //            $.each(q, function (a, b) {
    //                $.each(b.events, function (key, evtData) {
    //                    eventData.push({
    //                        title: evtData.title,
    //                        start: new Date(b.eventDate),
    //                        backgroundColor: setEventColors(b.eventDate, evtData.status, 'bc'),
    //                        borderColor: setEventColors(b.eventDate, evtData.status, 'bc'),
    //                        textColor: setEventColors(b.eventDate, evtData.status, 'tc'),
    //                        notes: evtData.notes,
    //                        status: evtData.status,
    //                        type: p
    //                    });
    //                });

    //            });
    //        });
    //    }
    //    return eventData;
    //}

    //function setEventColors(evt_date, status, color_type) {
    //    var curr_date = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
    //    var evt_date = new Date(new Date(evt_date).getFullYear(), new Date(evt_date).getMonth(), new Date(evt_date).getDate());
    //    //yellow -1 day
    //    if (((Math.abs(evt_date - curr_date) / (1000 * 60 * 60 * 24)) == 1) && evt_date > curr_date) {
    //        return (color_type == 'bc') ? '#ffe56d' : '#000000';
    //    }
    //        //red for past and not completed
    //    else if (((Math.abs(curr_date - evt_date) / (1000 * 60 * 60 * 24)) >= 1) && curr_date > evt_date && status == '') {
    //        return (color_type == 'bc') ? '#a80101' : '';
    //    }
    //        //green for completed
    //    else if (status.toLowerCase() == 'completed') {
    //        return (color_type == 'bc') ? '#b6e791' : '#000000';
    //    }
    //        //none grey out (default)
    //    else {
    //        return (color_type == 'bc') ? '#f8f8f8' : '#000000';
    //    }
    //}
}