﻿var dealershipsList = [];
var selectedDealer = {};
var title_part = "";
$('#_myChart').live('pagebeforecreate', function () {
    var image_path = "../images/gizmo_load_anim.gif";
    //var over = '<div id="waitPopUp" data-role="popup" class="ui-content"  style="width:900;height:500;"  data-rel="dialog" data-dismissible="false" data-history="false"><center><span style="width:500px;height:200px;"><img id="loading1" alt="Loading...." src="' + image_path + '" style="width: 100px;height: 100px;" /><p><span id="loadingText1" style="font-weight:bold;color:#4a80af;font-family:Tahoma,verdana, Arial;font-size:large;">Loading Report....</span></p></span></center></div>';
    //var over = '<div id="waitPopUpCharts" data-role="popup" class="ui-content"  style="width:900;height:500;"  data-rel="dialog" data-dismissible="false" data-history="false"><center><span style="width:500px;height:200px;"><img id="loading1" alt="Loading...." src="' + image_path + '" /><p><span id="loadingText1" style="font-weight:bold;color:#4a80af;font-family:Tahoma,verdana, Arial;font-size:large;">Loading Co-op Info....</span></p></span></center></div>';
    //$('#_myChart').append(over);
    title_part = (sessionStorage.customerNumber == 279) ? 'KDM' : 'Gizmo';
    loadCoopInfo();
    window.setTimeout(function () {
        createCoopInfoPopup();
        $('#tblCoopInfo').table();
        $('#ddlDealerships').selectmenu();
        $('#ddlDealerships').val(selectedDealer.dealerShipName).selectmenu('refresh');
    }, 1000);
});
$(document).on('pageshow', '#_myChart', function () {
    $('#tblCoopInfo').table('refresh');
    //$('#waitPopUpCharts').popup('open');
});
function loadCoopInfo() {
    if (sessionStorage.coopInfo == undefined || sessionStorage.coopInfo == null || sessionStorage.coopInfo == "") {
        var coop_info_path = "../JSON/_coopInfo.JSON";
        var timer_id;
        $.getJSON(coop_info_path, function (data) {
            sessionStorage.coopInfo = JSON.stringify(data[sessionStorage.customerNumber]);
            data = data[sessionStorage.customerNumber];
            $.each(data.dealerShipsOwned, function (key, val) {
                dealershipsList.push({
                    "dealershipName": val.dealerShipName,
                    "dealershipId": val.dealerShipName
                });
                if (key == 0) {
                    selectedDealer = val;
                }
            });
        });
    }
    else {
        var coop_info = $.parseJSON(sessionStorage.coopInfo);
        if (coop_info != undefined && coop_info != null && coop_info != "") {
            dealershipsList = [];
            $.each(coop_info.dealerShipsOwned, function (key, val) {
                dealershipsList.push({
                    "dealershipName": val.dealerShipName,
                    "dealershipId": val.dealerShipName
                });
                if (key == 0) {
                    selectedDealer = val;
                }
            });
        }
    }
}

function getSelectedDealer() {
    //selectedDealer
    var coop_info = $.parseJSON(sessionStorage.coopInfo);

    if (coop_info != undefined && coop_info != null && coop_info != "") {
        var selected_dealer = $.grep(coop_info.dealerShipsOwned, function (obj) {
            return obj.dealerShipName === $('#ddlDealerships').val();
        });
        if (selected_dealer.length > 0) {
            selectedDealer = selected_dealer[0];
        }
    }
    $('#dvCharts').empty();
    $('#dvBudgetTable').empty();
    createCoopInfoPopup();
    $('#tblCoopInfo').table();
    $('#ddlDealerships').selectmenu();
    $('#ddlDealerships').val(selectedDealer.dealerShipName).selectmenu('refresh')
}

function createCoopInfoPopup() {
    var coop_info = $.parseJSON(sessionStorage.coopInfo);

    if (selectedDealer != undefined && selectedDealer != null && selectedDealer != "" && Object.keys(selectedDealer).length > 0) {

        var dealerships_list = "";
        $.each(dealershipsList, function (key, val) {
            //if (key == 0)
            //    dealerships_list = "<option selected value'" + val.dealershipId + "'>" + val.dealershipName + "</option>";
            //else
                dealerships_list += "<option value'" + val.dealershipId + "'>" + val.dealershipName + "</option>";
        });

        dealerships_list = "<div css='float:right'><select id='ddlDealerships' data-inset='true' data-mini='true' data-theme='a' onchange='getSelectedDealer();'>" + dealerships_list + "</select></div>"

        $('#dvHeading').empty();
        $('#dvHeading').append('<div style="margin-bottom:10px;font-weight:bold;font-size:20px"><span>' + coop_info.dealerName + '</span></div>');
        $('#dvHeading').append('<div class="ui-grid-a" style="font-size:14px"><div class="ui-block-a" style="font-weight:bold;width:18%">Dealer:&nbsp;</div><div class="ui-block-b">' + selectedDealer.dealerShipName + '</div></div>');
        var charts_ui1 = "", charts_ui2 = "";
        charts_ui1 += '<div id="donutchart1" style="height:100%;"></div>';
        $('#dvCharts1').empty();
        $('#dvCharts1').append(charts_ui1);
        var popup_ui_table1 = "", popup_ui_table2 = "";
        popup_ui_table1 = '<div class="ui-grid-a" style="font-size:12px"><div class="ui-block-a" style="font-weight:bold;text-align:right;width:25%">Co-op Bal:&nbsp;</div><div class="ui-block-b">$' + selectedDealer.coopBalance + '.00</div></div>';

        $('#dvBudgetTable1').empty();
        $('#dvBudgetTable1').append(popup_ui_table1);

        charts_ui2 = '<br /><div id="donutchart2" style="height:100%;"></div>';
        $('#dvCharts2').empty();
        $('#dvCharts2').append(charts_ui2);
        popup_ui_table2 = '<div class="ui-grid-a" style="font-size:12px;"><div class="ui-block-a" style="font-weight:bold;width:45%">' + title_part + ' Budget Total:&nbsp;</div><div class="ui-block-b" style="text-align:right;width:15%">$' + selectedDealer.kdmBudgetTotal + '.00</div>';
        popup_ui_table2 += '<div class="ui-block-a" style="font-weight:bold;width:45%">' + title_part + ' Budget Spent:&nbsp;</div><div class="ui-block-b" style="text-align:right;width:15%">$' + selectedDealer.kdmBudgetSpent + '.00</div>';
        popup_ui_table2 += '<div class="ui-block-a" style="font-weight:bold;width:45%">' + title_part + ' Total Balance:&nbsp;</div><div class="ui-block-b" style="text-align:right;width:15%">$' + selectedDealer.kdmTotalBalance + '.00</div></div>';
        $('#dvBudgetTable2').empty();
        $('#dvBudgetTable2').append(popup_ui_table2);
        $('#dvFooterNote').empty();
        $('#dvFooterNote').append('<div align=left><p>Last updated ' + coop_info.lastUpdated + '</p></div>');
        drawChart();
        $('#dvWaitAnimation').hide();
        //$('#waitPopUpCharts').popup('close');
    }
}

function drawChart() {
    var formatter = new google.visualization.NumberFormat({ prefix: '$' });

    var coop_info = $.parseJSON(sessionStorage.coopInfo);
    //coop_info = coop_info.dealerShipsOwned[0];
    //if (coop_info != undefined && coop_info != null && coop_info != "") {
    if (selectedDealer != undefined && selectedDealer != null && selectedDealer != "" && Object.keys(selectedDealer).length > 0) {
        coop_info = selectedDealer;
        var data1 = google.visualization.arrayToDataTable([
          ['Task', ((title_part == 'KDM') ? 'Kubota Funds' : 'Gizmo Funds')],
          [title_part + ' Budget', coop_info.coopFunds.kdmBudget],
          ['Other Spend', coop_info.coopFunds.otherSpend],
          ['Available Co-op Funds', coop_info.coopFunds.availableCoopFunds]
        ]);
        formatter.format(data1, 1);
        var options1 = {
            title: ((title_part == 'KDM') ? 'Kubota Co-op' : 'Gizmo Co-op'),
            pieHole: 0.4,
            chartArea: { left: 12, top: 38, right: 18, bottom: 12, width: "100%", height: "100%" },
            height: '30%',
            width: '30%',
            titleTextStyle: {
                //color: <string>,    // any HTML string color ('red', '#cc00cc')
                //fontName: <string>, // i.e. 'Times New Roman'
                fontSize: 14, // 12, 18 whatever you want (don't specify px)
                bold: true,    // true or false
                //italic: <boolean>   // true of false
            }
        };

        var chart1 = new google.visualization.PieChart(document.getElementById('donutchart1'));
        chart1.draw(data1, options1);

        var data2 = google.visualization.arrayToDataTable([
          ['Task', ((title_part == 'KDM') ? 'Kubota Funds' : 'Gizmo Funds')],
          [title_part + ' Spent', coop_info.kdmBudget.kdmSpent],
          [title_part + ' Available Credit', coop_info.kdmBudget.kdmAvailableCredit],
          [title_part + ' Available Co-op', coop_info.kdmBudget.kdmAvailableCoop]
        ]);
        formatter.format(data2, 1);
        var options2 = {
            title: title_part + ' Budget',
            pieHole: 0.4,
            chartArea: { left: 12, top: 40, right: 14, bottom: 12, width: "100%", height: "100%" },
            height: '30%',
            width: '30%',
            titleTextStyle: {
                //color: <string>,    // any HTML string color ('red', '#cc00cc')
                //fontName: <string>, // i.e. 'Times New Roman'
                fontSize: 14, // 12, 18 whatever you want (don't specify px)
                bold: true,    // true or false
                //italic: <boolean>   // true of false
            }
        };
        var chart2 = new google.visualization.PieChart(document.getElementById('donutchart2'));
        chart2.draw(data2, options2);
        $('#dvWaitAnimation').hide();
        //$('#waitPopUpCharts').popup('close');
    }
}