﻿jQuery.support.cors = true;

//******************** Global Variables Start **************************
var gServiceUrl = '../JSON/_jobSetupLocation.JSON';
var gStateServiceUrl = '../JSON/_listStates.JSON';
//var gOutputServiceUrl = '../JSON/_userProfile.JSON';
var gOutputServiceUrl = serviceURLDomain + 'api/UserProfile/' + sessionStorage.username + '/1/' + appPrivileges.customerNumber + '/specific';

var gData;
var gStateData;
var gOutputData;

var urlString = unescape(window.location)
var queryString = urlString.substr(urlString.indexOf("?") + 1, urlString.length)
var params = queryString.split("&");
var getUserName = params[0].split("=")[1]; //we need to pass this username to get actual user profile from webservice
var isNewUser = true;

if (getUserName == "self")
    getUserName = sessionStorage.username;

if (getUserName != "" && getUserName != undefined) {
    isNewUser = false;
}

//******************** Global Variables End **************************

//******************** Constants Start **************************
var PATTERN1 = /^[ABC]/;
var PATTERN2 = /^[DEFGHIJ]/;
var PATTERN3 = /^[KLM]/;
var PATTERN4 = /^[NOPQRS]/;
var PATTERN5 = /^[TUVWXYZ]/;
//******************** Constants End **************************

//******************** Page Load Events Start **************************
$(document).on('pageshow', '#_userProfile', function (event) {
    !(appPrivileges.display_userProfileLocations != undefined && appPrivileges.display_userProfileLocations) ? $("#fsLocations").hide() : "";  //getting info from authorization web service
    makeGData();
    makeGOutputData();
});

$('#_userProfile').live('pagebeforecreate', function (event) {
    displayMessage('_userProfile');
    //test for mobility...
    loadMobility();
});
//******************** Page Load Events End **************************


//******************** Public Functions Start **************************
function makeGData() {
    getCORS(serviceURLDomain + "api/JobStore/" + appPrivileges.customerNumber, null, function (data) {
        gData = data.items;
        buildLists();
    }, function (error_response) {
        showErrorResponseText(error_response);
    });

    //states dropdown binding
    $.ajax({
        url: gStateServiceUrl,
        dataType: 'json',
        timeout: 10000,
        type: 'GET',
        async: false,
        success: function (stateData) {
            gStateData = stateData;
            buildStates();
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            $('#alertmsg').text("Error status :" + textStatus + ". Error type: " + errorThrown + ". Error Message: " + XMLHttpRequest.responseXML);
            $('#popupDialog').popup('open');
        }
    });
}

function buildLists() {
    var pager = ""
    var facility_counter = 0;
    var column_counter = 0;
    var temp_location_data = "";
    var locations = "";

    var li_atoc = "";
    var li_dtoj = "";
    var li_ktom = "";
    var li_ntos = "";
    var li_ttoz = "";
    var index = 0;

    if (sessionStorage.selectedLocationsUserProfile != undefined && sessionStorage.selectedLocationsUserProfile != "")
        removeSelectedLocations();

    li_atoc = '<div data-role="collapsible" data-collapsed-icon="arrow-r" data-expanded-icon="arrow-d"><h2>A - C</h2><ul data-role="listview" data-divider-theme="d" id="aToc" data-divider-theme="d" data-split-icon="minus" data-split-theme="d">';
    li_dtoj = '<div data-role="collapsible" data-collapsed-icon="arrow-r" data-expanded-icon="arrow-d"><h2>D - J</h2><ul data-role="listview" data-divider-theme="d" id="dToj" id="dToj" data-divider-theme="d" data-split-icon="minus" data-split-theme="d">';
    li_ktom = '<div data-role="collapsible" data-collapsed-icon="arrow-r" data-expanded-icon="arrow-d"><h2>K - M</h2><ul data-role="listview" data-divider-theme="d" id="kTom" id="kTom" data-divider-theme="d" data-split-icon="minus" data-split-theme="d">';
    li_ntos = '<div data-role="collapsible" data-collapsed-icon="arrow-r" data-expanded-icon="arrow-d"><h2>N - S</h2><ul data-role="listview" data-divider-theme="d" id="nTos" id="nTos" data-divider-theme="d" data-split-icon="minus" data-split-theme="d">';
    li_ttoz = '<div data-role="collapsible" data-collapsed-icon="arrow-r" data-expanded-icon="arrow-d"><h2>T - Z</h2><ul data-role="listview" data-divider-theme="d" id="tToz" id="tToz" data-divider-theme="d" data-split-icon="minus" data-split-theme="d">';
    $('#dvLocations').empty();
    $.each(gData, function (location_key, location_val) {

        var id = "liLocation" + location_val.locationId;

        var city_name = (location_val.city != null && location_val.city != "" && location_val.city.indexOf(location_val.city) != -1) ? location_val.city.replace(" ", "#") : location_val.city;
        var store_name = (location_val.storeName != undefined && location_val.storeName != "") ? location_val.storeName : '';
        var param = index + '|' + store_name + '|' + city_name + '|' + location_val.state + '|' + location_val.storeId.toString() + '|' + index;

        var li = '<li data-icon="false" data-icon="plus" id="li' + location_val.storeId.toString() + '_' + index + '" +  ><a href="#" onclick="addSelected(\'' + param + '\');">' + location_val.storeId.toString() + ' - ' + location_val.city + ', ' + location_val.state + '</a></li>';
        if (location_val.storeId.toString().match(PATTERN1) != null && location_key.toString().match(PATTERN1) != "")
            li_atoc += li;
        else if (location_val.storeId.toString().match(PATTERN2) != null && location_val.storeId.toString().match(PATTERN2) != "")
            li_dtoj += li;
        else if (location_val.storeId.toString().match(PATTERN3) != null && location_val.storeId.toString().match(PATTERN3) != "")
            li_ktom += li;
        else if (location_val.storeId.toString().match(PATTERN4) != null && location_val.storeId.toString().match(PATTERN4) != "")
            li_ntos += li;
        else if (location_val.storeId.toString().match(PATTERN5) != null && location_val.storeId.toString().match(PATTERN5) != "")
            li_ttoz += li;
        index++;
    });
    li_atoc += '</ul></div>';
    li_dtoj += '</ul></div>';
    li_ktom += '</ul></div>';
    li_ntos += '</ul></div>';
    li_ttoz += '</ul></div>';
    var locations_header = '<h3 style="padding-bottom:13px; padding-top:7px;">Locations</h3>';
    $('#dvLocations').append(locations_header + li_atoc + li_dtoj + li_ktom + li_ntos + li_ttoz);
    $('#dvLocations').collapsibleset('refresh');

    $("#dvLocations div ul").each(function (i) {
        $(this).listview();
    });
    buildSelectedLists();
}

function removeSelectedLocations() {
    var selected_locations = jQuery.parseJSON(sessionStorage.selectedLocationsUserProfile);
    for (i = 0; i < selected_locations.length; i++) {
        gData = jQuery.grep(gData, function (a) {
            return a.storeId != selected_locations[i].storeId;
        });
    }
}

function buildStates() {
    $('#selStates').empty();
    $.each(gStateData, function (i, option) {
        if (option.selected == "selected")
            $('#selStates').append('<option value="' + option.stateValue + '" selected>' + option.stateName + '</option>');
        else
            $('#selStates').append('<option value="' + option.stateValue + '">' + option.stateName + '</option>');
    });
    $('#selStates').selectmenu("refresh", true);
}

function makeGOutputData() {
    if (sessionStorage.userProfile == undefined && sessionStorage.userProfile == undefined)
        getCORS(gOutputServiceUrl, null, function (dataOutput) {
            gOutputData = dataOutput;
            buildOutputLists();
        }, function (error_response) {
            showErrorResponseText(error_response);
        });

        //$.getJSON(gOutputServiceUrl, function (dataOutput) {
        //    gOutputData = dataOutput;
        //    buildOutputLists();
        //});
    else {
        gOutputData = jQuery.parseJSON(sessionStorage.userProfile);
        buildOutputLists();
    }
}

function buildOutputLists() {
    $.each(gOutputData, function (key, val) {
        switch (key) {
            case "Username":
                $('#txtUserName').val(val);
                break;
            //case "password":
            //    $('#txtPassword').val(val);
            //    break;
            //case "confirmPassword":
            //    $('#txtConfirmPassword').val(val);
            //    break;
            case "Role":
                $('input[name=rdoUserRole][value="' + val + '"]').attr('checked', true).checkboxradio("refresh");
                break;
            case "Company":
                fillControlsWithData(val, 'selCompany', 'ddl');
                $('#selCompany').selectmenu("refresh", true);
                break;
            case "firstName":
                $('#txtFirstName').val(val);
                break;
            case "LastName":
                $('#txtLastName').val(val);
                break;
            case "Email":
                $('#txtEmail').val(val);
                break;
            case "Phone":
                $('#txtPhone').val(val);
                break;
            case "Address":
                $('#txtAddress1').val(val);
                break;
            case "Address2":
                $('#txtAddress2').val(val);
                break;
            case "City":
                $('#txtCity').val(val);
                break;
            case "State":
                fillControlsWithData(val, 'selStates', 'ddl');
                $('#selStates').selectmenu("refresh", true);
                break;
            case "Zip":
                $('#txtZip').val(val);
                break;
        }
    });
    sessionStorage.userProfile = JSON.stringify(gOutputData);
}

function fillControlsWithData(item, ctrl, type) {
    if (type == 'ddl') {
        $.each($("#" + ctrl + " option"), function (i, option) {
            if (item == $("#" + ctrl + ' option')[i].value) {
                $('#' + ctrl).find('select').eq(0).closest('.ui-select').find('.ui-btn-text').find('span').html($("#" + ctrl + ' option')[i].text);
                $("#" + ctrl + " option").eq(i).attr('selected', true);
            }
        });
    }

}

function bindUserProfileData() {
    if (!fnValidate())
        return false;

    $.each(gOutputData, function (key, val) {
        switch (key) {
            case "userName":
                gOutputData[key] = $('#txtUserName').val();
                break;
            case "password":
                gOutputData[key] = $('#txtPassword').val();
                break;
            case "confirmPassword":
                gOutputData[key] = $('#txtConfirmPassword').val();
                break;
            case "userRole":
                gOutputData[key] = $('input[name=rdoUserRole]:radio:checked')[0] ? $('input[name=rdoUserRole]:radio:checked')[0].defaultValue : "";
                break;
            case "company":
                gOutputData[key] = $("#selCompany option:selected").val();
                break;
            case "firstName":
                gOutputData[key] = $('#txtFirstName').val();
                break;
            case "lastName":
                gOutputData[key] = $('#txtLastName').val();
                break;
            case "email":
                gOutputData[key] = $('#txtEmail').val();
                break;
            case "phone":
                gOutputData[key] = $('#txtPhone').val();
                break;
            case "address1":
                gOutputData[key] = $('#txtAddress1').val();
                break;
            case "address2":
                gOutputData[key] = $('#txtAddress2').val();
                break;
            case "city":
                gOutputData[key] = $('#txtCity').val();
                break;
            case "state":
                gOutputData[key] = $("#selStates option:selected").val();
                break;
            case "zip":
                gOutputData[key] = $('#txtZip').val();
                break;
        }
    });
    sessionStorage.userProfile = JSON.stringify(gOutputData);
}

function addSelected(value) {

    var param_str = value.split("|");
    var selected_locations = [];
    if (sessionStorage.selectedLocationsUserProfile != undefined && sessionStorage.selectedLocationsUserProfile != "")
        selected_locations = jQuery.parseJSON(sessionStorage.selectedLocationsUserProfile);

    var location_info = {};
    location_info["storeName"] = param_str[1];
    location_info["city"] = param_str[2].replace("#", " ");
    location_info["state"] = param_str[3];
    location_info["storeId"] = param_str[4];

    if (selected_locations.length == 0)
        selected_locations.push(location_info);
    else
        selected_locations[selected_locations.length] = location_info;

    gData.splice(param_str[0], 1);
    var p_div = $('#li' + param_str[4] + '_' + param_str[5])[0].parentElement;
    sessionStorage.selectedLocationsUserProfile = JSON.stringify(selected_locations);

    buildLists();
    $($('div.ui-collapsible-content').find('ul[id="' + p_div.id + '"]')[0].parentElement.parentElement).trigger("expand");
    buildSelectedLists();
}

function removeSelected(value) {
    var param_str = value.split("|");
    var selected_locations = [];

    selected_locations = jQuery.parseJSON(sessionStorage.selectedLocationsUserProfile);
    var location_info = {};
    location_info["storeName"] = param_str[1];
    location_info["city"] = param_str[2].replace("#", " ");
    location_info["state"] = param_str[3];
    location_info["storeId"] = param_str[4];
    location_info["originalIndex"] = param_str[5];

    gData.splice(param_str[5], 0, location_info);
    gData = gData.sort(function (a, b) {
        return a.locationId.toLowerCase() > b.locationId.toLowerCase() ? 1 : -1;
    });
    selected_locations.splice(param_str[0], 1);
    sessionStorage.selectedLocationsUserProfile = JSON.stringify(selected_locations);

    buildLists();
    buildSelectedLists();
}

function buildSelectedLists() {

    var pager = ""
    var facility_counter = 0;
    var column_counter = 0;
    var temp_location_data = "";
    var locations = "";

    var li_atoc = "";
    var li_dtoj = "";
    var li_ktom = "";
    var li_ntos = "";
    var li_ttoz = "";
    var index = 0;

    li_atoc = '<div data-role="collapsible" data-collapsed-icon="arrow-r" data-expanded-icon="arrow-d"><h2>A - C</h2><ul data-role="listview" data-divider-theme="d" id="aToc" data-divider-theme="d" data-split-icon="minus" data-split-theme="d">';
    li_dtoj = '<div data-role="collapsible" data-collapsed-icon="arrow-r" data-expanded-icon="arrow-d"><h2>D - J</h2><ul data-role="listview" data-divider-theme="d" id="dToj" id="dToj" data-divider-theme="d" data-split-icon="minus" data-split-theme="d">';
    li_ktom = '<div data-role="collapsible" data-collapsed-icon="arrow-r" data-expanded-icon="arrow-d"><h2>K - M</h2><ul data-role="listview" data-divider-theme="d" id="kTom" id="kTom" data-divider-theme="d" data-split-icon="minus" data-split-theme="d">';
    li_ntos = '<div data-role="collapsible" data-collapsed-icon="arrow-r" data-expanded-icon="arrow-d"><h2>N - S</h2><ul data-role="listview" data-divider-theme="d" id="nTos" id="nTos" data-divider-theme="d" data-split-icon="minus" data-split-theme="d">';
    li_ttoz = '<div data-role="collapsible" data-collapsed-icon="arrow-r" data-expanded-icon="arrow-d"><h2>T - Z</h2><ul data-role="listview" data-divider-theme="d" id="tToz" id="tToz" data-divider-theme="d" data-split-icon="minus" data-split-theme="d">';
    $('#dvselectedLocations').empty();
    if (sessionStorage.selectedLocationsUserProfile != "" && sessionStorage.selectedLocationsUserProfile != null) {
        $.each(jQuery.parseJSON(sessionStorage.selectedLocationsUserProfile), function (location_key, location_val) {

            var id = "liLocation" + location_val.locationId;

            var city_name = (location_val.city != null && location_val.city != "" && location_val.city.indexOf(location_val.city) != -1) ? location_val.city.replace(" ", "#") : location_val.city;
            var store_name = (location_val.storeName != undefined && location_val.storeName != "") ? location_val.storeName : '';
            var param = index + '|' + store_name + '|' + city_name + '|' + location_val.state + '|' + location_val.storeId.toString() + '|' + index;

            var li = '<li data-icon="false" data-icon="plus" id="li' + location_val.storeId.toString() + '_' + index + '" +  ><a href="#" onclick="addSelected(\'' + param + '\');">' + location_val.storeId.toString() + ' - ' + location_val.city + ', ' + location_val.state + '</a></li>';
            if (location_val.storeId.toString().match(PATTERN1) != null && location_key.toString().match(PATTERN1) != "")
                li_atoc += li;
            else if (location_val.storeId.toString().match(PATTERN2) != null && location_val.storeId.toString().match(PATTERN2) != "")
                li_dtoj += li;
            else if (location_val.storeId.toString().match(PATTERN3) != null && location_val.storeId.toString().match(PATTERN3) != "")
                li_ktom += li;
            else if (location_val.storeId.toString().match(PATTERN4) != null && location_val.storeId.toString().match(PATTERN4) != "")
                li_ntos += li;
            else if (location_val.storeId.toString().match(PATTERN5) != null && location_val.storeId.toString().match(PATTERN5) != "")
                li_ttoz += li;
            index++;
        });
    }
    li_atoc += '</ul></div>';
    li_dtoj += '</ul></div>';
    li_ktom += '</ul></div>';
    li_ntos += '</ul></div>';
    li_ttoz += '</ul></div>';
    var locations_header = '<h3 style="padding-bottom:13px; padding-top:7px;">Assigned Locations</h3>';
    $('#dvselectedLocations').append(locations_header + li_atoc + li_dtoj + li_ktom + li_ntos + li_ttoz);
    $('#dvselectedLocations').collapsibleset('refresh');

    $("#dvselectedLocations div ul").each(function (i) {
        $(this).listview();
    });
}

function displayAssignedLocations(locations_list) {
    var no_locations_item = "";
    if (locations_list.length == 0) {
        no_locations_item += '<li data-icon="info"><a>No Locations Added</a></li>';
        $("#selectedLocation").append(no_locations_item);
        $("#headerLocation").html("Assigned Locations"); //No Locations Added
    }
    else {
        $("#headerLocation").html("Assigned Locations");
        $("#selectedLocation").append(locations_list);
    }
    $("#selectedLocation").listview('refresh');
}

function fnValidate() {

    var msg = "";

    if (isNewUser) {
        if ($("#txtPassword").val() == "") {
            msg += 'Password cannot be empty.';
        }
        if ($("#txtConfirmPassword").val() == "") {
            msg += '<br />Confirm Password cannot be empty.';
        }
    }

    if ($("#selCompany").val() == "") {
        msg += '<br />Company cannot be empty.';
    }

    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    if ($("#txtEmail").val() == "" || !emailReg.test($("#txtEmail").val())) {
        msg += '<br />Provide Email in proper format.';
    }

    if (!ValidateCompanyPhoneFax($("#txtPhone").val())) {
        msg += '<br />Valid Phone number is required(ex: ###-###-####).';
    }

    if (sessionStorage.selectedLocationsUserProfile == undefined || sessionStorage.selectedLocationsUserProfile == null || sessionStorage.selectedLocationsUserProfile == "") {
        msg += '<br />At least one location should be selected.';
    }

    if (msg != undefined && msg != "" && msg != null) {
        $('#alertmsg').html(msg);
        $("#popupDialog").popup('open');
        return false;
    }
}

function fnSelectAllLocations() {
    sessionStorage.selectedLocationsUserProfile = JSON.stringify(gData);
    gData = [];
    buildLists();
    buildSelectedLists();
}

function fnRemoveAllLocations() {
    makeGData();
    sessionStorage.selectedLocationsUserProfile = "";
    buildLists();
    buildSelectedLists();
}


//******************** Public Functions End **************************