﻿jQuery.support.cors = true;

//******************** Global Variables Start **************************
var gData;
var gOutputData;
var jobNumber = "";
var facilityId = "";
jobNumber = getSessionData("jobNumber");
//facilityId = (appPrivileges.customerNumber == "1") ? 2 : (appPrivileges.customerNumber == CW) ? 1 : getSessionData("facilityId");
facilityId = sessionStorage.facilityId;

var serviceURL = serviceURLDomain + "api/Logs/" + jobNumber + "/" + facilityId;
var gOutputServiceUrl = '../JSON/_jobSetupOutput.JSON';
//******************** Global Variables End **************************

//******************** Page Load Events Start **************************
$('#_jobSubmit').live('pagebeforecreate', function (event) {
    displayNavLinks();
    if (appPrivileges.customerNumber != undefined) {
        if (appPrivileges.customerNumber == DATAWORKS) {
            //to show ONLY two buttons i.e. Load Circulation/Advertiser Files 
            //when user clicks on "Load Cicculation" and "Load Advertiser" buttons.
            var is_new_analysis = '';
            is_new_analysis = getSessionData("isNewAnalysis");
            if (is_new_analysis != 'true') {
                loadLeftButtons();
            }
        }
    }

    !(appPrivileges.display_chkBatching != undefined && appPrivileges.display_chkBatching) ? $("#chkBatching").hide() : "";  //getting info from authorization web service
    !(appPrivileges.display_lblBatching != undefined && appPrivileges.display_lblBatching) ? $("#lblBatching").hide() : "";  //getting info from authorization web service

    !(appPrivileges.display_chkMailStream != undefined && appPrivileges.display_chkMailStream) ? $("#chkMailStream").hide() : "";  //getting info from authorization web service
    !(appPrivileges.display_lblMailStream != undefined && appPrivileges.display_lblMailStream) ? $("#lblMailStream").hide() : "";  //getting info from authorization web service

    !(appPrivileges.display_chkMailTracker != undefined && appPrivileges.display_chkMailTracker) ? $("#chkMailTracker").hide() : "";  //getting info from authorization web service
    !(appPrivileges.display_lblMailTracker != undefined && appPrivileges.display_lblMailTracker) ? $("#lblMailTracker").hide() : "";  //getting info from authorization web service

    !(appPrivileges.display_chkVersion != undefined && appPrivileges.display_chkVersion) ? $("#chkVersion").hide() : "";  //getting info from authorization web service
    !(appPrivileges.display_lblVersion != undefined && appPrivileges.display_lblVersion) ? $("#lblVersion").hide() : "";  //getting info from authorization web service

    var jobNumber = '';
    var jobDesc = '';
    jobNumber = sessionStorage.jobNumber;
    jobDesc = sessionStorage.jobDesc;
    if ((jobNumber != undefined && jobDesc != undefined)) {
        $('#navHeader').html(jobDesc + ' | ' + jobNumber);
    }
    else {
        clickedLoadAdvt = getSessionData("LoadAdvertisers");
        if (sessionStorage.isNewAnalysis == "true")
            $('#navHeader').html("New Analysis");
        else if (clickedLoadAdvt === "loadAdvt")
            $('#navHeader').html("Load Advertiser");
        else
            $('#navHeader').html("New Job");
    }
    if (jQuery.browser.msie) {
        createIFrame();
    }
    if (sessionStorage.jobSetupOutput != undefined && sessionStorage.jobSetupOutput != null) {
        gOutputData = jQuery.parseJSON(sessionStorage.jobSetupOutput);
        if (gOutputData != null) {
            getUploadedFilesCount();
        }
    }
    else {
        $.getJSON(gOutputServiceUrl, function (dataOutput) {
            gOutputData = dataOutput;
            if (gOutputData != null) {
                getUploadedFilesCount();
            }
        });
    }
    //test for mobility...
    loadMobility();
});
//******************** Page Load Events End **************************

//******************** Public Functions Start **************************
function getData() {
    if (jQuery.browser.msie) {
        setTimeout(function loadData() {
            makeGData();
        }, 2000);
    }
}

$(document).on('pageshow', '#_jobSubmit', function (event) {
    if (!jQuery.browser.msie) {
        makeGData();
    }
    $("#_jobSubmit").trigger("create");
});

function makeGData() {
    if (jQuery.browser.msie) {
        callService("GET", serviceURL, "");
    }
    else {
        getCORS(serviceURL, null, function (data1) {
                gData = data1;
                buildGrid(gData);
        }, function (error_response) {
            showErrorResponseText(error_response);
        });
    }
}

function buildGrid() {
    $("#gridMain").jqGrid({
        datastr: gData,
        datatype: 'jsonstring',
        colNames: ["ID", 'Log Type', "Details", "Created", "Created By"],
        colModel: [
        { name: 'id', index: 'id', width: 1, hidden: true, key: true },
        { name: 'logType', width: '200%', sortable: false },
        { name: 'details', width: '300%', sortable: false },
        { name: 'created', width: '180%', sortable: false },
        { name: 'createdBy', width: '100%', sortable: false }
    ],
        treeGridModel: 'adjacency',
        height: 'auto',
        width: '300%',
        rowNum: 10000,
        treeGrid: true,
        ExpandColumn: 'logType',
        caption: "Job Processing Log",
        gridComplete: function () {
            $.each($('div[class="tree-wrap tree-wrap-ltr"]').find('div[class="ui-icon ui-icon-triangle-1-e tree-plus treeclick"]'), function (key, val) {
                val.parentElement.class = "ui-icon ui-icon-triangle-1-s tree-minus treeclick";
                val.style.backgroundColor = "white";
                val.style.left = "0px";
            });
            $.each($('div[class="ui-jqgrid-titlebar ui-widget-header ui-corner-top ui-helper-clearfix"]'), function (key, val) {
                val.style.backgroundColor = "white";
            });
        }
    });
}

function refreshGrid() {
    $("#gridMain").GridUnload();
    makeGData();
}

//function fnSubmitJob() {
//    var job_submit_json = [
//                            {
//                               "customerNumber":"100003",
//                               "jobNumber": 774252.0,
//                               "userName":"jim.wright",
//                               "userEmail":"jim.wright@tribunedirect.com",
//                               "jobTypeId":1,
//                               "jobName": "Test Dataworks Job.",
//                               "nodeName":""
//                            }
//                       ];

//                           //var post_job_submit_url = myProtocol + 'nexus.tribunedirect.com/api/JobTest';
//                           var post_job_submit_url = myProtocol + 'ksinternal.tribunedirect.com/api/JobTest';

//    if (jQuery.browser.msie) { // FOR IE browser.
//        callService("POST", post_job_submit_url, JSON.stringify(job_submit_json));
//    }
//    else { // FOR other browsers
//        postCORS(post_job_submit_url, JSON.stringify(job_submit_json), function (response) {
//            var msg = (response.toLowerCase() == "success") ? 'Job created successfully.' : 'Job creation failed.';
//            $('#alertmsg').text(msg);
//            $('#popupDialog').popup('open');
//        });
//    }
//}

function closePopup() {
    $("#advancedPreferences").popup("close");
    //makeGData();
}

function callJobSubmit() {
    document.location.href = 'jobSubmitJob.html';
}

function listener(event) {
    gData = jQuery.parseJSON(event.data);
    buildGrid();
}

if (window.addEventListener) {
    addEventListener("message", listener, false);
} else {
    attachEvent("onmessage", listener);
}
function loadLeftButtons() {
    var clickedLoadAdvt = '';
    clickedLoadAdvt = getSessionData("LoadAdvertisers");
    var load_advt_file = 'Load Advertiser File';
    var load_circ_file = 'Load Circulation File';
    var upload_a_file = "Upload A File";
    if (clickedLoadAdvt == "loadAdvt") {
        onlyShowTwoButtons(load_advt_file, upload_a_file);
        $('#aProjectionFile').attr("href", "jobListManagement.html");
    }
    $('#aProjectionFile').click(function () {
        saveSessionData("advertiser_session", "clickedProj");
    });
}
function onlyShowTwoButtons(button_text, upload_a_file) {
    $("#liSetupLocations").hide();
    $("#liSchMilestones").hide();
    $("#liTemplate").hide();
    $('#liListManagement').show();
    $('#aListManagement').attr("href", "jobListManagement.html");
    $('#aListManagement').html(button_text);
    $('#liProjectionFile').show();
    $('#liStoreList').hide();
    $('#liAnalysisSetup').hide();
    $('#liOutPut').hide();
    $('#divHeading').html('<h2>' + button_text + '</h2>');
    $('#divContent').hide();
    $('#lnkUploadFile').html(upload_a_file);
}
//******************** Public Functions End **************************