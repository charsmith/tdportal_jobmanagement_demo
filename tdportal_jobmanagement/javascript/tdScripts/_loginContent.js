﻿function getScottsContent(default_data) {
    var strScottsContent = "";
    var evntURL3 = "http://events.constantcontact.com/register/event?llr=giscy6wab&oeidk=a07edq71a7e7ec45368";
    var evntURL4 = "http://events.constantcontact.com/register/event?llr=giscy6wab&oeidk=a07edspwl2oe833ea81";
    var evntURL5 = "http://events.constantcontact.com/register/event?llr=giscy6wab&oeidk=a07edspyeuad7a12157";
    var scottsTemplates = "";
    var thumbImagePath = logoPath + "jobThumbnails/Scotts/";
    $('#tblImg1').attr('style', default_data.tblImg1Width);
    $('#tblImg2').attr('style', default_data.tblImg2Width);
    $('#tblImg3').attr('style', default_data.tblImg3Width);
    strScottsContent = $('#tblImg1').append('<tr><td style="text-align: justify;" class="word_wrap">' + default_data.img1Text + '</td></tr>' +
                       // '   <tr><td align="left" style="color: rgb(7, 85, 52); font-weight:bold;">REGISTER NOW:</td></tr>' +
                       // '   <tr><td style=""><a data-role="button" href="' + evntURL3 + '" target="_blank" data-mini="true" style="margin-left:0px;"><span style="max-width:312px;" class="word_wrap">Thu, Feb 16: 1-1:30 PM CST</span></a></td></tr>' +
                        '   <tr><td align="left" style="color: rgb(7, 85, 52); font-weight:bold;">HOW IT WORKS:</td></tr>' +
						'   <tr><td style=""><a data-role="button" href="http://images.tribunedirect.com/exposedImages/gizmo/Scotts_TAP_Process.pdf" target="_blank" data-mini="true" style="margin-left:0px;"><span style="max-width:312px;" class="word_wrap">Gizmo Opt-In Process</span></a></td></tr>' +
						'    <tr><td style="">&nbsp;</td></tr>'+
                        '  <tr><td><div align="center" data-role="collapsible-set" data-theme="a" data-content-theme="d" data-mini="true" style="padding-left: 0px; padding-right: 0px;">' +
                        ' <div data-role="collapsible" data-collapsed-icon="carat-r" data-expanded-icon="carat-d" data-theme="a" data-content-theme="d" data-collapsed="true">' +
                        ' <h3>2017 Templates</h3><fieldset data-role="controlgroup" data-mini="true" data-theme="a">' +
                        //' <div class="word_wrap" style="font-size:13px;color:#075534; line-height: 1.3;">' +
                        //' <img title="Four Step – North East/Midwest" style="width:228px;cursor:pointer;" onclick="javascript:viewPDF(\'_t9_fourStepNorthEastMidwest\');" src="' + thumbImagePath + 't9_thumb.png"/><p>Four Step – North East/Midwest<hr style="font-size:13px;color:#075534; line-height: 1.3;"/></p></div>' +
                        //' <div class="word_wrap" style="font-size:13px;color:#075534; line-height: 1.3;">' +
                        //' <img title="Four Step – South/West" style="width:228px;cursor:pointer;" onclick="javascript:viewPDF(\'_t1_fourStepSouthWest\');" src="' + thumbImagePath + 't1_thumb.png"/><p>Four Step – South/West<hr style="font-size:13px;color:#075534; line-height: 1.3;"/></p></div>' +

                        ' <div class="word_wrap" style="font-size:13px;color:#075534; line-height: 1.3;">' +
						' <img title="Take Control" style="width:228px;cursor:pointer;" onclick="javascript:viewPDF(\'_t16_takeControl\');" src="' + thumbImagePath + 't16_thumb.png"/><p>Take Control<hr style="font-size:13px;color:#075534; line-height: 1.3;"/></p></div>' +
                       
                        ' <div class="word_wrap" style="font-size:13px;color:#075534; line-height: 1.3;">' +
                        ' <img title="Four Step Late Spring" style="width:228px;cursor:pointer;" onclick="javascript:viewPDF(\'_t12_fourStepLateSpring\');" src="' + thumbImagePath + 't12_thumb.png"/><p>Four Step Late Spring<hr style="font-size:13px;color:#075534; line-height: 1.3;"/></p></div>' +
                        ' <div class="word_wrap" style="font-size:13px;color:#075534; line-height: 1.3;">' +
                        ' <img title="May I" style="width:228px;cursor:pointer;" onclick="javascript:viewPDF(\'_t13_may1\');" src="' + thumbImagePath + 't13_thumb.png"/><p>May I<hr style="font-size:13px;color:#075534; line-height: 1.3;"/></p></div>' +

                        ' <div class="word_wrap" style="font-size:13px;color:#075534; line-height: 1.3;">' +
                        ' <img title="May II" style="width:228px;cursor:pointer;" onclick="javascript:viewPDF(\'_t14_may2\');" src="' + thumbImagePath + 't14_thumb.png"/><p>May II<hr style="font-size:13px;color:#075534; line-height: 1.3;"/></p></div>' +
                        ' <div class="word_wrap" style="font-size:13px;color:#075534; line-height: 1.3;">' +
                        ' <img title="May III" style="width:228px;cursor:pointer;" onclick="javascript:viewPDF(\'_t15_may3\');" src="' + thumbImagePath + 't15_thumb.png"/><p>May III<hr style="font-size:13px;color:#075534; line-height: 1.3;"/></p></div>' +

                        ' <div class="word_wrap" style="font-size:13px;color:#075534; line-height: 1.3;">' +
                        ' <img title="Lawn Care" style="width:228px;cursor:pointer;" onclick="javascript:viewPDF(\'_t2_lawnCare\');" src="' + thumbImagePath + 't2_thumb.png"/><p>Lawn Care<hr style="font-size:13px;color:#075534; line-height: 1.3;"/></p></div>' +
                        ' <div class="word_wrap" style="font-size:13px;color:#075534; line-height: 1.3;">' +
                        ' <img title="Let’s Get Growing" style="width:228px;cursor:pointer;" onclick="javascript:viewPDF(\'_t3_letsGetGrowing\');" src="' + thumbImagePath + 't3_thumb.png"/><p>Let’s Get Growing<hr style="font-size:13px;color:#075534; line-height: 1.3;"/></p></div>' +
                        ' <div class="word_wrap" style="font-size:13px;color:#075534; line-height: 1.3;">' +
                        ' <img title="No More Weeds" style="width:228px;cursor:pointer;" onclick="javascript:viewPDF(\'_t4_noMoreWeeds\');" src="' + thumbImagePath + 't4_thumb.png"/><p>No More Weeds<hr style="font-size:13px;color:#075534; line-height: 1.3;"/></p></div>' +
                        ' <div class="word_wrap" style="font-size:13px;color:#075534; line-height: 1.3;">' +
                        ' <img title="No More Bugs" style="width:228px;cursor:pointer;" onclick="javascript:viewPDF(\'_t5_noMoreBugs\');" src="' + thumbImagePath + 't5_thumb.png"/><p>No More Bugs<hr style="font-size:13px;color:#075534; line-height: 1.3;"/></p></div>' +
                        ' <div class="word_wrap" style="font-size:13px;color:#075534; line-height: 1.3;">' +
                        ' <img title="Spring Makeover I" style="width:228px;cursor:pointer;" onclick="javascript:viewPDF(\'_t6_springMakeover1\');" src="' + thumbImagePath + 't6_thumb.png"/><p>Spring Makeover I<hr style="font-size:13px;color:#075534; line-height: 1.3;"/></p></div>' +
                        ' <div class="word_wrap" style="font-size:13px;color:#075534; line-height: 1.3;">' +
                        ' <img title="Spring Makeover II" style="width:228px;cursor:pointer;" onclick="javascript:viewPDF(\'_t7_springMakeover2\');" src="' + thumbImagePath + 't7_thumb.png"/><p>Spring Makeover II<hr style="font-size:13px;color:#075534; line-height: 1.3;"/></p></div>' +
                        ' <div class="word_wrap" style="font-size:13px;color:#075534; line-height: 1.3;">' +
                        ' <img title="Summer Survival" style="width:228px;cursor:pointer;" onclick="javascript:viewPDF(\'_t8_summerSurvival\');" src="' + thumbImagePath + 't8_thumb.png"/><p>Summer Survival</p></div>' +
						//' <div class="word_wrap" style="font-size:13px;color:#075534; line-height: 1.3;">' +
                        //' <img title="Four Step - TV" style="width:228px;cursor:pointer;" onclick="javascript:viewPDF(\'_t10_fourStepTV\');" src="' + thumbImagePath + 't10_thumb.png"/><p>Four Step - TV<hr style="font-size:13px;color:#075534; line-height: 1.3;"/></p></div>' +
                        //' <div class="word_wrap" style="font-size:13px;color:#075534; line-height: 1.3;">' +
                        //' <img title="Four Step - Circular" style="width:228px;cursor:pointer;" onclick="javascript:viewPDF(\'_t11_fourStepCircular\');" src="' + thumbImagePath + 't11_thumb.png"/><p>Four Step - Circular</p></div>' +
                        ' </fieldset></div></div></td></tr>');

    $('#tblImg2').append('<tr><td style="text-align: justify;" class="word_wrap">' + default_data.img2Text + '</td></tr>' +
    '   <tr><td align="left" style="color: rgb(7, 85, 52); font-weight:bold;">IMPORTANT DATES:</td></tr>' +
    '   <tr><td align="left" style="font-size:13px;color:#075534; line-height: 1.3;">Opt in <b>Sep 6 - 13</b> for the <b>Oct 4</b> In-Home</td></tr>' +
    '   <tr><td align="left" style="font-size:13px;color:#075534; line-height: 1.3;">Opt in <b>Sep 21 - 26</b> for the <b>Oct 18</b> In-Home</td></tr>' +
    '   <tr><td align="left" style="font-size:13px;color:#075534; line-height: 1.3;">Opt in <b>Oct 9 - 16</b> for the <b>Nov 8</b> In-Home</td></tr>' +
    '   <tr><td align="left" style="font-size:13px;color:#075534; line-height: 1.3;">Opt in <b>Oct 23 - 30</b> for the <b>Nov 22</b> In-Home</td></tr>' +
	'   <tr><td align="left" style="font-size:13px;color:#075534; line-height: 1.3;">Opt in <b>Nov 6 - 13</b> for the <b>Dec 6</b> In-Home</td></tr>' +
	'   <tr><td align="left" style="font-size:13px;color:#075534; line-height: 1.3;">Opt in <b>Nov 20 - 27</b> for the <b>Dec 20</b> In-Home</td></tr>' +
	'   <tr><td style="">&nbsp;</td></tr>'   
    );

   $('#tblImg3').append('<tr><td style="text-align: justify;" class="word_wrap">' + default_data.img3Text + '</td></tr>' +
   '   <tr><td align="left" style="color: rgb(7, 85, 52); font-weight:bold;"><br />CO-OP SOLUTIONS:</td></tr>' +
   //'   <tr><td align="left" style="font-size:13px; color:#777777; line-height: 1.5;">National Direct Marketing Consultant</td></tr>' +
   //'   <tr><td align="left" style="font-size:13px; color:#777777; line-height: 1.5;">National Manager TAP co-op Solutions</td></tr>' +
   '   <tr><td align="left" style="font-size:13px; color:#f69323; line-height: 1.5;"><a style="color:#f69323;" href="mailto:TD-ScottsTeam@tribunedirect.com"><span style="color:#f69323;" >TD-ScottsTeam@tribunedirect.com</span></a></td></tr>' +
   '   <tr><td align="left" style="font-size:13px; color:#777777; line-height: 1.5;">Ph: 708.836.2228</td>' +
   '   <tr><td style="">&nbsp;</td></tr>');
    return strScottsContent;
}

function viewPDF(thumb){
    var pdf_name = thumb.replace(/\b\.\b/g, "_");
    var preview_artwork_url = logoPath + "Scotts/templatePreviews/" + pdf_name + ".pdf";    
    window.open(preview_artwork_url);
}

var imageTextStyle = '<tr><td style="text-align: justify;" class="word_wrap">{0}</td></tr>';

function loadTextContentBelowImages(default_data, customer_number) {
    switch (customer_number) {
        case ACE:
		case CW:
		case SPORTSKING:
        case CASEYS:
        case ANDERSEN:
            $('#tblImg1').attr('style', default_data.tblImg1Width);
            $('#tblImg2').attr('style', default_data.tblImg2Width);
            $('#tblImg3').attr('style', default_data.tblImg3Width);
            $('#tblImg1').append(stringPlaceHolder(imageTextStyle, [default_data.img1Text]));
            $('#tblImg2').append(stringPlaceHolder(imageTextStyle, [default_data.img2Text]));
            $('#tblImg3').append(stringPlaceHolder(imageTextStyle, [default_data.img3Text]));
            break;
        default:
            break;
    }    
}

