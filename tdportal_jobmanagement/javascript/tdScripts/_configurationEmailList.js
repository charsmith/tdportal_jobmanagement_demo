﻿//******************** Data URL Variables Start **************************

var gServiceEmailList = "../JSON/_configurationEmails.JSON";
var gServiceEmailListData;
var emailData;
var pageObj;
var gOutputData;
var gPageSize = 20;
//******************** Data URL Variables End **************************
var mapping = { 'ignore': ["__ko_mapping__"] }
function getEmailRecipientsData() {
    
    if (appPrivileges.roleName == "admin" && jobCustomerNumber != BUFFALONEWS && jobCustomerNumber != PIONEERPRESS && jobCustomerNumber != REDPLUM) {//todo: remove customer condition later
        var job_number = getSessionData("jobNumber");
        gServiceEmailList = serviceURLDomain + "api/InfoAction_getEmailLists/" + sessionStorage.facilityId + "/" + jobCustomerNumber + "/" + job_number;
        getCORS(gServiceEmailList, null, function (response_data) {
            emailData = response_data;
        }, function (error_response) {
            showErrorResponseText(error_response, true);
        });
    }
    else {
        emailData = {};
    }
};

function createPagerObject1(total_pages, start, end) {
    var pager_length = Math.ceil(total_pages / gPageSize);
    var pager_object = [];
    if (total_pages > 1) {
        for (var j = 0; j < total_pages; j++) {
            pager_object.push({ 'name': 'Page ' + (j + 1), 'value': (j) });
        }
    }
    return pager_object;
}

var emailRecipientGroup = function (name, email_list_info, selected_page_value, search_value) {
    var self = this;
    ko.mapping.fromJS(email_list_info, {}, self);
    //self.caption = ko.observable(name);
    //self.name = email_list_info.name;
    //self.type = ko.observable(email_list_info.type);
    self.listType = ko.observable(((email_list_info.type != undefined && email_list_info.type != null && email_list_info.type != "") ? email_list_info.type : "email"));
    //self.description = ko.observable();
    //self.description(email_list_info.description);
    //self.emails = ko.observableArray([]);
    //$.each(email_list_info.infoEmailItemList, function (key, val) {
    //    self.emails.push(new emailRecipient(val, self.type()));
    //});
    self.pagingData = ko.observableArray([]).extend({ rateLimit: { method: "notifyWhenChangesStop", timeout: 200 } });
    self.selectedPageValue = ko.observable();
    self.selectedPageValue(selected_page_value);
    self.showMoreOption = ko.observable();
    self.showSearchField = ko.observable();
    self.searchValue = ko.observable();
    self.prevSearchValue = ko.observable('');
    self.searchValue(((search_value != undefined && search_value != null) ? search_value : ""));
    self.selectedFilterOption = ko.observable();
    self.selectedFilterOption('emailAddress');
};

var emailRecipient = function (data_info, type) {
    var self = this;
    self.name = ko.observable((data_info != undefined && data_info != null && data_info != "") ? data_info.name : "");
    //User Id is nothing but either emailAddress for list type 'email' or UserName for list type 'gizmoUser'
    self.userId = ko.observable((data_info != undefined && data_info != null && data_info != "") ? ((type == "email") ? data_info.emailAddress : data_info.gizmoUser) : "");
    self.type = ko.observable(type);
    self.fieldsList = ko.computed(function () {
        return "Name: " + self.name();
    });
    self.isActive = ko.observable(true);
    if (data_info != undefined && data_info != null && data_info != "")
        self.isActive((data_info.active == "true" || data_info.active == undefined) ? true : false);
    //self.fieldsList = ko.computed(function () {
    //    var temp_fields = "";
    //    temp_fields = "First Name: " + self.firstName() + ' | ' + "Last Name: " + self.lastName();
    //    //$.each(data_info, function (key, val) {
    //    //    if (key.toLowerCase() != "emailaddress") {
    //    //        var field_name = "";
    //    //        field_name = getDisplayGroupName(key);
    //    //        temp_fields += (temp_fields != "") ? ' | ' + field_name + ': ' + val : field_name + ': ' + val;
    //    //    }
    //    //});
    //    return temp_fields;
    //});
}

var makeEmailRecipients = function (self) {
    self.emailData = emailData;
    self.emailsList = ko.observableArray([]);
    self.selectedList = ko.observable({});
    self.prevSelectedList = ko.observable({});
    self.selectedEmailListItem = ko.observable({});
    self.prevSelectedEmailListItem = {};
    self.selectedEmailType = ko.observable();
    self.emailsQuantity = ko.observable();
    self.sendEmailType = ko.observable();
    self.emailTypeMode = ko.observable();
    self.sendEmailsList = ko.observableArray([]);
    self.getPageDisplayName = function (page_name) {
        var PATTERN = /^[ABCDEFGHIJKLMNOPQRSTUVWXYZ]/;
        var page_display_name = '';
        $.each(new String(page_name), function (key, value) {
            if (key > 0 && value.match(PATTERN)) {
                page_display_name += ((!page_display_name.endsWith(' ') && !page_display_name.endsWith('-')) ? ' ' : '') + value;
            }
            else {
                page_display_name += value;
            }
        });
        return page_display_name;
    };
    var temp_key_email = "";
    var temp_email_list = [];
    var default_list = "";
    if (self.emailData != undefined && Object.keys(self.emailData).length > 0) {
        $.each(self.emailData, function (key, val) {
            //$.each(val, function (key1, val1) {
            var index = 0;
            var temp_key = key;
            $.each(val.infoEmailItemList, function (key1, val1) {
                val1.isActive = ((val1.active == "true" || val1.active == undefined) ? true : false);
                if ((temp_key == ((window.location.href.toLocaleLowerCase().indexOf('milestones') > -1) ? "milestone" : "default")) && index < 3) {
                    default_list += (default_list != "") ? ", " + val1.emailAddress : val1.emailAddress;
                }
                index++;
            });
            temp_key_email = self.getPageDisplayName(val.label.initCap());
            self.emailsList.push(new emailRecipientGroup(temp_key_email, val, 0, ''));
            //});
        });
    }
    if (appPrivileges.roleName == "admin") {
        window.setTimeout(function () {
            if (window.location.href.toLocaleLowerCase().indexOf('milestones') > -1 && jobCustomerNumber!=CASEYS) {
                $('#dvEmailLists').append("<div data-emailList='milestones'><span style='font-weight:bold'>Milestone Email List: </span><span>" + default_list + "<span><span>&nbsp;&nbsp;" + (sessionStorage.userRole == 'admin' ? "<a href='#' onclick='pageObj.getEmailLists()'>edit</a></span></div>" : '</span></div>'));
            }
            else if (window.location.href.toLocaleLowerCase().indexOf('submitorder') > -1 && jobCustomerNumber != CASEYS) {
                window.setTimeout(function () {
                    $('#dvOrderContent').append("<div data-emailList='submitorder'><span style='font-weight:bold'>Email Contact List: </span><span>" + default_list + "<span><span>&nbsp;&nbsp;" + (sessionStorage.userRole == 'admin' ? "<a href='#' onclick='pageObj.getEmailLists()'>edit</a></span></div>" : '</span></div>'));
                }, 1000);
            }
        }, 1000);
    }

    self.canUpdateList = ko.computed(function () {
        // test conditions for saving here (i.e. whether all the fields have values)
        // and return true if you can save
        // for example:
        if (self.emailsList().length == 0)
            $('#btnUpdateList').addClass('ui-disabled');
        else
            $('#btnUpdateList').removeClass('ui-disabled');
        return self.emailsList().length > 0;
    });

    self.openNewEmailListPopup = function (data, el) {
        $('#rdoGizmoUser').removeClass('ui-disabled');
        $('#rdoGizmoUser')[0].disabled = false;
        $('#editEmailListPopup').popup('close');
        $('#btnMakeList').text('Add');
        $('#popupEmailListInfo h3').text('Add Email List');
        self.selectedList({});
        self.prevSelectedList({});
        $('#popupEmailListInfo input[type=radio]').attr('checked', false);
        $('#rdoEmail').attr('checked', true);//.checkboxradio('refresh');
        window.setTimeout(function () {
            $('#popupEmailListInfo').popup('open');
            $('#popupEmailListInfo input[type=radio]').checkboxradio('refresh');
        }, 300);
    }
    self.addEmailClick = function (data, el) {
        var ctrl = (el.targetElement || el.target || el.currentTarget);
        self.emailTypeMode('new');
        $('#popupEmailRecipient h3').text('Add Email Recipient');
        $('#btnAddNewRecipient').text('Add Email Recipient');
        var temp_item = {};
        temp_item[(data.type() == "email" ? "emailAddress" : "gizmoUser")] = "";
        temp_item["name"] = "";
        temp_item["isActive"] = true;
        self.selectedEmailListItem({});
        self.selectedEmailListItem(ko.mapping.fromJS(temp_item, {}));
        self.prevSelectedEmailListItem = temp_item;
        $('#dvUserId label[for=txtUserId]').text((data.type() == "email" ? "Email:" : "Gizmo User:"));
        $('#editEmailListPopup').popup('close');
        window.setTimeout(function () {
            $('#popupEmailRecipient').popup('open', { positionTo: 'window' });
        }, 200);
    };

    self.cancelListInfo = function () {
        $('#popupEmailListInfo').popup('close');
        window.setTimeout(function () {
            $('#editEmailListPopup').popup('open');
        }, 500);
        self.selectedList().label(self.prevSelectedList().label());
        self.selectedList().description(self.prevSelectedList().description());
        self.selectedList().type(self.prevSelectedList().type());
        self.selectedList().infoEmailItemList(self.prevSelectedList().infoEmailItemList());
        self.selectedList().name(self.prevSelectedList().name());

        $('#dv' + self.prevSelectedList().label().replace(/ /g, '_') + ' ul').listview('refresh');
        self.prevSelectedList({});
        self.selectedList({});
    };

    //Adds new user/email list.
    self.addNewList = function (data, el) {
        if (!self.addOrEditEmailListValidation($('#btnMakeList').text().toLowerCase())) return false;
        var label = $('#txtEmailListLabel').val();
        //var name = label.replace(' ', '').initSmall();
        var name = (label.indexOf(' ') > -1) ? label.substring(0, label.indexOf(' ')).toLowerCase() : label.toLowerCase();
        //if (self.prevSelectedList().type() != data.type()) {
        //    $('#confirmMsg').html("All the existing members of this list will be deleted, do you want to continue?");
        //    $('#okButConfirm').text('Continue')
        //    $('#okButConfirm').unbind('click');
        //    $('#popupEmailListInfo').popup('close');
        //    $('#okButConfirm').bind('click', function () {
        //        $('#okButConfirm').unbind('click');
        //        $('#popupConfirmDialog').popup('close');
        //        data.users.removeAll();
        //        window.setTimeout(function () {
        //            $("#editEmailListPopup").popup('open');
        //        }, 500);
        //    });
        //    window.setTimeout(function () {
        //        $('#popupConfirmDialog').popup('open');
        //    }, 500);
        //    return false;
        //}


        var type = $('input[name=rdoType]:checked').val();
        var desc = $('#txtEmailListDescription').val();
        if ($('#btnMakeList').text().indexOf('Add') > -1) {
            var email_list_info = {};
            email_list_info["label"] = label;
            email_list_info["name"] = name;
            email_list_info["infoEmailItemList"] = [];
            email_list_info["type"] = type;
            email_list_info["description"] = desc;
            self.emailsList.push(new emailRecipientGroup(label, email_list_info, 0, ''));

            $('#alertmsg').html('<b>' + label + '</b> has been added successfully.');
            $("#popupEmailListInfo").popup('close');
            $('#okBut').removeAttr('onclick');
            $('#okBut').unbind('click');
            $('#okBut').bind('click', function () {
                $('#okBut').unbind('click');
                $('#okBut').attr('onclick', '$(\'#popupDialog\').popup(\'close\');');
                $('#popupDialog').popup('close');
                self.updateEmailListsUI(label);
            });
            window.setTimeout(function () {
                $('#popupDialog').popup('open');
            }, 500);
        }
        else {
            if (self.selectedList().type() != self.selectedList().listType()) {
                $('#confirmMsg').html('All the existing members of <b>' + label + '</b> will be deleted, do wou want to continue?');
                $('#okButConfirm').text('Confirm');
                $('#okButConfirm').unbind('click');
                $("#popupEmailListInfo").popup('close');
                $('#okButConfirm').bind('click', function () {
                    $('#okButConfirm').unbind('click');
                    $('#popupConfirmDialog').popup('close');
                    //self.selectedList().type(self.selectedList().listType());
                    $.each(self.emailsList(), function (key, val) {
                        if (val.name().toLowerCase() == self.selectedList().name().toLowerCase()) {
                            val.infoEmailItemList.removeAll();
                            val.type(self.selectedList().listType());
                            return false;
                        }
                    });
                    //$('#popupEmailListInfo').popup('close');
                    $('#alertmsg').html('<b>' + label + '</b> has been updated successfully.');
                    $('#okBut').removeAttr('onclick');
                    $('#okBut').unbind('click');
                    $('#okBut').bind('click', function () {
                        $('#okBut').unbind('click');
                        $('#okBut').attr('onclick', '$(\'#popupDialog\').popup(\'close\');');
                        $('#popupDialog').popup('close');
                        self.updateEmailListsUI(label);
                    });
                    window.setTimeout(function () {
                        $('#popupDialog').popup('open');
                    }, 500);
                });
                $('#cancelButConfirm').unbind('click');
                $('#cancelButConfirm').bind('click', function () {
                    $('#cancelButConfirm').unbind('click');
                    $('#cancelButConfirm').removeAttr('onclick');
                    $('#popupConfirmDialog').popup('close');
                    $('#popupEmailListInfo input[type=radio]').attr('checked', false);
                    $('#popupEmailListInfo input[type=radio][value="' + self.selectedList().type() + '"]').attr('checked', true);
                    self.selectedList().listType(self.selectedList().type());
                    $('#popupEmailListInfo input[type=radio]').checkboxradio('refresh');
                    window.setTimeout(function () {
                        $("#editEmailListPopup").popup('open');
                    }, 500);
                });
                window.setTimeout(function () {
                    $('#popupConfirmDialog').popup('open');
                }, 500);
            }
            else {
                //self.selectedList().type(self.selectedList().listType());
                $("#popupEmailListInfo").popup('close');
                $('#alertmsg').html('<b>' + label + '</b> has been updated successfully.');
                $('#okBut').removeAttr('onclick');
                $('#okBut').unbind('click');
                $('#okBut').bind('click', function () {
                    $('#okBut').unbind('click');
                    $('#okBut').attr('onclick', '$(\'#popupDialog\').popup(\'close\');');
                    $('#popupDialog').popup('close');
                    self.updateEmailListsUI(label);
                });
                window.setTimeout(function () {
                    $('#popupDialog').popup('open');
                }, 500);
            }
        }
        /*$('#popupEmailListInfo').popup('close');
        $('#okBut').removeAttr('onclick');
        $('#okBut').bind('click', function () {
            $('#okBut').unbind('click');
            $('#okBut').attr('onclick', '$(\'#popupDialog\').popup(\'close\');');
            $('#popupDialog').popup('close');
            window.setTimeout(function () {
                $('#dv' + label.replace(/ /g, '_')).trigger('create');
                $('#dvEmailRecipients').collapsibleset('refresh');
                $.each($('#dvEmailRecipients div[data-role=collapsible]'), function () {
                    $(this).on("collapsiblecollapse", function (e) {
                        $(this).data("previous-state", "collapsed");
                        //$(this).find('.ui-input-clear').unbind('click');
                    });
                    $(this).on("collapsibleexpand", function (e) {
                        $(this).data("previous-state", "expanded");
                        //$('.ui-content').on('click', '.ui-input-clear', function (e) {
                        self.getData(this.id);
                    });
                    $(this).find('.ui-input-clear').bind('click', function (e) {
                        if (e.originalEvent) {
                            vm.search();
                        }
                    });
                });
                $('#editEmailListPopup').popup('open');
            }, 500);
        });
        window.setTimeout(function () {
            $('#popupDialog').popup('open');
        }, 500);*/
    };

    self.updateEmailListsUI = function (label) {
        window.setTimeout(function () {
            $('#dv' + label.replace(/ /g, '_')).trigger('create');
            $('#dvEmailRecipients').collapsibleset('refresh');
            $.each($('#dvEmailRecipients div[data-role=collapsible]'), function () {
                $(this).on("collapsiblecollapse", function (e) {
                    $(this).data("previous-state", "collapsed");
                    //$(this).find('.ui-input-clear').unbind('click');
                });
                $(this).on("collapsibleexpand", function (e) {
                    $(this).data("previous-state", "expanded");
                    //$('.ui-content').on('click', '.ui-input-clear', function (e) {
                    self.getData(this.id);
                });
                $(this).find('.ui-input-clear').bind('click', function (e) {
                    if (e.originalEvent) {
                        vm.search();
                    }
                });
            });
            $('#editEmailListPopup').popup('open');
        }, 500);
    };

    //Updates the existing user/email list.
    self.openUpdateList = function (data, el) {
        $('#btnMakeList').text('Update');
        $('#popupEmailListInfo h3').text('Update Email List');
        self.selectedList(data);
        var temp_prev = ko.toJS(data, mapping);
        temp_prev = ko.mapping.fromJS(temp_prev);
        self.prevSelectedList(temp_prev);
        if (data.name() == 'default' || data.label().toLowerCase().indexOf('default') > -1) {
            $('#rdoGizmoUser').addClass('ui-disabled');
            $('#rdoGizmoUser')[0].disabled = true;
        }
        else {
            $('#rdoGizmoUser').removeClass('ui-disabled');
            $('#rdoGizmoUser')[0].disabled = false;
        }
        $("#editEmailListPopup").popup('close');
        window.setTimeout(function () {
            $('#popupEmailListInfo input[type=radio]').checkboxradio('refresh');
            $('#popupEmailListInfo').popup('open', { positionTo: 'window' });
        }, 500);
    };
    //Removes the selected list
    self.removeEmailList = function (data, e) {
        var ctrl_id = (e.targetElement || e.currentTarget || e.target);
        var ctrl = $(ctrl_id);
        var context_data = ko.contextFor(ctrl[0]).$parent.emailsList;
        $('#confirmMsg').html('Do you want to delete <b>' + data.label() + '</b>?');
        $('#okButConfirm').text('Delete')
        $('#okButConfirm').unbind('click');
        $("#editEmailListPopup").popup('close');
        $('#okButConfirm').bind('click', function () {
            $('#okButConfirm').unbind('click');
            $('#popupConfirmDialog').popup('close');
            context_data.remove(data);
            if (context_data.length == 0) {
                //if (Object.keys(response_data).length == 0) {
                //window.setTimeout(function () {
                //$('#dvNoLists').text('Currently there are no lists available. Click "Add New List" to add a new list.');
                //$('#dvNoLists').css('display', 'block');
                //$('#btnUpdateList').addClass('ui-disabled');
                //$('#btnUpdateList')[0].disabled = true;
                //}, 1000);
                // }
            }
            $('#dvEmailRecipients').collapsibleset('refresh');
            window.setTimeout(function () {
                $("#editEmailListPopup").popup('open');
            }, 500);
        });
        $('#cancelButConfirm').unbind('click');
        $('#cancelButConfirm').bind('click', function () {
            $('#cancelButConfirm').unbind('click');
            $('#popupConfirmDialog').popup('close');
            window.setTimeout(function () {
                $("#editEmailListPopup").popup('open');
            }, 500);
        });
        window.setTimeout(function () {
            $('#popupConfirmDialog').popup('open');
        }, 500);
    };

    self.addNewEmailRecipient = function (data, el) {
        var selected_target = $('#hdnSelectedGroup').val();
        if (selected_target != "") {
            var context_data = ko.dataFor($('#' + selected_target.replace(/ /g, '_'))[0]);
            if (!self.emailRecipientValidation(context_data.type())) {
                self.selectedEmailListItem(self.prevSelectedEmailListItem);
                return false;
            }
            $.each(self.emailsList(), function (key, val) {
                if (context_data.label() == val.label()) {
                    var verify_email_exist = $.grep(val.infoEmailItemList(), function (el) {
                        return ((el.emailAddress || el.gizmoUser)() === (self.selectedEmailListItem().emailAddress || self.selectedEmailListItem().gizmoUser)());
                    });
                    if ($('#btnAddNewRecipient').text().toLowerCase().indexOf('update') == -1) {
                        if (verify_email_exist.length == 0) {
                            //val.infoEmailItemList.push(new emailRecipient(ko.toJS(self.selectedEmailListItem), context_data.type()));
                            val.infoEmailItemList.push(self.selectedEmailListItem());
                        }
                        else {
                            $("#okBut").unbind("click");
                            $("#okBut").bind('click', function () {
                                $("#okBut").unbind("click");
                                $("#okBut").attr('onclick', '$("#popupDialog").popup("close")');
                                $('#popupDialog').popup('close');
                                $('#dvEmailRecipients ul').listview('refresh');
                                window.setTimeout(function () {
                                    $('#editEmailListPopup').popup('open', { positionTo: 'window' });
                                }, 200);
                            });

                            $("#popupEmailRecipient").popup('close');
                            $('#alertmsg').html("The email recipient already exists.");
                            $('#popupDialog').popup('open');
                        }
                    }
                    return false;
                }
            });
        }
        self.prevSelectedEmailListItem = {};
        self.selectedEmailListItem({});

        //if (self.emailTypeMode() == "new") {
        //    $.each($('input[type=checkbox]'), function (key, val) {
        //        if ($(this).is(":checked")) {
        //            $.each(self.emailsList(), function (key1, val1) {
        //                if (self.getPageDisplayName(val.value.initCap()) == val1.caption()) {
        //                    val1.emails.push(new emailRecipient(ko.toJS(self.selectedEmailListItem)));
        //                    return false;
        //                }
        //            });
        //        }
        //    });
        //}
        //else if (self.emailTypeMode() == "edit") {
        //    $.each(self.emailsList(), function (key1, val1) {
        //        if (self.selectedEmailType().toLowerCase() == val1.caption().toLowerCase() && !$('input[type=checkbox][value="' + self.selectedEmailType() + '"]').is(':checked')) {
        //            if (self.selectedEmailType().toLowerCase() == val1.caption().toLowerCase()) {
        //                val1.emails.remove(self.selectedEmailListItem());
        //            }
        //        }
        //        else if (self.selectedEmailType().toLowerCase() != val1.caption().toLowerCase() && $('input[type=checkbox][value="' + val1.caption() + '"]').is(':checked')) {
        //            val1.emails.push(new emailRecipient(ko.toJS(self.selectedEmailListItem)));
        //        }
        //    });

        //    self.selectedEmailListItem({});
        //}
        $("#popupEmailRecipient").popup('close');
        //}
        $('#dvEmailRecipients ul').listview('refresh');

        window.setTimeout(function () {
            $('#editEmailListPopup').popup('open', { positionTo: 'window' });
        }, 200);
    }

    //self.emailTypeChange = function (data, event) {
    //    if (event.target.checked)
    //        $('#dvEmailInfo').css('display', 'block');
    //    else
    //        $('#dvEmailInfo').css('display', 'none');
    //};


    self.updateEmailList = function (data, el) {
        var updated_emails_list = [];

        updated_emails_list = ko.mapping.toJS(self.emailsList, mapping);
        var k = updated_emails_list;
        var temp_data = {};
        $.each(updated_emails_list, function (key, val) {
            temp_data[val.name] = val;
        });
        $("#editEmailListPopup").popup('close');
        var job_number = getSessionData("jobNumber");
        var update_email_list_service = serviceURLDomain + "api/InfoAction_updateEmailLists/" + sessionStorage.facilityId + "/" + jobCustomerNumber + "/" + job_number;
        var default_list = "";
        postCORS(update_email_list_service, JSON.stringify(temp_data), function (response) {

            getEmailRecipientsData();

            self.emailData = emailData;
            window.setTimeout(function () {
                var temp_key_email = "";
                var temp_email_list = [];
                self.emailsList.removeAll();
                $.each(self.emailData, function (key, val) {
                    //$.each(val, function (key1, val1) {
                    var index = 0;
                    $.each(val.infoEmailItemList, function (key1, val1) {
                        val1.isActive = ((val1.active == "true" || val1.active == undefined) ? true : false);
                        if ((key == ((window.location.href.toLocaleLowerCase().indexOf('milestones') > -1) ? "milestone" : "default")) && index < 3) {
                            default_list += (default_list != "") ? ", " + val1.emailAddress : val1.emailAddress;
                        }
                        index++;
                    });
                    temp_key_email = self.getPageDisplayName(val.label.initCap());
                    self.emailsList.push(new emailRecipientGroup(temp_key_email, val, 0, ''));
                    //});
                });
                if (window.location.href.toLocaleLowerCase().indexOf('milestones') > -1) {
                    $('#dvEmailLists div[data-emailList="milestones"]').empty();
                    $('#dvEmailLists div[data-emailList="milestones"]').append("<span style='font-weight:bold'>Milestone Email List: </span><span>" + default_list + "<span><span>&nbsp;&nbsp;" + (sessionStorage.userRole == 'admin' ? "<a href='#' onclick='pageObj.getEmailLists()'>edit</a></span></div>" : '</span>'));
                }
                else if (window.location.href.toLocaleLowerCase().indexOf('submitorder') > -1) {
                    window.setTimeout(function () {
                        $('#dvOrderContent div[data-emailList="submitorder"]').empty();
                        $('#dvOrderContent div[data-emailList="submitorder"').append("<span style='font-weight:bold'>Email Contact List: </span><span>" + default_list + "<span><span>&nbsp;&nbsp;" + (sessionStorage.userRole == 'admin' ? "<a href='#' onclick='pageObj.getEmailLists()'>edit</a></span></div>" : '</span>'));
                    }, 1000);
                }
            }, 1000);

            $('#alertmsg').html('Emails List has been updated successfully.');
            window.setTimeout(function () {
                $('#popupDialog').popup('open');
            }, 500);
        }, function (response_error) {
            showErrorResponseText(response_error, false);
        });
    };

    self.editEmailClick = function (data, el) {
        var ctrl = (el.targetElement || el.target || el.currentTarget);
        self.emailTypeMode('edit');
        $('#popupEmailRecipient h3').text('Update Email Recipient');
        $('#btnAddNewRecipient').text('Update Email Recipient');
        self.selectedEmailListItem(data);
        var temp_selected_list_item = (ko.toJS(data));
        delete temp_selected_list_item["fieldsList"];
        var context_data = ko.contextFor(ctrl).$parents[1];
        self.prevSelectedEmailListItem = temp_selected_list_item;
        $('#dvUserId label[for=txtUserId]').text((context_data.type() == "email" ? "Email:" : "Gizmo User:"));
        $('#editEmailListPopup').popup('close');
        window.setTimeout(function () {
            $('#popupEmailRecipient').popup('open', { positionTo: 'window' });
        }, 200);
    };

    self.addItem = function (data, event) {
        var ctrl_id = (event.targetElement || event.target || event.currentTarget);
        data.isActive(true);
        self.selectedElement = ctrl_id;
        $($(self.selectedElement)[0].previousElementSibling).removeClass('ui-disabled')[0].disabled = false;

        $(self.selectedElement).removeClass('ui-icon-plus').addClass('ui-icon-delete').attr('data-icon', 'delete');

        $(self.selectedElement).attr('title', 'Remove');
    }

    self.selectedElement = null;

    self.removeItem = function (data, event) {
        if (data.isActive() == false) {
            self.addItem(data, event);
            return false;
        }
        var ctrl_id = (event.targetElement || event.target || event.currentTarget);
        self.selectedElement = ctrl_id;
        //var ctrl = $('#' + e.target.id);
        var context_data;
        var selected_target = $('#hdnSelectedGroup').val();
        if (selected_target != "") {
            var page_index = 0;
            context_data = "";
            //var ctrl_id = 'dv' + selected_target.replace(/ /g, '_');
            var ctrl_id = selected_target.replace(/ /g, '_');
            if (selected_target != "") {
                //context_data = ko.dataFor($('#dv' + selected_target.replace(/ /g, '_'))[0]);
                context_data = ko.dataFor($('#' + selected_target.replace(/ /g, '_'))[0]);
            }
            var selected_page_value = (context_data.selectedPageValue() != undefined && context_data.selectedPageValue() != null && context_data.selectedPageValue() != "") ? context_data.selectedPageValue() : 0;
        }
        //
        var message = 'Are you sure you want remove this item?';
        $('#confirmNavMsg').html(message);
        $('#dontSaveNavBut').text('Make Inactive');
        $('#dontSaveNavBut').bind('click', function () {
            $('#dontSaveNavBut').unbind('click');

            //Make inactive 
            data.isActive(false);

            $($(self.selectedElement)[0].previousElementSibling).addClass('ui-disabled')[0].disabled = true;
            $(self.selectedElement).removeClass('ui-icon-delete').addClass('ui-icon-plus').attr('data-icon', 'plus');
            $(self.selectedElement).attr('title', 'Make Active');
            //$("#" + ctrl_id).attr('data-icon', "plus")
            //.find('.ui-icon')
            //    .addClass('ui-icon-' + "plus")
            //    .removeClass('ui-icon-' + "delete");

            $('#popupNavConfirmDialog').popup('close');
            window.setTimeout(function () {
                $('#editEmailListPopup').popup('open', { positionTo: 'window' });
            }, 200);
        });
        $('#cancelNavBut').bind('click', function () {
            $('#cancelNavBut').unbind('click');
            $('#popupNavConfirmDialog').popup('close');
            window.setTimeout(function () {
                $('#editEmailListPopup').popup('open', { positionTo: 'window' });
            }, 200);
        });
        $('#saveNavBut').html('Remove');
        $('#saveNavBut').bind('click', function () {
            $('#saveNavBut').unbind('click');
            $.each(self.emailsList(), function (key, val) {
                if (context_data.label() == val.label() && data.isActive()) {
                    val.infoEmailItemList.remove(data);
                    return false;
                }
            });
            $('#popupNavConfirmDialog').popup('close');
            window.setTimeout(function () {
                $('#editEmailListPopup').popup('open', { positionTo: 'window' });
            }, 200);
        });
        $('#editEmailListPopup').popup('close');
        window.setTimeout(function () {
            $('#popupNavConfirmDialog').popup('open', { positionTo: 'window' });
        }, 200);
    };

    self.emailCancel = function () {
        if (self.emailTypeMode() == "new")
            self.selectedEmailListItem({});
        else {
            $.each(self.prevSelectedEmailListItem, function (key, val) {
                self.selectedEmailListItem()[key](val);
            });
        }
        self.prevSelectedEmailListItem = {};
        self.selectedEmailListItem({});
        $('#popupEmailRecipient').popup('close');
        window.setTimeout(function () {
            $('#editEmailListPopup').popup('open');
        }, 200);
    };

    self.addOrEditEmailListValidation = function (mode) {
        var msg = "";
        var regFirstLast = /^[a-zA-Z ]*$/;
        var regDescription = /^[A-Za-z0-9 .'?!,@$#-_\n\r]*$/;///[^A-Za-z0-9 .'?!,@$#-_\n\r]/
        if ($("#txtEmailListLabel").val() == "") {
            if (msg != undefined && msg != "" && msg != null)
                msg += "<br />Please enter Label";
            else
                msg = "Please enter Label";
            $('#txtEmailListLabel').val('');
        }
        else if (!regFirstLast.test($("#txtEmailListLabel").val())) {
            if (msg != undefined && msg != "" && msg != null)
                msg += "<br />Please enter valid Label";
            else
                msg = "Please enter valid Label";
            $('#txtEmailListLabel').val('');
        }
        if ($("#txtEmailListDescription").val() == "") {
            if (msg != undefined && msg != "" && msg != null)
                msg += "<br />Please enter Description";
            else
                msg = "Please enter Description";
            $('#txtEmailListDescription').val('');
        }
        else if (!regDescription.test($("#txtEmailListDescription").val())) {
            if (msg != undefined && msg != "" && msg != null)
                msg += "<br />Please enter valid Description";
            else
                msg = "Please enter valid Description";
            $('#txtEmailListDescription').val('');
        }
        var emailList_name = $("#txtEmailListLabel").val();
        if (emailList_name.length > 0) {
            emailList_name = (emailList_name.indexOf(' ') > -1) ? emailList_name.substring(0, emailList_name.indexOf(' ')) : emailList_name;
            var verify_emaillist_name_exist = "";
            if (mode == 'add')
                verify_emaillist_name_exist = $.grep(self.emailsList(), function (el) {
                    return el.name().toLowerCase() === emailList_name.toLowerCase();
                });
            else {
                var selected_list = self.selectedList();
                verify_emaillist_name_exist = $.grep(self.emailsList(), function (el) {
                    return (el.name().toLowerCase() === emailList_name.toLowerCase() && el.name().toLowerCase() != selected_list.name().toLowerCase());
                });
            }

            if (verify_emaillist_name_exist.length > 0) {
                if (msg != undefined && msg != "" && msg != null)
                    msg += "There is already a list with the name <b>" + emailList_name + "</b>, please enter different. Names are determined with first word of your label."
                else
                    msg = "There is already a list with the name <b>" + emailList_name + "</b>, please enter different. Names are determined with first word of your label.";
            }
        }

        if (msg != "") {
            $('#alertmsg').html(msg);
            $('#alertmsg').css('text-align', 'left');
            $('#popupEmailListInfo').popup('close');
            $('#okBut').unbind('click');
            $('#okBut').removeAttr('onclick');
            $("#popupDialog").popup('open');
            $("#okBut").bind('click', function () {
                $('#okBut').unbind('click');
                $('#okBut').attr('onclick', '$(\'#popupDialog\').popup(\'close\');');
                $('#popupDialog').popup('close');
                window.setTimeout(function () {
                    $('#popupEmailListInfo').popup('open');
                }, 300);
            });
            return false;
        }
        return true;
    }

    self.emailRecipientValidation = function (type) {
        var msg = "";
        var regEmail = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        var regName = /^[a-zA-Z _]*$/;
        var regxGizmoUser = /^[A-Za-z0-9_]*$/

        if ($("#txtUserName").val() == "") {
            if (msg != undefined && msg != "" && msg != null)
                msg += "<br />Please enter Name";
            else
                msg = "Please enter Name";
            $('#txtUserName').val('');
        }
        else if (!regName.test($("#txtUserName").val())) {
            if (msg != undefined && msg != "" && msg != null)
                msg += "<br />Please enter valid Name";
            else
                msg = "Please enter valid Name";
            $('#txtUserName').val('');
        }

        if ($("#txtUserId").val() == "") {
            if (msg != undefined && msg != "" && msg != null)
                msg += ((type == "email") ? "<br />Please enter Email address." : "<br />Please enter Gizmo User.");
            else
                msg = ((type == "email") ? "Please enter Email address." : "Please enter Gizmo User.");
            $('#txtUserId').val('');
        }
        else if (type == "email") {
            if (!regEmail.test($("#txtUserId").val())) {
                if (msg != undefined && msg != "" && msg != null)
                    msg += "<br />Please enter valid Email address.";
                else
                    msg = "Please enter valid Email address.";
                $('#txtUserId').val('');
            }
        }
        else if (!regxGizmoUser.test($("#txtUserId").val())) {
            if (msg != undefined && msg != "" && msg != null)
                msg += "<br />Please enter valid Gizmo User";
            else
                msg = "Please enter valid Gizmo User";
            $('#txtUserId').val('');
        }

        if (msg != "") {
            $('#alertmsg').html(msg);
            $('#alertmsg').css('text-align', 'left');
            $('#popupEmailRecipient').popup('close');
            $("#popupDialog").popup('open');
            $("#okBut").bind('click', function () {
                $('#popupDialog').popup('close');
                $("#okBut").unbind('click');
                $('#okBut').bind('click', function () {
                    $('#popupDialog').popup('close');
                });
                window.setTimeout(function setDelay() {
                    $('#popupEmailRecipient').popup('open');
                }, 300);
            });
            return false;
        }
        return true;

    };

    self.getData = function (id) {
        $('#hdnSelectedGroup').val(id);
        //if (jobCustomerNumber == AAG) {
        //    var context_data = ko.dataFor($('#' + id.replace(/ /g, '_'))[0]);
        //    var page_index = context_data.selectedPageValue();
        //    page_index = (page_index != undefined && page_index != null && page_index != "") ? page_index : 0;
        //    self.getSelectedEmailsList(id, context_data, context_data.caption, page_index);
        //}
    };

    self.moreClick = function (el, e) {
        //if (jobCustomerNumber == MOTIV8) {
        //    if (e.originalEvent) {
        //        var ctrl_id = (e.targetElement || e.currentTarget || e.target).id;
        //        var ctrl = $('#' + ctrl_id);
        //        var context_data = ko.contextFor(ctrl[0]).$data;

        //        context_data.selectedPageValue(context_data.selectedPageValue() + 1)

        //        self.getSelectedEmailsList(ctrl_id, context_data, context_data.caption(), (context_data.selectedPageValue()));
        //        //window.setTimeout(function setUI() {
        //        $("#" + ctrl_id.replace('btn', '')).find('ul').trigger("create").listview().listview('refresh');
        //        $("div[id='dv" + context_data.caption().replace('ddl', '') + "']").find('ul').listview('refresh').trigger('create');
        //        $("div[id='dv" + context_data.caption().replace('ddl', '') + "']").find('ul').find('select').selectmenu('refresh').trigger('create');
        //        $("div[id='dv" + context_data.caption().replace('ddl', '') + "']").find('ul').find("input[data-type=search]").trigger('create');
        //        //}, 40);
        //    }
        //}
    };

    self.pageChanged = function (el, e) {
        //if (jobCustomerNumber == MOTIV8) {
        //    if (e.originalEvent) {
        //        var ctrl_id = (e.targetElement || e.target || e.currentTarget).id;
        //        var ctrl = $('#' + e.target.id);
        //        var context_data = ko.contextFor(ctrl[0]).$data;
        //        self.getSelectedEmailsList(ctrl_id, context_data, context_data.caption(), ctrl.val());
        //        $("div[id='dv" + context_data.caption().replace('ddl', '') + "']").find('ul').listview('refresh').trigger('create');
        //        $("div[id='dv" + context_data.caption().replace('ddl', '') + "']").find('ul').find('select').selectmenu('refresh').trigger('create');
        //        $("div[id='dv" + context_data.caption().replace('ddl', '') + "']").find('ul').find("input[data-type=search]").trigger('create');
        //    }
        //}
    };

    self.searchFilterClick = function (el, e) {
        var selected_target = $('#hdnSelectedGroup').val();
        var page_index = 0;
        var context_data = "";
        var ctrl_id = 'dv' + selected_target.replace(/ /g, '_');
        if (selected_target != "") {
            //context_data = ko.dataFor($('#dv' + selected_target.replace(/ /g, '_'))[0]);
            context_data = ko.dataFor($('#' + selected_target.replace(/ /g, '_'))[0]);
            var selected_option = '';
            if (context_data.selectedFilterOption() != "")
                selected_option = context_data.selectedFilterOption();
            else {
                selected_option = "emailAddress";
                context_data.selectedFilterOption(selected_option);
            }
        }
        $('input:radio[name="rdoFilterList"]').filter('[value="' + context_data.selectedFilterOption() + '"]').parent().find("label[for].ui-btn").click();
        //$("input[name=rdoFilterList]").val(context_data.selectedFilterOption());
        //$('#rdo' + selected_option).attr('checked', true).checkboxradio('refresh');
        $('#popupSearchFilter').popup('open');
    };

    self.setFilter = function (el, e) {
        var selected_target = $('#hdnSelectedGroup').val();
        var page_index = 0;
        var context_data = "";
        var ctrl_id = 'dv' + selected_target.replace(/ /g, '_');
        if (selected_target != "") {
            //context_data = ko.dataFor($('#dv' + selected_target.replace(/ /g, '_'))[0]);
            context_data = ko.dataFor($('#' + selected_target.replace(/ /g, '_'))[0]);
            var selected_option = $("input[name=rdoFilterList]:checked").val();
            context_data.selectedFilterOption(selected_option);
            if ($("#txt" + selected_target.replace('dv', '')).val().toLowerCase() == $("#txt" + selected_target.replace('dv', '')).attr('placeholder').toLowerCase())
                $("#txt" + selected_target.replace('dv', '')).val('');

            switch (selected_option) {
                case "firstName":
                    $("#txt" + selected_target.replace('dv', '')).attr("placeholder", "Search by First Name");
                    break;
                case "lastName":
                    $("#txt" + selected_target.replace('dv', '')).attr("placeholder", "Search by Last Name");
                    break;
                case "emailAddress":
                    $("#txt" + selected_target.replace('dv', '')).attr("placeholder", "Search by Email address");
                    break;
            }
            //window.setTimeout(function setPlaceHo)
            createPlaceHolderforIE();
        }
        $('#popupSearchFilter').popup('close');
    }

    self.getSelectedEmailsList = function (id, context_data, caption, page_index) {
        var emails_list_service_url = serviceURLDomain + "api/GizmoEmailCustomer_find/" + jobCustomerNumber + "/" + sessionStorage.facilityId + "/" + sessionStorage.jobNumber + "/" + context_data.caption().toLowerCase()

        var post_json = {
            "pageIndex": (page_index != undefined && page_index != null && page_index != "") ? page_index : 0,
        };
        var search_term = context_data.selectedFilterOption();
        post_json[search_term] = "";

        post_json[search_term] = (context_data.searchValue() != "" && $('#' + id).find('input[ data-type="search"]').val() != $('#' + id).find('input[ data-type="search"]').attr('placeholder')) ? context_data.searchValue() : "";

        postCORS(emails_list_service_url, JSON.stringify(post_json), function (response_data) {
            context_data.emails.removeAll();
            $.each(response_data.emails, function (key, val) {
                context_data.emails.push(new emailRecipient(val, context_data.type()))
            });
            context_data.pagingData(createPagerObject1(response_data.totalPages, 0, 0));
            context_data.prevSearchValue(context_data.searchValue());
            var selected_page_value = (context_data.selectedPageValue() != undefined && context_data.selectedPageValue() != null && context_data.selectedPageValue() != "") ? context_data.selectedPageValue() : 0;
            var is_more_visible = (parseInt(selected_page_value + 1) < parseInt(response_data.totalPages)) ? true : false;
            context_data.showMoreOption(is_more_visible);
            $("#" + context_data.caption()).find('ul').trigger("create").listview().listview('refresh');
            window.setTimeout(function setUI() {
                $("div[id='dv" + context_data.caption() + "']").find('ul').listview('refresh').trigger('create');
                $("div[id='dv" + context_data.caption() + "']").find('ul').find('select').selectmenu('refresh').trigger('create');
                $("div[id='dv" + context_data.caption() + "']").find('ul').find("input[data-type=search]").trigger('create');
                createPlaceHolderforIE();
            }, 5);
        }, function (error_response) {
            showErrorResponseText(error_response, false);
        });
    };

    self.search = function (data, event) {
        //if (jobCustomerNumber == MOTIV8) {
        //    if (!self.searchValidation()) return false;
        //    var selected_target = $('#hdnSelectedGroup').val();
        //    var page_index = 0;
        //    var context_data = "";
        //    //var ctrl_id = 'dv' + selected_target.replace(/ /g, '_');
        //    var ctrl_id = selected_target.replace(/ /g, '_');
        //    if (selected_target != "") {
        //        //context_data = ko.dataFor($('#dv' + selected_target.replace(/ /g, '_'))[0]);
        //        context_data = ko.dataFor($('#' + selected_target.replace(/ /g, '_'))[0]);
        //        page_index = context_data.selectedPageValue();
        //        page_index = (page_index != undefined && page_index != null && page_index != "") ? page_index : 0;
        //        page_index = (context_data.prevSearchValue().toLowerCase().indexOf(context_data.searchValue().toLowerCase()) == -1) ? 0 : parseInt(page_index);
        //        if (event != undefined)
        //            context_data.selectedPageValue(0);
        //        self.getSelectedEmailsList(ctrl_id, context_data, context_data.caption(), page_index);
        //        //(ctrl, context_data, title, service_url, index_list_name, page_index, sort_order, search_term)
        //    }
        //}
    };

    self.searchWithEnterKey = function (data, event) {
        try {
            if (event.which == 13) {
                //ko.contextFor($('#dv' + $('#hdnSelectedGroup').val().replace(/ /g, '_'))[0]).$data.searchValue(event.target.value);
                ko.contextFor($('#' + $('#hdnSelectedGroup').val().replace(/ /g, '_'))[0]).$data.searchValue(event.target.value);
                self.search(data, event);
                return false;
            }
            return true;
        }
        catch (e) {
        }
    };

    self.ValidateEmailConfirm = function () {
        var msg = "";
        var regEmail = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        var regxSubject = /^[ A-Za-z0-9_./#&-]*$/;
        if (($("#txtSubject").val().trim() == $("#txtSubject").attr('placeholder')) || $("#txtSubject").val().trim() == "") {
            if (msg != undefined && msg != "" && msg != null)
                msg += "<br />Please enter Subject.";
            else
                msg = "Please enter Subject.";
            $("#txtSubject").val('');
        }
        else if (($("#txtSubject").val().trim() != $("#txtSubject").attr('placeholder')) && $("#txtSubject").val().trim() != "" && !regxSubject.test($("#txtSubject").val())) {
            if (msg != undefined && msg != "" && msg != null)
                msg += "<br />Please enter valid Subject.";
            else
                msg = "Please enter valid Subject.";
            $("#txtSubject").val('');
        }
        if ($("#txtFromEmail").val() == undefined || $("#txtFromEmail").val() == null || ($("#txtFromEmail").val().trim() == $("#txtFromEmail").attr('placeholder')) || $("#txtFromEmail").val().trim() == "") {
            if (msg != undefined && msg != "" && msg != null)
                msg += "<br />Please enter From Email address.";
            else
                msg = "Please enter From Email address.";
            $("#txtFromEmail").val('');
        }

        else if (($("#txtFromEmail").val().trim() != $("#txtFromEmail").attr('placeholder')) && $("#txtFromEmail").val().trim() != "" && !regEmail.test($("#txtFromEmail").val())) {
            if (msg != undefined && msg != "" && msg != null)
                msg += "<br />Please enter valid From Email address.";
            else
                msg = "Please enter valid From Email address.";
            $("#txtFromEmail").val('');
        }
        if (msg != "") {
            $('#alertmsg').html(msg);
            $('#alertmsg').css('text-align', 'left');
            $('#popupSendEmailConfirm').popup('close');
            $("#popupDialog").popup('open');
            $("#okBut").bind('click', function () {
                $('#popupDialog').popup('close');
                $("#okBut").unbind('click');
                $('#okBut').bind('click', function () {
                    $('#popupDialog').popup('close');
                });
                window.setTimeout(function setDelay() {
                    $('#popupSendEmailConfirm').popup('open');
                }, 300);
            });
            return false;
        }
        return true;
    };

    self.searchValidation = function () {
        var context_data = "";
        var regxSearch;
        var selected_target = $('#hdnSelectedGroup').val();
        //context_data = ko.dataFor($('#dv' + selected_target.replace(/ /g, '_'))[0]);
        context_data = ko.dataFor($('#' + selected_target.replace(/ /g, '_'))[0]);
        var search_value = context_data.searchValue().toLowerCase();
        var selected_option = $("input[name=rdoFilterList]:checked").val();
        switch (selected_option) {
            case "firstName":
            case "lastName":
                regxSearch = /^[a-zA-Z ]*$/;
                break;
            case "emailAddress":
                regxSearch = /^[ A-Za-z0-9@_.-]*$/;
                break;
        }
        if (search_value != "" && !regxSearch.test(search_value)) {
            $('#alertmsg').html("Please enter valid search text");
            $('#alertmsg').css('text-align', 'left');
            $("#popupDialog").popup('open');
            return false;
        }
        return true;
    };
};

String.prototype.initCap = function () {
    return this.replace(/(?:^|\s)[a-z]/g, function (m) {
        return m.toUpperCase();
    });
};

String.prototype.endsWith = function (suffix) {
    return this.indexOf(suffix, this.length - suffix.length) !== -1;
};
