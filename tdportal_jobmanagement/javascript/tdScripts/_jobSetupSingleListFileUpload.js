﻿//******************** Global Variables Start **************************
var filesToUpload = [];
var pagePrefs = "";
var statesJson = [];
var selectedStates = [];
//******************** Global Variables End **************************

//******************** Page Load Events Start **************************

$('#_jobSetupUploadList').live('pagebeforecreate', function (event) {
    displayNavLinks();
    displayMessage('_jobSetupUploadList');
    displayValidationMessage('_jobSetupUploadList');
    confirmMessage();
    loadStatesToJson();
    //load IFRAME for IE to upload files.
    loadUploadIFrame();
    //test for mobility...
    loadMobility();
    displayListFileInfo();
});

$(window).on('popupbeforeposition', 'div:jqmData(role="popup")', function () {
    var notDismissible = $(this).jqmData('dismissible') === false;
    if (notDismissible) {
        $('.ui-popup-screen').off();
    }
});

$(document).on('pageshow', '#_jobSetupUploadList', function (event) {
    (($.browser.msie) ? $(window.frames[0].frames.frameElement).contents().find('input[type=file]') : $('input[type=file]')).bind('change', function (e) {
        navigate($(e.target))
    });
    //fillStates(0);
});

//******************** Page Load Events End **************************

//******************** Public Functions Start **************************

//function fillStates(ctrl_cnt) {
//    $.getJSON(gStateServiceUrl, function (dataStates) {
//        $.each(dataStates, function (key, val) {
//            $("#ddlStates" + ctrl_cnt).append('<option value="' + val.stateValue + '" ' + val.selected + '>' + val.stateName + '</option>');
//        });
//        $.each($('div select[id^=ddlStates]'), function (a) {
//            $(this).selectmenu();
//        });
//        $('#dvStates').css("display", "block");
//        //$("#ddlStates" + ctrl_cnt).selectmenu('refresh');
//    });
//}
function loadStatesToJson() {
    $.getJSON(gStateServiceUrl, function (dataStates) {
        statesJson = dataStates;
    });
}

function fillStates(ctrl, template_to_bind, json_to_bind) {
    $('#' + ctrl).empty();
    $("#" + template_to_bind).tmpl(json_to_bind).appendTo("#" + ctrl);
    $('#' + ctrl + ' li[id=li]').remove();
    if (ctrl.toLowerCase().indexOf('selected') == -1 && selectedStates.length > 0) {
        $.each(selectedStates, function (a, b) {
            $('#' + ctrl).find('li[id=li' + b.stateValue + ']').remove();
        });
    }
    $('#' + ctrl).listview('refresh');
    $('#dvStates').css("display", "block");
}

function addStates(state_value, state_name, ctrl) {
    selectedStates.push({ "stateName": state_name, "stateValue": state_value });
    $('#' + ctrl).find('li[id=li' + state_value + ']').remove();
    fillStates('ulSelectedStates', 'selectedStatesTemplate', selectedStates);
    $('#ulSelectedStates').prepend('<li data-role="list-divider" >Selected States</li>');
    $('#ulSelectedStates').listview('refresh');
}

function removeStates(state_value) {
    var count = 0;
    $.each(selectedStates, function (a, b) {
        if (b.stateValue.toLowerCase() == state_value.toLowerCase())
            return false;
        else
            count++
    });
    selectedStates.splice(count, 1);
    fillStates('ulSelectedStates', 'selectedStatesTemplate', selectedStates);
    $('#ulSelectedStates').prepend('<li data-role="list-divider" >Selected States</li>');
    $('#ulSelectedStates').listview('refresh');
    fillStates('ulStates', 'statesTemplate', statesJson);
}

function createStatesDDL(count) {
    var total_ddls = $('div select[id^=ddlStates]').length;
    var cnt = count - 1;
    var is_valid = 1;
    var changing_value = $('#ddlStates' + cnt).val();
    $.each($('div select[id^=ddlStates]'), function (a, b) {
        if (cnt != a && changing_value == b.value && changing_value != "") {
            is_valid = 0;
            $('#ddlStates' + cnt).val("");
            $('#validationAlertmsg').text('The State is already selected. Please choose anothor one.');
            $('#popupListDefinition').popup('close');
            window.setTimeout(function getDelay() {
                $('#validationPopupDialog').popup('open');
                return false;
            }, 200);
        }
    });
    if (is_valid == 1) {
        if (total_ddls >= 10) {
            $('#validationAlertmsg').text('The maximum number of States can be selected 10 only.');
            $('#popupListDefinition').popup('close');
            window.setTimeout(function getDelay() {
                $('#validationPopupDialog').popup('open');
                return false;
            }, 200);
        }
        else if (total_ddls == count) {
            var selected_ctrl = $('div select[id^=ddlStates]')[total_ddls - 1];
            var new_ctrl = selected_ctrl.cloneNode(true);
            new_ctrl.id = 'ddlStates' + count;
            $(new_ctrl).attr('onchange', "createStatesDDL(" + (parseInt(count) + 1) + ")");
            //$(new_ctrl).find('option[value=' + $(selected_ctrl).val() + ']').remove();
            $('#dvStates').append(new_ctrl);
            $.each($('div select[id^=ddlStates]'), function (a) {
                $(this).selectmenu();
            });
        }
    }
}

function removeDropStateDDLs() {
    var drop_states_count = $('#dvStates div[class=ui-select]').length;
    if (drop_states_count > 1) {
        var counter = 1;
        $.each($('#dvStates div[class=ui-select]'), function (a) {
            if (counter > 1)
                $(this).remove();
            else {
                $(this).find('select[id=ddlStates' + (counter - 1) + ']').val('').selectmenu('refresh');
            }
            counter++;
        });
    }
    else {
        $('#ddlStates0').val('').selectmenu('refresh');
    }
}

function displayListFileInfo() {
    if (sessionStorage.jobSetupOutput == undefined && sessionStorage.jobSetupOutput == undefined) {
        makeGOutputData();
    }
    else {
        if (sessionStorage.jobSetupOutput != undefined) {
            gOutputData = jQuery.parseJSON(sessionStorage.jobSetupOutput);
            var file_data;
            file_data = (gOutputData != undefined && gOutputData.standardizeAction.standardizeFileList != undefined) ? gOutputData.standardizeAction.standardizeFileList : [];
            if (file_data.length == 0) {
                file_data.push({
                    sourceType: "",
                    fileName: "",                   
                    fileMap: "",
                    priority: "",
                    fileSizePretty: "",
                    fileStatus: "",
                    locationId: "",
                    optionalAttributesDict: { dropAllStatesExcept: "" }
                });
                if (sessionStorage.jobSetupOutputCompare != undefined) {
                    var goutput_compare_data = jQuery.parseJSON(sessionStorage.jobSetupOutputCompare);
                    if (goutput_compare_data.standardizeAction.standardizeFileList != undefined && JSON.stringify(goutput_compare_data.standardizeAction.standardizeFileList).length == 2) {
                        goutput_compare_data.standardizeAction["standardizeFileList"] = file_data;
                        sessionStorage.jobSetupOutputCompare = JSON.stringify(goutput_compare_data);
                    }
                }
                if (gOutputData == undefined) {
                    gOutputData = {};
                    gOutputData["customerNumber"] = "1234";
                    gOutputData["jobName"] = "Test Dataworks Job.";
                    gOutputData["userName"] = "jim";
                    gOutputData["jobNumber"] = 56195;
                    gOutputData["userEmail"] = "jim.wright@tribunedirect.com";
                    gOutputData["jobTypeId"] = 1;
                    gOutputData["nodeName"] = "";
                }
                if (gOutputData["standardizeAction"] == undefined) {
                    gOutputData["standardizeAction"] = {};
                    gOutputData.standardizeAction["name"] = "standardize1";
                    gOutputData.standardizeAction["type"] = "standardize";
                    gOutputData.standardizeAction["isEnabled"] = true;
                    gOutputData.standardizeAction["standardizeFileList"] = [];
                }
                gOutputData.standardizeAction.standardizeFileList = file_data;

            }
            buildOutputLists(false);
        }
    }
    if (sessionStorage.filesToUpload != undefined) {
        filesToUpload = jQuery.parseJSON(sessionStorage.filesToUpload);
        //makeFileListBeforePost(filesToUpload);
    }
    getUploadedFilesCount();
}

function makeGOutputData() {
    var file_data;
    file_data = (gOutputData != undefined && gOutputData.standardizeAction.standardizeFileList != undefined) ? gOutputData.standardizeAction.standardizeFileList : [];

    if (file_data.length == 0) {
        file_data.push({
            sourceType: "",
            fileName: "",           
            fileMap: "",
            priority: "",
            fileSizePretty: "",
            fileStatus: "",
            locationId: "",
            optionalAttributesDict: { dropAllStatesExcept: "" }
        });
        if (sessionStorage.jobSetupOutputCompare != undefined) {
            var goutput_compare_data = jQuery.parseJSON(sessionStorage.jobSetupOutputCompare);
            if (goutput_compare_data.standardizeAction.standardizeFileList != undefined && JSON.stringify(goutput_compare_data.standardizeAction.standardizeFileList).length == 2) {
                goutput_compare_data.standardizeAction["standardizeFileList"] = file_data;
                //goutput_compare_data.milestoneAction["standardizeFileList"] = (gData.milestoneDict != undefined) ? gData.milestoneDict : [];
                sessionStorage.jobSetupOutputCompare = JSON.stringify(goutput_compare_data);
            }
        }
        if (gOutputData == undefined) {
            gOutputData = {};
            gOutputData["customerNumber"] = "1234";
            gOutputData["jobName"] = "Test Dataworks Job.";
            gOutputData["userName"] = "jim";
            gOutputData["jobNumber"] = 56195;
            gOutputData["userEmail"] = "jim.wright@tribunedirect.com";
            gOutputData["jobTypeId"] = 1;
            gOutputData["nodeName"] = "";
        }
        if (gOutputData["standardizeAction"] == undefined) {
            gOutputData["standardizeAction"] = {};
            gOutputData.standardizeAction["name"] = "standardize1";
            gOutputData.standardizeAction["type"] = "standardize";
            gOutputData.standardizeAction["isEnabled"] = true;
            gOutputData.standardizeAction["standardizeFileList"] = [];
        }
        gOutputData.standardizeAction.standardizeFileList = file_data;
    }
    buildOutputLists(false);
}

function buildOutputLists(is_artwork) {
    var file_data;
    file_data = (gOutputData != undefined && gOutputData.standardizeAction.standardizeFileList != undefined) ? gOutputData.standardizeAction.standardizeFileList : [];
    if (file_data.length > 0) {
        makeFileList(file_data);
    }
    sessionStorage.jobSetupOutput = JSON.stringify(gOutputData);
}


function makeFileList(array_name) {
    var array = eval(array_name);
    var list_info = "";
    var mail_files = "";
    var mail_files_cnt = 0;
    var dnm_files = "";
    var dnm_files_cnt = 0;
    var seed_files = "";
    var seed_files_cnt = 0;
    var source_type = "";
    $("#uploadList").empty();

    $("#btnMapSelected").hide();
    $("#btnRemoveSelected").hide();

    $.each(array_name, function (key, val) {
        if (val.fileName != "") {
            source_type = getFileType(val.sourceType.toString());

            //TO BE DISCUSSED.
            var file_mapping_url = 'fileMappingMapper.html?ft=' + source_type + '&fn=' + val.fileName;
            var file_name = val.fileName.substring(0, val.fileName.lastIndexOf('.') + 1) + val.fileName.substring(val.fileName.lastIndexOf('.') + 1).toLowerCase().replace('gz', 'txt');

            //TO BE DISCUSSED ABOUT NAVIGATING TO FILE MAPPING PAGE.
            //var file_list = '<li data-theme="c"><a  data-rel="popup" onclick="fnpopUp(\'' + source_type.toLowerCase() + '\');"><h3>' + file_name + '</h3><p>Delimeter: ' + val.format.substring(0, 1).toUpperCase() + val.format.substring(1);

            var file_list = '<li data-theme="c"><a data-rel="popup" onclick="fnpopUp(\'' + source_type.toLowerCase() + '\',event,\'' + file_name + '\');"><h3>' + file_name + '</h3>';

            //TO BE DISCUSSED.
            var drop_all_states_except = "";
            if (val.optionalAttributesDict != undefined && val.optionalAttributesDict != null) {
                drop_all_states_except = (customerNumber == CW && val.optionalAttributesDict.dropAllStatesExcept != undefined && val.optionalAttributesDict.dropAllStatesExcept != null && val.optionalAttributesDict.dropAllStatesExcept != "") ? ' | Drop All States Except: ' + val.optionalAttributesDict.dropAllStatesExcept : '';
                var regex = /,$/;
                drop_all_states_except = (drop_all_states_except != "") ? ((regex.test(drop_all_states_except)) ? drop_all_states_except.substring(0, drop_all_states_except.length - 1) : drop_all_states_except) : "";
            }

            var map_link = (appPrivileges.roleName == "admin") ? "<p> | <a href='#' onclick='navigateMapper(\"" + file_mapping_url + "\",event );'>Map</a></p>" : '';
            var map_label = (appPrivileges.roleName == "admin") ? " | File Map: " + val.fileMap : '';

            var file_info = '<table border="0" style="border:none;"><tr><td><p>File Size: ' + val.fileSizePretty + drop_all_states_except + ' | File Status:' + val.fileStatus + '</p></td><td>' + map_link + '</td></tr></table></a>';
            file_list += file_info + '<a href="#" onclick="return removeAFile(\'' + val.fileName + '\',\'' + source_type + '\',\'' + getFileTypeCode(source_type) + '\',\'' + val.locationId + '\');">Remove List</a></li>';
            //file_list += ' | File Map: ' + val.fileMap + '  | File Size: ' + val.fileSizePretty + ' | File Status:' + val.fileStatus + '</p><a href="#" onclick="return removeAFile(\'' + val.fileName + '\',\'' + source_type + '\',\'' + getFileTypeCode(source_type) + '\',\'' + val.locationId + '\');">Remove List</a></li>';

            if (source_type.toLowerCase() == "source") {
                mail_files += file_list;
                mail_files_cnt++;
            }
            else if (source_type.toLowerCase() == "seed") {
                seed_files += file_list;
                seed_files_cnt++;
            }
            else if (source_type.toLowerCase() == "donotmail") {
                dnm_files += file_list;
                dnm_files_cnt++;
            }

            $("#btnMapSelected").show();
            $("#btnRemoveSelected").show();
            $('#tblFiles').show();
        }
        else {
            $("#lnkUploadFile").show();
        }
    });

    //is_file_map_displayed = ((sessionStorage.userRole == "user" || sessionStorage.userRole == "power") && !(appPrivileges.display_uploadFileMap != undefined && appPrivileges.display_uploadFileMap)) ? '<th>File Size</th><th>File Status</th></theader>' : '<th>File Map</th><th>File Size</th><th>File Status</th><th>Map</th></theader>'; //getting info from authorization web service
    mail_grid_info = '<div id="dvSourceFiles" data-role="collapsible" data-theme="b" data-collapsed-icon="arrow-r" data-expanded-icon="arrow-d" data-collapsed="false"><h2>Mail File<span class="ui-li-has-count ui-li-count ui-btn-up-c ui-btn-corner-all" style="right:15px;float:right">' + mail_files_cnt + '</span></h2>';
    mail_grid_info += '<ul id="ulSourceFiles" data-role="listview" data-theme="d" data-divider-theme="d" data-split-icon="delete" data-split-theme="d" >';
    if (mail_files != "")
        list_info += mail_grid_info + mail_files;
    else
        list_info += mail_grid_info + "<li data-icon='myapp-upload'><a onclick='fnpopUp(\"source\",event,\"\")' data-rel='popup'>Click Here to Upload File</a></li>";

    list_info += '</ul></div>';
    if (sessionStorage.userRole == "admin") {
        seed_grid_info = '<div id="dvSeedFiles" data-role="collapsible" data-theme="b" data-collapsed-icon="arrow-r" data-expanded-icon="arrow-d" data-collapsed="true"><h2>Seed File<span class="ui-li-has-count ui-li-count ui-btn-up-c ui-btn-corner-all" style="right:15px;float:right">' + seed_files_cnt + '</span></h2>';
        seed_grid_info += '<ul id="ulSeedFiles" data-role="listview" data-theme="d" data-divider-theme="d" data-split-icon="delete" data-split-theme="d" >';
        if (seed_files != "")
            list_info += seed_grid_info + seed_files;
        else
            list_info += seed_grid_info + "<li data-icon='myapp-upload'><a onclick='fnpopUp(\"seed\",event,\"\")'  data-rel='popup'>Click Here to Upload File</a></li>";
        list_info += '</ul></div>';

        dnm_grid_info = '<div id="dvDoNotMailFiles" data-role="collapsible" data-theme="b" data-collapsed-icon="arrow-r" data-expanded-icon="arrow-d" data-collapsed="true"><h2>Do Not Mail File<span class="ui-li-has-count ui-li-count ui-btn-up-c ui-btn-corner-all" style="right:15px;float:right">' + dnm_files_cnt + '</span></h2>';
        dnm_grid_info += '<ul id="ulDoNotMailFiles" data-role="listview" data-theme="d" data-divider-theme="d" data-split-icon="delete" data-split-theme="d" >';
        if (dnm_files != "")
            list_info += dnm_grid_info + dnm_files;
        else
            list_info += dnm_grid_info + "<li data-icon='myapp-upload'><a onclick='fnpopUp(\"donotmail\",event,\"\")'  data-rel='popup'>Click Here to Upload File</a></li>";
        list_info += '</ul></div>';
    }
    if (list_info != "") {
        $("#uploadList").empty();
        $("#uploadList").append(list_info);
        $('div.ui-collapsible-content', "#dvSourceFiles").trigger('expand');
    }
    if (appPrivileges.roleName != "admin") $('div:[id^=dv]').find('.ui-li-has-count').hide();
    //    if (gOutputData != null) {
    //        getUploadedFilesCount();
    //    }
}

function fnpopUp(file_type, event, old_file_name) {
    var job_ticket_info = $.extend(true, {}, gOutputData);
    var job_checkout_info = {};
    job_checkout_info["selectedLocationsList"] = job_ticket_info.selectedLocationsAction.selectedLocationsList;
    job_checkout_info["isFirstClassMail"] = (job_ticket_info.mailstreamAction.mailClass != undefined && job_ticket_info.mailstreamAction.mailClass == "1") ? 1 : 0;
    job_checkout_info["jobTemplate"] = (job_ticket_info.templateName != undefined) ? job_ticket_info.templateName : ((job_ticket_info.template != undefined) ? job_ticket_info.template : '');
    job_checkout_info["mailstreamAction"] = job_ticket_info.mailstreamAction;
    postCORS(serviceURLDomain + "api/CheckOut/" + facilityId + '/' + appPrivileges.customerNumber + '/' + jobNumber + '/asdf', JSON.stringify(job_checkout_info), function (response) {
        if (response.checkOutAction != undefined && response.checkOutAction.checkOutApproval != undefined && response.checkOutAction.checkOutApproval.isCountsApproved != undefined
                    && response.checkOutAction.checkOutApproval.isCountsApproved.toLowerCase() == "true") {
            $('#alertmsg').html("The data has already been approved. To add or remove data the approval must be cleared in the Approval/Checkout screen. Please send an email to <a href='mailto:CA-CampingTeam@conversionalliance.com'>CA-CampingTeam@conversionalliance.com</a>");
            $("#popupDialog").popup("open");
            return false;
        } else {
            //Else part
            selectedStates = [];
            resetFileUploadControl(filesToUpload);
            resetFields();
            fillStates('ulStates', 'statesTemplate', statesJson);
            uploadingFiles = "";
            if (((event.srcElement != undefined) ? event.srcElement : event.target).innerHTML.toLowerCase() != 'map') {
                $('#hidEditUploadFile').val((old_file_name != '') ? old_file_name : '');
                $('#name').val('');
                //$('#select-choice-1').val('select').selectmenu('refresh');
                //$('#select-choice-1').val('fixed').selectmenu('refresh');
                //if (appPrivileges.roleName == "admin") {
                    //$('#dvDDlDelimiter').css("display", "block");
                    //$('#dvLblDelimiter').css("display", "none");
                   // $('#select-choice-1').val('fixed').selectmenu('refresh');
               // }
               // else {
                    //$('#dvDDlDelimiter').css("display", "none");
                    //$('#dvLblDelimiter').css("display", "none");
               // }

                $('#hidUploadFileType').val(file_type);
                $("#popupListDefinition").popup("open");
                if (file_type == 'source')
                    $("#popupListDefinition h3[id=popupHeader]").html('Select Mail File');
                else if (file_type == 'seed')
                    $("#popupListDefinition h3[id=popupHeader]").html('Select Seed File');
                else
                    $("#popupListDefinition h3[id=popupHeader]").html('Select Do Not Mail File');
            }
        }
    }, function (error_response) {
        showErrorResponseText(error_response);
    });
}

function navigateMapper(url, event) {
    event.preventDefault();
    $("#popupListDefinition").popup("close");
    $('#divError').text('');
    sessionStorage.continueToOrder = window.location.href.substring(window.location.href.lastIndexOf('/') + 1);
    window.location.href = url;
}

function updateFileGrid() {
    var delimiter = ""//(appPrivileges.roleName == "admin") ? $('#select-choice-1').val() : $('#lblDelimiter').text();
    addFileInfo($('#hidUploadFileType').val(), $('#name').val(), delimiter);
    //reset the fields...
    resetFields();
    if (appPrivileges.roleName == "admin")
        $('#select-choice-1').get(0).selectedIndex = 0;
    var upload_form = ($.browser.msie) ? window.frames[0].document.getElementById('spnFileInput') : document.getElementById('spnFileInput');
    var file_ctrls = $(upload_form).find('input[type=file]');
    if (file_ctrls.length > 0) {
        var newFileInput = file_ctrls[0].cloneNode(true);
        newFileInput.value = null;
        newFileInput.style.display = "";
        newFileInput.id = "mailFiles[]";
        $((($.browser.msie) ? window.frames[0].document.getElementById('spnFileInput') : document.getElementById('spnFileInput'))).find('input[type=file]').remove();
        $((($.browser.msie) ? window.frames[0].document.getElementById('spnFileInput') : document.getElementById('spnFileInput'))).append(newFileInput);
        (($.browser.msie) ? $(window.frames[0].frames.frameElement).contents().find('input[type=file]') : $('input[type=file]')).bind('change', function (e) {
            navigate($(e.target))
        });
        fileCount = 0;
    }
    filesToUpload = [];
    $("#divFilesToUpload").hide();
    sessionStorage.removeItem("filesToUpload");
}

//Browse to specific folder
function navigate(ctrl) {
    var file_name = $(ctrl).val();
    var file_type = $('#hidUploadFileType').val();
    var list_file_type = "";
    if ($('#hidUploadFileType').val() == 'source' || $('#hidUploadFileType').val() == 'mail')
        list_file_type = 'source';
    else if ($('#hidUploadFileType').val() == 'donotmail')
        list_file_type = 'donotmail';
    else
        list_file_type = 'seed';
    var check_file_name = ($(ctrl).val().indexOf('\\') > -1) ? $(ctrl).val().substring($(ctrl).val().lastIndexOf('\\') + 1, $(ctrl).val().length) : $(ctrl).val();
    var myRegexp = /^([a-zA-Z0-9_\.\-\^\ ])+$/ig;
    if (!myRegexp.test(check_file_name.substring(0, check_file_name.lastIndexOf('.')))) {
        resetFileUploadControl(filesToUpload);
        fileCount = (fileCount <= 0) ? fileCount = 0 : fileCount - 1;
        $('#popupListDefinition').popup('close');
        $('#alertmsg').text('Illegal character in file name. Uploading cannot continue.');
        $('#popupDialog').popup('open');
        return false;
    }
    else {
        var extension = file_name.split('.');
        var extension = file_name.substring(file_name.lastIndexOf('.') + 1);
        var allowedExtension = ["txt", "csv", "dat", "data", "lst", "xls", "xlsx", "doc", "docx", "gz"];
        var isValidFileFormat = false;
        var uploading_file_name = "";
        if (typeof (extension) != 'undefined' && extension.length > 1) {
            for (var i = 0; i < allowedExtension.length; i++) {
                //if (extension[1].toLowerCase().indexOf(allowedExtension[i]) != -1) {
                if (extension.toLowerCase() == allowedExtension[i]) {
                    isValidFileFormat = true;
                    //var file_type = (sessionStorage.userRole == "user" || sessionStorage.userRole == "power") ? 'source' : $("#fieldType").val();
                    var file_type = (sessionStorage.userRole == "user" || sessionStorage.userRole == "power") ? 'source' : list_file_type;
                    $('#hidUploadFile').val(file_name + '|' + file_type);
                    //var filemappername = extension[0] + "_map.xml";
                    var filemappername = file_name.substring(file_name.lastIndexOf('\\') + 1, file_name.lastIndexOf('.')) + "_map.xml";
                    $('#name').val(file_name.substring(file_name.lastIndexOf("\\") + 1));
                    //populate the file map field
                    $('#txtFileMap').val(filemappername);
                    if ($('#hidMode').val().toLowerCase() == "edit") {
                        $('#testButtonOk').hide();
                        $('#btnUpdate').show();
                    }
                    else {
                        $('#testButtonOk').show();
                        $('#btnUpdate').hide();
                    }
                    updateFileList();
                    break;
                }
            }
            if (!isValidFileFormat) {
                resetFileUploadControl(filesToUpload);
                fileCount = (fileCount <= 0) ? fileCount = 0 : fileCount - 1;
                $('#popupListDefinition').popup('close');
                $('#alertmsg').text('Invalid File Type...Only "TXT","CSV","DAT","DATA","LST","XLS","XLSX","DOC", "GZ" and "DOCX" types are accepted.');
                $('#popupDialog').popup('open');
                return true;
            }
        }

    }

}
function resetFields() {
    $('#name').val("");
    //$('#select-choice-1').val('select').selectmenu('refresh');
    //if (appPrivileges.userRole == "admin") {
    //    $('#dvDDlDelimiter').css('display', 'block');
    //    $('#dvLblDelimiter').css('display', 'none');
    //    $('#select-choice-1').val('fixed').selectmenu('refresh');
    //}
    //else {
    //    $('#dvDDlDelimiter').css('display', 'none');
    //    $('#dvLblDelimiter').css('display', 'none');
    //}
    //removeDropStateDDLs();
    $('#ulStates').empty();
    $('#ulSelectedStates').empty();
}

function addFileInfo(source_type, filename, delimiter) {
    var file_name = filename;
    var source_type_code = getFileTypeCode(source_type);
    file_name = file_name.substring(file_name.lastIndexOf("\\") + 1);
    file_name = file_name.substring(0, file_name.lastIndexOf('.') + 1) + file_name.substring(file_name.lastIndexOf('.') + 1).toLowerCase().replace('gz', 'txt');

    var drop_all_states_except = "";
    $.each($('#ulSelectedStates li a'), function (a, b) {
        drop_all_states_except += (drop_all_states_except != '') ? ',' + $(b).attr('value') : $(b).attr('value');
    });

    var is_file_exists = false;

    var file_info;

    file_info = (gOutputData.standardizeAction.standardizeFileList != undefined) ? gOutputData.standardizeAction.standardizeFileList : 0;

    if (file_info.length > 0) {
        var file_list = [];
        file_list = jQuery.grep(file_info, function (obj) {
            return (obj.sourceType === parseInt(source_type_code) && obj.fileName.toLowerCase() === $('#hidEditUploadFile').val().toLowerCase());
        });
        if (!isNaN(parseInt(selectedKey)) && file_list.length > 0) {
            file_info[selectedKey].fileName = file_name;
            file_info[selectedKey].sourceType = parseInt(source_type_code);
            
            if (file_info[selectedKey].optionalAttributesDict == undefined)
                file_info[selectedKey]["optionalAttributesDict"] = { "dropAllStatesExcept": "" };
            else if (file_info[selectedKey].optionalAttributesDict == null)
                file_info[selectedKey].optionalAttributesDict = { "dropAllStatesExcept": "" };
            file_info[selectedKey].optionalAttributesDict.dropAllStatesExcept = drop_all_states_except;
        }
        else {
            if (file_list.length > 0) {
                $.each(file_list, function (key1, val1) {
                    if (parseInt(source_type_code) == val1.sourceType) {
                        val1.fileName = file_name;
                        is_file_exists = true;
                        val1.fileSizePretty = "";
                        val1.fileStatus = "";                       
                        if (val1.optionalAttributesDict == undefined)
                            val1["optionalAttributesDict"] = { "dropAllStatesExcept": "" };
                        else if (val1.optionalAttributesDict == null)
                            val1.optionalAttributesDict = { "dropAllStatesExcept": "" };

                        val1.optionalAttributesDict.dropAllStatesExcept = drop_all_states_except;
                    }
                });
            }

            if (file_list.length == 0 && file_info[0].sourceType == "" && file_info[0].fileName == "") {
                file_info[0].fileName = file_name;
                file_info[0].sourceType = parseInt(source_type_code);
                if (file_info[0].optionalAttributesDict == undefined)
                    file_info[0]["optionalAttributesDict"] = { "dropAllStatesExcept": "" };
                else if (file_info[0].optionalAttributesDict == null)
                    file_info[0].optionalAttributesDict = { "dropAllStatesExcept": "" };

                file_info[0].optionalAttributesDict.dropAllStatesExcept = drop_all_states_except;
            }
            else if (!is_file_exists) {
                file_info.push({
                    sourceType: parseInt(source_type_code),
                    fileName: file_name,                    
                    fileMap: "",
                    priority: "",
                    fileSizePretty: "",
                    fileStatus: "",
                    locationId: "",
                    optionalAttributesDict: { dropAllStatesExcept: drop_all_states_except }
                });

            }
        }
    }
    selectedKey = '';
    selectedStates = [];
    sessionStorage.jobSetupOutput = JSON.stringify(gOutputData);
    buildOutputLists(false);
    $("div ul").each(function (i) {
        $(this).listview();
    });
    $("#uploadList").collapsibleset('refresh');
    $('div.ui-collapsible-content', "#dvSourceFiles").trigger('expand');
    $('#hidEditUploadFile').val('');
}

function removeAFile(file_name, source_type, source_type_code, location_id) {
    var job_ticket_info = $.extend(true, {}, gOutputData);
    var job_checkout_info = {};
    job_checkout_info["selectedLocationsList"] = job_ticket_info.selectedLocationsAction.selectedLocationsList;
    job_checkout_info["isFirstClassMail"] = (job_ticket_info.mailstreamAction.mailClass != undefined && job_ticket_info.mailstreamAction.mailClass == "1") ? 1 : 0;
    job_checkout_info["jobTemplate"] = (job_ticket_info.templateName != undefined) ? job_ticket_info.templateName : ((job_ticket_info.template != undefined) ? job_ticket_info.template : '');
    job_checkout_info["mailstreamAction"] = job_ticket_info.mailstreamAction;
    postCORS(serviceURLDomain + "api/CheckOut/" + facilityId + '/' + appPrivileges.customerNumber + '/' + jobNumber + '/asdf', JSON.stringify(job_checkout_info), function (response) {
        if (response.checkOutAction != undefined && response.checkOutAction.checkOutApproval != undefined && response.checkOutAction.checkOutApproval.isCountsApproved != undefined
                    && response.checkOutAction.checkOutApproval.isCountsApproved.toLowerCase() == "true") {
            $('#alertmsg').html("The data has already been approved. To add or remove data the approval must be cleared in the Approval/Checkout screen. Please send an email to <a href='mailto:CA-CampingTeam@conversionalliance.com'>CA-CampingTeam@conversionalliance.com</a>");
            $("#popupDialog").popup("open");
            return false;
        } else {
            //Else part
            $('#divError').text('');
            var file_delete_url = serviceURLDomain + "api/FileUpload/delete/" + facilityId + "/" + jobNumber + "/";
            var selected_link = getSessionData('advertiser_session');
            file_delete_url = file_delete_url + source_type_code + "/" + file_name;
            getCORS(file_delete_url, null, function (prod_data) {
                var file_info = (gOutputData.standardizeAction.standardizeFileList != undefined) ? gOutputData.standardizeAction.standardizeFileList : 0;
                var remove_files = [];
                $.each(file_info, function (key1, val1) {
                    if (val1.sourceType == source_type_code && val1.fileName.toLowerCase() == file_name.toLowerCase()) {
                        remove_files.push(key1);
                    }
                });

                if (remove_files.length > 0) {
                    $.each(remove_files, function (key2, val2) {
                        file_info.splice(val2, 1);
                        if (file_info.length == 0) {
                            file_info.push({
                                sourceType: "",
                                fileName: "",                              
                                fileMap: "",
                                priority: "",
                                fileSizePretty: "",
                                fileStatus: "",
                                locationId: "",
                                optionalAttributesDict: { dropAllStatesExcept: "" }
                            });
                        }
                    });
                }
                sessionStorage.jobSetupOutput = JSON.stringify(gOutputData);
                postJobSetUpData('save');
                buildOutputLists(false);
                $("div ul").each(function (i) {
                    $(this).listview();
                });
                $("#uploadList").collapsibleset('refresh');
                $('div.ui-collapsible-content', "#dvSourceFiles").trigger('expand');
                if (gOutputData != null) {
                    getUploadedFilesCount();
                }
            }, function (error_response) {
                showErrorResponseText(error_response);
            });
        }
    }, function (error_response) {
        showErrorResponseText(error_response);
    });
}

function resetFileUploadPopup() {
    $('#name').val("");
    //$('#select-choice-1').val('select').selectmenu('refresh');
    //if (appPrivileges.userRole == "admin") {
    //    $('#select-choice-1').val('fixed').selectmenu('refresh');
    //    $('#dvDDlDelimiter').css('display', 'block');
    //    $('#dvLblDelimiter').css('display', 'none');
    //}
    //else {
    //    $('#dvDDlDelimiter').css('display', 'none');
    //    $('#dvLblDelimiter').css('display', 'none');
    //}
}

function uploadValidation(validate_all) {
    var msg = "<ul>";
    //    if ($('#ddlSelectLocations') != undefined && $('#ddlSelectLocations').val() == "select")
    //        msg += "<li>Please select Location.</li>";

    //    if ($('#fieldType').val() == "select" && (sessionStorage.userRole != "user"|| sessionStorage.userRole != "power")
    //        msg += "<li>Please select List type.</li>";

    if (appPrivileges.userRole == "admin" && $('#select-choice-1').val() == "select")
        msg += "<li>Please select File Delimeter.</li>";
    if (validate_all) {
        if ($('#name').val() == "")
            msg += "<li>Please choose a file to upload.</li>";
    }
    msg += "</ul>";

    if (msg != "<ul></ul>") {
        $("#popupListDefinition").popup("close");
        $('#validationAlertmsg').html(msg);
        window.setTimeout(function getDelay() {
            $('#validationPopupDialog').popup('open');
            return false;
        }, 200);

    }
    else
        return true;
}

function updateFileList() {
    if (!uploadValidation(false)) return false;

    var file_info = $('#hidUploadFile').val();
    var file_name = file_info.split('|')[0];
    var source_type = file_info.split('|')[1];
    var source_type_code = getFileTypeCode(source_type);
    var selected_location = $('#ddlSelectLocations').val();
    var uploading_file_name = "";
    if (file_name != "") {
        uploading_file_name = file_name.substring(file_name.lastIndexOf("\\") + 1);
        uploadingFiles += (uploadingFiles != "") ? "|" + uploading_file_name + "^" + source_type_code : uploading_file_name + "^" + source_type_code;
    }
    else {
        $("#popupListDefinition").popup("close");
    }
}

function addNewFile() {
    $('#divError').text('');
    if (gOutputData == undefined)
        gOutputData = jQuery.parseJSON(sessionStorage.jobSetupOutput);
    if ((filesToUpload.length < maxNumberOfFiles) && (gOutputData.standardizeAction.standardizeFileList.length < maxNumberOfFiles) && ((filesToUpload.length + ((gOutputData.standardizeAction.standardizeFileList.length >= 1 && gOutputData.standardizeAction.standardizeFileList[0].fileName != "") ? gOutputData.standardizeAction.standardizeFileList.length : 0)) < maxNumberOfFiles)) {
        addFileInput();
        resetFields();
        showHidePopupDataOnLocationChange('uploadList');
        fillStates('ulStates', 'statesTemplate', statesJson);
        $('#testButtonOk').show();
        $(($.browser.msie) ? window.frames[0].document.getElementById('spnFileInput') : document.getElementById('spnFileInput'))[0].style.display = "";

        $('#btnUpdate').hide();
        $('#hidUploadFile').val('');
        popUpListDefinitionDisplay();
    }
    else {
        var msg = "The maximum number of files to be uploaded per order has been reached. Please remove a previously uploaded file in order to upload a new file to this order. If this order requires more than 5 files, please contact your Account Manager.";
        $('#alertmsg').text(msg);
        $('#popupDialog').popup('open');
        return false;
    }
}

function validateUploadingListFiles() {
    if (filesToUpload.length > 0) {
        var msg = "The files you have selected have not been uploaded to this order. Please click the 'Upload Selected Lists' button to upload the selected files or Remove selected files you do not want to upload.";
        $('#alertmsg').text(msg);
        $('#popupDialog').popup('open');
        return false;
    }
    else {
        return true;
    }
}

function closeDialog() {
    $('#popupDialog').popup('open');
    $('#popupDialog').popup('close');
}
//******************** Public Functions End **************************