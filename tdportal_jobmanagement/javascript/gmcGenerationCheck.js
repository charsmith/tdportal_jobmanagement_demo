﻿var isOrderLocationExists = function (page_name) {
    var is_orderlocations_exist = false;
    if (sessionStorage.jobSetupOutput != undefined && sessionStorage.jobSetupOutput != null && sessionStorage.jobSetupOutput != "") {
        gOutputData = $.parseJSON(sessionStorage.jobSetupOutput);
        is_orderlocations_exist = true;
        if (gOutputData.selectedLocationsAction == undefined || gOutputData.selectedLocationsAction == null || gOutputData.selectedLocationsAction.selectedLocationsList == undefined || gOutputData.selectedLocationsAction.selectedLocationsList == null || Object.keys(gOutputData.selectedLocationsAction.selectedLocationsList).length == 0) {
            is_orderlocations_exist = false;
            var alert_msg = "";
            if (page_name == '_jobApproval') {
                alert_msg = "Proofing requires locations to be added on the Select Locations page. You must add locations before continuing with Proof generation.";
                $('#viewFrame')[0].contentWindow.document.body.innerHTML = "";
            }
            else {
                alert_msg = "Submit requires locations to be added on the Select Locations page. You must add locations before continuing with your order.";
            }
            $('#alertmsg').html(alert_msg);
            $('#okBut').unbind('click');
            $('#okBut').bind('click', function () {
                $('#okBut').unbind('click');
                $('#popupDialog').popup('close');
                window.location.href = "../jobSelectLocations/jobSelectLocations.html";
            });
            $('#popupDialog').popup('open');
        }
    }
    return is_orderlocations_exist;
}

var isOfferListValid = function (page_name) {
    var is_offers_valid = false;
    if (sessionStorage.jobSetupOutput != undefined && sessionStorage.jobSetupOutput != null && sessionStorage.jobSetupOutput != "") {
        gOutputData = $.parseJSON(sessionStorage.jobSetupOutput);      
        if ((gOutputData.variableOfferAction != undefined && gOutputData.variableOfferAction != null && gOutputData.variableOfferAction != "") &&
            (gOutputData.variableOfferAction.offerList != undefined && gOutputData.variableOfferAction.offerList != null && gOutputData.variableOfferAction.offerList != "") && Object.keys(gOutputData.variableOfferAction.offerList).length > 0) {
            var offer_list = gOutputData.variableOfferAction.offerList;
            is_offers_valid = true;
            $.each(offer_list, function (k, v) {
                if (!is_offers_valid) {
                    return false;
                }
                var discount_type = "";
                $.each(v.fieldList, function (k1, v1) {
                    if (v1.name.trim().toLowerCase() == "discounttype") {
                        discount_type = v1.value;
                    }
                });
                $.each(v.fieldList, function (k1, v1) {
                    if (discount_type.replace(/ /g, '').toLowerCase() != "everydaysavings") {
                        if (v1.value.trim() == "" && v1.name.trim().indexOf("limit") == -1) {
                            is_offers_valid = false;
                            return false;
                        }
                    }
                    else {
                        if ((['purchaseprice', 'itemnumber','productsize', 'productcoverage'].indexOf(v1.name.toLowerCase()) > -1) && (v1.value.trim() == "")) {
                            is_offers_valid = false;
                            return false;
                        }
                    }
                });
            });
        }
    }
    if (!is_offers_valid) {
        var alert_msg = "";        
        if (page_name == '_jobApproval') {
            alert_msg = "Proofing requires offer values to be added on the Offer Setup page. You must add offer values before continuing with Proof generation.";
            $('#viewFrame')[0].contentWindow.document.body.innerHTML = "";
        }
        else {
            alert_msg = "Submit requires offer values to be added on the Offer Setup page. You must add offer values before continuing with your order.";
        }
        $('#alertmsg').html(alert_msg);
        $('#okBut').unbind('click');
        $('#okBut').bind('click', function () {
            $('#okBut').unbind('click');
            $('#popupDialog').popup('close');
            if (gOutputData.jobTypeId != 97 && gOutputData.jobTypeId != ACE_CustomUpload_JobType) {
                window.location.href = "../jobOfferSetup/jobOfferSetup.html";
            }
            
        });
        $('#popupDialog').popup('open');
    }
    return is_offers_valid;
}

var isSelectImagesValid = function (page_name) {
    var is_select_images_valid = true;
    if (sessionStorage.jobSetupOutput != undefined && sessionStorage.jobSetupOutput != null && sessionStorage.jobSetupOutput != "") {
        gOutputData = $.parseJSON(sessionStorage.jobSetupOutput);        
        var image_list = gOutputData.variableImageAction.variableFieldList;
        var msg = "";
        if (image_list.length == 0) {
            if (gOutputData.templateName.toLowerCase() != "_endlessyes_8_5x11") {
                msg = "custom logo and base artwork";
            }
            else {
                msg = "custom logo";
            }
            is_select_images_valid = false;
        }
        else {
            $.each(image_list, function (k, v) {
                if (v.value == "") {
                    if (gOutputData.templateName.toLowerCase() != "_endlessyes_8_5x11") {
                        msg += (v.name == "logo1" ? "custom logo" : " and base artwork")
                        is_select_images_valid = false;
                    }
                    else if (v.name != "base1") { //Because no base artwork for "_endlessyes_8_5x11" template
                        msg = "custom logo";
                        is_select_images_valid = false;
                    }                    
                }
            });
        }       
        if (!is_select_images_valid) {
            var alert_msg = "";
            if (page_name == '_jobApproval') {
                alert_msg = "Proofing requires " + msg + " to be added on the Select Images page. You must add image value(s) before continuing with Proof generation.";
                $('#viewFrame')[0].contentWindow.document.body.innerHTML = "";
            }
            else {
                alert_msg = "Submit requires " + msg + " to be added on the Select Images page. You must add image value(s) before continuing with your order.";
            }
            $('#alertmsg').html(alert_msg);
            $('#okBut').unbind('click');
            $('#okBut').bind('click', function () {
                $('#okBut').unbind('click');
                $('#popupDialog').popup('close');
                window.location.href = "../jobSelectImages/jobSelectImages.html";
            });
            $('#popupDialog').popup('open');
        }
    }
    return is_select_images_valid;
}

var isVariableFieldListValid = function (page_name) {
    var is_variableFieldList_valid = false;
    if (sessionStorage.jobSetupOutput != undefined && sessionStorage.jobSetupOutput != null && sessionStorage.jobSetupOutput != "") {
        gOutputData = $.parseJSON(sessionStorage.jobSetupOutput);  
        is_variableFieldList_valid = true;
        if (gOutputData.variableTextAction.variableFieldList == undefined || gOutputData.variableTextAction.variableFieldList == "" || gOutputData.variableTextAction.variableFieldList == null || gOutputData.variableTextAction.variableFieldList.length == 0) {
            is_variableFieldList_valid = false;
            var alert_msg = "";
            if (page_name == '_jobApproval') {
                alert_msg = "Proofing requires locations to be added on the Custom Text page. You must add locations before continuing with Proof generation.";
                $('#viewFrame')[0].contentWindow.document.body.innerHTML = "";
            }
            else {
                alert_msg = "Submit requires locations to be added on the Custom Text page. You must add locations before continuing with your order.";
            }
            $('#alertmsg').html(alert_msg);                     
            $('#okBut').unbind('click');
            $('#okBut').bind('click', function () {
                $('#okBut').unbind('click');
                $('#popupDialog').popup('close');
                if (gOutputData.jobTypeId != 97 && gOutputData.jobTypeId != ACE_CustomUpload_JobType) {
                    window.location.href = "../jobCustomText/jobCustomText.html";
                }                
            });
            $('#popupDialog').popup('open');
        }
    }
    return is_variableFieldList_valid;        
}