﻿ACE_CustomUpload_JobType = 150;
ANDERSEN_CustomUpload_JobType = 162;
SHERWINWILLIAMS_CustomUpload_JobType = 0;
MILWAUKEE_CustomUpload_JobType = 9999;
BENJAMINMOORE_CustomUpload_JobType = 9999;
TRAEGER_CustomUpload_JobType = 9999;

//Job Navigation Links - jobNavLinks.js
isDeleteApprovalPage = function () {
    var customer = getCustomerNumber();
    return [DKI, ICYNENE, CASEYS, CW, SCOTTS, EQUINOX, REDPLUM, ACE, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1 ? false : true;
}

isChangePageDisplayLabel = function () {
    var customer = getCustomerNumber();    
    return [DKI, ICYNENE, DCA, KUBOTA, OH, SPORTSKING, GWA, CASEYS, CW, SCOTTS, REDPLUM, BUFFALONEWS, PIONEERPRESS, EQUINOX, ALLIED, ACE, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER, AAG].indexOf(customer) > -1 ? true : false;
}

isListSelectionAvailable = function () {
    var customer = getCustomerNumber();    
    return [DKI, ICYNENE, CASEYS, ALLIED, JETS, CW, REGIS, SCOTTS, BRIGHTHOUSE, BBB, AAG, TRIBUNEPUBLISHING, SANDIEGO, AH, SPORTSAUTHORITY, WALGREENS, DATAWORKS, SAFEWAY, LOWES, DCA, KUBOTA, GWA, SPORTSKING, EQUINOX, ACE, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1 ? true : false;
}

isCustomizedPageBuildCustomer = function () {
    var customer = getCustomerNumber();
    return [DKI, ICYNENE].indexOf(customer) > -1 ? true : false;
}

isDisplaySliderInTemplatePage = function () {
    var customer = getCustomerNumber();    
    return [DKI, ICYNENE, BBB, ALLIED, REDPLUM, JETS, AAG, TRIBUNEPUBLISHING, SANDIEGO, AH, SPORTSAUTHORITY, WALGREENS, MOTIV8, SAFEWAY, LOWES, CASEYS, CW, DCA, KUBOTA, GWA, OH, SPORTSKING, SCOTTS, EQUINOX, ACE, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1 ? true : false;
}

isDisplaySliderInLocationsPage = function () {
    var customer = getCustomerNumber();    
    return [DKI, ICYNENE, JETS, ALLIED, CASEYS, CW, DCA, AAG, KUBOTA, GWA, OH, SPORTSKING, SCOTTS, BUFFALONEWS, PIONEERPRESS, REDPLUM, EQUINOX, ACE, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1 ? true : false;
}

isDisplaySliderInPage = function () {
    var customer = getCustomerNumber();    
    return [DKI, ICYNENE, BBB, ALLIED, AAG, TRIBUNEPUBLISHING, SANDIEGO, AH, SPORTSAUTHORITY, WALGREENS, JETS, MOTIV8, SAFEWAY, LOWES, CASEYS, CW, DCA, KUBOTA, GWA, OH, SPORTSKING, SCOTTS, BUFFALONEWS, PIONEERPRESS, REDPLUM, EQUINOX, ACE, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1 ? true : false;
}

isDisplayContinueInSubmitOrder = function () {
    var customer = getCustomerNumber();
    return [DKI, ICYNENE].indexOf(customer) > -1 ? false : true;
}

isDisplaySaveInList = function () {
    var customer = getCustomerNumber();
    return [DKI, ICYNENE].indexOf(customer) > -1 ? true : false;
}

isDisplayChatWidget = function () {
    var customer = getCustomerNumber();
    return [SCOTTS, CW, ACE, MILWAUKEE,CASEYS].indexOf(customer) > -1 ? true : false;
}

//Index Page
isCreateDemoHintsInIndexPage = function () {
    var customer = getCustomerNumber();
    return [DKI, ICYNENE, ACE, SCOTTS, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1 ? true : false;
}
isWelcomeMsgRequired = function () {
    var customer = getCustomerNumber();
    return [BBB, AAG, TRIBUNEPUBLISHING, SANDIEGO, AH, SPORTSAUTHORITY, WALGREENS, JETS, CASEYS, CW, MOTIV8, ALLIED, REDPLUM, SAFEWAY, LOWES, DCA, KUBOTA, GWA, OH, SPORTSKING, BUFFALONEWS, PIONEERPRESS, EQUINOX, DKI, ICYNENE, SCOTTS, ANDERSEN, SHERWINWILLIAMS].indexOf(customer) > -1 ? true : false;
}

showDemoIntroductionPopup = function () {
    var customer = getCustomerNumber();
    return [
        DKI,
        ICYNENE,
        BBB,
        ALLIED,
        JETS,
        CASEYS,
        CW,
        REDPLUM,
        OH,
        BUFFALONEWS,
        PIONEERPRESS,
        EQUINOX,
        ACE
    ].indexOf(customer) > -1 ? true : false;
}

showDemoListJobs = function () {
    var customer = getCustomerNumber();
    return [DKI, ICYNENE, ACE, SCOTTS, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1 ? false : true;
}

showDemoReports = function () {
    var customer = getCustomerNumber();
    return [DKI, ICYNENE, ACE, SCOTTS, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1 ? false : true;
}

showJetsDemoJobsDiv = function () {
    var customer = getCustomerNumber();
    return [DKI, ICYNENE, ACE, SCOTTS, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1 ? true : false;
}

showAboutDemoDiv = function () {
    var customer = getCustomerNumber();
    return [DKI, ICYNENE, ACE, SCOTTS, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1 ? false : true;
}

showBobsListDiv = function () {
    var customer = getCustomerNumber();
    return [DKI, ICYNENE, ACE, SCOTTS, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1 ? false : true;
}

changeLinkNewJobText = function () {
    var customer = getCustomerNumber();
    return [DKI, ICYNENE].indexOf(customer) > -1 ? true : false;
}

showDemoEmailListDiv = function () {
    var customer = getCustomerNumber();
    return [DKI, ICYNENE, ACE, SCOTTS, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1 ? false : true;
}

isLoadDemoEmailList = function () {
    var customer = getCustomerNumber();
    return [DKI, ICYNENE].indexOf(customer) > -1 ? true : false;
}

isAppendCustomerToLogin = function () {
    var customer = getCustomerNumber();
    return [DKI, ICYNENE, ACE, SCOTTS, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1 ? true : false;
}

isLoadCampingWorldPagePreferences = function () {
    var customer = getCustomerNumber();
    return [DKI, ICYNENE, ACE, SCOTTS, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1 ? true : false;
}

isSetShowDemoHintsON = function (customer) {
    return [DKI, ICYNENE, ACE, SCOTTS, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1 ? true : false;
}

isHideInHomeinJobsList = function () {
    var customer = getCustomerNumber();
    return [DKI, ICYNENE, ACE, SCOTTS, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1 ? true : false;
}

isBreakIndexCustomized = function () {
    var customer = getCustomerNumber();
    return [DKI, ICYNENE, ACE, SCOTTS, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1 ? true : false;
}

isFilterAttributesRequired = function () {
    var customer = getCustomerNumber();
    return [DKI, ICYNENE, ACE, SCOTTS, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1 ? true : false;
}

isRefreshJetsDemoJobsDiv = function () {
    var customer = getCustomerNumber();
    return [DKI, ICYNENE, ACE, SCOTTS, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1 ? true : false;
}

isJetsDemoJobsDivCustomer = function () {
    var customer = getCustomerNumber();
    return [DKI, ICYNENE, BBB, ALLIED, JETS, CASEYS, CW, SCOTTS, AAG, TRIBUNEPUBLISHING, SANDIEGO, AH, SPORTSAUTHORITY, WALGREENS, MOTIV8, SAFEWAY, LOWES, DCA, KUBOTA, OH, SPORTSKING, REDPLUM, GWA, REDPLUM, BUFFALONEWS, PIONEERPRESS, EQUINOX, ACE, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1 ? true : false;
}

isExpandSourceRequired = function () {
    var customer = getCustomerNumber();
    return [
        DKI,
        ICYNENE,
        ACE,
        ALLIED,
        SPORTSAUTHORITY,
        WALGREENS,
        MOTIV8,
        SAFEWAY,
        LOWES,
        CW,
        SCOTTS,
        CASEYS,
        DCA,
        KUBOTA,
        OH,
        SPORTSKING,
        GWA,
        REDPLUM,
        BUFFALONEWS,
        PIONEERPRESS,
        EQUINOX,
        ANDERSEN,
        SHERWINWILLIAMS,
        MILWAUKEE,
        BENJAMINMOORE,
        TRAEGER
    ].indexOf(customer) > -1 ? true : false;
}
 
isGroupedJobsColumn2 = function () {
    var customer = getCustomerNumber();
    return [
            DKI,
            ICYNENE,
            ACE,
            BBB,
            JETS,
            CW,
            SCOTTS,
            CASEYS,
            AAG,
            TRIBUNEPUBLISHING,
            SANDIEGO,
            AH,
            SPORTSAUTHORITY,
            WALGREENS,
            MOTIV8,
            ALLIED,
            SAFEWAY,
            LOWES,
            DCA,
            KUBOTA,
            OH,
            SPORTSKING,
            GWA,
            REDPLUM,
            BUFFALONEWS,
            PIONEERPRESS,
            EQUINOX,
            ANDERSEN,
            SHERWINWILLIAMS,
            MILWAUKEE,
            BENJAMINMOORE,
            TRAEGER
          ].indexOf(customer) > -1 ? true : false;
}

isUpdateDemoEmailList = function () {
    var customer = getCustomerNumber();
    return [DKI, ICYNENE].indexOf(customer) > -1 ? true : false;
}

isFilterJSONReports = function () {
    var customer = getCustomerNumber();
    return [DKI, ICYNENE].indexOf(customer) > -1 ? true : false;
}

displayReports = function () {
    var display_reports = false;
    var customer = getCustomerNumber();
    var role = getUserRole();

    switch (role) {
        case 'admin':
            display_reports = [SCOTTS, ACE, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1 ? true : false;
            break;
        default: break;
    }

    return display_reports;
}

showReportsSection = function () {
    var show_report_section = true;
    var customer = getCustomerNumber();
    var role = getUserRole();

    switch (role) {
        case 'admin':
            show_report_section = [KUBOTA].indexOf(customer) > -1 ? false : true;
            break;
        case 'power':
            show_report_section = [KUBOTA, CW].indexOf(customer) > -1 ? false : true;
            break;
        case 'user':
            show_report_section = [KUBOTA, CW].indexOf(customer) > -1 ? false : true;
            break;
        default: break;
    }

    return show_report_section;
}

//jobUploadFiles.js
showLocationAddress = function () {
    var customer = getCustomerNumber();
    return [DKI, ICYNENE].indexOf(customer) > -1 ? true : false;
}

showArtFileUploadDemoHint = function () {
    var customer = getCustomerNumber();    
    return [DKI, ICYNENE, ACE, BBB, MOTIV8, CW, DCA, KUBOTA, OH, SPORTSKING, GWA, CASEYS, SCOTTS, EQUINOX, ALLIED, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1 ? true : false;
}

//jobPieceAttributes.js
isCWPieceAttributePreferences = function () {
    var customer = getCustomerNumber();
    return [
            ANDERSEN,
            DKI,
            ICYNENE,
            REGIS,
            SCOTTS,
            "203",
            BRIGHTHOUSE,
            CASEYS,
            ALLIED,
            DCA,
            AAG,
            KUBOTA,
            OH,
            SPORTSKING,
            GWA,
            EQUINOX,
            SHERWINWILLIAMS,
            MILWAUKEE,
            BENJAMINMOORE,
            TRAEGER
          ].indexOf(customer) > -1 ? true : false;
}

showPieceAttributesDemoHint = function () {
    var customer = getCustomerNumber();
    return ([
                DKI,
                ICYNENE,
                DCA,
                KUBOTA,
                CASEYS,
                CW,
                AAG,
                JETS,
                ALLIED,
                OH,
                SPORTSKING,
                GWA,
                SCOTTS,
                EQUINOX,
                ANDERSEN,
                SHERWINWILLIAMS,
                MILWAUKEE,
                BENJAMINMOORE,
                TRAEGER
          ].indexOf(customer) > -1);
}

//jobListSelection.js
isCreateListSelectionDemoHints = function () {
    var customer = getCustomerNumber();
    return [DKI, ICYNENE].indexOf(customer) > -1 ? true : false;
}
isDefaultSelectByRadiusExpand = function () {
    var customer = getCustomerNumber();
    return [DKI, ICYNENE].indexOf(customer) > -1 ? true : false;
}
isCASEYSListSelectionPreferences = function () {
    var customer = getCustomerNumber();
    return [DKI, ICYNENE, ACE].indexOf(customer) > -1 ? true : false;
}
hideClientListDiv = function () {
    var customer = getCustomerNumber();
    return [DKI, ICYNENE].indexOf(customer) > -1 ? true : false;
}
showStoreSelectionPopup = function () {
    var customer = getCustomerNumber();
    return [DKI, ICYNENE, ACE].indexOf(customer) > -1 ? true : false;
}
showPostalBoundarySelectionsPopup = function () {
    var customer = getCustomerNumber();
    return [DKI, ICYNENE, ACE].indexOf(customer) > -1 ? true : false;
}
showlocationsRequiredMessage = function () {
    var customer = getCustomerNumber();
    return [DKI, ICYNENE, ACE].indexOf(customer) > -1 ? true : false;
}
showListSavedMessage = function () {
    var customer = getCustomerNumber();
    return [DKI, ICYNENE].indexOf(customer) > -1 ? true : false;
}
hideMailAroundCompitor = function () {
    var customer = getCustomerNumber();
    return [DKI, ICYNENE].indexOf(customer) > -1 ? true : false;
}
showCarrierRouteWarningMessage = function () {
    var customer = getCustomerNumber();
    return [DKI, ICYNENE].indexOf(customer) > -1 ? true : false;
}
showSelectionMethodsListDemoHint = function () {
    var customer = getCustomerNumber();
    return [DKI, ICYNENE, ACE].indexOf(customer) > -1 ? true : false;
}
showPerLocationTargetMessage = function () {
    var customer = getCustomerNumber();
    return [DKI, ICYNENE, ACE].indexOf(customer) > -1 ? true : false;
}
showDemographics = function () {
    var customer = getCustomerNumber();
    return [DKI, ICYNENE].indexOf(customer) > -1 ? true : false;
}

hideCustomerConfigAndCollapsibleDemos = function () {
    var customer = getCustomerNumber();
    return [CASEYS, JETS, AAG, SCOTTS, ACE, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1 ? true : false;
}

showResidentListOptions = function () {
    var show_resident_checklist = false;
    var customer = getCustomerNumber();
    var role = getUserRole();

    switch (role) {
        case 'admin':
            show_resident_checklist = [SCOTTS, ACE, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1 ? true : false;
            break;
        case 'power': break;
        case 'user': break;
        default: break;
    }

    return show_resident_checklist;
}

showFixedInHomeDatesForSelection = function () {
    var customer = getCustomerNumber();
    var role = getUserRole();
    var show_fixed_in_home_dates = false;
    switch (role) {
        case "power":
        case "user":
            show_fixed_in_home_dates = [MILWAUKEE, ACE, SCOTTS].indexOf(customer) > -1;
            break;
        default:
            break;
    }

    return show_fixed_in_home_dates;
}