﻿//Index
featureFinanceSection = function () {
    var feature_finance = false;
    var customer = getCustomerNumber();
    var role = getUserRole();
    
    switch (role) {
        case 'admin':
           // feature_finance = [CW].indexOf(customer) > -1 ? true : false;
            break;
        case 'power': break;
        case 'user': break;
        default: break;
    }
    return feature_finance;
}

featureModuleConfiguration = function () {
    var feature_module_config = false;
    var customer = getCustomerNumber();
    var role = getUserRole();

    switch (role) {
        case 'admin':
            feature_module_config = [].indexOf(customer) > -1 ? true : false;
            break;
        case 'power': break;
        case 'user': break;
        default: break;
    }
    return feature_module_config;
}

featureJobManagement = function () {
    var feature_jobManagement = false;
    var customer = getCustomerNumber();
    var role = getUserRole();

    switch (role) {
        case 'admin':
            feature_jobManagement = [SPORTSKING].indexOf(customer) > -1 ? true : false;
            break;
        case 'power': break;
        case 'user': break;
        default: break;
    }
    return feature_jobManagement;
}

featureStatusThermometer = function () {
    return false;
}

featureShortageModulesConfiguration = function () {
    var feature_shortage_corrections = false;
    var customer = getCustomerNumber();
    var role = getUserRole();
    switch (role) {
        case 'admin':
            feature_shortage_corrections = [REDPLUM].indexOf(customer) > -1 ? true : false;
            break;
        default: break;
    }
    return feature_shortage_corrections;
}

featureSharedMailDashboard = function () {
    var feature_shared_mail = false;
    var customer = getCustomerNumber();
    var role = getUserRole();
    switch (role) {
        case 'admin':
            feature_shared_mail = [REDPLUM].indexOf(customer) > -1 ? true : false;
            break;
        default: break;
    }
    return feature_shared_mail;
}

//Locations
featureNewLocations = function () {
    //return [GWA, SCOTTS].indexOf(jobCustomerNumber) > -1 ? true : false;

    var feature_search_new_locations = false;
    var customer = getCustomerNumber();
    var role = getUserRole();

    switch (role) {
        case 'admin':
            feature_search_new_locations = [SCOTTS, ACE].indexOf(jobCustomerNumber) > -1 ? true : false;
            break;
        case 'power': break;
        case 'user':
            feature_search_new_locations = [GWA].indexOf(jobCustomerNumber) > -1 ? true : false;
            break;
        default: break;
    }
    return feature_search_new_locations;
}

featureLocationProfileUpdate = function () {
   // return ([GWA].indexOf(jobCustomerNumber) > -1 && ['admin', 'power'].indexOf(sessionStorage.userRole) > -1) ? true : false;


    var feature_edit_locations = false;
    var customer = getCustomerNumber();
    var role = getUserRole();

    switch (role) {
        case 'admin':
            feature_edit_locations = [SCOTTS, ACE, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(jobCustomerNumber) > -1 ? true : false;
            break;
        case 'power':
        case 'user':
            feature_edit_locations = [GWA].indexOf(jobCustomerNumber) > -1 ? true : false;
            break;
        default: break;
    }
    return feature_edit_locations;
}

featureDefaultLoadExistingLocations = function () {
    var feature_default_load = false;
    var customer = getCustomerNumber();
    var role = getUserRole();

    switch (role) {
        case 'admin':           
            break;
        case 'power': break;
        case 'user':
            feature_default_load = [SCOTTS, ACE, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1 ? true : false;
            break;
        default: break;
    }
    return feature_default_load;
}
//PieceAttributes
featureUpdateDimentions = function () {
    var update_feature = false;
    switch (sessionStorage.userRole) {
        case 'admin':
            update_feature = true;
            break;
        case 'power':
            update_feature = [GWA].indexOf(jobCustomerNumber) > -1 ? true : false;
            break;
        case 'user': break;
        default: break
    }
    return update_feature;
}

//Submit Order
featureBillingInvoice = function () {
    var invoice_feature = false;
    var customer = getCustomerNumber();
    var role = getUserRole();
    switch (role) {
        case 'admin':
            invoice_feature = [GWA, CW].indexOf(customer) > -1 ? true : false;
            break;
        case 'power':
            invoice_feature = [GWA].indexOf(customer) > -1 ? true : false;
            break;
        case 'user': break;
        default: break
    }
    return invoice_feature;
}

featurePostageInvoice = function () {
    var invoice_feature = false;
    switch (sessionStorage.userRole) {
        case 'admin':
            invoice_feature = [CW].indexOf(jobCustomerNumber) > -1 ? true : false;
            break;
        case 'power':break;
        case 'user': break;
        default:break
    }
    return invoice_feature;
}

//Job Management
featureLogAccountReceivables = function () {
    var feature_accountreceivables = false;
    var customer = getCustomerNumber();
    var role = getUserRole();

    switch (role) {
        case 'admin':
            feature_accountreceivables = [CW].indexOf(customer) > -1 ? true : false;
            break;
        case 'power': break;
        case 'user': break;
        default: break;
    }
    return feature_accountreceivables;
}

//List Selection
showScatterMapsByDefault = function () {
    var show_scatter_maps = false;
    var customer = getCustomerNumber();

    show_scatter_maps = [SCOTTS, SPORTSKING, ACE, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1 ? true : false;

    return show_scatter_maps;
}

hideSurroundingLocMarkersByDefault = function () {
    var show_surrounding_loc_markers = false;
    var customer = getCustomerNumber();

    show_surrounding_loc_markers = [SCOTTS, ACE, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1 ? true : false;

    return show_surrounding_loc_markers;
}

//Utility Functions
getCustomerNumber = function () {
    var customer_number = jobCustomerNumber;
    if (customer_number == undefined || customer_number == "" || customer_number == null) {
        if (appPrivileges != undefined && appPrivileges != null && appPrivileges != "")
            customer_number = appPrivileges.customerNumber;     
        else
            customer_number = sessionStorage.customerNumber;
    }

    return customer_number;
}

getUserRole = function () {
    var user_role = sessionStorage.userRole;
    if (user_role == undefined || user_role == "" || user_role == null) {
        user_role = appPrivileges != undefined && appPrivileges != null && appPrivileges != "" ? appPrivileges.roleName : '';
    }

    return user_role;
}