﻿//reports webservice URL
//var reportsURL = myProtocol + "reporting.tribunedirect.com";
var jsonOutput;
var jsonPage;
var jsonURL;

var environments = ["localhost", "sg-srv-td2"]

//hide the static image (loading...) after opening the reports.
function removeLoadingImg() {
    $('#waitPopUp').popup('close');
}
//calls the webservice to show the SSRS reports based on the user selection report
function fnDisplayReports(json_report, output_type) {
    var report_URL = reportsURL + "api/reportv2_setreportinfo";
    jsonOutput = output_type;
    jsonPage = JSON.stringify(json_report);
    jsonURL = report_URL;
    var temp_auth_string = "";
    if (jobCustomerNumber == DCA || jobCustomerNumber == SPORTSKING) {
        temp_auth_string = sessionStorage.authString;
        //sessionStorage.authString = "Basic YWRtaW5fdGQxOmFkbWluX3RkMQ==";
        sessionStorage.authString = makeBasicAuth(sessionStorage.userRole + "_td1", sessionStorage.userRole + "_td1");
    }
    /*if (jobCustomerNumber == CW && json_report.reportName == "Daily Milestones") { //todo: Maulika requirement on date 06/12/2017 pdf hardcoded.
        openPDFFromResource(json_report.reportName);
    }
    else {*/
        postCORS(report_URL, JSON.stringify(json_report), function (response) {
            if (jobCustomerNumber == DCA || jobCustomerNumber == SPORTSKING) {
                sessionStorage.authString = temp_auth_string;
            }
            windowOpen(output_type, json_report.reportName);
            setTimeout(function getDelay() { setTimeout }, 500);
            //removeLoadingImg();
        }, function (response_error) {
            if (jobCustomerNumber == DCA || jobCustomerNumber == SPORTSKING) {
                sessionStorage.authString = temp_auth_string;
            }
            removeLoadingImg();
            showErrorResponseText(response_error, false);
            //$('#alertmsg').text((response_error.responseText != undefined) ? response_error.responseText : response_error.statusText);
            //setTimeout(function getDelay() {
            //    $('#popupDialog').popup('open');
            //}, 1000);

        });
    //}    
}
//create the JSON format to call the webservice (ssrsFolder,report input parameters and type of account (jobNumber)
function createJSONForReport(report_name, output_type, input_report_params, ssrsFolder, jobNumber) {

// Seneca
// These are duplicate comments from the _regionalHome.js scripts
//1) In Reports.js you are:
// if (ssrsFolder == "facilityPage") {
// ssrsFolder = "ScheduleReporting";
//which seemed to be the long way round when you are already passing a value as ssrsFolder
//to the method. I am just set the ssrs value to what it has to be and passing that.
//2) We should find a way remove the literal hard coding and logical switches and if then if else
//blocks to improve maintenance
    var json_report = {};
    if (ssrsFolder == "facilityPage") {
        ssrsFolder = "ScheduleReporting";
    } else if (ssrsFolder == "acctingrpts") {
        ssrsFolder = "Web Invoices";
    } else if (ssrsFolder == "gridrpts") {
        ssrsFolder = "Camping World";
    }
//json_report["facilityId"] = sessionStorage.facilityId;

// Seneca
// I would espect by thye time this next block is being executed you would not need to replace the underscores with spaces. What if a report actually has under scores in the name?
// Bill
    json_report["ssrsFolder"] = ssrsFolder;
    json_report["outputType"] = output_type;
    json_report["reportName"] = report_name.replace(/_/g, ' ');
    json_report["parameters"] = createReportParamJSON(report_name, input_report_params, jobNumber);
    //$('#overlay').css('display', 'block');
    $('#waitPopUp').popup('open');
    setTimeout(function loadData() { fnDisplayReports(json_report, output_type); }, 1000);
}
//create a array of input parameters for the selected report and return input parameters array.
function createReportParamJSON(rpt_name, param_json, jobNumber) {
    var customer_number = appPrivileges.customerNumber;
    //if (customer_number != CW) {
    //    jobNumber = 56195;
    //}
    switch (rpt_name.replace(/_/g, ' ').toLowerCase()) {
        case "billing invoice":
            var facility_id = appPrivileges.facility_id;
            param_json.push({
                "name": "customerNumber",
                "value": customer_number
            });
            param_json.push({
                "name": "jobNumber",
                "value": jobNumber
            });
            param_json.push({
                "name": "facilityId",
                "value": facility_id
            });
            break;
        case "rptpostagerequestall":
            var facility_id = appPrivileges.facility_id;
            param_json.push({
                "name": "customerId",
                "value": customer_number
            });
            param_json.push({
                "name": "jobNumber",
                "value": jobNumber
            });
            param_json.push({
                "name": "facilityId",
                "value": facility_id
            });
            break;
        case "dp address occurances":
            param_json.push({
                "name": "customerNumber",
                "value": customer_number
            });
            param_json.push({
                "name": "jobNumber",
                "value": jobNumber
            });
            param_json.push({
                "name": "code1Flag",
                "value": 0
            });
            param_json.push({
                "name": "dpvFlag",
                "value": 0
            });
            break;
        case "is reject details":
        case "is mailstream summary":
        case "is standardization summary":
            param_json.push({
                "name": "jobNumber",
                "value": jobNumber
            });
            break;
        case "is state store counts":
        case "is zip code counts":
            param_json.push({
                "name": "jobNumber",
                "value": jobNumber
            });
            param_json.push({
                "name": "code1Flag",
                "value": 0
            });
            param_json.push({
                "name": "dpvFlag",
                "value": 0
            });
            break;
        //case "iswaterfallreport":
        case "waterfall report":
            param_json.push({
                "name": "jobNumber",
                "value": jobNumber
            });
            break;
    }
    return param_json;
}
//to open the reports in a window (iframe), this method is called in "fnDisplayReports".
//function windowOpen(response, output_type) {
function windowOpen(output_type, report_name) {
    var content_type = '';
    var htmlText = "";
    var report_preview_url = reportsURL + "api/reportv2/" + ((sessionStorage.customerNumber == DCA || appPrivileges.customerNumber == SPORTSKING) ? sessionStorage.userRole + "_td1" : sessionStorage.username) + "_" + report_name.replace(/\s/g, "_");
    //removeLoadingImg();
    $('#popPDF').popup('close');
    switch (output_type) {
        case "pdf":
            content_type = ' type="application/pdf"'
            break;
        case "doc":
            content_type = ' type="application/msword"'
            break;
        case "xls":
            content_type = ' type="application/x-msexcel"'
            break;
        case "image":
            content_type = ' type="image/tiff" src="data:image/tiff;base64,'
            break;
    }

    $('#popPDF').popup('close');
    var htmlText = "";
    //if (output_type == 'pdf' || output_type == 'xml') {
    htmlText = '<iframe width=850 HEIGHT=500 id="rptFrame"'
                      + content_type
                      + ' src="' + report_preview_url
                      + '"></iframe>';
    //}
    //    else {
    //        htmlText = '<iframe width=50 HEIGHT=50 id="rptFrame"'
    //                      + content_type
    //                      + ' src="' + report_preview_url
    //                      + '"></iframe>';
    //    }
    if (output_type == 'pdf' || output_type == 'xml') {
        $('#popupPDFContent').html(htmlText);
    } else {
        $('#popupPDFContent').html(htmlText);
    }

    var iframe = document.getElementById('rptFrame');

    //DOMContentLoaded
    if (iframe.addEventListener) {
        iframe.addEventListener('onload', callback(output_type), true);
    } else {
        iframe.attachEvent('onload', callback(output_type));
    }

    if (output_type == 'pdf' || output_type == 'xml') {
        var wait_ctrl = '<div id="dvWaitImage" >';
        $('#waitPopUp').popup('open');
        $('#waitPopUp').popup('close');
        wait_ctrl += $('#waitPopUp').html() + "</div>";
        $('#popupPDFContent').prepend(wait_ctrl);
        //var maxHeight = ($('#popPDF').height() / 2) - 190 + "px";
        //var maxWidth = ($('#popPDF').width() / 2 - 80) + "px";
        var maxHeight = ($('#popPDF').height() / 2) - 190 + "px";
        var maxWidth = ($('#popPDF').width() / 2) + 340 + "px";
        $('#dvWaitImage').css('position', 'absolute');
        $("#dvWaitImage").css("margin-top", maxHeight);
        $("#dvWaitImage").css("margin-left", maxWidth);

        $('#popPDF').popup('open', { positionTo: 'window' });
    }
}

function openPDFFromResource(report_name) {
    var report_preview_url = '../images/Daily Milestones.pdf';
    $('#popPDF').popup('close');
    var htmlText = "";
    var output_type = 'pdf';
    htmlText = '<iframe width=850 HEIGHT=500 id="rptFrame" type="application/pdf" src="' + report_preview_url+ '"></iframe>';
    $('#popupPDFContent').html(htmlText);    

    var iframe = document.getElementById('rptFrame');

    //DOMContentLoaded
    if (iframe.addEventListener) {
        iframe.addEventListener('onload', callback(output_type), true);
    } else {
        iframe.attachEvent('onload', callback(output_type));
    }

    var wait_ctrl = '<div id="dvWaitImage" >';
    $('#waitPopUp').popup('open');
    $('#waitPopUp').popup('close');
    wait_ctrl += $('#waitPopUp').html() + "</div>";
    $('#popupPDFContent').prepend(wait_ctrl);
    //var maxHeight = ($('#popPDF').height() / 2) - 190 + "px";
    //var maxWidth = ($('#popPDF').width() / 2 - 80) + "px";
    var maxHeight = ($('#popPDF').height() / 2) - 190 + "px";
    var maxWidth = ($('#popPDF').width() / 2) + 340 + "px";
    $('#dvWaitImage').css('position', 'absolute');
    $("#dvWaitImage").css("margin-top", maxHeight);
    $("#dvWaitImage").css("margin-left", maxWidth);

    $('#popPDF').popup('open', { positionTo: 'window' });
}

var timeInterval = "";
function callback(output_type) {
    var is_loaded = false;
    timeInterval = setInterval(function () {
        //$('#rptFrame')[0].contentDocument.message == "Access is denied.\r\n"
        //if (((navigator.userAgent.indexOf("MSIE") > -1 && !window.opera) && typeof ($('#rptFrame')[0].contentDocument) == "unknown") ||
        //        ((navigator.userAgent.indexOf("MSIE") == -1) && (typeof ($('#rptFrame')[0].contentDocument) == "object"))) {
        is_loaded = true;
        clearInterval(timeInterval);
        timeInterval = "";

        if (output_type != 'pdf' && output_type != 'xml') {
            if (window.location.href.toString().toLowerCase().indexOf('regionalhome') > -1) {
                setTimeout(function getDelay() {
                    //$('#popPDF').popup('open');
                    $('#popPDF').popup('close');
                    $('#waitPopUp').popup('open');
                    $('#waitPopUp').popup('close');
                }, 8500);
            }
            else {
                setTimeout(function getDelay() {
                    $('#popPDF').popup('close');
                    $('#waitPopUp').popup('open');
                    $('#waitPopUp').popup('close');
                }, 3000);
            }
        }
        else {
            //if (navigator.userAgent.indexOf("MSIE") > -1 && !window.opera) {

            setTimeout(function setTimer() {
                $('#waitPopUp').popup('open');
                $('#waitPopUp').popup('close');
                removeLoadingImg();
                $('#dvWaitImage').empty();
            }, 5000);
            //                }
            //                else {
            //                    $('#dvWaitImage').empty();
            //                }
        }
        //}
    }, 1000);
}

function populateAccountingReportsOutputForm(report_name, store_id) {
    $("#hdnReportSelected").val(report_name);
    $("#hdnStoreId").val(store_id);
    //$('#divOpenPostageInvoices').show();
    //$('#divOpenAccountsReceivable').show();
    //$('#divDailyMilestones').show();
    //$('#divJobInvoiceSummary').show();
    //$('#divProductionRelease').show();
    //$('#divLettershopStatus').show();
	$("#selectOutputType").val('pdf').selectmenu('refresh');
    $('#divSelectStoreId').hide();
    report_name = report_name.replace(/_/g, '');
    switch (report_name) {
        case "OpenPostageInvoices":
            $('#divOpenPostageInvoices').show();
            $('#divOpenAccountsReceivable').hide();
            $('#divJobInvoiceSummary').hide();
            $('#divDailyMilestones').hide();
            $('#divProductionRelease').hide();
            $('#divLettershopStatus').hide();
            $('#divSelectStoreId').hide();
            $('#dvIssueDate').hide();
            $('#divOpenJobs').hide();
            $("#txtCutomerIdPostageInvoices").val(appPrivileges.customerNumber);
            break;
        case "OpenAccountsReceivable":
            $('#divOpenAccountsReceivable').show();
            $('#divOpenPostageInvoices').hide();
            $('#divJobInvoiceSummary').hide();
            $('#divDailyMilestones').hide();
            $('#divProductionRelease').hide();
            $('#divLettershopStatus').hide();
            $('#divSelectStoreId').hide();
            $('#dvIssueDate').hide();
            $('#divOpenJobs').hide();
            $("#txtCutomerIdAccountsReceivable").val(appPrivileges.customerNumber);
            break;
        case "JobInvoiceSummary":
            $('#divJobInvoiceSummary').show();
            $('#divOpenPostageInvoices').hide();
            $('#divOpenAccountsReceivable').hide();
            $('#divDailyMilestones').hide();
            $('#divProductionRelease').hide();
            $('#divLettershopStatus').hide();
            $('#divSelectStoreId').hide();
            $('#dvIssueDate').hide();
            $('#divOpenJobs').hide();
            $("#txtCutomerIdInvoiceSummary").val(appPrivileges.customerNumber);
            //$("#txtJobNumberInvoiceSummary").val(jobNumber);
            //$("#txtFacilityIdInvoiceSummary").val(facilityId);
            $("#txtFacilityIdInvoiceSummary").val(sessionStorage.facilityId);
            //$("#txtJobNumberInvoiceSummary").val(jobNumber);
            break;
        case "DailyMilestones":
            $('#divDailyMilestones').show();
            $('#divOpenAccountsReceivable').hide();
            $('#divOpenPostageInvoices').hide();
            $('#divJobInvoiceSummary').hide();
            $('#divProductionRelease').hide();
            $('#divLettershopStatus').hide();
            $('#divSelectStoreId').hide();
            $('#dvIssueDate').hide();
            $('#divOpenJobs').hide();
            $("#txtCutomerIdDailyMilestones").val(appPrivileges.customerNumber);
            break;
        case "rptBillingInvoice":
        case "rptPostageRequest":
            $('#divJobInvoiceSummary').hide();
            $('#divOpenPostageInvoices').hide();
            $('#divOpenAccountsReceivable').hide();
            $('#divDailyMilestones').hide();
            $('#divProductionRelease').hide();
            $('#divLettershopStatus').hide();
            $('#divSelectStoreId').show();
            $('#dvIssueDate').hide();
            $('#divIsDPMailStreamSummaryReport').hide();
            $('#divIsDPRejectDetailsReport').hide();
            $('#divOpenJobs').hide();
            break;
        case "ArtworkSummary":
            $('#divJobInvoiceSummary').show();
            $('#divOpenPostageInvoices').hide();
            $('#divOpenAccountsReceivable').hide();
            $('#divDailyMilestones').hide();
            $('#divProductionRelease').hide();
            $('#divLettershopStatus').hide();
            $('#divSelectStoreId').hide();
            $('#dvIssueDate').hide();
            $('#divOpenJobs').hide();
            $("#txtCutomerIdInvoiceSummary").val(appPrivileges.customerNumber);
            //$("#txtJobNumberInvoiceSummary").val(jobNumber);
            //$("#txtFacilityIdInvoiceSummary").val(facilityId);
            $("#txtFacilityIdInvoiceSummary").val(sessionStorage.facilityId);
            $('#dvJobNumberInvoiceSummary').css('display', 'none');
            $('#dvFacilityIdInvoiceSummary').css('display', 'block');
            $('#dvStartEndDateInvoiceSummary').css('display', 'none');
            //$("#txtJobNumberInvoiceSummary").val(jobNumber);
            if (appPrivileges.customerNumber == DCA || appPrivileges.customerNumber == SPORTSKING) {
                $('#dvFacilityIdInvoiceSummary label[for="txtFacilityIdInvoiceSummary"]').hide();
                $('#txtFacilityIdInvoiceSummary').parent().css('display', 'none');

                $('#divJobInvoiceSummary label[for="txtCutomerIdInvoiceSummary"]').hide();
                $('#txtCutomerIdInvoiceSummary').parent().css('display', 'none');
            }
            break;
        case "PostageSummary":
        case "GeolocationSummary":
            $('#divJobInvoiceSummary').show();
            $('#divOpenPostageInvoices').hide();
            $('#divOpenAccountsReceivable').hide();
            $('#divDailyMilestones').hide();
            $('#divProductionRelease').hide();
            $('#divLettershopStatus').hide();
            $('#divSelectStoreId').hide();
            $('#dvIssueDate').hide();
            $('#divOpenJobs').hide();
            $("#txtCutomerIdInvoiceSummary").val(appPrivileges.customerNumber);
            //$("#txtJobNumberInvoiceSummary").val(jobNumber);
            //$("#txtFacilityIdInvoiceSummary").val(facilityId);
            $("#txtFacilityIdInvoiceSummary").val(sessionStorage.facilityId);
            $('#dvJobNumberInvoiceSummary').css('display', 'none');
            $('#dvFacilityIdInvoiceSummary').css('display', 'none');
            $('#dvStartEndDateInvoiceSummary').css('display', 'block');
            //$("#txtJobNumberInvoiceSummary").val(jobNumber);
            if (appPrivileges.customerNumber == DCA || appPrivileges.customerNumber == SPORTSKING) {
                $('#divJobInvoiceSummary label[for="txtCutomerIdInvoiceSummary"]').hide();
                $('#txtCutomerIdInvoiceSummary').parent().css('display', 'none');
                $('#txtStartDateInvoiceSummary').datepicker('setDate', '01/01/' + (new Date()).getFullYear());
                $('#txtEndDateInvoiceSummary').datepicker('setDate', report_name == "GeolocationSummary" ?  (new Date()) : (new Date(date.getFullYear(), (date.getMonth() + 2), 0)));
            }
            break;
        case "ProductionReleaseReport":
            $('#divProductionRelease').show();
            $('#divOpenPostageInvoices').hide();
            $('#divOpenAccountsReceivable').hide();
            $('#divJobInvoiceSummary').hide();
            $('#divDailyMilestones').hide();
            $('#divLettershopStatus').hide();
            $('#divSelectStoreId').hide();
            $('#divOpenJobs').hide();
            //$("#txtCutomerIdPostageInvoices").val(appPrivileges.customerNumber);
            break;
        case "LettershopProductionReport":
            $('#divLettershopStatus').show();
            $('#divOpenPostageInvoices').hide();
            $('#divOpenAccountsReceivable').hide();
            $('#divJobInvoiceSummary').hide();
            $('#divDailyMilestones').hide();
            $('#divProductionRelease').hide();
            $('#divSelectStoreId').hide();
            $('#dvIssueDate').hide();
            $('#divOpenJobs').hide();
            //$("#txtCutomerIdPostageInvoices").val(appPrivileges.customerNumber);
            break;
        case "Materialsat2.5%":
        case "MaterialsNotReceived":
        case "MaterialswithWeights":
        case "LateDelivery":
            $('#divLettershopStatus').hide();
            $('#divOpenPostageInvoices').hide();
            $('#divOpenAccountsReceivable').hide();
            $('#divJobInvoiceSummary').hide();
            $('#divDailyMilestones').hide();
            $('#divProductionRelease').hide();
            $('#divSelectStoreId').hide();
            $('#divOpenJobs').hide();
            $('#dvIssueDate').show();
            break;
        case "OpenJobs":
            $('#divJobInvoiceSummary').hide();
            $('#divOpenPostageInvoices').hide();
            $('#divOpenAccountsReceivable').hide();
            $('#divDailyMilestones').hide();
            $('#divProductionRelease').hide();
            $('#divLettershopStatus').hide();
            $('#divSelectStoreId').hide();
            $('#dvIssueDate').hide();
            $('#divOpenJobs').show();
            $("#txtCutomerIdInvoiceSummary").val(appPrivileges.customerNumber);
            $("#txtFacilityIdInvoiceSummary").val(sessionStorage.facilityId);
            $('#dvJobNumberInvoiceSummary').css('display', 'none');
            $('#dvFacilityIdInvoiceSummary').css('display', 'none');
            $('#dvStartEndDateInvoiceSummary').css('display', 'none');
            //$("#txtJobNumberInvoiceSummary").val(jobNumber);
            if (appPrivileges.customerNumber == DCA || appPrivileges.customerNumber == SPORTSKING) {
                $('#divOpenJobs label[for="txtCutomerIdOpenJobs"]').hide();
                $('#txtCutomerIdOpenJobs').parent().css('display', 'none');

                $('#divOpenJobs label[for="txtFacilityIdOpenJobs"]').hide();
                $('#txtFacilityIdOpenJobs').parent().css('display', 'none');

                $('#divOpenJobs label[for="txtStageOpenJobs"]').hide();
                $('#txtStageOpenJobs').parent().css('display', 'none');
                $('#lblPositiveJobNumbers').css('display', 'none');
            }
            break;
        case "PostageAdvanceInvoice":
            $('#divOpenAccountsReceivable').show();
            $('#divOpenPostageInvoices').hide();
            $('#divJobInvoiceSummary').hide();
            $('#divDailyMilestones').hide();
            $('#divProductionRelease').hide();
            $('#divLettershopStatus').hide();
            $('#divSelectStoreId').hide();
            $('#dvIssueDate').hide();
            $('#divOpenJobs').hide();
            $("#txtCutomerIdAccountsReceivable").val(appPrivileges.customerNumber);
            break;
        case "BillingInvoice":
            $('#divOpenAccountsReceivable').show();
            $('#divOpenPostageInvoices').hide();
            $('#divJobInvoiceSummary').hide();
            $('#divDailyMilestones').hide();
            $('#divProductionRelease').hide();
            $('#divLettershopStatus').hide();
            $('#divSelectStoreId').hide();
            $('#dvIssueDate').hide();
            $('#divOpenJobs').hide();
            $("#txtCutomerIdAccountsReceivable").val(appPrivileges.customerNumber);
            break;
    };
    $('#txtStartDateInvoiceSummary').datepicker('disable');
    $('#txtEndDateInvoiceSummary').datepicker('disable');
    $(document).on("popupafteropen", "#popupReportsParam", function (event, ui) {
        $('#txtStartDateInvoiceSummary').datepicker('enable');
        $('#txtEndDateInvoiceSummary').datepicker('enable');
    });
    $('#txtJobNumberInvoiceSummary').val('');
    $('#dvIssueDate').val('');
    $("#popupReportsParam").popup("open");    
    //$('#txtStartDateInvoiceSummary').val('');
    //$('#txtEndDateInvoiceSummary').val('');
    
}

function updateAccountingReportsCancel() {
    $("#popupReportsParam").popup("open");
    $("#popupReportsParam").popup("close");
}
function fnReportsValidation() {
    var rpt_selected = $('#hdnReportSelected').val();
    var msg;
    rpt_selected = rpt_selected.replace(/_/g, '');
    switch (rpt_selected) {
        case "OpenPostageInvoices":
            if ($('#txtCutomerIdPostageInvoices').val() == "")
                if (msg != undefined && msg != "" && msg != null)
                    msg += "<br />Please enter Customer Id";

                else
                    msg = "Please enter Customer Id";
            break;
        case "OpenAccountsReceivable":
            if ($('#txtCutomerIdAccountsReceivable').val() == "")
                if (msg != undefined && msg != "" && msg != null)
                    msg += "<br />Please enter Customer Id";

                else
                    msg = "Please enter Customer Id";
            break;
        case "DailyMilestones":
            if ($('#txtCutomerIdDailyMilestones').val() == "") {
                if (msg != undefined && msg != "" && msg != null)
                    msg += "<br />Please enter Customer Id";

                else
                    msg = "Please enter Customer Id";

            }
            break;
        case "JobInvoiceSummary":
            if ($('#txtFacilityIdInvoiceSummary').val() == "") {
                msg = "Please enter Facility Id";
            }
            break;
        case "ProductionReleaseReport":
            if ($('#txtFacilityIdProduction').val() == "") {
                if (msg != undefined && msg != "" && msg != null)
                    msg += "<br />Please enter Facility Id";

                else
                    msg = "Please enter Facility Id";

            }
            break;
        case "LettershopProductionReport":
            if ($('#txtFacilityIdLettesrShop').val() == "") {
                if (msg != undefined && msg != "" && msg != null)
                    msg += "<br />Please enter Facility Id";

                else
                    msg = "Please enter Facility Id";
            }
            break;
        case "rptBillingInvoice":
        case "rptPostageRequest":
            if ($('#ddlStoreId').val() == "select") {
                if (msg != undefined && msg != "" && msg != null)
                    msg += "<br />Please select Store Id";

                else
                    msg = "Please select Store Id";
            }
            break;
        case "Materialsat2.5%":
        case "MaterialsNotReceived":
        case "MaterialswithWeights":
        case "LateDelivery":
            if ($('#txtIssueDate').val() == "") {
                msg = "Please enter Issue Date";
            }
            break;
    }
    if (msg != undefined && msg != null && msg != "") {
        $("#popupReportsParam").popup('open');
        $("#popupReportsParam").popup('close');
        $('#alertmsg').html(msg);
        $("#popupDialog").popup('open');
        $("#okBut").attr("onclick", "$('#popupDialog').popup('close');$('#popupReportsParam').popup('open');");
        return false;
    }
    $("#okBut").attr("onclick", "$('#popupDialog').popup('close');");
    return true;
}

//if ($('#txtCutomerIdPostageInvoices').val() == "" || $('#txtCutomerIdAccountsReceivable').val() == "") {
//    $('#popupReportsParam').popup('close');
//    $('#alertmsg').html('Please enter Customer Id ');
//    $("#popupDialog").popup('open');
//    $("#okBut").attr("onclick", "$('#popupDialog').popup('close');");
//}
////if(

function updateAccountingReportsVal() {
    if (!fnReportsValidation())
        return false;
    if (!fnDateFormatMMDDYYYY('txtStartDateInvoiceSummary', 1)) //date format validation
        return false;
    if (!fnDateFormatMMDDYYYY('txtEndDateInvoiceSummary', 1)) //date format validation
        return false;
    if (!fnStartEndDateValidation($('#txtStartDateInvoiceSummary').val(), $('#txtEndDateInvoiceSummary').val()))
        return false;

    var output_type = $("#selectOutputType").val();
    var report_name = $("#hdnReportSelected").val();
    var st_date_invoice = $("#txtStartDateInvoiceSummary").val();
    var end_date_invoice = $("#txtEndDateInvoiceSummary").val();
    var input_report_params = [];
    var customer_id = appPrivileges.customerNumber;
    var facility_id = appPrivileges.facility_id;
    ssrsFolder = "acctingrpts";
    report_name = report_name.replace(/_/g, ' ');
    $("#popupReportsParam").popup("close");
    switch (report_name) {
        case "Open Postage Invoices":
            input_report_params.push({
                "name": "customerId",
                "value": $("#txtCutomerIdPostageInvoices").val()
            });
            break;
        case "Open Accounts Receivable":
            input_report_params.push({
                "name": "customerId",
                "value": $("#txtCutomerIdAccountsReceivable").val()
            });
            break;
        case "Daily Milestones":
            input_report_params.push({
                "name": "customerId",
                "value": $("#txtCutomerIdDailyMilestones").val()
            });
            ssrsFolder = "gridrpts";
            break;
        case "Job Invoice Summary":
            input_report_params.push({
                "name": "customerId",
                "value": $("#txtCutomerIdInvoiceSummary").val()
            });
            input_report_params.push({
                "name": "facilityId",
                "value": $("#txtFacilityIdInvoiceSummary").val()
            });
            input_report_params.push({
                "name": "jobNumber",
                "value": $("#txtJobNumberInvoiceSummary").val()
            });
            input_report_params.push({
                "name": "startDate",
                "value": st_date_invoice
            });
            input_report_params.push({
                "name": "endDate",
                "value": end_date_invoice
            });
            break;

        case "Artwork Summary":
            input_report_params.push({
                "name": "customerId",
                "value": $("#txtCutomerIdInvoiceSummary").val()
            });
            input_report_params.push({
                "name": "facilityId",
                "value": $("#txtFacilityIdInvoiceSummary").val()
            });
            ssrsFolder = "Gizmo";
            report_name = "ArtworkSummary"
            var env_id = getEnvironmentId();
            input_report_params.push({
                "name": "stageOrPrd",
                "value": env_id
            });
            break;
        case "Postage Summary":
        case "Geolocation Summary":
            input_report_params.push({
                "name": "customerId",
                "value": $("#txtCutomerIdInvoiceSummary").val()
            });
            input_report_params.push({
                "name": "beginDate",
                "value": st_date_invoice
            });
            input_report_params.push({
                "name": "endDate",
                "value": end_date_invoice
            });
            var env_id = getEnvironmentId();
            input_report_params.push({
                "name": "stageOrPrd",
                "value": env_id
            });
            ssrsFolder = "gizmo";
            if (report_name == "Postage Summary")
                report_name = "Postage Request Summary";
            else if (report_name == "Geolocation Summary")
                report_name = "Geo Locations by Date";
            break;
        case "rptPostageRequest":
        case "rptBillingInvoice":
            input_report_params.push({
                "name": "customerId",
                "value": customer_id
            });
            input_report_params.push({
                "name": "facilityId",
                "value": (typeof pageObj != 'undefined' && typeof pageObj != 'null') ? pageObj.facilityId : facilityId
            });
            input_report_params.push({
                "name": "jobNumber",
                "value": (typeof pageObj != 'undefined' && typeof pageObj != 'null') ? pageObj.jobNumber : jobNumber
            });
            input_report_params.push({
                //"name": (report_name == "rptPostageRequest") ? "storePk" : "storeId",
                "name": "storePk",
                //"name": "storeId",
                "value": $("#ddlStoreId").val()
            });
            //ssrsFolder = "gridrpts";
            ssrsFolder = "gizmo";
            break;
        case "Lettershop Production Report":
        case "Production Release Report":
            ssrsFolder = "ProductionScheduling";
            input_report_params.push({
                "name": "facilityId",
                "value": facility_id
            });
            break;
        case "Open Jobs":
            input_report_params.push({
                "name": "customerId",
                "value": customer_id
            });
            input_report_params.push({
                "name": "facilityId",
                "value": (typeof pageObj != 'undefined' && typeof pageObj != 'null') ? pageObj.facilityId : facilityId
            });
            var env_id = getEnvironmentId();
            input_report_params.push({
                "name": "stageOrPrd",
                "value": env_id
            });
            ssrsFolder = "gizmo";
            break;
    };
    createJSONForReport(report_name, output_type, input_report_params, ssrsFolder, $("#txtJobNumberInvoiceSummary").val());

}
function getEnvironmentId() {
    var host_name = "";
    var parser = document.createElement('a');
    parser.href = window.location.href;
    // env = 1: dev, 2: demo, 3: live
    parser.protocol; // => "http:"
    parser.host;     // => "example.com:3000"
    parser.hostname; // => "example.com"
    parser.port;     // => "3000"
    parser.pathname; // => "/pathname/"
    parser.hash;     // => "#hash"
    parser.search;
    var host_name_parts = parser.hostname.split('.');
    var ret_value = "";
    $.each(environments, function (key, val) {
        if (window.location.href.toLowerCase().indexOf(val.toLowerCase()) > -1 || window.location.href.toLocaleLowerCase().indexOf('tddev') > -1 || host_name_parts.length > 3)
            ret_value = 1;
    });
    if (ret_value == "") {
        if (window.location.href.toLocaleLowerCase().indexOf('tddemo') > -1)
            ret_value = 2;
        else
            ret_value = 3;
    }
    return ret_value;
}

//------------------ New Reports Implementation START - 12292016------------------
function openReport(report_name, output_type, selected_report_json) {
    var content_type = '';
    var htmlText = "";
    //removeLoadingImg();
    if (output_type == "xls")
        output_type = "excel"
    $('#popPDF').popup('close');
    switch (output_type) {
        case "pdf":
            content_type = 'application|pdf'
            break;
        case "doc":
            content_type = 'application|msword'
            break;
        case "html":
            content_type = 'text|html'
            break;
        case "xls":
        case "excel":
            content_type = 'application|x-msexcel'
            //content_type = 'application|vnd.ms-excel';
            break;
            //case "image":
            //    content_type = ' type="image|tiff" src="data:image/tiff;base64,'
            //    break;
    }
    var report_url = serviceURLDomain + "api/SsrsReport_get/" + sessionStorage.facilityId + "/" + jobCustomerNumber + "/" + sessionStorage.jobNumber + "/" + report_name + "/" + content_type + "/" + output_type;
    var htmlText = "";
    $('#popPDF').popup('close');
    $('#waitPopUp').popup('close');


    window.setTimeout(function () {
        $('#popPDF').popup('open', { positionTo: 'window' });
        //if (output_type == 'pdf' || output_type == 'xml') {
            var wait_ctrl = '<div id="dvWaitImage" >';
            $('#waitPopUp').popup('open');
            $('#waitPopUp').popup('close');
            wait_ctrl += $('#waitPopUp').html() + "</div>";
            $('#popupPDFContent').prepend(wait_ctrl);
            //var maxHeight = ($('#popPDF').height() / 2) - 190 + "px";
            //var maxWidth = ($('#popPDF').width() / 2 - 80) + "px";
            var maxHeight = ($('#popPDF').height() / 2) - 190 + "px";
            var maxWidth = (($('#popPDF').width() / 2) -70) + "px";
            $('#dvWaitImage').css('position', 'absolute');
            $("#dvWaitImage").css("margin-top", maxHeight);
            $("#dvWaitImage").css("margin-left", maxWidth);

            $('#popPDF').popup('open', { positionTo: 'window' });
        //}
        var xhr = new XMLHttpRequest();
        xhr.open("POST", report_url);
        xhr.responseType = "arraybuffer";
        xhr.setRequestHeader("Authorization", sessionStorage.authString);
        xhr.setRequestHeader("Accept", "application/json");
        xhr.setRequestHeader("Content-type", "application/json");
        xhr.onload = function () {
            if (this.status === 200) {
                
                if (output_type != 'pdf') {
                    var a = document.createElement('a');
                    a.href = URL.createObjectURL(new Blob([xhr.response], { type: content_type.replace('|', '/') })); // xhr.response is a blob
                    a.download = report_name; // Set the file name.
                    a.style.display = 'none';
                    document.body.appendChild(a);
                    a.click();
                    delete a;
                    $('#popPDF').popup('close');
                }
                else {
                    var blob = new Blob([xhr.response], { type: content_type.replace('|', '/') });
                    var objectUrl = URL.createObjectURL(blob);
                    var is_msie = msieversion();
                    if (is_msie) {
                        var url = objectUrl.substring(0, objectUrl.indexOf(":"));
                        url += ':serviceURLDomain';
                        url += objectUrl.substring(objectUrl.indexOf(":") + 1);
                        //url = encodeURIComponent(url);
                        generatePDFViewer(content_type, url, output_type);
                    } else
                        generatePDFViewer(content_type, objectUrl, output_type);
                }
            }
            else if (this.status == 417) {
                $('#popPDF').popup('close');
                showErrorResponseText(this, false);
            }
        };
        xhr.onerror = function (e) {
            $('#popPDF').popup('close');
            showErrorResponseText(e.target, false);
        };
        xhr.send(JSON.stringify(selected_report_json));
    }, 1000);



}

function msieversion() {

    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");

    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))  // If Internet Explorer, return version number
    {
        //alert(parseInt(ua.substring(msie + 5, ua.indexOf(".", msie))));
        return true;
    }
    else  // If another browser, return 0
    {
        return false;
        alert('otherbrowser');
    }
}

function fnGetBrowserVersion() {
    var N = navigator.appName, ua = navigator.userAgent, tem;
    //var M = ua.match(/(opera|chrome|safari|firefox|msie)\/?\s*(\.?\d+(\.\d+)*)/i);
    var M = ua.match(/(chrome|safari|firefox|msie)\/?\s*(\.?\d+(\.\d+)*)/i);
    if (M && (tem = ua.match(/version\/([\.\d]+)/i)) != null) M[2] = tem[1];
    M = M ? [M[1], M[2]] : [N, navigator.appVersion, '-?'];
    return M[1];
}

function generatePDFViewer(content_type, pdfData, output_type) {
    //htmlText = '<iframe width=850 HEIGHT=500 id="rptFrame"'
    //   + content_type.replace('|','/')
    //   + ' src="' + pdfData
    //   + '"></iframe>';
    //if (output_type == 'pdf' || output_type == 'xml') {
    //    $('#popupPDFContent').html(htmlText);
    //} else {
    //    $('#popupPDFContent').html(htmlText);
    //}

    var iframe = document.getElementById('rptFrame');
    iframe.src = pdfData;
    iframe.type = content_type.replace('|', '/');
    if (output_type == 'pdf' || output_type == 'xml') {
        //DOMContentLoaded
        if (iframe.addEventListener) {
            iframe.addEventListener('onload', callback(output_type), true);
        } else {
            iframe.attachEvent('onload', callback(output_type));
        }
    }
    else {
        $('#popPDF').popup('close');
    }
}
//------------------ END ---------------------------------------------------------