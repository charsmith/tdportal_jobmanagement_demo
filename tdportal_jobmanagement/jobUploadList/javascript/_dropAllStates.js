﻿var dropAllStatesObj = function (self) {
    self.statesJson = [];
    self.selectedStates = [];
    //Loads states JSON into page level variable.
    self.loadStatesToJson = function () {
        $.getJSON(self.gStateServiceUrl, function (dataStates) {
            dataStates.splice(0, 1);
            self.statesJson = dataStates;
            self.fillStates('ulStates', 'statesTemplate', self.statesJson, true);
        });
    };

    //Lists the states in the UI
    self.fillStates = function (ctrl, template_to_bind, json_to_bind, is_page_load) {
        $('#' + ctrl).empty();
        if (ctrl.toLowerCase().indexOf('selected') > -1)
            $("#" + template_to_bind).tmpl(json_to_bind).attr('data-icon', 'delete').appendTo("#" + ctrl);
        else {
            $("#" + template_to_bind).tmpl(json_to_bind).appendTo("#" + ctrl);

        }
        if (self.selectedStates.length > 0) {
            $.each(self.selectedStates, function (a, b) {
                if (ctrl.toLowerCase().indexOf('selected') == -1)
                    $('#' + ctrl).find('li[id=li' + b.stateValue + ']').remove();
            });
            if (ctrl.toLowerCase().indexOf('selected') > -1)
                $('#' + ctrl).prepend('<li data-theme="d" data-role="list-divider" >Selected States</li>');
        }
        window.setTimeout(function getDealy() { $('#' + ctrl).listview('refresh'); }, 500);
    };
    //Adds the selected state to the Selected States list.
    self.addStates = function (state_value, state_name, ctrl) {
        $('#li' + state_value).find('a').removeAttr('onclick');
        window.setTimeout(function getDelay() { self.addSelectedStates(state_value, state_name, ctrl); }, 1000);
    };

    //Updates the list of selected states into the UI.
    self.addSelectedStates = function (state_value, state_name, ctrl) {
        self.selectedStates.push({ "stateName": state_name, "stateValue": state_value });
        self.selectedStates = self.selectedStates.sort(function (a, b) {
            return a.stateName > b.stateName ? 1 : -1;
        });
        $('#' + ctrl).find('li[id=li' + state_value + ']').remove();
        self.fillStates('ulSelectedStates', 'selectedStatesTemplate', self.selectedStates, false);
        if ($('#ulSelectedStates li:not([id^=li])').length == 0)
            $('#ulSelectedStates').prepend('<li data-role="list-divider" >Selected States</li>');
        $('#ulSelectedStates').listview('refresh');
        if (self.selectedStates.length > 0)
            $('#ulSelectedStates').show();
    };
    //Removes the selected state from Selected States list.
    self.removeStates = function (state_value) {
        window.setTimeout(function getDelay() { self.removeSelectedStates(state_value); }, 1000);
    }

    //Updates the states and selected states lists in UI.
    self.removeSelectedStates = function (state_value) {
        var count = 0;
        $.each(self.selectedStates, function (a, b) {
            if (b.stateValue.toLowerCase() == state_value.toLowerCase())
                return false;
            else
                count++
        });
        self.selectedStates.splice(count, 1);
        if (self.selectedStates.length == 0)
            $('#ulSelectedStates').hide();
        self.fillStates('ulSelectedStates', 'selectedStatesTemplate', self.selectedStates, false);
        if ($('#ulSelectedStates li:not([id^=li])').length == 0)
            $('#ulSelectedStates').prepend('<li data-role="list-divider" >Selected States</li>');
        $('#ulSelectedStates').listview('refresh');
        self.fillStates('ulStates', 'statesTemplate', self.statesJson, false);
    }

    //Persists the selected states into the Job Ticket JSON in location session.
    self.persistDropAllStatesExcept = function () {
        var selected_states = "";
        $.each($('#ulSelectedStates li[id^=li]'), function (a, b) {
            //selected_states += (selected_states != "") ? ',' + $(b).find('a').attr('value') : $(b).find('a').attr('value');
            selected_states += (selected_states != "") ? ',' + "'" + $(b).find('a').attr('value') + "'" : "'" + $(b).find('a').attr('value') + "'";
        });
        if (sessionStorage.jobSetupOutput != undefined)
            gOutputData = jQuery.parseJSON(sessionStorage.jobSetupOutput);

        if (gOutputData.mergePurgeAction != undefined && gOutputData.mergePurgeAction != null) {
            if (gOutputData.mergePurgeAction.optionalAttributesDict != undefined && gOutputData.mergePurgeAction.optionalAttributesDict != null) {
                if (gOutputData.mergePurgeAction.optionalAttributesDict.dropAllStatesExcept != undefined)
                    gOutputData.mergePurgeAction.optionalAttributesDict.dropAllStatesExcept = selected_states;
                if (gOutputData.mergePurgeAction.optionalAttributesDict.keepAllKeycodedAs != undefined)
                    gOutputData.mergePurgeAction.optionalAttributesDict["keepAllKeycodedAs"] = $('#txtKeyCode').val();
            }
            else {
                gOutputData.mergePurgeAction["optionalAttributesDict"] = {};
                gOutputData.mergePurgeAction.optionalAttributesDict["dropAllStatesExcept"] = selected_states;
                gOutputData.mergePurgeAction.optionalAttributesDict["keepAllKeycodedAs"] = $('#txtKeyCode').val();
            }

        }
        sessionStorage.jobSetupOutput = JSON.stringify(gOutputData);
    }

    //Displays the list selected states in Job Ticket JSON from session.
    self.displayDropAllStatesExcept = function () {
        var drop_all_states_except = "";
        if (self.gOutputData.mergePurgeAction != undefined && self.gOutputData.mergePurgeAction != null) {
            if (self.gOutputData.mergePurgeAction.optionalAttributesDict != undefined && self.gOutputData.mergePurgeAction.optionalAttributesDict != null) {
                if (self.gOutputData.mergePurgeAction.optionalAttributesDict.dropAllStatesExcept != undefined && self.gOutputData.mergePurgeAction.optionalAttributesDict.dropAllStatesExcept != null)
                    drop_all_states_except = self.gOutputData.mergePurgeAction.optionalAttributesDict.dropAllStatesExcept;
                if (self.gOutputData.mergePurgeAction.optionalAttributesDict.keepAllKeycodedAs != undefined || self.gOutputData.mergePurgeAction.optionalAttributesDict.keepAllKeycodedAs != null)
                    $('#txtKeyCode').val((self.gOutputData.mergePurgeAction.optionalAttributesDict.keepAllKeycodedAs != "") ? self.gOutputData.mergePurgeAction.optionalAttributesDict.keepAllKeycodedAs : "05100100");
            }
        }

        if (drop_all_states_except != undefined && drop_all_states_except != "" && drop_all_states_except.length > 0) {
            var selected_drop_all_states = (drop_all_states_except != undefined && drop_all_states_except != "") ? drop_all_states_except.trim().split(',') : "";
            if (selected_drop_all_states.length > 0) {
                $('#ulSelectedStates').empty();
                $.each(selected_drop_all_states, function (a, b) {
                    var ctrl = b.replace(/'/g, "")
                    if ($('#ulStates').find('[id=li' + ctrl + ']').length > 0) {
                        self.selectedStates.push({ "stateName": $('#ulStates').find('[id=li' + ctrl + ']').find('a').text(), "stateValue": $('#ulStates').find('[id=li' + ctrl + ']').find('a').attr('value') });
                    }
                });
                $('#ulSelectedStates').show();
                self.fillStates('ulSelectedStates', 'selectedStatesTemplate', self.selectedStates, false);
                self.fillStates('ulStates', 'statesTemplate', self.statesJson, false);
            }
        }
    }
};