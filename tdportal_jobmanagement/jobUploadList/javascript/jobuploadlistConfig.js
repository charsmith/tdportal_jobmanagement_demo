﻿//jobSetupUploadList.js
isCWUploadListPreferences = function () {
    var customer = getCustomerNumber();    
    return ([DKI, ICYNENE, REGIS, SCOTTS, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER, BRIGHTHOUSE, BBB, AAG, TRIBUNEPUBLISHING, SANDIEGO, AH, SPORTSAUTHORITY, WALGREENS, DATAWORKS, SAFEWAY, LOWES, DCA, KUBOTA, OH, SPORTSKING, GWA, REDPLUM, BUFFALONEWS, PIONEERPRESS, EQUINOX].indexOf(customer) > -1);
}

isCreateUploadListDemoHints = function () {
    var customer = getCustomerNumber();    
    return ([DKI, ICYNENE, BBB, AAG, TRIBUNEPUBLISHING, SANDIEGO, AH, SPORTSAUTHORITY, WALGREENS, SAFEWAY, LOWES, CW, DCA, KUBOTA, OH, SPORTSKING, GWA, CASEYS, SCOTTS, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER, EQUINOX].indexOf(customer) > -1);
}

showLocationRequiredValidation = function () {
    var customer = getCustomerNumber();    
    return ([DKI, ICYNENE, CW, SCOTTS, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER, KUBOTA, SPORTSKING, GWA, BUFFALONEWS, PIONEERPRESS, REDPLUM].indexOf(customer) > -1);
}

showUploadSelectedListDemoHints = function () {
    var customer = getCustomerNumber();
    return ([BBB, AAG, SPORTSAUTHORITY, WALGREENS, SAFEWAY, LOWES, CW, SCOTTS, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);
}

showUploadSelectedListDemoHintsInUpdateFileList = function () {
    var customer = getCustomerNumber();    
    return ([BBB, AAG, TRIBUNEPUBLISHING, SANDIEGO, AH, SPORTSAUTHORITY, WALGREENS, SAFEWAY, LOWES, CW, OH, SCOTTS, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);
}

showListUploadDemoHints = function () {
    var customer = getCustomerNumber();
    return ([BBB, AAG, TRIBUNEPUBLISHING, SANDIEGO, AH, SPORTSAUTHORITY, WALGREENS, SAFEWAY, LOWES, CW, OH].indexOf(customer) > -1);
}

showListUploadingDemoHints = function () {
    var customer = getCustomerNumber();    
    return ([DCA, KUBOTA, SPORTSKING, GWA, SCOTTS, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);
}

isLoadOrderedLocations = function (customer_number) {
    var customer = customer_number;
    if (customer == undefined || customer == null || customer == "") {
        customer = getCustomerNumber();
    }
    return ([CW, SCOTTS, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER, KUBOTA, SPORTSKING, CW, GWA].indexOf(customer) > -1);
}

isLoadLocationsToMakeFileList = function () {
    var customer = getCustomerNumber();
    return ([CW, SCOTTS, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER, BRIGHTHOUSE, OH, KUBOTA, SPORTSKING, GWA, BUFFALONEWS, PIONEERPRESS, REDPLUM].indexOf(customer) > -1);
}

isShowEditDialog = function () {
    var customer = getCustomerNumber();
    return ([CW, SCOTTS, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER, OH, KUBOTA, SPORTSKING, GWA, BUFFALONEWS, PIONEERPRESS, REDPLUM].indexOf(customer) > -1);
}

isLinkedToRequiredInFileInfo = function () {
    var customer = getCustomerNumber();    
    return ([KUBOTA, SPORTSKING, GWA, SCOTTS, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER, BUFFALONEWS, PIONEERPRESS, REDPLUM].indexOf(customer) == -1);
}

isNeedsCountSetToTrue = function () {
    var customer = getCustomerNumber();
    return ([CW, SCOTTS, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER, BRIGHTHOUSE, BBB, AAG, TRIBUNEPUBLISHING, SANDIEGO, AH, SPORTSAUTHORITY, WALGREENS, DATAWORKS, SAFEWAY, LOWES].indexOf(customer) > -1);
}

