﻿$.mobile.popup.prototype.options.history = false;
jQuery.support.cors = true;
var pageObj;
var isDragDrop = false;
var isMultiple = false;
var formData;
var data = [];
var temp_data = [];
var temp_edit_file = [];
var dontShowHintsAgain = (localStorage.getItem('doNotShowHintsAgain') != undefined && localStorage.getItem('doNotShowHintsAgain') != null && localStorage.getItem('doNotShowHintsAgain') != "" && localStorage.getItem('doNotShowHintsAgain') != "false") ? true : false;
//******************** Page Load Events Start **************************

$('#_jobSetupUploadList').live('pagebeforecreate', function (event) {
    displayNavLinks();
    displayMessage('_jobSetupUploadList');

    pageObj = new manageUploadLists();
    pageObj.displayValidationMessage('_jobSetupUploadList');
    //confirmMessage();
    createConfirmMessage('_jobSetupUploadList');
    pageObj.loadStatesToJson();
    if (sessionStorage.ListManagementPrefs == undefined || sessionStorage.ListManagementPrefs == null || sessionStorage.ListManagementPrefs == "")
        if (isCWUploadListPreferences())
            getPagePreferences('ListManagement.html', sessionStorage.facilityId, CW);
        else
            getPagePreferences('ListManagement.html', sessionStorage.facilityId, jobCustomerNumber);
    else {
        managePagePrefs(jQuery.parseJSON(sessionStorage.ListManagementPrefs));
    }
    //load IFRAME for IE to upload files.
    loadUploadIFrame();
    //test for mobility...
    //loadMobility();
    $("#navLinksPanel").on("panelopen", function (event, ui) {
        sessionStorage.isNavPanelOpen = true;
    });
    sessionStorage.removeItem('filesToUpload');

    if (isCreateUploadListDemoHints()) {
        setDemoHintsSliderValue();
        window.setTimeout(function loadHints() {
            createDemoHints("jobUploadList");
        }, 200);
    }
});

$(document).keyup(function (e) {
    if (e.keyCode == 27) {
        return false;
    }
});

$(window).on('popupbeforeposition', 'div:jqmData(role="popup")', function () {
    var notDismissible = $(this).jqmData('dismissible') === false;
    if (notDismissible) {
        $('.ui-popup-screen').off();
    }
});

$(document).on('pageshow', '#_jobSetupUploadList', function (event) {

    $('#popupListDefinition').keypress(function (e) {
        if (e.which == 13) {
            return false;
        }
    });
    $('#popupListDefinition').on("popupafteropen", function () {
        window.setTimeout(function () {
            $('#testButtonCancel')[0].focus();
        }, 1000);
    });

    if (jobCustomerNumber == CASEYS || jobCustomerNumber == CW) {
        $('#uploadListDemoHints p a').text('Turn Hints Off');
        $('#uploadSelectedListDemoHints p a').text('Turn Hints Off');
        $('#uploadedListDemoHint p a').text('Turn Hints Off');
    }
    if (!dontShowHintsAgain && (showListUploadDemoHints()) && (sessionStorage.showDemoHints == undefined || sessionStorage.showDemoHints == null || sessionStorage.showDemoHints == "" || sessionStorage.showDemoHints == "on") && (sessionStorage.isListUploadDemoHintsDisplayed == undefined || sessionStorage.isListUploadDemoHintsDisplayed == null || sessionStorage.isListUploadDemoHintsDisplayed == "false")) {
        sessionStorage.isListUploadDemoHintsDisplayed = true;
        openDemoHints();
    }
    if (!dontShowHintsAgain && (showListUploadingDemoHints()) && (sessionStorage.showDemoHints == undefined || sessionStorage.showDemoHints == null || sessionStorage.showDemoHints == "" || sessionStorage.showDemoHints == "on") && (sessionStorage.isListUploadingHintsDisplayed == undefined || sessionStorage.isListUploadingHintsDisplayed == null || sessionStorage.isListUploadingHintsDisplayed == "" || sessionStorage.isListUploadingHintsDisplayed == "false")) {
        sessionStorage.isListUploadingHintsDisplayed = true;
        window.setTimeout(function openHintPopup() {
            $('#uploadingListHints').popup('open', { positionTo: '#navHeader' });
        }, 1000);
    }

    (($.browser.msie) ? $(window.frames[0].frames.frameElement).contents().find('input[type=file]') : $('input[type=file]')).bind('change', function (e) {
        pageObj.navigate(e, $(e.target))
    });
    persistNavPanelState();
    pagePrefs = jQuery.parseJSON(sessionStorage.ListManagementPrefs);
    $('#txtKeyCode').ForceNumericOnly();
    if (!fnVerifyScriptBlockDict(pagePrefs))
        return false;

    if (jobCustomerNumber == DCA || jobCustomerNumber == KUBOTA || jobCustomerNumber == OH || jobCustomerNumber == SPORTSKING || jobCustomerNumber == GWA) {
        $('div[data-role="content"] h2').html("List Upload");
    }

    if (pagePrefs != null) {
        pageObj.displayListFileInfo();
        //eval(getURLDecode(pagePrefs.scriptBlockDict.pageshow));
        if (sessionStorage.roleName == "user" || sessionStorage.roleName == "power") {
            $('#dvListType').css('display', 'none');
        }
    }
    if (isLoadOrderedLocations(pageObj.customerNumber)) {
        pageObj.loadOrderedLocations();        
    }
    else {
        $('#dvLocations').css('display', 'none');
        $('#dvDoNotAssign').css('display', 'none');
    }
    if (jobCustomerNumber == BRIGHTHOUSE || jobCustomerNumber == BBB || jobCustomerNumber == AAG || jobCustomerNumber == TRIBUNEPUBLISHING || jobCustomerNumber == SANDIEGO || jobCustomerNumber == AH || jobCustomerNumber == SPORTSAUTHORITY || jobCustomerNumber == WALGREENS || jobCustomerNumber == DATAWORKS || jobCustomerNumber == SAFEWAY || jobCustomerNumber == LOWES)
        pageObj.loadSelectedStores('List');
    
    if ('draggable' in document.createElement('span'))
        $('#spnBtnText').html('"Upload"');
    else
        $('#spnBtnText').html('"Upload Selected Lists"');

    $('#okBut').attr('onclick', 'pageObj.closeDialog();');
    //For now "Keycode" value is hardcoded, later times it would be handled as per the given requirements.  
    //Also it should be displayed for all user. Later it should be for admin user only.

    window.setTimeout(function getDealy() { pageObj.displayDropAllStatesExcept(); }, 1000);
    $('#txtKeyCode').val("05100100");

    //if ((jobCustomerNumber == "1" || (jobCustomerNumber == BBB || jobCustomerNumber == AAG || jobCustomerNumber == TRIBUNEPUBLISHING || jobCustomerNumber == AH || jobCustomerNumber == SPORTSAUTHORITY || jobCustomerNumber == WALGREENS || jobCustomerNumber == DATAWORKS || jobCustomerNumber == SAFEWAY || jobCustomerNumber == LOWES) && appPrivileges.roleName == "user"))
     //   $('#dvDropAllStates').hide();

    if (jobCustomerNumber == CW /* && appPrivileges.roleName == "admin" */) {
        if (appPrivileges.roleName != "admin") {
            $('#dvKeyCodeDetails').hide();
        }
        $('#dvDropAllStates').show();
    }
    else {
        $('#dvDropAllStates').hide();
    }
    $('#sldrShowHints').slider("refresh");
    $.each($('input[type=checkbox]'), function (a, b) {
        $(this).checkboxradio();
    });
    $.each($('fieldset[data-role="controlgroup"]'), function (a, b) {
        $(this).controlgroup();
    });
});

var openDemoHints = function () {
    window.setTimeout(function openHintPopup() {
        $('#uploadListDemoHints').popup('open', { positionTo: '#navHeader' });
    }, 2000);
};
function openHint() {
    $('#popupDialog').popup('close');
    window.setTimeout(function openHintPopup() {
        $('#uploadedListDemoHint').popup('open', { positionTo: '#uploadList' });
    }, 1000);
}

//Checks the given string ends with a specific (given character).
String.prototype.endsWith = function (suffix) {
    return this.indexOf(suffix, this.length - suffix.length) !== -1;
};
//******************** Page Load Events End **************************
