﻿//jobScheduleMilestones.js
isCWMilestonesPreferences = function () {
    var customer = getCustomerNumber();
    return ([
            ANDERSEN,
            DKI,
            ICYNENE,
            REGIS,
            SCOTTS,
            "203",
            SUNTIMES,
            BRIGHTHOUSE,
            JETS,
            ALLIED,
            DCA,
            AAG,
            KUBOTA,
            OH,
            SPORTSKING,
            GWA,
            REDPLUM,
            BUFFALONEWS,
            PIONEERPRESS,
            EQUINOX,
            SHERWINWILLIAMS,
            MILWAUKEE,
            BENJAMINMOORE,
            TRAEGER
    ].indexOf(customer) > -1);
}

isCreateMilestoneDemoHints = function () {
    var customer = getCustomerNumber();
    return ([
            ANDERSEN,
            DKI,
            ICYNENE,
            CASEYS,
            CW,
            DCA,
            AAG,
            KUBOTA,
            OH,
            SPORTSKING,
            GWA,
            SCOTTS,
            EQUINOX,
            SHERWINWILLIAMS,
            MILWAUKEE,
            BENJAMINMOORE,
            TRAEGER
        ].indexOf(customer) > -1);
}

isMilestoneHintTextChange = function () {
    var customer = getCustomerNumber();
    return ([DKI, ICYNENE].indexOf(customer) > -1);
}

showMilestonesDemoHInt = function () {
    var customer = getCustomerNumber();
    return ([
                DKI,
                ICYNENE,
                CASEYS,
                CW,
                DCA,
                AAG,
                KUBOTA,
                OH,
                SPORTSKING,
                GWA,
                SCOTTS,
                EQUINOX,
                ANDERSEN,
                SHERWINWILLIAMS,
                MILWAUKEE,
                BENJAMINMOORE,
                TRAEGER
    ].indexOf(customer) > -1);
}

showDataArrival = function () {
    var customer = getCustomerNumber();
    return ([CW, REGIS, SCOTTS, SHERWINWILLIAMS, "203", BRIGHTHOUSE, OH, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);
}

isNeededToVerifyIsRequiredAttribute = function () {
    var customer = getCustomerNumber();
    return ([CW, REGIS, SCOTTS, BRIGHTHOUSE, ANDERSEN, SHERWINWILLIAMS, "203", MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);
}

isValidationRequiredForDataAndListArrival = function () {
    var customer = getCustomerNumber();
    return ([CW, REGIS, SCOTTS, "203", BRIGHTHOUSE, OH, ANDERSEN, SHERWINWILLIAMS, MILWAUKEE, BENJAMINMOORE, TRAEGER].indexOf(customer) > -1);
}