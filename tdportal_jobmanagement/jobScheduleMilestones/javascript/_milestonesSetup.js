﻿var milestonesSetup = function () {
    var self = this;
    self.gData;
    self.gOutputData;
    self.gOutputServiceUrl = '../JSON/_jobSetupOutput.JSON';
    self.jobNumber = "";
    self.facilityId = "";
    self.gServiceUrl = "";
    self.jobNumber = getSessionData("jobNumber");
    self.facilityId = sessionStorage.facilityId;
    self.caseysStaicJobNumber = sessionStorage.jobNumber;
    var inhome_date_param = (sessionStorage.jobSetupOutput != undefined && sessionStorage.jobSetupOutput != null) ? jQuery.parseJSON(sessionStorage.jobSetupOutput).inHomeDate.replace(/\//g, "_") : "";
    
    if (jobCustomerNumber == CASEYS) //cgs    
    //gServiceUrl = serviceURLDomain + "api/Milestone/" + self.facilityId + "/" + self.caseysStaicJobNumber + "/" + job_customer_number + "/caseysOnline/null";
        self.gServiceUrl = serviceURLDomain + "api/Milestone/" + self.facilityId + "/" + self.caseysStaicJobNumber + "/" + jobCustomerNumber + "/caseysOnline/" + inhome_date_param;
    else {
        //var inhome_date_param = (sessionStorage.jobSetupOutput != undefined && sessionStorage.jobSetupOutput != null) ? jQuery.parseJSON(sessionStorage.jobSetupOutput).inHomeDate.replace(/\//g, "_") : "";
        self.gServiceUrl = serviceURLDomain + "api/Milestone/" + self.facilityId + "/" + sessionStorage.jobNumber + "/" + jobCustomerNumber + "/null/" + inhome_date_param;
    }

    self.getEmailLists = function () {
        window.setTimeout(function () {
            $.each($('#dvEmailRecipients div[data-role=collapsible]'), function () {
                $(this).on("collapsiblecollapse", function (e) {
                    $(this).data("previous-state", "collapsed");
                    //$(this).find('.ui-input-clear').unbind('click');
                });
                $(this).on("collapsibleexpand", function (e) {
                    $(this).data("previous-state", "expanded");
                    //$('.ui-content').on('click', '.ui-input-clear', function (e) {
                    self.getData(this.id);
                });
                $(this).find('.ui-input-clear').bind('click', function (e) {
                    if (e.originalEvent) {
                        vm.search();
                    }
                });
            });
            window.setTimeout(function () {
                $('#dvEmailRecipients').collapsibleset('refresh');
                $('#dvEmailRecipients').trigger('create');
                $('#editEmailListPopup').popup('open');
            }, 500);
        }, 500);
    };

    //Loads the page information.
    self.loadData = function () {
        pagePrefs = jQuery.parseJSON(sessionStorage.scheduleMilestonesPrefs);

        if (!fnVerifyScriptBlockDict(pagePrefs))
            return false;

        if (pagePrefs != null) {
            //gServiceUrl = eval(getURLDecode(pagePrefs.scriptBlockDict.gServiceUrl));
            self.makeGData();
            managePagePrefs(jQuery.parseJSON(sessionStorage.scheduleMilestonesPrefs));
        }
        //if (appPrivileges.customerNumber == CASEYS && sessionStorage.userRole != "admin") //for CGS and normal user should not get Save button in any of the pages
        //{
        //    $('#btnSaveTop').css('visibility', 'hidden');
        //    $('#btnDistSignUpEmail').css('display', 'none');
        //    $('#btnDistApprovalEmail').css('display', 'none');
        //}
    };
    //Prepares the data for page to load.
    self.makeGData = function () {
        getCORS(self.gServiceUrl, null, function (data) {
            if (jobCustomerNumber == JETS) {
                data = {
                    "milestoneDict": {
                        "signUpStart": {
                            "ranking": 5,
                            "hasChanged": 1,
                            "isRequired": 1,
                            "isVisible": true,
                            "isIndex": false,
                            "description": "Opt-In Start",
                            "scheduledDate": "11/2/2015",
                            "scheduledBy": "",
                            "completedDate": "",
                            "completedBy": ""
                        },
                        "signUpEnd": {
                            "ranking": 10,
                            "hasChanged": 1,
                            "isRequired": 1,
                            "isVisible": true,
                            "isIndex": false,
                            "description": "Opt-In End",
                            "scheduledDate": "11/9/2015",
                            "scheduledBy": "",
                            "completedDate": "",
                            "completedBy": ""
                        },
                        "approvalStart": {
                            "ranking": 15,
                            "hasChanged": 1,
                            "isRequired": 1,
                            "isVisible": true,
                            "isIndex": false,
                            "description": "Approval Start",
                            "scheduledDate": "11/13/2015 ",
                            "scheduledBy": "",
                            "completedDate": "",
                            "completedBy": ""
                        },
                        "approvalEnd": {
                            "ranking": 20,
                            "hasChanged": 1,
                            "isRequired": 1,
                            "isVisible": true,
                            "isIndex": false,
                            "description": "Approval End",
                            "scheduledDate": "11/20/2015",
                            "scheduledBy": "",
                            "completedDate": "",
                            "completedBy": ""
                        },
                        "inhome": {
                            "ranking": 95,
                            "hasChanged": 1,
                            "isRequired": 1,
                            "isVisible": true,
                            "isIndex": false,
                            "description": "In-Home",
                            "scheduledDate": "12/1/2015 ",
                            "scheduledBy": "",
                            "completedDate": "",
                            "completedBy": ""
                        }
                    }
                }
            }
            //else if (jobCustomerNumber == CASEYS) {
            //    if (data.milestoneDict.atUsps != undefined && data.milestoneDict.atUsps != null) data.milestoneDict.atUsps.isVisible = false;
            //    if (data.milestoneDict.invoice != undefined && data.milestoneDict.invoice != null) data.milestoneDict.invoice.isVisible = false;
            //    if (data.milestoneDict.is != undefined && data.milestoneDict.is != null) data.milestoneDict.is.isVisible = false;
            //    if (data.milestoneDict.lettershopDropProduction != undefined && data.milestoneDict.lettershopDropProduction != null) data.milestoneDict.lettershopDropProduction.isVisible = false;
            //    if (data.milestoneDict.mailDrop != undefined && data.milestoneDict.mailDrop != null) data.milestoneDict.mailDrop.isVisible = false;
            //    if (data.milestoneDict.lettershopDropRelease != undefined && data.milestoneDict.lettershopDropRelease != null) data.milestoneDict.lettershopDropRelease.isVisible = false;
            //}
            self.gData = data;
            if ((showDataArrival()) && self.gData.milestoneDict != undefined && self.gData.milestoneDict.list != undefined && self.gData.milestoneDict.list.description != undefined) //"Data"/ "list arrival" to be displayed as "Data Arrival" - Charlie mail dated 11/5/2013
                if (self.gData.milestoneDict.list.description.toLowerCase() == "data" || self.gData.milestoneDict.list.description.toLowerCase() == "list arrival")
                    self.gData.milestoneDict.list.description = "Data Arrival";
            self.gData.milestoneDict = sortMilestones(self.gData.milestoneDict);

            $.each(self.gData.milestoneDict, function (keyMileSt, valMileSt) {//added temporarily till this value returns from web service.
                if ((["art arrival", "data", "data arrival"].indexOf(valMileSt.description.toLowerCase()) > -1 || valMileSt.description.toLowerCase().indexOf("list") != -1) && (isNeededToVerifyIsRequiredAttribute())) {
                    valMileSt.isRequired = 1;
                }
            });
            if (appPrivileges.customerNumber == "1" && self.jobNumber != "-1")
                self.loadComments();
            self.makeGOutputData();
        }, function (error_response) {
            showErrorResponseText(error_response, true);
        });
    };
    //Prepares the data that has to be displayed in the fields from output session.
    self.makeGOutputData = function () {
        if (sessionStorage.jobSetupOutput != undefined) {
            self.gOutputData = jQuery.parseJSON(sessionStorage.jobSetupOutput);
            if (self.gOutputData.milestoneAction.milestoneDict != undefined && self.gOutputData.milestoneAction.milestoneDict != null && JSON.stringify(self.gOutputData.milestoneAction.milestoneDict).length == 2 && self.gData != null) {
                self.gOutputData.milestoneAction["milestoneDict"] = [];
                self.gOutputData.milestoneAction["milestoneDict"] = (self.gData.milestoneDict != undefined) ? self.gData.milestoneDict : [];
            }
            else if ((self.gOutputData.milestoneAction.milestoneDict == undefined || self.gOutputData.milestoneAction.milestoneDict == null || JSON.stringify(self.gOutputData.milestoneAction.milestoneDict).length == 2) && self.gData != null) {
                self.gOutputData.milestoneAction["milestoneDict"] = [];
                self.gOutputData.milestoneAction["milestoneDict"] = (self.gData.milestoneDict != undefined) ? self.gData.milestoneDict : [];
            }
            if (self.gOutputData.templateName.toLowerCase().indexOf('solo') > -1 || self.gOutputData.templateName.toLowerCase().indexOf('quarterly') > -1 || self.gOutputData.templateName.toLowerCase().indexOf('delivery') > -1) {
                if (self.gData.milestoneDict.signUpStart != undefined && self.gData.milestoneDict.signUpStart != null) self.gData.milestoneDict.signUpStart.isVisible = false;
                if (self.gData.milestoneDict.signUpEnd != undefined && self.gData.milestoneDict.signUpEnd != null) self.gData.milestoneDict.signUpEnd.isVisible = false;
                if (self.gData.milestoneDict.approvalStart != undefined && self.gData.milestoneDict.approvalStart != null) self.gData.milestoneDict.approvalStart.isVisible = false;
                if (self.gData.milestoneDict.approvalEnd != undefined && self.gData.milestoneDict.approvalEnd != null) self.gData.milestoneDict.approvalEnd.isVisible = false;
            }
        }
        else {
            self.gOutputData["milestoneAction"] = self.gData;
        }

        if (self.gData.milestoneDict != undefined) {
            if (self.gData.milestoneDict.inhome!=undefined && self.gData.milestoneDict.inhome.scheduledDate == "") {
                if (self.gOutputData.milestoneAction.milestoneDict != undefined)
                    self.gOutputData.milestoneAction.milestoneDict.inhome.scheduledDate = self.gOutputData.inHomeDate;
            }
        }

        if (sessionStorage.jobSetupOutputCompare != undefined) {
            var goutput_compare_data = jQuery.parseJSON(sessionStorage.jobSetupOutputCompare);
            if (goutput_compare_data.milestoneAction.milestoneDict != undefined && JSON.stringify(goutput_compare_data.milestoneAction.milestoneDict).length == 2 && self.gData != null) {
                goutput_compare_data.milestoneAction["milestoneDict"] = [];
                goutput_compare_data.milestoneAction["milestoneDict"] = (self.gData.milestoneDict != undefined) ? self.gData.milestoneDict : [];
                if (self.gData.milestoneDict != undefined) {
                    if (self.gData.milestoneDict.inhome!=undefined && self.gData.milestoneDict.inhome.scheduledDate == "" && (self.gOutputData.inHomeDate == goutput_compare_data.inHomeDate)) {
                        goutput_compare_data.milestoneAction.milestoneDict.inhome.scheduledDate = self.gOutputData.inHomeDate;
                    }
                }

                sessionStorage.jobSetupOutputCompare = JSON.stringify(goutput_compare_data);
            }
            else {
                if (self.gData.milestoneDict != undefined) {
                    if (self.gData.milestoneDict.inhome!=undefined && self.gData.milestoneDict.inhome.scheduledDate == "" && (self.gOutputData.inHomeDate == goutput_compare_data.inHomeDate)) {
                        goutput_compare_data.milestoneAction.milestoneDict.inhome.scheduledDate = self.gOutputData.inHomeDate;
                        sessionStorage.jobSetupOutputCompare = JSON.stringify(goutput_compare_data);
                    }
                }
            }
        }
        self.buildLists();
        self.buildOutputLists();
        $('#btnDistSignUpEmail').click(function () {
            self.sendDistrictEmails('SU');
        });
        $('#btnDistApprovalEmail').click(function () {
            self.sendDistrictEmails('AP');
        });
    };
    //Change event for the dropdown on milestone section and persforms the needed action.
    self.updateStartIndex = function (ctrl) {
        //if ($("#hdnMilestoneFilter").val() != "standard") 
        if ($("#hdnMilestoneFilter").val() == "rush") {
            if (self.fnMilestonesValidation()) {
                $("#hdnMilestoneFilter").val($("#" + ctrl.id).val());
                self.buildLists();
                self.buildOutputLists();
            }
            else {
                $('#selMilestones').val($("#hdnMilestoneFilter").val());
            }
        }
        else {
            $("#hdnMilestoneFilter").val($("#" + ctrl.id).val());
            self.buildLists();
            self.buildOutputLists();
        }
    };
    //Closes the milestone popup.
    self.closeEditMilestonePopUp = function (id) {
        $(id).keypress(function (e) {
            if (e.keyCode == 13) {
                $(this).trigger("enterKey");
                //$('#popupLogin').popup('open');
                $('#popupLogin').popup('open');
                $('#popupLogin').find('#btnSubmit').trigger('click');
                //$('#popupLogin').popup('close');
                $('#popupLogin').popup('open');
                return false;
            }
        });
    };
    //Creates milestone items on the page.
    self.makeMileStones = function (value) {
        var milestones_content = '';
        var counter = 0;
        var header = "";
        var display_block_days = "";
        var date_picker_dates = "";

        $("#ulMileStones").empty();
        $("#spnHeaderMilestones").text("Schedule Milestones");
        var mile_stone_data = (value.length > 0) ? value[0] : value;
        $.each(mile_stone_data, function (keyMileSt, valMileSt) {
            keyMileSt = keyMileSt.toLowerCase();
            //if (appPrivileges.customerNumber != CW || ((valMileSt.description.toLowerCase() == "art arrival" || valMileSt.description.toLowerCase() == "data" || valMileSt.description.toLowerCase() == "data arrival" || valMileSt.description.toLowerCase().indexOf("list") != -1 || valMileSt.description.toLowerCase() == "in-home" || valMileSt.description.toLowerCase() == "sale start" || valMileSt.description.toLowerCase() == "sale end") && appPrivileges.customerNumber == CW)) {        
            if (valMileSt.isVisible != undefined && valMileSt.isVisible) {
                //eval(getURLDecode(pagePrefs.scriptBlockDict.makeMileStones));
                if (counter == 0) {
                    header = '<li data-role="list-divider" data-theme="f"><a target="_self" href="#" style="text-decoration:none; color:#fff;"></a>';
                    var sel_option = "";
                    if (((jobCustomerNumber == JETS) && (sessionStorage.userRole == "user" || sessionStorage.userRole == "power")) || (jobCustomerNumber == CASEYS && sessionStorage.userRole == "user")) { // for CGS
                        sel_option = "standard";
                        $("#hdnMilestoneFilter").val(sel_option);
                    } else {
                        sel_option = $("#hdnMilestoneFilter").val();
                    }
                    header += '<div id="containDiv" style="position: relative; left:65%; right: 10px; margin: 0; width:35%;" data-role="controlgroup" data-type="horizontal" data-mini="true" >';
                    header += '<select name="selMilestones" id="selMilestones" data-theme="e" data-overlay-theme="d" data-native-menu="true" data-mini="true" onchange="pageObj.updateStartIndex(this);" data-native-menu="false">';
                    header += (sel_option == 'standard') ? '<option value="standard" selected>View All</option>' : '<option value="standard">View All</option>';
                    header += (sel_option == 'rush') ? '<option value="rush" selected>Edit Scheduled Date</option>' : '<option value="rush">Edit Scheduled Date</option>';
                    header += (sel_option == 'express') ? '<option value="express" selected>Edit Completed Date</option>' : '<option value="express">Edit Completed Date</option>';
                    header += '</select></div>';
                    header += '</li>'
                    $("#ulMileStones").append(header);
                }
                var asterisk = (valMileSt.isRequired == 1) ? "*" : "";
                $("#spnHeaderMilestones").text($("#hdnMilestoneFilter").val() == 'standard' ? "Milestones" : $("#hdnMilestoneFilter").val() == 'rush' ? "Schedule Milestones" : "Completed Milestones");

                if ($("#hdnMilestoneFilter").val() == 'standard') {
                    //if ((jobCustomerNumber == CASEYS || jobCustomerNumber == JETS) && (sessionStorage.userRole == "user" || sessionStorage.userRole == "power")) { // for CGS
                    if (((jobCustomerNumber == JETS) && (sessionStorage.userRole == "user" || sessionStorage.userRole == "power")) || (jobCustomerNumber == CASEYS && sessionStorage.userRole == "user")) {
                        milestones_content = '<li data-icon="false" id="edit" style="cursor:default"><label for="txt ' + keyMileSt + '">' + asterisk + ' ' + valMileSt.description + ' </label><p>';
                    } else {
                        milestones_content = '<li data-icon="edit" id="edit" style="cursor:default" data-theme="c"  ><a data-rel="dialog" onclick=nonEditableMilestoneMessage("' + keyMileSt + '"); data-rel="popup" data-position-to="window"  data-inline="true"><label for="txt ' + keyMileSt + '">' + asterisk + ' ' + valMileSt.description + ' </label><p>';
                    }
                    milestones_content += '<br /><span name="spnSche' + keyMileSt + '" id="spnSche' + keyMileSt + '" data-mini="true"  />';
                    milestones_content += '<br /><span name="spnComp' + keyMileSt + '" id="spnComp' + keyMileSt + '" data-mini="true"  />';

                    if (keyMileSt.toLowerCase().indexOf("inhome") > -1)
                        milestones_content += '<br /><label for="schedDate" style="font-weight:normal;font-size:12px;">All orders require a minimum of a 3 day in-home designation window.</label>';
                }
                else {
                    milestones_content = '<li data-icon="false" style="cursor:default" data-theme="c" ><a><label for="txt ' + keyMileSt + '">' + (($("#hdnMilestoneFilter").val() == 'rush') ? asterisk : "") + ' ' + valMileSt.description + ' </label><p>';
                    if (valMileSt.description.toLowerCase() == "shipped" && $("#hdnMilestoneFilter").val() == 'rush')
                        milestones_content += '<br /><input type="text" onfocus="nonEditableMilestoneMessage(\'' + keyMileSt + '\');" onmousedown="nonEditableMilestoneMessage(\'' + keyMileSt + '\');" name="txt' + keyMileSt + '" id="txt' + keyMileSt + '" placeholder="MM/DD/YY" class="ui-body-c ui-corner-all ui-shadow-inset" style="height:20px;width:98%" data-mini="true"  /><br />';
                    else
                        milestones_content += '<br /><input type="text" onfocus="showDatePicker();" onblur="return pageObj.dateValidationAndPersist(this, 0, \'' + keyMileSt + '\');" name="txt' + keyMileSt + '" id="txt' + keyMileSt + '" placeholder="MM/DD/YY" class="ui-body-c ui-corner-all ui-shadow-inset" style="height:20px;width:98%" data-mini="true"  /><br />';
                    if (keyMileSt.toLowerCase().indexOf("inhome") > -1)
                        milestones_content += '<label for="schedDate" style="font-weight:normal;font-size:12px;">All orders require a minimum of a 3 day in-home designation window.</label>';
                }
                if (((jobCustomerNumber == JETS) && (sessionStorage.userRole == "user" || sessionStorage.userRole == "power")) || (jobCustomerNumber == CASEYS && sessionStorage.userRole == "user")) {//((jobCustomerNumber == CASEYS || jobCustomerNumber == JETS) && (sessionStorage.userRole == "user" || sessionStorage.userRole == "power")) { // for CGS
                    milestones_content += '</p></li>';
                } else {
                    milestones_content += '</p></a></li>';
                }

                $("#ulMileStones").append(milestones_content);

                //if (appPrivileges.customerNumber == CW && sessionStorage.jobNumber != "-1") {
                //    $('#txtinhome').addClass('ui-disabled'); //inhome date should be disabled for saved jobs (greater than -1)
                //    $('#txtinhome').css('opacity', '0.7');
                //}

                display_block_days = "";
                date_picker_dates = "";

                if (jobCustomerNumber == CASEYS || jobCustomerNumber == JETS) { // for CGS
                    var in_home_date = new Date(self.gOutputData.inHomeDate);
                    var curr_date = new Date();
                    var temp_date = in_home_date - curr_date;
                    var one_day = 1000 * 60 * 60 * 24;

                    switch (keyMileSt.toLowerCase()) {
                        case "signupstart":
                            display_block_days = Math.floor((in_home_date.getTime() - curr_date.getTime()) / one_day) - (42 - 1);
                            date_picker_dates = Math.floor((in_home_date.getTime() - curr_date.getTime()) / one_day) - (42 - 1);  // 6 week prior to inhome date
                            break;
                        case "signupend":
                            display_block_days = Math.floor((in_home_date.getTime() - curr_date.getTime()) / one_day) - (35 - 1);
                            date_picker_dates = Math.floor((in_home_date.getTime() - curr_date.getTime()) / one_day) - (35 - 1); //1 week after start date
                            break;
                        case "approvalsstart":
                            display_block_days = Math.floor((in_home_date.getTime() - curr_date.getTime()) / one_day) - (28 - 1);
                            date_picker_dates = Math.floor((in_home_date.getTime() - curr_date.getTime()) / one_day) - (28 - 1); // 4 week prior to inhome date
                            break;
                        case "approvalsend":
                            display_block_days = Math.floor((in_home_date.getTime() - curr_date.getTime()) / one_day) - (21 - 1);
                            date_picker_dates = Math.floor((in_home_date.getTime() - curr_date.getTime()) / one_day) - (21 - 1); // 1 week after approval start date
                            break;
                        case "inhome":
                            display_block_days = self.gOutputData.inHomeDate;
                            date_picker_dates = self.gOutputData.inHomeDate;
                            break;
                    }
                }

                if ($("#hdnMilestoneFilter").val() != 'standard') {
                    if (!($("#hdnMilestoneFilter").val() == 'rush' && valMileSt.description.toLowerCase() == "shipped"))
                        jQuery("#txt" + keyMileSt).datepicker({
                            minDate: new Date(2009, 0, 1),
                            dateFormat: 'mm/dd/yy',
                            constrainInput: true,
                            beforeShowDay: function (date) {
                                if (keyMileSt == "inhome") {
                                    if (showFixedInHomeDatesForSelection() && $("#hdnMilestoneFilter").val() == "rush") {
                                        return displaySelectedDays(date);
                                    }
                                    else {
                                        return displayBlockDays(date, (sessionStorage.userRole != "admin" && ("txt" + keyMileSt) == 'txtinhome') ? ((jobCustomerNumber == DCA || jobCustomerNumber == SPORTSKING) ? 15 : ((jobCustomerNumber == OH) ? 7 : 21)) : display_block_days);
                                    }                                   
                                }                                
                                else
                                    return displayBlockDaysWithSundays(date, (sessionStorage.userRole != "admin" && ("txt" + keyMileSt) == 'txtinhome') ? ((jobCustomerNumber == DCA || jobCustomerNumber == SPORTSKING) ? 15 : ((jobCustomerNumber == OH) ? 7 : 21)) : display_block_days);
                            },
                            onSelect: function () {
                                self.fnSaveMilestones(keyMileSt);
                            }
                        });

                    //$("#txt" + keyMileSt).datepicker('setDate', (sessionStorage.userRole != "admin" && ("txt" + keyMileSt) == 'txtinhome') ? '21d' : date_picker_dates);
                }
                if (((jobCustomerNumber == JETS) && (sessionStorage.userRole == "user" || sessionStorage.userRole == "power")) || (jobCustomerNumber == CASEYS && sessionStorage.userRole == "user")){//((jobCustomerNumber == CASEYS || jobCustomerNumber == JETS) && (sessionStorage.userRole == "user" || sessionStorage.userRole == "power")) {
                    $('#containDiv').css('height', '20px');
                }
                //if (appPrivileges.customerNumber != CASEYS) {
                //    $("#containDiv select").each(function (i) {
                //        $(this).selectmenu();
                //    });
                //}
                $("#containDiv select").each(function (i) {
                    $(this).selectmenu();
                });
                window.setTimeout(function delay() { $("#ulMileStones").listview('refresh'); }, 50);
                counter++;
            }
        });
    };
    //Triggers the sending the district manager emails.
    self.sendDistrictEmails = function (mail_type) {
        var web_url_managers = serviceURLDomain + 'api/DistrictEmail/' + jobCustomerNumber + '/' + self.facilityId + '/' + self.caseysStaicJobNumber + '/' + mail_type;
        getCORS(web_url_managers, null, function (response) {
            $('#alertmsg').html(response);
            $("#popupDialog").popup('open');
        }, function (error_response) {
            //$('#alertmsg').html(error_response.responseText);
            //$('#popupDialog').css('display', 'block');
            showErrorResponseText(error_response, false);
        });
    };
    //Populates the milstone edit window.
    self.populateForm = function (pk) {
        $("#hdnSelectedMilestone").val(pk);
        var single_data;
        $.each(self.gOutputData["milestoneAction"].milestoneDict, function (key, val) {
            key = key.toLowerCase();
            if (key == pk) {
                if (val.scheduledDate == "1/1/1900 12:00:00 AM") {
                    val.scheduledDate = "";
                }
                if (val.completedDate == "1/1/1900 12:00:00 AM") {
                    val.completedDate = "";
                }
                $("#scheduledDate").val((val.scheduledDate.indexOf(' ') == -1) ? val.scheduledDate : val.scheduledDate.substring(0, val.scheduledDate.indexOf(' ')));
                $("#completedDate").val((val.completedDate.indexOf(' ') == -1) ? val.completedDate : val.completedDate.substring(0, val.completedDate.indexOf(' ')));

            }
        });

        jQuery("#scheduledDate").datepicker({
            minDate: new Date(2009, 0, 1),
            dateFormat: 'mm/dd/yy',
            constrainInput: true,
            beforeShowDay: function (date) {
                if ($("#hdnSelectedMilestone").val() == "inhome")
                    return displayBlockDays(date, 0);
                else
                    return displayBlockDaysWithSundays(date, 0);
            }
        });

        jQuery("#completedDate").datepicker({
            minDate: new Date(2009, 0, 1),
            dateFormat: 'mm/dd/yy',
            constrainInput: true,
            beforeShowDay: function (date) {
                if ($("#hdnSelectedMilestone").val() == "inhome")
                    return displayBlockDays(date, 0);
                else
                    return displayBlockDaysWithSundays(date, 0);
            }
        });
        $('#scheduledDate').datepicker('disable');
        $('#completedDate').datepicker('disable');
        $(document).on("popupafteropen", "#popupLogin", function (event, ui) {
            $('#scheduledDate').datepicker('enable');
            $('#completedDate').datepicker('enable');
        });
        $('#popupLogin').popup('open');
    };
    //Updates the values entered in the popup window in the milestone object.
    self.updateVal = function () {
        if (!fnDateFormatMMDDYYYY('scheduledDate', 1)) //date format validation
            return false;
        if (!fnDateFormatMMDDYYYY('completedDate', 1)) //date format validation
            return false;

        var sched_date = $("#scheduledDate").val();
        var cmpl_date = $("#completedDate").val();

        var msg = "";
        $.each(self.gOutputData["milestoneAction"].milestoneDict, function (key, value) {
            key = key.toLowerCase();
            if (value.isRequired == 1 && $("#hdnSelectedMilestone").val() == key && $("#scheduledDate").val() == "") {
                msg = value.description + " date is mandatory!";
            }
        });
        if (msg != "") {
            //$("#popupLogin").popup("close");
            $('#popupLogin').popup('close');
            $('#alertmsg').html(msg);
            $("#popupDialog").popup('open');
            return false;
        }

        msg = self.popupValidation(self.gOutputData["milestoneAction"].milestoneDict, sched_date, cmpl_date, 'scheduledDate');
        if (msg != "") {
            msg = "<b>Scheduled Milestones</b> </br></br>" + msg;
        }
        //var msg_completed = popupValidation(self.gOutputData["milestoneAction"].milestoneDict, sched_date, cmpl_date, 'completedDate');
        //if (msg_completed != "") {
        //    msg += "</br><b>Completed Milestones</b> </br></br>" + msg_completed;
        //}
        if (msg != "") {
            // $("#popupLogin").popup("close");
            $('#popupLogin').popup('close');
            $('#alertmsg').html(msg);
            window.setTimeout(function () {
                $("#popupDialog").popup('open');
            }, 200);
            return false;
        }

        $.each(self.gOutputData["milestoneAction"].milestoneDict, function (key, val) {
            key = key.toLowerCase();
            if (key == $("#hdnSelectedMilestone").val()) {
                val.scheduledDate = sched_date;
                val.scheduledBy = sessionStorage.username;
                val.completedDate = cmpl_date;
                val.completedBy = sessionStorage.username;
                val.hasChanged = 1;
            }
            if (key == "inhome")
                self.gOutputData.inHomeDate = sched_date;
        });

        sessionStorage.jobSetupOutput = JSON.stringify(self.gOutputData);
        self.makeMileStones(self.gOutputData["milestoneAction"].milestoneDict);
        //$("#popupLogin").popup("close");
        $('#popupLogin').popup('close');
        self.buildOutputLists();
    };
    //Saves the selected milestone dates.
    self.fnSaveMilestones = function (sel_key_name) {
        $.each(self.gOutputData["milestoneAction"].milestoneDict, function (key, val) {
            key = key.toLowerCase();
            if (sel_key_name == key) {
                if ($("#hdnMilestoneFilter").val() == "rush") {
                    val.scheduledDate = $("#txt" + key).val();
                    val.scheduledBy = sessionStorage.username;
                    val.hasChanged = 1;

                }
                else if ($("#hdnMilestoneFilter").val() == "express") {
                    val.completedDate = $("#txt" + key).val();
                    val.completedBy = sessionStorage.username;
                    val.hasChanged = 1;
                }
                if (key == "inhome")
                    self.gOutputData.inHomeDate = $("#txt" + key).val();
            }
        });
        sessionStorage.jobSetupOutput = JSON.stringify(self.gOutputData);
    };
    //Builds the list of milestone items to be displayed over the page.
    self.buildLists = function () {
        if (self.gData != null) {
            var g_data = self.gData;
            $.each(g_data, function (key, val) {
                switch (key) {
                    case "milestoneDict":
                        if ($("#pageName").val() == "mileStones") {
                            $("#mileStones").empty();
                            $("#mileStones").listview('refresh');
                            self.makeMileStones(val);
                        }
                        break;
                }
            });
        }
    };
    //Builds the milestone item values and binds to respective fields to be displayed over page.
    self.buildOutputLists = function () {
        var one_day = 1000 * 60 * 60 * 24;
        var val2_scheduled_date = "";
        var val2_completed_date = "";
        var dt_diff = "";
        var curr_date = new Date();
        var dt_sche_diff = "";
        if (self.gOutputData != null) {
            getBubbleCounts(self.gOutputData);
        }
        //$.each(self.gOutputData["milestoneAction"], function (key, val) {
        $.each(self.gOutputData, function (key1, val1) {
            if (key1 == "milestoneAction") {
                $.each(val1, function (key, val) {
                    if (key != undefined) {
                        switch (key) {
                            case "milestoneDict":
                                if ($("#pageName").val() == "mileStones") {
                                    var mile_stone_data = (val.length > 0) ? val[0] : val;
                                    $.each(mile_stone_data, function (keyMileStones, valMileStones) {
                                        val2_scheduled_date = "";
                                        val2_completed_date = "";
                                        dt_diff = "";
                                        keyMileStones = keyMileStones.toLowerCase();
                                        if (valMileStones.scheduledDate != "" && valMileStones.scheduledDate != undefined && valMileStones.scheduledDate != "1/1/1900 12:00:00 AM") {
                                            var due_date = new Date(valMileStones.scheduledDate);
                                            val2_scheduled_date = (valMileStones.scheduledDate.indexOf(' ') == -1) ? valMileStones.scheduledDate : valMileStones.scheduledDate.substring(0, valMileStones.scheduledDate.indexOf(' '));
                                            val2_completed_date = (valMileStones.completedDate.indexOf(' ') == -1) ? valMileStones.completedDate : valMileStones.completedDate.substring(0, valMileStones.completedDate.indexOf(' '));
                                            dt_diff = Math.floor((due_date.getTime() - curr_date.getTime()) / one_day);

                                            var due_comp_date = new Date(valMileStones.completedDate);
                                            dt_sche_diff = Math.floor((due_comp_date.getTime() - due_date.getTime()) / one_day);
                                        }
                                        val2_scheduled_date = (val2_scheduled_date == '1/1/1900') ? "" : val2_scheduled_date;
                                        val2_completed_date = (val2_completed_date == '1/1/1900') ? "" : val2_completed_date;

                                        if ($("#hdnMilestoneFilter").val() == "standard") {
                                            $("#spnSche" + keyMileStones).html((val2_scheduled_date) != "" ? "Scheduled: " + val2_scheduled_date : ((jobCustomerNumber != CASEYS && jobCustomerNumber != JETS) ? "Click to enter Scheduled Date" : ""));
                                            $("#spnComp" + keyMileStones).html((val2_completed_date) != "" ? "Completed: " + val2_completed_date : ((jobCustomerNumber != CASEYS && jobCustomerNumber != JETS) ? "Click to enter Completed Date" : ""));
                                            if (val2_completed_date == "") {
                                                if ((dt_diff) < -1)
                                                    $("#spnSche" + keyMileStones).addClass("incompleteMilestone");
                                            }
                                            else {
                                                if (dt_sche_diff <= 0)
                                                    $("#spnComp" + keyMileStones).addClass("completedMilestone");
                                                else
                                                    $("#spnComp" + keyMileStones).addClass("incompleteMilestone");
                                            }
                                        }
                                        else if ($("#hdnMilestoneFilter").val() == "rush" && val2_scheduled_date != "") {
                                            $("#txt" + keyMileStones).val(val2_scheduled_date);
                                        }
                                        else if ($("#hdnMilestoneFilter").val() == "express" && val2_completed_date != "") {
                                            $("#txt" + keyMileStones).val(val2_completed_date);
                                        }

                                        if ($("#hdnMilestoneFilter").val() == "express" && val2_completed_date == "" && $("#txt" + keyMileStones).val() == "" && keyMileStones.toLowerCase().indexOf("inhome") > -1) //if editCompleted inhome date is nothing, we need to show EditSchedule inhome date. Added on 24th Jul, 2013
                                            $("#txt" + keyMileStones).val(val2_scheduled_date);
                                    });
                                }
                                break;
                        }
                    }
                });
            }
        });
        if (appPrivileges.customerNumber == "1" && self.jobNumber != "-1")
            self.createComments(self.addedComments);
        sessionStorage.jobSetupOutput = JSON.stringify(self.gOutputData);
    };
    new validateMilestones(self); // Extends the view model to integrate the milestone validations apiS.
    new manageComments(self); // Extends the view model to integrate the api to manage comments.
};