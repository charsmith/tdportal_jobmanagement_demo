﻿$.mobile.popup.prototype.options.history = false;
jQuery.support.cors = true;
//var job_customer_number = (sessionStorage.jobCustomerNumber != undefined && sessionStorage.jobCustomerNumber != null && sessionStorage.jobCustomerNumber != "") ? sessionStorage.jobCustomerNumber : appPrivileges.customerNumber;

var pageObj;
var dontShowHintsAgain = (localStorage.getItem('doNotShowHintsAgain') != undefined && localStorage.getItem('doNotShowHintsAgain') != null && localStorage.getItem('doNotShowHintsAgain') != "" && localStorage.getItem('doNotShowHintsAgain') != "false") ? true : false;

$('#_jobMilestones').live('pagebeforecreate', function (event) {
    displayNavLinks();
    displayMessage('_jobMilestones');
    confirmNavigationMessage('_jobMilestones');
    createConfirmMessage('_jobMilestones');
    if (sessionStorage.scheduleMilestonesPrefs == undefined || sessionStorage.scheduleMilestonesPrefs == null || sessionStorage.scheduleMilestonesPrefs == "")
        if (isCWMilestonesPreferences())
            getPagePreferences('scheduleMilestones.html', sessionStorage.facilityId, CW);
        else
            getPagePreferences('scheduleMilestones.html', sessionStorage.facilityId, jobCustomerNumber);
    else {
        managePagePrefs(jQuery.parseJSON(sessionStorage.scheduleMilestonesPrefs));
    }
    
    //test for mobility...
    //loadMobility();
    $("#navLinksPanel").on("panelopen", function (event, ui) {
        sessionStorage.isNavPanelOpen = true;
    });
    pageObj = new milestonesSetup();
    if (isCreateMilestoneDemoHints()) {
        setDemoHintsSliderValue();
        //window.setTimeout(function loadHints() {
        createDemoHints("jobMilestones");
        // }, 100);
    }
    window.setTimeout(function loadHints() {
        $('#sldrShowHints').trigger('create');
    }, 3000);
});

$(document).on('pageshow', '#_jobMilestones', function (event) {
    if (jobCustomerNumber == CASEYS || jobCustomerNumber == CW || jobCustomerNumber == AAG || jobCustomerNumber == EQUINOX || isMilestoneHintTextChange()) {
        $('#mileStoneHints p a').text('Turn Hints Off');
    }
    if (!dontShowHintsAgain && (showMilestonesDemoHInt()) && (sessionStorage.showDemoHints == undefined || sessionStorage.showDemoHints == null || sessionStorage.showDemoHints == "" || sessionStorage.showDemoHints == "on") && (sessionStorage.isMileStonesDemoHintsDisplayed == undefined || sessionStorage.isMileStonesDemoHintsDisplayed == null || sessionStorage.isMileStonesDemoHintsDisplayed == "false")) {
        sessionStorage.isMileStonesDemoHintsDisplayed = true;
        $('#mileStoneHints').popup('close');
        window.setTimeout(function loadHints() {
            $('#mileStoneHints').popup('open', { positionTo: '#ulMileStones' });
        }, 500);
    }
    pageObj.loadData();
    window.setTimeout(function () {
        ko.applyBindings(pageObj);
    }, 1000);
    if (((jobCustomerNumber == JETS) && (sessionStorage.userRole == "admin")) || (jobCustomerNumber == CASEYS && sessionStorage.userRole != "user")){//((jobCustomerNumber == CASEYS || jobCustomerNumber == JETS) && sessionStorage.userRole == "admin") {
        $('#btnSaveTop').css('visibility', 'visible');
        $('#btnSave').css('visibility', 'visible');
        //$('#selMilestones').css('visibility', 'visible');

        $('#btnSaveTop').css("display", "");
        $('#btnSave').css("display", "");
        $('#containDiv div').css("display", "block");
    }

    if (appPrivileges.customerNumber != "1") {
        $('#dvFlagComments').css('display', 'none');
        $('#dvCommentsDisplay').css('display', 'none');
    }
    $('#filterChkViewAllComments').attr('checked', true).checkboxradio('refresh');
    $("#popupComments").bind({
        popupafterclose: function (event, ui) {
            $("#okBut").attr("onclick", "$('#popupDialog').popup('close');");
        }
    });
    persistNavPanelState();
    $('#sldrShowHints').slider("refresh");
    //$('#divBottom td').first().find('label').remove();
    //$('#divBottom td').first().css('width', '100px');
    //$('#tdShowHints select').remove();
});