# README #

2016-12-02
----------------
•Casey's Delivery Enhancements, Scotts Enhancements, job management related changes (Sprint 2016.07, 2016.08, 2016.09 and 2016.10 changes)
•tdDev & tdDemo sync for demo.

Created tag 2016.10.002

2017-02-17
----------------
•Scotts Release and Opt-In Links
•tdDev & tdDemo sync for Scotts release.

Created tag 2017.01.001

2017-02-22
----------------
* Scotts Release (Opt-In)
* tdDemo and tdDev sync

Created tag 2017.01.002